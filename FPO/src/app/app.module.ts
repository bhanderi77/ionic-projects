import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { firebaseConfig } from './firebaseCredentials';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { HttpModule } from '@angular/http';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { AnimationService , AnimatesDirective } from 'css-animator';

@NgModule({
	declarations: [
		MyApp,
		HomePage,
		AnimatesDirective
	],
	imports: [
		BrowserModule,
		HttpModule,
		AngularFireModule.initializeApp(firebaseConfig),
		AngularFirestoreModule,
		IonicModule.forRoot(MyApp)
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		HomePage
	],
	providers: [
		StatusBar,
		SplashScreen,
		File,
		FileChooser,
		FilePath,
		{ provide: ErrorHandler, useClass: IonicErrorHandler },
		FirebaseProvider,
		AnimationService
	]
})
export class AppModule { }
