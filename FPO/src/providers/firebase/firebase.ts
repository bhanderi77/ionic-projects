import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore'
import { FarmerDetails } from '../../model/farmer';
import { DocumentReference, WriteBatch } from '@firebase/firestore-types';
/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {

	constructor(private afs: AngularFirestore) {
		console.log('Hello FirebaseProvider Provider');
	}

	async createFarmer(farmer: FarmerDetails): Promise<void> {
		let farmerID = this.afs.createId();
		let docRef: DocumentReference = this.afs.doc(`/FarmerList/${farmerID}`).ref;
		if (docRef) {
			return docRef.set({
				ID: farmerID,
				name: farmer.name,
				city: farmer.city,
				state: farmer.state,
				phoneNumber: farmer.phoneNumber,
				landSize: farmer.landSize
			});
		}
		return null;
	}

	async importFarmers(data):Promise<void> {
		let batch: WriteBatch = this.afs.firestore.batch();
		for (let i = 0; i < data.length; i++){
			let farmerID = this.afs.createId();
			let docRef: DocumentReference = this.afs.doc(`/FarmerList/${farmerID}`).ref;
			if(docRef){
				await batch.set(docRef,{
					ID : farmerID,
					name : data[i].name,
					city: data[i].city,
					state: data[i].state,
					phoneNumber: data[i].phoneNumber,
					landSize: data[i].landSize
				});
			}
		}
		await batch.commit();
		return null;
	}

	getFarmerList(): AngularFirestoreCollection<FarmerDetails> {
		return this.afs.collection<FarmerDetails>(`/FarmerList`, ref => ref.orderBy('state'));
	}

}
