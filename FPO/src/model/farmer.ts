export interface FarmerDetails {
	ID: string;
	name: string;
	crops:Array<string>;
	city: string;
	state: string;
	phoneNumber: number;
	landSize: number;
}

export interface FpoDetails {
	ID: string;
	name: string;
	area: string;
	crops: Array<string>;
}