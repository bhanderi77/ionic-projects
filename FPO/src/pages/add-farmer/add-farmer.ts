import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FarmerDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';

@IonicPage()
@Component({
	selector: 'page-add-farmer',
	templateUrl: 'add-farmer.html',
})
export class AddFarmerPage {

	farmer = {} as FarmerDetails;
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public viewCtrl: ViewController,
		public firebaseProvider: FirebaseProvider) {

	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AddFarmerPage');
	}

	async saveFarmer() {
		await this.firebaseProvider.createFarmer(this.farmer);
		this.viewCtrl.dismiss();
	}

	goToRootPage() {
		this.viewCtrl.dismiss();
	}

}
