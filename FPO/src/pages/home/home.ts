import { Component, ViewChild , ElementRef } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { FarmerDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';
import { File, IFile, FileEntry, Entry } from '@ionic-native/file';
import * as XLSX from 'xlsx';
import { AnimationBuilder , AnimationService } from 'css-animator';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	farmerList: Array<FarmerDetails>;
	data: any;
	importedFarmers: Array<FarmerDetails>;
	private animator: AnimationBuilder;

	@ViewChild('cardElement') cardElem ;


	constructor(private navCtrl: NavController,
		private modalCtrl: ModalController,
		private animationService: AnimationService,
		private firebaseProvider: FirebaseProvider,
		private fileChooser: FileChooser,
		private filePath: FilePath,
		private file: File) {
		this.loadFarmers();
		
	}

	ionViewDidEnter(){
		this.animator = this.animationService.builder();
		console.log(this.cardElem.nativeElement);
		this.animator.setType('bounceInRight').show(this.cardElem.nativeElement);
	}

	loadFarmers() {
		this.firebaseProvider.getFarmerList().valueChanges()
			.subscribe(farmerListFromDB => {
				this.farmerList = [];
				for (let i = 0; i < farmerListFromDB.length; i++) {
					let f = {} as FarmerDetails;
					f.name = farmerListFromDB[i].name;
					f.city = farmerListFromDB[i].city;
					f.state = farmerListFromDB[i].state;
					f.phoneNumber = farmerListFromDB[i].phoneNumber;
					f.landSize = farmerListFromDB[i].landSize;
					this.farmerList.push(f);
				}
				console.log('farmerlist : ', this.farmerList);
			})
	}

	async read(bstr: string) {
		const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
		let params = ["name", "city", "state", "phoneNumber", "landSize"];
		/* grab first sheet */
		const wsname: string = wb.SheetNames[0];
		const ws: XLSX.WorkSheet = wb.Sheets[wsname];
		this.data = (XLSX.utils.sheet_to_json(ws, { header: 1 }));
		let header = this.data[0];
		let is_same: boolean;

		// for (let i = 0; i < params.length; i++) {
		// 	is_same = true;
		// 	for (let j = 0; j < header.length; j++){
		// 		if(params[i] === header[j] ){
		// 			is_same = true;
		// 		}else{
		// 			is_same = false;
		// 			break;
		// 		}
		// 	}
		// 	if(!is_same){
		// 		break;
		// 	}
		// }

		is_same = params.every(function (element, index) {
			console.log("element : ", element);
			console.log("header[index] : ", header[index]);
			
			return element === header[index];
		});
		if (is_same) {
			/* save data */
			this.data = (XLSX.utils.sheet_to_json(ws, {}));
			console.log(this.data);
			await this.firebaseProvider.importFarmers(this.data);
		}
		else {
			alert("Invalid file format");
		}
	}

	importFarmerDetails() {
		console.log('Inside importFarmerDetails');
		try {
			this.fileChooser.open().then(uri => {
				this.filePath.resolveNativePath(uri).then(choosenFilePath => {
					this.file.resolveLocalFilesystemUrl(choosenFilePath).then((entry: Entry) => {
						entry.getParent((directoryEntry: Entry) => {
							this.file.readAsBinaryString(directoryEntry.nativeURL, entry.name).then(bstr => {
								console.log(entry.name);
								console.log(entry.name.split("."));
								let fileExtension = entry.name.split(".");
								if (fileExtension[fileExtension.length - 1].toLowerCase() === "xlsx" || fileExtension[1].toLowerCase() === "xls") {
									this.read(bstr);
								}
								else {
									alert("Invalid file type");
								}

							})
						})
					})

				})
			})
		}
		catch (e) {
			console.log(e);
		}

	}

	addFarmer() {
		const modal = this.modalCtrl.create('AddFarmerPage');
		modal.present();
		modal.onDidDismiss((data) => {

		});
	}

}
