import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public name:string = "";
  public num:number=null;

  constructor(public navCtrl: NavController,private socialSharing: SocialSharing) {

  }

  share(){
    let msg = this.name + ' ' + this.num;
    this.socialSharing.shareViaWhatsApp(msg,null,null);
  }

}
