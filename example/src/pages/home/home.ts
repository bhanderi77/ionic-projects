import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DataFinderProvider } from '../../providers/data-finder/data-finder';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

	brothers = [];

  constructor(public navCtrl: NavController , private dataFinder : DataFinderProvider) {

  	this.dataFinder.getJSONDataAsync("./assets/data/data.json").then(data=>{
  		this.BrotherData(data);
  		console.log('brother data : ' +data);
  	})

  }

  BrotherData(data:any){
  	this.brothers = data.Brothers;
  	console.log(this.brothers);
  }


}
