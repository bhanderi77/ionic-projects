import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
/*
  Generated class for the DataFinderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataFinderProvider {

  constructor(public http: Http) {
  }

  public getJSONDataAsync(filePath:string) :Promise<any> {

  	return new Promise((resolve,reject)=>{
  		this.http.get(filePath)
  		.subscribe(
  				res => {
  					var jsonRes = res.json();
  					resolve(jsonRes);
  					console.log('json result : ' +jsonRes);
  				}
  			)
  		console.log('file path : ' + filePath);

		}
  	);

  }

}
