import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

import { packagedItem } from '../../model/packagedItem';
import { packageRule } from '../../model/packageRule';
import { myConstants } from '../../model/myConstants';

/*
  Generated class for the InventoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InventoryProvider {

  constructor(public afs: AngularFirestore) {
    console.log('Hello InventoryProvider Provider');
  }

  getPackageRulesList():AngularFirestoreCollection<packageRule>
  {
    //return this.afs.collection<packageRule>(`/packageRulesList`);
    return this.afs.collection('/packageRulesList'); 
  } 

}
