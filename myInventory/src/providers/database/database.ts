import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http/';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map'
import { BehaviorSubject } from 'rxjs/Rx'
import { Storage } from '@ionic/storage'
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Platform } from 'ionic-angular/platform/platform';
import { packageRule } from '../../model/packageRule';
import { myConstants } from '../../model/myConstants';
import { packagedItem } from '../../model/packagedItem';


/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;

  constructor(public http: Http, private sqlitePorter: SQLitePorter, private storage: Storage, private sqlite: SQLite, private platform: Platform ) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
    	this.sqlite.create({
    		name: 'myBusiness.db',
    		location: 'default'
    	}).then((db: SQLiteObject) => {
			this.database = db;
			this.storage.set("database_filled", false);
    		this.storage.get('database_filled').then(val => {
    			if(val) {
    				this.databaseReady.next(true);
    			} else { //insert initial records in database & set database_filled to true
    				this.http.get('assets/myInventory.sql')
    									.map(res => res.text())
    									.subscribe(sql => {
    										this.sqlitePorter.importSqlToDb(this.database,sql)
    										.then(data => {
    											this.databaseReady.next(true);
    											this.storage.set('database_filled',true);	
    										})
    										.catch(e => console.log(e));
    									})
    			}
    		})
   		})
    });    
  }

  getDatabaseState() {
    return this.databaseReady.asObservable();
  }

  getPackageRules() {
    return this.database.executeSql("SELECT * FROM PackageRules",[])
      .then(data => {
        let packagRules = [];
        for(let i=0;i<data.rows.length;i++)
        {
          packagRules.push({
            id: data.rows.item(i).ID, 
            bGroupRead: data.rows.item(i).bGroupRead,
            curTaskCode: data.rows.item(i).curTaskCode,
            packageStatus: data.rows.item(i).packageStatus,
            nextTaskCode: data.rows.item(i).nextTaskCode,
            bMultipleOutput: data.rows.item(i).bMultipleOutput,
            bPartialAssign: data.rows.item(i).bPartialAssign,
            bReducedOutput: data.rows.item(i).bReducedOutput, 
            bIncursCost: data.rows.item(i).bIncursCost           
          });
        }
        return packagRules;
      });
  }

  getBtoCPackageRules(taskCode:string) {
    console.log("Welcome to DatabaseProvider:getBtoCPackageRules(taskCode) " + taskCode);
    let where=[taskCode,myConstants.packageStatus.RD]
    return this.database.executeSql("SELECT * FROM PackageRules WHERE curTaskCode=? AND packageStatus=?",where)
    .then(data => {
      let packagRules = [];
      for(let i=0;i<data.rows.length;i++)
      {
        packagRules.push({
          id: data.rows.item(i).ID, 
          bGroupRead: data.rows.item(i).bGroupRead,
          curTaskCode: data.rows.item(i).curTaskCode,
          packageStatus: data.rows.item(i).packageStatus,
          nextTaskCode: data.rows.item(i).nextTaskCode,
          bMultipleOutput: data.rows.item(i).bMultipleOutput,
          bPartialAssign: data.rows.item(i).bPartialAssign,
          bReducedOutput: data.rows.item(i).bReducedOutput, 
          bIncursCost: data.rows.item(i).bIncursCost        
        });
      }
      return packagRules;
    })
    .catch(e=>{
      console.log("getBtoCPackageRules : Error in SELECT * FROM PackageRules");
      return [];
    });
  }

  getAllTaskCode() {
    return this.database.executeSql("SELECT distinct nextTaskCode FROM PackageRules",[])
      .then(data => {
        let taskCodeList = [];
        for(let i=0;i<data.rows.length;i++)
        {
          taskCodeList.push({
            taskCode: data.rows.item(i).nextTaskCode,            
          });
        }
        return taskCodeList;
      }).catch(e=>{
        console.log("getAllTaskCode : Error in SELECT distinct nextTaskCode FROM PackageRules");
        return [];
      });
  }

  getPackagedItemsForPackageRule(rule:packageRule)
  {
    console.log("Inside DatabaseProvider: getPackagedItemsForPackageRule(packageRule) for " + rule.curTaskCode + ' ' + rule.packageStatus + ' ' + rule.nextTaskCode);
    let where = [rule.curTaskCode,rule.packageStatus,rule.nextTaskCode];
    return this.database.executeSql("SELECT * FROM PackagedItems WHERE packageCurTaskCode=? AND packageStatus=? AND packageNextTaskCode=?",where)
      .then(data => {
        let packagedItems = [];
        for(let i=0;i<data.rows.length;i++)
        {          
          packagedItems.push({
            id: data.rows.item(i).ID, 
            packageID: data.rows.item(i).packageID,
            parentPackageID: data.rows.item(i).parentPackageID,
            packageGroupID: data.rows.item(i).packageGroupID,
            packageCurTaskCode: data.rows.item(i).packageCurTaskCode,
            packageCurTaskOwner: data.rows.item(i).packageCurTaskOwner,
            packageGroupRead: data.rows.item(i).packageGroupRead,
            packageStatus: data.rows.item(i).packageStatus,
            packageNextTaskCode: data.rows.item(i).packageNextTaskCode,
            packageNextTaskOwner: data.rows.item(i).packageNextTaskOwner,
            packageMultipleOutput: data.rows.item(i).packageMultipleOutput,
            packagePartialAssignment: data.rows.item(i).packagePartialAssignment,
            packageReducedOutput: data.rows.item(i).packageReducedOutput,
            packageIncursCost: data.rows.item(i).packageIncursCost,
            itemRemarks: data.rows.item(i).itemRemarks,
            itemQty: data.rows.item(i).itemQty,
            itemWeight: data.rows.item(i).itemWeight,
            itemCost: data.rows.item(i).itemCost,
            itemTax: data.rows.item(i).itemTax                  
          });
        }
        return packagedItems;
      })
      .catch(e=>{
        console.log("getPackagedItemsForPackageRule : Error in SELECT * FROM PackagedItems");
        return [];
      });
  }

  getPackagedItemsForTaskCode(taskCode:string)
  {
    console.log("Inside DatabaseProvider: getPackagedItemsForTaskCode(taskCode) for " + taskCode);
    let where = [taskCode,taskCode];
    return this.database.executeSql("SELECT * FROM PackagedItems WHERE packageCurTaskCode=? OR packageNextTaskCode=? ORDER BY date_created ASC",where)
      .then(data => {
        let packagedItems = [];
        for(let i=0;i<data.rows.length;i++)
        {
          console.log("data.rows.item(i).packageCurGrouped" + data.rows.item(i).packageCurGrouped);
          console.log("data.rows.item(i).packageNextGrouped" + data.rows.item(i).packageNextGrouped);
          
          packagedItems.push({
            id: data.rows.item(i).ID, 
            packageID: data.rows.item(i).packageID,
            parentPackageID: data.rows.item(i).parentPackageID,
            packageGroupID: data.rows.item(i).packageGroupID,
            packageCurTaskCode: data.rows.item(i).packageCurTaskCode,
            packageCurTaskOwner: data.rows.item(i).packageCurTaskOwner,
            packageGroupRead: data.rows.item(i).packageGroupRead,
            packageStatus: data.rows.item(i).packageStatus,
            packageNextTaskCode: data.rows.item(i).packageNextTaskCode,
            packageNextTaskOwner: data.rows.item(i).packageNextTaskOwner,
            packageMultipleOutput: data.rows.item(i).packageMultipleOutput,
            packagePartialAssignment: data.rows.item(i).packagePartialAssignment,
            packageReducedOutput: data.rows.item(i).packageReducedOutput,
            packageIncursCost: data.rows.item(i).packageIncursCost,
            itemRemarks: data.rows.item(i).itemRemarks,
            itemQty: data.rows.item(i).itemQty,
            itemWeight: data.rows.item(i).itemWeight,
            itemCost: data.rows.item(i).itemCost,
            itemTax: data.rows.item(i).itemTax                  
          });
        }
        return packagedItems;
      })
      .catch(e=>{
        console.log("getPackagedItemsForTaskCode : Error in SELECT * FROM PackagedItems");
        return [];
      });
  }

  getAllPackagedItems()
  {
    //console.log("Inside DatabaseProvider: getAllPackagedItems() for ");
    //let where = [taskCode,taskCode];
    return this.database.executeSql("SELECT * FROM PackagedItems",[])
      .then(data => {
        let packagedItems = [];
        for(let i=0;i<data.rows.length;i++)
        {
          packagedItems.push({
            id: data.rows.item(i).ID, 
            packageID: data.rows.item(i).packageID,
            parentPackageID: data.rows.item(i).parentPackageID,
            packageGroupID: data.rows.item(i).packageGroupID,
            packageCurTaskCode: data.rows.item(i).packageCurTaskCode,
            packageCurTaskOwner: data.rows.item(i).packageCurTaskOwner,
            packageGroupRead: data.rows.item(i).packageGroupRead,
            packageStatus: data.rows.item(i).packageStatus,
            packageNextTaskCode: data.rows.item(i).packageNextTaskCode,
            packageNextTaskOwner: data.rows.item(i).packageNextTaskOwner,
            packageMultipleOutput: data.rows.item(i).packageMultipleOutput,
            packagePartialAssignment: data.rows.item(i).packagePartialAssignment,
            packageReducedOutput: data.rows.item(i).packageReducedOutput,
            packageIncursCost: data.rows.item(i).packageIncursCost,
            itemRemarks: data.rows.item(i).itemRemarks,
            itemQty: data.rows.item(i).itemQty,
            itemWeight: data.rows.item(i).itemWeight,
            itemCost: data.rows.item(i).itemCost,
            itemTax: data.rows.item(i).itemTax                  
          });
        }
        return packagedItems;
      })
      .catch(e=>{
        console.log("getPackagedItemsForTaskCode : Error in SELECT * FROM PackagedItems");
        return [];
      });
  }

  addPackagedItem(pItem:packagedItem):Promise<any>
  {
    console.log("Welcome to DatabaseProvider:addPackagedItem(pItem) ");
    console.log("pItem:Basic " + pItem.packageID + "," + pItem.parentPackageID + "," + pItem.packageGroupID);
    console.log("pItem:Rule " + pItem.packageCurTaskCode + "," + pItem.packageGroupRead + "," + pItem.packageStatus + "," + pItem.packageNextTaskCode + "," + pItem.packageMultipleOutput);
    console.log("pItem:Details " + pItem.itemQty + "," + pItem.itemWeight + "," + pItem.itemCost + "," + pItem.itemTax);
    
    let values=[pItem.packageID,pItem.parentPackageID,pItem.packageGroupID,pItem.packageCurTaskCode,pItem.packageCurTaskOwner,pItem.packageGroupRead,pItem.packageStatus,pItem.packageNextTaskCode,pItem.packageNextTaskOwner,pItem.packageMultipleOutput,pItem.packagePartialAssignment,pItem.packageReducedOutput,pItem.packageIncursCost,pItem.itemRemarks,pItem.itemQty,pItem.itemWeight,pItem.itemCost,pItem.itemTax];
    
    return this.database.executeSql("INSERT INTO PackagedItems(packageID,parentPackageID,packageGroupID,packageCurTaskCode,packageCurTaskOwner,packageGroupRead,packageStatus,packageNextTaskCode,packageNextTaskOwner,packageMultipleOutput,packagePartialAssignment,packageReducedOutput,packageIncursCost,itemRemarks,itemQty,itemWeight,itemCost,itemTax) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
    .then(res=>{
      console.log("INSERT INTO PackagedItems was SUCCESSFULL");
      return res;
    })
    .catch(e=> {
      console.log("Error from executeSql: INSERT INTO PackagedItems" + e);
    });
  }

  addPackagedItemArray(pItem:Array<packagedItem>):Promise<any>
  {
    console.log("Welcome to DatabaseProvider:addPackagedItem(pItem) " + pItem.length);
    /*
    console.log("pItem:Basic " + pItem.packageID + "," + pItem.parentPackageID + "," + pItem.packageGroupID);
    console.log("pItem:Rule " + pItem.packageCurTaskCode + "," + pItem.packageCurGrouped + "," + pItem.packageStatus + "," + pItem.packageNextTaskCode + "," + pItem.packageNextGrouped);
    console.log("pItem:Details " + pItem.itemQty + "," + pItem.itemWeight + "," + pItem.itemCost + "," + pItem.itemTax);
    */
    const insertStmts: string[] = pItem.map(p=> `INSERT INTO PackagedItems(packageID,parentPackageID,packageGroupID,packageCurTaskCode,packageCurTaskOwner,packageGroupRead,packageStatus,packageNextTaskCode,packageNextTaskOwner,packageMultipleOutput,packagePartialAssignment,packageReducedOutput,packageIncursCost,itemRemarks,itemQty,itemWeight,itemCost,itemTax) VALUES('${p.packageID}',${p.parentPackageID},'${p.packageGroupID}','${p.packageCurTaskCode}','${p.packageCurTaskOwner}',${p.packageGroupRead},'${p.packageStatus}','${p.packageNextTaskCode}','${p.packageNextTaskOwner}',${p.packageMultipleOutput},${p.packagePartialAssignment},${p.packageReducedOutput},${p.packageIncursCost},'${p.itemRemarks}',${p.itemQty},${p.itemWeight},${p.itemCost},${p.itemTax})`);
    console.log("insertStmts :" + insertStmts);
    return this.database.sqlBatch(insertStmts)
    .then(res=>{
      console.log("INSERT INTO PackagedItems was SUCCESSFULL");
      return res;
    })
    .catch(e=> {
      console.log("Error from executeSql: INSERT INTO PackagedItems" + e);
    });
  }

  updatePackagedItemStatus(pItemID:number,status:myConstants.packageStatus):Promise<any>
  {
    console.log("Welcome to DatabaseProvider:updatePackagedItemToCO(pItemID, status) ");
    console.log("pItemID " + pItemID + "status " + status);
    
    let values=[status,pItemID];
    
    return this.database.executeSql("UPDATE PackagedItems SET packageStatus=?,last_updated=current_timestamp WHERE ID=?",values)
    .then(res=>{
      console.log("UPDATE PackagedItems was SUCCESSFULL");
      return res;
    })
    .catch(e=> {
      console.log("Error from executeSql: UPDATE PackagedItems SET packageStatus=?" + e);
    });
  }

  updatePackagedItemList(pItem:Array<packagedItem>,group:string,status:myConstants.packageStatus):Promise<any>
  {

    console.log("Welcome to DatabaseProvider:addPackagedItem(pItem) " + pItem.length);
    const updateStmts: string[] = pItem.map(p=> `UPDATE PackagedItems SET packageGroupID='${group}', packageStatus='${status}',last_updated=current_timestamp WHERE ID=${p.id}`);
    console.log("updateStmts :" + updateStmts);
    return this.database.sqlBatch(updateStmts)
    .then(res=>{
      console.log("UPDATE PackagedItems SET packageGroupID, was SUCCESSFULL");
      return res;
    })
    .catch(e=> {
      console.log("Error from sqlBatch: UPDATE PackagedItems SET packageGroupID" + e);
    });
  }
}

