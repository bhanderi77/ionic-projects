import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DatabaseProvider } from '../providers/database/database';

import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { SQLite } from '@ionic-native/sqlite';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { AngularFireModule } from 'angularfire2';
//import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
//import { firebaseConfig } from './firebaseCredential';

import { InventoryProvider } from '../providers/inventory/inventory';

const firebaseConfig = {
  apiKey: "AIzaSyALMBiqliM2Q85Pt32m4F9pisDrMg-I8jA",
  authDomain: "mybusiness-5bb16.firebaseapp.com",
  databaseURL: "https://mybusiness-5bb16.firebaseio.com",
  projectId: "mybusiness-5bb16",
  storageBucket: "mybusiness-5bb16.appspot.com",
  messagingSenderId: "585432762445"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpModule,   
    ChartsModule, 
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),  
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider,
    SQLitePorter,
    SQLite,
    File,
    FileOpener,
    InventoryProvider
  ]
})
export class AppModule {}
