import { myConstants } from '../model/myConstants';
import { packageRule } from '../model/packageRule';

export interface packagedItem {
    id:number;
    packageID:string;
    parentPackageID:number;
    packageGroupID:string;
    packageCurTaskCode: string;
    packageCurTaskOwner: string;
    packageGroupRead: boolean;
    packageStatus: myConstants.packageStatus;    
    packageNextTaskCode: string;
    packageNextTaskOwner: string;
    packageMultipleOutput: boolean;
    packagePartialAssignment: boolean; 
    packageReducedOutput: boolean; 
    packageIncursCost: boolean;  
    //pRule:packageRule;   
    itemRemarks: string;
    itemQty: number;
    itemWeight: number;
    itemCost: number;
    itemTax: number;
}