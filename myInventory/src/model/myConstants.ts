export namespace myConstants {
    export const enum packageStatus {
        RD = 'RD',
        PD = 'PD',
        IP = 'IP',
        CO = 'CO'
      }; 
    export const enum taskCode {
        BUY = 'BUY',
        ROUGH = 'ROUGH',
        MFG_ISSUE = 'MFG ISSUE',
        MFG_RETURN = 'MFG RETURN',
        SALES = 'SALES',
        NONE = 'NONE'
    };
    export const TRUE=1;
    export const FALSE=0;
    export const TASK_OUTPUT_MAX_REC=1;
}