export interface summaryDetails {
    totalQty: number;
    totalWeight: number;
    totalCost: number;
    totalTax: number;
}
export interface taskSummary {
    taskCode: string;
    Input:summaryDetails;
    InProgress:summaryDetails;
    Completed:summaryDetails;
    Output:summaryDetails;
    Pending:summaryDetails;
}