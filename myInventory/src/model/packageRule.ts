import { myConstants } from '../model/myConstants';

export interface packageRule {
    id:string;
    curTaskCode: string;
    packageStatus: myConstants.packageStatus;    
    nextTaskCode: string;
    bGroupRead: boolean;    
    bMultipleOutput: boolean;
    bPartialAssign: boolean;
    bReducedOutput:boolean; 
    bIncursCost:boolean;   
}
