export interface taskOutput{
	weight:number;
	qty:number;
	remark:string;
	validWeight:boolean;
	validQty:boolean;
}