import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ItemSliding } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { packagedItem } from '../../model/packagedItem';
import { packageRule } from '../../model/packageRule';
import { myConstants } from '../../model/myConstants';
import { taskOutput } from '../../model/taskOutput';
/**
 * Generated class for the TaskOutputPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task-output',
  templateUrl: 'task-output.html',
})
export class TaskOutputPage {

  bValidOutput:boolean;
  private totalOutputWeightFlag:boolean;
  private totalOutputQtyFlag:boolean;
  private outputWeightFlag:boolean;
  private outputQtyFlag:boolean;

  private totalOutputQty:number;
  private pItem:packagedItem;
  private output:Array<taskOutput>;
  private maxInput:number;
  private inputWeight:number;
  private totalOutputWeight:number;
  constructor(private navCtrl: NavController, 
    private databaseProvider: DatabaseProvider,
    private navParams: NavParams ) {
      this.pItem = navParams.get("pItem");     
      // this.outputWeight = new Array(myConstants.TASK_OUTPUT_MAX_REC);
      // this.outputWeight.length=1;
      this.totalOutputWeightFlag=true;
      this.totalOutputQtyFlag=true;
      this.outputQtyFlag=true;
      this.outputWeightFlag=true;

      this.bValidOutput=false;
      this.inputWeight=this.pItem.itemWeight;
      this.output=[];
      this.totalOutputWeight=0;
      this.totalOutputQty=0;
      this.initializeData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskOutputPage');
  }

  TrackBy(index: number, obj: any):any
  {
    console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  initializeData(){
    let o= {} as taskOutput;
    o.weight=null;
    o.qty=null;
    o.remark=this.pItem.itemRemarks;
    o.validWeight=true;
    o.validQty=true;
    this.output.push(o);
  }

  deleteInputRow(o:taskOutput,item: ItemSliding){
    this.output.splice(this.output.indexOf(o),1);
  }


  addInputRow(){
    let o= {} as taskOutput;
    o.weight=null;
    o.qty=null;
    o.remark="";
    o.validWeight=true;
    o.validQty=true;
    this.output.push(o);
    this.bValidOutput=false;
  }

  validateWeight(o:taskOutput){
    this.totalOutputWeight=0;
    if((o.weight > this.inputWeight) || (o.weight) < 0){
      o.validWeight=false;
      this.outputWeightFlag = false;
    }
    else{
      o.validWeight=true;
      this.outputWeightFlag = true;
    }
    for(let i = 0 ; i < this.output.length ;i++){
      this.totalOutputWeight += +(this.output[i].weight);
    }
    if(this.totalOutputWeight <= this.inputWeight){
      this.totalOutputWeightFlag = true;
    }
    else{
      this.totalOutputWeightFlag = false;
    }
    this.validateOutput();
  }

  validateQty(o:taskOutput){
    this.totalOutputQty=0;
    if(o.qty <= 0){
      o.validQty = false;
      this.outputQtyFlag = false;
    }
    else{
      o.validQty = true;
      this.outputQtyFlag = true;
    }
    for(let i = 0 ; i < this.output.length ;i++){
      this.totalOutputQty += +(this.output[i].qty);
    }
    if(this.totalOutputQty < this.pItem.itemQty){
      this.totalOutputWeightFlag = false;
    }
    else{
      this.totalOutputWeightFlag = true;
    }
    this.validateOutput();
  }


  validateOutput(){
    if( this.totalOutputWeightFlag === true && this.outputWeightFlag === true && this.totalOutputQtyFlag === true && this.outputQtyFlag === true ){
      this.bValidOutput = true;
    }
    else{
      this.bValidOutput = false;
    }
  }


  saveOutput()
  {
    /*CREATE NEW ENTRY B->C in RD status and update B->B to CO status
    (01) Create new object of packagedItem
    (02) Set item Details and packageID related fields
    (03) Find out applicable package rule
    (03.1) Set package rule
    (03.2) set owner details
    (04) add new entry to DB 
    (05) Change status to CO for selPackagedItem
    (06) refresh data for this page
    */
   console.log("TaskSummaryPage: Welcome to saveOutput() ");
   
   let bMultipleOutput:boolean=false;
   //let totalOutput:number=0;
   // totalOutput = this.output[0].weight;
   for(let i=1;(i<this.output.length && this.output[i].weight>0);i++)
      {
        bMultipleOutput=true;
        // totalOutput += +(this.output[i].weight);    
      }
  /*
  if((this.pItem.packageMultipleOutput) && (!bMultipleOutput))
  {
    alert("Enter multiple outputs");
    return;
  }  
  else */
  if((!this.pItem.packageMultipleOutput) && (bMultipleOutput))
  {
    alert("Enter single output");
    return;
  }
  

  if((this.totalOutputWeight === this.pItem.itemWeight) && (this.pItem.packageReducedOutput))
  {
    alert("Total Output can not be same as input");
    return;
  }
   console.log("bMultipleOutput: " + bMultipleOutput);  
   console.log("totalOutput: " + this.totalOutputWeight); 
   
   //(03) Find out applicable package rule
   let packageRuleListBtoC:Array<packageRule> = [];
   return this.databaseProvider.getBtoCPackageRules(this.pItem.packageNextTaskCode)
   .then(data => 
   {
     //console.log("Await completed for this.databaseProvider.getBtoCPackageRules")
     packageRuleListBtoC = data;
    if(packageRuleListBtoC.length === 1)
    {
      let packagedItemBtoCList:Array<packagedItem> = [];
      console.log("this.outputWeight.length " + this.output.length);
      
      for(let i=0;(i<this.output.length && (this.output[i].weight)>0 && (this.output[i].qty)>0);i++)
      {
        console.log("this.outputWeight[" + i + "]: " + (this.output[i].weight));
        let packagedItemBtoC = {} as packagedItem;
        
        packagedItemBtoC.itemRemarks ="";
        //(02) Set item Details and packageID related fields                            
        packagedItemBtoC.itemQty = (this.output[i].weight);
        packagedItemBtoC.itemWeight = (this.output[i].weight);
        
        //TBC
        if(bMultipleOutput)
        {
          packagedItemBtoC.itemCost = ((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.itemCost));
          packagedItemBtoC.itemTax = ((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.itemTax));
        }
        else
        {
          packagedItemBtoC.itemCost = (this.pItem.itemCost);
          packagedItemBtoC.itemTax = (this.pItem.itemTax);
        }  
        if(this.pItem.packageIncursCost)
        {
          packagedItemBtoC.itemCost += 200;
          packagedItemBtoC.itemTax += 5;
        }
        
        if((!bMultipleOutput) && (this.totalOutputWeight === this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "=" + packagedItemBtoC.itemWeight;        
        }
        else if((!bMultipleOutput) && (this.totalOutputWeight !== this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "-" + packagedItemBtoC.itemWeight;        
        }
        else if((bMultipleOutput) && (this.totalOutputWeight === this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "<=" + packagedItemBtoC.itemWeight;        
        }
        else if((bMultipleOutput) && (this.totalOutputWeight !== this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "<-" + packagedItemBtoC.itemWeight;        
        }
        if(bMultipleOutput)
        {
          packagedItemBtoC.packageGroupID = this.pItem.packageID;
        }
        else
        {
          packagedItemBtoC.packageGroupID = this.pItem.packageGroupID;
        }
        
        packagedItemBtoC.parentPackageID = this.pItem.parentPackageID;
        
        /*
        if(this.pItem.packagePartialAssignment)
        {
          packagedItemBtoC.parentPackageID = this.pItem.id;
        }
        else
        {
          packagedItemBtoC.parentPackageID = this.pItem.parentPackageID;
        }
        */
        
        //(03.1) Set package rule       
        packagedItemBtoC.packageGroupRead = packageRuleListBtoC[0].bGroupRead;
        packagedItemBtoC.packageCurTaskCode = packageRuleListBtoC[0].curTaskCode;
        packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
        packagedItemBtoC.packageNextTaskCode = packageRuleListBtoC[0].nextTaskCode;
        packagedItemBtoC.packageMultipleOutput = packageRuleListBtoC[0].bMultipleOutput;
        packagedItemBtoC.packagePartialAssignment = packageRuleListBtoC[0].bPartialAssign;
        packagedItemBtoC.packageReducedOutput = packageRuleListBtoC[0].bReducedOutput;
        packagedItemBtoC.packageIncursCost = packageRuleListBtoC[0].bIncursCost;

        //(03.2) set owner details                              
        packagedItemBtoC.packageCurTaskOwner = this.pItem.packageNextTaskOwner;
        packagedItemBtoC.packageNextTaskOwner = this.pItem.packageNextTaskOwner;
        
        packagedItemBtoCList.push(packagedItemBtoC);       
      }
       //(04) add new entry B->C with RD status to DB
      return this.databaseProvider.addPackagedItemArray(packagedItemBtoCList)
      .then(res=>{
        console.log("B->C added");
        return this.databaseProvider.updatePackagedItemStatus(this.pItem.id,myConstants.packageStatus.CO)
        .then(res=>{
          console.log("B->B updated to CO"); 
          this.navCtrl.pop();          
        })
        .catch(e=>{
          console.log("Error in this.databaseProvider.updatePackagedItemStatus()" + e);
        });
      }).catch(e=>{
        console.log("Error in this.databaseProvider.addPackagedItem(packagedItemBtoC)" + e);
      });
    }//end of if(packageRuleListBtoC.length === 1)
    else if(packageRuleListBtoC.length === 0)
    {
      alert("TaskOutputPage:No rule exists to save output");
    }
    else if(packageRuleListBtoC.length > 1)
    {
      alert("TaskOutputPage:Multiple rule exists to save output");
    }
   })
   .catch(e=>{
     console.log("Error in this.databaseProvider.getBtoCPackageRules()" + e);
   });
  }
}
