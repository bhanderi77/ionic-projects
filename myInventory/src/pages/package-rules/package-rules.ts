import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { packageRule } from '../../model/packageRule';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the PackageRulesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-package-rules',
  templateUrl: 'package-rules.html',
})
export class PackageRulesPage {
  //private packagRuleList:Observable<packageRule[]>;
  private packagRuleList:Array<packageRule>;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private alertCtrl: AlertController,
    private databaseProvider: DatabaseProvider, 
    private inventoryProvider: InventoryProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackageRulesPage');
    //this.packagRuleList = this.inventoryProvider.getPackageRulesList().valueChanges();
    this.initilizeData();
    
  }
  ionViewDidEnter() {
    console.log('ionViewDidEnter PackageRulesPage');    
  }

  initilizeData()
  {
    this.packagRuleList = [];
    this.databaseProvider.getPackageRules()
    .then(data => {
      this.packagRuleList = data;    

      this.packagRuleList.forEach(rule => {
          console.log("ID: " + rule.id + "\n"
          + "curGrouped: " + rule.bGroupRead + "\n"
          + "curTaskCode: " + rule.curTaskCode + "\n"
          + "packageStatus: " + rule.packageStatus + "\n"
          + "bMultipleOutput: " + rule.bMultipleOutput + "\n"
          + "bPartialAssign: " + rule.bPartialAssign + "\n"
          + "bReducedOutput: " + rule.bReducedOutput + "\n"
          + "bIncursCost: " + rule.bIncursCost + "\n") ;
      });      
    }); 
  }


  /*
  addRule()
  {
    const prompt: Alert = this.alertCtrl.create({
      message: 'How many are you adding?',
      inputs: [
        { name: 'CurTaskCode',type: 'string' },
        { name: 'packageStatus',type: 'string' },
        { name: 'NextTaskCode',type: 'string' },
        { name: 'bGroupRead',type: 'boolean' },
        { name: 'bMultipleOutput',type: 'boolean' },
        { name: 'bPartialAssign',type: 'boolean' },
        { name: 'bReducedOutput',type: 'boolean' },
        { name: 'bIncursCost',type: 'boolean' },        
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          },
        },
        {
          text: 'Add',
          handler: data => {
            let p = {} as packageRule;
            p.curTaskCode = data.curTaskCode;
            p.packageStatus = data.packageStatus;
            p.curTaskCode = data.CurTaskCode;
            p.curTaskCode = data.CurTaskCode;
            p.curTaskCode = data.CurTaskCode;
            p.curTaskCode = data.CurTaskCode;
            p.curTaskCode = data.CurTaskCode;
            this.inventoryProvider.createPackageRule(p).then();
          },
        },
      ],
    });
    prompt.present();
  }
  */
}
