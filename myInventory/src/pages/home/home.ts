import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { PackageRulesPage } from '../package-rules/package-rules';
import { taskSummary,summaryDetails } from '../../model/taskSummary';
import { packagedItem } from '../../model/packagedItem';
import { myConstants } from '../../model/myConstants';
import { Summary } from '@angular/compiler';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // public InputBarChartOptions:any = {
  //   scaleShowVerticalLines: false,
  //   responsive: true,
  //   scales: {
  //           xAxes: [{stacked: true}],
  //           yAxes: [{stacked: true}]
  //   }
  // };
  public chartLegend:boolean=false;
  //Chart Labels
  // public InputBarChartLabels:string[] = ['Input'];
  // public InputBarChartType:string = 'horizontalBar';
  // public InputBarChartLegend:boolean=true;
 
  // //Chart data
  // public InputBarChartData:any[] = [
  //   {data: [66], label: 'Pending'},
  //   {data: [29], label: 'In progress'}
  // ];

  // public OutputBarChartOptions:any = {
  //   scaleShowVerticalLines: false,
  //   responsive: true,
  //   scales: {
  //           xAxes: [{stacked: true}],
  //           yAxes: [{stacked: true}]
  //   }
  // };

  // //Chart Labels
  // public OutputBarChartLabels:string[] = ['Output'];
  // public OutputBarChartType:string = 'horizontalBar';
  // public OutputBarChartLegend:boolean=true;
 
  // //Chart data
  // public OutputBarChartData:any[] = [
  //   {data: [50], label: 'Completed'},
  //   {data: [45], label: 'In progress'}
  // ];

  private taskSummaryList:Array<taskSummary>;
  constructor(public navCtrl: NavController,private databaseProvider: DatabaseProvider) {
    this.taskSummaryList = [];
    /*
    this.databaseProvider.getDatabaseState().subscribe(rdy => {
      console.log("inside HomePage::constructor -> rdy " + rdy);
      if(rdy) {
        console.log("HomePage:DB is ready");          
        this.initilizeData();       
      }
    });*/
  }
  
  ionViewDidLoad() {
    console.log('HomePage::ionViewDidLoad');
  }

  ionViewDidEnter() {
    console.log('HomePage::ionViewDidEnter');
    this.databaseProvider.getDatabaseState().subscribe(rdy => {
      console.log("inside HomePage::ionViewDidEnter -> rdy " + rdy);
      if(rdy) {
        console.log("HomePage:DB is ready");          
        this.initilizeData();       
      }
    }); 
  }

  initilizeData()
  {
    this.taskSummaryList = [];
   
    this.databaseProvider.getAllTaskCode()
    .then(taskCodeList => {
      taskCodeList.forEach(t => {
        if(t.taskCode != myConstants.taskCode.NONE)
        {
        console.log("Calling getTaskSummary for " + t.taskCode)
        this.getTaskSummary(t.taskCode);
        }
      });
    });    
  }



  
  gotoPackageRules()
  {
    console.log('HomePage::gotoPackageRules() : ');
    this.navCtrl.push('PackageRulesPage');  
  }

  gotoTaskSummary(taskCode:string)
  {
    console.log('HomePage::gotoTaskSummary(taskCode) for ' + taskCode);
    this.navCtrl.push('TaskSummaryPage',{taskCode:taskCode});  
  }

  getTaskSummary(taskCode:string)
  {
    let packagedItemList:Array<packagedItem>;
    packagedItemList = [];
    let taskSummaryRec = {} as taskSummary;
    taskSummaryRec.taskCode = taskCode;
    taskSummaryRec.Input = {} as summaryDetails;
    taskSummaryRec.Input.totalQty=0;
    taskSummaryRec.Input.totalWeight=0;
    taskSummaryRec.Input.totalCost=0;
    taskSummaryRec.Input.totalTax=0;

    taskSummaryRec.InProgress = {} as summaryDetails;
    taskSummaryRec.InProgress.totalQty=0;
    taskSummaryRec.InProgress.totalWeight=0;
    taskSummaryRec.InProgress.totalCost=0;
    taskSummaryRec.InProgress.totalTax=0;

    taskSummaryRec.Completed = {} as summaryDetails;
    taskSummaryRec.Completed.totalQty=0;
    taskSummaryRec.Completed.totalWeight=0;
    taskSummaryRec.Completed.totalCost=0;
    taskSummaryRec.Completed.totalTax=0;

    taskSummaryRec.Pending = {} as summaryDetails;
    taskSummaryRec.Pending.totalQty=0;
    taskSummaryRec.Pending.totalWeight=0;
    taskSummaryRec.Pending.totalCost=0;
    taskSummaryRec.Pending.totalTax=0;

    taskSummaryRec.Output = {} as summaryDetails;
    taskSummaryRec.Output.totalQty=0;
    taskSummaryRec.Output.totalWeight=0;
    taskSummaryRec.Output.totalCost=0;
    taskSummaryRec.Output.totalTax=0;

    this.databaseProvider.getPackagedItemsForTaskCode(taskSummaryRec.taskCode)
    .then(data => {
      packagedItemList = data; 
      
      packagedItemList.forEach(p => {
          console.log("ID: " + p.id + "\n"
          + "packageID: " + p.packageID + "\n"
          + "parentPackageID: " + p.parentPackageID + "\n"
          + "packageCurTaskCode: " + p.packageCurTaskCode + "\n"
          + "packageNextTaskCode: " + p.packageNextTaskCode + "\n"
          + "itemQty: " + p.itemQty + "\n"
          + "itemWeight: " + p.itemWeight + "\n"
          ) ;

          if(p.packageNextTaskCode === taskSummaryRec.taskCode)
          {//A->B or B->B
            if(p.packageCurTaskCode !== p.packageNextTaskCode)
            {//A->B
              if( (p.packageStatus === myConstants.packageStatus.RD)
                || (p.packageStatus === myConstants.packageStatus.PD)
                || (p.packageStatus === myConstants.packageStatus.IP)
                || (p.packageStatus === myConstants.packageStatus.CO)
                )
              {//A->B and  either RD or PD or CO or IP
                taskSummaryRec.Input.totalQty += p.itemQty;
                taskSummaryRec.Input.totalWeight += p.itemWeight;
                taskSummaryRec.Input.totalCost += p.itemCost;
                taskSummaryRec.Input.totalTax += p.itemTax;

                /*
                if((p.packageStatus === myConstants.packageStatus.CO) && (!p.packagePartialAssignment))
                {//A->B, CO
                  taskSummaryRec.Completed.totalQty += p.itemQty;
                  taskSummaryRec.Completed.totalWeight += p.itemWeight;
                  taskSummaryRec.Completed.totalCost += p.itemCost;
                  taskSummaryRec.Completed.totalTax += p.itemTax;
                }
                */
              }
            }
            else if((p.packageCurTaskCode === p.packageNextTaskCode))
            {//B->B, so either IP or CO
              if(p.packageStatus === myConstants.packageStatus.IP)
              {//B->B and IP 
                taskSummaryRec.InProgress.totalQty += p.itemQty;
                taskSummaryRec.InProgress.totalWeight += p.itemWeight;
                taskSummaryRec.InProgress.totalCost += p.itemCost;
                taskSummaryRec.InProgress.totalTax += p.itemTax;
              }
              else if((p.packageStatus === myConstants.packageStatus.CO))
              {//B->B and CO
                taskSummaryRec.Completed.totalQty += p.itemQty;
                taskSummaryRec.Completed.totalWeight += p.itemWeight;
                taskSummaryRec.Completed.totalCost += p.itemCost;
                taskSummaryRec.Completed.totalTax += p.itemTax;
              }
            }
          }
          else if(p.packageCurTaskCode === taskSummaryRec.taskCode)
          {//B->C
            if( (p.packageStatus === myConstants.packageStatus.RD)
            || (p.packageStatus === myConstants.packageStatus.PD)
            || (p.packageStatus === myConstants.packageStatus.CO)
            || (p.packageStatus === myConstants.packageStatus.IP)
            )
            {
              taskSummaryRec.Output.totalQty += p.itemQty;
              taskSummaryRec.Output.totalWeight += p.itemWeight;
              taskSummaryRec.Output.totalCost += p.itemCost;
              taskSummaryRec.Output.totalTax += p.itemTax;
            }            
          }         
      });//end of loop over packagedItems
      taskSummaryRec.Pending.totalQty = taskSummaryRec.Input.totalQty - (taskSummaryRec.InProgress.totalQty + taskSummaryRec.Completed.totalQty);
      taskSummaryRec.Pending.totalWeight = taskSummaryRec.Input.totalWeight - (taskSummaryRec.InProgress.totalWeight + taskSummaryRec.Completed.totalWeight);
      taskSummaryRec.Pending.totalCost = taskSummaryRec.Input.totalCost - (taskSummaryRec.InProgress.totalCost + taskSummaryRec.Completed.totalCost);
      taskSummaryRec.Pending.totalTax = taskSummaryRec.Input.totalTax - (taskSummaryRec.InProgress.totalTax + taskSummaryRec.Completed.totalTax);

      this.taskSummaryList.push(taskSummaryRec);
    });

  }
  
}
