import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController, NavParams,AlertController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { taskSummary,summaryDetails } from '../../model/taskSummary';
import { packagedItem } from '../../model/packagedItem';
import { packageRule } from '../../model/packageRule';
import { myConstants } from '../../model/myConstants';

@IonicPage()
@Component({
  selector: 'page-task-summary',
  templateUrl: 'task-summary.html',
})
export class TaskSummaryPage {
  private taskCode:string;
  private filterValue:string;
  private allPackagedItemList:Array<packagedItem>;
  private packagedItemList:Array<packagedItem>;
  private summary:taskSummary;
  private selectedPackageIDList:Array<number>;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private navParams: NavParams,private databaseProvider: DatabaseProvider) {
    this.taskCode = navParams.get("taskCode");
    this.selectedPackageIDList = [];
    this.resetSummary();   
    this.databaseProvider.getDatabaseState().subscribe(rdy => {
      console.log("inside TaskSummaryPage::constructor -> rdy " + rdy);
      if(rdy) {
        console.log("TaskSummaryPage:DB is ready"); 
        this.filterValue = "A_B";            
        this.initializeData();             
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskSummaryPage');
  }

  TrackBy(index: number, obj: any):any
  {
    console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  filterPackagedItems(value)
  {
    console.log("Welcome to filterPackagedItems(value): " + value);
    switch(value)
    {
      case "A_B": {
        this.packagedItemList = [];
        this.packagedItemList = this.allPackagedItemList.filter(packagedItem=>((packagedItem.packageNextTaskCode !== packagedItem.packageCurTaskCode) && (packagedItem.packageNextTaskCode === this.taskCode)));
        this.selectedPackageIDList = [];
        this.selectedPackageIDList = new Array(this.packagedItemList.length);    
        break;
      }
      case "B_B_IP": {
        this.packagedItemList = [];
        this.packagedItemList = this.allPackagedItemList.filter(packagedItem=>((packagedItem.packageNextTaskCode === packagedItem.packageCurTaskCode) && (packagedItem.packageNextTaskCode === this.taskCode) && (packagedItem.packageStatus === myConstants.packageStatus.IP)));
        break;
      }
      case "B_B_CO": {
        this.packagedItemList = [];
        this.packagedItemList = this.allPackagedItemList.filter(packagedItem=>((packagedItem.packageNextTaskCode === packagedItem.packageCurTaskCode) && (packagedItem.packageNextTaskCode === this.taskCode) && (packagedItem.packageStatus === myConstants.packageStatus.CO)));
        break;
      }
      case "B_C": {
        this.packagedItemList = [];
        this.packagedItemList = this.allPackagedItemList.filter(packagedItem=>((packagedItem.packageNextTaskCode !== packagedItem.packageCurTaskCode) && (packagedItem.packageCurTaskCode === this.taskCode)));
        break;
      }
      default:
    }
    
      
  }

  resetSummary()
  {
    this.summary = {} as taskSummary;
    this.summary.taskCode = this.taskCode;
    this.summary.Input = {} as summaryDetails;
    this.summary.Input.totalQty=0;
    this.summary.Input.totalWeight=0;
    this.summary.Input.totalCost=0;
    this.summary.Input.totalTax=0;

    this.summary.InProgress = {} as summaryDetails;
    this.summary.InProgress.totalQty=0;
    this.summary.InProgress.totalWeight=0;
    this.summary.InProgress.totalCost=0;
    this.summary.InProgress.totalTax=0;

    this.summary.Completed = {} as summaryDetails;
    this.summary.Completed.totalQty=0;
    this.summary.Completed.totalWeight=0;
    this.summary.Completed.totalCost=0;
    this.summary.Completed.totalTax=0;

    this.summary.Pending = {} as summaryDetails;
    this.summary.Pending.totalQty=0;
    this.summary.Pending.totalWeight=0;
    this.summary.Pending.totalCost=0;
    this.summary.Pending.totalTax=0;

    this.summary.Output = {} as summaryDetails;
    this.summary.Output.totalQty=0;
    this.summary.Output.totalWeight=0;
    this.summary.Output.totalCost=0;
    this.summary.Output.totalTax=0;
  }

  initializeData()
  {
    this.allPackagedItemList = [];
    //let rule = {} as packageRule;    

    this.databaseProvider.getPackagedItemsForTaskCode(this.taskCode)
    .then(data => {
      this.allPackagedItemList = data; 
      
      this.allPackagedItemList.forEach(p => {
          console.log("ID: " + p.id + "\n"
          + "packageID: " + p.packageID + "\n"
          + "parentPackageID: " + p.parentPackageID + "\n"
          + "packageGroupRead: " + p.packageGroupRead + "\n"
          + "packageCurTaskCode: " + p.packageCurTaskCode + "\n"
          + "packageStatus: " + p.packageStatus + "\n"
          + "packageNextTaskCode: " + p.packageNextTaskCode + "\n"
          + "packageMultipleOutput: " + p.packageMultipleOutput + "\n"
          + "packagePartialAssignment: " + p.packagePartialAssignment + "\n"
          + "packageReducedOutput: " + p.packageReducedOutput + "\n"
          + "packageIncursCost: " + p.packageIncursCost + "\n"
          + "itemQty: " + p.itemQty + "\n"
          + "itemWeight: " + p.itemWeight + "\n"
          ) ;
      });//end of loop over packagedItems  
      this.buildSummary(); 
      this.filterPackagedItems(this.filterValue);            
    });
  }

  buildSummary()
  {
    console.log("TaskSummaryPage: Welcome to buildSummary()");
    this.resetSummary();
    this.allPackagedItemList.forEach(p => {
      
      if(p.packageNextTaskCode === this.taskCode)
      {//A->B or B->B
        if(p.packageCurTaskCode !== p.packageNextTaskCode)
        {//A->B
          if( (p.packageStatus === myConstants.packageStatus.RD)
            || (p.packageStatus === myConstants.packageStatus.PD)
            || (p.packageStatus === myConstants.packageStatus.IP)                
            || (p.packageStatus === myConstants.packageStatus.CO)
            )
          {//A->B and  either RD or PD or CO. Ignore IP as there will be B->B records
            this.summary.Input.totalQty += p.itemQty;
            this.summary.Input.totalWeight += p.itemWeight;
            this.summary.Input.totalCost += p.itemCost;
            this.summary.Input.totalTax += p.itemTax;
            /*
            if((p.packageStatus === myConstants.packageStatus.CO) && (!p.packagePartialAssignment))
            {//A->B, CO
              
              this.summary.Completed.totalQty += p.itemQty;
              this.summary.Completed.totalWeight += p.itemWeight;
              this.summary.Completed.totalCost += p.itemCost;
              this.summary.Completed.totalTax += p.itemTax;           
            }
            */
          }
        }
        else if((p.packageCurTaskCode === p.packageNextTaskCode))
        {//B->B, so either IP or CO
          if(p.packageStatus === myConstants.packageStatus.IP)
          {//B->B and IP 
            this.summary.InProgress.totalQty += p.itemQty;
            this.summary.InProgress.totalWeight += p.itemWeight;
            this.summary.InProgress.totalCost += p.itemCost;
            this.summary.InProgress.totalTax += p.itemTax;
          }
          else if((p.packageStatus === myConstants.packageStatus.CO))
          {//B->B and CO
            this.summary.Completed.totalQty += p.itemQty;
            this.summary.Completed.totalWeight += p.itemWeight;
            this.summary.Completed.totalCost += p.itemCost;
            this.summary.Completed.totalTax += p.itemTax;
          }
        }
      }
      else if(p.packageCurTaskCode === this.taskCode)
      {//B->C
        if( (p.packageStatus === myConstants.packageStatus.RD)
        || (p.packageStatus === myConstants.packageStatus.PD)
        || (p.packageStatus === myConstants.packageStatus.CO)
        || (p.packageStatus === myConstants.packageStatus.IP)
        )
        {
          this.summary.Output.totalQty += p.itemQty;
          this.summary.Output.totalWeight += p.itemWeight;
          this.summary.Output.totalCost += p.itemCost;
          this.summary.Output.totalTax += p.itemTax;
        }                    
      }
      console.log("this.summary.Input.totalWeight " + this.summary.Input.totalWeight);
      console.log("this.summary.InProgress.totalWeight " + this.summary.InProgress.totalWeight);
      console.log("this.summary.Completed.totalWeight " + this.summary.Completed.totalWeight);
      console.log("this.summary.Output.totalWeight " + this.summary.Output.totalWeight);         
    });//end of loop over packagedItems
    this.summary.Pending.totalQty = this.summary.Input.totalQty - (this.summary.InProgress.totalQty + this.summary.Completed.totalQty);
    this.summary.Pending.totalWeight = this.summary.Input.totalWeight - (this.summary.InProgress.totalWeight + this.summary.Completed.totalWeight);
    this.summary.Pending.totalCost = this.summary.Input.totalCost - (this.summary.InProgress.totalCost + this.summary.Completed.totalCost);
    this.summary.Pending.totalTax = this.summary.Input.totalTax - (this.summary.InProgress.totalTax + this.summary.Completed.totalTax);
  }

  /****************************************************************
  This method is invoked either 
  Input: A->B RD/PD entries where packageInProgressApplicable is true
  Output : B->C entries with RD status
  ****************************************************************/
  startTask(selPackagedItem:packagedItem)
  {
    console.log("Inside TaskSummaryPage::startTask()");
    console.log("Selected Item=> PackageID: " + selPackagedItem.packageID + " Status: " + selPackagedItem.packageStatus 
          + " Weight: " + selPackagedItem.itemWeight);

    if(   (selPackagedItem.packageCurTaskCode === selPackagedItem.packageNextTaskCode)
      ||  ( (selPackagedItem.packageCurTaskCode !== selPackagedItem.packageNextTaskCode)
          && !(  (selPackagedItem.packageStatus === myConstants.packageStatus.RD)
              ||(selPackagedItem.packageStatus === myConstants.packageStatus.PD))
          )
      )
    {//Invalid entries
      alert(selPackagedItem.packageCurTaskCode + " " + selPackagedItem.packageStatus + " " + selPackagedItem.packageNextTaskCode + " CAN NOT be started");
      return;
    }

    if( !(selPackagedItem.packageGroupRead || selPackagedItem.packagePartialAssignment))
    {//A->B RD for everthing to be started in one go
      return this.generateSingleBtoBForSelected(selPackagedItem);     
    }      
    else if( (!selPackagedItem.packageGroupRead) && (selPackagedItem.packagePartialAssignment))
    {//A->B RD/PD entries where entire input is not supposed to be started together i.e.Manager assign part of input to someone in team
      console.log("Valid entries i.e. A->B RD/IP");
      let pending = {} as summaryDetails;
      pending.totalQty = selPackagedItem.itemQty;
      pending.totalWeight = selPackagedItem.itemWeight;
      pending.totalCost = selPackagedItem.itemCost;
      pending.totalTax = selPackagedItem.itemTax;

      let relevantPackagedItemList:Array<packagedItem>;    
      relevantPackagedItemList = this.allPackagedItemList.filter(packagedItem=>packagedItem.parentPackageID === selPackagedItem.id);
      relevantPackagedItemList.forEach(relevantPackagedItem => {
        console.log("relevantPackagedItem: " + relevantPackagedItem.packageID
                    + " " + relevantPackagedItem.packageCurTaskCode 
                    + " " + relevantPackagedItem.packageStatus
                    + " " + relevantPackagedItem.packageNextTaskCode
                    + " " + relevantPackagedItem.itemWeight);
        if((relevantPackagedItem.packageCurTaskCode === selPackagedItem.packageNextTaskCode)
          &&(relevantPackagedItem.packageCurTaskCode === relevantPackagedItem.packageNextTaskCode))
        {//if B->B
          if((relevantPackagedItem.packageStatus === myConstants.packageStatus.RD) || (relevantPackagedItem.packageStatus === myConstants.packageStatus.IP) || (relevantPackagedItem.packageStatus === myConstants.packageStatus.CO))
          {
          pending.totalQty -= relevantPackagedItem.itemQty;
          pending.totalWeight -= relevantPackagedItem.itemWeight;
          pending.totalCost -= relevantPackagedItem.itemCost;
          pending.totalTax -= relevantPackagedItem.itemTax;
          }
        }
      });//end of loop over relevantPackagedItemList
      //alert("packagedItem Weight: " + selPackagedItem.itemWeight + " Available: " + pending.totalWeight);
      //Choose how much to complete
      let alert = this.alertCtrl.create({
        title: "Enter WEIGHT(Max " + pending.totalWeight + ") & QTY(Max " + pending.totalQty + ")",
        inputs: [ { name: 'ToBeWeight', placeholder: 'Enter Weight'},
                  { name: 'ToBeQty', placeholder: 'Enter Qty'}
                ],
        buttons:
                [ { 
                    text: 'Cancel',
                    handler : data => { console.log('Cancel clicked');}
                  },
                  { 
                    text: 'Ok',
                    handler : data => 
                    { 
                      console.log('Ok clicked : ' + data.ToBeWeight);
                      if((data.ToBeWeight > pending.totalWeight) || (data.ToBeQty > pending.totalQty ))
                      {
                        return false;
                      }
                      else
                      {
                          
                          /*CREATE NEW ENTRY B->B in IP status and updated status of A->B from RD/PD to PD/CO
                          (01) Create new object of packagedItem
                          (02) Set item Details and packageID related fields
                          (03.1) Set package rule
                          (03.2) set owner details
                          (04) add new entry to DB 
                          (05) Change status to PD/IP for selPackagedItem
                          (06) refresh data for this page
                        */
                        console.log("CREATE NEW ENTRY B->B in IP status");
                        //(01) Create new object of packagedItem
                        let packagedItemBtoB = {} as packagedItem;
                        //(02) Set item Details and packageID related fields                            
                        packagedItemBtoB.itemQty = data.ToBeQty;
                        packagedItemBtoB.itemWeight = data.ToBeWeight;
                        packagedItemBtoB.itemCost = pending.totalCost * (data.ToBeWeight/pending.totalWeight);
                        packagedItemBtoB.itemTax = pending.totalTax * (data.ToBeWeight/pending.totalWeight);
                        
                        if(packagedItemBtoB.itemWeight == pending.totalWeight)
                        {
                          packagedItemBtoB.packageID = selPackagedItem.packageID + "|";// + packagedItemBtoB.itemWeight;
                          packagedItemBtoB.parentPackageID = selPackagedItem.id;
                          packagedItemBtoB.packageGroupID = selPackagedItem.packageGroupID;
                        }
                        else
                        {
                          packagedItemBtoB.packageID = selPackagedItem.packageID + "|<" + packagedItemBtoB.itemWeight;
                          packagedItemBtoB.parentPackageID = selPackagedItem.id;
                          packagedItemBtoB.packageGroupID = selPackagedItem.packageGroupID;                         
                        }
                        

                        //(03.1) Set package rule
                        packagedItemBtoB.packageGroupRead = selPackagedItem.packageGroupRead;
                        packagedItemBtoB.packageCurTaskCode = selPackagedItem.packageNextTaskCode;
                        packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
                        packagedItemBtoB.packageNextTaskCode = selPackagedItem.packageNextTaskCode;
                        packagedItemBtoB.packageMultipleOutput = selPackagedItem.packageMultipleOutput;
                        packagedItemBtoB.packagePartialAssignment = selPackagedItem.packagePartialAssignment;
                        packagedItemBtoB.packageReducedOutput = selPackagedItem.packageReducedOutput;
                        packagedItemBtoB.packageIncursCost = selPackagedItem.packageIncursCost;

                        //(03.2) set owner details                              
                        packagedItemBtoB.packageCurTaskOwner = selPackagedItem.packageNextTaskOwner;
                        packagedItemBtoB.packageNextTaskOwner = selPackagedItem.packageNextTaskOwner;
                        //(04) add new entry B->B with IP status to DB
                        this.databaseProvider.addPackagedItem(packagedItemBtoB)
                        .then(res=>{
                          if(packagedItemBtoB.itemWeight == pending.totalWeight)
                          {//everything is started
                            //(05) Change status to CO for selPackagedItem
                            this.databaseProvider.updatePackagedItemStatus(selPackagedItem.id,myConstants.packageStatus.CO).then(res=>{
                              //(06) refresh data for this page
                              this.initializeData();
                            })
                            .catch(e => {
                              console.log("Error from this.databaseProvider.updatePackagedItemStatus() to CO : " + e);
                            });
                          }
                          else if(packagedItemBtoB.itemWeight < pending.totalWeight)
                          {//NOT everything is started
                            //(05) Change status to PD for selPackagedItem
                            this.databaseProvider.updatePackagedItemStatus(selPackagedItem.id,myConstants.packageStatus.PD).then(res=>{
                              //(06) refresh data for this page
                              this.initializeData();
                            })
                            .catch(e => {
                              console.log("Error from this.databaseProvider.updatePackagedItemStatus() to PD : " + e);
                            });
                          }
                        })
                        .catch(e=>{
                          console.log("Error from this.databaseProvider.addPackagedItem(packagedItemBtoB) : " + e);
                        });    
                      }
                  }
                }
              ]
        }); 
      alert.present();
    }
    else if(selPackagedItem.packageGroupRead)
    {
      alert("Read Multiple Entries");
    }
  }

  generateSingleBtoCForSelected(pItem:packagedItem)
  {
    console.log("Welcome to TaskSummaryPage::generateSingleBtoC(pItem) " + pItem.packageCurTaskCode + " " + pItem.packageStatus + " " + pItem.packageNextTaskCode + " " +pItem.itemWeight )
    /*CREATE NEW ENTRY B->C in RD status and updated status of A->B from RD to CO
    (01) Create new object of packagedItem
    (02) Set item Details and packageID related fields
    (03.1) Set package rule
    (03.2) set owner details
    (04) add new entry to DB 
    (05) Change status to CO for selPackagedItem
    (06) refresh data for this page
    */
    console.log("CREATE NEW ENTRY B->C in RD status");

    let packageRuleListBtoC:Array<packageRule> = [];
    return this.databaseProvider.getBtoCPackageRules(pItem.packageNextTaskCode)
    .then(data => 
    {
      //console.log("Await completed for this.databaseProvider.getBtoCPackageRules")
      packageRuleListBtoC = data;
      if(packageRuleListBtoC.length === 1)
      {     
        //(01) Create new object of packagedItem
        let packagedItemBtoC = {} as packagedItem;
        //(02) Set item Details and packageID related fields                            
        packagedItemBtoC.itemQty = pItem.itemQty;
        packagedItemBtoC.itemWeight = pItem.itemWeight;
        packagedItemBtoC.itemCost = pItem.itemCost;
        packagedItemBtoC.itemTax = pItem.itemTax;
        
        packagedItemBtoC.packageID = pItem.packageID + "/" + packagedItemBtoC.itemWeight;
        packagedItemBtoC.parentPackageID = pItem.id;
        packagedItemBtoC.packageGroupID = pItem.packageGroupID;
        

        //(03.1) Set package rule
        packagedItemBtoC.packageGroupRead = packageRuleListBtoC[0].bGroupRead;
        packagedItemBtoC.packageCurTaskCode = packageRuleListBtoC[0].curTaskCode;
        packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
        packagedItemBtoC.packageNextTaskCode = packageRuleListBtoC[0].nextTaskCode;
        packagedItemBtoC.packageMultipleOutput = packageRuleListBtoC[0].bMultipleOutput;
        packagedItemBtoC.packagePartialAssignment = packageRuleListBtoC[0].bPartialAssign;
        packagedItemBtoC.packageReducedOutput = packageRuleListBtoC[0].bReducedOutput;
        packagedItemBtoC.packageIncursCost = packageRuleListBtoC[0].bIncursCost;

        //(03.2) set owner details //TBD                             
        packagedItemBtoC.packageCurTaskOwner = pItem.packageNextTaskOwner;
        packagedItemBtoC.packageNextTaskOwner = pItem.packageNextTaskOwner;
        //(04) add new entry B->C with RD status to DB
        this.databaseProvider.addPackagedItem(packagedItemBtoC)
        .then(res=>{
            //(05) Change status to CO for selPackagedItem
            this.databaseProvider.updatePackagedItemStatus(pItem.id,myConstants.packageStatus.CO).then(res=>{
              //(06) refresh data for this page
              this.initializeData();
            })
            .catch(e => {
              console.log("Error from databaseProvider.updatePackagedItemStatus() to CO : " + e);
            });        
        })
        .catch(e=>{
          console.log("Error from databaseProvider.addPackagedItem(packagedItemBtoC) : " + e);
        });
      }
    })
    .catch(e=>{
      console.log("Error from databaseProvider.getBtoCPackageRules : " + e);
    });    
  }

  generateSingleBtoBForSelected(pItem:packagedItem)
  {
    console.log("Welcome to TaskSummaryPage::generateSingleBtoBForSelected(pItem) " + pItem.packageCurTaskCode + " " + pItem.packageStatus + " " + pItem.packageNextTaskCode + " " +pItem.itemWeight )
    /*CREATE NEW ENTRY B->B in IP status and updated status of A->B from RD to CO
    (01) Create new object of packagedItem
    (02) Set item Details and packageID related fields
    (03.1) Set package rule
    (03.2) set owner details
    (04) add new entry to DB 
    (05) Change status to CO for selPackagedItem
    (06) refresh data for this page
    */
    console.log("CREATE NEW ENTRY B->B in IP status");

    //(01) Create new object of packagedItem
    let packagedItemBtoB = {} as packagedItem;
    //(02) Set item Details and packageID related fields                            
    packagedItemBtoB.itemQty = pItem.itemQty;
    packagedItemBtoB.itemWeight = pItem.itemWeight;
    packagedItemBtoB.itemCost = pItem.itemCost;
    packagedItemBtoB.itemTax = pItem.itemTax;
    
    if(!(pItem.packagePartialAssignment || pItem.packageReducedOutput))
    {
      packagedItemBtoB.packageID = pItem.packageID;;
      packagedItemBtoB.parentPackageID = pItem.id;
      packagedItemBtoB.packageGroupID = pItem.packageGroupID; 
    }
    else
    {
      packagedItemBtoB.packageID = pItem.packageID + "|";// + packagedItemBtoB.itemWeight;
      packagedItemBtoB.parentPackageID = pItem.id;
      packagedItemBtoB.packageGroupID = pItem.packageGroupID;    
    }
    

    //(03.1) Set package rule
    packagedItemBtoB.packageGroupRead = pItem.packageGroupRead;
    packagedItemBtoB.packageCurTaskCode = pItem.packageNextTaskCode;
    packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
    packagedItemBtoB.packageNextTaskCode = pItem.packageNextTaskCode;
    packagedItemBtoB.packageMultipleOutput = pItem.packageMultipleOutput;
    packagedItemBtoB.packagePartialAssignment = pItem.packagePartialAssignment;
    packagedItemBtoB.packageReducedOutput = pItem.packageReducedOutput;
    packagedItemBtoB.packageIncursCost = pItem.packageIncursCost;

    //(03.2) set owner details //TBD                             
    packagedItemBtoB.packageCurTaskOwner = pItem.packageNextTaskOwner;
    packagedItemBtoB.packageNextTaskOwner = pItem.packageNextTaskOwner;
    //(04) add new entry B->B with IP status to DB
    return this.databaseProvider.addPackagedItem(packagedItemBtoB)
    .then(res=>{
        //(05) Change status to CO for pItem
        return this.databaseProvider.updatePackagedItemStatus(pItem.id,myConstants.packageStatus.CO).then(res=>{
          //(06) refresh data for this page
          this.initializeData();
        })
        .catch(e => {
          console.log("Error from databaseProvider.updatePackagedItemStatus() to CO : " + e);
        });        
    })
    .catch(e=>{
      console.log("Error from databaseProvider.addPackagedItem(packagedItemBtoB) : " + e);
    });        
  }


  /****************************************************************
  This method is invoked either 
  from B->B IP entries
  OR A-B entries where packageInProgressApplicable is false
  ****************************************************************/
  completeTask(selPackagedItem:packagedItem)
  {//A->B or B->B
    console.log("Inside TaskSummaryPage::completeTask()");
    console.log("Selected Item=> PackageID: " + selPackagedItem.packageID + " Status: " + selPackagedItem.packageStatus 
          + " Weight: " + selPackagedItem.itemWeight);

    if(selPackagedItem.packageStatus === myConstants.packageStatus.CO)
    {//Already in CO status
      alert("This is ALREADY completed");
      return;
    }    
    //Check if there are B->B entries with IP status or A->B entries with RD/PD/IP status
    if((selPackagedItem.packageCurTaskCode === selPackagedItem.packageNextTaskCode)
    && (selPackagedItem.packageStatus === myConstants.packageStatus.IP)
    && ((selPackagedItem.packageMultipleOutput) || (selPackagedItem.packageReducedOutput)))
    {//B->B IP 
      const modal = this.modalCtrl.create('TaskOutputPage',{pItem:selPackagedItem});
      modal.present();
      modal.onDidDismiss((data) => {
        this.initializeData();  
      });
    }
    else if((selPackagedItem.packageCurTaskCode === selPackagedItem.packageNextTaskCode)
            && (selPackagedItem.packageStatus === myConstants.packageStatus.IP)
            && (!selPackagedItem.packageMultipleOutput) 
            && (!selPackagedItem.packageReducedOutput))
    {//A->B where packagePartialAssignment=false OR B->B IP for single o/p same as original
      /*CREATE NEW ENTRY B->C in RD status and B->B in CO status
      (01) Create new object of packagedItem
      (02) Set item Details and packageID related fields
      (03) Find out applicable package rule
      (03.1) Set package rule
      (03.2) set owner details
      (04) add new entry to DB 
      (05) Change status to CO for selPackagedItem
      (06) refresh data for this page
      */
      console.log("CREATE NEW ENTRY B->C in RD status");
      //(01) Create new object of packagedItem
      let packagedItemBtoC = {} as packagedItem;
      
      //(02) Set item Details and packageID related fields                            
      packagedItemBtoC.itemQty = selPackagedItem.itemQty;
      packagedItemBtoC.itemWeight = selPackagedItem.itemWeight;
      packagedItemBtoC.itemCost = selPackagedItem.itemCost;
      packagedItemBtoC.itemTax = selPackagedItem.itemTax;
      
      packagedItemBtoC.packageID = selPackagedItem.packageID;
      packagedItemBtoC.parentPackageID = selPackagedItem.parentPackageID;
      packagedItemBtoC.packageGroupID = selPackagedItem.packageGroupID;
      
      //(03) Find out applicable package rule
      let packageRuleListBtoC:Array<packageRule> = [];
      this.databaseProvider.getBtoCPackageRules(selPackagedItem.packageNextTaskCode)
      .then(data => {
        packageRuleListBtoC = data;
        if(packageRuleListBtoC.length === 1)
        {
          //(03.1) Set package rule
          packagedItemBtoC.packageGroupRead = packageRuleListBtoC[0].bGroupRead;
          packagedItemBtoC.packageCurTaskCode = packageRuleListBtoC[0].curTaskCode;
          packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
          packagedItemBtoC.packageNextTaskCode = packageRuleListBtoC[0].nextTaskCode;
          packagedItemBtoC.packageMultipleOutput = packageRuleListBtoC[0].bMultipleOutput;
          packagedItemBtoC.packagePartialAssignment = packageRuleListBtoC[0].bPartialAssign;
          packagedItemBtoC.packageReducedOutput = packageRuleListBtoC[0].bReducedOutput;
          packagedItemBtoC.packageIncursCost = packageRuleListBtoC[0].bIncursCost;

          //(03.2) set owner details                              
          packagedItemBtoC.packageCurTaskOwner = selPackagedItem.packageNextTaskOwner;
          packagedItemBtoC.packageNextTaskOwner = selPackagedItem.packageNextTaskOwner;
          
          //(04) add new entry B->C with RD status to DB
          this.databaseProvider.addPackagedItem(packagedItemBtoC)
          .then(res=>{
            console.log("B->C added");
            this.databaseProvider.updatePackagedItemStatus(selPackagedItem.id,myConstants.packageStatus.CO).then(res=>{
              //(06) refresh data for this page
              this.initializeData();
            })
            .catch(e => {
              console.log("Error from this.databaseProvider.updatePackagedItemStatus() to CO : " + e);
            });                                                                                 
          })
          .catch(e => {
              console.log("Error from this.databaseProvider.addPackagedItem(packagedItemBtoC) : " + e);
          });                                  
        }
        else
        {
          console.log("More than one, B->C rules with RD status, exists for " + selPackagedItem.packageNextTaskCode );
        }
      })
      .catch(e=>{
        console.log("Error in databaseProvider.getBtoCPackageRules " + e);
      });     
    }    
  }

  
  submitTask(p:packagedItem)
  {
    alert("PackageID: " + p.packageID + " Status: " + p.packageStatus + " Weight: " + p.itemWeight);
  }

  mergeInputs()
  {
    console.log("Welecome TaskSummaryPage::mergeInputs() " + this.selectedPackageIDList.length);
    let totalSelected:number=0;
    let selectedPackagedItemList:Array<packagedItem>;
    selectedPackagedItemList = [];
    let packagedItemBtoB = {} as packagedItem;
    packagedItemBtoB.itemQty = 0;
    packagedItemBtoB.itemWeight = 0;
    packagedItemBtoB.itemCost = 0;
    packagedItemBtoB.itemTax = 0;

    for(let i=0;i<this.selectedPackageIDList.length;i++)
    {
      console.log("selectedPackageIDList[" + i + "]" + this.selectedPackageIDList[i]);
      if(this.selectedPackageIDList[i])
      {
        totalSelected++;
        packagedItemBtoB.itemQty += this.packagedItemList[i].itemQty;
        packagedItemBtoB.itemWeight += this.packagedItemList[i].itemWeight;
        packagedItemBtoB.itemCost += this.packagedItemList[i].itemCost;
        packagedItemBtoB.itemTax += this.packagedItemList[i].itemTax;

        packagedItemBtoB.packageGroupRead = this.packagedItemList[i].packageGroupRead;
        packagedItemBtoB.packageCurTaskCode = this.packagedItemList[i].packageNextTaskCode;
        packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
        packagedItemBtoB.packageNextTaskCode = this.packagedItemList[i].packageNextTaskCode;
        packagedItemBtoB.packageMultipleOutput = this.packagedItemList[i].packageMultipleOutput;
        packagedItemBtoB.packagePartialAssignment = this.packagedItemList[i].packagePartialAssignment;
        packagedItemBtoB.packageReducedOutput = this.packagedItemList[i].packageReducedOutput;
        packagedItemBtoB.packageIncursCost = this.packagedItemList[i].packageIncursCost;

        packagedItemBtoB.packageCurTaskOwner = this.packagedItemList[i].packageNextTaskOwner;
        packagedItemBtoB.packageNextTaskOwner = this.packagedItemList[i].packageNextTaskOwner;        

        packagedItemBtoB.parentPackageID = 0;
        packagedItemBtoB.packageGroupID = this.packagedItemList[i].packageGroupID;

        selectedPackagedItemList.push(this.packagedItemList[i]);
      }
    }
    packagedItemBtoB.packageID =  packagedItemBtoB.itemWeight + "_" + totalSelected;   

    console.log("packagedItemBtoB.packageGroupID: " + packagedItemBtoB.packageGroupID);
    this.databaseProvider.addPackagedItem(packagedItemBtoB)
    .then(res =>{
      this.databaseProvider.updatePackagedItemList(selectedPackagedItemList,packagedItemBtoB.packageGroupID,myConstants.packageStatus.CO)
      .then(res =>{
        console.log("SUCCESS!!!!");
        this.initializeData();
        this.filterValue="B_B_IP";
      })
      .catch(e => {
        console.log("Error from databaseProvider.updatePackagedItemList(...)" + e)
      });
    })
    .catch(e => {
      console.log("Error from databaseProvider.addPackagedItem(packagedItemBtoB)" + e)
    });

  }

  showTrace(pSelectedItem:packagedItem)
  {
    const modal = this.modalCtrl.create('PackageTracePage',{pItem:pSelectedItem});
    modal.present();
    /*
    modal.onDidDismiss((data) => {
      this.initializeData();  
    }); 
    */     
  }
}
