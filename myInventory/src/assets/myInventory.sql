DROP TABLE IF EXISTS PartyProfile;

DROP TABLE IF EXISTS PackagedItems;
DROP TABLE IF EXISTS PackageRules;

CREATE TABLE IF NOT EXISTS PackageRules(ID INTEGER PRIMARY KEY AUTOINCREMENT,
date_created datetime default current_timestamp,
last_updated datetime defaul NULL,
operator_id TEXT defaul NULL,
curTaskCode TEXT NOT NULL,
packageStatus TEXT NOT NULL,
nextTaskCode TEXT NOT NULL,
bGroupRead BOOLEAN NOT NULL,
bMultipleOutput BOOLEAN NOT NULL,
bPartialAssign BOOLEAN NOT NULL,
bReducedOutput BOOLEAN NOT NULL,
bIncursCost BOOLEAN NOT NULL
); 

INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'BUY','RD',0,'ROUGH',0,0,0,0
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'ROUGH','RD',0,'MFG_ISSUE',1,0,0,0
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'MFG_ISSUE','RD',0,'SARIN & LS',1,0,0,1
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'SARIN & LS','RD',0,'4P',0,0,1,1
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    '4P','RD',0,'RG',0,0,1,1
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'RG','RD',0,'PAVILLION',0,0,1,1
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'PAVILLION','RD',0,'8 PEL',0,0,1,1
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    '8 PEL','RD',0,'CROWN',0,0,1,1
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'CROWN','RD',1,'MFG_RETURN',0,0,0,0
);
INSERT INTO PackageRules(
curTaskCode,packageStatus,bGroupRead,nextTaskCode,bMultipleOutput,bPartialAssign,bReducedOutput,bIncursCost
) VALUES (
    'MFG_RETURN','RD',0,'SALES',1,0,0,0
);

CREATE TABLE IF NOT EXISTS PackagedItems(ID INTEGER PRIMARY KEY AUTOINCREMENT,
date_created datetime default current_timestamp,
last_updated datetime default NULL,
operator_id TEXT default NULL,
packageID TEXT NOT NULL,
parentPackageID INTEGER default 0,
packageGroupID TEXT defaul '0',
packageCurTaskCode TEXT NOT NULL,
packageCurTaskOwner TEXT NOT NULL,
packageGroupRead BOOLEAN NOT NULL,
packageStatus TEXT NOT NULL,
packageNextTaskCode TEXT NOT NULL,
packageNextTaskOwner TEXT NOT NULL,
packageMultipleOutput BOOLEAN NOT NULL,
packagePartialAssignment BOOLEAN NOT NULL,
packageReducedOutput BOOLEAN NOT NULL,
packageIncursCost BOOLEAN NOT NULL,
itemRemarks TEXT,
itemQty NUMERIC NOT NULL,
itemWeight NUMERIC NOT NULL,
itemCost NUMERIC,
itemTax NUMERIC
); 

INSERT INTO PackagedItems(
parentPackageID,packageGroupID,packageID,packageCurTaskCode,packageCurTaskOwner,packageGroupRead,packageStatus,packageNextTaskCode,packageNextTaskOwner,packageMultipleOutput,packagePartialAssignment,packageReducedOutput,packageIncursCost,
itemQty,itemWeight,itemCost,itemTax
)
VALUES(
0,'0','01.400','BUY','101(Ravi)',0,'RD','ROUGH','101(Ravi)',0,0,0,0,
-1,400,32000,80
);

