DROP TABLE IF EXISTS PartyProfile;

DROP TABLE IF EXISTS PackagedItems;
DROP TABLE IF EXISTS PackageRules;

CREATE TABLE IF NOT EXISTS PackageRules(ID INTEGER PRIMARY KEY AUTOINCREMENT,
date_created datetime default current_timestamp,
last_updated datetime defaul NULL,
operator_id TEXT defaul NULL,
curGrouped BOOLEAN NOT NULL,
curTaskCode TEXT NOT NULL,
packageStatus TEXT NOT NULL,
nextTaskCode TEXT NOT NULL,
nextGrouped BOOLEAN NOT NULL,
partialAssignment BOOLEAN NOT NULL,
reducedOutput BOOLEAN NOT NULL
); 

INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    0,'BUY','RD','ROUGH',0,0,0
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    0,'BUY','CO','ROUGH',0,0,0
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    0,'ROUGH','RD','ASSORT',1,0,0
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    0,'ROUGH','CO','ASSORT',1,0,0
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    0,'ASSORT','RD','MFG',1,1,1
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    0,'ASSORT','CO','MFG',1,1,1
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    1,'MFG','RD','RETURN',1,1,0
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    1,'MFG','CO','RETURN',1,1,0
);
INSERT INTO PackageRules(
curGrouped,curTaskCode,packageStatus,nextTaskCode,nextGrouped,partialAssignment,reducedOutput
) VALUES (
    0,'RETURN','RD','SALES',0,0,0
);

CREATE TABLE IF NOT EXISTS PackagedItems(ID INTEGER PRIMARY KEY AUTOINCREMENT,
date_created datetime default current_timestamp,
last_updated datetime default NULL,
operator_id TEXT default NULL,
packageID TEXT NOT NULL,
parentPackageID INTEGER default 0,
packageGroupID TEXT defaul '0',
packageCurTaskCode TEXT NOT NULL,
packageCurTaskOwner TEXT NOT NULL,
packageCurGrouped BOOLEAN NOT NULL,
packageStatus TEXT NOT NULL,
packageNextTaskCode TEXT NOT NULL,
packageNextTaskOwner TEXT NOT NULL,
packageNextGrouped BOOLEAN NOT NULL,
packagePartialAssignment BOOLEAN NOT NULL,
packageReducedOutput BOOLEAN NOT NULL,
itemRemarks TEXT,
itemQty NUMERIC NOT NULL,
itemWeight NUMERIC NOT NULL,
itemCost NUMERIC,
itemTax NUMERIC
); 

INSERT INTO PackagedItems(
parentPackageID,packageGroupID,packageID,packageCurTaskCode,packageCurTaskOwner,packageCurGrouped,packageStatus,packageNextTaskCode,packageNextTaskOwner,packageNextGrouped,packagePartialAssignment,packageReducedOutput,
itemQty,itemWeight,itemCost,itemTax
)
VALUES(
0,'0','01.400','BUY','101(Ravi)',0,'RD','ROUGH','101(Ravi)',0,0,0,
-1,400,32000,80
);