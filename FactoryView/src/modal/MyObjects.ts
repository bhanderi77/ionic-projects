/* Factory View App */
import { DateTime } from "ionic-angular/components/datetime/datetime";

export namespace Global{

    export const enum ContactType {
      BUYER = "Buyer",
      SUPPLIER = "Supplier",
      BROKER = "Broker",
      LENDER = "Lender",
      BORROWER = "Borrower",
      MANUFACTURER = "Manufacturer",
      CONTRACTOR = "Contractor"
    };
}


export interface BusinessContact {
    ID: number;
    Type: string;//Customer/Vendor/Broker/Contractor/Employee/Lenaoder
    Name: string;
    Email_1: string;
    Phone_1: string;
    ExpirationDate: Date;
    Address_Line_1: string;
    Address_Line_2: string;
    City: string;
    State:string;
    Country:string;
    Pincode: string;
    TaskCode: string;
};

export interface Task {
    Code: string;
    Category: string;
    Description: string;
    ProductivityUOM : string; //Weight or Time
}

export interface TaskAgreement {
    ID: string;
    EffectiveDate: Date;
    ExpirationDate: Date;
    TaskCode: string; //FK to Task.Code
    UOM: string; //Per Pcs or Per Day
    Rate: number;
}

export interface WorkDetailRecord {
    ExecutorID : BusinessContact; //BusinessContact.ID of worker
    TaskCode:string;
    IfBilled:boolean;
    day:number;
    month:number;
    year:number;
    StartTime: DateTime;
    EndTime: DateTime;
    InputQty: number;
    OutputQty: number;
    InputQuality: number;//E.g. Weight
    OutputQuality: number;//E.g. Weight
    BillingUOM: string;//E.g. Per Pc or Daily
    BillingQty: number; //from user
    BillingRate: number; //from user or settings
    BillingPenaltyOrReward :number; //future use 
    BillingValue: number; // = (trxQty * trxRate) + trxPenaltyOrReward
    BillingCurrency : string; //from user
    BillingCurrencyRate : number; //from settings
    Remarks: string; //from user        
}

export interface DailyExecutorSummaryRecord {
    day:number;
    month:number;
    year:number;
    Executor:BusinessContact;//worker
    BilledQty: number;
    BilledAmount: number;
    LastBilledDate: Date;
    UnBilledQty : number;
    LastUnBilledDate: Date;
    // businessCurrency : string;
    amtReceviedOrPaid: number;
    LastPaymentDate: Date;
    amtPending: number;
    // amtPenalty: number; //future use
    // amtReward: number; //future use
}

export interface DailyTotalWorkSummaryRecord {
    day:number;
    month:number;
    year:number;
    // Executor:BusinessContact;//worker
    BilledQty: number;
    BilledAmount: number;
    LastBilledDate: Date;
    UnBilledQty : number;
    LastUnBilledDate: Date;
    // businessCurrency : string;
    amtReceviedOrPaid: number;
    LastPaymentDate: Date;
    amtPending: number;
    // amtPenalty: number; //future use
    // amtReward: number; //future use
}