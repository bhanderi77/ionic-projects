import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { SQLite , SQLiteObject } from '@ionic-native/sqlite';
import { BehaviorSubject } from 'rxjs/Rx' ;
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { BusinessContact , DailyExecutorSummaryRecord , WorkDetailRecord } from '../../modal/MyObjects'



@Injectable()
export class DatabaseProvider {
  
  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;
  constructor(public http: Http , private sqlitePorter:SQLitePorter , private storage : Storage , private sqlite : SQLite , private platform : Platform) {
    console.log('Hello DatabaseProvider Provider');
    this.databaseReady = new BehaviorSubject(false);
    	this.platform.ready().then(() => {
    		// console.log('database provider constructor : creating db');
    		this.sqlite.create({
    			name: 'factoryView.db',
    			location: 'default'
    		})
    		.then((db: SQLiteObject) => {
    			this.database = db;
    			//this.storage.set("database_filled",false);
    			this.storage.get('database_filled').then(val =>{
    				if (val){
    					this.databaseReady.next(true);
    				}else{
    					this.fillDatabase();
    				}
    			})
    		})
    	});
  }

  fillDatabase(){
  	this.http.get('assets/dummyDB.sql')
		.map(res => res.text())
		.subscribe(sql => {
			// console.log('Inside fillDatabase : importing sql to db');
			this.sqlitePorter.importSqlToDb(this.database,sql)
			.then(data => {
				// console.log('Inside fillDatabase : data imported to db');
				this.databaseReady.next(true);
				this.storage.set('database_filled',true);
			})
		});
  }

  getDatabaseState(){
  	return this.databaseReady.asObservable();
  }

  addBussinessContact(contact:BusinessContact):Promise<any> {
  	let data = [contact.Type,contact.Name,contact.Email_1,contact.Phone_1,contact.Address_Line_1,contact.Address_Line_2,contact.City,contact.State,contact.Country,contact.Pincode,contact.TaskCode];
  	return new Promise((resolve,reject)=>{
		this.database.executeSql("INSERT INTO BussinessContacts(Type,Name,Email_1,Phone_1,Address_Line_1,Address_Line_2,City,State,Country,Pincode,TaskCode) VALUES (?,?,?,?,?,?,?,?,?,?,?)",data)
			.then(res => { resolve(res);})
			.catch(e=>console.log("Error in executeSql:INSERT INTO BussinessContact " + e));
	});
  }

  getBussinessContacts(type:string):Promise<any>{
  	console.log(" inside getBussinessContacts type :  " + type);
  	let data = [type];
  	console.log("data " + data[0]);
  	return this.database.executeSql("SELECT * FROM BussinessContacts",[])
   .then(data => {
		let contacts = [];
		console.log("data.rows.length : " + data.rows.length);
		// if(data.rows.length > 0 ) {
			for(let i=0;i<data.rows.length;i++) {
				contacts.push({
          ID: data.rows.item(i).ID,
          Name: data.rows.item(i).Name,
          Type: data.rows.item(i).Type,
          Email_1: data.rows.item(i).Email_1, 
          Phone_1: data.rows.item(i).Phone_1, 
          ExpirationDate: data.rows.item(i).ExpirationDate,
          Address_Line_1: data.rows.item(i).Address_Line_1,
          Address_Line_2: data.rows.item(i).Address_Line_2,
          City: data.rows.item(i).City, 
          State: data.rows.item(i).State, 
          Country: data.rows.item(i).Country,
          Pincode: data.rows.item(i).Pincode,
          TaskCode: data.rows.item(i).TaskCode
         });
			}
		  return contacts;
		
		// return [];  		
	 });
  }

  addWorkDetails(work:WorkDetailRecord){

  }

  getWorkDetailsForExecutor(executorId:number){

  }

  getInputQtyForExecutorOnDay(executorId:number , day:number , month:number , year:number){
  	let where = [executorId,day,month,year];
  	console.log("Inside getInputQtyForExecutorOnDay  ");
  	return this.database.executeSql("SELECT SUM(InputQty) as InputQty FROM WorkDetail WHERE ExecutorID = ? AND Day = ? AND Month = ? AND Year = ?",where)
  	.then(data=>{
  		console.log(" data.rows.item(0).InputQty : " + data.rows.item(0).InputQty);
  		if(data.rows.item(0).InputQty === null){
  			return 0;
  		}
  		return data.rows.item(0).InputQty;
  	})
  	.catch(e=>console.log("Error in executeSql:SELECT SUM(InputQty) " + e));
  }

  getWorkDetailsForDay(day:number,month:number,year:number){

  }

  addDailyExecutorSummary(executorSummary:DailyExecutorSummaryRecord){

  }

  updateDailyExecutorSummary(executorId:number , day:number , month:number , year:number){

  }

}
