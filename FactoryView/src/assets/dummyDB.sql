DROP TABLE IF EXISTS BussinessContacts;
DROP TABLE IF EXISTS DailyExecutorSummary;
CREATE TABLE IF NOT EXISTS BussinessContacts (ID INTEGER PRIMARY KEY AUTOINCREMENT ,
 Type TEXT , 
 Name TEXT , 
 Email_1 TEXT,
 Phone_1 NUMBER,
 ExpirationDate TEXT default NULL,
 Address_Line_1 TEXT,
 Address_Line_2 TEXT,
 City TEXT,
 State TEXT,
 Country TEXT,
 Pincode TEXT,
 TaskCode TEXT);

CREATE TABLE IF NOT EXISTS WorkDetail ( ID INTEGER PRIMARY KEY AUTOINCREMENT ,
  date_created datetime default current_timestamp,
  last_updated datetime,
  operator_id TEXT,
	ExecutorID INTEGER,
  TaskCode TEXT,
  IfBilled BOOLEAN,
  Day INTEGER,
	Month INTEGER,
	Year INTEGER,
  StartTime TEXT,
  EndTime TEXT,
  InputQty NUMBER,
  OutputQty NUMBER,
  InputQuality NUMBER,
  OutputQuality NUMBER,
  BillingUOM TEXT,
  BillingQty NUMBER, 
  BillingRate NUMBER,
  BillingPenaltyOrReward NUMBER,  
  BillingValue NUMBER, 
  BillingCurrency TEXT,
  BillingCurrencyRate NUMBER,
  Remarks TEXT
);

CREATE TABLE IF NOT EXISTS DailyExecutorSummary (ID INTEGER PRIMARY KEY AUTOINCREMENT,
  date_created datetime default current_timestamp,
  last_updated datetime,
  operator_id TEXT,
	Day INTEGER,
  Month INTEGER,
  Year INTEGER,
	ExecutorID INTEGER,
	BilledQty NUMBER,
	BilledAmount NUMBER,
	LastBilledDate TEXT,
	UnBilledQty NUMBER,
	LastUnBilledDate TEXT,
	businessCurrency TEXT,
	amtReceviedOrPaid NUMBER,
	LastPaymentDate TEXT,
	amtPending NUMBER,
	amtPenalty NUMBER, 
	amtReward NUMBER );







