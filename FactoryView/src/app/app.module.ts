import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule , NavController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { StaffPage } from '../pages/staff/staff';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TradersPage } from '../pages/traders/traders';
import { DailyWorkPage } from '../pages/daily-work/daily-work';
import { ExpensePage } from '../pages/expense/expense';
import { DatePicker } from '@ionic-native/date-picker';
import { DatePickerModule } from 'ion-datepicker';
import { DatabaseProvider } from '../providers/database/database';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { SQLite } from '@ionic-native/sqlite';
import { IonicStorageModule } from '@ionic/storage';
import { Http , HttpModule } from '@angular/http';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    StaffPage,
    TradersPage,
    ExpensePage,
    DailyWorkPage
  ],

  imports: [
    BrowserModule,
    HttpModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp),
    DatePickerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    StaffPage,
    TradersPage,
    ExpensePage,
    DailyWorkPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatePicker,
    DatabaseProvider,
    SQLitePorter,
    SQLite
  ]
})
export class AppModule {}
