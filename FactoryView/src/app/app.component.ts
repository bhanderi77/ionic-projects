import { Component , ViewChild } from '@angular/core';
import { Platform ,Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StaffPage } from '../pages/staff/staff';
import { HomePage } from '../pages/home/home';
import { TradersPage } from '../pages/traders/traders';
import { ExpensePage } from '../pages/expense/expense';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav:Nav;
  rootPage:any = HomePage;
  productOpen : boolean = true;
  serviceOpen : boolean = true;
  contactsOpen : boolean = true;
  financeOpen : boolean = true;
  departmentsOpen : boolean = true;
  transactionsOpen : boolean = true;
  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  subMenu(id:any){
    console.log('id : ' + id);
    switch (id) {
      case 1:
        this.productOpen = !this.productOpen;
        break;
      case 2:
        this.serviceOpen = !this.serviceOpen;
        break;
      case 3:
        this.contactsOpen = !this.contactsOpen;
        break;
      case 4:
        this.financeOpen = !this.financeOpen;
        break;
      case 5:
        this.departmentsOpen = !this.departmentsOpen;
        break;
      case 6:
        this.transactionsOpen = !this.transactionsOpen;
        break;  
    }
  }

  openPage(page:any){
    console.log('page : '+page);
    switch (page) {
      case "home":
        this.nav.setRoot(HomePage,{});
        break;
      case "traders":
        this.nav.push(TradersPage,{});
        break;
      case "staff" :
        this.nav.push(StaffPage,{});
        break;
      case "expense":
        this.nav.push(ExpensePage,{});
        break;
    }
  }
}

