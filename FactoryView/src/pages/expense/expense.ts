import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController , Slides , Content,Platform } from 'ionic-angular';
import { AddExpensePage } from '../add-expense/add-expense';
/**
 * Generated class for the ExpensePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-expense',
  templateUrl: 'expense.html',
})
export class ExpensePage {
  @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;
  @ViewChild('MultiItemsScrollingTabs') ItemsTitles : Content;

  SwipedTabsIndicator :any= null;
  tabTitleWidthArray :Array<number>= [];
  tabElementWidth_px :number= 70;
  screenWidth_px :number= 0;
  isRight :boolean= true;
  isLeft:boolean= true;
  tabs:any=[];
  offset:number=0;

  constructor(public navCtrl: NavController,public modalCtrl: ModalController, public navParams: NavParams ,platform: Platform) {
    this.tabs=["November-17","December-17","January-18","February-18"];
    console.log('Width: ' + platform.width());
    this.screenWidth_px=platform.width();
    this.offset = 21
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad ExpensePage');
    this.SwipedTabsIndicator = null;
    this.tabTitleWidthArray=[];
    this.tabElementWidth_px=70;
    this.isRight=true;
    this.isLeft=true;
    this.SwipedTabsIndicator = document.getElementById("indicator");
    for (let i in this.tabs){
      if(i === '2'){
        this.tabTitleWidthArray.push(document.getElementById("tabTitle"+i).offsetWidth - 2);
        console.log("tabTitleWidthArray[" + i + "] :" + this.tabTitleWidthArray[i]);
      }
      else{
        this.tabTitleWidthArray.push(document.getElementById("tabTitle"+i).offsetWidth);
                console.log("tabTitleWidthArray[" + i + "] :" + this.tabTitleWidthArray[i]);

      }

    }
    this.selectTab(3);
  }

  addExpense(){
    const modal = this.modalCtrl.create('AddExpensePage');
    modal.present();
  }

    scrollIndicatiorTab()
  {
            console.log("Entered scrollIndicatiorTab");

    this.ItemsTitles.scrollTo(this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())-this.screenWidth_px/2,0);
  }


  selectTab(index)
  {
    console.log("Entered selectTab for index : "+ index);
    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+(this.offset)+"px";
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';
    this.SwipedTabsSlider.slideTo(index);
    console.log("indicator width : " + this.SwipedTabsIndicator.style.width);
  }

  calculateDistanceToSpnd(index)
  {
    console.log("entered calculateDistanceToSpnd for index : " + index);
    var result=0;
    for (var _i = 0; _i < index; _i++) {
      if(_i === 2){
        result=result+this.tabTitleWidthArray[_i]+2;
      }
      else{
          result=result+this.tabTitleWidthArray[_i];
      }
    }
    console.log("result : "+ result);
    return result;
  }

  updateIndicatorPosition() {
        console.log("Entered updateIndicatorPosition");

    var index=this.SwipedTabsSlider.getActiveIndex();
    if( this.SwipedTabsSlider.length()==index)
      index=index-1;
    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+(this.offset)+"px";
    if(index === 3){
      this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';  

    }
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';

  }

  updateIndicatorPositionOnTouchEnd()
  {
    console.log("Entered updateIndicatorPositionOnTouchEnd");
    setTimeout( () => { this.updateIndicatorPosition(); }, 200);
  }

  animateIndicator($event)
  {
    console.log("Entered animateIndicator");

    this.isLeft=false;
    this.isRight=false;
    var currentSliderCenterProgress =(1/(this.SwipedTabsSlider.length()-1) )*this.SwipedTabsSlider.getActiveIndex();
    if($event.progress < currentSliderCenterProgress)
    {
      this.isLeft=true;
      this.isRight=false;

    } if($event.progress > currentSliderCenterProgress)
    {
      this.isLeft=false;
      this.isRight=true;
    }

    if(this.SwipedTabsSlider.isEnd())
      this.isRight=false;

    if( this.SwipedTabsSlider.isBeginning())
      this.isLeft=false;

    if(this.isRight)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()+1])
      +'px,0,0)';

    if(this.isLeft)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()-1])
      +'px,0,0)';

    if(!this.isRight && !this.isLeft)
      this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()]+"px";
  }

}
