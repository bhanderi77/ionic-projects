import { Component ,ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams , Slides , Content,Platform} from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database'


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {

  @ViewChild('SwipedTabsSlider') SwipedTabsSlider: Slides ;
  @ViewChild('MultiItemsScrollingTabs') ItemsTitles : Content;

  SwipedTabsIndicator :any= null;
  tabTitleWidthArray :Array<number>= [];
  tabElementWidth_px :number= 70;
  screenWidth_px :number= 0;
  isRight :boolean= true;
  isLeft:boolean= true;
  tabs:any=[];
  currentTab:number;
  offset:number=0;
  months:any=[];

  constructor(private databaseProvider : DatabaseProvider , public navCtrl: NavController ,platform: Platform) {

    this.months=["January","February","March","April","May","June","July","August","September","October","November","December"]

    if(platform.is('core')){
      this.tabs=[];
      this.currentTab=7;
      for(let i = 0 ; i < 8 ; i++){
        this.tabs.push(this.months[i]);
      }
    }

    if(platform.is('android'|| 'mobileweb')){
      this.tabs=[];
      this.currentTab=3;
      for(let i = 0 ; i < 4 ; i++){
        this.tabs.push(this.months[i]);
      }
    }

    if(platform.isLandscape()){
      this.tabs=[];
      this.currentTab=7;
      for(let i = 0 ; i < 8 ; i++){
        this.tabs.push(this.months[i]);
      }
    }
    //this.tabs=["November-17","December-17","January-18","February-18"];
    console.log('Width: ' + platform.width());
    console.log('Platform : ' + platform[0]);

    this.screenWidth_px=platform.width();
    this.offset = 21;

    this.databaseProvider.getDatabaseState().subscribe(rdy =>{
      if(rdy){
      
      }
    });

  }

  ionViewDidEnter() {
    this.SwipedTabsIndicator = null;
    this.tabTitleWidthArray=[];
    this.tabElementWidth_px=70;
    this.isRight=true;
    this.isLeft=true;
    this.SwipedTabsIndicator = document.getElementById("indicator");
    for (let i in this.tabs){
      if(i === '2'){
      	this.tabTitleWidthArray.push(document.getElementById("tabTitle"+i).offsetWidth - 2);
      }
      else{
      	this.tabTitleWidthArray.push(document.getElementById("tabTitle"+i).offsetWidth);
      }

    }
    this.selectTab(this.currentTab);
  }

  scrollIndicatiorTab()
  {
    this.ItemsTitles.scrollTo(this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())-this.screenWidth_px/2,0);
  }

  selectTab(index)
  {
    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+(this.offset)+"px";
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';
    this.SwipedTabsSlider.slideTo(index);
  }

  calculateDistanceToSpnd(index)
  {
    var result=0;
    for (var _i = 0; _i < index; _i++) {
    	if(_i === 2){
    		result=result+this.tabTitleWidthArray[_i]+2;
    	}
    	else{
      		result=result+this.tabTitleWidthArray[_i];
    	}
    }
    return result;
  }

  updateIndicatorPosition() {
    var index=this.SwipedTabsSlider.getActiveIndex();
    if( this.SwipedTabsSlider.length()==index)
      index=index-1;
    this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[index]+(this.offset)+"px";
  	if(index === 3){
  		this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';	
  	}
    this.SwipedTabsIndicator.style.webkitTransform = 'translate3d('+(this.calculateDistanceToSpnd(index))+'px,0,0)';

  }

  updateIndicatorPositionOnTouchEnd()
  {
    setTimeout( () => { this.updateIndicatorPosition(); }, 200);
  }

  animateIndicator($event)
  {

    this.isLeft=false;
    this.isRight=false;
    var currentSliderCenterProgress =(1/(this.SwipedTabsSlider.length()-1) )*this.SwipedTabsSlider.getActiveIndex();
    if($event.progress < currentSliderCenterProgress)
    {
      this.isLeft=true;
      this.isRight=false;

    } if($event.progress > currentSliderCenterProgress)
    {
      this.isLeft=false;
      this.isRight=true;
    }

    if(this.SwipedTabsSlider.isEnd())
      this.isRight=false;

    if( this.SwipedTabsSlider.isBeginning())
      this.isLeft=false;

    if(this.isRight)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()+1])
      +'px,0,0)';

    if(this.isLeft)
      this.SwipedTabsIndicator.style.webkitTransform =
      'translate3d('+( this.calculateDistanceToSpnd(this.SwipedTabsSlider.getActiveIndex())
        +($event.progress - currentSliderCenterProgress) *(this.SwipedTabsSlider.length()-1)*this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()-1])
      +'px,0,0)';

    if(!this.isRight && !this.isLeft)
      this.SwipedTabsIndicator.style.width = this.tabTitleWidthArray[this.SwipedTabsSlider.getActiveIndex()]+"px";
  }
}