import { Component  } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl  } from '@angular/forms';
import { min } from 'rxjs/operator/min';
import { DatePicker } from '@ionic-native/date-picker';

@IonicPage()
@Component({
  selector: 'page-add-expense',
  templateUrl: 'add-expense.html'
})
export class AddExpensePage {
  selectClass : any;
  amount:number;
  type:string;
  formGroup: any;
  inputAmount:AbstractControl;
  inputType:AbstractControl;

  constructor(private DatePicker:DatePicker,public navCtrl: NavController, public navParams: NavParams , private viewCtrl: ViewController , public formBuilder: FormBuilder ) {
  	this.amount = 0;
  	this.type="";
  	this.formGroup = formBuilder.group({
  		inputType:['',Validators.required],
        inputAmount:['',Validators.compose([Validators.min(1),Validators.required])]
    });
    this.inputAmount = this.formGroup.controls['inputAmount'];
    this.inputType = this.formGroup.controls['inputType'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddExpensePage');
  }

  saveExpense(){
    this.viewCtrl.dismiss();
  }
}
