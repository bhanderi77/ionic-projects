import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TradersPage } from './traders';

@NgModule({
  declarations: [
    TradersPage,
  ],
  imports: [
    IonicPageModule.forChild(TradersPage),
  ],
})
export class TradersPageModule {}
