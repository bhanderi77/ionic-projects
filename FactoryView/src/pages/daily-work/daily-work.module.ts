import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DailyWorkPage } from './daily-work';

@NgModule({
  declarations: [
    DailyWorkPage,
  ],
  imports: [
    IonicPageModule.forChild(DailyWorkPage),
  ],
})
export class DailyWorkPageModule {}
