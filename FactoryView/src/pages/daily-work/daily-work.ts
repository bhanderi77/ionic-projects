import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BusinessContact , DailyExecutorSummaryRecord , WorkDetailRecord , Global } from '../../modal/MyObjects'
import { DatabaseProvider } from '../../providers/database/database';

/**
 * Generated class for the DailyWorkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-daily-work',
  templateUrl: 'daily-work.html',
})
export class DailyWorkPage {

  staffList:Array<BusinessContact>;
  unBilledWorkQty: Array<number>;

  constructor(public navCtrl: NavController, public navParams: NavParams , private databaseProvider : DatabaseProvider) {
  	this.staffList=[];
    this.unBilledWorkQty=[];
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter DailyWorkPage');
    this.databaseProvider.getDatabaseState().subscribe(rdy =>{
      if(rdy){
        this.initializeData();
      }
    });
  }

  initializeData(){
    this.databaseProvider.getBussinessContacts(Global.ContactType.CONTRACTOR)
    .then(data=>{
      this.staffList=data;
      // console.log("contact data : " + this.staffList[0]);
      let today = new Date();
      for(let i = 0 ; i < this.staffList.length ; i++){
        console.log("calling getInputQtyForExecutorOnDay for " + this.staffList[i].ID + " " + this.staffList[i].Name);
        
        this.databaseProvider.getInputQtyForExecutorOnDay(this.staffList[i].ID,today.getDay(),today.getMonth()+1,today.getFullYear())
        .then(qty=>{
          
          this.unBilledWorkQty[i]=qty;
        }).catch(e=>console.log("Error in databaseProvider.getInputQtyForExecutorOnDay " + e));
      }
    }).catch(e=>console.log("Error in databaseProvider.getBussinessContacts " + e));

  }

  saveWork(){
    
  }

}
