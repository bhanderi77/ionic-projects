import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';

import { FormBuilder, FormGroup, Validators, AbstractControl  } from '@angular/forms';
import { DatabaseProvider } from '../../providers/database/database';
import { Global , BusinessContact } from '../../modal/MyObjects'
/**
 * Generated class for the HumanResourcePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-human-resource',
  templateUrl: 'human-resource.html',
})
export class HumanResourcePage {

  name:string;
  phone_1:string;
  formGroup: any;
  inputName:AbstractControl;
  inputPhone_1:AbstractControl;

  constructor( private viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams , public formBuilder: FormBuilder , private databaseProvider : DatabaseProvider) {
  	this.name="";
  	this.phone_1="";
  	this.formGroup = formBuilder.group({
  		inputName:['',Validators.required],
        inputPhone_1:['',Validators.compose([Validators.minLength(10),Validators.required,Validators.maxLength(10)])]
    });
    this.inputName = this.formGroup.controls['inputName'];
    this.inputPhone_1 = this.formGroup.controls['inputPhone_1'];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HumanResourcePage');
  }

  saveHumanResource(){
    let contact = {} as BusinessContact;
    contact.Type = Global.ContactType.CONTRACTOR;
    contact.Name = this.name;
    contact.Email_1 = "";
    contact.Phone_1 = this.phone_1;
    contact.Address_Line_1 = "";
    contact.Address_Line_2 = "";
    contact.City = "";
    contact.State = "";
    contact.Country = "";
    contact.Pincode = "";
    contact.TaskCode = "1";
  	this.databaseProvider.addBussinessContact(contact).then(res=>{
      console.log("Contact added.");
    }).catch(e=>console.log("Error in saveHumanResource  " + e));

    this.viewCtrl.dismiss();
  }


}
