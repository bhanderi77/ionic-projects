import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ModalController } from 'ionic-angular';
import { BusinessContact , DailyExecutorSummaryRecord , WorkDetailRecord , Global } from '../../modal/MyObjects'
import { DailyWorkPage } from '../daily-work/daily-work';
import { HumanResourcePage } from '../human-resource/human-resource';
import { DatabaseProvider } from '../../providers/database/database';

/**
 * Generated class for the StaffPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-staff',
  templateUrl: 'staff.html',
})
export class StaffPage {

  staffList:Array<BusinessContact>;
  dailyExecutorSummaryList:Array<DailyExecutorSummaryRecord>;
  workDetailRecordList:Array<WorkDetailRecord>;

  constructor(public navCtrl: NavController, public navParams: NavParams ,public modalCtrl: ModalController , private databaseProvider : DatabaseProvider) {
    
    this.databaseProvider.getDatabaseState().subscribe(rdy =>{
      if(rdy){
        this.initializeData();
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StaffPage');

  }

  initializeData(){
    this.databaseProvider.getBussinessContacts(Global.ContactType.CONTRACTOR).then(data=>{
            this.staffList=data;
            // console.log("contact data : " + this.staffList[0]);
    });

    this.workDetailRecordList=[];


    this.dailyExecutorSummaryList=[];
    let today = new Date();
    //this.dailyExecutorSummaryList.push({"day":today.getDay(),"month":today.getMonth()+1,"year":today.getFullYear(),"Executor":this.staffList[0],"BilledQty":300,"BilledAmount":3000,"LastBilledDate":today,"UnBilledQty":35,"LastUnBilledDate":today,"amtReceviedOrPaid":2000,"LastPaymentDate":today,"amtPending":1000});
  }

  addHumanResource(){
    const modal = this.modalCtrl.create('HumanResourcePage');
    modal.present();
  }

  addWork(){
    this.navCtrl.push(DailyWorkPage,{});
  }

}
