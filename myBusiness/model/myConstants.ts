export namespace myConstants {
    export const CurrenyRateForeignToLocal = 66;
    export const ForeignCurrency = "USD";
    export const LocalCurrency = "INR";
    export const enum bizTrxStatus {
        OPEN = "OPEN",
        CLOSED = "CLOSED"
    };
    export const  ROI: number = 1.5;
    export const enum partyCategory {
        BUYER = "Buyer",
        SUPPLIER = "Supplier",
        BROKER = "Broker",
        LENDER = "Lender",
        BORROWER = "Borrower",
        MANUFACTURER = "Manufacturer"
    };
    export const enum trxType {
        ROUGH_IMPORT = "Import_Rough",
        ROUGH_BUY = "Buy_Rough",
        ROUGH_SELL = "Sell_Rough",
        POLISH_BUY = "Buy_Polish",
        POLISH_SELL = "Sell_Polish",
        MANUFACTURING = "In_Manufacturing",
        DUE_DEBIT = "Due_Debit",
        DUE_CREDIT = "Due_Credit",
        IN_CREDIT = "In_Credit",
        OUT_DEBIT = "Out_Debit"
    };
    export const enum paymentMethod {
        CASH = "cash",
        CHEQUE = "cheq",
        COURIER = "cour"
    }
}
