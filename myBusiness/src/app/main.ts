import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
//import { IndianCurrency } from '../model/IndianCurrency.pipe'

platformBrowserDynamic().bootstrapModule(AppModule);
