import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { StatusBar } from '@ionic-native/status-bar';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { CallNumber} from '@ionic-native/call-number';
import { Network } from '@ionic-native/network';

import { DatePipe } from '@angular/common';
import { Device } from '@ionic-native/device';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { GooglePlus } from '@ionic-native/google-plus';

import { MyApp } from './app.component';
import { IonicStorageModule } from '@ionic/storage'
import { HttpModule } from '@angular/http'


import { FormsModule } from '@angular/forms'
import { CustomFormsModule } from 'ng2-validation'
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

import { TransactionProvider } from '../providers/transaction/transaction';
import { InventoryProvider } from '../providers/inventory/inventory';


import { firebaseConfig } from './firebaseCredentials';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';
import { AuthProvider } from '../providers/auth/auth';
import { SettingsProvider } from '../providers/settings/settings';
import { EntityProvider } from '../providers/entity/entity';
import { RateCardProvider } from '../providers/rate-card/rate-card';
import { MenuProvider } from '../providers/menu/menu';


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CustomFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),    
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  //  SQLitePorter,
  //  SQLite,
    File,
    FileOpener,
    CallNumber,
    Device,
    DatePipe,
    Contacts,
    Contact,
    TransactionProvider,
    InventoryProvider,
    AuthProvider,
    SettingsProvider,
    GooglePlus,
    Network,
    FingerprintAIO,
    OpenNativeSettings,
    SettingsProvider,
    EntityProvider,
    RateCardProvider,
    MenuProvider
  ]
})
export class AppModule {}
