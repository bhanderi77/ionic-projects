import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackageRulesPage } from './package-rules';

@NgModule({
  declarations: [
    PackageRulesPage,
  ],
  imports: [
    IonicPageModule.forChild(PackageRulesPage),
  ],
})
export class PackageRulesPageModule {}
