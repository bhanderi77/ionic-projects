import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { User, checkIfValid } from '../../model/user';

/**
 * Generated class for the SelectPurchaseStockPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-purchase-stock',
  templateUrl: 'select-purchase-stock.html',
})
export class SelectPurchaseStockPage {
  private user:User;
  private selectValue:string;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');  
    this.selectValue = "FOR_PLANNING"
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad SelectPurchaseStockPage');
  }

  applyFilter()
  {
    console.log("Welcome to SelectPurchaseStockPage:applyFilter() " + this.selectValue);
    this.viewCtrl.dismiss(this.selectValue); 
  }

}
