import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Platform, AlertController, ViewController } from 'ionic-angular';
import { DatePipe } from '@angular/common';
import { Network } from '@ionic-native/network';

import { SettingsProvider } from '../../providers/settings/settings';
import { TransactionProvider } from '../../providers/transaction/transaction';

import { FinanceSettings } from '../../model/referenceData';
import { PartySummary, BusinessTransactionSummary, calculateBusinessSummary, BusinessSummary } from '../../model/partySummary';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { myConstants } from '../../model/myConstants';
import { User, checkIfValid } from '../../model/user';
import { Subscription } from 'rxjs';
/**
 * Generated class for the SummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'pagePartySummary',
  templateUrl: 'partySummary.html',
})
export class PartySummaryPage {

  private pSummary: PartySummary;
  private pdfObj: any;
  private trxCurrency: string;
  private exchangeRate: number;
  private financeSettings: FinanceSettings;
  private user: User;
  private confidentialityLevel: number;
  private mySubscriptions: Subscription;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public modalCtrl: ModalController,
    public viewCtrl: ViewController,
    private transactionProvider: TransactionProvider,
    private settingsProvider: SettingsProvider,
    private platform: Platform,
    private file: File,
    private fileOpener: FileOpener,
    private alertCtrl: AlertController,
    public datepipe: DatePipe,
    public popOverCtrl: PopoverController,
    private network: Network) {
    this.pSummary = {} as PartySummary;
    this.pSummary.businessSummary = {} as BusinessSummary;
    this.pSummary.profile = navParams.get("partyProfile");

    this.user = {} as User;
    this.user = navParams.get('User');
    this.trxCurrency = myConstants.localCurrency;

    this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
  }

  ionViewDidLoad() {
    console.log('PartySummaryPage::ionViewDidLoad : PartyName : ' + this.pSummary.profile.fullName);
    if (!checkIfValid(this.user)) {
      alert("Invalid UserID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      this.viewCtrl.dismiss();
    }
  }

  ionViewWillEnter() {
    console.log("ionViewWillEnter " + this.confidentialityLevel);
    this.initializeData();
  }

  async initializeData() {
    console.log("Welcome to PartySummaryPage::initializeData() ");
    this.mySubscriptions = new Subscription();

    this.financeSettings = await this.settingsProvider.getFinanceSettings(this.user);

    this.pSummary.businessSummary.bizTrxs = [];
    this.pSummary.businessSummary.finTrxs = [];
    this.pSummary.businessSummary.bizTrxSummary = [];

    this.mySubscriptions.add(this.transactionProvider.getBusinessTransactionsForPartyID(this.user, this.pSummary.profile.id).valueChanges()
      .subscribe(businessTransactionList => {
        console.log("Subscribed to getBusinessTransactionsForPartyID: " + businessTransactionList.length);
        this.pSummary.businessSummary.bizTrxs = [];
        this.pSummary.businessSummary.bizTrxs = businessTransactionList;
        calculateBusinessSummary(this.pSummary.businessSummary, this.financeSettings.exchangeRate, this.confidentialityLevel);
      }));

    this.mySubscriptions.add(this.transactionProvider.getFinanceTransactionsForPartyID(this.user, this.pSummary.profile.id).valueChanges()
      .subscribe(financeTransactionList => {
        console.log("Subscribed to getFinanceTransactionsForPartyID: " + financeTransactionList.length);
        this.pSummary.businessSummary.finTrxs = [];
        this.pSummary.businessSummary.finTrxs = financeTransactionList;
        calculateBusinessSummary(this.pSummary.businessSummary, this.financeSettings.exchangeRate, this.confidentialityLevel);
      }));

  }

  ionViewWillLeave() {
    console.log("Welcome to PartySummaryPage::ionViewWillLeave()");
    this.mySubscriptions.unsubscribe();
  }

  addFinanceTransaction() {
    if (this.network.type === 'none') {
      alert('The internet connection appears to be offline! Please check your connection');
    }
    else {
      const modal = this.modalCtrl.create('FinanceTransactionPage', {
        User: this.user, party:
          this.pSummary.profile, isNewTrxn: true, financeSettings: this.financeSettings, confidentialityLevel: this.confidentialityLevel
      }); //RB2101
      modal.present();
      console.log("inside SummaryPage::addFinanceTrxn()");
      modal.onDidDismiss((data) => {
        //   getPartySummary(this.firebaseProvider,this.pSummary);  
      });
    }
  }

  addBusinessTransaction() {
    console.log("this.network.type" + this.network.type);
    if (this.network.type === 'none') {
      alert('The internet connection appears to be offline! Please check your connection');
    }
    else {
      /*
      const modal = this.modalCtrl.create('BusinessTransactionPage',{User:this.user,party:this.pSummary.profile,financeSettings:this.financeSettings,confidentialityLevel:this.confidentialityLevel}); //RB2101
      modal.present();
      console.log("inside SummaryPage::addBusinessTrxn() : pSummary.partyID => " + this.pSummary.profile.id);
      modal.onDidDismiss(data => {
        console.log('addBusinessTrnx::onDidDismiss()');
        //getPartySummary(this.databaseProvider,this.pSummary);  
      });
      */
      if (this.pSummary.profile.category === myConstants.partyCategory.SUPPLIER) {
        this.navCtrl.push('BusinessTransactionPage', { User: this.user, party: this.pSummary.profile, financeSettings: this.financeSettings, confidentialityLevel: this.confidentialityLevel });
      }
      else {
        this.navCtrl.push('SalesTransactionItemsPage', { User: this.user, fromEntity: "PARTY", partyProfile: this.pSummary.profile, confidentialityLevel: this.confidentialityLevel });
      }
    }
  }

  openPopOver(event, trxSummary) {
    let popover = this.popOverCtrl.create('BusinessTransactionPopoverPage', { User: this.user, trxSummary: trxSummary, pSummary: this.pSummary, financeSettings: this.financeSettings });
    popover.present({
      ev: event
    });
  }

  downloadPdf() {
    let today = new Date();

    function buildTableBody(data, columns) {
      let body = [];
      console.log("data length " + data.length);
      console.log("column length " + columns.length);

      body.push(columns);

      data.forEach(function (row) {
        var dataRow = [];

        columns.forEach(function (column) {
          dataRow.push(row[column]);
        })

        body.push(dataRow);
      });

      return body;
    }

    function table(data, columns) {
      return {
        alignment: 'center',
        table: {
          headerRows: 1,
          widths: ['40%', '20%', '20%', '20%'],
          body: buildTableBody(data, columns), fillColor: 'black'
        },
        layout: {
          fillColor: function (i, node) {
            return (i === 0) ? '#CCCCCC' : null;
          }
        }
      };
    }

    function displayBusinessTrxHeader1(businessTrxSummary) {
      let datepipe: DatePipe = new DatePipe('en-US');
      return {
        alignment: 'center',
        table: {
          headerRows: 0,
          widths: ['40%', '20%', '40%'],
          body: [
            [
              { text: datepipe.transform(businessTrxSummary.businessTransaction.trxDate, 'dd-MM-yyyy'), color: 'white', bold: true, fontSize: 15 },
              { text: businessTrxSummary.businessTransaction.trxDueDays + ' days', color: 'white', bold: true, fontSize: 15 },
              { text: datepipe.transform(businessTrxSummary.businessTransaction.trxDueDate, 'dd-MM-yyyy'), color: 'white', bold: true, fontSize: 15 }
            ]
          ], fillColor: 'black'
        },
        layout: {
          fillColor: function (i, node) {
            return (i === 0) ? '#212121' : null;
          }
        }
      };
    }

    function displayBusinessTrxHeader2(businessTrxSummary) {
      return {
        alignment: 'center',
        table: {
          headerRows: 0,
          widths: ['*'],
          body: [
            [
              { text: businessTrxSummary.businessTransaction.trxQty + ' cts x ' + businessTrxSummary.businessTransaction.trxRate + ' ' + businessTrxSummary.businessTransaction.trxCurrency, color: 'white', bold: true, fontSize: 15 }
            ]
          ]
        },
        layout: {
          fillColor: function (i, node) {
            return (i === 0) ? '#212121' : null;
          }
        }
      };
    }


    function displayBusinessTrxFooter(businessTrxSummary) {
      return {
        alignment: 'center',
        table: {
          headerRows: 0,
          widths: ['34%', '33%', '33%'],
          body: [
            [
              { text: businessTrxSummary.trxLocalAmount, fontSize: 15, bold: true, italic: true },
              { text: businessTrxSummary.amtReceviedOrPaidinLocal, color: 'green', fontSize: 15, bold: true, italic: true },
              { text: businessTrxSummary.amtPendinginLocal, color: 'red', fontSize: 15, bold: true, italic: true }
            ],
            [
              { text: 'Interest ==> ', alignment: 'right', fontSize: 15, italic: true },
              { text: '(' + businessTrxSummary.interestRealizedinLocal + ')', color: 'black', fontSize: 15, italic: true },
              { text: '(' + businessTrxSummary.interestUnRealizedinLocal + ')', color: 'black', fontSize: 15, italic: true }
            ]
          ], fillColor: 'red'
        },
        layout: {
          fillColor: function (i, node) {
            return (true) ? '#CCCCCC' : null;
          }
        }
      };
    }
    function displayTransactions(businessTrxSummary, partyProfile) {
      let recs = [];
      for (var i = 0; i < businessTrxSummary.length; i++) {
        recs.push(displayBusinessTrxHeader1(businessTrxSummary[i]));
        recs.push(displayBusinessTrxHeader2(businessTrxSummary[i]));
        recs.push('\n');
        recs.push(displayFinanceTrx(businessTrxSummary[i].finTrxs, partyProfile));
        recs.push('\n');
        recs.push(displayBusinessTrxFooter(businessTrxSummary[i]));
        recs.push('\n\n');
      }
      return recs;
    }

    function displayFinanceTrx(finTrxs, partyProfile) {
      return {
        alignment: 'center',
        table: {
          headerRows: 0,
          widths: ['20%', '20%', '20%', '20%', '20%'],
          body: buildFinanceBody(finTrxs, partyProfile), fillColor: 'black'
        },
      };
    }

    function buildFinanceBody(finTrxs, partyProfile) {
      let body = [];
      let columns = ['Date', 'Type', 'Amount', 'Interest', 'Remarks']
      let datepipe: DatePipe = new DatePipe('en-US');
      console.log("PDF:No. of Fin Trx " + finTrxs.length);

      //body.push(columns);

      finTrxs.forEach(function (row) {
        let dataRow = [];
        dataRow.push(datepipe.transform(row.trxDate, 'dd-MM-yyyy'));
        console.log("PDF: Category - " + partyProfile.category + " trxType - " + row.trxType);
        if (partyProfile.category === myConstants.partyCategory.BUYER) {
          if (row.trxType === myConstants.trxType.IN_CREDIT) { dataRow.push("R (-)"); }
          else if (row.trxType === myConstants.trxType.OUT_DEBIT) { dataRow.push("P (+)"); }
          else if ((row.trxType === myConstants.trxType.DUE_DEBIT) || (row.trxType === myConstants.trxType.DUE_CREDIT)) { dataRow.push("Due"); }
        }
        else if (partyProfile.category === myConstants.partyCategory.SUPPLIER) {
          if (row.trxType === myConstants.trxType.IN_CREDIT) { dataRow.push("P (+)"); }
          else if (row.trxType === myConstants.trxType.OUT_DEBIT) { dataRow.push("R (-)"); }
          else if ((row.trxType === myConstants.trxType.DUE_DEBIT) || (row.trxType === myConstants.trxType.DUE_CREDIT)) { dataRow.push("Due"); }
        }
        dataRow.push(row.trxLocalValue);
        dataRow.push(row.trxInterestRealizedinLocal);
        dataRow.push(row.trxRemarks);
        body.push(dataRow);
      });
      return body;
    }

    let datepipe: DatePipe = new DatePipe('en-US');
    let dd = {
      content: [
        { text: this.pSummary.profile.fullName, bold: true, alignment: 'center', fontSize: 20 },
        '\n',
        { text: 'Summary as on : ' + this.pSummary.sysDate.getDate() + '-' + (this.pSummary.sysDate.getMonth() + 1) + '-' + this.pSummary.sysDate.getFullYear(), italic: true, alignment: 'center', fontSize: 10 },
        //{text: 'Summary as on : ' + datepipe.transform(this.pSummary.sysDate.toISOString(),'medium'), italic:true,alignment:'center' , fontSize : 10},
        '\n',
        { text: 'Total : ' + this.pSummary.businessSummary.businessCurrency + " " + this.pSummary.businessSummary.businessAmtTotalinLocal, bold: true, alignment: 'center', fontSize: 15 },
        '\n',
        {
          alignment: 'center',
          columns: [
            {
              text: 'Received : ' + this.pSummary.businessSummary.amtReceviedOrPaidinLocal, alignment: 'center', color: 'green', fontSize: 15, bold: true
            },
            {
              text: 'Pending : ' + this.pSummary.businessSummary.amtPendinginLocal, alignment: 'center', color: 'red', fontSize: 15, bold: true
            }
          ]
        },
        {
          alignment: 'center',
          columns: [
            {
              text: 'Interest on Received : ' + this.pSummary.businessSummary.interestRealizedinLocal, alignment: 'center', color: 'black', fontSize: 15, bold: true
            },
            {
              text: 'Interest on Remaining : ' + this.pSummary.businessSummary.interestUnRealizedinLocal, color: 'black', fontSize: 15, bold: true
            }
          ]
        },
        '\n',
        displayTransactions(this.pSummary.businessSummary.bizTrxSummary, this.pSummary.profile)
      ]
    }

    this.pdfObj = pdfMake.createPdf(dd);
    let date = today.getDate();
    let month = today.getMonth() + 1;
    let year = today.getFullYear();
    let pdfName = this.pSummary.profile.fullName + '_' + date + '_' + month + '_' + year + '.pdf';
    console.log('month : ' + today.getMonth());
    if (this.platform.is('cordova')) {
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory, pdfName, blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory + pdfName, 'application/pdf');
        })
      });
    }
    else {
      // On a browser simply use download!
      this.pdfObj.download();
    }
  }

  gotoBusinessTrxSummaryPage(bizTrxSummary: BusinessTransactionSummary) { //bizTrx:BusinessTransaction, pSummary:PartySummary,index:number
    //console.log('diff days' + pSummary.bizTrxSummary[index].diffDays);
    //this.navCtrl.push('BusinessTransactionSummaryPage',{User:this.user,bizTrxSummary:bizTrxSummary,profile:profile}); //,{finTrxSummary: finTrxSummary}
    this.navCtrl.push('BusinessTransactionSummaryPage', { User: this.user, bizTrxID: bizTrxSummary.businessTransaction.ID, partyProfile: this.pSummary.profile }); //,{finTrxSummary: finTrxSummary}
  }

  gotoPartyTransaction() {
    //this.navCtrl.push('PartyTransactionsPage',{User:this.user,financeSettings:this.financeSettings,pSummary:this.pSummary}); //,{finTrxSummary: finTrxSummary}
    this.navCtrl.push('PartyTransactionsPage', { User: this.user, partyProfile: this.pSummary.profile }); //,{finTrxSummary: finTrxSummary}
  }

  onTitleClick() //Added by Rashmin - 09082018
  {
    if ((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC)
      && (this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova'))) {
      let modal = this.modalCtrl.create('LockScreenPage');
      modal.present();
      modal.onDidDismiss((data) => {
        if (data === true) {
          this.switchConfidentialityLevel();
        }
        else {
          alert('Authentication Failed !!')
        }
      });
    }
    else //web flow
    {
      this.switchConfidentialityLevel();
    }
  }

  switchConfidentialityLevel() {
    if ((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC)
      && (true))//this.user.confidentialityLevel >= myConstants.confidentialityLevel.PUBLIC
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.CONFIDENTIAL;
    }
    else {
      this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    }
    calculateBusinessSummary(this.pSummary.businessSummary, this.financeSettings.exchangeRate, this.confidentialityLevel);
  }


}
