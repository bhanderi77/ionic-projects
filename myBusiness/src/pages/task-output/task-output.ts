import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Loading,LoadingController,AlertController,ItemSliding,ViewController} from 'ionic-angular';
//import { DatabaseProvider } from '../../providers/database/database';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { SettingsProvider } from '../../providers/settings/settings';

import { PackagedItem,ItemShape,ItemSieve,ItemQualityLevel } from '../../model/packagedItem';
import { PackageRule } from '../../model/packageRule';
import { myConstants,round } from '../../model/myConstants';
import { taskOutput } from '../../model/taskOutput';
import { User } from '../../model/user';
/**
 * Generated class for the TaskOutputPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task-output',
  templateUrl: 'task-output.html',
})
export class TaskOutputPage {
  private user:User;
  private bValidOutput:boolean;
  private totalOutputWeightFlag:boolean;
  private totalOutputQtyFlag:boolean;
//  private totalOutputTrxShareFlag:boolean;
  private outputWeightFlag:boolean;
  private outputQtyFlag:boolean;
  //private outputTrxShareFlag:boolean;


  private totalOutputQty:number;
  private pItem:PackagedItem;
  private pOutputItemList:Array<PackagedItem>;

  private output:Array<taskOutput>;
  private packageRuleListBtoC:Array<PackageRule>;
  private inputWeight:number;
  private totalOutputWeight:number;
  //private totalOutputTrxShare:number;
  private itemSieveList:Array<ItemSieve>;
  private itemShapeList:Array<ItemShape>;
  private itemQualityLevelList:Array<ItemQualityLevel>;

  constructor(private navCtrl: NavController,
    private viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private inventoryProvider: InventoryProvider,
    private settingsProvider: SettingsProvider,    
    private navParams: NavParams ) 
    {
      this.user = {} as User;
      this.user = navParams.get('User');
  
      this.pItem = navParams.get("pItem");  
      // this.outputWeight = new Array(myConstants.TASK_OUTPUT_MAX_REC);
      // this.outputWeight.length=1;
      this.totalOutputWeightFlag=true;
      this.totalOutputQtyFlag=true;
      this.outputQtyFlag=true;
      this.outputWeightFlag=true;
    //  this.outputTrxShareFlag=true;

      this.bValidOutput=false;
      this.inputWeight= +this.pItem.itemWeight;
      this.output=[];
      this.pOutputItemList = [];
      
      this.totalOutputWeight=0;
      this.totalOutputQty=0;
      //this.totalOutputTrxShare=0;

      this.initializeData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskOutputPage');
    console.log("packageRuleListBtoC.length " + this.packageRuleListBtoC.length);
  }

  dismissModel(){
    this.viewCtrl.dismiss();
  }

  TrackBy(index: number, obj: any):any
  {
    //console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  async initializeData()
  {
    console.log("Welcome to TaskOutputPage::initializeData()");
    this.packageRuleListBtoC = [];
    this.itemShapeList = [];
    this.itemSieveList = [];
    this.itemQualityLevelList= [];
      
    this.packageRuleListBtoC = await this.settingsProvider.getPackageRule_For_A(this.user,this.pItem.pRule.nextTaskCode);
    this.itemShapeList = await this.settingsProvider.getItemShapeList(this.user,true);
    console.log("itemShapeList.length " + this.itemShapeList.length);
    this.itemSieveList = await this.settingsProvider.getItemSieveList(this.user,myConstants.itemType.POLISHED,true);
    console.log("itemSieveList.length " + this.itemSieveList.length);
    this.itemQualityLevelList = await this.settingsProvider.getItemQualityLevelList(this.user,myConstants.itemType.POLISHED,true);
    console.log("itemQualityLevelList.length " + this.itemQualityLevelList.length);
   
  }

  deleteInputRow(o:taskOutput,item: ItemSliding)
  {
    this.output.splice(this.output.indexOf(o),1);
    this.validateOutput();
  }


  addInputRow()
  {
    console.log("Welcome to TaskOutputPage:addInputRow()");
    let outputTaskCode:string = "";

    console.log("this.packageRuleListBtoC.length " + this.packageRuleListBtoC.length);
    if(this.packageRuleListBtoC.length > 1)
    {
      let alert = this.alertCtrl.create();
      for(let i=0; i<this.packageRuleListBtoC.length;i++)
      {
        alert.addInput({type: 'radio',
                      label: this.packageRuleListBtoC[i].nextTaskCode,
                      value: this.packageRuleListBtoC[i].nextTaskCode,
                      checked: false});
      }
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: (data:string) => {
          console.log("data " + data);
          outputTaskCode = data;
          this.createOutputRow(outputTaskCode);
          this.bValidOutput=false;
        }
      });
      alert.present();
    }
    else
    {
      outputTaskCode = this.packageRuleListBtoC[0].nextTaskCode;
      this.createOutputRow(outputTaskCode);
      this.bValidOutput=false;
    }
    
  }

  createOutputRow(outputTaskCode:string)
  {
    console.log("Welcome to TaskOutputPage:createOutputRow()");
    if((outputTaskCode === myConstants.taskCode.SALES) && (this.pItem.pRule.nextTaskCode === myConstants.taskCode.ASSORTMENT))
    {
      console.log("Show alert for sieve " + this.itemSieveList.length);
      let alert = this.alertCtrl.create();
      for(let i=0; i<this.itemSieveList.length;i++)
      {
        alert.addInput({type: 'radio',
                      label: this.itemSieveList[i].id,
                      value: this.itemSieveList[i].id,
                      checked: false});
      }
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: (data:string) => {
          console.log("data " + data);
          
          for(let i=0; i<this.itemQualityLevelList.length;i++)
          {
            let o= {} as taskOutput;
            o.weight= +0;
            o.qty= +0;
            o.remark="";
            //o.nextTaskCode = nextTask;
            o.nextTaskCode = outputTaskCode;
            o.sieve = data;
            o.shape = this.pItem.itemShape.id;
            o.qualityLevel = this.itemQualityLevelList[i].id;
            o.validWeight=true;
            o.validQty=true;
            console.log("o.shape : " + o.shape + " o.sieve : " + o.sieve + " o.qualityLevel : " + o.qualityLevel);
            this.output.push(o);
          }          
          this.bValidOutput=false;
        }
      });
      alert.present();
    }
    else if(outputTaskCode === myConstants.taskCode.ASSORTMENT)
    {
      console.log("Show alert for Shape " + this.itemShapeList.length);
      let alert = this.alertCtrl.create();
      for(let i=0; i<this.itemShapeList.length;i++)
      {
        alert.addInput({type: 'radio',
                      label: this.itemShapeList[i].id,
                      value: this.itemShapeList[i].id,
                      checked: false});
      }
      alert.addButton('Cancel');
      alert.addButton({
        text: 'OK',
        handler: (data:string) => {
          console.log("data " + data);
          
          
          let o= {} as taskOutput;
          o.weight= +0;
          o.qty= +0;
          o.remark="";
          //o.nextTaskCode = nextTask;
          o.nextTaskCode = outputTaskCode;
          o.sieve = "";
          o.shape = data;
          o.qualityLevel = "";
          o.validWeight=false;
          o.validQty=false;
          console.log("o.shape : " + o.shape + " o.sieve : " + o.sieve + " o.qualityLevel : " + o.qualityLevel);
          this.output.push(o);
          this.bValidOutput=false;
        }
      });
      alert.present();
    }
    else
    {
      let o= {} as taskOutput;
      
      o.weight= +0;
      o.qty= +0;
      o.remark="";
      //o.nextTaskCode = nextTask;
      o.nextTaskCode = outputTaskCode;
      o.sieve = myConstants.itemSieve.NOTDEFINED;
      o.shape = myConstants.itemShape.NOTDEFINED;
      o.qualityLevel = myConstants.polishQualityLevel.NOTDEFINED;
      o.validWeight=false;
      o.validQty=false;
      console.log("o.nextTaskCode : " + o.nextTaskCode);
      this.output.push(o);
    }
  }

  validateWeight(o:taskOutput)
  {
    console.log("Welcome to validateWeight : " + o.weight + " " + o.qty);
    if(o.weight === undefined)
    {
      o.validWeight=false;
      return;
    }
    if(( +o.weight <= +this.inputWeight) || (+o.weight >= 0))
    {
      o.validWeight=true;
      this.outputWeightFlag = true;
    }
    else
    {
      o.validWeight=false;
      this.outputWeightFlag = false;
    }
    console.log("o.validWeight " + o.validWeight + " this.outputWeightFlag " + this.outputWeightFlag);
    this.validateOutput();
  }

  /*
  validateQty(o:taskOutput){
    if(+o.qty >= 0)
    {
      o.validQty = true;
      this.outputQtyFlag = true;
    }
    else
    {
      o.validQty = false;
      this.outputQtyFlag = false;
    }
    
    this.validateOutput();
  }*/


  validateOutput()
  {
    this.totalOutputWeight=0;
    this.totalOutputQty=0;

    for(let i = 0 ; i < this.output.length ;i++)
    {
      this.totalOutputWeight += +(this.output[i].weight);
      this.totalOutputQty += +(this.output[i].qty);
      console.log("this.totalOutputWeight " + this.totalOutputWeight);
    }
    
    this.totalOutputWeight = round(this.totalOutputWeight,2);
    this.totalOutputQty = round(this.totalOutputQty,2);
    console.log("\n this.totalOutputWeight " + this.totalOutputWeight);
    console.log("this.inputWeight " + this.inputWeight);
    if(+this.totalOutputWeight > 0)
    {
      if((+this.totalOutputWeight < +this.inputWeight) && (this.pItem.pRule.bReducedOutput))
        this.totalOutputWeightFlag = true;
      else if((+this.totalOutputWeight === +this.inputWeight) && (!this.pItem.pRule.bReducedOutput))
        this.totalOutputWeightFlag = true;
      else
        this.totalOutputWeightFlag = false;
    }
    else
    {
      this.totalOutputWeightFlag = false;
    }
    
    this.totalOutputQtyFlag = true;
    this.outputQtyFlag = true;
    /*
    if((+this.totalOutputQty >= +this.pItem.itemQty) && (+this.totalOutputQty > 0))
      this.totalOutputQtyFlag = true;
    else
      this.totalOutputQtyFlag = false;
    */

    if( (this.totalOutputWeightFlag === true) && (this.outputWeightFlag === true)
     && (this.totalOutputQtyFlag === true) && (this.outputQtyFlag === true )
      )
    {
      console.log("this.totalOutputWeightFlag " + this.totalOutputWeightFlag);
      console.log("this.totalOutputQtyFlag " + this.totalOutputQtyFlag);
      console.log("this.outputWeightFlag " + this.outputWeightFlag);
      console.log("this.outputQtyFlag " + this.outputQtyFlag);
      this.bValidOutput = true;
      console.log("this.output.length " + this.output.length);
      /*
      for(let i=0;i<this.output.length;i++)
      {
        console.log("i=" + i + " " + this.output[i].weight + " " + this.output[i].validWeight + " " + this.output[i].qty + " " + this.output[i].validQty);
        if(!(this.output[i].validWeight) || !(this.output[i].validQty)){
          this.bValidOutput = false;
          break;
        }
      }*/
    }
    else
    {
      this.bValidOutput = false;
    }
    
  }
  

  async saveOutput()
  {
    /*CREATE NEW ENTRY B->C in RD status and update B->B to CO status
    (01) Create new object of packagedItem
    (02) Set item Details and packageID related fields
    (03) Find out applicable package rule
    (03.1) Set package rule
    (03.2) set owner details
    (04) add new entry to DB 
    (05) Change status to CO for selPackagedItem
    (06) refresh data for this page
    */
    console.log("TaskSummaryPage: Welcome to saveOutput() ");
   
    let totalOutputRecs:number=0;
   //let totalOutput:number=0;
   // totalOutput = this.output[0].weight;
    for(let i=0;(i<this.output.length);i++)
    {
      if(this.output[i].weight>0)
        totalOutputRecs += 1;
      // totalOutput += +(this.output[i].weight);    
    }
    if((!this.pItem.pRule.bMultipleOutput) && (totalOutputRecs > 1))
    {
      alert("Enter single output");
      return;
    }
  
    if((this.totalOutputWeight === this.pItem.itemWeight) && (this.pItem.pRule.bReducedOutput))
    {
      alert("Total Output can not be same as input");
      return;
    }

    if((this.totalOutputWeight !== this.pItem.itemWeight) && (!this.pItem.pRule.bReducedOutput))
    {
      alert("Total Output weight has to be same as input weight");
      return;
    }
    console.log("totalOutputRecs: " + totalOutputRecs);  
    console.log("totalOutput: " + this.totalOutputWeight); 
    
    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();

   //(03) Find out applicable package rule
    if(this.packageRuleListBtoC.length >= 1)
    {
      let packagedItemBtoCList:Array<PackagedItem> = [];
      console.log("this.outputWeight.length " + this.output.length);
      
      for(let i=0;(i<this.output.length);i++)
      {
        console.log("this.outputWeight[" + i + "]: " + (this.output[i].weight));
        
        if(this.output[i].weight>0)
        {
          let packagedItemBtoC = {} as PackagedItem;
        
          packagedItemBtoC.itemRemarks = (this.output[i].remark);
          //(02) Set item Details and packageID related fields                            
          packagedItemBtoC.itemQty = +(this.output[i].qty);
          packagedItemBtoC.itemWeight = +(this.output[i].weight);
          packagedItemBtoC.itemValuation = this.pItem.itemValuation;

          packagedItemBtoC.confidentialityLevel = this.pItem.confidentialityLevel;

          packagedItemBtoC.trxID = this.pItem.trxID;
          packagedItemBtoC.trxDate = this.pItem.trxDate;
          packagedItemBtoC.trxPartyID = this.pItem.trxPartyID;
          packagedItemBtoC.trxPartyName = this.pItem.trxPartyName;
          packagedItemBtoC.trxWeight = this.pItem.trxWeight;
          packagedItemBtoC.trxRate = this.pItem.trxRate;
          packagedItemBtoC.trxCurrency = this.pItem.trxCurrency;
          packagedItemBtoC.trxShare = round(((+this.output[i].weight/+this.totalOutputWeight)*(+this.pItem.trxShare)),2);
          
          
          /*
          if(totalOutputRecs > 1)
          {
            packagedItemBtoC.taskCost = round(((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (0)),2);
            packagedItemBtoC.accumulatedTaskCost = round(((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.accumulatedTaskCost)),2);
            packagedItemBtoC.accumulatedTaskCost += packagedItemBtoC.taskCost;
            packagedItemBtoC.accumulatedTaskCost = round(packagedItemBtoC.accumulatedTaskCost,2);

            packagedItemBtoC.roughWeight = round(((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.roughWeight)),2);
          }
          else
          {
            packagedItemBtoC.taskCost = 0;
            packagedItemBtoC.accumulatedTaskCost = (this.pItem.accumulatedTaskCost);
            packagedItemBtoC.accumulatedTaskCost += packagedItemBtoC.taskCost;
            packagedItemBtoC.accumulatedTaskCost = round(+packagedItemBtoC.accumulatedTaskCost,2);

            packagedItemBtoC.roughWeight = +this.pItem.roughWeight;          
          } 
          */

          if(this.pItem.pRule.bIncursCost)
          {
            packagedItemBtoC.taskCost = round(((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (200)),2);
            packagedItemBtoC.parentID = this.pItem.parentID;
          }
          else
          {
            packagedItemBtoC.taskCost = 0;
            packagedItemBtoC.parentID = this.pItem.id;
          }         
          

          packagedItemBtoC.taskCost = round(((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (0)),2);
          packagedItemBtoC.accumulatedTaskCost = round(((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.accumulatedTaskCost)),2);
          packagedItemBtoC.accumulatedTaskCost += packagedItemBtoC.taskCost;
          packagedItemBtoC.accumulatedTaskCost = round(packagedItemBtoC.accumulatedTaskCost,2);

          packagedItemBtoC.roughWeight = round(((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.roughWeight)),2);
          
          packagedItemBtoC.packageID = "";
          /*
          if((totalOutputRecs === 1) && (this.totalOutputWeight === this.pItem.itemWeight))
          {
            packagedItemBtoC.packageID = this.pItem.packageID + "=" + packagedItemBtoC.itemWeight;        
          }
          else if((totalOutputRecs === 1) && (this.totalOutputWeight !== this.pItem.itemWeight))
          {
            packagedItemBtoC.packageID = this.pItem.packageID + "-" + packagedItemBtoC.itemWeight;        
          }
          else if((totalOutputRecs > 1) && (this.totalOutputWeight === this.pItem.itemWeight))
          {
            packagedItemBtoC.packageID = this.pItem.packageID + "<=" + packagedItemBtoC.itemWeight;        
          }
          else if((totalOutputRecs > 1) && (this.totalOutputWeight !== this.pItem.itemWeight))
          {
            packagedItemBtoC.packageID = this.pItem.packageID + "<-" + packagedItemBtoC.itemWeight;        
          }
          */
          if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.PLANNING))
          {
            packagedItemBtoC.packageGroupID = this.pItem.packageGroupID+"/"+packagedItemBtoC.itemWeight;
          }
          else
          {
            packagedItemBtoC.packageGroupID = this.pItem.packageGroupID
          }
          
          packagedItemBtoC.packageGroupName = this.pItem.packageGroupName;
          if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.PLANNING)
          && (this.output[i].remark))
          {
            packagedItemBtoC.packageGroupName = this.output[i].remark;
          }

          
          //(03.1) Set package rule
          let pRuleList:Array<PackageRule> = [];
          pRuleList = this.packageRuleListBtoC.filter(pRule => pRule.nextTaskCode === this.output[i].nextTaskCode );  
          packagedItemBtoC.pRule = pRuleList[0];
          packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
          //(03.2) set owner details                              
          packagedItemBtoC.packageCurTaskOwner = this.pItem.packageNextTaskOwner;
          packagedItemBtoC.packageNextTaskOwner = this.pItem.packageNextTaskOwner;
          
          //packagedItemBtoC.itemShape = {} as ItemShape;
          /*
          */
          let iShape:Array<ItemShape> = [];
          let iSieve:Array<ItemSieve> = [];
          let iQualityLevel:Array<ItemQualityLevel> = [];

          iShape = this.itemShapeList.filter(shape=>shape.id === this.output[i].shape);
          iSieve = this.itemSieveList.filter(sieve=>sieve.id === this.output[i].sieve);
          iQualityLevel = this.itemQualityLevelList.filter(qualityLevel=>qualityLevel.id === this.output[i].qualityLevel);
          if(iShape.length)
          {
            packagedItemBtoC.itemShape = iShape[0];          
          }
          else
          {
            packagedItemBtoC.itemShape = await this.settingsProvider.getItemShape(this.user,this.output[i].shape);
          }
          if(iSieve.length)
          {
            packagedItemBtoC.itemSieve = iSieve[0];          
          }
          else
          {
            packagedItemBtoC.itemSieve = await this.settingsProvider.getItemSieve(this.user,this.output[i].sieve);
          }
          if(iQualityLevel.length)
          {
            packagedItemBtoC.itemQualityLevel = iQualityLevel[0];          
          }
          else
          {
            packagedItemBtoC.itemQualityLevel = await this.settingsProvider.getItemQualityLevel(this.user,this.output[i].qualityLevel);
          }
          packagedItemBtoCList.push(packagedItemBtoC);       
        }        
      }
      // console.log("length of packagedItemBtoCList = " + );
      loading.present();
       //(04) add new entry B->C with RD status to DB
      return this.inventoryProvider.createPackagedItemArray_BtoC(this.user,packagedItemBtoCList,this.pItem)
      .then(()=>{
        console.log("B->C added");
        loading.dismiss();        
        this.navCtrl.pop();
      }).catch(e=>{
        console.log("Error in this.inventoryProvider.createPackagedItemArray_BtoC" + e);
      });
    }//end of if(packageRuleListBtoC.length === 1)
    else if(this.packageRuleListBtoC.length === 0)
    {
      alert("TaskOutputPage:No rule exists to save output");
    }    
  }
}
