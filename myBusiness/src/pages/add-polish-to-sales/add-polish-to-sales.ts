import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ModalController,Loading,LoadingController } from 'ionic-angular';
import { FormControl } from '@angular/forms';

import { InventoryProvider } from '../../providers/inventory/inventory';
import { SettingsProvider } from '../../providers/settings/settings';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { EntityProvider } from '../../providers/entity/entity';

import { PackagedItem,ItemType,ItemShape,ItemSieve,ItemQualityLevel,ItemValuation } from '../../model/packagedItem';
import { BusinessTransaction } from '../../model/businessTransaction';
import { FinanceTransaction } from '../../model/financeTransaction';
import { BusinessTransactionSummary,getBusinessTransactionSummary } from '../../model/partySummary';


import { MarketPrice } from '../../model/marketPrice';
import { SalesSummaryByTSS, SalesSummary } from '../../model/salesSummary';
//import { doInitialize,doRound,doTotal,doAverage } from '../../model/salesSummary';
import { InventoryParams } from '../../model/rateCard';

import { User, checkIfValid } from '../../model/user';
import { SalesItem } from '../../model/salesItem';
import { PackageRule } from '../../model/packageRule';

import { myConstants, round } from '../../model/myConstants';
import { Subscription } from 'rxjs';

/**
 * Generated class for the AddPolishToSalesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-polish-to-sales',
  templateUrl: 'add-polish-to-sales.html',
})
export class AddPolishToSalesPage {
  private user:User;
  private loading:Loading;
  private currencyExchangeRate:number;

//  private partyProfile:BusinessContact;
  private businessTransaction:BusinessTransaction;
  private daysForDue:number;
  private finTrxList:Array<FinanceTransaction>;
  private trxAvgPurchaseCost:number;
  private trxAvgInterestCost:number;
  
  private labourRate:number;
  
  private inventoryParams:InventoryParams;
  private pItemList_AB:Array<PackagedItem>;
  private pItem:PackagedItem;

  private itemShapeList:Array<ItemShape>;
  private itemSieveList:Array<ItemSieve>;
  private itemQualityLevelList:Array<ItemQualityLevel>;
  private itemPriceList:Array<MarketPrice>;

  private roughWeight:number;
  private totalInputWeight:number;
  
  private confidentialityLevel:number;

  private salesSummaryByTSSList:Array<SalesSummaryByTSS>;
  private totalSalesSummary:SalesSummary;

  constructor(public navCtrl: NavController, 
    private settingsProvider:SettingsProvider,
    private inventoryProvider:InventoryProvider,
    private transactionProvider:TransactionProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,    
    public navParams: NavParams) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');
    
    //this.partyProfile = {} as BusinessContact;
    this.businessTransaction = {} as BusinessTransaction;
    this.daysForDue = 0;

    this.salesSummaryByTSSList = [];
    this.totalSalesSummary = new SalesSummary();
    //doInitialize(this.totalSalesSummary);

    this.totalInputWeight = 0;
    this.pItem = {} as PackagedItem;
    this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    
    this.itemPriceList = [];
    this.roughWeight = 0;
    
    this.finTrxList = [];
    this.trxAvgPurchaseCost = 0;
    this.trxAvgInterestCost = 0;
    this.labourRate = 0;
    
    this.inventoryParams = {} as InventoryParams;
    this.inventoryParams.avgP2SGain = 0;
    this.inventoryParams.avgMfgGain = 0;
    this.inventoryParams.avgMarketRateForSaleable = 0;
    this.inventoryParams.avgMfgCostForPolished = 0;
    this.inventoryParams.avgMfgCostForRough = 0;
    this.inventoryParams.purchaseCostPerSaleableUnit = 0;
    this.inventoryParams.interestCostPerSaleableUnit = 0;
    this.inventoryParams.totalMarketValueForSaleable = 0;
    this.inventoryParams.totalPurchaseWeight = 0;
    this.inventoryParams.totalSaleablePolishedWeight = 0;
    this.inventoryParams.totalSaleableRoughWeight = 0;
    this.inventoryParams.totalSaleableTopsWeight = 0;
    this.inventoryParams.totalSaleableWeight = 0;
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad AddPolishToSalesPage');
  }

  ionViewWillEnter()
  {
    console.log('ionViewDidLoad AddPolishToSalesPage');
    this.pItem = this.navParams.get('pItem');
    if(!this.pItem)
    {
      this.createPurchaseItem();
    }
    this.initializeData();
  }

  async createPurchaseItem()
  {
    console.log("Welcome to AddPolishToSalesPage::createPurchaseItem()");
    if(!this.pItem)
    {
      this.pItem = {} as PackagedItem;
      this.pItem.itemType = {} as ItemType;
      this.pItem.trxID = "";
      this.pItem.trxDate = "";
      this.pItem.trxPartyID = "";
      this.pItem.trxPartyName = "";
      this.pItem.trxWeight = 0;
      this.pItem.trxRate = 0;
      this.pItem.trxCurrency = "";
      this.pItem.trxShare = 100;
          
      this.pItem.roughWeight = 0;
      this.pItem.taskCost = 0;
      this.pItem.accumulatedTaskCost = 0;
            
      this.pItem.id = "";
      this.pItem.packageID = "";        
      this.pItem.packageGroupID = "";
      this.pItem.packageGroupName = "";
          
      this.pItem.pRule = {} as PackageRule;
      this.pItem.pRule.curTaskCode = myConstants.taskCode.MANUAL;
      this.pItem.pRule.nextTaskCode = myConstants.taskCode.MANUAL;
      this.pItem.pRule.bGroupRead = false;
      this.pItem.pRule.bIncursCost = false;
      this.pItem.pRule.bMultipleOutput = false;
      this.pItem.pRule.bPartialAssign = false;
      this.pItem.pRule.bReducedOutput = false;
      this.pItem.pRule.bFirst = false;
      this.pItem.pRule.bLast = false;
      this.pItem.packageStatus = myConstants.packageStatus.CO;
      //(03.2) set owner details                              
      this.pItem.packageCurTaskOwner = this.pItem.packageNextTaskOwner;
      this.pItem.packageNextTaskOwner = this.pItem.packageNextTaskOwner;
    }
  }

  async initializeData()
  {
    console.log("Welcome to AddPolishToSalesPage:initializeData()");

    this.settingsProvider.getMarketPriceList(this.user)
    .then(priceList => {
      this.itemPriceList = priceList;
      //this.calculateSummaryOfSaleable();
    });

    if(this.pItem.pRule.curTaskCode === myConstants.taskCode.ASSORTMENT)
    {
      this.totalInputWeight = this.pItem.itemWeight;
    }
    
    /*
    this.entityProvider.getBusinessContact(this.user,this.pItem.trxPartyID)
    .then(contact => {
      this.partyProfile = contact;
    }).catch(e => {
      console.log("Error from this.entityProvider.getBusinessContact " + e);
    });*/
    
    this.currencyExchangeRate = 1;
    this.currencyExchangeRate = await this.settingsProvider.getExchangeRate(this.user);
    
    this.businessTransaction = {} as BusinessTransaction;
    // this.businessTransaction = await 
    this.transactionProvider.getBusinessTransaction(this.user,this.pItem.trxID)
    .then(bizTrx => {
      console.log("Received bizTrx");
      this.businessTransaction = bizTrx;
      this.daysForDue = 0;
      if(this.businessTransaction)
      {
        let today: Date = new Date();
        let dueDate: Date = (new Date(this.businessTransaction.trxDueDate));
        let duration: number = dueDate.valueOf() - today.valueOf();
        this.daysForDue = Math.floor(duration / (1000 * 3600 * 24)); 
        this.calculatePurchaseCostAndInterest();
      } 
    });

    this.itemShapeList = [];
    //this.polishShapeList = await this.settingsProvider.getItemShapeList(false);
    this.settingsProvider.getItemShapeList(this.user,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemShapeList = pList;
      }
    });
    
    this.itemSieveList = [];
    //this.polishSieveList = await this.settingsProvider.getItemSieveList(true);
    this.settingsProvider.getItemSieveList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemSieveList = pList;
        console.log("this.itemSieveList.length " + this.itemSieveList.length);
      }
    });
    
    this.itemQualityLevelList = [];
    //this.itemQualityLevelList = await this.settingsProvider.getItemQualityLevelList(true);
    this.settingsProvider.getItemQualityLevelList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemQualityLevelList = pList;   
      }
    });
    
  }

  getMarketRateForItem(s:SalesItem):number
  {
    //console.log("Welcome to SalesStockSummaryPage:getMarketRate() " + s.itemShape.id + s.itemSieve.id + s.itemQualityLevel.id);
    if(this.itemPriceList.length > 0)
    {
      for(let i=0;i<this.itemPriceList.length;i++)
      {
        /*
        console.log("shapeID " + this.polishPriceList[i].shapeID);
        console.log("sieveID " + this.polishPriceList[i].sieveID);
        console.log("qualityLevelID " + this.polishPriceList[i].qualityLevelID);
        */
        if( (this.itemPriceList[i].itemTypeID === s.itemType.id)
          && (this.itemPriceList[i].shapeID === s.itemShape.id)
          && (this.itemPriceList[i].sieveID === s.itemSieve.id)
          && (this.itemPriceList[i].qualityLevelID === s.itemQualityLevel.id))
        {
          //console.log("Returning this.polishPriceList[i].rate " + this.polishPriceList[i].rate);
          return this.itemPriceList[i].rate;
        }
      }
    }
    else
    {
      return -1;
    }
  }

  calculateSummaryOfSaleable()
  {
    console.log('Welcome to SalesTransactionItemsPage:calculateSummaryOfSaleable()');
    //doInitialize(this.totalSalesSummary);
    this.totalSalesSummary.initialize();
    this.inventoryParams.totalSaleablePolishedWeight = 0;
    this.inventoryParams.totalSaleableRoughWeight = 0;
    this.inventoryParams.totalSaleableTopsWeight = 0;
    this.inventoryParams.totalSaleableWeight = 0;
    this.inventoryParams.totalMarketValueForSaleable = 0;
    this.inventoryParams.totalPurchaseWeight = 0;

    for(let i=0;i<this.salesSummaryByTSSList.length;i++)
    {
      this.calculateSalesSummaryByTSS(this.salesSummaryByTSSList[i]);

      this.totalSalesSummary.totalSaleableWeight += +this.salesSummaryByTSSList[i].totalSaleableWeight;
      this.totalSalesSummary.totalSaleablePolishedWeight += +this.salesSummaryByTSSList[i].totalSaleablePolishedWeight;
      this.totalSalesSummary.totalSaleableRoughWeight += +this.salesSummaryByTSSList[i].totalSaleableRoughWeight;
      this.totalSalesSummary.totalSaleableTopsWeight += +this.salesSummaryByTSSList[i].totalSaleableTopsWeight;
      
      this.totalSalesSummary.totalPriceValue += +this.salesSummaryByTSSList[i].totalPriceValue;
      
      this.totalSalesSummary.totalPurchaseCost += +this.salesSummaryByTSSList[i].totalPurchaseCost;
      this.totalSalesSummary.totalInterestCost += +this.salesSummaryByTSSList[i].totalInterestCost;
      this.totalSalesSummary.totalMfgCost += +this.salesSummaryByTSSList[i].totalMfgCost;
      this.totalSalesSummary.totalGrossCost += +this.salesSummaryByTSSList[i].totalGrossCost;

      //this.totalSalesSummary.totalPriceValue += +this.salesSummaryByTSSList[i].salesSummary.totalPriceValue;
    }
    //doRound(this.totalSalesSummary,2);
    this.totalSalesSummary.doRound(2);
    //doAverage(this.totalSalesSummary,2);
    this.totalSalesSummary.doAverage(2);
    
    this.inventoryParams.totalSaleablePolishedWeight = this.totalSalesSummary.totalSaleablePolishedWeight;
    this.inventoryParams.totalSaleableRoughWeight = this.totalSalesSummary.totalSaleableRoughWeight;
    this.inventoryParams.totalSaleableTopsWeight = this.totalSalesSummary.totalSaleableTopsWeight;
    this.inventoryParams.totalSaleableWeight = this.totalSalesSummary.totalSaleableWeight;
    this.inventoryParams.totalMarketValueForSaleable = this.totalSalesSummary.totalPriceValue;
    
    //this.calculateGain();  
  }

  async addToSalesSummaryBySSList(itemTypeID:string)
  {
    console.log('Welcome to AddPolishToSalesPage:addToSalesSummaryBySSList() ' + itemTypeID);
    let salesSummaryByTSS = new SalesSummaryByTSS();
    
    salesSummaryByTSS.itemType = await this.settingsProvider.getItemType(this.user,itemTypeID);
    salesSummaryByTSS.itemShape = {} as ItemShape;
    salesSummaryByTSS.itemSieveMin = {} as ItemSieve;
    salesSummaryByTSS.itemSieveMax = {} as ItemSieve;
    
    //salesSummaryByTSS.salesSummary = new SalesSummary();
    //doInitialize(salesSummaryByTSS.salesSummary);
    salesSummaryByTSS.salesItemList = [];
    
    this.selectShape(salesSummaryByTSS);
  }

  removeFromSalesSummaryBySSList(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log('Welcome to AddPolishToSalesPage:removeFromSalesSummaryBySSList() ');
    this.salesSummaryByTSSList.splice(this.salesSummaryByTSSList.indexOf(salesSummaryByTSS),1);
    this.calculateInventoryParameters();
    this.calculateSummaryOfSaleable();                                                
  }

  selectShape(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Show alert for Shape " + this.itemShapeList.length);
    let alert = this.alertCtrl.create();
    for(let i=0; i<this.itemShapeList.length;i++)
    {
      alert.addInput({type: 'radio',
                    label: this.itemShapeList[i].name,
                    value: this.itemShapeList[i].id,
                    checked: false});
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        let selectedShapeList:Array<ItemShape> = [];
        selectedShapeList = this.itemShapeList.filter(itemShape => itemShape.id === data);
        if(selectedShapeList.length === 1)
        {
          salesSummaryByTSS.itemShape = selectedShapeList[0];
        }
        this.selectSieve(salesSummaryByTSS);    
      }
    });
    alert.present();
  }

  selectSieve(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Show alert for Sieve " + this.itemSieveList.length);
    let alert = this.alertCtrl.create();
    let itemSieveList = this.itemSieveList.filter(itemSieve => itemSieve.itemTypeID === salesSummaryByTSS.itemType.id);
    for(let i=0; (i<itemSieveList.length-1);i++)
    {
      //if(this.itemSieveList[i].itemType === salesSummaryByTSS.itemType)
      {
        alert.addInput({type: 'radio',
                    label: "+"+itemSieveList[i].name+" -"+itemSieveList[i+1].name,
                    value: itemSieveList[i].id,
                    checked: false});
      }        
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        let selectedSieveList:Array<ItemSieve> = [];
        selectedSieveList = itemSieveList.filter(itemSieve => itemSieve.id === data);
        
        if(selectedSieveList.length === 1)
        {
          salesSummaryByTSS.itemSieveMin = selectedSieveList[0];
          let indexMin:number = itemSieveList.indexOf(salesSummaryByTSS.itemSieveMin);
          salesSummaryByTSS.itemSieveMax = itemSieveList[indexMin+1];
          console.log("indexMin " + indexMin);
          /*selectedSieveList = itemSieveList.filter(itemSieve => itemSieve.rank === (salesSummaryByTSS.itemSieveMin.rank+1));
          console.log("itemSieveList.length " + itemSieveList.length);
          console.log("itemSieveMax.rank" + `(salesSummaryByTSS.itemSieveMin.rank+1)`);          
          console.log("selectedSieveList.length " + selectedSieveList.length);
          if(selectedSieveList.length === 1)
          {
            salesSummaryByTSS.itemSieveMax = selectedSieveList[0];
          }
          */            
        }
        for(let i=0;i<this.itemQualityLevelList.length;i++)
        {
          if(this.itemQualityLevelList[i].itemTypeID === salesSummaryByTSS.itemType.id)
          {
            let salesItem = new SalesItem();
            salesItem.itemType = salesSummaryByTSS.itemType;
            salesItem.itemShape = salesSummaryByTSS.itemShape;
            salesItem.itemSieve = salesSummaryByTSS.itemSieveMin;
            salesItem.itemQualityLevel = this.itemQualityLevelList[i]; 
            salesItem.avgPriceRate = this.getMarketRateForItem(salesItem); 
                    
            salesSummaryByTSS.salesItemList.push(salesItem);
          }            
        }
        this.salesSummaryByTSSList.push(salesSummaryByTSS); 
        this.calculateSummaryOfSaleable();       
      }
    });
    alert.present();
  }

  addSalesItems(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Welcome to AddPolishToSalesPage:addSalesItems() " + salesSummaryByTSS.salesItemList.length);
    
    const modal = this.modalCtrl.create('AddPolishPage',{User:this.user,fromEntity:"RP-W",qualityLevelList:this.itemQualityLevelList,salesItemList:salesSummaryByTSS.salesItemList});
    modal.present();
    modal.onDidDismiss(() => {
      this.calculateInventoryParameters();
  //    this.calculateSalesSummaryByTSS(salesSummaryByTSS);
      this.calculateSummaryOfSaleable();  
    }); 
    //polishedItemsSummary.polishedItemList
  }

  calculateSalesSummaryByTSS(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Welcome to SalesTransactionItemsPage::calculateSalesSummaryByTSS() ");
    //console.log(salesSummaryByTSS.itemType + " " + salesSummaryByTSS.itemShape + " " + salesSummaryByTSS.itemSieveMin + " " + salesSummaryByTSS.itemSieveMax);
    //doInitialize(salesSummaryByTSS.salesSummary);
    salesSummaryByTSS.initialize();
    
    for(let i=0;i<salesSummaryByTSS.salesItemList.length;i++)
    {
      //console.log(salesSummaryByTSS.salesItemList[i].itemType + " " + salesSummaryByTSS.salesItemList[i].itemShape + " " + salesSummaryByTSS.salesItemList[i].itemSieve.name);  
      if((this.inventoryParams.avgMarketRateForSaleable) && (salesSummaryByTSS.salesItemList[i].saleableWeight))
      {
        salesSummaryByTSS.salesItemList[i].valueFactor = round(salesSummaryByTSS.salesItemList[i].avgPriceRate/this.inventoryParams.avgMarketRateForSaleable,10);
      }
      else
      {
        salesSummaryByTSS.salesItemList[i].valueFactor = 0.0;
      }
      salesSummaryByTSS.salesItemList[i].avgPurchaseCost = round(this.inventoryParams.purchaseCostPerSaleableUnit*salesSummaryByTSS.salesItemList[i].valueFactor,0);
      salesSummaryByTSS.salesItemList[i].avgInterestCost = round(this.inventoryParams.interestCostPerSaleableUnit*salesSummaryByTSS.salesItemList[i].valueFactor,0);
      if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.POLISHED)
      {
        salesSummaryByTSS.salesItemList[i].avgMfgCost = this.inventoryParams.avgMfgCostForPolished;
      }
      else if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.ROUGH)
      {
        salesSummaryByTSS.salesItemList[i].avgMfgCost = this.inventoryParams.avgMfgCostForRough;
      }
      else if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.TOPS)
      {
        salesSummaryByTSS.salesItemList[i].avgMfgCost = this.inventoryParams.avgMfgCostForTops;
      }
      if(salesSummaryByTSS.salesItemList[i].valueFactor)
      {
        console.log(salesSummaryByTSS.salesItemList[i].valueFactor);
        console.log(salesSummaryByTSS.salesItemList[i].avgPurchaseCost + " " + salesSummaryByTSS.salesItemList[i].avgInterestCost + " " + salesSummaryByTSS.salesItemList[i].avgMfgCost);  
      }
      salesSummaryByTSS.salesItemList[i].avgGrossCost = round(+salesSummaryByTSS.salesItemList[i].avgPurchaseCost + salesSummaryByTSS.salesItemList[i].avgInterestCost + salesSummaryByTSS.salesItemList[i].avgMfgCost,10);
      //salesSummaryByTSS.salesItemList[i].avgPriceRate = this.getMarketRateForItem(salesSummaryByTSS.salesItemList[i]);   
      
      salesSummaryByTSS.salesItemList[i].qty = 0;
      salesSummaryByTSS.salesItemList[i].doTotal(10);
      
      salesSummaryByTSS.totalSaleableWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.POLISHED)
      {
        salesSummaryByTSS.totalSaleablePolishedWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      }
      else if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.ROUGH)
      {
        salesSummaryByTSS.totalSaleableRoughWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      }
      else if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.TOPS)
      {
        salesSummaryByTSS.totalSaleableTopsWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      }
      //salesSummaryByTSS.salesSummary.totalPurchaseWeight += +salesSummaryByTSS.salesItemList[i].purchaseWeight;
      salesSummaryByTSS.totalQty += +salesSummaryByTSS.salesItemList[i].qty;
      salesSummaryByTSS.totalPriceValue += salesSummaryByTSS.salesItemList[i].totalPriceValue;
      salesSummaryByTSS.totalPurchaseCost += +round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgPurchaseCost,2);
      salesSummaryByTSS.totalInterestCost += +round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgInterestCost,2);
      salesSummaryByTSS.totalMfgCost += +round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgMfgCost,2);
      salesSummaryByTSS.totalGrossCost += +round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgGrossCost,2);
      
    }
    //doRound(salesSummaryByTSS.salesSummary,10);
    salesSummaryByTSS.doRound(10);
    //doAverage(salesSummaryByTSS.salesSummary,10);
    salesSummaryByTSS.doAverage(10);

    if(this.inventoryParams.avgMarketRateForSaleable)
    {
      salesSummaryByTSS.avgValueFactor = round(salesSummaryByTSS.avgPriceRate/this.inventoryParams.avgMarketRateForSaleable,10);
    }
    else
    {
      salesSummaryByTSS.avgValueFactor = 0.0;
    }
    
  }
  /*
  calculateSalesSummaryByTSS(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Welcome to AddPolishToSalesPage::calculatePolishSummaryByTSS()");
    salesSummaryByTSS.salesSummary.totalSaleableWeight = 0;
    salesSummaryByTSS.salesSummary.totalPurchaseWeight = 0;
    salesSummaryByTSS.salesSummary.totalQty = 0;
    salesSummaryByTSS.salesSummary.totalPriceValue = 0;
    salesSummaryByTSS.salesSummary.avgPriceRate = 0;
    salesSummaryByTSS.salesSummary.avgCostFactor = 0;
  
    for(let i=0;i<salesSummaryByTSS.salesItemList.length;i++)
    {
      salesSummaryByTSS.salesItemList[i].avgPriceRate = this.getMarketRateForItem(salesSummaryByTSS.salesItemList[i]);
      salesSummaryByTSS.salesItemList[i].totalPriceValue = round((+salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgPriceRate),2);
      salesSummaryByTSS.salesItemList[i].qty = 0;

      salesSummaryByTSS.salesSummary.totalSaleableWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      salesSummaryByTSS.salesSummary.totalPurchaseWeight += +salesSummaryByTSS.salesItemList[i].purchaseWeight;
      salesSummaryByTSS.salesSummary.totalQty += +salesSummaryByTSS.salesItemList[i].qty;
      salesSummaryByTSS.salesSummary.totalPriceValue += salesSummaryByTSS.salesItemList[i].totalPriceValue;
      salesSummaryByTSS.salesSummary.avgCostFactor += round((salesSummaryByTSS.salesItemList[i].costFactor*salesSummaryByTSS.salesItemList[i].saleableWeight),2);
    }
    
    salesSummaryByTSS.salesSummary.totalSaleableWeight = round(salesSummaryByTSS.salesSummary.totalSaleableWeight,2);
    salesSummaryByTSS.salesSummary.totalPurchaseWeight = round(salesSummaryByTSS.salesSummary.totalPurchaseWeight,2);
    salesSummaryByTSS.salesSummary.totalQty = round(salesSummaryByTSS.salesSummary.totalQty,2);
    salesSummaryByTSS.salesSummary.totalPriceValue = round(salesSummaryByTSS.salesSummary.totalPriceValue,2);
    
    if(salesSummaryByTSS.salesSummary.totalSaleableWeight > 0)
    {
      salesSummaryByTSS.salesSummary.avgPriceRate = round(salesSummaryByTSS.salesSummary.totalPriceValue/salesSummaryByTSS.salesSummary.totalSaleableWeight,2);
      salesSummaryByTSS.salesSummary.avgCostFactor = round(salesSummaryByTSS.salesSummary.avgCostFactor/salesSummaryByTSS.salesSummary.totalSaleableWeight,2);
    }
    else
    {
      salesSummaryByTSS.salesSummary.avgPriceRate = 0;
      salesSummaryByTSS.salesSummary.avgCostFactor = 0;
    }
    this.calculateSummaryOfSaleable();  
    this.calculateGain();  
  }
  */

  async submit()
  {
    console.log("Welcome to AddPolishToSalesPage:submit() ");
    let packagedItemBtoCList:Array<PackagedItem>;
    packagedItemBtoCList = [];
    for(let i=0;i<this.salesSummaryByTSSList.length;i++)
    {
      for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
      {
        if(this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight > 0)
        {
          let salesItem = new SalesItem();
          salesItem = this.salesSummaryByTSSList[i].salesItemList[j];
          let packagedItemBtoC = {} as PackagedItem;
      
        //  packagedItemBtoC.itemRemarks = (salesItem.remarks);
          //(02) Set item Details and packageID related fields                            
          packagedItemBtoC.itemQty = +(0);
          packagedItemBtoC.itemWeight = +(salesItem.saleableWeight);
          packagedItemBtoC.itemType = salesItem.itemType;
          packagedItemBtoC.itemValuation = {} as ItemValuation;

          packagedItemBtoC.confidentialityLevel = this.confidentialityLevel;

          packagedItemBtoC.trxID = this.pItem.trxID;
          packagedItemBtoC.trxDate = this.pItem.trxDate;
          packagedItemBtoC.trxPartyID = this.pItem.trxPartyID;
          packagedItemBtoC.trxPartyName = this.pItem.trxPartyName;
          packagedItemBtoC.trxWeight = this.pItem.trxWeight;
          packagedItemBtoC.trxRate = this.pItem.trxRate;
          packagedItemBtoC.trxCurrency = this.pItem.trxCurrency;
          //packagedItemBtoC.trxShare = 0;
          packagedItemBtoC.trxShare = round(((+salesItem.saleableWeight/+this.totalSalesSummary.totalSaleableWeight)*(+this.pItem.trxShare)),2);
          
          if(salesItem.itemType.id === myConstants.itemType.ROUGH)
          {
            packagedItemBtoC.roughWeight = salesItem.saleableWeight;
            packagedItemBtoC.taskCost = this.inventoryParams.avgMfgCostForRough;//0
            packagedItemBtoC.accumulatedTaskCost = packagedItemBtoC.taskCost;
          }
          else if(salesItem.itemType.id === myConstants.itemType.TOPS)
          {
            if(this.inventoryParams.avgMfgGain)
            {
              packagedItemBtoC.roughWeight = round(((packagedItemBtoC.itemWeight/this.inventoryParams.avgMfgGain)*100),2);
            }
            packagedItemBtoC.taskCost = this.inventoryParams.avgMfgCostForPolished;//0
            packagedItemBtoC.accumulatedTaskCost = packagedItemBtoC.taskCost;
          }
          else
          {
            if(this.inventoryParams.avgMfgGain)
            {
              packagedItemBtoC.roughWeight = round(((packagedItemBtoC.itemWeight/this.inventoryParams.avgMfgGain)*100),2);
            }
            packagedItemBtoC.taskCost = round(((this.inventoryParams.avgMfgCostForPolished)*(packagedItemBtoC.itemWeight)),2);
            packagedItemBtoC.accumulatedTaskCost = packagedItemBtoC.taskCost;
          }
          
          
            
          if(this.pItem.pRule.bIncursCost)
          {
            packagedItemBtoC.parentID = this.pItem.parentID;
          }
          else
          {
            packagedItemBtoC.parentID = this.pItem.id;
          }
          
          packagedItemBtoC.packageID = "";        
          packagedItemBtoC.packageGroupID = this.pItem.packageGroupID
          packagedItemBtoC.packageGroupName = this.pItem.packageGroupName;
          
          //(03.1) Set package rule
          let pRuleList:Array<PackageRule> = [];
          pRuleList = await this.settingsProvider.getPackageRule_For_AB(this.user,this.pItem.pRule.nextTaskCode,myConstants.taskCode.SALES);
          packagedItemBtoC.pRule = pRuleList[0];
          packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
          //(03.2) set owner details                              
          packagedItemBtoC.packageCurTaskOwner = this.pItem.packageNextTaskOwner;
          packagedItemBtoC.packageNextTaskOwner = this.pItem.packageNextTaskOwner;
          
          //packagedItemBtoC.itemShape = {} as ItemShape;
          /*
          */
          let iType:Array<ItemShape> = [];
          let iShape:Array<ItemShape> = [];
          let iSieve:Array<ItemSieve> = [];
          let iQualityLevel:Array<ItemQualityLevel> = [];

          iType = this.itemShapeList.filter(type=>type.id === salesItem.itemType.id);
          iShape = this.itemShapeList.filter(shape=>shape.id === salesItem.itemShape.id);
          iSieve = this.itemSieveList.filter(sieve=>(sieve.id === salesItem.itemSieve.id) && (sieve.itemTypeID === salesItem.itemType.id));
          iQualityLevel = this.itemQualityLevelList.filter(qualityLevel=>(qualityLevel.id === salesItem.itemQualityLevel.id) && (qualityLevel.itemTypeID === salesItem.itemType.id));
          console.log("salesItem.itemSieve.name " + salesItem.itemSieve.name);
          console.log("iSieve.length " + iSieve.length);
          if(iType.length)
          {
            packagedItemBtoC.itemType = iType[0];          
          }
          if(iShape.length)
          {
            packagedItemBtoC.itemShape = iShape[0];          
          }
          if(iSieve.length)
          {
            packagedItemBtoC.itemSieve = iSieve[0];          
          }
          if(iQualityLevel.length)
          {
            packagedItemBtoC.itemQualityLevel = iQualityLevel[0];          
          }
          
          packagedItemBtoCList.push(packagedItemBtoC);       
        }
      }
    }
    console.log("packagedItemBtoCList.length " + packagedItemBtoCList.length);
    this.presentLoading();

    //(04) add new entry B->C with RD status to DB
    return this.inventoryProvider.createPackagedItemArray_BtoC(this.user,packagedItemBtoCList,this.pItem)
    .then(()=>{
      console.log("B->C added");
      this.loading.dismiss();        
      this.navCtrl.pop();
    }).catch(e=>{
      console.log("Error in this.inventoryProvider.createPackagedItemArray_BtoC" + e);
    });
  }
  
  selectBusinessTransaction()
  {
    console.log("Welcome to AddPolishToSalesPage::selectBusinessTransaction() ");
    const modal = this.modalCtrl.create('SelectBusinessTransactionPage',{User:this.user});
    modal.present();
    modal.onDidDismiss((returnValue) => {
      this.businessTransaction = returnValue.selectedBizTrx;
      this.totalInputWeight = this.businessTransaction.trxWeight;
      if((this.pItem) && (this.pItem.pRule.curTaskCode === myConstants.taskCode.MANUAL))
      {
        this.pItem.trxID = this.businessTransaction.ID;
        this.pItem.trxDate = this.businessTransaction.trxDate; 
        this.pItem.trxPartyID = this.businessTransaction.trxPartyID;
        this.pItem.trxPartyName = this.businessTransaction.trxPartyName;
        this.pItem.trxWeight = this.businessTransaction.trxWeight;
        this.pItem.trxRate = this.businessTransaction.trxRate;
        this.pItem.trxCurrency = this.businessTransaction.trxCurrency;
      }
      this.calculatePurchaseCostAndInterest();
    });
    
  }

  changeInRoughWeight()
  {
    if((this.pItem) && (this.pItem.pRule.curTaskCode === myConstants.taskCode.MANUAL) && (this.businessTransaction))
    {
      this.pItem.packageGroupID = ""+this.businessTransaction.trxWeight + "/" + this.roughWeight;
      this.pItem.packageGroupName = "";
    }
    this.calculateInventoryParameters();
  //  this.calculateSummaryOfSaleable();
  }

  changeInAvgMfgCost()
  {
    this.calculateCostPerSaleableUnit();
  }

  async calculatePurchaseCostAndInterest()
  {
    console.log("Welcome to SalesStockListPage::calculatePurchaseCostAndInterest() ");
    if(this.businessTransaction)
    {
      //this.partyProfile = await this.entityProvider.getBusinessContact(this.user,this.pItem.trxPartyID);
      
      this.finTrxList = await this.transactionProvider.getFinanceTransactionsListForBizTrxID(this.user,this.businessTransaction.ID);
    }
    if((this.finTrxList.length > 0) && (this.businessTransaction))
    {
      let bizTrxSummary = {} as BusinessTransactionSummary;
      let totalPurchaseWeight:number = this.businessTransaction.trxWeight;
      
      bizTrxSummary = getBusinessTransactionSummary(this.businessTransaction,this.finTrxList,this.currencyExchangeRate);
      this.trxAvgPurchaseCost = 0;
      this.trxAvgInterestCost = 0;
      
      if(bizTrxSummary)
      {
        console.log("bizTrxSummary.amtReceviedOrPaidinLocal " + bizTrxSummary.amtReceviedOrPaidinLocal);
        console.log("bizTrxSummary.interestRealizedinLocal " + bizTrxSummary.interestRealizedinLocal);
        console.log("bizTrxSummary.amtPendinginLocal " + bizTrxSummary.amtPendinginLocal);
        console.log("bizTrxSummary.interestUnRealizedinLocal " + bizTrxSummary.interestUnRealizedinLocal);
        this.trxAvgPurchaseCost += round((+bizTrxSummary.amtReceviedOrPaidinLocal + +bizTrxSummary.amtPendinginLocal),2);
        this.trxAvgInterestCost += round((+bizTrxSummary.interestRealizedinLocal + +bizTrxSummary.interestUnRealizedinLocal),2);
      }
      console.log("AVG:this.trxAvgPurchaseCost/totalPurchaseWeight " + this.trxAvgPurchaseCost + "/" + totalPurchaseWeight);
      this.trxAvgPurchaseCost = round((this.trxAvgPurchaseCost/totalPurchaseWeight),2);
      this.trxAvgInterestCost = round((this.trxAvgInterestCost/totalPurchaseWeight),2);

      this.calculateCostPerSaleableUnit();
    }
  }

  calculateCostPerSaleableUnit()
  {
    console.log("Welcome to AddPolishToSalesPage::calculateCostPerSaleableUnit() ");
    this.inventoryParams.purchaseCostPerSaleableUnit = 0.0;
    this.inventoryParams.interestCostPerSaleableUnit = 0.0;
    
    if((this.inventoryParams.avgP2SGain) && (this.trxAvgPurchaseCost))
    {
      this.inventoryParams.purchaseCostPerSaleableUnit = Math.ceil((this.trxAvgPurchaseCost/this.inventoryParams.avgP2SGain)*100);
      this.inventoryParams.interestCostPerSaleableUnit = Math.ceil((this.trxAvgInterestCost/this.inventoryParams.avgP2SGain)*100); 
    }    
    
    console.log("trxAvgPurchaseCost " + this.trxAvgPurchaseCost);
    console.log("trxAvgInterestCost " + this.trxAvgInterestCost);
    console.log("purchaseCostPerSaleableUnit " + this.inventoryParams.purchaseCostPerSaleableUnit);
    console.log("interestCostPerSaleableUnit " + this.inventoryParams.interestCostPerSaleableUnit);
    
    this.calculateSummaryOfSaleable();
  }
  
  calculateInventoryParameters():void
  {
    console.log("Welcome to AddPolishToSalesPage:calculateInventoryParameters() ");
    
    this.inventoryParams.avgP2SGain = 0.0;
    this.inventoryParams.avgMfgGain = 0.0;
    
    this.inventoryParams.totalSaleablePolishedWeight = 0.0;
    this.inventoryParams.totalSaleableRoughWeight = 0.0;
    this.inventoryParams.totalSaleableTopsWeight = 0.0;
    this.inventoryParams.totalSaleableWeight = 0.0;
    this.inventoryParams.totalPurchaseWeight = 0.0;
    
    this.inventoryParams.totalMarketValueForSaleable = 0.0;
    this.inventoryParams.avgMarketRateForSaleable = 0.0; 
    
    this.inventoryParams.avgMfgCostForPolished = 0.0;
    this.inventoryParams.avgMfgCostForRough = 0.0;       
    this.inventoryParams.avgMfgCostForTops = 0.0;       
    
    if(this.roughWeight > 0)
    {
      this.inventoryParams.totalPurchaseWeight = this.roughWeight;

      let marketRate:number = 0;
      for(let i=0;i<this.salesSummaryByTSSList.length;i++)
      {
        for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
        {
          if(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id === myConstants.itemType.POLISHED)
          {
            this.inventoryParams.totalSaleablePolishedWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
          }
          else if(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id === myConstants.itemType.ROUGH)
          {
            this.inventoryParams.totalSaleableRoughWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
          }
          else if(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id === myConstants.itemType.TOPS)
          {
            this.inventoryParams.totalSaleableTopsWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
          }
          this.inventoryParams.totalSaleableWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
          
          marketRate = this.getMarketRateForItem(this.salesSummaryByTSSList[i].salesItemList[j]);
          this.inventoryParams.totalMarketValueForSaleable += round(marketRate*this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight,0);
        }
      }
    }
    
    if(this.inventoryParams.totalPurchaseWeight > 0)
    {
      //this.avgMfgCostForSaleable = round((this.avgMfgCostForSaleable/totalPolishedWeight),10);
      this.inventoryParams.avgP2SGain = round((this.inventoryParams.totalSaleableWeight/this.inventoryParams.totalPurchaseWeight)*100,2);
      
      let totalMfgInput:number = +this.inventoryParams.totalPurchaseWeight - +this.inventoryParams.totalSaleableRoughWeight;
      let totalMfgOutput:number = +this.inventoryParams.totalSaleablePolishedWeight + +this.inventoryParams.totalSaleableTopsWeight;
      /*
      console.log("totalMfgInput " + totalMfgInput);
      console.log("totalMfgOutput " + totalMfgOutput);
      console.log("totalSaleablePolishedWeight " + this.inventoryParams.totalSaleablePolishedWeight);
      console.log("labourRate " + this.labourRate);
      */
      if(totalMfgInput)
      {
        this.inventoryParams.avgMfgGain = round((totalMfgOutput/totalMfgInput)*100,2);
      }

      if(this.inventoryParams.totalSaleableWeight)
      {
        this.inventoryParams.avgMfgCostForPolished = round((this.labourRate*totalMfgInput)/this.inventoryParams.totalSaleablePolishedWeight,0);
        this.inventoryParams.avgMfgCostForRough = 0;
        this.inventoryParams.avgMfgCostForTops = 0;
        this.inventoryParams.avgMarketRateForSaleable = round(this.inventoryParams.totalMarketValueForSaleable/this.inventoryParams.totalSaleableWeight,10);
      }
    }    
    //console.log("this.avgP2SGain " + this.inventoryParams.avgP2SGain);
    this.calculateCostPerSaleableUnit();
  }  

  presentLoading()
  {
    this.loading = this.loadingCtrl.create();
    this.loading.present();
  }
}
