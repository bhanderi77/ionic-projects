import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPolishToSalesPage } from './add-polish-to-sales';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AddPolishToSalesPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPolishToSalesPage),
    PipesModule
  ],
})
export class AddPolishToSalesPageModule {}
