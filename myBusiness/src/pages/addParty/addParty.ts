import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { Loading,LoadingController,AlertController } from 'ionic-angular';
//import { DatabaseProvider } from '../../providers/database/database';
//import { TransactionProvider } from '../../providers/transaction/transaction';
import { EntityProvider } from '../../providers/entity/entity';

import { SettingsProvider } from '../../providers/settings/settings';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
//import { min } from 'rxjs/operator/min';
//import { HomePage } from '../home/home';
//import { partyProfile, Global, States } from '../../model/interface';
import { BusinessContact } from '../../model/businessContact';
import { StateRegion } from '../../model/referenceData';

import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { User, checkIfValid } from '../../model/user';

/**
 * Generated class for the AddpartyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageAddParty',
  templateUrl: 'addParty.html',
})
export class AddPartyPage {

  private formGroup: FormGroup;
  
  private party: BusinessContact;
  private editParty : BusinessContact; 

  private fullName: AbstractControl;
  private category: AbstractControl;
  private phone1: AbstractControl;
  private email1: AbstractControl;
  private ROIForEarlyPayment: AbstractControl;
  private ROIForLatePayment: AbstractControl;
  private addressLine1: AbstractControl;
  private addressLine2: AbstractControl;
  private city: AbstractControl;
  private pincode: AbstractControl;
  private stateRegion: AbstractControl;
  private GSTIN: AbstractControl;
  private PAN: AbstractControl;
  private Brokerage: AbstractControl;
  
  private allStates: Array<StateRegion>;
  private id:string;

  private phoneNumber: any;
  private partyName: any;
  private user:User;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private settingsProvider: SettingsProvider, 
    private entityProvider: EntityProvider,
    private viewCtrl: ViewController,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    private platform: Platform,
    private contacts: Contacts) 
  {
    //this.editParty={id:'',creation_time:'',server_time:'',userID:'',organisationID:'',fullName:'',category:'Buyer',phone_1:'',email_1:'',ROIForEarlyPayment:1.5,ROIForLatePayment:1.5,addressLine1:'',addressLine2:'',city:'',pincode:'',stateRegion:'',GSTIN:'',PAN:'',confidentialityLevel:0}; // HB27/1/2018
    this.editParty = {} as BusinessContact;
    this.editParty.id = "";
    this.id = "";
    this.id = navParams.get('ID');
    this.partyName = navParams.get('partyName');  //RB13052018
    this.phoneNumber = navParams.get('phoneNumber');  //RB13052018
    
    this.user = {} as User;
    this.user = navParams.get('User');  
    console.log("id : " + this.id);
    this.formGroup = formBuilder.group({
      fullName:['',Validators.required],
      category:['',Validators.required],
      phone1:['',Validators.required], //Validators.compose([Validators.minLength(10),Validators.maxLength(10),
      email1:[''],
      ROIForEarlyPayment:['',Validators.compose([Validators.min(0)])],
      ROIForLatePayment:['',Validators.compose([Validators.min(0)])],
      //Brokerage:['',Validators.compose([Validators.min(0)])],
      addressLine1:['',Validators.required],
      addressLine2:['',Validators.required],
      city:['',Validators.required],
      pincode:['',Validators.required],
      stateRegion:['',Validators.required], 
      GSTIN:['',Validators.required],
      PAN:['',Validators.required]      
    });

      
    this.fullName = this.formGroup.controls['fullName'];
    this.category = this.formGroup.controls['category'];
    this.phone1 = this.formGroup.controls['phone1'];
    this.email1 = this.formGroup.controls['email1'];
    this.ROIForEarlyPayment = this.formGroup.controls['ROIForEarlyPayment'];
    this.ROIForLatePayment = this.formGroup.controls['ROIForLatePayment'];
    //this.Brokerage = this.formGroup.controls['Brokerage'];
    this.addressLine1 = this.formGroup.controls['addressLine1'];
    this.addressLine2 = this.formGroup.controls['addressLine2'];
    this.city = this.formGroup.controls['city'];
    this.pincode = this.formGroup.controls['pincode'];
    this.stateRegion = this.formGroup.controls['stateRegion'];
    this.GSTIN = this.formGroup.controls['GSTIN'];
    this.PAN = this.formGroup.controls['PAN'];

    this.party = {} as BusinessContact;
    if(this.id)
    { 
      if((this.user.ID != '') && (this.user.customerID != ''))
      {
        this.entityProvider.getBusinessContact(this.user,this.id) 
        .then(data => {
          this.editParty = data;
        });
      }
    }
    this.getStates();
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad AddpartyPage');
    if(!checkIfValid(this.user))
    {
      alert("Invalid UserID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      this.viewCtrl.dismiss();
    }
  }

  async savePartyProfile() 
  { 
    this.party.fullName = <string> (this.fullName.value);
    this.party.category = <string> (this.category.value);
    this.party.phone_1 = <string> (this.phone1.value);
    this.party.email_1 = <string> (this.email1.value);
    this.party.ROIForEarlyPayment = <number> (this.ROIForEarlyPayment.value);
    this.party.ROIForLatePayment = <number> (this.ROIForLatePayment.value);
    //this.party.Brokerage = <number> (this.Brokerage.value);
    this.party.addressLine1 = <string> (this.addressLine1.value);
    this.party.addressLine2 = <string> (this.addressLine2.value);
    this.party.city = <string> (this.city.value);
    this.party.pincode = <string> (this.pincode.value);
    this.party.stateRegion = <string> (this.stateRegion.value);
    this.party.GSTIN = <string> (this.GSTIN.value);
    this.party.PAN = <string> (this.PAN.value);

    this.party.userID = <string> (this.user.ID);
    this.party.organisationID = <string> (this.user.customerID);
    this.party.confidentialityLevel = 0;

    console.log("Inside addPartyProfile: " + this.party.fullName);
    if(this.id === "")
    { 
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
  
      if((this.user.ID != '') && (this.user.customerID != ''))
      {
        await this.entityProvider.createBusinessContact(this.user,this.party);
        console.log("PartyProfile added successfully");  
        loading.dismiss();
        this.viewCtrl.dismiss();  
      }
      else
      {
        alert("Invalid UserID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
        loading.dismiss();
        this.viewCtrl.dismiss();
      }
    }
    else
    {
      this.party.id = this.id;
      
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();

      if(checkIfValid(this.user))
      {
        await this.entityProvider.updateBusinessContact(this.user,this.party)
        loading.dismiss();
        
        console.log("PartyProfile edited successfully");
        this.viewCtrl.dismiss();  
      }
      else 
      {
        alert("Invalid UserID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
        loading.dismiss();
        this.viewCtrl.dismiss();
      }
      
    }
       
  } 

  getStates(){
    console.log("Inside getStates()");
    this.allStates = [];
    //let gstStates = [];
    this.settingsProvider.getAllStateRegions(this.user).valueChanges()
    .subscribe(stateList => {
      this.allStates = stateList;
      console.log("# of States : " + this.allStates.length);
    });
  }

  goToRootPage(){
    this.viewCtrl.dismiss();
  }

  initContacts()
  {
    if(this.platform.is('android') || this.platform.is('ios'))
    {
      this.contacts.pickContact()
      .then((response: Contact) => { 
        console.log(response); 
        if(this.platform.is('android'))   
        {                
          //this.addParty(response.displayName,response.phoneNumbers[0].value);
          this.fullName.setValue(response.displayName);
          this.phone1.setValue(response.phoneNumbers[0].value);
        }
        else if(this.platform.is('ios'))
        {
          //this.addParty(response.name.givenName,response.phoneNumbers[0].value);
          this.fullName.setValue(response.name.givenName);
          this.phone1.setValue(response.phoneNumbers[0].value);
        }
      });
    }
  }

}
