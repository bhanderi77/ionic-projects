import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessTransactionItemsPage } from './business-transaction-items';

@NgModule({
  declarations: [
    BusinessTransactionItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessTransactionItemsPage),
  ],
})
export class BusinessTransactionItemsPageModule {}
