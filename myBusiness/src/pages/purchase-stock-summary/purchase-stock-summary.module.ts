import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PurchaseStockSummaryPage } from './purchase-stock-summary';

@NgModule({
  declarations: [
    PurchaseStockSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(PurchaseStockSummaryPage),
  ],
})
export class PurchaseStockSummaryPageModule {}
