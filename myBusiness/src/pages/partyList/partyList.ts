import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ModalController } from 'ionic-angular';
import { Contacts, Contact} from '@ionic-native/contacts';
import { FinanceSettings } from '../../model/referenceData';
import { User, checkIfValid } from '../../model/user';
import { AuthProvider } from '../../providers/auth/auth';
import { EntityProvider } from '../../providers/entity/entity';

import { BusinessContact } from '../../model/businessContact';
import { CallNumber } from '@ionic-native/call-number';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { myConstants } from '../../model/myConstants';
/**
 * Generated class for the PartyListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-party-list',
  templateUrl: 'partyList.html',
})
export class PartyListPage {

  private financeSettings: FinanceSettings;
  private user:User;
  private partyProfiles:Array<BusinessContact>;
  private filteredPartyProfiles:Array<BusinessContact>;	
  private partyCategory: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private platform: Platform, private contacts: Contacts,
    private modalCtrl: ModalController, private authProvider: AuthProvider,
    private entityProvider: EntityProvider,private call : CallNumber,
    private alertCtrl: AlertController) 
  {

    this.user = {} as User; 
    this.user = this.navParams.get('User');
    this.partyCategory ='';
    this.partyCategory = this.navParams.get('partyCategory');
    this.partyProfiles = [];
    this.filteredPartyProfiles = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartyListPage');
  }

  ionViewWillEnter()
  {
    console.log('ionViewWillEnter PartyListPage');
    if(checkIfValid(this.user) && this.authProvider.getAuthenticated())
    { 
      this.financeSettings = this.navParams.get('financeSettings');
      this.loadPartyList();  
    }
  }


  //RB13052018
  addParty() {
    console.log('addParty::partyName : ');
    console.log('addParty::phoneNumber : ');
    const modal = this.modalCtrl.create('AddPartyPage',{User:this.user,ID:""});
    modal.present();
    console.log("inside addParty method");
    modal.onDidDismiss((data) => {
      this.loadPartyList();
    });    
  }

  loadPartyList()
  {
    this.entityProvider.getAllBusinessContactList(this.user)
    .then(contactList => {
      this.partyProfiles = contactList;
      this.filterPartyProfilesList();
    });

  }

  async callNumber(number:any):Promise<any>{
    try{
      await this.call.callNumber(String(number),true);
    }
    catch(e){
      console.error(e);
    }
  }

  editOrDeletePartyProfile(p:BusinessContact){          // HB27/1/2018
    let name = p.fullName;
    let prompt = this.alertCtrl.create({
      title: name,
      buttons: [
        {
          text: 'Edit',
          handler : () => {
            this.editParty(p);
          }
        },

        {
          text: 'delete',
          handler : () => {
            this.deleteParty(p);
          }
        }
      ]
    });
    prompt.present();
  } 

  editParty(p:BusinessContact){    // HB27/1/2018
    console.log('Edit pressed');
    let modal = this.modalCtrl.create('AddPartyPage',{User:this.user,ID:p.id});
    modal.present();
    modal.onDidDismiss((data) => {
      this.loadPartyList();
    });
  }  // HB27/1/2018

  deleteParty(p:BusinessContact){    // HB28/1/2018
    console.log('Delete pressed');
    let name = p.fullName;
    let id = p.id;
    console.log("Party ID : " + id);
    let confirm = this.alertCtrl.create({
      title: 'Delete ' + name,
      message: 'Do you want to delete ' + name + '?',
      buttons: [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          handler: () => {
           /* this.databaseProvider.deleteSelectedParty(id).then(res=>{
              this.loadPartyDetails();
            })*/
          }
        }
      ]
    });
    confirm.present();
  } 

  goToSummaryPage(p:BusinessContact) {
    console.log('PartySummaryListPage::goToSummaryPage() : ' + p.fullName);
    //this.navCtrl.push('PartySummaryPage',{User:this.user,pSummary: p, orgSettings: this.orgSettings});
    this.navCtrl.push('PartySummaryPage',{User:this.user,partyProfile:p});
  }

  filterButtonClick(){
    console.log("Show alert for filter PartySummaryListPage " );
    let alert = this.alertCtrl.create();
    alert.addInput({type: 'radio',
                  label: 'Buyer',
                  value: myConstants.partyCategory.BUYER,
                  checked: false},
                  );
    alert.addInput({type: 'radio',
                  label: 'Supplier',
                  value: myConstants.partyCategory.SUPPLIER,
                  checked: false},
                  );
    alert.addInput({type: 'radio',
                  label: 'Broker',
                  value: myConstants.partyCategory.BROKER,
                  checked: false},
                  );
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        this.partyCategory = data;
        this.filterPartyProfilesList();  
      }
    });
    alert.present();
  }

  filterPartyProfilesList(){
    console.log("partyCategory " + this.partyCategory);
    if(this.partyCategory === myConstants.partyCategory.BUYER)
    {
      console.log("filteredPartyProfiles::Buyer");   
      console.log("filteredPartyProfiles->Buyer::Length: " + this.partyProfiles.length);
      this.filteredPartyProfiles = this.partyProfiles.filter(partyProfiles => (partyProfiles.category === myConstants.partyCategory.BUYER));
      console.log("filteredPartyProfiles::Buyer::Length: " + this.filteredPartyProfiles.length);
    }
    else if(this.partyCategory === myConstants.partyCategory.SUPPLIER)
    {
      console.log("filteredPartyProfiles::Supplier"); 
      this.filteredPartyProfiles = this.partyProfiles.filter(partyProfiles => (partyProfiles.category === myConstants.partyCategory.SUPPLIER));
      console.log("filteredPartyProfiles::Supplier::Length: " + this.filteredPartyProfiles.length);
    }
    else if(this.partyCategory === myConstants.partyCategory.BROKER)
    {
      console.log("filterPartyTransactions::Broker");
      this.filteredPartyProfiles = this.partyProfiles.filter(partyProfiles => (partyProfiles.category === myConstants.partyCategory.BROKER));
      console.log("filteredPartyProfiles::Broker::Length: " + this.filteredPartyProfiles.length);
    }
    else
    {
      this.filteredPartyProfiles = this.partyProfiles; 
    } 
  }
}
