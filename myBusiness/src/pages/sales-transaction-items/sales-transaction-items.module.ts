import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesTransactionItemsPage } from './sales-transaction-items';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SalesTransactionItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesTransactionItemsPage),
    PipesModule
  ],
})
export class SalesTransactionItemsPageModule {}
