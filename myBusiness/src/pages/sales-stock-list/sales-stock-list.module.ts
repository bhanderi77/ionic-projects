import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesStockListPage } from './sales-stock-list';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SalesStockListPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesStockListPage),
    PipesModule
  ],
})
export class SalesStockListPageModule {}
