import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,ViewController } from 'ionic-angular';
import { FormControl } from '@angular/forms';

import { InventoryProvider } from '../../providers/inventory/inventory';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';

import { ItemType,ItemShape,ItemQualityLevel, ItemSieve } from '../../model/packagedItem';
import { PackagedItem } from '../../model/packagedItem';

import { myConstants, round } from '../../model/myConstants';
import { MarketPrice } from '../../model/marketPrice';

import { User, checkIfValid } from '../../model/user';

import { Subscription } from 'rxjs';

/**
 * Generated class for the SalesStockListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sales-stock-list',
  templateUrl: 'sales-stock-list.html',
})
export class SalesStockListPage {
  private user:User;  
  
  private itemTypeList:Array<ItemType>;
  private itemShapeList:Array<ItemShape>;
  private itemSieveList:Array<ItemSieve>;
  private itemFilteredSieveList:Array<ItemSieve>;
  //private itemRoughSieveList:Array<ItemSieve>;
  private itemQualityLevelList:Array<ItemQualityLevel>;
  private itemFilteredQualityLevelList:Array<ItemQualityLevel>;
  //private itemRoughQualityLevelList:Array<ItemQualityLevel>;

  private iType:ItemType;
  private iShape:ItemShape;
  private iSieveMin:ItemSieve;
  private iSieveMax:ItemSieve;
  private iQualityLevelMin:ItemQualityLevel;
  private iQualityLevelMax:ItemQualityLevel;
  private iSeriesID:string;
  private iSeriesName:string;
  private iTrxPartyName:string;
  private iTrxDate:string;
  private iTrxWeight:number;
  private iTrxRate:number;
  private iTrxCurrency:string;

  private packagedItemList:Array<PackagedItem>;
  private filteredItemList:Array<PackagedItem>;

  private totalWeight:number;
  private mySubscriptions:Subscription;

  private bSearching:boolean;
  private searchInput:string;
  private searchControl:FormControl;
  private searchList:Array<string>;
  private searchSeriesID:string;
  constructor(public navCtrl: NavController, 
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private modalCtrl: ModalController,
    private inventoryProvider: InventoryProvider,
    private transactionProvider: TransactionProvider,
    private settingsProvider: SettingsProvider) 
  {   
    console.log("Welcome to SalesStockListPage::constructor()");
    this.user = {} as User;
    
    this.iType = {} as ItemType;
    this.iType.id = "";
    
    this.searchControl = new FormControl();
    this.searchInput = "";
    this.searchList = [];
    this.bSearching = false;
    this.searchSeriesID = "";
    //this.rangeSieveMin = 0;
    //this.rangeSieveMax = 0;
    this.itemTypeList = [];
    this.itemShapeList = [];
    this.itemSieveList = [];
    this.itemFilteredSieveList = [];
    this.itemQualityLevelList = [];
    this.itemFilteredQualityLevelList = [];
    this.packagedItemList = [];
    this.filteredItemList = [];
    
    this.user = navParams.get('User');
    this.totalWeight = 0;    
  }

  async initializeData()
  {
    console.log("Welcome to SalesStockSummaryPage:initializeData()");
    this.mySubscriptions = new Subscription();

    this.iType = {} as ItemType;
    this.iShape = {} as ItemShape;
    this.iSieveMin = {} as ItemSieve;
    this.iSieveMax = {} as ItemSieve;
    this.iQualityLevelMin = {} as ItemQualityLevel;
    this.iQualityLevelMax = {} as ItemQualityLevel;
    
    this.iType.id = "";
    this.iShape.id = "";
    this.iSieveMin.id = "";
    this.iSieveMax.id = "";
    this.iQualityLevelMin.id = "";
    this.iQualityLevelMax.id = "";
    
    this.iSeriesID = "";
    this.iSeriesName = "";
    this.iTrxPartyName = "";
    this.iTrxDate = '';
    this.iTrxWeight = 0;
    this.iTrxRate = 0;
    this.iTrxCurrency = "";
    
    this.iType = this.navParams.get('iType');
    this.iShape = this.navParams.get('iShape');
    this.iSieveMin = this.navParams.get('iSieveMin');
    this.iSieveMax = this.navParams.get('iSieveMax');
    this.iQualityLevelMin = this.navParams.get('iQualityLevelMin');
    this.iQualityLevelMax = this.navParams.get('iQualityLevelMax');
    
    this.iSeriesID = this.navParams.get('iSeriesID');
    this.iSeriesName = this.navParams.get('iSeriesName');
    this.iTrxPartyName = this.navParams.get('iTrxPartyName');
    this.iTrxDate = this.navParams.get('iTrxDate');
    this.iTrxWeight = this.navParams.get('iTrxWeight');
    this.iTrxRate = this.navParams.get('iTrxRate');
    this.iTrxCurrency = this.navParams.get('iTrxCurrency');
    
    this.itemTypeList = [];
    this.settingsProvider.getItemTypeList(this.user,true)
    .then(pList => {
      this.itemTypeList = pList;
      if(this.itemTypeList.length > 0)
      {
        console.log("this.iType.id " + this.iType.id);
        let i:number=0;
        
        if(this.iType.id)
        {
          for(i=0;i<this.itemTypeList.length;i++)
          {
            if(this.iType.id === this.itemTypeList[i].id)
            {
              this.iType = this.itemTypeList[i];
            }
          }
        }
        else
        {
          this.iType = this.itemTypeList[0];
        }
        this.filterPackagedItem();                  
      }
    });

    this.itemShapeList = [];
    //this.polishShapeList = await this.settingsProvider.getItemShapeList(false);
    this.settingsProvider.getItemShapeList(this.user,true)
    .then(pList => {
      this.itemShapeList = pList;
      if(this.itemShapeList.length > 0)
      {
        console.log("this.iShape.id " + this.iShape.id);
        let i:number=0;
        
        if(this.iShape.id)
        {
          for(i=0;i<this.itemShapeList.length;i++)
          {
            if(this.iShape.id === this.itemShapeList[i].id)
            {
              this.iShape = this.itemShapeList[i];
            }
          }
        }
        else
        {
          this.iShape = this.itemShapeList[0];
        }

        this.filterPackagedItem();                  
      }
    });
    
    this.itemSieveList = [];
    this.iType.id = myConstants.itemType.POLISHED;
    this.itemFilteredSieveList = [];
    
    this.settingsProvider.getItemSieveList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemSieveList = pList;
        this.onItemTypeChange();
      }      
    });
    
    this.itemQualityLevelList = [];
    this.itemFilteredQualityLevelList = [];
    
    this.settingsProvider.getItemQualityLevelList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemQualityLevelList = pList;
        this.onItemTypeChange();
      }      
    });
    
    this.packagedItemList = [];
    this.mySubscriptions.add(this.inventoryProvider.get_packagedItems_For_B(this.user,myConstants.taskCode.SALES).valueChanges().subscribe(pItemList => {
      console.log("valueChanged for get_packagedItems_For_B");
      this.packagedItemList = [];
      this.packagedItemList = pItemList;      
      this.prepareSearchList();
      this.filterPackagedItem();      
    }));    
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad SalesStockListPage');
  }

  ionViewWillEnter() 
  {
    console.log('ionViewWillEnter SalesStockListPage');
    this.initializeData();
  }

  ionViewWillLeave() 
  {
    console.log('ionViewWillLeave SalesStockListPage');
    this.mySubscriptions.unsubscribe();
  }

  onItemTypeChange()
  {
    let i:number=0;
    this.itemFilteredSieveList = this.itemSieveList.filter(itemSieve => itemSieve.itemTypeID === this.iType.id);
    this.itemFilteredQualityLevelList = this.itemQualityLevelList.filter(itemQualityLevel => itemQualityLevel.itemTypeID === this.iType.id);
    
    for(i=0;i<this.itemFilteredSieveList.length;i++)
    {
      if(this.iSieveMin.id === this.itemFilteredSieveList[i].id)
      {
        this.iSieveMin = this.itemFilteredSieveList[i];
      }
      if(this.iSieveMax.id === this.itemFilteredSieveList[i].id)
      {
        this.iSieveMax = this.itemFilteredSieveList[i];
      }
    }
    
    if((this.itemFilteredSieveList.length > 0) && (!this.iSieveMin.id) && (!this.iSieveMax.id))
    {
      this.iSieveMin = <ItemSieve> this.itemFilteredSieveList[0];
      this.iSieveMax = <ItemSieve> this.itemFilteredSieveList[this.itemFilteredSieveList.length-1];
    }

    //if(this.itemFilteredQualityLevelList.length > 0)
    if((this.itemFilteredQualityLevelList.length > 0) && (!this.iQualityLevelMin.id) && (!this.iQualityLevelMax.id))
    {
      this.iQualityLevelMin = this.itemFilteredQualityLevelList[0];
      this.iQualityLevelMax = this.itemFilteredQualityLevelList[this.itemFilteredQualityLevelList.length-1];
    }
    this.filterPackagedItem();    
  }

  filterPackagedItem()
  {
    console.log("Welcome to SalesStockListPage:filterPackagedItem() ");
    /*
    console.log("Selected Shape " + this.iShape.id);
    console.log("Min Sieve " + this.iSieveMin.id +  " " + this.iSieveMin.rank);
    console.log("Max Sieve " + this.iSieveMax.id +  " " + this.iSieveMax.rank);
    console.log("Min Quality Rank " + this.iQualityLevelMin.id +  " " + this.iQualityLevelMin.rank);
    console.log("Max Quality Rank " + this.iQualityLevelMax.id +  " " + this.iQualityLevelMax.rank);
    */
    this.filteredItemList = this.packagedItemList;
    if(this.iType)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.itemType.id === this.iType.id));  
    }
    
    if((this.iShape.id) && (this.iShape.id !== myConstants.itemShape.ALL))
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.itemShape.id === this.iShape.id));
    }
    if((this.iSieveMin.id) && (this.iSieveMax.id))
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => ((pItem.itemSieve.rank >= this.iSieveMin.rank) && (pItem.itemSieve.rank < this.iSieveMax.rank)));
    }

    if(((this.iType.id === myConstants.itemType.ROUGH) || (this.iType.id === myConstants.itemType.TOPS)) && (this.iQualityLevelMin.id))
    {
      this.iQualityLevelMax = this.iQualityLevelMin;
    }
    if((this.iQualityLevelMin.id) && (this.iQualityLevelMax.id))
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => ((pItem.itemQualityLevel.rank >= this.iQualityLevelMin.rank) && (pItem.itemQualityLevel.rank <= this.iQualityLevelMax.rank)));
    }
    
    if(this.iSeriesID)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.packageGroupID === this.iSeriesID));
    }
    if(this.iSeriesName)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.packageGroupName === this.iSeriesName));
    }
    if(this.iTrxPartyName)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxPartyName === this.iTrxPartyName));
    }
    if(this.iTrxDate)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxDate === this.iTrxDate));
    }
    if(this.iTrxWeight)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxWeight === this.iTrxWeight));
    }
    if(this.iTrxRate)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxRate === this.iTrxRate));
    }
    if(this.iTrxCurrency)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxCurrency === this.iTrxCurrency));
    }
      
    console.log("this.filteredItemList.length " + this.filteredItemList.length);

    this.totalWeight = 0;
    for(let i=0;i<this.filteredItemList.length;i++)
    {
      this.totalWeight += +this.filteredItemList[i].itemWeight;
    }
    this.totalWeight = round(this.totalWeight,2);
  }

  onPackageClicked(pSelectedItem:PackagedItem)
  {
    console.log("Welcome to SalesStockListPage:onPackageClicked()");
    console.log("curTaskCode " + pSelectedItem.pRule.curTaskCode);
    console.log("nextTaskCode " + pSelectedItem.pRule.nextTaskCode);
    console.log("id " + pSelectedItem.id);
    this.navCtrl.push('ItemDetailPage',{User:this.user,pItem:pSelectedItem}); 
  }

  prepareSearchList()
  {
    console.log("Welcome to SalesStockListPage:prepareSearchList()");
    this.searchList = [];
    for(let i = 0;i<this.packagedItemList.length;i++)
    {
      let searchItem:string = "";
      searchItem += this.packagedItemList[i].packageGroupID;
      if(this.packagedItemList[i].packageGroupName)
      {
        searchItem += ","+this.packagedItemList[i].packageGroupName;
      }
      
      if(this.packagedItemList[i].trxPartyName)
      {
        searchItem += ","+this.packagedItemList[i].trxPartyName;
      }
      if(this.packagedItemList[i].trxDate)
      {
        searchItem += ","+this.packagedItemList[i].trxDate;
      }
      if((this.packagedItemList[i].trxWeight) && (this.packagedItemList[i].trxRate) && (this.packagedItemList[i].trxCurrency))
      {
        searchItem += ","+this.packagedItemList[i].trxWeight+"ct X "+this.packagedItemList[i].trxRate+this.packagedItemList[i].trxCurrency;
      }
      if(this.searchList.filter(s => s === searchItem).length === 0)
      {//If unique
        this.searchList.push(searchItem);
      }
    }
  }

  onSearch(searchbar)
  {
    console.log("Welcome to SalesStockListPage:onSearch()");
    // console.log('inside onSearch this.SearchInput : ' + this.SearchInput);
    if((this.searchInput.length == 0) || (this.bSearching = false))
    {
      this.searchList = [];
      this.searchSeriesID = "";
      this.iSeriesID = "";
      this.iSeriesName = "";
      this.iTrxPartyName = "";
      this.iTrxDate = "";
      this.iTrxWeight=null;
      this.iTrxRate=null;
      this.iTrxCurrency="";

      this.filterPackagedItem();  
    }
    else
    {
      this.bSearching = true;
  	  this.prepareSearchList();
  	
      this.searchList = this.searchList.filter((item) => {
        if(item.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1)
        {       
          return true;
        }
        else
          return false;
      });
    }
  	
  }

  inputSelected(event,searchItem)
  {
    console.log("Welcome to SalesStockListPage:inputSelected()" + searchItem);
    this.searchList = [];
    this.searchSeriesID = ""+searchItem;
    this.searchInput = searchItem;
    this.bSearching = false;
    
    this.iSeriesID = "";
    this.iSeriesName = "";
    this.iTrxPartyName = "";
    this.iTrxDate = "";
    this.iTrxWeight = null;
    this.iTrxRate = null;
    this.iTrxCurrency = "";

    let weightAndRateAndCurrency:string="";
    let rateAndCurrency:string="";
    let searchParams:Array<string> = [];
    searchParams = this.searchSeriesID.split(",",4);
    if(searchParams.length === 5)
    {
      this.iSeriesID = searchParams[0];
      this.iSeriesName = searchParams[1];
      this.iTrxPartyName = searchParams[2];
      this.iTrxDate = searchParams[3];
      weightAndRateAndCurrency = searchParams[4];
      
      searchParams = weightAndRateAndCurrency.split("ct X ",2);
      this.iTrxWeight = +searchParams[0];
      rateAndCurrency = searchParams[1];//RateCurrency i.e. 530.12USD or 3124INR
      
      this.iTrxRate = +rateAndCurrency.slice(0,this.iTrxCurrency.length-3);
      this.iTrxCurrency = rateAndCurrency.slice(this.iTrxCurrency.length-3);//Get INR from 3134INR
    }
    else if(searchParams.length === 4)
    {
      this.iSeriesID = searchParams[0];
      this.iTrxPartyName = searchParams[1];
      this.iTrxDate = searchParams[2];
      weightAndRateAndCurrency = searchParams[3];
      
      searchParams = weightAndRateAndCurrency.split("ct X ",2);
      this.iTrxWeight = +searchParams[0];
      rateAndCurrency = searchParams[1];//RateCurrency i.e. 530.12USD or 3124INR
      
      this.iTrxRate = +rateAndCurrency.slice(0,this.iTrxCurrency.length-3);
      this.iTrxCurrency = rateAndCurrency.slice(this.iTrxCurrency.length-3);//Get INR from 3134INR
    }
    this.filterPackagedItem();  
  }
  
  getNextSieve(itemSieve:ItemSieve)
  {
    let i:number = 0;
    for(i=0;i<this.itemSieveList.length;i++)
    {
      if((this.itemSieveList[i].itemTypeID === this.iType.id)
      && (this.itemSieveList[i].id === itemSieve.id))
      {
        break;
      }
    }
    if(i < this.itemSieveList.length-1)
      return this.itemSieveList[i+1]
    else
      return itemSieve;
  }

  close()
  {
    this.viewCtrl.dismiss({iShape:this.iShape,iSieveMin:this.iSieveMin,iSieveMax:this.iSieveMax,iQualityLevelMin:this.iQualityLevelMin,iQualityLevelMax:this.iQualityLevelMax}); 
  }

}
