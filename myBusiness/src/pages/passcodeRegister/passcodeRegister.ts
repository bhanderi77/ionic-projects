import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,ViewController } from 'ionic-angular';
import { File } from "@ionic-native/file";
import { FingerprintAIO } from "@ionic-native/fingerprint-aio";
import { encryptPassCode } from '../../model/utility'
/**
 * Generated class for the PasscodeRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-passcode-register',
  templateUrl: 'passcodeRegister.html',
})
export class PasscodeRegisterPage {

  private radioIcon1: boolean;
  private radioIcon2: boolean;
  private radioIcon3: boolean;
  private radioIcon4: boolean;
  private pincode: string;
  private passCodeHeader: string;
  private passCode: string;
  private flag: number;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private file: File,
    private viewCtrl: ViewController,
    private faio: FingerprintAIO) {
      this.radioIcon1 = false;
      this.radioIcon2 = false;
      this.radioIcon3 = false;
      this.radioIcon4 = false;
      this.pincode = '';
      this.passCodeHeader = navParams.get('passCodeHeader');
      this.flag = navParams.get('flag');
      this.passCode = navParams.get('passCode');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PasscodeRegisterPage');
  }

  onPinClick(val:string){
    //this.visible = true;
    if(this.pincode.length<4)
    {
      this.pincode += val;
      console.log('PassCode ' + this.pincode);
    }

    if((this.pincode.length == 4) && (this.flag == 0))
    {
      this.radioIcon4 = true;
      console.log("inside if(this.pincode.length === 4 && this.passCodeHeader === 'Please Enter the Passcode')")
      this.passCodeHeader = 'Please Re-Enter the passcode to Confirm'
      let modal = this.modalCtrl.create('PasscodeRegisterPage',{passCodeHeader: this.passCodeHeader,flag:1,passCode: this.pincode});
      modal.present();
      modal.onDidDismiss((data) => {
        console.log('Inside passCodeRegister::onDidDismiss()');
        if(data === true)
        {
          console.log('Inside passCodeRegister::Dismissing ViewCtrl');
          this.viewCtrl.dismiss(data);
        }
        else{
          alert('Login Error!!!, Please try again!');
          this.viewCtrl.dismiss(false);
        }
      });
    }
    else if((this.pincode.length === 4) && (this.flag == 1))
    {
      this.radioIcon4 = true;
      this.passCodeHeader = 'Please Re-Enter the passcode to Confirm'
      if(this.passCode === this.pincode)
      {
        //call the function to write pincode to the file
        this.faio.show({
          clientId: 'Fingerprint-Demo',
          clientSecret: 'password', //Only necessary for Android
          disableBackup:true,  //Only for Android(optional)
          localizedFallbackTitle: 'Use Pin', //Only for iOS
          localizedReason: 'Please authenticate' //Only for iOS
        })
        .then((res) => {
          console.log('Response :' + res);
          this.writeToFile(this.pincode,'myBizPC.txt');
          this.viewCtrl.dismiss(true);
        }, (err) => {
          console.log(err);    
          this.viewCtrl.dismiss(false); 
        });
      }
      else{
        alert('Incorrect Passcode!!!, Please try again!');
        this.radioIcon4 = false;
        this.radioIcon3 = false;
        this.radioIcon2 = false;
        this.radioIcon1 = false;
        this.pincode = '';
      }
    }
    else
    {
      if((this.radioIcon1 === false) && (this.radioIcon2 === false) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
      {
        this.radioIcon1 = true;
        console.log('Click ' + val);
      }
      else if((this.radioIcon1 === true) && (this.radioIcon2 === false) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
      {
        this.radioIcon2 = true;
        console.log('Click ' + val);
      }
      else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
      {
        this.radioIcon3 = true;
        console.log('Click ' + val);
      }
      else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === true) && (this.radioIcon4 === false))
      {
        this.radioIcon4 = true;
        console.log('Click ' + val);
        console.log('PassCode ' + this.pincode);
      }
    }
  }

  onClearClick()
  {
    if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === true) && (this.radioIcon4 === true))
    {
      this.radioIcon4 = false;
      this.pincode = this.pincode[0] + this.pincode[1] + this.pincode[2];
      console.log('Click ' + this.pincode);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === true) && (this.radioIcon4 === false))
    {
      this.radioIcon3 = false;
      this.pincode = this.pincode[0] + this.pincode[1];
      console.log('Click ' + this.pincode);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
    {
      this.radioIcon2 = false;
      this.pincode = this.pincode[0];
      console.log('Click ' + this.pincode);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === false) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
    {
      this.radioIcon1 = false;
      this.pincode = '';
      console.log('PassCode ' + this.pincode);
    }
  }

  writeToFile(pinCode: string, filename: string){
    console.log('Inside writeToFile()')
    this.file.createFile(this.file.dataDirectory,filename,true)
    .then ((res) => {
      console.log(res);
      let sPassCode = encryptPassCode(pinCode);
      console.log('Ec PassCode : ' + sPassCode);
      this.file.writeExistingFile(this.file.dataDirectory,filename,sPassCode)
      .then((res) => console.log('Write to file successful' + res));
    });
  }
}
