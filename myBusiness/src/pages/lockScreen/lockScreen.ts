import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Platform,ModalController } from 'ionic-angular';
import { FingerprintAIO } from "@ionic-native/fingerprint-aio";
import { File } from "@ionic-native/file";
import { OpenNativeSettings } from '../../../node_modules/@ionic-native/open-native-settings';
import { decryptPassCode } from "../../model/utility";
/**
 * Generated class for the LockScreenPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lock-screen',
  templateUrl: 'lockScreen.html',
})
export class LockScreenPage {

  private radioIcon1: boolean;
  private radioIcon2: boolean;
  private radioIcon3: boolean;
  private radioIcon4: boolean;
  private pincode: string;
  private passCode: string;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    private faio: FingerprintAIO, private viewCtrl: ViewController,
    private platform: Platform, private file: File,
    private modalCtrl: ModalController, 
    private openNativeSettings: OpenNativeSettings) {
      this.radioIcon1 = false;
      this.radioIcon2 = false;
      this.radioIcon3 = false;
      this.radioIcon4 = false;
      this.pincode = '';
      this.passCode = this.navParams.get('passCode');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LockScreenPage');
  }

  ionViewDidEnter() {
    this.faio.isAvailable()
    .then((res) => {
      console.log('Response: ' + res);
      this.verify();
    },
    (err) => {
      alert('Please setup the FingerPrint Authentication first!');
      if(this.platform.is('ios'))
        this.openNativeSettings.open('touch');
      else if(this.platform.is('android'))
        this.openNativeSettings.open('security');
    });
  }

  verify()
  {
    this.faio.show({
      clientId: 'Fingerprint-Demo',
      clientSecret: 'password', //Only necessary for Android
      disableBackup:true,  //Only for Android(optional)
      localizedFallbackTitle: 'Use Pin', //Only for iOS
      localizedReason: 'Please authenticate' //Only for iOS
    })
    .then((res) => {
      console.log('Response :' + res);
      let data = true;
      this.viewCtrl.dismiss(data);
      //this.navCtrl.setRoot('HomePage');
    }, (err) => {
      console.log(err); 
      if(err === 'Error: Optional("Application retry limit exceeded.")')  
      {
        this.viewCtrl.dismiss(false);
      }   
      else if(err === 'Error: Optional("Canceled by user.")')
      {
        console.log('PinCode ' + this.pincode);
        console.log('PassCode ' + this.passCode);
        this.file.checkFile(this.file.dataDirectory,'myBizPC.txt')
          .then((res) => {
            console.log(res + 'File is present');
            this.file.readAsText(this.file.dataDirectory,'myBizPC.txt')
              .then((res) => {
                console.log('Response: ' + res);
                this.passCode = decryptPassCode(res);
                console.log('ReadingFileAsText Completed');
              })
              .catch((err) => {
                console.log(err)
              });
              
          })
          .catch((err) => {
            console.log(err + 'File is not present');
            let modal = this.modalCtrl.create('PasscodeRegisterPage',{passCodeHeader: 'Please Enter the passcode',flag:0});
            modal.present();
          });
      }
    });
  }
  onPinClick(val:string){
    if(this.pincode.length<4)
    {
      this.pincode += val;
      console.log('PassCode ' + this.pincode);
    }

    if((this.radioIcon1 === false) && (this.radioIcon2 === false) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
    {
      this.radioIcon1 = true;
      console.log('Click 1' + val);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === false) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
    {
      this.radioIcon2 = true;
      console.log('Click 2' + val);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
    {
      this.radioIcon3 = true;
      console.log('Click 3' + val);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === true) && (this.radioIcon4 === false))
    {
      this.radioIcon4 = true;
      console.log('Click 4' + val);
      console.log('PassCode ' + this.pincode);
    }

    if(this.pincode === this.passCode)
    {
      this.radioIcon4 = true;
      let data = true;
      this.viewCtrl.dismiss(data);
    }
    else if((this.pincode.length === 4) && (this.pincode != this.passCode))
    {
      this.radioIcon4 = true;
      alert('Incorrect Passcode!!!, Please try again!');
      this.radioIcon4 = false;
      this.radioIcon3 = false;
      this.radioIcon2 = false;
      this.radioIcon1 = false;
      this.pincode = '';
      console.log('radioIcon1 : ' + this.radioIcon1)
    }
  }

  onClearClick()
  {
    if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === true) && (this.radioIcon4 === true))
    {
      this.radioIcon4 = false;
      this.pincode = this.pincode[0] + this.pincode[1] + this.pincode[2];
      console.log('Click 1' + this.pincode);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === true) && (this.radioIcon4 === false))
    {
      this.radioIcon3 = false;
      this.pincode = this.pincode[0] + this.pincode[1];
      console.log('Click 2' + this.pincode);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === true) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
    {
      this.radioIcon2 = false;
      this.pincode = this.pincode[0];
      console.log('Click 3' + this.pincode);
    }
    else if((this.radioIcon1 === true) && (this.radioIcon2 === false) && (this.radioIcon3 === false) && (this.radioIcon4 === false))
    {
      this.radioIcon1 = false;
      this.pincode = '';
      console.log('PassCode ' + this.pincode);
    }
  }
}
