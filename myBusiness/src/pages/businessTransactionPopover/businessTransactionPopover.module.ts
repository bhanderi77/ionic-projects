import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessTransactionPopoverPage } from './businessTransactionPopover';

@NgModule({
  declarations: [
    BusinessTransactionPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessTransactionPopoverPage),
  ],
})
export class PartySummaryPopoverPageModule {}
