import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Loading,LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Network } from '@ionic-native/network';
//import { min } from 'rxjs/operator/min';

//import { DatabaseProvider } from '../../providers/database/database';
//import { AppSettings,businessTransaction,financeTransaction, Global, partyProfile } from '../../model/interface'

import { TransactionProvider } from '../../providers/transaction/transaction';

import { myConstants, round } from '../../model/myConstants';

import { FinanceSettings } from '../../model/referenceData';
import { BusinessContact } from '../../model/businessContact';
import { BusinessTransaction } from '../../model/businessTransaction';
import { FinanceTransaction } from '../../model/financeTransaction';
import { PartySummary,BusinessTransactionSummary } from '../../model/partySummary';
import { User,checkIfValid } from '../../model/user';

/**
 * Generated class for the FinanceTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageFinanceTransaction',
  templateUrl: 'financeTransaction.html',
})
export class FinanceTransactionPage {

  private trxnType: AbstractControl;
  private currType: AbstractControl;
  private trxCurrencyRate: AbstractControl;
  private trxnDate: AbstractControl;
  private amount: AbstractControl;
  //tax: AbstractControl;
  private paymentMethod: AbstractControl;
  private remarks: AbstractControl;
  private formGroup: any;

  private pBusinessContact: BusinessContact;
  private financeSettings:FinanceSettings;

  private myFinTrx : FinanceTransaction;
  //private editFinsTrx: FinanceTransaction;
  private isNewTrxn:boolean;

  private exchangeRate:number;
  private user:User;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, 
    private loadingCtrl: LoadingController,
    public formBuilder: FormBuilder, 
    private transactionProvider: TransactionProvider,
    private network: Network) {

      this.pBusinessContact = navParams.get('party');  //RB2101
      this.isNewTrxn = navParams.get('isNewTrxn');
      this.financeSettings = navParams.get('financeSettings');

      this.user = {} as User;
      this.user = navParams.get('User');
      this.exchangeRate = 1;
      this.myFinTrx = {} as FinanceTransaction;
      console.log("isNewTrxn : " + this.isNewTrxn);
      if(this.pBusinessContact.category === myConstants.partyCategory.SUPPLIER)
      {
        /*
        this.editFinsTrx.trxType = 'Out_Debit';
        this.editFinsTrx.trxCurrency = 'INR';
        this.editFinsTrx.trxPaymentMethod = 'cheq';
        */
        this.formGroup = formBuilder.group({
          trxnType:['Out_Debit',Validators.required],
          currType:['INR',Validators.required], //RB2101
          trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])],
          trxnDate:['',Validators.required],
          amount:['',Validators.compose([Validators.min(0),Validators.required])],
          //tax:['',Validators.compose([Validators.min(0),Validators.required])],
          paymentMethod:['cheq',Validators.required],
          remarks:['']
        });
      }
      else
      {
        /*
        this.editFinsTrx.trxType = 'In_Credit';
        this.editFinsTrx.trxCurrency = 'INR';
        this.editFinsTrx.trxPaymentMethod = 'cheq';
        */
        this.formGroup = formBuilder.group({
          trxnType:['In_Credit',Validators.required],
          currType:['INR',Validators.required], //RB2101
          trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])],
          trxnDate:['',Validators.required],
          amount:['',Validators.compose([Validators.min(0),Validators.required])],
          //tax:['',Validators.compose([Validators.min(0),Validators.required])],
          paymentMethod:['cheq',Validators.required],
          remarks:['']
        });
      }
      this.trxnType = this.formGroup.controls['trxnType'];
      this.currType = this.formGroup.controls['currType'];
      this.trxCurrencyRate = this.formGroup.controls['trxCurrencyRate'];
      this.trxnDate = this.formGroup.controls['trxnDate'];
      this.amount = this.formGroup.controls['amount'];
      //this.tax = this.formGroup.controls['tax'];
      this.paymentMethod = this.formGroup.controls['paymentMethod'];
      this.remarks = this.formGroup.controls['remarks'];
      
      this.myFinTrx.trxPartyID = this.pBusinessContact.id;
      this.myFinTrx.trxPartyName = this.pBusinessContact.fullName; 
      //this.editFinsTrx.trxPartyID = this.party.ID;
      console.log("FinanceTransactionPage for PartyID : " + this.myFinTrx.trxPartyID);
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinanceTransactionPage');
    if(!checkIfValid(this.user))
    {
      alert("Invalid userID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      this.viewCtrl.dismiss();
    }
  }

  goToSummaryPage() {
    this.viewCtrl.dismiss();
  }

  async saveTransaction()
  {
    console.log("Inside financeTransaction::saveTransaction");
    if(this.network.type === 'none')
    {
      alert('The internet connection appears to be offline! Please check your connection');
    }
    else
    {
      let loading: Loading;
      loading = this.loadingCtrl.create();
              
      //let bizOpenTrxs = [<BusinessTransaction>{}];
      let openBizTrx = {} as BusinessTransaction;
      //let finTrxsForPartyID = [<FinanceTransaction>{}];
      let lastFinTrxForBizTrx = {} as FinanceTransaction;
      let today: Date = new Date();

      this.myFinTrx.trxType = <string> this.trxnType.value;
      this.myFinTrx.trxCurrency = <string> this.currType.value;
      
      //this.myFinTrx.trxDate = <Date> this.trxnDate.value;  //<Date>
      this.myFinTrx.trxDate = this.trxnDate.value;  //<Date>

      //if finTrxCurrency -> USD then foriegnValue is same as amount entered by user     
      if(this.myFinTrx.trxCurrency === myConstants.foreignCurrency)
      {
        this.myFinTrx.trxCurrencyRate = <number> parseFloat(this.trxCurrencyRate.value);
        this.myFinTrx.trxLocalValue = <number> parseFloat(this.amount.value) * this.myFinTrx.trxCurrencyRate;  //this.myFinTrx.trxForiegnValue * this.myFinTrx.trxCurrencyRate;
        this.myFinTrx.trxLocalValue = round(this.myFinTrx.trxLocalValue,0);

        this.myFinTrx.trxForiegnValue = <number> parseFloat(this.amount.value);
        this.myFinTrx.trxForiegnValue = round(this.myFinTrx.trxForiegnValue,2);
        
      }
      else //finTrxCurrency is INR; store the same value in ForiegnValue since we don't have Exchange rate; so we will convert this value at runtime
      {
        this.myFinTrx.trxCurrencyRate = this.financeSettings.exchangeRate;
        this.myFinTrx.trxLocalValue = <number> parseFloat(this.amount.value);  
        this.myFinTrx.trxLocalValue = round(this.myFinTrx.trxLocalValue,0);

        this.myFinTrx.trxForiegnValue = <number> parseFloat(this.amount.value)/this.myFinTrx.trxCurrencyRate;
        this.myFinTrx.trxForiegnValue = round(this.myFinTrx.trxForiegnValue,2);
      }      
        
      this.myFinTrx.trxPaymentMethod = <string> this.paymentMethod.value;
      //this.myFinTrx.trxRemarks = "";
      this.myFinTrx.trxRemarks = <string> this.remarks.value;

      //this.myFinTrx.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;;

      loading.present();

      if(checkIfValid(this.user))
      {
        openBizTrx = await this.transactionProvider.getOpenBusinessTransactionsForPartyID(this.user,this.myFinTrx.trxPartyID,this.myFinTrx.trxCurrency);
        console.log("Found Open Transaction for settlement");
        //openBizTrx = <BusinessTransaction>data;
        if(!openBizTrx)  //if openBizTrxn are 0; => all payments are done
        {
          await loading.dismiss();
          alert("There are no pending amount to be settled");
          return;
        }
        else
        {
          console.log("openBizTrx.trxItems.length " + openBizTrx.trxItems.length);
        }

      
        lastFinTrxForBizTrx = await this.transactionProvider.getLastFinTrxForBizTrxID(this.user,this.myFinTrx.trxPartyID,openBizTrx.ID);
        let bValidInput: boolean = false;
        let bCloseBizTrx: boolean = false;
        if(lastFinTrxForBizTrx.trxClosingBalance !== 0)
        {//This business trx is not yet closed, so adjust this payment against same
          
          let myFinTrxValue:number = 0;
          if(openBizTrx.trxCurrency === myConstants.foreignCurrency)
          {
            myFinTrxValue = this.myFinTrx.trxForiegnValue;
          }
          else
          {
            myFinTrxValue = this.myFinTrx.trxLocalValue;
          }

          
          if(myFinTrxValue > lastFinTrxForBizTrx.trxClosingBalance)
          {//Partial adjustment of input payment
            bValidInput = false;
            await loading.dismiss();
            alert("Enter value less than or equal to " + lastFinTrxForBizTrx.trxCurrency + lastFinTrxForBizTrx.trxClosingBalance);
          }
          else
          {//Adjust entire input payment with this bizOpenTrxs[i]
        
            bValidInput = true;
            this.myFinTrx.BizTrxID  = lastFinTrxForBizTrx.BizTrxID;
            this.myFinTrx.confidentialityLevel = lastFinTrxForBizTrx.confidentialityLevel;
            this.myFinTrx.BizInvoiceID = lastFinTrxForBizTrx.BizInvoiceID;
            this.myFinTrx.trxOpeningBalance = lastFinTrxForBizTrx.trxClosingBalance; //To be calculated

            if(this.myFinTrx.trxType === myConstants.trxType.IN_CREDIT)
            {
              if(this.pBusinessContact.category === myConstants.partyCategory.BUYER)
              {
                this.myFinTrx.trxClosingBalance = round((lastFinTrxForBizTrx.trxClosingBalance - myFinTrxValue),2); 
              }
              else if(this.pBusinessContact.category === myConstants.partyCategory.SUPPLIER)
              {
                this.myFinTrx.trxClosingBalance = round((lastFinTrxForBizTrx.trxClosingBalance + myFinTrxValue),2);
              }
            }
            else if(this.myFinTrx.trxType === myConstants.trxType.OUT_DEBIT)
            {
              if(this.pBusinessContact.category === myConstants.partyCategory.BUYER)
              {
                this.myFinTrx.trxClosingBalance = round((lastFinTrxForBizTrx.trxClosingBalance + myFinTrxValue),2); //*this.myFinTrx.trxCurrencyRate
              }
              else if(this.pBusinessContact.category === myConstants.partyCategory.SUPPLIER)
              {
                this.myFinTrx.trxClosingBalance = round((lastFinTrxForBizTrx.trxClosingBalance - myFinTrxValue),2)
              }
            }
                  
            this.myFinTrx.trxInterestRealizedinLocal=0; //Reset to 0 as it need to be Calculated
            this.myFinTrx.trxInterestRealizedinForiegn=0;
            let trxDate: Date = (new Date(this.myFinTrx.trxDate));
            let dueDate: Date = (new Date(openBizTrx.trxDueDate));
            let duration: number = trxDate.valueOf() - dueDate.valueOf();
            let diffDays = Math.floor(duration / (1000 * 3600 * 24)); 
            console.log("Calculating interest on trxDate: " + this.myFinTrx.trxDate + " -> " + trxDate.valueOf());
            console.log("DueDate: " + openBizTrx.trxDueDate + " -> " + dueDate.valueOf());
            console.log("Diff: duration " + duration + " Days: " + diffDays);
            this.myFinTrx.trxDiffDays = diffDays;
            if(diffDays<0)
            {
              this.myFinTrx.trxInterestRealizedinLocal = round(((this.myFinTrx.trxLocalValue)*openBizTrx.trxROIForEarlyPayment*diffDays*12.0)/36500.0,0);
              this.myFinTrx.trxInterestRealizedinForiegn = round(((this.myFinTrx.trxForiegnValue)*openBizTrx.trxROIForEarlyPayment*diffDays*12.0)/36500.0,2);
            }
            else
            {
              this.myFinTrx.trxInterestRealizedinLocal = round(((this.myFinTrx.trxLocalValue)*openBizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0,0);
              this.myFinTrx.trxInterestRealizedinForiegn = round(((this.myFinTrx.trxForiegnValue)*openBizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0,2);
            }

            if(this.myFinTrx.trxClosingBalance === 0)
            {
              bCloseBizTrx = true;
            }
          }
          if(bValidInput)
          {
            console.log("this.myFinTrx.trxRemarks : " + this.myFinTrx.trxRemarks);
            await this.transactionProvider.createFinanceTransaction(this.user,this.myFinTrx);
            console.log("Inserted Successfully");
            await loading.dismiss();              
            this.navCtrl.pop();
          }
        }//end of if(lastFinTrxForBizTrx.trxClosingBalance !== 0)
      }
      else
      {
        alert("Invalid userID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      }
    }
  }

  isTrxChanged()
  {
    if(this.currType.value === myConstants.localCurrency)
    {
      this.exchangeRate = 1;
    }
    else{
      this.exchangeRate = this.financeSettings.exchangeRate;
    }
  }
}
