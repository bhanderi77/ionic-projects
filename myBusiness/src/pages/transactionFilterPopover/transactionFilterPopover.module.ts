import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TransactionFilterPopoverPage } from './transactionFilterPopover';

@NgModule({
  declarations: [
    TransactionFilterPopoverPage,
  ],
  imports: [
    IonicPageModule.forChild(TransactionFilterPopoverPage),
  ],
})
export class TransactionFilterPopoverPageModule {}
