import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the TransactionFilterPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-transaction-filter-popover',
  templateUrl: 'transactionFilterPopover.html',
})
export class TransactionFilterPopoverPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransactionFilterPopoverPage');
  }

  selectedTransaction(trxnType)
  {
    let trxType: string;
    trxType = trxnType;
    console.log("selectedTransaction::trxnType : " + trxType);
    this.viewCtrl.dismiss(trxType);
  }

}
