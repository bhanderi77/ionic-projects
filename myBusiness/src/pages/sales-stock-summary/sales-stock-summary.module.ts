import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesStockSummaryPage } from './sales-stock-summary';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    SalesStockSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesStockSummaryPage),
    PipesModule
  ],
})
export class SalesStockSummaryPageModule {}
