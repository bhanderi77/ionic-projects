import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController, NavParams,AlertController,Loading,LoadingController } from 'ionic-angular';
//import { DatabaseProvider } from '../../providers/database/database';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { taskSummary,summaryDetails } from '../../model/taskSummary';
import { PackagedItem } from '../../model/packagedItem';
import { PackageRule } from '../../model/packageRule';
import { User, checkIfValid } from '../../model/user';

import { myConstants,round } from '../../model/myConstants';
import { Subscription } from 'rxjs';



@IonicPage()
@Component({
  selector: 'page-task-summary',
  templateUrl: 'task-summary.html',
})
export class TaskSummaryPage {
  private user:User;  

  private taskCode:string;
  private trxID:string;
  
  private filterValue:string;
  //private allPackagedItemList:Array<PackagedItem>;
//  private packagedItemList:Observable<packagedItem[]>;
    private packagedItemList:Array<PackagedItem>;
//  private packagedItemList:Observable<packagedItem>;

/*
  private pItemListAtoB:Observable<packagedItem[]>;
  private pItemListBtoB_IP:Observable<packagedItem[]>;
  private pItemListBtoB_CO:Observable<packagedItem[]>;
  private pItemListBtoC:Observable<packagedItem[]>;
*/
  private pItemListAtoB:Array<PackagedItem>;
  private pItemListBtoB_IP:Array<PackagedItem>;
  private pItemListBtoB_CO:Array<PackagedItem>;
  private pItemListBtoC:Array<PackagedItem>;

  private summary:taskSummary;
  private selectedPackageIDList:Array<number>;
  private mySubscriptions:Subscription;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,    
    private navParams: NavParams,private inventoryProvider: InventoryProvider) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');
  
    this.taskCode = navParams.get("taskCode");
    this.trxID = navParams.get("trxID");
    
    this.selectedPackageIDList = [];
    //this.packagedItemList = [];
    //this.allPackagedItemList = [];
    this.resetSummary();   
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad TaskSummaryPage');
    this.filterValue = "A_B";
  }


  TrackBy(index: number, obj: any):any
  {
    //console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  ionViewWillEnter() 
  {
    console.log('ionViewWillEnter TaskSummaryPage');
    this.initializeData();
  }

  ionViewWillLeave() 
  {
    console.log('ionViewWillLeave TaskSummaryPage');
    this.mySubscriptions.unsubscribe();
  }

  async filterPackagedItems(value)
  {
    console.log("Welcome to filterPackagedItems(value): " + value);
   // console.log("this.packagedItemList.length " + this.packagedItemList.length);
    switch(value)
    {
      case "A_B": {//All input
        this.packagedItemList = this.pItemListAtoB;
        break;
      }
      case "A_B_CO" : {//Completed
        this.packagedItemList = this.pItemListAtoB.filter(pItem => pItem.packageStatus === myConstants.packageStatus.CO);
        break;
      }
      case "A_B_RD" : {//pending
        this.packagedItemList = this.pItemListAtoB.filter(pItem => (pItem.packageStatus === myConstants.packageStatus.RD));
        break;
      }
      case "B_B_IP": {//In Progress
        this.packagedItemList = this.pItemListBtoB_IP;
        break;
      }
      case "B_B_CO": {
        if(this.taskCode === myConstants.taskCode.MFG_RETURN)
          this.packagedItemList = this.pItemListAtoB.filter(pItem => (pItem.packageStatus === myConstants.packageStatus.CO));
        else
          this.packagedItemList = this.pItemListBtoB_CO;
        break;
      }
      case "B_C": {//Output
        console.log("Selected B_C " + this.pItemListBtoC.length);
        this.packagedItemList = this.pItemListBtoC;
        break;
      }
      default:
    }
  }

  resetSummary()
  {
    this.summary = {} as taskSummary;
    this.summary.taskCode = this.taskCode;
    this.summary.Input = {} as summaryDetails;
    this.summary.Input.totalQty=0;
    this.summary.Input.totalWeight=0;
    this.summary.Input.taskCost=0;
    this.summary.Input.accumulatedTaskCost=0;

    this.summary.InProgress = {} as summaryDetails;
    this.summary.InProgress.totalQty=0;
    this.summary.InProgress.totalWeight=0;
    this.summary.InProgress.taskCost=0;
    this.summary.InProgress.accumulatedTaskCost=0;

    this.summary.Completed = {} as summaryDetails;
    this.summary.Completed.totalQty=0;
    this.summary.Completed.totalWeight=0;
    this.summary.Completed.taskCost=0;
    this.summary.Completed.accumulatedTaskCost=0;

    this.summary.Pending = {} as summaryDetails;
    this.summary.Pending.totalQty=0;
    this.summary.Pending.totalWeight=0;
    this.summary.Pending.taskCost=0;
    this.summary.Pending.accumulatedTaskCost=0;

    this.summary.Output = {} as summaryDetails;
    this.summary.Output.totalQty=0;
    this.summary.Output.totalWeight=0;
    this.summary.Output.taskCost=0;
    this.summary.Output.accumulatedTaskCost=0;
  }

  async initializeData()
  {
    console.log("Welcome to TaskSummaryPage::initializeData() " + this.trxID);
    this.mySubscriptions = new Subscription();

    this.pItemListAtoB = [];
    this.pItemListBtoB_IP = [];
    this.pItemListBtoB_CO = [];
    this.pItemListBtoC = [];
    
    this.mySubscriptions.add(this.inventoryProvider.get_packagedItems_For_B(this.user,this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged for get_packagedItems_For_B");
      if(pItemList.length)
      {
        if(this.trxID)
          this.pItemListAtoB = pItemList.filter(pItem => pItem.trxID === this.trxID);
        else
          this.pItemListAtoB = pItemList;
        this.filterPackagedItems(this.filterValue);
        this.buildSummary();
      }      
    }));

    this.mySubscriptions.add(this.inventoryProvider.get_packagedItems_For_A(this.user,this.taskCode).valueChanges()
    .subscribe(pItemList => {
      console.log("valueChanged for get_packagedItems_For_A");
      if(pItemList.length)
      {
        if(this.trxID)
          this.pItemListBtoC = pItemList.filter(pItem => pItem.trxID === this.trxID);
        else
          this.pItemListBtoC = pItemList;
        this.filterPackagedItems(this.filterValue);
        this.buildSummary();
      }      
    }));

    this.mySubscriptions.add(this.inventoryProvider.get_BtoB_IP_packagedItems(this.user,this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged for get_BtoB_IP_packagedItems");    
      if(pItemList.length)
      {
        if(this.trxID)
          this.pItemListBtoB_IP = pItemList.filter(pItem => pItem.trxID === this.trxID);
        else
          this.pItemListBtoB_IP = pItemList;
        this.filterPackagedItems(this.filterValue);
        this.buildSummary();
      }      
    }));

    this.mySubscriptions.add(this.inventoryProvider.get_BtoB_CO_packagedItems(this.user,this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged for get_BtoB_CO_packagedItems");  
      if(pItemList.length)
      {
        if(this.trxID)
          this.pItemListBtoB_CO = pItemList.filter(pItem => pItem.trxID === this.trxID);
        else
          this.pItemListBtoB_CO = pItemList;
        this.filterPackagedItems(this.filterValue);
        this.buildSummary();
      }
    }));    
  }

  buildSummary()
  {
    console.log("TaskSummaryPage: Welcome to buildSummary()");
    this.resetSummary();
    let i:number=0;
    //for(let i=0;i<this.pItemListAtoB.)
    for(i=0;i<this.pItemListAtoB.length;i++)
    {
      console.log("this.pItemListAtoB[i].itemWeight " + this.pItemListAtoB[i].itemWeight);
      if((this.pItemListAtoB[i].pRule.curTaskCode !== this.pItemListAtoB[i].pRule.nextTaskCode)
      && ( (this.pItemListAtoB[i].packageStatus === myConstants.packageStatus.RD)
        || (this.pItemListAtoB[i].packageStatus === myConstants.packageStatus.PD)
        || (this.pItemListAtoB[i].packageStatus === myConstants.packageStatus.IP)                
        || (this.pItemListAtoB[i].packageStatus === myConstants.packageStatus.CO)
        ))
      {
        this.summary.Input.totalQty += round(+this.pItemListAtoB[i].itemQty,2);
        this.summary.Input.totalWeight += round(+this.pItemListAtoB[i].itemWeight,2);
        this.summary.Input.taskCost += round(+this.pItemListAtoB[i].taskCost,2);
        this.summary.Input.accumulatedTaskCost += round(+this.pItemListAtoB[i].accumulatedTaskCost,2);            
      }
    };
    
    for(i=0;i<this.pItemListBtoB_IP.length;i++)
    {
      console.log("this.pItemListBtoB_IP[i].itemWeight " + this.pItemListBtoB_IP[i].itemWeight);
      if((this.pItemListBtoB_IP[i].pRule.curTaskCode === this.pItemListBtoB_IP[i].pRule.nextTaskCode)
      && (this.pItemListBtoB_IP[i].packageStatus === myConstants.packageStatus.IP))
      {//B->B and IP 
        this.summary.InProgress.totalQty += round(+this.pItemListBtoB_IP[i].itemQty,2);
        this.summary.InProgress.totalWeight += round(+this.pItemListBtoB_IP[i].itemWeight,2);
        this.summary.InProgress.taskCost += round(+this.pItemListBtoB_IP[i].taskCost,2);
        this.summary.InProgress.accumulatedTaskCost += round(+this.pItemListBtoB_IP[i].accumulatedTaskCost,2);
      }      
    }

    for(i=0;i<this.pItemListBtoB_CO.length;i++)
    {
      console.log("this.pItemListBtoB_CO[i].itemWeight " + this.pItemListBtoB_CO[i].itemWeight);
      if((this.pItemListBtoB_CO[i].pRule.curTaskCode === this.pItemListBtoB_CO[i].pRule.nextTaskCode) 
          && (this.pItemListBtoB_CO[i].packageStatus === myConstants.packageStatus.CO))
      {//B->B and CO
        this.summary.Completed.totalQty += round(+this.pItemListBtoB_CO[i].itemQty,2);
        this.summary.Completed.totalWeight += round(+this.pItemListBtoB_CO[i].itemWeight,2);
        this.summary.Completed.taskCost += round(+this.pItemListBtoB_CO[i].taskCost,2);
        this.summary.Completed.accumulatedTaskCost += round(+this.pItemListBtoB_CO[i].accumulatedTaskCost,2);
      }
    };
    
    for(i=0;i<this.pItemListBtoC.length;i++)
    {
      console.log("this.pItemListBtoC[i].itemWeight " + this.pItemListBtoC[i].itemWeight);
      if( (this.pItemListBtoC[i].pRule.curTaskCode !== this.pItemListBtoC[i].pRule.nextTaskCode)
      &&  (  (this.pItemListBtoC[i].packageStatus === myConstants.packageStatus.RD)
          || (this.pItemListBtoC[i].packageStatus === myConstants.packageStatus.PD)
          || (this.pItemListBtoC[i].packageStatus === myConstants.packageStatus.CO)
          || (this.pItemListBtoC[i].packageStatus === myConstants.packageStatus.IP)
          ))
        {
          this.summary.Output.totalQty += round(+this.pItemListBtoC[i].itemQty,2);
          this.summary.Output.totalWeight += round(+this.pItemListBtoC[i].itemWeight,2);
          this.summary.Output.taskCost += round(+this.pItemListBtoC[i].taskCost,2);
          this.summary.Output.accumulatedTaskCost += round(+this.pItemListBtoC[i].accumulatedTaskCost,2);
        }  
    }
    
    this.summary.Pending.totalQty = round((+this.summary.Input.totalQty - (+this.summary.InProgress.totalQty + +this.summary.Completed.totalQty)),2);
    this.summary.Pending.totalWeight = round((+this.summary.Input.totalWeight - (+this.summary.InProgress.totalWeight + +this.summary.Completed.totalWeight)),2);
    this.summary.Pending.taskCost = round((+this.summary.Input.taskCost - (+this.summary.InProgress.taskCost + +this.summary.Completed.taskCost)),2);
    this.summary.Pending.accumulatedTaskCost = round((+this.summary.Input.accumulatedTaskCost - (+this.summary.InProgress.accumulatedTaskCost + +this.summary.Completed.accumulatedTaskCost)),2);
  }

  
  
  showTrace(pSelectedItem:PackagedItem)
  {
    /*const modal = this.modalCtrl.create('PackageTracePage',{pItem:pSelectedItem});
    modal.present();*/
   // const modal = this.modalCtrl.create('ItemDetailPage',{User:this.user,pItem:pSelectedItem});
    this.navCtrl.push('ItemDetailPage',{User:this.user,pItem:pSelectedItem}); 
    
    /*
    modal.onDidDismiss((data) => {
      this.initializeData();  
    }); 
    */     
  }

  onPackageClicked(pSelectedItem:PackagedItem)
  {
    console.log("Welcome to PurchaseStockSummaryPage:onPackageClicked()");
    console.log("curTaskCode " + pSelectedItem.pRule.curTaskCode);
    console.log("nextTaskCode " + pSelectedItem.pRule.nextTaskCode);
    console.log("id " + pSelectedItem.id);
    this.navCtrl.push('ItemDetailPage',{User:this.user,pItem:pSelectedItem}); 
    /*
    if((p.packageStatus === myConstants.packageStatus.RD) || (p.packageStatus === myConstants.packageStatus.PD))
    {
      this.startTask(p);
    }
    else if((p.packageStatus === myConstants.packageStatus.IP))
    {
      this.completeTask(p);
    }*/
    
  }
}
