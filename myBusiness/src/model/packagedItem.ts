import { myConstants } from '../model/myConstants';
import { PackageRule } from '../model/packageRule';
import { BusinessTransaction } from '../model/businessTransaction';

export interface ItemValuation {
    rate:number;
    value:number;

    costFactor:number;
    avgPurchaseCost:number;
    avgInterestCost:number;
    avgMfgCost:number;
    avgCost:number;

    totalPurchaseCost:number;
    interestCost:number;
    totalInterestCost:number;
    totalMfgCost:number;
    totalCost:number;

    profit:number;
    profitMargin:number;
}

export interface PackagedItem {
    id:string;
    creation_time:string;
    server_time:string;
    userID:string;
    customerID:string;
    confidentialityLevel:number;
    
    packageID:string;
    parentID:string;
    packageGroupID:string;
    packageGroupName:string;
    packageStatus: myConstants.packageStatus;
    packageCurTaskOwner: string;
    packageNextTaskOwner: string;    
    pRule:PackageRule;
    
    trxID:string;
    trxDate:string;//Date;
    trxPartyID:string;
    trxPartyName:string;
    trxWeight:number;
    trxRate:number;
    trxCurrency:string;
    trxShare:number;

    itemRemarks: string;
    itemQty: number;
    itemWeight: number;
    
    accumulatedTaskCost: number;
    taskCost:number;
    roughWeight:number;

    itemType:ItemType;
    itemShape:ItemShape;
    itemSieve:ItemSieve;
    itemQualityLevel:ItemQualityLevel;
    itemValuation:ItemValuation;
}

export interface ItemType {
    id:string;
    creation_time:string;
    name:string;
    rank:number;
    remarks:string;
}

export interface ItemShape {
    id:string;
    creation_time:string;
    name:string;
    rank:number;
    remarks:string;
}

export interface ItemSieve {
    id:string;
    creation_time:string;
    itemTypeID:string;
    name:string;
    rank:number;
    remarks:string;
}

export interface ItemQualityLevel {
    id:string;
    creation_time:string;
    itemTypeID:string;
    name:string;
    rank:number;
    remarks:string;
}