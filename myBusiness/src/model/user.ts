export interface User {
    ID:string;
    server_time:string;
    creation_time:string;
    firstName:string;
    lastName:string;
    mobileNo:string;
    email:string;
    customerID:string;
    appVersionID:string;
    dataVersionID:string;
    bWriteAccess:boolean;
}

export function checkIfValid(u:User):boolean
{
    if(u)
    {
 //       console.log("User: " + u.ID + " " + u.customerID + " " + u.dataVersionID);
        if((u.ID != '') && (u.ID != 'M') && (u.customerID))
        {
            return true;
        }
    }
    return false;
}