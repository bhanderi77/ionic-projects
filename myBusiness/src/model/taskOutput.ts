export interface taskOutput{
	shape:string;
	sieve:string;
	weight:number;
	qty:number;
	cost:number;
	tax:number;
	//trxShare:number;
	qualityLevel:string;
	remark:string;
	nextTaskCode:string;
	validWeight:boolean;
	validQty:boolean;
}

