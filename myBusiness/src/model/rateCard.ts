import { ItemType,ItemShape,ItemSieve,ItemQualityLevel } from './packagedItem';


export interface RateCard {
    itemType:ItemType;
    itemShape:ItemShape;
    itemSieve:ItemSieve;
    itemQualityLevel:ItemQualityLevel;
    valueFactor:number;
    avgPurchaseCost:number;
    avgInterestCost:number;
    avgMfgCost:number;
    avgGrossCost:number;
    avgMarketRate:number;
    avgSalesGain:number;
};

export interface InventoryParams {
    avgMfgGain:number;
    //avgTopsGain:number;
    //avgRoughGain:number;
    avgP2SGain:number;
    
    totalSaleablePolishedWeight:number;
    totalSaleableTopsWeight:number;
    totalSaleableRoughWeight:number;
    totalSaleableWeight:number;
    
    avgMfgCostForPolished:number;
    avgMfgCostForTops:number;
    avgMfgCostForRough:number;
    
    purchaseCostPerSaleableUnit:number;
    interestCostPerSaleableUnit:number;
    
    totalPurchaseWeight:number;
    
    totalMarketValueForSaleable:number;
    avgMarketRateForSaleable:number;
};
