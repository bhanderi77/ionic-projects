import { myConstants } from "./myConstants";
import { ItemQualityLevel } from "./packagedItem";

export interface MarketPrice {
    id:string,
    creation_time:string,
    itemTypeID:string;
    shapeID:string;
    sieveID:string;
    qualityLevelID:string;//myConstants.polishQualityLevel;
    qualityLevelRank:number;
    //uomID:myConstants.uom;
    rate:number;
}

export interface MarketPriceRough {
    sieveID:string;
    qualityLevelID:string;//myConstants.polishQualityLevel;
    //uomID:myConstants.uom;
    rate:number;
}