import { myConstants, round } from './myConstants';
import { ItemType,ItemShape,ItemSieve,ItemQualityLevel } from './packagedItem';
import { TaxRate,TaxAmount } from './tax';

export class SalesItem {
    id:string;

    itemType:ItemType;
    itemShape:ItemShape;
    itemSieve:ItemSieve;
    itemQualityLevel:ItemQualityLevel;

    saleableWeight: number;
    purchaseWeight: number;
    qty: number;
    taxRate:TaxRate;
    
    valueFactor:number;
    
    avgPriceRate:number;
    avgSalePrice:number;
    avgSalesTax:TaxAmount;
    avgGrossPrice:number;
    avgSalesBrokerage:number;
    avgNetPrice:number;
    avgSalesGain:number;

    avgPurchaseCost:number;
    avgInterestCost:number;
    avgMfgCost:number;
    avgGrossCost:number;
    avgGrossCostAtDueTerm:number;

    avgProfit:number;
    avgProfitMargin:number;

    totalPriceValue:number;
    totalSaleValue:number;
    totalSalesTax:TaxAmount;
    totalGrossValue:number;
    totalSalesBrokerage:number;
    totalNetValue:number;

    totalPurchaseCost:number;
    totalInterestCost:number;
    totalMfgCost:number;
    totalGrossCost:number;
    totalGrossCostAtDueTerm:number;

    totalProfit:number;
    totalProfitMargin:number;
    totalSalesGain:number;

    /*remarks: string;
    packageGroupID:string;
    packageGroupName:string;
    */
    trxID:string;
    trxStatus:string;
    trxDate:string;//Date;
    /*
    trxPartyID:string;
    trxPartyName:string;
    trxWeight:number;
    trxRate:number;
    */
    trxCurrency:string;
    trxCurrencyRate:number;
    trxDueDays:number;
    trxValidityDays:number;
    trxROIForLatePayment:number;
    trxBrokerageRate:number;

    

    public constructor()
    {
        this.id = "";
        this.itemType = {} as ItemType;
        this.itemType.id = "";
        this.itemShape = {} as ItemShape;
        this.itemShape.id = "";
        this.itemSieve = {} as ItemSieve;
        this.itemSieve.id = "";
        this.itemQualityLevel = {} as ItemQualityLevel;
        this.itemQualityLevel.id = "";

        this.saleableWeight = 0;
        this.purchaseWeight = 0;
        this.qty = 0;
        
        this.valueFactor = 0;
        
        
        this.avgPriceRate = 0;
        this.avgSalePrice = 0;
        this.avgSalesTax = {} as TaxAmount;
        this.avgSalesTax.amtCGST = 0;
        this.avgSalesTax.amtSGST = 0;
        this.avgSalesTax.amtIGST = 0;
        this.avgSalesTax.totalAmount = 0;

        this.avgGrossPrice = 0;
        this.avgSalesBrokerage = 0;
        this.avgNetPrice = 0;
        this.avgSalesGain = 0;

        this.avgProfit = 0;
        this.avgProfitMargin = 0;

        this.totalPriceValue = 0;
        this.totalSaleValue = 0;
        
        this.taxRate = {} as TaxRate;
        this.taxRate.rateCGST = 0;
        this.taxRate.rateSGST = 0;
        this.taxRate.rateIGST = 0;

        this.totalSalesTax = {} as TaxAmount;
        this.totalSalesTax.amtCGST = 0;
        this.totalSalesTax.amtSGST = 0;
        this.totalSalesTax.amtIGST = 0;
        this.totalSalesTax.totalAmount = 0;

        this.totalGrossValue = 0;
        this.totalSalesBrokerage = 0;
        this.totalNetValue = 0;
        this.totalSalesGain = 0;

        this.avgPurchaseCost = 0;
        this.avgInterestCost = 0;
        this.avgMfgCost = 0;
        this.avgGrossCost = 0;
        this.avgGrossCostAtDueTerm = 0;

        this.totalPurchaseCost = 0;
        this.totalInterestCost = 0;
        this.totalMfgCost = 0;
        this.totalGrossCost = 0;
        this.totalGrossCostAtDueTerm = 0;

        this.totalProfit = 0;
        this.totalProfitMargin = 0;

        /*this.remarks = "";
        this.packageGroupID = "";
        this.packageGroupName = "";
        */
        this.trxID = "";
        this.trxStatus = myConstants.bizTrxStatus.DRAFT;
        this.trxDate = "";
        this.trxDueDays = 0;
        this.trxValidityDays = 0;
        /*this.trxDate = "";
        this.trxPartyID = "";
        this.trxPartyName = "";
        this.trxWeight = 0;
        this.trxRate = 0;
        */
        this.trxCurrency = myConstants.localCurrency;
        this.trxCurrencyRate = 1;
        this.trxROIForLatePayment = 0;
        this.trxBrokerageRate = 0;
    }

    public init(s:SalesItem)
    {
        this.id = s.id;
        this.itemType = s.itemType;
        this.itemShape = s.itemShape;
        this.itemSieve = s.itemSieve;
        this.itemQualityLevel = s.itemQualityLevel;

        this.saleableWeight = s.saleableWeight;
        this.purchaseWeight = s.purchaseWeight;
        this.qty = s.qty;
        
        this.valueFactor = s.valueFactor;        
        
        this.avgPriceRate = s.avgPriceRate;
        this.avgSalePrice = s.avgSalePrice;
        this.avgSalesTax.amtCGST = s.avgSalesTax.amtCGST;
        this.avgSalesTax.amtSGST = s.avgSalesTax.amtSGST;
        this.avgSalesTax.amtIGST = s.avgSalesTax.amtIGST;
        this.avgSalesTax.totalAmount = s.avgSalesTax.totalAmount;

        this.avgGrossPrice = s.avgGrossPrice;
        this.avgSalesBrokerage = s.avgSalesBrokerage;
        this.avgNetPrice = s.avgNetPrice;
        this.avgSalesGain = s.avgSalesGain;

        this.avgProfit = s.avgProfit;
        this.avgProfitMargin = s.avgProfitMargin;

        this.totalPriceValue = s.totalPriceValue;
        this.totalSaleValue = s.totalSaleValue;
        
        this.taxRate.rateCGST = s.taxRate.rateCGST;
        this.taxRate.rateSGST = s.taxRate.rateSGST;
        this.taxRate.rateIGST = s.taxRate.rateIGST;

        this.totalSalesTax.amtCGST = s.totalSalesTax.amtCGST;
        this.totalSalesTax.amtSGST = s.totalSalesTax.amtSGST;
        this.totalSalesTax.amtIGST = s.totalSalesTax.amtIGST;
        this.totalSalesTax.totalAmount = s.totalSalesTax.totalAmount;

        this.totalGrossValue = s.totalGrossValue;
        this.totalSalesBrokerage = s.totalSalesBrokerage;
        this.totalNetValue = s.totalNetValue;
        this.totalSalesGain = s.totalSalesGain;

        this.avgPurchaseCost = s.avgPurchaseCost;
        this.avgInterestCost = s.avgInterestCost;
        this.avgMfgCost = s.avgMfgCost;
        this.avgGrossCost = s.avgGrossCost;
        this.avgGrossCostAtDueTerm = s.avgGrossCostAtDueTerm;

        this.totalPurchaseCost = s.totalPurchaseCost;
        this.totalInterestCost = s.totalInterestCost;
        this.totalMfgCost = s.totalMfgCost;
        this.totalGrossCost = s.totalGrossCost;
        this.totalGrossCostAtDueTerm = s.totalGrossCostAtDueTerm;

        this.totalProfit = s.totalProfit;
        this.totalProfitMargin = s.totalProfitMargin;

        this.trxID = s.trxID;
        this.trxStatus = s.trxStatus;
        this.trxDate = s.trxDate;
        this.trxDueDays = s.trxDueDays;
        this.trxValidityDays = s.trxValidityDays;
        this.trxCurrency = s.trxCurrency
        this.trxCurrencyRate = s.trxCurrencyRate;
        this.trxROIForLatePayment = s.trxROIForLatePayment;
        this.trxBrokerageRate = s.trxBrokerageRate;
    }

    public consoleLog()
    {
        console.log("id " + this.id);
        console.log("itemType.id " + this.itemType.id);
        console.log("itemShape.id " + this.itemShape.id);
        console.log("itemSieve.id " + this.itemSieve.id);
        console.log("itemQualityLevel.id " + this.itemQualityLevel.id);
        console.log(" ");
        console.log("trxCurrency " + this.trxCurrency);
        console.log("trxCurrencyRate " + this.trxCurrencyRate);        
        console.log(" ");
        console.log("saleableWeight " + this.saleableWeight);
        console.log("purchaseWeight " + this.purchaseWeight);
        console.log("qty " + this.qty);
        console.log("valueFactor " + this.valueFactor);
        console.log("trxROIForLatePayment " + this.trxROIForLatePayment + " %");
        console.log("trxBrokerageRate " + this.trxBrokerageRate + " %");
        console.log(" ");
        console.log("rateCGST " + this.taxRate.rateCGST + " %");
        console.log("rateSGST " + this.taxRate.rateSGST + " %");
        console.log("rateIGST " + this.taxRate.rateIGST + " %");
        console.log(" ");
        console.log("avgPriceRate " + this.avgPriceRate);
        console.log("avgSalePrice " + this.avgSalePrice);
        console.log("avgSalesTax " + this.avgSalesTax.totalAmount);
        console.log("avgGrossPrice " + this.avgGrossPrice);
        console.log("avgSalesBrokerage " + this.avgSalesBrokerage);
        console.log("avgNetPrice " + this.avgNetPrice);        
        console.log("avgSalesGain " + this.avgSalesGain);
        console.log(" ");
        console.log("avgPurchaseCost " + this.avgPurchaseCost);
        console.log("avgInterestCost " + this.avgInterestCost);
        console.log("avgMfgCost " + this.avgMfgCost);
        console.log("avgGrossCost " + this.avgGrossCost);
        console.log("avgGrossCostAtDueTerm " + this.avgGrossCostAtDueTerm);
        console.log(" ");
        console.log("avgProfit " + this.avgProfit);
        console.log("avgProfitMargin " + this.avgProfitMargin + " %");
        console.log(" ");
        console.log("totalPriceValue " + this.totalPriceValue);
        console.log("totalSaleValue " + this.totalSaleValue);
        console.log("totalSalesTax.amtCGST " + this.totalSalesTax.amtCGST);
        console.log("totalSalesTax.amtSGST " + this.totalSalesTax.amtSGST);
        console.log("totalSalesTax.amtIGST " + this.totalSalesTax.amtIGST);
        console.log("totalSalesTax " + this.totalSalesTax.totalAmount);
        console.log("totalGrossValue " + this.totalGrossValue);
        console.log("totalSalesBrokerage " + this.totalSalesBrokerage);
        console.log("totalNetValue " + this.totalNetValue);
        console.log("totalSalesGain " + this.totalSalesGain);
        console.log(" ");
        console.log("totalPurchaseCost " + this.totalPurchaseCost);
        console.log("totalInterestCost " + this.totalInterestCost);
        console.log("totalMfgCost " + this.totalMfgCost);
        console.log("totalGrossCost " + this.totalGrossCost);
        console.log("totalGrossCostAtDueTerm " + this.totalGrossCostAtDueTerm);
        console.log(" ");
        console.log("totalProfit " + this.totalProfit);
        console.log("totalProfitMargin " + this.totalProfitMargin + " %");
        console.log(" ");
        console.log("trxID " + this.trxID);
        console.log("trxDate " + this.trxDate);
        console.log("trxStatus " + this.trxStatus);
        console.log("trxDueDays " + this.trxDueDays);
        console.log("trxValidityDays " + this.trxValidityDays);
    }

    doRound(digits:number)
    {
        this.saleableWeight = round(this.saleableWeight,3);
        this.purchaseWeight = round(this.purchaseWeight,3);
        this.qty = round(this.qty,0);
        
        //this.valueFactor = round(this.valueFactor,digits);
        
        this.avgPriceRate = round(this.avgPriceRate,digits);
        this.avgSalePrice = round(this.avgSalePrice,digits);
        this.avgSalesTax.amtCGST = round(this.avgSalesTax.amtCGST,2);
        this.avgSalesTax.amtSGST = round(this.avgSalesTax.amtSGST,2);
        this.avgSalesTax.amtIGST = round(this.avgSalesTax.amtIGST,2);
        this.avgSalesTax.totalAmount = round(this.avgSalesTax.totalAmount,2);
        this.avgGrossPrice = round(this.avgGrossPrice,digits);
        this.avgSalesBrokerage = round(this.avgSalesBrokerage,2);
        this.avgNetPrice = round(this.avgNetPrice,digits);
        this.avgSalesGain = round(this.avgSalesGain,digits);
        this.avgProfit = round(this.avgProfit,digits);
        this.avgProfitMargin = round(this.avgProfitMargin,digits);

        this.totalPriceValue = round(this.totalPriceValue,digits);
        this.totalSaleValue = round(this.totalSaleValue,digits);
        this.totalSalesTax.amtCGST = round(this.totalSalesTax.amtCGST,digits);
        this.totalSalesTax.amtSGST = round(this.totalSalesTax.amtSGST,digits);
        this.totalSalesTax.amtIGST = round(this.totalSalesTax.amtIGST,digits);
        this.totalSalesTax.totalAmount = round(this.totalSalesTax.totalAmount,digits);
        this.totalGrossValue = round(this.totalGrossValue,digits);
        this.totalSalesBrokerage = round(this.totalSalesBrokerage,digits);
        this.totalNetValue = round(this.totalNetValue,digits);
        this.totalSalesGain = round(this.totalSalesGain,digits);

        this.avgPurchaseCost = round(this.avgPurchaseCost,digits);
        this.avgInterestCost = round(this.avgInterestCost,digits);
        this.avgMfgCost = round(this.avgMfgCost,digits);
        this.avgGrossCost = round(this.avgGrossCost,digits);
        this.avgGrossCostAtDueTerm = round(this.avgGrossCostAtDueTerm,digits);

        this.totalPurchaseCost = round(this.totalPurchaseCost,digits);
        this.totalInterestCost = round(this.totalInterestCost,digits);
        this.totalMfgCost = round(this.totalMfgCost,digits);
        this.totalGrossCost = round(this.totalGrossCost,digits);
        this.totalGrossCostAtDueTerm = round(this.totalGrossCostAtDueTerm,digits);

        this.totalProfit = round(this.totalProfit,digits);
        this.totalProfitMargin = round(this.totalProfitMargin,2);
    }

    doTotal(digits:number)
    {
        //console.log("Welcome to SalesItem:doTotal(digits) " + digits);

        if(this.avgPriceRate > 0)
        {
            this.totalPriceValue = round(this.avgPriceRate*this.saleableWeight,digits);
        }
        let duePenaltyRate:number = round((((+this.trxDueDays + +this.trxValidityDays)/30.0)*(+this.trxROIForLatePayment)),10);
        this.totalSaleValue = round(this.totalPriceValue*(1+(duePenaltyRate/100)),digits);
        this.totalSalesTax.amtCGST = round((this.taxRate.rateCGST/100.0)*this.totalSaleValue,digits);
        this.totalSalesTax.amtSGST = round((this.taxRate.rateSGST/100.0)*this.totalSaleValue,digits);
        this.totalSalesTax.amtIGST = round((this.taxRate.rateIGST/100.0)*this.totalSaleValue,digits);
        this.totalSalesTax.totalAmount = round(this.totalSalesTax.amtCGST + +this.totalSalesTax.amtSGST + +this.totalSalesTax.amtIGST,digits);
        this.totalGrossValue = round(+this.totalSaleValue + +this.totalSalesTax.totalAmount,digits);
        this.totalSalesBrokerage = round(this.totalSaleValue*(this.trxBrokerageRate/100),digits);
        this.totalNetValue = round(this.totalSaleValue - this.totalSalesBrokerage,digits);

        if(this.avgSalesGain > 0)
        {
            this.totalSalesGain = round(this.avgSalesGain*this.saleableWeight,digits);
        }
        
        this.totalPurchaseCost = round(this.avgPurchaseCost*this.saleableWeight,digits);
        this.totalInterestCost = round(this.avgInterestCost*this.saleableWeight,digits);
        this.totalMfgCost = round(this.avgMfgCost*this.saleableWeight,digits);
        this.totalGrossCost = round(this.avgGrossCost*this.saleableWeight,digits);
        this.totalGrossCostAtDueTerm = round(this.totalGrossCost*(1+(duePenaltyRate/100)),digits);
        this.totalProfit = round(this.totalNetValue - this.totalGrossCostAtDueTerm,digits);
        if(this.totalGrossCostAtDueTerm > 0)
        {
            this.totalProfitMargin = round((this.totalProfit/this.totalGrossCostAtDueTerm)*100,2);
        }
                
    }

    doAverage(digits:number)
    {
        if(this.saleableWeight > 0)
        {
            this.avgPriceRate = round(this.totalPriceValue/this.saleableWeight,digits);
            this.avgSalePrice = round(this.totalSaleValue/this.saleableWeight,digits);
            this.avgSalesTax.amtCGST = round(this.totalSalesTax.amtCGST/this.saleableWeight,2);
            this.avgSalesTax.amtSGST = round(this.totalSalesTax.amtSGST/this.saleableWeight,2);
            this.avgSalesTax.amtIGST= round(this.totalSalesTax.amtIGST/this.saleableWeight,2);
            this.avgSalesTax.totalAmount = round(this.totalSalesTax.totalAmount/this.saleableWeight,2);
            this.avgGrossPrice = round(this.totalGrossValue/this.saleableWeight,digits);
            this.avgSalesBrokerage = round(this.totalSalesBrokerage/this.saleableWeight,2);
            this.avgNetPrice = round(this.totalNetValue/this.saleableWeight,digits);
            this.avgSalesGain = round(this.totalSalesGain/this.saleableWeight,digits);

            this.avgPurchaseCost = round((this.totalPurchaseCost/this.saleableWeight),digits);
            this.avgInterestCost = round((this.totalInterestCost/this.saleableWeight),digits);
            this.avgMfgCost = round((this.totalMfgCost/this.saleableWeight),digits);
            this.avgGrossCost = round((this.totalGrossCost/this.saleableWeight),digits);
            this.avgGrossCostAtDueTerm = round((this.totalGrossCostAtDueTerm/this.saleableWeight),digits);

            this.avgProfit = round((this.totalProfit/this.saleableWeight),digits);

            if(this.avgGrossCostAtDueTerm > 0)
            {
                this.avgProfitMargin = round((this.avgProfit/this.avgGrossCostAtDueTerm)*100,digits);
            }            
        }       
    }

    setCurrency(currency:string,exchangeRate:number)
    {

        if((this.trxCurrency === myConstants.localCurrency) && (currency === myConstants.foreignCurrency))
        {
            this.trxCurrency = currency;
            this.trxCurrencyRate = exchangeRate;
           // this.trxRate = round(this.trxRate/this.trxCurrencyRate,2);
        
            this.avgPurchaseCost = round(this.avgPurchaseCost/this.trxCurrencyRate,2);
            this.avgInterestCost = round(this.avgInterestCost/this.trxCurrencyRate,2);
            this.avgMfgCost = round(this.avgMfgCost/this.trxCurrencyRate,2);
            this.avgGrossCost = round(this.avgGrossCost/this.trxCurrencyRate,2);
            this.avgGrossCostAtDueTerm = round(this.avgGrossCostAtDueTerm/this.trxCurrencyRate,2);
            
            this.avgPriceRate = round(this.avgPriceRate/this.trxCurrencyRate,2);
            /*
            this.avgSalePrice = round(this.avgSalePrice/this.trxCurrencyRate,2);
            this.avgSalesTax.amtCGST = round(this.avgSalesTax.amtCGST/this.trxCurrencyRate,2);
            this.avgSalesTax.amtSGST = round(this.avgSalesTax.amtSGST/this.trxCurrencyRate,2);
            this.avgSalesTax.amtIGST = round(this.avgSalesTax.amtIGST/this.trxCurrencyRate,2);
            this.avgSalesTax.totalAmount = round(this.avgSalesTax.totalAmount/this.trxCurrencyRate,2);
            this.avgGrossPrice = round(this.avgGrossPrice/this.trxCurrencyRate,2);
            this.avgSalesBrokerage = round(this.avgSalesBrokerage/this.trxCurrencyRate,2);
            this.avgNetPrice = round(this.avgNetPrice/this.trxCurrencyRate,2);
            this.avgSalesGain = round(this.avgSalesGain/this.trxCurrencyRate,2);

            this.avgProfit = round(this.avgProfit/this.trxCurrencyRate,2);
            this.avgProfitMargin = round(this.avgProfitMargin/this.trxCurrencyRate,2);
            */
            this.doTotal(2);
            this.doAverage(2);
        }
        else if((this.trxCurrency === myConstants.foreignCurrency) && (currency === myConstants.localCurrency))
        {            
            this.trxCurrency = currency;
            this.trxCurrencyRate = exchangeRate;
            //this.trxRate = round(this.trxRate*this.trxCurrencyRate,2);
        
            this.avgPurchaseCost = round((this.avgPurchaseCost*this.trxCurrencyRate),2);
            this.avgInterestCost = round((this.avgInterestCost*this.trxCurrencyRate),2);
            this.avgMfgCost = round((this.avgMfgCost*this.trxCurrencyRate),2);
            this.avgGrossCost = round((this.avgGrossCost*this.trxCurrencyRate),2);
            this.avgGrossCostAtDueTerm = round((this.avgGrossCostAtDueTerm*this.trxCurrencyRate),2);
            
            this.avgPriceRate = round((this.avgPriceRate*this.trxCurrencyRate),2);
            /*
            this.avgSalePrice = round(this.avgSalePrice*this.trxCurrencyRate,2);
            this.avgSalesTax.amtCGST = round(this.avgSalesTax.amtCGST*this.trxCurrencyRate,2);
            this.avgSalesTax.amtSGST = round(this.avgSalesTax.amtSGST*this.trxCurrencyRate,2);
            this.avgSalesTax.amtIGST = round(this.avgSalesTax.amtIGST*this.trxCurrencyRate,2);
            this.avgSalesTax.totalAmount = round(this.avgSalesTax.totalAmount*this.trxCurrencyRate,2);
            this.avgGrossPrice = round(this.avgGrossPrice*this.trxCurrencyRate,2);
            this.avgSalesBrokerage = round(this.avgSalesBrokerage*this.trxCurrencyRate,2);
            this.avgNetPrice = round(this.avgNetPrice*this.trxCurrencyRate,2);
            this.avgSalesGain = round(this.avgSalesGain*this.trxCurrencyRate,2);

            this.avgProfit = round(this.avgProfit*this.trxCurrencyRate,2);
            this.avgProfitMargin = round(this.avgProfitMargin*this.trxCurrencyRate,2);
            */
            this.doTotal(2);   
            this.doAverage(2);         
        }        
    }
    calculateTax(bStateRegion:boolean)
    {
        this.avgSalesTax.amtCGST = 0;
        this.avgSalesTax.amtSGST = 0;
        this.avgSalesTax.amtIGST = 0;
        this.avgSalesTax.totalAmount = 0;

        this.totalSalesTax.amtCGST = 0;
        this.totalSalesTax.amtSGST = 0;
        this.totalSalesTax.amtIGST = 0;
        this.totalSalesTax.totalAmount = 0;


        if(bStateRegion)
        {
            if(this.trxCurrency === myConstants.localCurrency)
            {
                this.avgSalesTax.amtCGST = round((this.avgSalePrice)*(this.taxRate.rateCGST/100),2);
                this.avgSalesTax.amtSGST = round((this.avgSalePrice)*(this.taxRate.rateSGST/100),2);

                this.totalSalesTax.amtCGST = round((this.saleableWeight*this.avgSalePrice)*(this.taxRate.rateCGST/100),2);
                this.totalSalesTax.amtSGST = round((this.saleableWeight*this.avgSalePrice)*(this.taxRate.rateSGST/100),2);

                this.avgSalesTax.totalAmount = round((+this.avgSalesTax.amtCGST + +this.avgSalesTax.amtSGST + +this.avgSalesTax.amtIGST),2);       
                this.totalSalesTax.totalAmount = round((+this.totalSalesTax.amtCGST + +this.totalSalesTax.amtSGST + +this.totalSalesTax.amtIGST),2);       
            }
            else if(this.trxCurrency === myConstants.foreignCurrency)
            {
                this.avgSalesTax.amtCGST = round((this.avgSalePrice)*(this.taxRate.rateCGST/100),2);
                this.avgSalesTax.amtSGST = round((this.avgSalePrice)*(this.taxRate.rateSGST/100),2);

                this.totalSalesTax.amtCGST = round((this.saleableWeight*this.avgSalePrice)*(this.taxRate.rateCGST/100),2);
                this.totalSalesTax.amtSGST = round((this.saleableWeight*this.avgSalePrice)*(this.taxRate.rateSGST/100),2);

                this.avgSalesTax.totalAmount = round((+this.avgSalesTax.amtCGST + +this.avgSalesTax.amtSGST + +this.avgSalesTax.amtIGST),2);       
                this.totalSalesTax.totalAmount = round((+this.totalSalesTax.amtCGST + +this.totalSalesTax.amtSGST + +this.totalSalesTax.amtIGST),2);       
            }
            
        }
        else
        {
            if(this.trxCurrency === myConstants.localCurrency)
            {
                this.avgSalesTax.amtIGST = round((this.avgSalePrice)*(this.taxRate.rateIGST/100),2);    
                this.totalSalesTax.amtIGST = round((this.saleableWeight*this.avgSalePrice)*(this.taxRate.rateIGST/100),2);    
         
                this.avgSalesTax.totalAmount = round((+this.avgSalesTax.amtCGST + +this.avgSalesTax.amtSGST + +this.avgSalesTax.amtIGST),2);       
                this.totalSalesTax.totalAmount = round((+this.totalSalesTax.amtCGST + +this.totalSalesTax.amtSGST + +this.totalSalesTax.amtIGST),2);       
            }
            else if(this.trxCurrency === myConstants.foreignCurrency)
            {
                this.avgSalesTax.amtIGST = round((this.avgSalePrice)*(this.taxRate.rateIGST/100),2);    
                this.totalSalesTax.amtIGST = round((this.saleableWeight*this.avgSalePrice)*(this.taxRate.rateIGST/100),2); 
         
                this.avgSalesTax.totalAmount = round((+this.avgSalesTax.amtCGST + +this.avgSalesTax.amtSGST + +this.avgSalesTax.amtIGST),2);       
                this.totalSalesTax.totalAmount = round((+this.totalSalesTax.amtCGST + +this.totalSalesTax.amtSGST + +this.totalSalesTax.amtIGST),2);          
            }            
        }        
    }
}