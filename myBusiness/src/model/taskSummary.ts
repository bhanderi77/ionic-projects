export interface summaryDetails {
    totalQty: number;
    totalWeight: number;
    taskCost: number;
    accumulatedTaskCost: number;
    totalBizTrxShare:number;
}
export interface taskSummary {
    taskCode: string;
    Input:summaryDetails;
    InProgress:summaryDetails;
    Completed:summaryDetails;
    Output:summaryDetails;
    Pending:summaryDetails;
}