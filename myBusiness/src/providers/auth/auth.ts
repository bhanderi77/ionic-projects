//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

//import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore } from 'angularfire2/firestore';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import * as firebase from 'firebase/app';

import { User } from '../../model/User';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  private firebaseUser: firebase.User;

  constructor(private afs: AngularFirestore,
    private platform:Platform,
    private gplus: GooglePlus) 
  {
    console.log('Hello AuthProvider Provider');
    firebase.auth().onAuthStateChanged( user => {
      console.log("inside onAuthStateChanged");
      if(user)
      {
        this.firebaseUser = user;
        console.log("this.firebaseUser is set " + user.email);
      }
      else
      {
        this.firebaseUser = null;
        console.log("this.firebaseUser is null");
      }
    });
    /*
    firebase.auth().onIdTokenChanged( user => {
      console.log("inside onIdTokenChanged");
      if(user)
      {
        this.firebaseUser = user;
        console.log("this.firebaseUser is set " + user.email);
      }
      else
      {
        this.firebaseUser = null;
        console.log("this.firebaseUser is null");
      }
    });*/
  }

  async getUser():Promise<User>
  {
    console.log("Welcome to AuthProvider::getUser()");
    
    let currentUser = {} as User;
    currentUser.ID = "";
    /*
    currentUser.deviceUUID = UUID;
    currentUser.devicePlatform = platform;
    currentUser.deviceManufacturer = manufacturer;
    currentUser.deviceModel = model;
    currentUser.deviceVersion = version;
    */
    currentUser.firstName = "";
    currentUser.lastName = "";
    currentUser.mobileNo = "";
    currentUser.email = "";
    currentUser.customerID = "";  
    currentUser.bWriteAccess = false;
    currentUser.appVersionID = "";
    currentUser.dataVersionID = "";
    /*
    console.log("currentUser.deviceUUID " + currentUser.deviceUUID);
    console.log("currentUser.devicePlatform " + currentUser.devicePlatform);
    console.log("currentUser.deviceManufacturer " + currentUser.deviceManufacturer);
    console.log("currentUser.deviceModel " + currentUser.deviceModel);
    console.log("currentUser.deviceVersion " + currentUser.deviceVersion);
    */
    await this.loginGoogle();
    if(this.firebaseUser)
    {
      console.log("firebaseUser.email " + this.firebaseUser.email);
      let colRef = this.afs.collection<User>(`/UserList/`).ref;
      const query = colRef.where('email','==',this.firebaseUser.email);
      
      return query.get()
      .then(querySnapshot => {
        if(querySnapshot.size > 1)
        {
          currentUser.ID = 'M';
        }
        else if(querySnapshot.size === 1)
        {
          currentUser.ID = querySnapshot.docs[0].id;
          currentUser.server_time = querySnapshot.docs[0].data().server_time;
          currentUser.creation_time = querySnapshot.docs[0].data().creation_time;
          
          currentUser.firstName = querySnapshot.docs[0].data().firstName;
          currentUser.lastName = querySnapshot.docs[0].data().lastName;
          currentUser.mobileNo = querySnapshot.docs[0].data().mobileNo;
          currentUser.email = querySnapshot.docs[0].data().email;
          currentUser.customerID = querySnapshot.docs[0].data().customerID;   
          currentUser.appVersionID = querySnapshot.docs[0].data().appVersionID;   
          currentUser.dataVersionID = querySnapshot.docs[0].data().dataVersionID;         
          currentUser.bWriteAccess = <boolean> querySnapshot.docs[0].data().bWriteAccess;         
        }
        return currentUser;
      });
    }
    else
    {
      console.log("return Promise.reject(currentUser)");
      return Promise.reject(currentUser);
    }    
  }

  async loginGoogle(): Promise<void> 
  {
    console.log("Welcome to AuthProvider::loginGoogle()");
    if(this.firebaseUser)
    {
      console.log(this.firebaseUser.email + " already logged in ");
   //   this.logoutGoogle();
   //   return;
    }

    if(!this.firebaseUser)
    {
      console.log(" No user has logged in...invoking login flow ");
      if(this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova'))
      {
        try 
        {
          const gplusUser = await this.gplus.login({
            'webClientId': '585432762445-otl1m6nlroimfvtp7it86kgrg2vj5490.apps.googleusercontent.com',
           //'webClientId': '585432762445-gc4aiho12127jinov0cd2h0vrkk8l7ic.apps.googleusercontent.com',
            'offline': false,
            'scopes': 'profile email'
          })
          
          console.log("Invoking signInAndRetrieveDataWithCredential");
          const userCredentials = await firebase.auth().signInAndRetrieveDataWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken));
          console.log("completed signInAndRetrieveDataWithCredential");
          //.then(userCredentials => {
            console.log("userCredentials.user.displayName " + userCredentials.user.displayName);
            console.log("userCredentials.user.uid " + userCredentials.user.uid);
          //});
        }
        catch(err) 
        {
          console.log("Error in Native flow  signInAndRetrieveDataWithCredential " + err);
        }
      }
      else
      {
        try {
          const provider = new firebase.auth.GoogleAuthProvider();
        
          const credential = await firebase.auth().signInWithPopup(provider);
          console.log("credential.user.displayName " + credential.user.displayName);
          console.log("credential.user.uid " + credential.user.uid);
     
        } catch(err) {
          console.log("Error in Web flow signInWithPopup " + err);
        }
      }
    }
  }

  /*
  async webGoogleLogin(): Promise<void> 
  {
    console.log("Welcome to AuthProvider::webGoogleLogin()");
    if(this.firebaseUser)
    {
      alert(this.firebaseUser.email + " already logged in ");
   //   this.logoutGoogle();
      return;
    }
    try {
      const provider = new firebase.auth.GoogleAuthProvider();
      const credential = await firebase.auth().signInWithPopup(provider);
    } catch(err) {
      console.log("Error in webGoogleLogin " + err);
    }
  
  }*/

  async logoutGoogle()
  {
    console.log("Welcome to AuthProvider::logoutGoogle");   
    
    try
    {
      await firebase.auth().signOut();
      if(this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova'))
      {
        await this.gplus.logout();
      }
      else
      {
      
      }
    }catch(e)
    {
      console.log("Error from firebase.auth().signOut " + e);
    }
  }

  getAuthenticated(): boolean
  {
    return this.firebaseUser !== null;    
  }

}
