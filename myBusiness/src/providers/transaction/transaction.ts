import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { DocumentReference, WriteBatch } from '@firebase/firestore-types';
//import { WriteBatch, DocumentReference } from '@firebase/firestore-types';
import { EntityProvider } from '../../providers/entity/entity';

import { Injectable } from '@angular/core';

import { BusinessContact } from '../../model/businessContact';
import { BusinessTransaction } from '../../model/businessTransaction';
import { FinanceTransaction } from '../../model/financeTransaction';
import { myConstants,round } from '../../model/myConstants';
import { getDateTimeString } from '../../model/utility';
import { TaxRate,TaxAmount } from '../../model/tax';
import { User,checkIfValid } from '../../model/user';

import { PackageRule } from '../../model/packageRule';
import { PackagedItem, ItemValuation,ItemType,ItemShape,ItemSieve,ItemQualityLevel } from '../../model/packagedItem';
import { InventoryProvider } from '../inventory/inventory';
import { SettingsProvider } from '../settings/settings';
import { SalesSummaryByTSS } from '../../model/salesSummary';
import { SalesItem } from '../../model/salesItem';



/*
  Generated class for the TransactionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.

  This provides CRUD methods for
  (1) businessContact
  (2) businessTransaction
  (3) financeTransaction
*/
@Injectable()
export class TransactionProvider {

  constructor(private afs:AngularFirestore,
    private settingsProvider:SettingsProvider,
    private inventoryProvider:InventoryProvider,
    private entityProvider:EntityProvider) {
    console.log('Hello TransactionProvider Provider');
    afs.firestore.settings({ timestampsInSnapshots: true });
  }


  /* CRUD for businessTransaction */
  async createBusinessTransaction(user:User,bizTrx:BusinessTransaction,salesSummaryByTSSList:Array<SalesSummaryByTSS>):Promise<string>
  {
    console.log("Welcome to TransactionProvider:createBusinessTransaction(BusinessTransaction)");
    /*
    For BUY transaction
      (1) Insert BusinessTransaction
      (2) Insert FinanceTransaction
      (3) Insert PackagedItem
    For SELL transaction
      (1) Delete old BusinessTransaction
      (2) Delete old FinanceTransaction //There won't be any FinTrx for Quotation
      (3) Delete old SalesItem
      (4) Insert new BusinessTransaction
      (5) Insert FinanceTransaction if OPEN trx i.e. Invoice instead of Quotation
      (6) Insert new SalesItem
    */

    //return new Promise((resolve,reject)=>{
    
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        return(null);
      }
      let batch:WriteBatch = this.afs.firestore.batch();
      
      if(bizTrx.trxType === myConstants.trxType.SELL)
      {
        let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList/${bizTrx.ID}`).ref;
    
        if(docRef)
        {
          batch.delete(docRef);
        
          let finColRef = this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`).ref; 
          let query = finColRef.where('BizTrxID','==',bizTrx.ID);//.orderBy('creation_time','asc');
          
          let querySnapshot = await query.get();
          console.log("No. of Fin Trx " + querySnapshot.size);
          for(let i=0;i<querySnapshot.size;i++)
          {
            batch.delete(querySnapshot.docs[i].ref);
          }

          let salesItemColRef = this.afs.collection<SalesItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_salesItemList`).ref; 
          query = salesItemColRef.where('trxID','==',bizTrx.ID);//.orderBy('creation_time','asc');
          querySnapshot = await query.get();
          console.log("No. of Sales Item " + querySnapshot.size);
          for(let i=0;i<querySnapshot.size;i++)
          {
            //querySnapshot.docs[i].ref.delete();
            batch.delete(querySnapshot.docs[i].ref);
          }              
        }//end of If BusinessTransaction
      }
      
      const current_time:string = getDateTimeString(new Date());
      let newBizTrxID : string = "";
      if(bizTrx.trxType === myConstants.trxType.BUY)
      {
        newBizTrxID = bizTrx.trxPartyName + "_" + bizTrx.trxDate + "_" + "B" + "_" + bizTrx.trxWeight + "cts_" + bizTrx.trxCurrency + "_" + (current_time);
      }
      else if(bizTrx.trxType === myConstants.trxType.SELL)
      {
        newBizTrxID = bizTrx.trxPartyName + "_" + bizTrx.trxDate + "_" + "S" + "_" + bizTrx.trxWeight + "cts_"+ bizTrx.trxCurrency + "_" + (current_time);
      }
      bizTrx.ID = newBizTrxID;
      bizTrx.server_time = "";
      bizTrx.creation_time = current_time;

      //let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/businessContactList/${bizTrx.trxPartyID}/businessTransactionList/${bizTrx.ID}`).ref;
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList/${bizTrx.ID}`).ref;
      if(docRef)
      {
        batch.set(docRef,{
          ID:bizTrx.ID,
          server_time:firebase.firestore.FieldValue.serverTimestamp(),
          creation_time:bizTrx.creation_time,    
          userID:user.ID,
          customerID:user.customerID,
          confidentialityLevel:bizTrx.confidentialityLevel,        
          trxDate: bizTrx.trxDate, 
          trxType: bizTrx.trxType,        
          trxPartyID: bizTrx.trxPartyID,
          trxPartyName: bizTrx.trxPartyName,
          trxBrokerID: bizTrx.trxBrokerID,
          trxBrokerName: bizTrx.trxBrokerName,        
          trxParentID: bizTrx.trxParentID,
          trxInvoiceID: bizTrx.trxInvoiceID,        
          trxWeight: bizTrx.trxWeight,
          trxRate: bizTrx.trxRate,
          trxTaxRate: bizTrx.trxTaxRate,      
          trxGrossRate:bizTrx.trxGrossRate,  
          trxBrokerageRate :bizTrx.trxBrokerageRate,
          trxValue: bizTrx.trxValue,
          trxTaxAmount: bizTrx.trxTaxAmount,
          trxGrossAmount: bizTrx.trxGrossAmount,
          trxBrokerageAmount: bizTrx.trxBrokerageAmount,        
          trxCurrency : bizTrx.trxCurrency,
          trxCurrencyRate : bizTrx.trxCurrencyRate,
          trxDueDays: bizTrx.trxDueDays,
          trxValidityDays: bizTrx.trxValidityDays,
          trxDueDate: bizTrx.trxDueDate,
          trxROIForEarlyPayment: bizTrx.trxROIForEarlyPayment,
          trxROIForLatePayment: bizTrx.trxROIForLatePayment,
          trxStatus: bizTrx.trxStatus,
          trxRemarks: bizTrx.trxRemarks,
          trxItems: bizTrx.trxItems
          //summary:bizTrx.summary
        });
      
        if(bizTrx.trxStatus === myConstants.bizTrxStatus.OPEN)
        {
          const finTrxID : string = bizTrx.trxPartyName + "_" + bizTrx.trxDate + "_" + "DUE" + "_" + bizTrx.trxCurrency + "_" + (current_time);
        
          let finTrx = {} as FinanceTransaction;
          finTrx.BizTrxID = newBizTrxID;
          finTrx.confidentialityLevel = bizTrx.confidentialityLevel;
          finTrx.BizInvoiceID = bizTrx.trxInvoiceID;
          finTrx.trxDate = bizTrx.trxDate;
          finTrx.trxPartyID = bizTrx.trxPartyID;
          finTrx.trxPartyName = bizTrx.trxPartyName;
          finTrx.trxCurrency = bizTrx.trxCurrency;
          finTrx.trxCurrencyRate = bizTrx.trxCurrencyRate; //TBC
          finTrx.trxInterestRealizedinLocal = 0;
          finTrx.trxInterestRealizedinForiegn= 0;
          finTrx.trxOpeningBalance = 0;
          finTrx.trxRemarks = bizTrx.trxRemarks;
          finTrx.trxPaymentMethod = "";
          finTrx.trxDiffDays = 0;
          
          if(bizTrx.trxType === myConstants.trxType.BUY)
          {
            finTrx.trxType = myConstants.trxType.DUE_DEBIT;
          }
          else if(bizTrx.trxType === myConstants.trxType.SELL)
          {
            finTrx.trxType = myConstants.trxType.DUE_CREDIT;
          }
          else
          {
            //invalid TBC
          }
          
          if(bizTrx.trxCurrency === myConstants.foreignCurrency)
          {
            finTrx.trxForiegnValue = round((bizTrx.trxGrossAmount),2);
            finTrx.trxLocalValue = round((finTrx.trxForiegnValue * bizTrx.trxCurrencyRate),0);
          
            finTrx.trxClosingBalance = round(finTrx.trxForiegnValue,2);
            
          }
          else if(bizTrx.trxCurrency === myConstants.localCurrency)
          {
            finTrx.trxLocalValue = round((bizTrx.trxGrossAmount),0);
            finTrx.trxForiegnValue = round((finTrx.trxLocalValue / bizTrx.trxCurrencyRate),2);

            finTrx.trxClosingBalance = round(finTrx.trxLocalValue,0);
          }
        
          let finTrxRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList/${finTrxID}`).ref;
          if(finTrxRef)
          {
            batch.set(finTrxRef,{
              ID:finTrxID,
              server_time:firebase.firestore.FieldValue.serverTimestamp(),
              creation_time:current_time,    
              userID:user.ID,
              customerID:user.customerID,
              confidentialityLevel:finTrx.confidentialityLevel,
              BizTrxID: finTrx.BizTrxID,
              BizInvoiceID: finTrx.BizInvoiceID,
              trxDate: finTrx.trxDate, 
              trxType: finTrx.trxType,
              trxPartyID: finTrx.trxPartyID,
              trxPartyName: finTrx.trxPartyName,
              trxForiegnValue: finTrx.trxForiegnValue,
              trxLocalValue: finTrx.trxLocalValue,
              trxCurrency : finTrx.trxCurrency,
              trxCurrencyRate : finTrx.trxCurrencyRate,
              trxInterestRealizedinLocal: finTrx.trxInterestRealizedinLocal,
              trxInterestRealizedinForiegn: finTrx.trxInterestRealizedinForiegn,
              trxDiffDays: finTrx.trxDiffDays,
              trxOpeningBalance: finTrx.trxOpeningBalance,
              trxClosingBalance: finTrx.trxClosingBalance,
              trxPaymentMethod: finTrx.trxPaymentMethod,
              trxRemarks: finTrx.trxRemarks
            });     
          }//end of if(finTrxRef)    
        }      

        if(bizTrx.trxType === myConstants.trxType.BUY)
        {
          let pRules:Array<PackageRule>;
          let pItem = {} as PackagedItem;
        
          pRules = await this.settingsProvider.getPackageRule_For_A(user,myConstants.taskCode.PURCHASE);
          console.log("addItem=>pRules.length " + pRules.length);
        
          if(pRules.length === 1)
          {
            console.log("pRule " + pRules[0].curTaskCode + " " + pRules[0].nextTaskCode);
            pItem.confidentialityLevel = bizTrx.confidentialityLevel;
            pItem.itemWeight = +bizTrx.trxWeight;// data.InputWeight;
            
            
            pItem.taskCost = 0; //data.InputCost;
            pItem.accumulatedTaskCost = +0;//data.InputTax;
            pItem.roughWeight = +bizTrx.trxWeight;
            pItem.itemQty = 0;
            pItem.itemRemarks = '';
            pItem.pRule = pRules[0];

            if(bizTrx.trxType === myConstants.trxType.BUY)
            {
              pItem.itemType = await this.settingsProvider.getItemType(user,myConstants.itemType.ROUGH);// data.InputWeight;
            }
            else if(bizTrx.trxType === myConstants.trxType.SELL)
            {
              pItem.itemType = await this.settingsProvider.getItemType(user,myConstants.itemType.POLISHED);// data.InputWeight;
            }
            
            
            pItem.itemShape = {} as ItemShape;
            pItem.itemShape.id = myConstants.itemShape.NOTDEFINED;
            pItem.itemShape.creation_time = current_time;
            pItem.itemShape.rank = 999;
            pItem.itemShape.name = myConstants.itemShape.NOTDEFINED;
            pItem.itemShape.remarks = "Not identified";
          
            pItem.itemSieve = {} as ItemSieve;
            pItem.itemSieve.id = myConstants.itemSieve.NOTDEFINED;
            pItem.itemSieve.creation_time = current_time;
            pItem.itemSieve.rank = 999;
            pItem.itemSieve.name = myConstants.itemSieve.NOTDEFINED;
            pItem.itemSieve.remarks = "Not identified";

            pItem.itemQualityLevel = {} as ItemQualityLevel;
            pItem.itemQualityLevel.id = myConstants.polishQualityLevel.NOTDEFINED;
            pItem.itemQualityLevel.creation_time = current_time;
            pItem.itemQualityLevel.rank = 999;
            pItem.itemQualityLevel.name = myConstants.polishQualityLevel.NOTDEFINED;
            pItem.itemQualityLevel.remarks = "Not identified";

            pItem.packageID = '';
            pItem.parentID = "";
            pItem.packageGroupID = ""+bizTrx.trxWeight;
            pItem.packageGroupName = ""+bizTrx.trxWeight;
            pItem.packageStatus = myConstants.packageStatus.RD;

            pItem.trxID = bizTrx.ID;
            pItem.trxPartyID = bizTrx.trxPartyID;
            
            let contact = {} as BusinessContact;
            contact = await this.entityProvider.getBusinessContact(user,bizTrx.trxPartyID);
            pItem.trxPartyName = contact.fullName;
            pItem.trxWeight = bizTrx.trxWeight;
            pItem.trxRate = bizTrx.trxRate;
            pItem.trxCurrency = bizTrx.trxCurrency;
            pItem.trxDate = bizTrx.trxDate;
            
            pItem.trxShare = +100;
            
            pItem.itemValuation = {} as ItemValuation;
            pItem.itemValuation.interestCost = +0;
            pItem.itemValuation.totalCost = +0;
            pItem.itemValuation.rate = +0;
            pItem.itemValuation.value = +0;
            pItem.itemValuation.profit = +0;
            pItem.itemValuation.profitMargin = +0;

            const ID: string = pItem.pRule.curTaskCode + "_" + pItem.pRule.nextTaskCode + "_" + pItem.itemWeight + "_" + (getDateTimeString(new Date()));
            console.log("ID " + ID);
          //return new Promise((resolve,reject)=>{  
            let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${ID}`).ref;
            console.log("docRef: " + docRef.id);
            if(docRef)
            {
              console.log("Calling batch.set for docRef " + docRef.id);
              batch.set(docRef,{
                id:ID,
                creation_time:getDateTimeString(new Date()),
                server_time:firebase.firestore.FieldValue.serverTimestamp(),
                userID:user.ID,
                customerID:user.customerID,
                confidentialityLevel:pItem.confidentialityLevel,
                packageID:pItem.packageID,
                parentID:pItem.parentID,
                packageGroupID:pItem.packageGroupID,
                packageGroupName:pItem.packageGroupName,
                packageStatus:pItem.packageStatus,
                pRule:pItem.pRule,
                trxID:pItem.trxID,
                trxPartyID:pItem.trxPartyID,
                trxPartyName:pItem.trxPartyName,
                trxDate:pItem.trxDate,              
                trxWeight:pItem.trxWeight,  
                trxRate:pItem.trxRate,  
                trxCurrency:pItem.trxCurrency,              
                trxShare:pItem.trxShare,
                itemValuation:pItem.itemValuation,
                itemWeight:pItem.itemWeight,
                itemType:pItem.itemType,
                itemQty:pItem.itemQty,
                taskCost:pItem.taskCost,
                accumulatedTaskCost:pItem.accumulatedTaskCost,
                roughWeight:pItem.roughWeight,
                itemShape:pItem.itemShape,       
                itemSieve:pItem.itemSieve,
                itemQualityLevel:pItem.itemQualityLevel,     
                itemRemarks:pItem.itemRemarks    
              });
            }    
          }//end of if(pRules.length === 1)
          else
          {
            console.log("No rules found for BUY");
            return null;
          }
        }// end of if(bizTrx.trxType === myConstants.trxType.BUY)
        else if((bizTrx.trxType === myConstants.trxType.SELL) && (salesSummaryByTSSList.length > 0))
        {          
          let salesItem = new SalesItem();
          for(let i=0;i<salesSummaryByTSSList.length;i++)
          {
            for(let j=0;j<salesSummaryByTSSList[i].salesItemList.length;j++)
            {
              if(salesSummaryByTSSList[i].salesItemList[j].saleableWeight > 0)
              {
                salesItem = salesSummaryByTSSList[i].salesItemList[j];
                salesItem.trxID = newBizTrxID;
                const ID: string = bizTrx.trxPartyName + "_" + bizTrx.trxDate + "_" + bizTrx.trxWeight + "cts_"+ bizTrx.trxCurrency + "_" + salesItem.saleableWeight + "cts_" + this.afs.createId();
                console.log("salesItem ID " + ID);
              //return new Promise((resolve,reject)=>{  
                let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_salesItemList/${ID}`).ref;
                if(docRef)
                {
                  console.log("Calling batch.set for docRef " + docRef.id);
                  batch.set(docRef,{
                    id:ID,
                    creation_time:getDateTimeString(new Date()),
                    server_time:firebase.firestore.FieldValue.serverTimestamp(),
                    userID:user.ID,
                    customerID:user.customerID,
                    itemType:salesItem.itemType,
                    itemShape:salesItem.itemShape,
                    itemSieve:salesItem.itemSieve,
                    itemQualityLevel:salesItem.itemQualityLevel,
                    saleableWeight:salesItem.saleableWeight,
                    purchaseWeight:salesItem.purchaseWeight,
                    qty:salesItem.qty,
                    taxRate:salesItem.taxRate,
                    valueFactor:salesItem.valueFactor,
                    avgPriceRate:salesItem.avgPriceRate,                
                    avgSalePrice:salesItem.avgSalePrice,
                    avgSalesTax:salesItem.avgSalesTax,
                    avgGrossPrice:salesItem.avgGrossPrice,
                    avgSalesBrokerage:salesItem.avgSalesBrokerage,
                    avgNetPrice:salesItem.avgNetPrice,
                    avgSalesGain:salesItem.avgSalesGain,
                    avgPurchaseCost:salesItem.avgPurchaseCost,            
                    avgInterestCost:salesItem.avgInterestCost, 
                    avgMfgCost:salesItem.avgMfgCost, 
                    avgGrossCost:salesItem.avgGrossCost, 
                    avgGrossCostAtDueTerm:salesItem.avgGrossCostAtDueTerm,
                    avgProfit:salesItem.avgProfit,            
                    avgProfitMargin:salesItem.avgProfitMargin, 
                    totalPriceValue:salesItem.totalPriceValue,            
                    totalSaleValue:salesItem.totalSaleValue,       
                    totalSalesTax:salesItem.totalSalesTax,
                    totalGrossValue:salesItem.totalGrossValue,
                    totalSalesBrokerage:salesItem.totalSalesBrokerage,
                    totalNetValue:salesItem.totalNetValue,
                    totalPurchaseCost:salesItem.totalPurchaseCost,
                    totalInterestCost:salesItem.totalInterestCost,
                    totalMfgCost:salesItem.totalMfgCost,
                    totalGrossCost:salesItem.totalMfgCost,
                    totalGrossCostAtDueTerm:salesItem.totalGrossCostAtDueTerm,
                    totalProfit:salesItem.totalProfit,            
                    totalProfitMargin:salesItem.totalProfitMargin,
                    totalSalesGain:salesItem.totalSalesGain,
                    trxID:salesItem.trxID,            
                    trxStatus:salesItem.trxStatus,
                    trxDate:salesItem.trxDate,
                    trxCurrency:salesItem.trxCurrency,
                    trxCurrencyRate:salesItem.trxCurrencyRate,
                    trxDueDays:salesItem.trxDueDays,
                    trxValidityDays:salesItem.trxValidityDays,
                    trxROIForLatePayment:salesItem.trxROIForLatePayment,
                    trxBrokerageRate:salesItem.trxBrokerageRate
                  });
                }
              }            
            }
          }
        }
        
        try
        {
          await batch.commit();  
        }
        catch(e)
        {
          console.log("Error from batch.commit() " + e);
          return(null);
        };
      }//end of If(docRef)
      else
      {
        console.log("Firestore document can not be created for this transaction !!");
        return null;
      }
  //  });
  }

  getAllBusinessTransactions(user:User,type:string):AngularFirestoreCollection<BusinessTransaction>
  {
    console.log("Welcome to TransactionProvider:getAllBusinessTransactions(): " + type);
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    if(type)
    {
      return this.afs.collection<BusinessTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList`, 
                                                  ref => ref.where('trxType','==',type)
                                                            .orderBy('trxDueDate')
                                                            .orderBy('trxValue','desc')); 
    }
    else
    {
      return this.afs.collection<BusinessTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList`, 
                                                  ref => ref.orderBy('trxDueDate')
                                                            .orderBy('trxValue','desc')); 
    }
    
  }

  getBusinessTransactionsForPartyID(user:User,contactID:string):AngularFirestoreCollection<BusinessTransaction>
  {
    console.log("Welcome to TransactionProvider:getBusinessTransactionsForPartyID(contactID): " + contactID);
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<BusinessTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList`, 
                                                  ref => ref.where('trxPartyID','==',contactID)
                                                            .orderBy('trxDueDate')
                                                            .orderBy('trxValue','desc')); 
  }

  getBusinessTransactionListForPartyID(user:User,contactID:string):Promise<BusinessTransaction[]>
  {
    console.log("Welcome to TransactionProvider:getBusinessTransactionsListForPartyID(contactID): " + contactID);
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      let bizTrxs:Array<BusinessTransaction> = [];
      const colRef = this.afs.collection<BusinessTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList`, 
                                                    ref => ref.where('trxPartyID','==',contactID)
                                                              .orderBy('trxDueDate')
                                                              .orderBy('trxValue','desc')).ref; 
      colRef.get().then(querySnapshot => {
        console.log("querySnapshot " + querySnapshot.size);
        bizTrxs = [];
        for(let i = 0; i < querySnapshot.size; i++)
        {
          let bizTransaction = {} as BusinessTransaction;
          bizTransaction.ID = querySnapshot.docs[i].id;
          bizTransaction.server_time = querySnapshot.docs[i].data().server_time;
          bizTransaction.creation_time = querySnapshot.docs[i].data().creation_time;
          bizTransaction.userID = querySnapshot.docs[i].data().userID;
          bizTransaction.customerID = querySnapshot.docs[i].data().customerID;
          bizTransaction.confidentialityLevel = querySnapshot.docs[i].data().confidentialityLevel;
          
          bizTransaction.trxDate = querySnapshot.docs[i].data().trxDate;
          bizTransaction.trxType = querySnapshot.docs[i].data().trxType;
          
          bizTransaction.trxPartyID = querySnapshot.docs[i].data().trxPartyID;
          bizTransaction.trxPartyName = querySnapshot.docs[i].data().trxPartyName;
          bizTransaction.trxBrokerID  = querySnapshot.docs[i].data().trxBrokerID;
          bizTransaction.trxBrokerName  = querySnapshot.docs[i].data().trxBrokerName;
          
          bizTransaction.trxParentID = querySnapshot.docs[i].data().trxParentID;
          bizTransaction.trxInvoiceID = querySnapshot.docs[i].data().trxInvoiceID;

          bizTransaction.trxWeight = querySnapshot.docs[i].data().trxQty;
        
          bizTransaction.trxRate = querySnapshot.docs[i].data().trxRate;
          bizTransaction.trxTaxRate  = querySnapshot.docs[i].data().trxTaxRate;
          bizTransaction.trxGrossRate  = querySnapshot.docs[i].data().trxGrossRate;
          bizTransaction.trxBrokerageRate  = querySnapshot.docs[i].data().trxBrokerageRate;

          bizTransaction.trxValue = querySnapshot.docs[i].data().trxValue;
          bizTransaction.trxTaxAmount  = querySnapshot.docs[i].data().trxTaxAmount;
          bizTransaction.trxGrossAmount  = querySnapshot.docs[i].data().trxGrossAmount;
          bizTransaction.trxBrokerageAmount  = querySnapshot.docs[i].data().trxBrokerageAmount;
          
          bizTransaction.trxCurrency  = querySnapshot.docs[i].data().trxCurrency;
          bizTransaction.trxCurrencyRate  = querySnapshot.docs[i].data().trxCurrencyRate;
          bizTransaction.trxDueDays = querySnapshot.docs[i].data().trxDueDays;
          bizTransaction.trxValidityDays = querySnapshot.docs[i].data().trxValidityDays;
          bizTransaction.trxDueDate = querySnapshot.docs[i].data().trxDueDate;
          bizTransaction.trxROIForEarlyPayment = querySnapshot.docs[i].data().trxROIForEarlyPayment;
          bizTransaction.trxROIForLatePayment = querySnapshot.docs[i].data().trxROIForLatePayment;
          
          bizTransaction.trxStatus = querySnapshot.docs[i].data().trxStatus;
          bizTransaction.trxRemarks = querySnapshot.docs[i].data().trxRemarks;
          bizTransaction.trxItems = querySnapshot.docs[i].data().trxItems;

          bizTrxs.push(bizTransaction);
        }
        resolve(bizTrxs);
      }).catch(e => {
        console.log("Error from colRef.get() " + e);
      });
    });    
  }

  getOpenBusinessTransactionsForPartyID(user:User,contactID:string,currency:string):Promise<BusinessTransaction>
  {
    console.log("Welcome to TransactionProvider:getOpenBusinessTransactionsForPartyID(contactID) : " + contactID);
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      let colRef = this.afs.collection<BusinessTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList`).ref; 
      
      const query = colRef.where('trxPartyID','==',contactID)
                          .where('trxStatus','==',myConstants.bizTrxStatus.OPEN)
                          .where('trxCurrency','==',currency)
                          .orderBy('trxDueDate',"asc").orderBy('trxValue',"desc")
                          .limit(1);
      query.get()
      .then(querySnapshot => {
        console.log("querySnapshot " + querySnapshot.size);
        if(querySnapshot.size > 0)
        {
          console.log("doc.data().trxQty : " + querySnapshot.docs[0].data().trxQty);
  
          let openBizTrx = {} as BusinessTransaction;
        //  openBizTrx.summary = {} as BusinessTransactionSummary;
            
          openBizTrx.ID = querySnapshot.docs[0].id;
          openBizTrx.server_time = querySnapshot.docs[0].data().server_time;
          openBizTrx.creation_time = querySnapshot.docs[0].data().creation_time;
          openBizTrx.userID = querySnapshot.docs[0].data().userID;
          openBizTrx.customerID = querySnapshot.docs[0].data().customerID;
          openBizTrx.confidentialityLevel = querySnapshot.docs[0].data().confidentialityLevel;
  
          openBizTrx.trxDate = querySnapshot.docs[0].data().trxDate;
          openBizTrx.trxType = querySnapshot.docs[0].data().trxType;
          openBizTrx.trxPartyID = querySnapshot.docs[0].data().trxPartyID;
          openBizTrx.trxPartyName = querySnapshot.docs[0].data().trxPartyName;
          openBizTrx.trxBrokerID = querySnapshot.docs[0].data().trxBrokerID;
          openBizTrx.trxBrokerName = querySnapshot.docs[0].data().trxBrokerName;
          
          openBizTrx.trxParentID = querySnapshot.docs[0].data().trxParentID;
          openBizTrx.trxInvoiceID = querySnapshot.docs[0].data().trxInvoiceID;

          openBizTrx.trxWeight = querySnapshot.docs[0].data().trxWeight;
          
          openBizTrx.trxRate = querySnapshot.docs[0].data().trxRate;
          openBizTrx.trxTaxRate = querySnapshot.docs[0].data().trxTaxRate;
          openBizTrx.trxGrossRate = querySnapshot.docs[0].data().trxGrossRate;
          openBizTrx.trxBrokerageRate = querySnapshot.docs[0].data().trxBrokerageRate;

          openBizTrx.trxValue = querySnapshot.docs[0].data().trxValue;
          openBizTrx.trxTaxAmount = querySnapshot.docs[0].data().trxTaxAmount;
          openBizTrx.trxGrossAmount = querySnapshot.docs[0].data().trxGrossAmount;
          openBizTrx.trxBrokerageAmount = querySnapshot.docs[0].data().trxBrokerageAmount;
 
          openBizTrx.trxCurrency  = querySnapshot.docs[0].data().trxCurrency;
          openBizTrx.trxCurrencyRate  = querySnapshot.docs[0].data().trxCurrencyRate;
          openBizTrx.trxDueDays = querySnapshot.docs[0].data().trxDueDays;
          openBizTrx.trxValidityDays = querySnapshot.docs[0].data().trxValidityDays;
          openBizTrx.trxDueDate = querySnapshot.docs[0].data().trxDueDate;
          openBizTrx.trxROIForEarlyPayment = querySnapshot.docs[0].data().trxROIForEarlyPayment;
          openBizTrx.trxROIForLatePayment = querySnapshot.docs[0].data().trxROIForLatePayment;
          
          openBizTrx.trxStatus = querySnapshot.docs[0].data().trxStatus;
          openBizTrx.trxRemarks = querySnapshot.docs[0].data().trxRemarks;
          openBizTrx.trxItems = querySnapshot.docs[0].data().trxItems;
      //    openBizTrx.summary = querySnapshot.docs[0].data().summary;
  
          console.log("openBizTrx : " + openBizTrx.ID + " " + openBizTrx.trxValue + " " + openBizTrx.trxDueDate);
          resolve(openBizTrx);          
        }
        else
        {
          resolve(null);   
        }
      });
    });    
  }

  getBusinessTransaction(user:User,trxID:string):Promise<BusinessTransaction>
  {
    console.log("Welcome to TransactionProvider:getBusinessTransaction()");
    return new Promise((resolve,reject) => {

      if(!(checkIfValid(user)  && (trxID)))
      {
        console.log("Invalid user or inputs");
        resolve(null);
      }
      let bizTrx = new BusinessTransaction();            
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList/${trxID}`).ref;
      
      return docRef.get().then(doc => {
          if (!doc.exists) {
              console.log('No such document!' + doc.id);
              bizTrx.ID="";
              return null;
          } 
          else 
          {
            bizTrx.ID = doc.id;
            bizTrx.server_time = doc.data().server_time;
            bizTrx.creation_time = doc.data().creation_time;
            bizTrx.userID = doc.data().userID;
            bizTrx.customerID = doc.data().customerID;
            bizTrx.confidentialityLevel = doc.data().confidentialityLevel;
            
            bizTrx.trxDate = doc.data().trxDate;
            bizTrx.trxType = doc.data().trxType;
            bizTrx.trxPartyID = doc.data().trxPartyID;
            bizTrx.trxPartyName = doc.data().trxPartyName;
            bizTrx.trxBrokerID = doc.data().trxBrokerID;
            bizTrx.trxBrokerName = doc.data().trxBrokerName;
          
            bizTrx.trxParentID = doc.data().trxParentID;
            bizTrx.trxInvoiceID = doc.data().trxInvoiceID;
          
            bizTrx.trxWeight = doc.data().trxWeight;

            bizTrx.trxRate = doc.data().trxRate;
            bizTrx.trxTaxRate = doc.data().trxTaxRate;
            bizTrx.trxGrossRate = doc.data().trxGrossRate;
            bizTrx.trxBrokerageRate = doc.data().trxBrokerageRate;            
            bizTrx.trxValue = doc.data().trxValue;
            bizTrx.trxTaxAmount = doc.data().trxTaxAmount;
            bizTrx.trxGrossAmount = doc.data().trxGrossAmount;
            bizTrx.trxBrokerageAmount = doc.data().trxBrokerageAmount;
          
          
            bizTrx.trxCurrency  = doc.data().trxCurrency;
            bizTrx.trxCurrencyRate  = doc.data().trxCurrencyRate;
            bizTrx.trxDueDays = doc.data().trxDueDays;
            bizTrx.trxValidityDays = doc.data().trxValidityDays;
            bizTrx.trxDueDate = doc.data().trxDueDate;
            bizTrx.trxROIForEarlyPayment = doc.data().trxROIForEarlyPayment;
            bizTrx.trxROIForLatePayment = doc.data().trxROIForLatePayment;
          
            bizTrx.trxStatus = doc.data().trxStatus;
            bizTrx.trxRemarks = doc.data().trxRemarks;
            bizTrx.trxItems = doc.data().trxItems;
            resolve(bizTrx);
          }
      })
      .catch(err => {
          console.log('Error getting BusinessTransaction document', err);
          resolve(null);
      });
    });
  }

  deleteBusinessTransaction(user:User,bizTrxID:string):Promise<boolean>
  {
    console.log("Welcome to TransactionProvider:deleteBusinessTransaction(partyID,bizTrxID)");
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      if(!bizTrxID)
      {
        console.log("non-existed trx");
        resolve(true);
      }
      let batch:WriteBatch = this.afs.firestore.batch();
  
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList/${bizTrxID}`).ref;
  
      if(docRef)
      {
        batch.delete(docRef);
      
        let finColRef = this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`).ref; 
        let query = finColRef.where('BizTrxID','==',bizTrxID);//.orderBy('creation_time','asc');
        
        query.get()
        .then(querySnapshot => 
        {
          console.log("No. of Fin Trx " + querySnapshot.size);
          for(let i=0;i<querySnapshot.size;i++)
          {
            batch.delete(querySnapshot.docs[i].ref);
          }

          let salesItemColRef = this.afs.collection<SalesItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_salesItemList`).ref; 
          let query = salesItemColRef.where('trxID','==',bizTrxID);//.orderBy('creation_time','asc');
          query.get()
          .then(querySnapshot => 
          {
            console.log("No. of Sales Item " + querySnapshot.size);
            for(let i=0;i<querySnapshot.size;i++)
            {
              //querySnapshot.docs[i].ref.delete();
              batch.delete(querySnapshot.docs[i].ref);
            }
            batch.commit()
            .then(() => {
              console.log("committed");    
              resolve(true);
            }).catch(e => {
              console.log("Error from batch.commit() " + e);
              reject(false);
            });
          });//end of salesItem query           
        });//end of FinTrx query
      }//end of If BusinessTransaction
    });    
  }

  getInterestForBizTrxID(user:User,contactID:string,bizTrxID:string,currencyExchangeRate:number):Promise<number>
  {
    console.log("Welcome to TransactionProvider:getInterestForBizTrxID() for bizTrxID " + bizTrxID);
    return new Promise((resolve,reject) => {
      if(!(checkIfValid(user) && (contactID) && (bizTrxID)))
      {
        console.log("Invalid user");
        resolve(0);
      }
      let interestForBizTrx:number = 0;

      this.getBusinessTransaction(user,bizTrxID)
      .then(businessTransaction => 
      {
        let bizTrx = {} as BusinessTransaction;
        bizTrx = businessTransaction;
        //let finTrxList:Array<FinanceTransaction> = [];
        //finTrxList = await 
        this.getFinanceTransactionsListForBizTrxID(user,bizTrxID)
        .then(trxList => 
        {
          let finTrxList:Array<FinanceTransaction>;
          finTrxList = trxList;
          //console.log("finTrxList.length " + finTrxList.length);
          let interestRealizedLocal = 0;
          let interestUnRealizedLocal = 0;
          
          for(let j = 0;j<finTrxList.length;j++)
          {
            interestRealizedLocal += round((+finTrxList[j].trxInterestRealizedinLocal),2);
          }
          
          let today: Date = new Date();
          let dueDate: Date = (new Date(bizTrx.trxDueDate));
          let duration: number = today.valueOf() - dueDate.valueOf();
          let diffDays:number = Math.floor(duration / (1000 * 3600 * 24)); 
          if(bizTrx.trxCurrency === myConstants.localCurrency)
          {
            if(diffDays > 0)
            {
              interestUnRealizedLocal = round(((finTrxList[finTrxList.length-1].trxClosingBalance*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
            else
            {
              interestUnRealizedLocal = round(((finTrxList[finTrxList.length-1].trxClosingBalance*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
          }
          else if(bizTrx.trxCurrency === myConstants.foreignCurrency)
          { 
            if(diffDays > 0)
            {
              interestUnRealizedLocal = round(((finTrxList[finTrxList.length-1].trxClosingBalance*currencyExchangeRate*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
            else
            {
              interestUnRealizedLocal = round(((finTrxList[finTrxList.length-1].trxClosingBalance*currencyExchangeRate*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
          }  
          interestForBizTrx = round((+interestUnRealizedLocal + +interestRealizedLocal),2); 
          resolve(interestForBizTrx);                
        }).catch(e => {
          console.log("Error from transactionProvider.getFinanceTransactionsListForBizTrxID() " + e);
          reject(0);
        });
      }).catch(e => {
        console.log("Error from transactionProvider.getBusinessTransaction " + e);
        reject(0);
      });
    });
  }

  /* CRUD for financeTransaction */
  createFinanceTransaction(user:User,finTrx:FinanceTransaction):Promise<string>
  {
    console.log("Welcome to TransactionProvider:createFinanceTransaction(FinanceTransaction)");
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      const current_time:string = getDateTimeString(new Date());
      let finTrxID : string = "";
      if(finTrx.trxCurrency === myConstants.localCurrency)
      {
        finTrxID = finTrx.trxPartyName + "_" + finTrx.trxDate + "_" + finTrx.trxLocalValue + "_" + finTrx.trxCurrency + "_" + (current_time);
      }
      else if(finTrx.trxCurrency === myConstants.foreignCurrency)
      {
        finTrxID = finTrx.trxPartyName + "_" + finTrx.trxDate + "_" + finTrx.trxForiegnValue + "_" + finTrx.trxCurrency + "_" + (current_time);
      }
      
      let batch:WriteBatch = this.afs.firestore.batch();

      let finTrxRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList/${finTrxID}`).ref;
      if(finTrxRef)
      {
        batch.set(finTrxRef,{
          ID:finTrxID,
          server_time:firebase.firestore.FieldValue.serverTimestamp(),
          creation_time:current_time,    
          userID:user.ID,
          customerID:user.customerID,
          confidentialityLevel:finTrx.confidentialityLevel,
          BizTrxID: finTrx.BizTrxID,
          BizInvoiceID: finTrx.BizInvoiceID,
          trxDate: finTrx.trxDate, 
          trxType: finTrx.trxType,
          trxPartyID: finTrx.trxPartyID,
          trxPartyName: finTrx.trxPartyName,
          trxForiegnValue: finTrx.trxForiegnValue,
          trxLocalValue: finTrx.trxLocalValue,
          trxCurrency : finTrx.trxCurrency,
          trxCurrencyRate : finTrx.trxCurrencyRate,
          trxInterestRealizedinLocal: finTrx.trxInterestRealizedinLocal,
          trxInterestRealizedinForiegn: finTrx.trxInterestRealizedinForiegn,
          trxDiffDays: finTrx.trxDiffDays,
          trxOpeningBalance: finTrx.trxOpeningBalance,
          trxClosingBalance: finTrx.trxClosingBalance,
          trxPaymentMethod: finTrx.trxPaymentMethod,
          trxRemarks: finTrx.trxRemarks
        });     
      }
      
      if(finTrx.trxClosingBalance === 0)
      {//Close relevant Business Transaction
        let bizTrxRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList/${finTrx.BizTrxID}`).ref;

        if(bizTrxRef)
        {
          batch.update(bizTrxRef,{
            server_time:firebase.firestore.FieldValue.serverTimestamp(),
            trxStatus: myConstants.bizTrxStatus.CLOSED,
          });
        }
      }
      batch.commit()
      .then(() => {
        console.log("committed");    
        resolve(finTrxID);
      }).catch(e => {
        console.log("Error from batch.commit() " + e);
        reject(null);
      }); 
    });
    
  }

  getAllFinanceTransactions(user:User):AngularFirestoreCollection<FinanceTransaction>
  {
    console.log("Welcome to TransactionProvider:getAllFinanceTransactions()");
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`, 
                                                  ref => ref.orderBy('BizTrxID').orderBy('creation_time')); 
  }
  
  getFinanceTransactionsForPartyID(user:User,contactID:string):AngularFirestoreCollection<FinanceTransaction>
  {
    console.log("Welcome to TransactionProvider:getFinanceTransactionsForPartyID(partyID)");
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`, 
                                                  ref => ref.where('trxPartyID','==',contactID)
                                                            .orderBy('creation_time')); 
  }

  getFinanceTransactionsForBizTrxID(user:User,contactID:string,bizTrxID:string):AngularFirestoreCollection<FinanceTransaction>
  {
    console.log("Welcome to TransactionProvider:getFinanceTransactionsForBizTrxID(partyID,bizTrxID)");
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`, 
                                                                  ref => ref.where('trxPartyID','==',contactID)
                                                                            .where('BizTrxID','==',bizTrxID)
                                                                            .orderBy('creation_time')); 
  }

  getFinanceTransactionsListForBizTrxID(user:User,bizTrxID:string):Promise<FinanceTransaction[]>
  {
    console.log("Welcome to TransactionProvider:getFinanceTransactionsListForBizTrxID(partyID,bizTrxID)");
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      let finTrxList:Array<FinanceTransaction> = [];
      const colRef = this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`).ref;
      const query = colRef.where('BizTrxID','==',bizTrxID).orderBy('creation_time');
      query.get().then(querySnapshot => {
        console.log("querySnapshot " + querySnapshot.size);
        finTrxList = [];
        for(let i = 0; i < querySnapshot.size; i++)
        {
          let finTransaction = {} as FinanceTransaction;
          finTransaction.ID = querySnapshot.docs[i].id;
          finTransaction.server_time = querySnapshot.docs[i].data().server_time;
          finTransaction.creation_time = querySnapshot.docs[i].data().creation_time;
          finTransaction.userID = querySnapshot.docs[i].data().userID;
          finTransaction.customerID = querySnapshot.docs[i].data().customerID;
          finTransaction.confidentialityLevel = querySnapshot.docs[i].data().confidentialityLevel;

          finTransaction.trxDate = querySnapshot.docs[i].data().trxDate;
          finTransaction.trxDiffDays = querySnapshot.docs[i].data().trxDiffDays;
          finTransaction.BizTrxID = querySnapshot.docs[i].data().BizTrxID;
          finTransaction.BizInvoiceID = querySnapshot.docs[i].data().BizInvoiceID;
          finTransaction.trxPartyID = querySnapshot.docs[i].data().trxPartyID;
          finTransaction.trxPartyName = querySnapshot.docs[i].data().trxPartyName;
          finTransaction.trxType = querySnapshot.docs[i].data().trxType;
          finTransaction.trxLocalValue = querySnapshot.docs[i].data().trxLocalValue;
          finTransaction.trxForiegnValue = querySnapshot.docs[i].data().trxForiegnValue;
          finTransaction.trxOpeningBalance = querySnapshot.docs[i].data().trxOpeningBalance;
          finTransaction.trxClosingBalance = querySnapshot.docs[i].data().trxClosingBalance;
          finTransaction.trxInterestRealizedinLocal = querySnapshot.docs[i].data().trxInterestRealizedinLocal;
          finTransaction.trxInterestRealizedinForiegn = querySnapshot.docs[i].data().trxInterestRealizedinForiegn;
          finTransaction.trxCurrency = querySnapshot.docs[i].data().trxCurrency;
          finTransaction.trxCurrencyRate = querySnapshot.docs[i].data().trxCurrencyRate;
          finTransaction.trxPaymentMethod = querySnapshot.docs[i].data().trxPaymentMethod;
          finTransaction.trxRemarks = querySnapshot.docs[i].data().trxRemarks;
          
          finTrxList.push(finTransaction);
        }
        resolve(finTrxList);
      }).catch(e => {
        console.log("Error in TransactionProvider:getFinanceTransactionsListForBizTrxID " + e);
        reject(finTrxList);
      });
    });
  }    

  getFinanceTransactionListForPartyID(user:User,contactID:string):Promise<FinanceTransaction[]>
  {
    console.log("Welcome to TransactionProvider:getFinanceTransactionListForPartyID(contactID): " + contactID);
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      let finTrxs:Array<FinanceTransaction>;
      const colRef = this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`, 
                                                              ref => ref.where('trxPartyID','==',contactID)
                                                                        .orderBy('creation_time')).ref; 
              
      colRef.get()
      .then(querySnapshot => {
        console.log("querySnapshot " + querySnapshot.size);
        finTrxs = [];
        for(let i = 0; i < querySnapshot.size; i++)
        {
          let finTransaction = {} as FinanceTransaction;
          finTransaction.ID = querySnapshot.docs[i].id;
          finTransaction.server_time = querySnapshot.docs[i].data().server_time;
          finTransaction.creation_time = querySnapshot.docs[i].data().creation_time;
          finTransaction.userID = querySnapshot.docs[i].data().userID;
          finTransaction.customerID = querySnapshot.docs[i].data().customerID;
          finTransaction.confidentialityLevel = querySnapshot.docs[i].data().confidentialityLevel;        
          
          finTransaction.trxDate = querySnapshot.docs[i].data().trxDate;
          finTransaction.trxDiffDays = querySnapshot.docs[i].data().trxDiffDays;
          finTransaction.BizTrxID = querySnapshot.docs[i].data().BizTrxID;
          finTransaction.BizInvoiceID = querySnapshot.docs[i].data().BizInvoiceID;
          finTransaction.trxPartyID = querySnapshot.docs[i].data().trxPartyID;
          finTransaction.trxPartyName = querySnapshot.docs[i].data().trxPartyName;
          finTransaction.trxType = querySnapshot.docs[i].data().trxType;
          finTransaction.trxLocalValue = querySnapshot.docs[i].data().trxLocalValue;
          finTransaction.trxForiegnValue = querySnapshot.docs[i].data().trxForiegnValue;
          finTransaction.trxOpeningBalance = querySnapshot.docs[i].data().trxOpeningBalance;
          finTransaction.trxClosingBalance = querySnapshot.docs[i].data().trxClosingBalance;
          finTransaction.trxInterestRealizedinLocal = querySnapshot.docs[i].data().trxInterestRealizedinLocal;
          finTransaction.trxInterestRealizedinForiegn = querySnapshot.docs[i].data().trxInterestRealizedinForiegn;
          finTransaction.trxCurrency = querySnapshot.docs[i].data().trxCurrency;
          finTransaction.trxCurrencyRate = querySnapshot.docs[i].data().trxCurrencyRate;
          finTransaction.trxPaymentMethod = querySnapshot.docs[i].data().trxPaymentMethod;
          finTransaction.trxRemarks = querySnapshot.docs[i].data().trxRemarks;
          
          finTrxs.push(finTransaction);
        }
        resolve(finTrxs);
      }).catch(e => {
        console.log("Error from colRef.get() " + e);
        reject([]);
      });
    });
    
  }

  /*
  getLastFinTrx(contactID:string,bizTrxID:string):AngularFirestoreCollection<FinanceTransaction>
  {
    console.log("Welcome to TransactionProvider:getLastFinTrx(partyID,bizTrxID)");
    return this.afs.collection<FinanceTransaction>(`/businessContactList/${contactID}/financeTransactionList`, 
                                                  ref => ref.where('BizTrxID','==',bizTrxID)
                                                            .orderBy('trxClosingBalance')
                                                            ); 
  }*/

  getLastFinTrxForBizTrxID(user:User,contactID:string,bizTrxID:string):Promise<FinanceTransaction>
  {
    console.log("Welcome to TransactionProvider:getLastFinTrxForBizTrxID(partyID,bizTrxID)");
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      let colRef = this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`).ref; 
      
      const query = colRef.where('trxPartyID','==',contactID).where('BizTrxID','==',bizTrxID).orderBy('creation_time','desc').limit(1);
      
      query.get()
      .then(querySnapshot => {
        console.log("querySnapshot.size " + querySnapshot.size);
        if(querySnapshot.size > 0)
        {
          let lastFinTrx = {} as FinanceTransaction;
          lastFinTrx.ID = querySnapshot.docs[0].data().ID;
          lastFinTrx.server_time = querySnapshot.docs[0].data().server_time;
          lastFinTrx.creation_time = querySnapshot.docs[0].data().creation_time;
          lastFinTrx.userID = querySnapshot.docs[0].data().userID;
          lastFinTrx.customerID = querySnapshot.docs[0].data().customerID;
          lastFinTrx.confidentialityLevel = querySnapshot.docs[0].data().confidentialityLevel;
          
          lastFinTrx.BizTrxID = querySnapshot.docs[0].data().BizTrxID;
          lastFinTrx.BizInvoiceID = querySnapshot.docs[0].data().BizInvoiceID;
          lastFinTrx.trxDate = querySnapshot.docs[0].data().trxDate;
          lastFinTrx.trxType = querySnapshot.docs[0].data().trxType;
          lastFinTrx.trxPartyID = querySnapshot.docs[0].data().trxPartyID;
          lastFinTrx.trxPartyName = querySnapshot.docs[0].data().trxPartyName;
          lastFinTrx.trxForiegnValue = querySnapshot.docs[0].data().trxForiegnValue;
          lastFinTrx.trxLocalValue = querySnapshot.docs[0].data().trxLocalValue;
          lastFinTrx.trxCurrency = querySnapshot.docs[0].data().trxCurrency;
          lastFinTrx.trxCurrencyRate = querySnapshot.docs[0].data().trxCurrencyRate;
          lastFinTrx.trxInterestRealizedinLocal = querySnapshot.docs[0].data().trxInterestRealizedinLocal;
          lastFinTrx.trxInterestRealizedinForiegn = querySnapshot.docs[0].data().trxInterestRealizedinForiegn;
          lastFinTrx.trxDiffDays = querySnapshot.docs[0].data().trxDiffDays;
          lastFinTrx.trxOpeningBalance = querySnapshot.docs[0].data().trxOpeningBalance;
          lastFinTrx.trxClosingBalance = querySnapshot.docs[0].data().trxClosingBalance;
          lastFinTrx.trxPaymentMethod = querySnapshot.docs[0].data().trxPaymentMethod;
          lastFinTrx.trxRemarks = querySnapshot.docs[0].data().trxRemarks;
  
          console.log("lastFinTrx : " + lastFinTrx.ID + " " + lastFinTrx.trxOpeningBalance + " " + lastFinTrx.trxLocalValue + " " + lastFinTrx.trxClosingBalance);
          resolve(lastFinTrx);
        }
        else
        {
          resolve(null);
        }
      }).catch(e => {
        console.log("Error from query.get() " + e);
        reject(null);
      });
    });    
  }

  deleteFinanceTransaction(user:User,finTransaction:FinanceTransaction):Promise<boolean>
  {
    console.log("Welcome to TransactionProvider:deleteFinanceTransaction(partyID,finTransaction)");
    console.log("trxPartyID " + finTransaction.trxPartyID);
    console.log("BizTrxID " + finTransaction.BizTrxID);
    console.log("user.customerID " + user.customerID);
    console.log("user.dataVersionID " + user.dataVersionID);
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      let batch:WriteBatch = this.afs.firestore.batch();

      let colRef = this.afs.collection<FinanceTransaction>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_financeTransactionList`).ref; 
      const query = colRef.where('trxPartyID','==',finTransaction.trxPartyID).where('BizTrxID','==',finTransaction.BizTrxID).orderBy('creation_time','asc');
      
      query.get()
      .then(querySnapshot => 
      {
        console.log("querySnapshot.size " + querySnapshot.size);
        let relevantFinTrxs:Array<FinanceTransaction>;
        let bFlag:boolean = false;
        let previousRec:number = 0;
        relevantFinTrxs = [];
        //for(let i=querySnapshot.size-1;i>=0;i--)
        for(let i=0;i<querySnapshot.size;i++)
        {
          console.log("Processing trx index : " + i);
          let currentFinTrx = {} as FinanceTransaction;
          currentFinTrx.ID = querySnapshot.docs[i].data().ID;
          currentFinTrx.server_time = querySnapshot.docs[i].data().server_time;
          currentFinTrx.creation_time = querySnapshot.docs[i].data().creation_time;
          currentFinTrx.userID = querySnapshot.docs[i].data().userID;
          currentFinTrx.customerID = querySnapshot.docs[i].data().customerID;
          currentFinTrx.confidentialityLevel = querySnapshot.docs[i].data().confidentialityLevel;

          currentFinTrx.BizTrxID = querySnapshot.docs[i].data().BizTrxID;
          currentFinTrx.BizInvoiceID = querySnapshot.docs[i].data().BizInvoiceID;
          currentFinTrx.trxDate = querySnapshot.docs[i].data().trxDate;
          currentFinTrx.trxType = querySnapshot.docs[i].data().trxType;
          currentFinTrx.trxPartyID = querySnapshot.docs[i].data().trxPartyID;
          currentFinTrx.trxPartyName = querySnapshot.docs[i].data().trxPartyName;
          currentFinTrx.trxForiegnValue = querySnapshot.docs[i].data().trxForiegnValue;
          currentFinTrx.trxLocalValue = querySnapshot.docs[i].data().trxLocalValue;
          currentFinTrx.trxCurrency = querySnapshot.docs[i].data().trxCurrency;
          currentFinTrx.trxCurrencyRate = querySnapshot.docs[i].data().trxCurrencyRate;
          currentFinTrx.trxInterestRealizedinLocal = querySnapshot.docs[i].data().trxInterestRealizedinLocal;
          currentFinTrx.trxInterestRealizedinForiegn = querySnapshot.docs[i].data().trxInterestRealizedinForiegn;
          currentFinTrx.trxDiffDays = querySnapshot.docs[i].data().trxDiffDays;
          currentFinTrx.trxOpeningBalance = querySnapshot.docs[i].data().trxOpeningBalance;
          currentFinTrx.trxClosingBalance = querySnapshot.docs[i].data().trxClosingBalance;
          currentFinTrx.trxPaymentMethod = querySnapshot.docs[i].data().trxPaymentMethod;
          currentFinTrx.trxRemarks = querySnapshot.docs[i].data().trxRemarks;

          if(currentFinTrx.ID === finTransaction.ID)
          {//Skip this transaction
            console.log("Skip this transaction : " + i);
            bFlag=true;
          //  previousRec = i - 1;
          //  relevantFinTrxs.push(currentFinTrx);
            batch.delete(querySnapshot.docs[i].ref);            
            continue;
          }
          
          if(bFlag)
          {
            console.log("bFlag : " + bFlag);
            console.log("relevantFinTrxs.length : " + relevantFinTrxs.length);
            console.log("currentFinTrx: " + currentFinTrx.trxOpeningBalance + " " + currentFinTrx.trxClosingBalance);
            //currentFinTrx.trxOpeningBalance = relevantFinTrxs[querySnapshot.size-1-previousRec].trxClosingBalance;
            currentFinTrx.trxOpeningBalance = relevantFinTrxs[relevantFinTrxs.length-1].trxClosingBalance;
            if(currentFinTrx.trxCurrency === myConstants.foreignCurrency)
            {
              currentFinTrx.trxClosingBalance = currentFinTrx.trxOpeningBalance - currentFinTrx.trxForiegnValue;
            }
            else if(currentFinTrx.trxCurrency === myConstants.localCurrency)
            {
              currentFinTrx.trxClosingBalance = currentFinTrx.trxOpeningBalance - currentFinTrx.trxLocalValue;
            }
           // previousRec = i;
           console.log("Pushing currentFinTrx: " + currentFinTrx.trxOpeningBalance + " " + currentFinTrx.trxClosingBalance); 
           relevantFinTrxs.push(currentFinTrx);
            
            /*
            querySnapshot.docs[i].ref.update({
              trxOpeningBalance:currentFinTrx.trxOpeningBalance,
              trxClosingBalance:currentFinTrx.trxClosingBalance
            });*/
            batch.update(querySnapshot.docs[i].ref,{
              trxOpeningBalance:currentFinTrx.trxOpeningBalance,
              trxClosingBalance:currentFinTrx.trxClosingBalance
            });
            
            if((i === (querySnapshot.size-1)) && (currentFinTrx.trxClosingBalance > 0))
            {//Reopen Business Transaction
              let bizRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessTransactionList/${finTransaction.BizTrxID}`).ref;
              batch.update(bizRef,{
                server_time:firebase.firestore.FieldValue.serverTimestamp(),
                trxStatus: myConstants.bizTrxStatus.OPEN
              });
            }
          }
          else
          {
            console.log("Pushing currentFinTrx: " + currentFinTrx.trxOpeningBalance + " " + currentFinTrx.trxClosingBalance); 
            relevantFinTrxs.push(currentFinTrx);
          }
        }//end of for loop
        batch.commit()
        .then(() => {
          console.log("committed");    
          resolve(true);
        }).catch(e => {
          console.log("Error from batch.commit() " + e);
          reject(false);
        }); 
      });
    });
  }

  getSalesItemListForBizTrxID(user:User,bizTrxID:string):Promise<SalesItem[]>
  {
    console.log("Welcome to TransactionProvider:getSalesItemListForBizTrxID(partyID,bizTrxID)");
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      let salesItemList:Array<SalesItem> = [];
      const colRef = this.afs.collection<SalesItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_salesItemList`).ref;
      const query = colRef.where('trxID','==',bizTrxID).orderBy('itemType.rank').orderBy('itemShape.rank').orderBy('itemSieve.rank').orderBy('itemQualityLevel.rank');
      query.get().then(querySnapshot => {
        console.log("querySnapshot " + querySnapshot.size);
        salesItemList = [];
        for(let i = 0; i < querySnapshot.size; i++)
        {
          let salesItem = {} as SalesItem;
          
          salesItem.id = querySnapshot.docs[i].data().id;

          salesItem.itemType = querySnapshot.docs[i].data().itemType;
          salesItem.itemShape = querySnapshot.docs[i].data().itemShape;
          salesItem.itemSieve = querySnapshot.docs[i].data().itemSieve;
          salesItem.itemQualityLevel = querySnapshot.docs[i].data().itemQualityLevel;

          salesItem.saleableWeight = querySnapshot.docs[i].data().saleableWeight;
          salesItem.purchaseWeight = querySnapshot.docs[i].data().purchaseWeight;
          salesItem.qty = querySnapshot.docs[i].data().qty;
          salesItem.valueFactor = querySnapshot.docs[i].data().valueFactor;
        
          salesItem.avgPriceRate = querySnapshot.docs[i].data().avgPriceRate;
          salesItem.avgSalePrice = querySnapshot.docs[i].data().avgSalePrice;
          salesItem.avgSalesTax = {} as TaxAmount;
          salesItem.avgSalesTax = querySnapshot.docs[i].data().avgSalesTax;
          salesItem.avgGrossPrice = querySnapshot.docs[i].data().avgGrossPrice;
          salesItem.avgSalesBrokerage = querySnapshot.docs[i].data().avgSalesBrokerage;
          salesItem.avgNetPrice = querySnapshot.docs[i].data().avgNetPrice;
          salesItem.avgSalesGain = querySnapshot.docs[i].data().avgSalesGain;

          salesItem.avgProfit = querySnapshot.docs[i].data().avgProfit;
          salesItem.avgProfitMargin = querySnapshot.docs[i].data().avgProfitMargin;
        
          salesItem.totalPriceValue = querySnapshot.docs[i].data().totalPriceValue;
          salesItem.totalSaleValue = querySnapshot.docs[i].data().totalSaleValue;
          salesItem.taxRate = {} as TaxRate;
          salesItem.taxRate = querySnapshot.docs[i].data().taxRate;
        
          salesItem.totalSalesTax = {} as TaxAmount;
          salesItem.totalSalesTax = querySnapshot.docs[i].data().totalSalesTax;
          salesItem.totalGrossValue = querySnapshot.docs[i].data().totalGrossValue;
          salesItem.totalSalesBrokerage = querySnapshot.docs[i].data().totalSalesBrokerage;
          salesItem.totalNetValue = querySnapshot.docs[i].data().totalNetValue;
          salesItem.totalSalesGain = querySnapshot.docs[i].data().totalSalesGain;
          
          salesItem.avgPurchaseCost = querySnapshot.docs[i].data().avgPurchaseCost;
          salesItem.avgInterestCost = querySnapshot.docs[i].data().avgInterestCost;
          salesItem.avgMfgCost = querySnapshot.docs[i].data().avgMfgCost;
          salesItem.avgGrossCost = querySnapshot.docs[i].data().avgGrossCost;
          salesItem.avgGrossCostAtDueTerm = querySnapshot.docs[i].data().avgGrossCostAtDueTerm;

          salesItem.totalPurchaseCost = querySnapshot.docs[i].data().totalPurchaseCost;
          salesItem.totalInterestCost = querySnapshot.docs[i].data().totalInterestCost;
          salesItem.totalMfgCost = querySnapshot.docs[i].data().totalMfgCost;
          salesItem.totalGrossCost = querySnapshot.docs[i].data().totalGrossCost;
          salesItem.totalGrossCostAtDueTerm = querySnapshot.docs[i].data().totalGrossCostAtDueTerm;

          salesItem.totalProfit = querySnapshot.docs[i].data().totalProfit;
          salesItem.totalProfitMargin = querySnapshot.docs[i].data().totalProfitMargin;
        
          salesItem.trxID = querySnapshot.docs[i].data().trxID;
          salesItem.trxDate = querySnapshot.docs[i].data().trxDate;
          salesItem.trxDueDays = querySnapshot.docs[i].data().trxDueDays;
          salesItem.trxValidityDays = querySnapshot.docs[i].data().trxValidityDays;
          salesItem.trxStatus = querySnapshot.docs[i].data().trxStatus;
          salesItem.trxCurrency = querySnapshot.docs[i].data().trxCurrency;
          salesItem.trxCurrencyRate = querySnapshot.docs[i].data().trxCurrencyRate;
          salesItem.trxROIForLatePayment = querySnapshot.docs[i].data().trxROIForLatePayment;
          salesItem.trxBrokerageRate = querySnapshot.docs[i].data().trxBrokerageRate;
          
          salesItemList.push(salesItem);
        }
        resolve(salesItemList);
      }).catch(e => {
        console.log("Error in TransactionProvider:getSalesItemListForBizTrxID " + e);
        reject(salesItemList);
      });
    });
  }

}
