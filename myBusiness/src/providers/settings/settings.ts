import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { WriteBatch } from '@firebase/firestore-types';

import { Injectable } from '@angular/core';
import { MarketPrice } from '../../model/marketPrice';
import { PackageRule } from '../../model/packageRule';
import { ItemType,ItemSieve, ItemShape,ItemQualityLevel } from '../../model/packagedItem';
import { StateRegion,FinanceSettings } from '../../model/referenceData';
import { User,checkIfValid } from '../../model/user';
import { getDateTimeString } from '../../model/utility';
import { resolveDefinition } from '@angular/core/src/view/util';
import { myConstants } from '../../model/myConstants';

/*
  Generated class for the SettingsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SettingsProvider {

  constructor(private afs:AngularFirestore) {
    console.log('Hello SettingsProvider Provider');
  }

  getItemTypeList(user:User,bExclusive:boolean):Promise<ItemType[]>
  {
    console.log("Welcome to SettingsProvider::getItemTypeList() " + bExclusive);
    
    return new Promise((resolve,reject) => {
      let itemTypeList:Array<ItemType> = [];
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
    
      const  colRef = this.afs.collection<ItemShape>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemTypeList`).ref;
      
      itemTypeList = [];
      if(colRef)
      {
        const query = colRef.orderBy("rank");

        query.get()
        .then(querySnapshot => {
          for(let i=0;i<querySnapshot.docs.length;i++)
          {
            //console.log("querySnapshot.docs[i].data().rank " + querySnapshot.docs[i].data().rank);
            if((!bExclusive) || ((querySnapshot.docs[i].data().rank > 0) && (querySnapshot.docs[i].data().rank < 999)))
            {
              let itemType = {} as ItemType;
            
              itemType.id = querySnapshot.docs[i].data().id;
              itemType.creation_time = querySnapshot.docs[i].data().creation_time;
              itemType.rank = querySnapshot.docs[i].data().rank;
              itemType.name = querySnapshot.docs[i].data().name;
              itemType.remarks = querySnapshot.docs[i].data().remarks;
              itemTypeList.push(itemType);
            }
          }
          resolve(itemTypeList);                    
        }).catch(e => {
          console.log("Error from query.get() inside SettingsProvider::getItemTypeList() " + e);
          reject(itemTypeList);
        });
      }
      else
      {
        resolve(itemTypeList);
      }
    });
  }

  getItemShapeList(user:User,bExclusive:boolean):Promise<ItemShape[]>
  {
    console.log("Welcome to SettingsProvider::getItemShapeList() " + bExclusive);
    
    return new Promise((resolve,reject) => {
      let itemShapeList:Array<ItemShape> = [];
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
    
      const  colRef = this.afs.collection<ItemShape>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemShapeList`).ref;
      
      itemShapeList = [];
      if(colRef)
      {
        const query = colRef.orderBy("rank");

        query.get()
        .then(querySnapshot => {
          for(let i=0;i<querySnapshot.docs.length;i++)
          {
            //console.log("querySnapshot.docs[i].data().rank " + querySnapshot.docs[i].data().rank);
            if((!bExclusive) || ((querySnapshot.docs[i].data().rank > 0) && (querySnapshot.docs[i].data().rank < 999)))
            {
              let itemShape = {} as ItemShape;
            
              itemShape.id = querySnapshot.docs[i].data().id;
              itemShape.creation_time = querySnapshot.docs[i].data().creation_time;
              itemShape.rank = querySnapshot.docs[i].data().rank;
              itemShape.name = querySnapshot.docs[i].data().name;
              itemShape.remarks = querySnapshot.docs[i].data().remarks;
              itemShapeList.push(itemShape);
            }
          }
          resolve(itemShapeList);                    
        }).catch(e => {
          console.log("Error from query.get() inside SettingsProvider::getItemShapeList() " + e);
          reject(itemShapeList);
        });
      }
      else
      {
        resolve(itemShapeList);
      }
    });
  }

  getItemSieveList(user:User,itemType:string,bExclusive:boolean):Promise<ItemSieve[]>
  {
    console.log("Welcome to SettingsProvider::getItemSieveList() " + itemType + " " + bExclusive);
    
    return new Promise((resolve,reject) => {
      let itemSieveList:Array<ItemSieve>;
      itemSieveList = [];
      if((!checkIfValid(user)) || !((itemType === myConstants.itemType.ROUGH) || (itemType === myConstants.itemType.POLISHED) || (itemType === myConstants.itemType.ALL)))
      {
        console.log("Invalid user and/or itemType");
        resolve([]);
      }

      const  colRef = this.afs.collection<ItemSieve>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemSieveList`).ref;
      let query:any;

      if(colRef)
      {
        if(itemType === myConstants.itemType.ALL)
        {
          query = colRef.orderBy("itemTypeID").orderBy("rank");
        }
        else
        {
          query = colRef.where('itemTypeID','==',itemType).orderBy("rank");
        }
        
        query.get()
        .then(querySnapshot => {
          for(let i=0;i<querySnapshot.docs.length;i++)
          {
            if((!bExclusive) || ((querySnapshot.docs[i].data().rank > 0) && (querySnapshot.docs[i].data().rank < 999)))
            {
              let itemSieve = {} as ItemSieve;
              itemSieve.id = querySnapshot.docs[i].data().id;
              itemSieve.creation_time = querySnapshot.docs[i].data().creation_time;
              itemSieve.rank = querySnapshot.docs[i].data().rank;
              itemSieve.name = querySnapshot.docs[i].data().name;
              itemSieve.itemTypeID = querySnapshot.docs[i].data().itemTypeID;
              itemSieve.remarks = querySnapshot.docs[i].data().remarks;
              
              itemSieveList.push(itemSieve);
            }
          }
          resolve(itemSieveList);
        }).catch(e => {
          console.log("Error from query.get() inside SettingsProvider::getItemSieveList() " + e);
          reject(itemSieveList);
        });
      }
      else
      {
        resolve(itemSieveList);
      }
    });
    
  }

  
  getItemQualityLevelList(user:User,itemType:string,bExclusive):Promise<ItemQualityLevel[]>
  {
    console.log("Welcome to SettingsProvider::getItemQualityLevelList() " + itemType + " " + bExclusive);
    
    return new Promise((resolve,reject) => {
      let itemQualityLevelList:Array<ItemQualityLevel>;
      itemQualityLevelList = [];

      if((!checkIfValid(user)) || !((itemType === myConstants.itemType.ROUGH) || (itemType === myConstants.itemType.POLISHED) || (itemType === myConstants.itemType.ALL)))
      {
        console.log("Invalid user");
        resolve(itemQualityLevelList);
      }

      const  colRef = this.afs.collection<ItemQualityLevel>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemQualityLevelList`).ref;
      let query:any;

      if(colRef)
      {
        if(itemType === myConstants.itemType.ALL)
        {
          query = colRef.orderBy("itemTypeID").orderBy("rank");
        }
        else
        {
          query = colRef.where('itemTypeID','==',itemType).orderBy("rank");
        }

        query.get().then(querySnapshot => {
          for(let i=0;i<querySnapshot.docs.length;i++)
          {
            if((!bExclusive) || ((querySnapshot.docs[i].data().rank > 0) && (querySnapshot.docs[i].data().rank < 999)))
            {
              let itemQualityLevel = {} as ItemQualityLevel;
              itemQualityLevel.id = querySnapshot.docs[i].data().id;
              itemQualityLevel.creation_time = querySnapshot.docs[i].data().creation_time;
              itemQualityLevel.name = querySnapshot.docs[i].data().name;
              itemQualityLevel.itemTypeID = querySnapshot.docs[i].data().itemTypeID;
              itemQualityLevel.rank = querySnapshot.docs[i].data().rank;
              itemQualityLevel.remarks = querySnapshot.docs[i].data().remarks;
              
              itemQualityLevelList.push(itemQualityLevel);          
            }
          }
          resolve(itemQualityLevelList);
        }).catch(e => {
          console.log("Error from query.get() inside SettingsProvider::getItemQualityLevelList() " + e);
          reject(itemQualityLevelList);
        });
      }
      else
      {
        resolve(itemQualityLevelList);
      }
    });
  }
  
  getItemType(user:User,id:string):Promise<ItemType>
  {
    console.log("Welcome to SettingsProvider::getItemType(id) : " + id);
    
    let itemType = {} as ItemType;
    itemType.id = "";
    itemType.rank = -1;
    itemType.creation_time = "";
    itemType.remarks = "";
    itemType.name = "";
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      if(id !== '')
      {
        const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemTypeList/${id}`).ref;
        if(docRef)
        {
          docRef.get().then(doc => {
            if (!doc.exists) {
                console.log('No such document!' + doc.id);
                itemType.id = "invalid";
                resolve(itemType);
            } else {
              itemType.id = doc.id;
              itemType.rank = doc.data().rank;
              itemType.name = doc.data().name;
              itemType.creation_time = doc.data().creation_time;
              itemType.remarks = doc.data().remarks;
              
              resolve(itemType);
            }
          })
          .catch(err => {
              console.log('Error getting itemType document', err);
              reject(itemType);
          });
        }
        else
        {
          console.log("docRef is invalid");
          reject(itemType);
        }
      }
      else
      {
        resolve(itemType);
      }
    });    
  }

  getItemShape(user:User,id:string):Promise<ItemShape>
  {
    console.log("Welcome to SettingsProvider::getItemShape(id) : " + id);
    
    let itemShape = {} as ItemShape;
    itemShape.id = "";
    itemShape.rank = -1;
    itemShape.creation_time = "";
    itemShape.remarks = "";
    itemShape.name = "";
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      if(id !== '')
      {
        const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemShapeList/${id}`).ref;
        if(docRef)
        {
          docRef.get().then(doc => {
            if (!doc.exists) {
                console.log('No such document!' + doc.id);
                itemShape.id = "invalid";
                resolve(itemShape);
            } else {
              itemShape.id = doc.id;
              itemShape.rank = doc.data().rank;
              itemShape.name = doc.data().name;
              itemShape.creation_time = doc.data().creation_time;
              itemShape.remarks = doc.data().remarks;
              
              resolve(itemShape);
            }
          })
          .catch(err => {
              console.log('Error getting itemShape document', err);
              reject(itemShape);
          });
        }
        else
        {
          console.log("docRef is invalid");
          reject(itemShape);
        }
      }
      else
      {
        resolve(itemShape);
      }
    });    
  }

  getItemQualityLevel(user:User,id:string):Promise<ItemQualityLevel>
  {
    console.log("Welcome to SettingsProvider::getItemQualityLevel(id) : " + id);
    
    let itemQualityLevel = {} as ItemQualityLevel;
    itemQualityLevel.id = "";
    itemQualityLevel.rank = -1;
    itemQualityLevel.creation_time = "";
    itemQualityLevel.remarks = "";
    itemQualityLevel.name = "";

    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      if(id !== '')
      {
        const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemQualityLevelList/${id}`).ref;
        if(docRef)
        {
          docRef.get().then(doc => {
            if (!doc.exists) {
                console.log('No such document!' + doc.id);
                itemQualityLevel.id = "invalid";
                resolve(itemQualityLevel);
            } else {
              itemQualityLevel.id = doc.id;
              itemQualityLevel.rank = doc.data().rank;
              itemQualityLevel.name = doc.data().name;
              itemQualityLevel.creation_time = doc.data().creation_time;
              itemQualityLevel.remarks = doc.data().remarks;
              console.log("itemQualityLevel.id " + itemQualityLevel.id + " itemQualityLevel.rank " + itemQualityLevel.rank);
              resolve(itemQualityLevel);
            }
          })
          .catch(err => {
              console.log('Error getting itemSize document', err);
              reject(itemQualityLevel);
          });
        }
        else
        {
          console.log("docRef is invalid");
          reject(itemQualityLevel);
        }
      }
      else
      {
      resolve(itemQualityLevel);  
      }
    });    
  }

  getItemSieve(user:User,id:string):Promise<ItemSieve>
  {
    console.log("Welcome to SettingsProvider::getItemSieve(id) : " + id);
    
    let itemSieve = {} as ItemSieve;
    itemSieve.id = "";
    itemSieve.rank = -1;
    itemSieve.creation_time = "";
    itemSieve.remarks = "";
    itemSieve.name = "";

    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      if(id !== '')
      {
        const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/itemSieveList/${id}`).ref;
        if(docRef)
        {
          docRef.get().then(doc => {
            if (!doc.exists) {
                console.log('No such document!' + doc.id);
                itemSieve.id = "invalid";
                resolve(itemSieve);
            } else {
              itemSieve.id = doc.id;
              itemSieve.rank = doc.data().rank;
              itemSieve.name = doc.data().name;
              itemSieve.creation_time = doc.data().creation_time;
              itemSieve.remarks = doc.data().remarks;
              console.log("itemSize.id " + itemSieve.id + " itemSize.rank " + itemSieve.rank);         
              resolve(itemSieve);
            }
          })
          .catch(err => {
              console.log('Error getting itemSize document', err);
              reject(itemSieve);
          });
        }
        else
        {
          console.log("docRef is invalid");
          reject(itemSieve);
        }
      }
      else
      {
        resolve(itemSieve);
      }
    });    
  }

  
  getMarketPriceListForTSSQ(user:User,iType:string,iShapeID:string,iSieveID:string,iQualityLevel:string):Promise<MarketPrice>
  {
    console.log("Welcome to SettingsProvider:getMarketPricePolishListForTSSQ() " + " " + iType + " " + iShapeID + " " + iSieveID + " " + iQualityLevel);
    
    let marketPrice = {} as MarketPrice;
    marketPrice.id = "";
    marketPrice.creation_time = "";
    marketPrice.itemTypeID = "";
    marketPrice.shapeID = "";
    marketPrice.sieveID = "";
    marketPrice.qualityLevelID = "";
    marketPrice.qualityLevelRank = 0;
    marketPrice.rate = 0;

    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      try
      {
        this.afs.collection<MarketPrice>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/marketPriceList`).ref
        .where('itemTypeID','==',iType)
        .where('shapeID','==',iShapeID)
        .where('sieveID','==',iSieveID)
        .where('qualityLevelID','==',iQualityLevel)
        //.orderBy('qualityLevelRank')
        .onSnapshot(querySnapshot => {
       //   console.log("querySnapshot.size: " + querySnapshot.size);
          if(querySnapshot.docs.length === 1)
          {
            marketPrice.id = querySnapshot.docs[0].data().id;
            marketPrice.creation_time = querySnapshot.docs[0].data().creation_time;
            marketPrice.itemTypeID = querySnapshot.docs[0].data().itemTypeID;
            marketPrice.shapeID = querySnapshot.docs[0].data().shapeID;
            marketPrice.sieveID = querySnapshot.docs[0].data().sieveID;
            marketPrice.qualityLevelID = querySnapshot.docs[0].data().qualityLevelID;
            marketPrice.qualityLevelRank = querySnapshot.docs[0].data().qualityLevelRank;
            marketPrice.rate = querySnapshot.docs[0].data().rate;
          }
          resolve(marketPrice);          
        });
      }
      catch(e)
      {
        console.log("Exception in getMarketPriceListForTSSQ " + e);
        reject(marketPrice);
      }
    });
  }

  
  
  getMarketPriceListForTSS(user:User,iType:string,iShapeID:string,iSieveID:string):Promise<MarketPrice[]>
  {
    console.log("Welcome to SettingsProvider:getMarketPricePolish() " + " " + iType + " " + iShapeID + " " + iSieveID);
    
    let marketPriceList:Array<MarketPrice> = [];
    
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      try
      {
        this.afs.collection<MarketPrice>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/marketPriceList`).ref
        .where('itemTypeID','==',iType)
        .where('shapeID','==',iShapeID)
        .where('sieveID','==',iSieveID)
        .orderBy('qualityLevelRank')
        .onSnapshot(querySnapshot => {
       //   console.log("querySnapshot.size: " + querySnapshot.size);
          marketPriceList = [];

          for(let i=0;i<querySnapshot.docs.length;i++)
          {
            let marketPrice = {} as MarketPrice;
            marketPrice.id = querySnapshot.docs[i].data().id;
            marketPrice.creation_time = querySnapshot.docs[i].data().creation_time;
            marketPrice.itemTypeID = querySnapshot.docs[i].data().itemTypeID;
            marketPrice.shapeID = querySnapshot.docs[i].data().shapeID;
            marketPrice.sieveID = querySnapshot.docs[i].data().sieveID;
            marketPrice.qualityLevelID = querySnapshot.docs[i].data().qualityLevelID;
            marketPrice.qualityLevelRank = querySnapshot.docs[i].data().qualityLevelRank;
            marketPrice.rate = querySnapshot.docs[i].data().rate;

            marketPriceList.push(marketPrice);
          }
          
          console.log("marketPriceList.length " + marketPriceList.length);
          resolve(marketPriceList);
        });
      }
      catch(e)
      {
        console.log("Exception in getMarketPriceListForTSS " + e);
        reject(marketPriceList);
      }
    });
  }

  getMarketPrice$(user:User):AngularFirestoreCollection<MarketPrice>
  {
    console.log("Welcome to SettingsProvider:getMarketPrice$() ");
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    try
    {
      return this.afs.collection<MarketPrice>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/marketPriceList`,
                                                ref => ref.orderBy('shapeID').orderBy('sieveID').orderBy('qualityLevelRank'));
      
    }
    catch(e)
    {
      console.log("Exception in getMarketPrice$ " + e);
      return null
    }
  }

  getMarketPriceList(user:User):Promise<MarketPrice[]>
  {
    console.log("Welcome to SettingsProvider:getMarketPriceList() ");
    
    let marketPriceList:Array<MarketPrice> = [];
    
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      try
      {
        this.afs.collection<MarketPrice>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/marketPriceList`).ref
        .orderBy('shapeID')
        .orderBy('sieveID')
        .orderBy('qualityLevelRank')
        .onSnapshot(querySnapshot => {
       //   console.log("querySnapshot.size: " + querySnapshot.size);
          marketPriceList = [];
          for(let i=0;i<querySnapshot.docs.length;i++)
          {
            let marketPrice = {} as MarketPrice;
            marketPrice.id = querySnapshot.docs[i].data().id;
            marketPrice.creation_time = querySnapshot.docs[i].data().creation_time;
            marketPrice.itemTypeID = querySnapshot.docs[i].data().itemTypeID;
            marketPrice.shapeID = querySnapshot.docs[i].data().shapeID;
            marketPrice.sieveID = querySnapshot.docs[i].data().sieveID;
            marketPrice.qualityLevelID = querySnapshot.docs[i].data().qualityLevelID;
            marketPrice.qualityLevelRank = querySnapshot.docs[i].data().qualityLevelRank;
            marketPrice.rate = querySnapshot.docs[i].data().rate;

            marketPriceList.push(marketPrice);
          }
          console.log("marketPriceList.length " + marketPriceList.length);
          resolve(marketPriceList);
        });
      }
      catch(e)
      {
        console.log("Exception in getMarketPriceList " + e);
        reject(marketPriceList);
      }
    });
  }

  saveMarketPriceList(user:User,priceList:Array<MarketPrice>):Promise<boolean>
  {
    console.log("Welcome to SettingsProvider::saveMarketPriceList() " + priceList.length);
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      let currentTime:string = (new Date().toISOString());
      let batch:WriteBatch = this.afs.firestore.batch();

      for(let i=0;i<priceList.length;i++)
      {
        if(priceList[i].id === '')
        {
          const ID:string = priceList[i].itemTypeID + "_" + priceList[i].shapeID + "_" + priceList[i].sieveID + "_" + priceList[i].qualityLevelID;
          const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/marketPriceList/${ID}`).ref;
          if(docRef)
          {
            batch.set(docRef,{
            id:ID,
            creation_time: currentTime,
            itemTypeID:priceList[i].itemTypeID,
            shapeID: priceList[i].shapeID,
            sieveID: priceList[i].sieveID,
            qualityLevelID: priceList[i].qualityLevelID,
            qualityLevelRank: priceList[i].qualityLevelRank,
            rate: priceList[i].rate
            });
          }
        }
        else
        {
          const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/marketPriceList/${priceList[i].id}`).ref;
          if(docRef)
          {
            batch.update(docRef,{
            creation_time: currentTime,
            itemTypeID:priceList[i].itemTypeID,
            shapeID: priceList[i].shapeID,
            sieveID: priceList[i].sieveID,
            qualityLevelID: priceList[i].qualityLevelID,
            qualityLevelRank: priceList[i].qualityLevelRank,
            rate: priceList[i].rate
            });
          }
        }          
      }
      batch.commit()
      .then(() => {
        console.log("committed");    
        resolve(true);
      }).catch(e => {
        console.log("Error from batch.commit() " + e);
        reject(null);
      }); 
    });    
  }

  getPackageRulesList(user:User):AngularFirestoreCollection<PackageRule>
  {
    console.log("Welcome to InventoryProvider::getPackageRulesList() ");
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackageRule>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList`, ref => ref.orderBy('sequenceID')); 
  }
  
  getPackageRulesArray(user:User):Promise<PackageRule[]>
  { 
    console.log("Welcome to InventoryProvider::getPackageRulesArray() ");
    
    let pRules:Array<PackageRule>;
    pRules = [];
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      this.afs.collection<PackageRule>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList`)
      .ref.orderBy('sequenceID')
      .onSnapshot(querySnapshot => {
        console.log("querySnapshot.size: " + querySnapshot.size);
        for(let i=0;i<querySnapshot.docs.length;i++)
        {
        //  console.log("querySnapshot.doc: " + doc.data().id);
          let pRule = {} as PackageRule;
          pRule.id = querySnapshot.docs[i].data().id;
          pRule.creation_time = querySnapshot.docs[i].data().creation_time;
          pRule.curTaskCode = querySnapshot.docs[i].data().curTaskCode;
          //pRule.packageStatus = doc.data().packageStatus;    
          pRule.nextTaskCode = querySnapshot.docs[i].data().nextTaskCode;
          pRule.sequenceID = querySnapshot.docs[i].data().sequenceID;
          pRule.bGroupRead = querySnapshot.docs[i].data().bGroupRead;
          pRule.bMultipleOutput = querySnapshot.docs[i].data().bMultipleOutput;
          pRule.bPartialAssign = querySnapshot.docs[i].data().bPartialAssign;
          pRule.bReducedOutput = querySnapshot.docs[i].data().bReducedOutput;
          pRule.bIncursCost = querySnapshot.docs[i].data().bIncursCost;
          pRule.bFirst = querySnapshot.docs[i].data().bFirst;
          pRule.bLast = querySnapshot.docs[i].data().bLast;
        
          pRules.push(pRule);
        };
        console.log("pRules.length " + pRules.length);
        resolve(pRules);
      });
    });
  }

  
  createPackageRule(user:User,p:PackageRule): Promise<string>
  {
    console.log("Welcome to InventoryProvider:createPackageRule(PackageRule)");
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      const ruleID: string = p.curTaskCode + "_" + p.nextTaskCode + "_" + getDateTimeString(new Date());
      const summaryID: string = p.nextTaskCode;
      const docRefRule = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList/${ruleID}`).ref;
      const docRefSummary = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/inventorySummaryList/${summaryID}`).ref;
  
      let batch:WriteBatch = this.afs.firestore.batch();
      if(docRefRule && docRefSummary)
      {
        batch.set(docRefRule,{
          id:ruleID,
          server_time:firebase.firestore.FieldValue.serverTimestamp(),
          creation_time:getDateTimeString(new Date()),    
          curTaskCode: p.curTaskCode,
          //packageStatus: p.packageStatus,
          nextTaskCode: p.nextTaskCode,
          sequenceID: p.sequenceID,
          bGroupRead: p.bGroupRead,
          bMultipleOutput: p.bMultipleOutput ,
          bPartialAssign: p.bPartialAssign,
          bReducedOutput: p.bReducedOutput,
          bIncursCost: p.bIncursCost,
          bFirst: p.bFirst,
          bLast: p.bLast            
        });
  
        batch.commit()
        .then(() => {
          console.log("committed");    
          resolve(ruleID);
        }).catch(e => {
          console.log("Error from batch.commit() " + e);
          reject(null);
        });   
      }
    });
    
     
  }
  
  async createPackageRuleArray(user:User,pRules:Array<PackageRule>): Promise<boolean>
  {
    console.log("Welcome to InventoryProvider:createPackageRuleArray(...)");
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return false;
    }
    for(let i=0;i<pRules.length;i++)
    {
      let ruleID: string = this.afs.createId();
      let newDoc:AngularFirestoreDocument<PackageRule>; 
      await this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList/${ruleID}`)
        .set({
          id:ruleID,
          curTaskCode: pRules[i].curTaskCode,
          //packageStatus: pRules[i].packageStatus,
          nextTaskCode: pRules[i].nextTaskCode,
          sequenceID: pRules[i].sequenceID,
          bGroupRead: pRules[i].bGroupRead,
          bMultipleOutput: pRules[i].bMultipleOutput ,
          bPartialAssign: pRules[i].bPartialAssign,
          bReducedOutput: pRules[i].bReducedOutput,
          bIncursCost: pRules[i].bIncursCost,
          bFirst: pRules[i].bFirst,
          bLast: pRules[i].bLast            
        });
      console.log("added for i: " + i);
    }
    return true;
  }

  getPackageRule(user:User,pID:string):Promise<PackageRule>
  { 
    console.log("Welcome to InventoryProvider:getPackageRule(...)");
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      let pRule = {} as PackageRule;            
      let docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList/${pID}`).ref;
      docRef.get()
      .then(doc => {
          if (!doc.exists) {
              console.log('No such document!' + doc.id);
              resolve(null);
          } else {
              pRule.id = doc.id;
              pRule.curTaskCode = doc.data().curTaskCode;
              resolve(pRule);
          }
      })
      .catch(err => {
          console.log('Error getting document', err);
          reject(null);
      });
    });
  }

  getPackageRule_For_A(user:User,taskCode:string):Promise<PackageRule[]>
  { 
    console.log("Welcome to InventoryProvider::getPackageRule_For_A(nextTaskCode) for " + taskCode);
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      let pRules:Array<PackageRule>;
      pRules = [];
      this.afs.collection<PackageRule>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList`)
      .ref.where('curTaskCode','==',taskCode)
      .onSnapshot(querySnapshot => {
        console.log("querySnapshot.size: " + querySnapshot.size);
        for(let i=0;i<querySnapshot.docs.length;i++)
        {
          console.log("querySnapshot.doc: " + querySnapshot.docs[i].data().id);
          let pRule = {} as PackageRule;
          pRule.id = querySnapshot.docs[i].data().id;
          pRule.creation_time = querySnapshot.docs[i].data().creation_time;
          pRule.curTaskCode = querySnapshot.docs[i].data().curTaskCode;
          //pRule.packageStatus = doc.data().packageStatus;    
          pRule.nextTaskCode = querySnapshot.docs[i].data().nextTaskCode;
          pRule.bGroupRead = querySnapshot.docs[i].data().bGroupRead;
          pRule.sequenceID = querySnapshot.docs[i].data().sequenceID;
          pRule.bMultipleOutput = querySnapshot.docs[i].data().bMultipleOutput;
          pRule.bPartialAssign = querySnapshot.docs[i].data().bPartialAssign;
          pRule.bReducedOutput = querySnapshot.docs[i].data().bReducedOutput;
          pRule.bIncursCost = querySnapshot.docs[i].data().bIncursCost;
          pRule.bFirst = querySnapshot.docs[i].data().bFirst;
          pRule.bLast = querySnapshot.docs[i].data().bLast;
        
          pRules.push(pRule);
        };
        console.log("pRules.length " + pRules.length);
        resolve(pRules);
      });
    });
    
  }

  getPackageRule_For_AB(user:User,curTaskCode:string,nextTaskCode:string):Promise<PackageRule[]>
  { 
    console.log("Welcome to InventoryProvider::getPackageRule_For_AB() for " + curTaskCode + " " + nextTaskCode);
    
    let pRules:Array<PackageRule>;
    pRules = [];
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      this.afs.collection<PackageRule>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList`)
      .ref.where('curTaskCode','==',curTaskCode).where('nextTaskCode','==',nextTaskCode)
      .onSnapshot(querySnapshot => {
        console.log("querySnapshot.size: " + querySnapshot.size);
        for(let i=0;i<querySnapshot.docs.length;i++)
        {
          console.log("querySnapshot.doc: " + querySnapshot.docs[i].data().id);
          let pRule = {} as PackageRule;
          pRule.id = querySnapshot.docs[i].data().id;
          pRule.creation_time = querySnapshot.docs[i].data().creation_time;
          pRule.curTaskCode = querySnapshot.docs[i].data().curTaskCode;
          //pRule.packageStatus = doc.data().packageStatus;    
          pRule.nextTaskCode = querySnapshot.docs[i].data().nextTaskCode;
          pRule.bGroupRead = querySnapshot.docs[i].data().bGroupRead;
          pRule.sequenceID = querySnapshot.docs[i].data().sequenceID;
          pRule.bMultipleOutput = querySnapshot.docs[i].data().bMultipleOutput;
          pRule.bPartialAssign = querySnapshot.docs[i].data().bPartialAssign;
          pRule.bReducedOutput = querySnapshot.docs[i].data().bReducedOutput;
          pRule.bIncursCost = querySnapshot.docs[i].data().bIncursCost;
          pRule.bFirst = querySnapshot.docs[i].data().bFirst;
          pRule.bLast = querySnapshot.docs[i].data().bLast;
        
          pRules.push(pRule);
        };
        console.log("pRules.length " + pRules.length);
        resolve(pRules);
      });
    });
  }

  deletePackageRule(user:User,pID:string):Promise<boolean>
  { 
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList/${pID}`)
      .delete()
      .then(() => {
        resolve(true);
      })
      .catch(e => {
        console.log("Error from this.afs.doc(pItem.id).delete() " + e);
        reject(false);
      });
    });    
  }
  
  updatePackageRule(user:User,p:PackageRule):Promise<boolean>
  {
    console.log("Welcome to InventoryProvider:updatePackageRule(packageRule) " + p.id + " " + p.bGroupRead);
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      const docRef:AngularFirestoreDocument<PackageRule> = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/packageRulesList/${p.id}`);
      docRef.update({bGroupRead: !p.bGroupRead})
      .then(() => {
        resolve(true);
      })
      .catch(e => {
        console.log("Error from docRef.update " + e);
        reject(false);
      });
    });    
  }

  /* CRUD for Reference Data like organisationSettings, stateRegion etc */
  getAllStateRegions(user:User):AngularFirestoreCollection<StateRegion>
  {
    console.log("Welcome to FirebaseProvider:getAllStateRegions()");
    if(!checkIfValid(user))
    {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<StateRegion>(`/globalReferenceDataList/${user.dataVersionID}/stateRegionList`, 
                                                  ref => ref.orderBy('name')); 
  }

  getFinanceSettings(user:User):Promise<FinanceSettings>
  {
    console.log("Welcome to FirebaseProvider:getFinanceSettings()");
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      let financeSettings = {} as FinanceSettings;
      const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/settingsList/Finance`).ref;
      if(docRef)
      {
        docRef.get()
        .then(doc => {
          financeSettings.creation_time = (doc.data().creation_time);
          financeSettings.foreignCurrency = (doc.data().foreignCurrency);
          financeSettings.localCurrency = (doc.data().localCurrency);
          financeSettings.exchangeRate = +(doc.data().exchangeRate);
          financeSettings.dueDays = +(doc.data().dueDays);
          financeSettings.cGSTRate = +(doc.data().cGSTRate);
          financeSettings.sGSTRate = +(doc.data().sGSTRate);
          financeSettings.iGSTRate = +(doc.data().iGSTRate);
          resolve(financeSettings);        
        }).catch(e => {
          console.log("Error from docRef.get() " + e);
          reject(null);
        });
      }
      else
      {
        resolve(null);    
      }
    });
  }

  getExchangeRate(user:User):Promise<number>
  {
    console.log("Welcome to FirebaseProvider:getExchangeRate()");
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      let exchangeRate:number=1;
      const docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/settingsList/Finance`).ref;
      if(docRef)
      {
        docRef.get()
        .then(doc => {
          exchangeRate = +(doc.data().exchangeRate);
          resolve(exchangeRate);        
        }).catch(e => {
          console.log("Error from docRef.get() " + e);
          reject(1);
        });
      }
      else
      {
        resolve(1);    
      }
    });
     
  }

  updateFinanceSettings(user:User,s:FinanceSettings):Promise<boolean>
  {
    console.log("Welcome to FirebaseProvider:updateFinanceSettings(FinanceSettings)");
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      if((s.foreignCurrency === '') || (s.localCurrency === ''))
      {
        resolve(false);
      }
      let currentTime:string = (new Date().toISOString());
      this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/settingsList/Finance`).update({
        creation_time: currentTime,
        foreignCurrency: s.foreignCurrency,
        localCurrency: s.localCurrency,
        exchangeRate: s.exchangeRate,
        dueDays: s.dueDays,
        cGSTRate: s.cGSTRate,
        sGSTRate: s.sGSTRate,
        iGSTRate: s.iGSTRate
      }).then(() => {
        resolve(true);
      }).catch(e => {
        console.log("Error from doc.update() " + e);
        reject(false);
      }); 
    });    
  }  
}