//import { AngularFireModule } from 'angularfire2';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
//import { AngularFireDatabase } from 'angularfire2/database';
//import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';

//import { DateTime } from 'ionic-angular/components/datetime/datetime';
import { DocumentReference, WriteBatch } from '@firebase/firestore-types';
import { PackagedItem } from '../../model/packagedItem';
import { myConstants } from '../../model/myConstants';
import { getDateTimeString } from '../../model/utility';
import { User, checkIfValid } from '../../model/user';



/*
  Generated class for the InventoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InventoryProvider {
  //private fs:any;
  constructor(private afs: AngularFirestore) {
    console.log('Hello InventoryProvider Provider');
  }


  /*
  get_AtoB_packagedItems_For_A(curTaskCode:string):AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList',
                                            ref => ref.where('pRule.curTaskCode','==',curTaskCode)
                                                      .where('pRule.nextTaskCode','!=',curTaskCode)); 
  }*/

  getAll_AtoB_packagedItems(user: User): AngularFirestoreCollection<PackagedItem> {
    if (!checkIfValid(user)) {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB`);
  }

  getAll_BtoB_packagedItems(user: User): AngularFirestoreCollection<PackagedItem> {
    if (!checkIfValid(user)) {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB`);
  }

  getPackagedItemsListForBizTrxID(user: User, bizTrxID: string): Promise<PackagedItem[]> {
    console.log("Welcome to getPackagedItemsListForBizTrxID for " + bizTrxID);
    return new Promise((resolve, reject) => {
      let returnItemList: Array<PackagedItem>;
      returnItemList = [];
      if (!(checkIfValid(user) && (bizTrxID))) {
        console.log("Invalid user");
        resolve(returnItemList);
      }
      const colRef = this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB`).ref;
      const query = colRef.where('trxID', '==', bizTrxID).orderBy('creation_time');
      if (query) {
        query.get().then(querySnapshot => {
          console.log("querySnapshot.size: " + querySnapshot.size);
          returnItemList = []
          //await querySnapshot.forEach(doc => 
          for (let i = 0; i < querySnapshot.docs.length; i++) {
            console.log("querySnapshot.doc: " + querySnapshot.docs[i].data().id);
            let pItem = {} as PackagedItem;
            pItem.id = querySnapshot.docs[i].data().id;
            pItem.creation_time = querySnapshot.docs[i].data().creation_time;
            pItem.confidentialityLevel = querySnapshot.docs[i].data().confidentialityLevel;
            pItem.userID = querySnapshot.docs[i].data().userID;
            pItem.packageID = querySnapshot.docs[i].data().packageID;
            pItem.parentID = querySnapshot.docs[i].data().parentID;
            pItem.packageGroupID = querySnapshot.docs[i].data().packageGroupID;
            pItem.packageGroupName = querySnapshot.docs[i].data().packageGroupName;
            pItem.packageStatus = querySnapshot.docs[i].data().packageStatus;
            pItem.packageCurTaskOwner = querySnapshot.docs[i].data().packageCurTaskOwner;
            pItem.packageNextTaskOwner = querySnapshot.docs[i].data().packageNextTaskOwner;
            pItem.pRule = querySnapshot.docs[i].data().pRule;
            pItem.trxID = querySnapshot.docs[i].data().trxID;
            pItem.trxPartyID = querySnapshot.docs[i].data().trxPartyID;
            pItem.trxPartyName = querySnapshot.docs[i].data().trxPartyName;
            pItem.trxDate = querySnapshot.docs[i].data().trxDate;
            pItem.trxWeight = querySnapshot.docs[i].data().trxWeight;
            pItem.trxRate = querySnapshot.docs[i].data().trxRate;
            pItem.trxCurrency = querySnapshot.docs[i].data().trxCurrency;
            pItem.trxShare = querySnapshot.docs[i].data().trxShare;
            pItem.itemValuation = querySnapshot.docs[i].data().itemValuation;
            pItem.itemRemarks = querySnapshot.docs[i].data().itemRemarks;
            pItem.itemWeight = querySnapshot.docs[i].data().itemWeight;
            pItem.itemType = querySnapshot.docs[i].data().itemType;
            pItem.itemQty = querySnapshot.docs[i].data().itemQty;
            pItem.taskCost = querySnapshot.docs[i].data().taskCost;
            pItem.accumulatedTaskCost = querySnapshot.docs[i].data().accumulatedTaskCost;
            pItem.roughWeight = querySnapshot.docs[i].data().roughWeight;
            pItem.itemShape = querySnapshot.docs[i].data().itemShape;
            pItem.itemSieve = querySnapshot.docs[i].data().itemSieve;
            pItem.itemQualityLevel = querySnapshot.docs[i].data().itemQualityLevel;

            returnItemList.push(pItem);
          }//end of FOR loop
          console.log("returnItemList.length " + returnItemList.length);
          resolve(returnItemList);
        }).catch(e => {
          console.log("Error from query.get() " + e);
          reject(returnItemList)
        });
      }
      else {
        resolve(returnItemList);
      }
    });
  }




  /* Order by itemShape,itemSieve,itemQualityLevel is required for showing sales stock */
  get_packagedItems_For_B(user: User, taskCode: string): AngularFirestoreCollection<PackagedItem> {
    if (!checkIfValid(user)) {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB`,
      ref => ref.where('pRule.nextTaskCode', '==', taskCode)
        .orderBy('itemShape.rank')
        .orderBy('itemSieve.rank')
        .orderBy('itemQualityLevel.rank'));
  }

  get_packagedItems_For_A(user: User, taskCode: string): AngularFirestoreCollection<PackagedItem> {
    if (!checkIfValid(user)) {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB`,
      ref => ref.where('pRule.curTaskCode', '==', taskCode)
        .orderBy('itemShape.rank')
        .orderBy('itemSieve.rank')
        .orderBy('itemQualityLevel.rank'));
  }

  get_BtoB_IP_packagedItems(user: User, taskCode: string): AngularFirestoreCollection<PackagedItem> {
    if (!checkIfValid(user)) {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB`,
      ref => ref.where('pRule.curTaskCode', '==', taskCode)
        .where('pRule.nextTaskCode', '==', taskCode)
        .where('packageStatus', '==', myConstants.packageStatus.IP));

  }

  get_BtoB_IP_packagedItemsList(user: User, taskCode: string): Promise<PackagedItem[]> {
    console.log("Welcome to get_BtoB_IP_packagedItemsList for " + taskCode);
    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        resolve([]);
      }
      let returnItemList: Array<PackagedItem>;
      returnItemList = [];
      const colRef = this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB`,
        ref => ref.where('pRule.curTaskCode', '==', taskCode)
          .where('pRule.nextTaskCode', '==', taskCode)
          .where('packageStatus', '==', myConstants.packageStatus.IP)).ref;
      colRef.get()
        .then(querySnapshot => {
          //console.log("querySnapshot.size: " + querySnapshot.size);
          returnItemList = []
          //await querySnapshot.forEach(doc => 
          for (let i = 0; i < querySnapshot.docs.length; i++) {
            //console.log("querySnapshot.doc: " + querySnapshot.docs[i].data().id);
            let pItem = {} as PackagedItem;
            pItem.id = querySnapshot.docs[i].data().id;
            pItem.creation_time = querySnapshot.docs[i].data().creation_time;
            pItem.confidentialityLevel = querySnapshot.docs[i].data().confidentialityLevel;
            pItem.userID = querySnapshot.docs[i].data().userID;
            pItem.packageID = querySnapshot.docs[i].data().packageID;
            pItem.parentID = querySnapshot.docs[i].data().parentID;
            pItem.packageGroupID = querySnapshot.docs[i].data().packageGroupID;
            pItem.packageGroupName = querySnapshot.docs[i].data().packageGroupName;
            pItem.packageStatus = querySnapshot.docs[i].data().packageStatus;
            pItem.packageCurTaskOwner = querySnapshot.docs[i].data().packageCurTaskOwner;
            pItem.packageNextTaskOwner = querySnapshot.docs[i].data().packageNextTaskOwner;
            pItem.pRule = querySnapshot.docs[i].data().pRule;
            pItem.trxID = querySnapshot.docs[i].data().trxID;
            pItem.trxPartyID = querySnapshot.docs[i].data().trxPartyID;
            pItem.trxPartyName = querySnapshot.docs[i].data().trxPartyName;
            pItem.trxDate = querySnapshot.docs[i].data().trxDate;
            pItem.trxWeight = querySnapshot.docs[i].data().trxWeight;
            pItem.trxRate = querySnapshot.docs[i].data().trxRate;
            pItem.trxCurrency = querySnapshot.docs[i].data().trxCurrency;
            pItem.trxShare = querySnapshot.docs[i].data().trxShare;
            pItem.itemValuation = querySnapshot.docs[i].data().itemValuation;
            pItem.itemRemarks = querySnapshot.docs[i].data().itemRemarks;
            pItem.itemWeight = querySnapshot.docs[i].data().itemWeight;
            pItem.itemType = querySnapshot.docs[i].data().itemType;
            pItem.itemQty = querySnapshot.docs[i].data().itemQty;
            pItem.taskCost = querySnapshot.docs[i].data().taskCost;
            pItem.accumulatedTaskCost = querySnapshot.docs[i].data().accumulatedTaskCost;
            pItem.roughWeight = querySnapshot.docs[i].data().roughWeight;
            pItem.itemShape = querySnapshot.docs[i].data().itemShape;
            pItem.itemSieve = querySnapshot.docs[i].data().itemSieve;
            pItem.itemQualityLevel = querySnapshot.docs[i].data().itemQualityLevel;

            returnItemList.push(pItem);
          }
          console.log("returnItemList.length " + returnItemList.length);
          resolve(returnItemList);
        }).catch(e => {
          console.log("Error from colRef.get() " + e);
          reject([]);
        });
    });
  }


  undo_BtoC_RD_packagedItem(user: User, pItem: PackagedItem): Promise<boolean> {
    console.log("Welcome to InventoryProvider::undo_BtoC_RD_packagedItem() " + pItem.parentID);

    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        resolve(false);
      }
      /*
      01) This is B-C, RD 
      02) Find siblings i.e. similar B-C entries having same parentID
      03) Delete all these B-C entries
      04) If parent is B-B then update its status to IP
      05) if parent is A-B then update its status to RD
      */
      let parentID: string = pItem.parentID;
      let siblingItemID: string = "";
      let batch: WriteBatch = this.afs.firestore.batch();
      const siblingsColRef = this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB`).ref;
      const query = siblingsColRef.where('parentID', '==', parentID);//.orderBy('creation_time');
      query.get().then(querySnapshot => {
        console.log("size of siblingsColRef: " + querySnapshot.size);
        for (let i = 0; i < querySnapshot.docs.length; i++) {
          //  console.log("querySnapshot.doc: " + querySnapshot.docs[i].data().id);
          siblingItemID = querySnapshot.docs[i].data().id;
          let docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${siblingItemID}`).ref;
          batch.delete(docRef);
        }

        let BtoBItemID: string = "";
        const parentColRef = this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB`).ref;
        const parentQuery = parentColRef.where('parentID', '==', parentID);
        parentQuery.get().then(querySnapshot => {
          console.log("size of parentColRef: " + querySnapshot.size);
          if (querySnapshot.docs.length === 1) {
            console.log("Update B-B to IP");
            BtoBItemID = querySnapshot.docs[0].data().id;
            let parentDocRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB/${BtoBItemID}`).ref;
            batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.IP });
          }
          else if (querySnapshot.docs.length === 0) {
            console.log("Update A-B to RD");
            let parentDocRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${pItem.parentID}`).ref;
            batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.RD });
          }
          batch.commit()
            .then(() => {
              console.log("committed");
              resolve(true);
            }).catch(e => {
              console.log("Error from batch.commit() " + e);
              reject(null);
            });
        }).catch(e => {
          console.log("Error from BtoBColRef.get() " + e);
          reject(false);
        });
      }).catch(e => {
        console.log("Error from siblingsColRef.get() " + e);
        reject(false);
      });
    });//end of promise    
  }

  undo_BtoB_IP_packagedItem(user: User, pItem: PackagedItem): Promise<boolean> {
    console.log("Welcome to InventoryProvider::undo_BtoB_IP_packagedItem() " + pItem.parentID);

    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        resolve(false);
      }
      /*
      01) This is B-B, IP 
      02) Delete this B-B,IP
      03) update status of parent A-B to RD
      */
      let batch: WriteBatch = this.afs.firestore.batch();
      let docRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB/${pItem.id}`).ref;
      batch.delete(docRef);

      let parentDocRef = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${pItem.parentID}`).ref;
      batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.RD });

      batch.commit()
        .then(() => {
          console.log("committed");
          resolve(true);
        }).catch(e => {
          console.log("Error from batch.commit() " + e);
          reject(null);
        });
    });//end of promise    
  }

  get_BtoB_CO_packagedItems(user: User, taskCode: string): AngularFirestoreCollection<PackagedItem> {
    if (!checkIfValid(user)) {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB`,
      ref => ref.where('pRule.curTaskCode', '==', taskCode)
        .where('pRule.nextTaskCode', '==', taskCode)
        .where('packageStatus', '==', myConstants.packageStatus.CO));
  }

  get_BtoB_packagedItems(user: User, taskCode: string): AngularFirestoreCollection<PackagedItem> {
    if (!checkIfValid(user)) {
      console.log("Invalid user");
      return null;
    }
    return this.afs.collection<PackagedItem>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB`,
      ref => ref.where('pRule.curTaskCode', '==', taskCode)
        .where('pRule.nextTaskCode', '==', taskCode));
  }


  /*
  async createPackagedItem_BtoC(pItem:packagedItem,ppID:string): Promise<void>
  {
    console.log("Welcome to InventoryProvider:createPackagedItem_BtoC(packagedItem)")
    console.log("pItem=>" + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
    console.log("pItem=>" + pItem.itemWeight + " " + pItem.itemQty + " " + pItem.itemCost + " " + pItem.itemTax);
    console.log("pItem=>" + pItem.parentPackageID + " " + pItem.packageID + " " + pItem.packageGroupID);
    
    //const ID: string = this.afs.createId();
    const ID: string = pItem.pRule.curTaskCode + "_" + pItem.pRule.nextTaskCode + "_" + (new Date().toISOString());
    
   // console.log("FieldValue.serverTimestamp():" + firebase.firestore.FieldValue.serverTimestamp());
    console.log("ID " + ID);
    //return new Promise((resolve,reject)=>{  
    let docRefBtoC:DocumentReference = this.afs.doc(`/packagedItemList_AB/${ID}`).ref;
    console.log("docRef: " + docRefBtoC.id);
    let docRefSummaryC:DocumentReference = this.afs.doc(`/inventorySummaryList/${pItem.pRule.nextTaskCode}`).ref;
    
    //let batch:WriteBatch = this.afs.firestore.batch();
    
    if(docRefBtoC && docRefSummaryC)
    {
      console.log("Calling transaction.set for docRefBtoC " + docRefBtoC.id);
      return this.afs.firestore.runTransaction(transaction => 
      {
        //return transaction.get(docRefSummaryC).then(docSummaryC =>
        return transaction.get(docRefBtoC).then(docBtoC => 
        {
          /*
          let newInput = {} as summaryDetails;
          newInput.totalWeight = docSummaryC.data().input.totalWeight + pItem.itemWeight;
          newInput.totalQty = docSummaryC.data().input.totalQty + pItem.itemQty;
          newInput.totalCost = docSummaryC.data().input.totalCost + pItem.itemCost;
          newInput.totalTax = docSummaryC.data().input.totalTax + pItem.itemTax;

          let newPending = {} as summaryDetails;
          newPending.totalWeight = docSummaryC.data().pending.totalWeight + pItem.itemWeight;
          newPending.totalQty = docSummaryC.data().pending.totalQty + pItem.itemQty;
          newPending.totalCost = docSummaryC.data().pending.totalCost + pItem.itemCost;
          newPending.totalTax = docSummaryC.data().pending.totalTax + pItem.itemTax;
          */ /*
  if(ppID !== "")
  {
    console.log("Parent exists");
    let docRefBtoB:DocumentReference = this.afs.doc(`/packagedItemList_BB/${ppID}`).ref;
    let docRefSummaryB:DocumentReference = this.afs.doc(`/inventorySummaryList/${pItem.pRule.curTaskCode}`).ref;
    if(docRefBtoB && docRefSummaryB)
    {
     // return transaction.get(docRefSummaryB).then(docSummaryB => 
     {
        return transaction.get(docRefBtoB).then(docBtoB => {
          /*
          let newCompleted = {} as summaryDetails;
          newCompleted.totalWeight = docSummaryB.data().completed.totalWeight + docBtoB.data().itemWeight;
          newCompleted.totalQty = docSummaryB.data().completed.totalQty + docBtoB.data().itemQty;
          newCompleted.totalCost = docSummaryB.data().completed.totalCost + docBtoB.data().itemCost;
          newCompleted.totalTax = docSummaryB.data().completed.totalTax + docBtoB.data().itemTax;
          
          let newInProgress = {} as summaryDetails;
          newInProgress.totalWeight = docSummaryB.data().inProgress.totalWeight - docBtoB.data().itemWeight;
          newInProgress.totalQty = docSummaryB.data().inProgress.totalQty - docBtoB.data().itemQty;
          newInProgress.totalCost = docSummaryB.data().inProgress.totalCost - docBtoB.data().itemCost;
          newInProgress.totalTax = docSummaryB.data().inProgress.totalTax - docBtoB.data().itemTax;
          
          let newOutput = {} as summaryDetails;
          newOutput.totalWeight = docSummaryB.data().output.totalWeight + pItem.itemWeight;
          newOutput.totalQty = docSummaryB.data().output.totalQty + pItem.itemQty;
          newOutput.totalCost = docSummaryB.data().output.totalCost + pItem.itemCost;
          newOutput.totalTax = docSummaryB.data().output.totalTax + pItem.itemTax;
          */ /*
  //transaction.update(docRefSummaryB, {inProgress:newInProgress,completed:newCompleted,output:newOutput});
  transaction.update(docRefBtoB,{packageStatus:myConstants.packageStatus.CO}); 
  //transaction.update(docRefSummaryC, {input:newInput,pending:newPending});
  transaction.set(docRefBtoC,{
    id:ID,
    creation_time:new Date().toLocaleString(),
    packageID:pItem.packageID,
    parentPackageID:pItem.parentPackageID,
    packageGroupID:pItem.packageGroupID,
    packageStatus:pItem.packageStatus,
    pRule:pItem.pRule,
    itemWeight:pItem.itemWeight,
    itemQty:pItem.itemQty,
    itemCost:pItem.itemCost,
    itemTax:pItem.itemTax,
    itemRemarks:pItem.itemRemarks    
  }); 
});//end of transaction.get(docRefBtoB).then(docBtoB
}//);//end of transaction.get(docRefSummaryB).then(docSummaryB
}//end of if(docRefBtoB && docRefSummaryB)            
}//end of if(ppID !== "")
else
{
// transaction.update(docRefSummaryC, {input:newInput,pending:newPending});
transaction.set(docRefBtoC,{
id:ID,
creation_time:new Date().toLocaleString(),
packageID:pItem.packageID,
parentPackageID:pItem.parentPackageID,
packageGroupID:pItem.packageGroupID,
packageStatus:pItem.packageStatus,
pRule:pItem.pRule,
itemWeight:pItem.itemWeight,
itemQty:pItem.itemQty,
itemCost:pItem.itemCost,
itemTax:pItem.itemTax,
itemRemarks:pItem.itemRemarks    
}); 
}
});//end of transaction.get(docRefBtoC).then(docBtoC
});//end of this.afs.firestore.runTransaction(transaction
}//end of if(docRefBtoC && docRefSummaryC) 
} */

  createPackagedItem_BtoC(user: User, pItem: PackagedItem, pParentItem: PackagedItem): Promise<string> {
    console.log("Welcome to InventoryProvider:createPackagedItem(packagedItem)");
    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        resolve(null);
      }
      /*
      console.log("pItem=>" + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
      console.log("pItem=>" + pItem.itemWeight + " " + pItem.itemQty + " " + pItem.itemCost + " " + pItem.itemTax);
      console.log("pItem=>" + pItem.parentID + " " + pItem.packageID + " " + pItem.packageGroupID);
      */
      //const ID: string = this.afs.createId();
      const ID: string = pItem.pRule.curTaskCode + "_" + pItem.pRule.nextTaskCode + "_" + pItem.itemWeight + "_" + (getDateTimeString(new Date()));

      console.log("ID " + ID);
      //return new Promise((resolve,reject)=>{  
      let docRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${ID}`).ref;
      console.log("docRef: " + docRef.id);
      let batch: WriteBatch = this.afs.firestore.batch();
      if (docRef) {
        console.log("Calling batch.set for docRef " + docRef.id);
        batch.set(docRef, {
          id: ID,
          creation_time: getDateTimeString(new Date()),
          confidentialityLevel: pItem.confidentialityLevel,
          userID: user.ID,
          packageID: pItem.packageID,
          parentID: pItem.parentID,
          packageGroupID: pItem.packageGroupID,
          packageGroupName: pItem.packageGroupName,
          packageStatus: pItem.packageStatus,
          pRule: pItem.pRule,
          trxID: pItem.trxID,
          trxPartyID: pItem.trxPartyID,
          trxPartyName: pItem.trxPartyName,
          trxDate: pItem.trxDate,
          trxWeight: pItem.trxWeight,
          trxRate: pItem.trxRate,
          trxCurrency: pItem.trxCurrency,
          trxShare: pItem.trxShare,
          itemValuation: pItem.itemValuation,
          itemWeight: pItem.itemWeight,
          itemType: pItem.itemType,
          itemQty: pItem.itemQty,
          taskCost: pItem.taskCost,
          accumulatedTaskCost: pItem.accumulatedTaskCost,
          roughWeight: pItem.roughWeight,
          itemShape: pItem.itemShape,
          itemSieve: pItem.itemSieve,
          itemQualityLevel: pItem.itemQualityLevel,
          itemRemarks: pItem.itemRemarks
        });
      }

      if (pParentItem) {
        console.log("Parent exists");
        if (pParentItem.pRule.curTaskCode === pParentItem.pRule.nextTaskCode) {
          let parentDocRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB/${pParentItem.id}`).ref;
          batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.CO });
        }
        else {
          let parentDocRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${pParentItem.id}`).ref;
          batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.CO });
        }
      }
      batch.commit()
        .then(() => {
          console.log("committed");
          resolve(ID);
        }).catch(e => {
          console.log("Error from batch.commit() " + e);
          reject(null);
        });
    });
  }


  createPackagedItem_BtoB(user: User, pItem: PackagedItem, pParentItem: PackagedItem): Promise<string> {
    console.log("Welcome to InventoryProvider:createPackagedItem(packagedItem)");
    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        resolve(null);
      }
      /*
      console.log("pItem=>" + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
      console.log("pItem=>" + pItem.itemWeight + " " + pItem.itemQty + " " + pItem.itemCost + " " + pItem.itemTax);
      console.log("pItem=>" + pItem.parentID + " " + pItem.packageID + " " + pItem.packageGroupID);
      */
      //const ID: string = this.afs.createId();
      const ID: string = pItem.pRule.curTaskCode + "_" + pItem.pRule.nextTaskCode + "_" + pItem.itemWeight + "_" + (getDateTimeString(new Date()));
      // console.log("FieldValue.serverTimestamp():" + firebase.firestore.FieldValue.serverTimestamp());
      //return new Promise((resolve,reject)=>{  
      let docRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB/${ID}`).ref;
      let batch: WriteBatch = this.afs.firestore.batch();
      if (docRef) {
        console.log("Calling batch.set for docRef " + docRef.id);
        batch.set(docRef, {
          id: ID,
          creation_time: getDateTimeString(new Date()),
          confidentialityLevel: pItem.confidentialityLevel,
          userID: user.ID,
          packageID: pItem.packageID,
          parentID: pItem.parentID,
          packageGroupID: pItem.packageGroupID,
          packageGroupName: pItem.packageGroupName,
          packageStatus: pItem.packageStatus,
          pRule: pItem.pRule,
          trxID: pItem.trxID,
          trxPartyID: pItem.trxPartyID,
          trxPartyName: pItem.trxPartyName,
          trxDate: pItem.trxDate,
          trxWeight: pItem.trxWeight,
          trxRate: pItem.trxRate,
          trxCurrency: pItem.trxCurrency,
          trxShare: pItem.trxShare,
          itemValuation: pItem.itemValuation,
          itemWeight: pItem.itemWeight,
          itemType: pItem.itemType,
          itemQty: pItem.itemQty,
          taskCost: pItem.taskCost,
          accumulatedTaskCost: pItem.accumulatedTaskCost,
          roughWeight: pItem.roughWeight,
          itemShape: pItem.itemShape,
          itemSieve: pItem.itemSieve,
          itemQualityLevel: pItem.itemQualityLevel,
          itemRemarks: pItem.itemRemarks
        });
      }
      if (pParentItem) {
        console.log("Parent exists");
        if (pParentItem.pRule.curTaskCode === pParentItem.pRule.nextTaskCode) {
          let parentDocRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB/${pParentItem.id}`).ref;
          batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.CO });
        }
        else {
          let parentDocRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${pParentItem.id}`).ref;
          batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.CO });
        }
      }
      batch.commit()
        .then(() => {
          console.log("committed");
          resolve(ID);
        }).catch(e => {
          console.log("Error from batch.commit() " + e);
          reject(null);
        });
    });
  }
  /*
  async createPackagedItem_BtoB(pItem:packagedItem,ppID:string): Promise<void>
  {
    console.log("Welcome to InventoryProvider:createPackagedItem(packagedItem)")
    console.log("pItem=>" + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
    console.log("pItem=>" + pItem.itemWeight + " " + pItem.itemQty + " " + pItem.itemCost + " " + pItem.itemTax);
    console.log("pItem=>" + pItem.parentPackageID + " " + pItem.packageID + " " + pItem.packageGroupID);
    
    //const ID: string = this.afs.createId();
    const ID: string = pItem.pRule.curTaskCode + "_" + pItem.pRule.nextTaskCode + "_" + (new Date().toISOString());
    
   // console.log("FieldValue.serverTimestamp():" + firebase.firestore.FieldValue.serverTimestamp());
    console.log("ID " + ID);
    //return new Promise((resolve,reject)=>{  
    let docRefBtoB:DocumentReference = this.afs.doc(`/packagedItemList_BB/${ID}`).ref;
    let docRefSummaryB:DocumentReference = this.afs.doc(`/inventorySummaryList/${pItem.pRule.curTaskCode}`).ref;
    console.log("docRef: " + docRefBtoB.id);
    
    return this.afs.firestore.runTransaction(transaction => {
      //return transaction.get(docRefSummaryB).then(docSummaryB => 
      return transaction.get(docRefBtoB).then(docBtoB => 
      {
        /*
        let newInProgress = {} as summaryDetails;
        newInProgress.totalWeight = docSummaryB.data().inProgress.totalWeight + pItem.itemWeight;
        newInProgress.totalQty = docSummaryB.data().inProgress.totalQty + pItem.itemQty;
        newInProgress.totalCost = docSummaryB.data().inProgress.totalCost + pItem.itemCost;
        newInProgress.totalTax = docSummaryB.data().inProgress.totalTax + pItem.itemTax;

        let newPending = {} as summaryDetails;
        newPending.totalWeight = docSummaryB.data().pending.totalWeight - pItem.itemWeight;
        newPending.totalQty = docSummaryB.data().pending.totalQty - pItem.itemQty;
        newPending.totalCost = docSummaryB.data().pending.totalCost - pItem.itemCost;
        newPending.totalTax = docSummaryB.data().pending.totalTax - pItem.itemTax;
        */ /*
  //transaction.update(docRefSummaryB, {inProgress:newInProgress,pending:newPending});

  if(docRefBtoB)
  {
    console.log("Calling transaction.set for docRefBtoB " + docRefBtoB.id);
    transaction.set(docRefBtoB,{
      id:ID,
      creation_time:new Date().toLocaleString(),
      packageID:pItem.packageID,
      parentPackageID:pItem.parentPackageID,
      packageGroupID:pItem.packageGroupID,
      packageStatus:pItem.packageStatus,
      pRule:pItem.pRule,
      itemWeight:pItem.itemWeight,
      itemQty:pItem.itemQty,
      itemCost:pItem.itemCost,
      itemTax:pItem.itemTax,
      itemRemarks:pItem.itemRemarks    
    });
  }  

  if(ppID !== "")
  {
    console.log("Parent exists");
    let AtoBdocRef:DocumentReference = this.afs.doc(`/packagedItemList_AB/${ppID}`).ref
    transaction.update(AtoBdocRef,{packageStatus:myConstants.packageStatus.CO});
  }
  
  console.log("committed");
});
});
} */

  createPackagedItemArray_BtoC(user: User, pItemList: Array<PackagedItem>, pParentItem: PackagedItem): Promise<boolean> {
    console.log("Welcome to InventoryProvider:createPackagedItemArray_BtoC(packagedItem)");
    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        return (false);
      }
      console.log("length of pItemList " + pItemList.length);
      //await pItemList.forEach(pItem => 
      let batch: WriteBatch = this.afs.firestore.batch();
      for (let i = 0; i < pItemList.length; i++) {
        //  const ID: string = this.afs.createId();
        const ID: string = pItemList[i].pRule.curTaskCode + "_" + pItemList[i].pRule.nextTaskCode + "_" + pItemList[i].itemWeight + "_" + this.afs.createId();

        let docRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${ID}`).ref;
        if (docRef) {
          batch.set(docRef, {
            id: ID,
            creation_time: getDateTimeString(new Date()),
            confidentialityLevel: pItemList[i].confidentialityLevel,
            userID: user.ID,
            packageID: pItemList[i].packageID,
            parentID: pItemList[i].parentID,
            packageGroupID: pItemList[i].packageGroupID,
            packageGroupName: pItemList[i].packageGroupName,
            packageStatus: pItemList[i].packageStatus,
            pRule: pItemList[i].pRule,
            trxID: pItemList[i].trxID,
            trxPartyID: pItemList[i].trxPartyID,
            trxPartyName: pItemList[i].trxPartyName,
            trxDate: pItemList[i].trxDate,
            trxWeight: pItemList[i].trxWeight,
            trxRate: pItemList[i].trxRate,
            trxCurrency: pItemList[i].trxCurrency,
            trxShare: pItemList[i].trxShare,
            itemValuation: pItemList[i].itemValuation,
            itemWeight: pItemList[i].itemWeight,
            itemType: pItemList[i].itemType,
            itemQty: pItemList[i].itemQty,
            taskCost: pItemList[i].taskCost,
            accumulatedTaskCost: pItemList[i].accumulatedTaskCost,
            roughWeight: pItemList[i].roughWeight,
            itemShape: pItemList[i].itemShape,
            itemSieve: pItemList[i].itemSieve,
            itemQualityLevel: pItemList[i].itemQualityLevel,
            itemRemarks: pItemList[i].itemRemarks
          });
        }
      };
      if ((pParentItem) && (pParentItem.id)) {
        console.log("Parent exists");
        if (pParentItem.pRule.curTaskCode === pParentItem.pRule.nextTaskCode) {
          let parentDocRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB/${pParentItem.id}`).ref;
          batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.CO });
        }
        else {
          let parentDocRef: DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${pParentItem.id}`).ref;
          batch.update(parentDocRef, { packageStatus: myConstants.packageStatus.CO });
        }
      }
      batch.commit()
        .then(() => {
          console.log("committed");
          resolve(true);
        }).catch(e => {
          console.log("Error from batch.commit() " + e);
          reject(false);
        });
    });

  }

  /*
  async createPackagedItemArray_BtoC(pItemList:Array<packagedItem>,ppID:string):Promise<void>
  {
    console.log("Welcome to InventoryProvider:createPackagedItem(packagedItem)")
    if(ppID === "")
    {
      return;
    }
    let itemTotal = {} as summaryDetails;
    itemTotal.totalWeight = 0;
    itemTotal.totalQty = 0;
    itemTotal.totalCost = 0;
    itemTotal.totalTax = 0;
    for(let i=0;i<pItemList.length;i++)
    {
      itemTotal.totalWeight += +pItemList[i].itemWeight;
      itemTotal.totalQty += +pItemList[i].itemQty;
      itemTotal.totalCost += +pItemList[i].itemCost;
      itemTotal.totalTax += +pItemList[i].itemTax;
    }
    //await pItemList.forEach(pItem => 
    let batch:WriteBatch = this.afs.firestore.batch();
    let docRefSummaryC:DocumentReference = this.afs.doc(`/inventorySummaryList/${pItemList[0].pRule.nextTaskCode}`).ref;

    return this.afs.firestore.runTransaction(transaction => {
      //return transaction.get(docRefSummaryC).then(docSummaryC => 
      {
        /*
        let newInput = {} as summaryDetails;
        newInput.totalWeight = docSummaryC.data().input.totalWeight + itemTotal.totalWeight;
        newInput.totalQty = docSummaryC.data().input.totalQty + itemTotal.totalQty;
        newInput.totalCost = docSummaryC.data().input.totalCost + itemTotal.totalCost;
        newInput.totalTax = docSummaryC.data().input.totalTax + itemTotal.totalTax;
        
        let newPending = {} as summaryDetails;
        newPending.totalWeight = docSummaryC.data().pending.totalWeight + itemTotal.totalWeight;
        newPending.totalQty = docSummaryC.data().pending.totalQty + itemTotal.totalQty;
        newPending.totalCost = docSummaryC.data().pending.totalCost + itemTotal.totalCost;
        newPending.totalTax = docSummaryC.data().pending.totalTax + itemTotal.totalTax;
        */ /*
  let docRefBtoB:DocumentReference = this.afs.doc(`/packagedItemList_BB/${ppID}`).ref;
  let docRefSummaryB:DocumentReference = this.afs.doc(`/inventorySummaryList/${pItemList[0].pRule.curTaskCode}`).ref;
  if(docRefBtoB && docRefSummaryB)
  {
    //return transaction.get(docRefSummaryB).then(docSummaryB => 
    {
      return transaction.get(docRefBtoB).then(docBtoB => {
        /*
        let newCompleted = {} as summaryDetails;
        newCompleted.totalWeight = docSummaryB.data().completed.totalWeight + docBtoB.data().itemWeight;
        newCompleted.totalQty = docSummaryB.data().completed.totalQty + docBtoB.data().itemQty;
        newCompleted.totalCost = docSummaryB.data().completed.totalCost + docBtoB.data().itemCost;
        newCompleted.totalTax = docSummaryB.data().completed.totalTax + docBtoB.data().itemTax;
        
        let newInProgress = {} as summaryDetails;
        newInProgress.totalWeight = docSummaryB.data().inProgress.totalWeight - docBtoB.data().itemWeight;
        newInProgress.totalQty = docSummaryB.data().inProgress.totalQty - docBtoB.data().itemQty;
        newInProgress.totalCost = docSummaryB.data().inProgress.totalCost - docBtoB.data().itemCost;
        newInProgress.totalTax = docSummaryB.data().inProgress.totalTax - docBtoB.data().itemTax;
        
        let newOutput = {} as summaryDetails;
        newOutput.totalWeight = docSummaryB.data().output.totalWeight + itemTotal.totalWeight;
        newOutput.totalQty = docSummaryB.data().output.totalQty + itemTotal.totalQty;
        newOutput.totalCost = docSummaryB.data().output.totalCost + itemTotal.totalCost;
        newOutput.totalTax = docSummaryB.data().output.totalTax + itemTotal.totalTax;
        */ /*
 // transaction.update(docRefSummaryB, {inProgress:newInProgress,completed:newCompleted,output:newOutput});
  transaction.update(docRefBtoB,{packageStatus:myConstants.packageStatus.CO}); 
 // transaction.update(docRefSummaryC, {input:newInput,pending:newPending});
  for(let i=0;i<pItemList.length;i++)
  {
  //  const ID: string = this.afs.createId();
    const ID: string = pItemList[i].pRule.curTaskCode + "_" + pItemList[i].pRule.nextTaskCode + "_" + this.afs.createId();
  
    let docRef:DocumentReference = this.afs.doc(`/packagedItemList_AB/${ID}`).ref;
    console.log("docRef: " + docRef.id);
    if(docRef)
    {
      console.log("Calling transaction.set for docRef " + docRef.id);
      transaction.set(docRef,{
        id:ID,
        creation_time:new Date().toLocaleString(),
        packageID:pItemList[i].packageID,
        parentPackageID:pItemList[i].parentPackageID,
        packageGroupID:pItemList[i].packageGroupID,
        packageStatus:pItemList[i].packageStatus,
        pRule:pItemList[i].pRule,
        itemWeight:pItemList[i].itemWeight,
        itemQty:pItemList[i].itemQty,
        itemCost:pItemList[i].itemCost,
        itemTax:pItemList[i].itemTax,
        itemRemarks:pItemList[i].itemRemarks    
      });
    }
  }//end of FOR loop , to set pItemList[i]
});//end of transaction.get(docRefBtoB).then(docBtoB
}//);//end of transaction.get(docRefSummaryB).then(docSummaryB
}//end of if(docRefBtoB && docRefSummaryB)            
}//);//end of transaction.get(docRefSummaryC).then(docRefSummaryC
});//end of this.afs.firestore.runTransaction(transaction
} */

  updateStatus_For_AtoB_PackagedItem(user: User, pItemID: string, status: myConstants.packageStatus): Promise<boolean> {
    console.log("Welcome to InventoryProvider:updatePackageRule(packageRule) " + pItemID);
    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        resolve(false);
      }
      const docRef: AngularFirestoreDocument<PackagedItem> = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${pItemID}`);
      docRef.update({ packageStatus: status })
        .then(() => {
          resolve(true);
        }).catch(e => {
          console.log("Error from docRef.update " + e);
          reject(true);
        });
    });
  }

  deletePackagedItem(user: User, pItem: PackagedItem): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!checkIfValid(user)) {
        console.log("Invalid user");
        resolve(false);
      }
      if (pItem.pRule.curTaskCode === pItem.pRule.nextTaskCode) {
        this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_BB/${pItem.id}`)
          .delete()
          .then(() => {
            resolve(true);
          })
          .catch(e => {
            console.log("Error from this.afs.doc(pItem.id).delete() " + e);
            reject(false);
          });
      }
      else {
        return this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_packagedItemList_AB/${pItem.id}`)
          .delete()
          .then(() => {
            resolve(true);
          })
          .catch(e => {
            console.log("Error from this.afs.doc(pItem.id).delete() " + e);
            reject(false);
          });
      }
    });
  }


  /*
  async updateStatus_For_AtoB_PackagedItemList(pItemList:Array<PackagedItem>,group:string,status:myConstants.packageStatus):Promise<any>
  {
    console.log("Welcome to InventoryProvider:updatePackagedItemList(group) " + group);

    //TBD : This need to be transaction
    for(let i=0;i<pItemList.length;i++)
    {
      const docRef:AngularFirestoreDocument<PackagedItem> = this.afs.doc(`/packagedItemList_AB/${pItemList[i].id}`);
      docRef.update({packageStatus : status});
    }
    
  }*/
}
