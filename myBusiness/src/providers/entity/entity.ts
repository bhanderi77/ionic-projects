import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { DocumentReference } from '@firebase/firestore-types';
//import { WriteBatch, DocumentReference } from '@firebase/firestore-types';

import { Injectable } from '@angular/core';

import { BusinessContact } from '../../model/businessContact';
import { getDateTimeString } from '../../model/utility';
//import { PartySummary } from '../../model/partySummary';
import { User,checkIfValid } from '../../model/user';
import { Organisation } from '../../model/organisation';


/*
  Generated class for the EntityProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EntityProvider {

  constructor(private afs:AngularFirestore) {
    console.log('Hello EntityProvider Provider');
  }

  /* CRUD for businessContact */
  createBusinessContact(user:User,c:BusinessContact):Promise<string>
  {
    console.log("Welcome to EntityProvider:createBusinessContact(BusinessContact)");
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(null);
      }
      const current_time:string = getDateTimeString(new Date());
      const contactID : string = c.fullName + "_" + (current_time);
      
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessContactList/${contactID}`).ref;
      if(docRef)
      {
        docRef.set({
          id:contactID,
          server_time:firebase.firestore.FieldValue.serverTimestamp(),
          creation_time:current_time,  
          userID:c.userID,
          organisationID:c.organisationID,  
          confidentialityLevel:c.confidentialityLevel,
          fullName: c.fullName,
          category: c.category,
          phone_1: c.phone_1,
          email_1: c.email_1,
          ROIForEarlyPayment: c.ROIForEarlyPayment, 
          ROIForLatePayment: c.ROIForLatePayment,
          addressLine1: c.addressLine1,
          addressLine2: c.addressLine2,
          city: c.city,
          pincode: c.pincode,
          stateRegion: c.stateRegion,
          GSTIN: c.GSTIN,
          PAN: c.PAN
        }).then(() => {
          resolve(contactID);
        }).catch(e => {
          console.log("Exception from docRef.set() " + e);
          reject(null);
        });     
      }
      else
      {
        reject(null);
      }
    });
  }

  getAllBusinessContact(user:User):AngularFirestoreCollection<BusinessContact>
  {
    console.log("Welcome to EntityProvider:getBusinessContactList()");
    if(!checkIfValid(user))
      {
        console.log("Invalid user");
        return(null);
      }
    return this.afs.collection<BusinessContact>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessContactList`, ref => ref.orderBy('fullName')); 
  }

  getAllBusinessContactList(user:User):Promise<BusinessContact[]>
  {
    console.log("Welcome to EntityProvider:getBusinessContactList() ");
    
    return new Promise((resolve,reject)=>{
      let contactList:Array<BusinessContact> = [];
    
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve([]);
      }
      try
      {
        this.afs.collection<BusinessContact>(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessContactList`).ref
        .orderBy('fullName')
        .onSnapshot(querySnapshot => {
       //   console.log("querySnapshot.size: " + querySnapshot.size);
          contactList = [];
          for(let i=0;i<querySnapshot.docs.length;i++)
          {
            let contact = {} as BusinessContact;
            contact.id = querySnapshot.docs[i].data().id;
            contact.userID = querySnapshot.docs[i].data().userID;
            contact.organisationID = querySnapshot.docs[i].data().organisationID;
            contact.confidentialityLevel = querySnapshot.docs[i].data().confidentialityLevel;
            contact.fullName = querySnapshot.docs[i].data().fullName;
            contact.category = querySnapshot.docs[i].data().category;
            contact.phone_1 = querySnapshot.docs[i].data().phone_1;
            contact.email_1 = querySnapshot.docs[i].data().email_1;
            contact.ROIForEarlyPayment = querySnapshot.docs[i].data().ROIForEarlyPayment;
            contact.ROIForLatePayment = querySnapshot.docs[i].data().ROIForLatePayment;
            contact.addressLine1 = querySnapshot.docs[i].data().addressLine1;
            contact.addressLine2 = querySnapshot.docs[i].data().addressLine2;
            contact.city = querySnapshot.docs[i].data().city;
            contact.pincode = querySnapshot.docs[i].data().pincode;
            contact.stateRegion = querySnapshot.docs[i].data().stateRegion;
            contact.GSTIN = querySnapshot.docs[i].data().GSTIN;
            contact.PAN = querySnapshot.docs[i].data().PAN;
            
            contactList.push(contact);
          }
          console.log("contactList.length " + contactList.length);
          resolve(contactList);
        });
      }
      catch(e)
      {
        console.log("Exception in getBusinessContactList " + e);
        reject(contactList);
      }
    });
  }

  getBusinessContact(user:User,contactID:string):Promise<BusinessContact>
  {
    console.log("Welcome to EntityProvider:getBusinessContact(contactID) " + contactID);
    return new Promise((resolve,reject) => {
      if(!(checkIfValid(user) && (contactID)))
      {
        console.log("Invalid user or input");
        resolve(null);
      }
      let bContact = {} as BusinessContact;            
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessContactList/${contactID}`).ref;
      return docRef.get().then(doc => {
          if (!doc.exists) 
          {
              console.log('No such document!' + doc.id);
              resolve(null);
          } 
          else 
          {
            bContact.id = doc.id;
            bContact.userID = doc.data().userID;
            bContact.organisationID = doc.data().organisationID;
            bContact.confidentialityLevel = doc.data().confidentialityLevel;
            bContact.fullName = doc.data().fullName;
            bContact.category = doc.data().category;
            bContact.phone_1 = doc.data().phone_1;
            bContact.email_1 = doc.data().email_1;
            bContact.ROIForEarlyPayment = doc.data().ROIForEarlyPayment;
            bContact.ROIForLatePayment = doc.data().ROIForLatePayment;
            bContact.addressLine1 = doc.data().addressLine1;
            bContact.addressLine2 = doc.data().addressLine2;
            bContact.city = doc.data().city;
            bContact.pincode = doc.data().pincode;
            bContact.stateRegion = doc.data().stateRegion;
            bContact.GSTIN = doc.data().GSTIN;
            bContact.PAN = doc.data().PAN;
            resolve(bContact);
          }
      })
      .catch(err => {
          console.log('Error getting BusinessContact document', err);
          resolve(null);
      });
    });
  }

  updateBusinessContact(user:User,c:BusinessContact):Promise<boolean>
  {
    console.log("Welcome to EntityProvider:updateBusinessContact(BusinessContact) " + c.id);
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      const current_time:string = getDateTimeString(new Date());
      
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessContactList/${c.id}`).ref;
     // let docRef = this.afs.doc(`/customerList/${user.customerID}/businessContactList/${c.ID}`).ref;
      if(docRef)
      {
        docRef.update({
          server_time:firebase.firestore.FieldValue.serverTimestamp(),
          creation_time:current_time,
          userID:c.userID,
          organisationID:c.organisationID,
          confidentialityLevel:c.confidentialityLevel,    
          fullName: c.fullName,
          category: c.category,
          phone_1: c.phone_1,
          email_1: c.email_1,
          ROIForEarlyPayment: c.ROIForEarlyPayment, 
          ROIForLatePayment: c.ROIForLatePayment,
          addressLine1: c.addressLine1,
          addressLine2: c.addressLine2,
          city: c.city,
          pincode: c.pincode,
          stateRegion: c.stateRegion,
          GSTIN: c.GSTIN,
          PAN: c.PAN
        }).then(() => {
          resolve(true);
        }).catch(e => {
          console.log("Exception from docRef.update() " + e);
          reject(false);
        });    
      }
      else
      {
        reject(false);
      }
    });    
  }  

  deleteBusinessContact(user:User,c:BusinessContact):Promise<boolean>
  {
    console.log("Welcome to EntityProvider:deleteBusinessContact(contactID) " + c.id);
    return new Promise((resolve,reject)=>{
      if(!checkIfValid(user))
      {
        console.log("Invalid user");
        resolve(false);
      }
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}/dataVersionList/${user.dataVersionID}/_businessContactList/${c.id}`).ref;
      //let docRef = this.afs.doc(`/customerList/${user.customerID}/businessContactList/${c.ID}`).ref;
      if(docRef)
      {
        docRef.delete()
        .then(() => {
          resolve(true);
        }).catch(e => {
          console.log("Error from docRef.delete() " + e);
          reject(false);
        });
      }      
    });
    
  }

  getOrganisationDetails(user:User):Promise<Organisation>
  {
    console.log("Welcome to EntityProvider:getOrganisationDetails() ");
    return new Promise((resolve,reject) => {
      if(!checkIfValid(user))
      {
        console.log("Invalid user or input");
        resolve(null);
      }
      let organisation = {} as Organisation;            
      let docRef:DocumentReference = this.afs.doc(`/customerList/${user.customerID}`).ref;
      return docRef.get().then(doc => {
          if (!doc.exists) 
          {
              console.log('No such document!' + doc.id);
              resolve(null);
          } 
          else 
          {
            organisation.id = doc.id;
            organisation.name = doc.data().name;
            organisation.email = doc.data().email;
            organisation.phone_1 = doc.data().phone_1;
            organisation.phone_2 = doc.data().phone_2;
            organisation.address_1 = doc.data().address_1;
            organisation.address_2 = doc.data().address_2;
            organisation.city = doc.data().city;
            organisation.stateCode = doc.data().stateCode;
            organisation.pinCode = doc.data().pinCode;
            organisation.PAN = doc.data().PAN;
            organisation.GSTIN = doc.data().GSTIN;
            organisation.bankAccountID = doc.data().bankAccountID;
            organisation.bankAccountName = doc.data().bankAccountName;
            organisation.bankName = doc.data().bankName;
            organisation.bankBranch = doc.data().bankBranch;
            organisation.bankIFSC = doc.data().bankIFSC;            
            resolve(organisation);
          }
      })
      .catch(err => {
          console.log('Error getting Organisation document', err);
          resolve(null);
      });
    });
  }

}
