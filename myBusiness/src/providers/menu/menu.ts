import { Injectable } from '@angular/core';

@Injectable()
export class MenuProvider {

  constructor() { }

  getSideMenus() {
    return [
      {
        title: 'Home',
        pageName: 'HomePage',
        icon: 'home'
      },
      {
        title: 'Party List',
        icon: 'contacts',
        subPages: 
        [
          {
            title: 'Buyer',
            pageName: 'PartyListPage',
            icon: 'contact'
          },
          {
            title: 'Supplier',
            pageName: 'PartyListPage',
            icon: 'contact'
          },
          {
            title: 'Broker',
            pageName: 'PartyListPage',
            icon: 'contact'
          }
        ]
      },
      {
        title: 'Party Summary',
        icon:'card',
        subPages: 
        [
          {
            title: 'Buyer',
            pageName: 'PartySummaryListPage',
            icon: 'contact'
          },
          {
            title: 'Supplier',
            pageName: 'PartySummaryListPage',
            icon: 'contact'
          },
          {
            title: 'Broker',
            pageName: 'PartySummaryListPage',
            icon: 'contact'
          }
        ]
      },
      {
        title: 'My Inventory',
        icon: 'shuffle',
        subPages: 
        [
          {
            title: 'Overview',
            pageName: 'MyInventoryPage',
            icon: 'shuffle'
          },
          {
            title: 'Planning',
            pageName: 'TaskSummaryPage',
            icon: 'shuffle'
          },
          {
            title: 'In Mfg',
            pageName: 'TaskSummaryPage',
            icon: 'shuffle'
          },
          {
            title: 'Mfg Return',
            pageName: 'TaskSummaryPage',
            icon: 'shuffle'
          },
          {
            title: 'Assortment',
            pageName: 'TaskSummaryPage',
            icon: 'shuffle'
          },
          {
            title: 'Sales',
            pageName: 'TaskSummaryPage',
            icon: 'shuffle'
          }
        ]
      },
      {
        title: 'My Polish Stock',
        pageName: 'SalesStockSummaryPage',
        icon: 'shuffle'
      },
      {
        title: 'My PriceList',
        pageName: 'MyPricePage',
        icon: 'shuffle'
      },
      {
        title: 'Transactions',
        icon: 'repeat',
        subPages: 
        [
          {
            title: 'Purchase',
            pageName: 'PartyTransactionsPage',
            icon: 'trending-up'
          },
          {
            title: 'Sales',
            pageName: 'PartyTransactionsPage',
            icon: 'trending-up'
          },
          {
            title: 'Payment',
            pageName: 'PartyTransactionsPage',
            icon: 'cash'
          }
        ]
      },
    ];
  }
}