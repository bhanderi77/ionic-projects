import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database'
import { FormControl } from '@angular/forms';
import { CallNumber } from '@ionic-native/call-number';
/**
 * Generated class for the SamajPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@IonicPage()
@Component({
  selector: 'page-samaj',
  templateUrl: 'samaj.html',
})
export class SamajPage {
  language: string;
  city: string;
  samaj = [];
  searching: any = false;
  all_samaj = [];
  cities = [];
  all_cities = [];
  SearchInput: string = '';
  SearchControl: FormControl;

  constructor(public navCtrl: NavController, public navParams: NavParams, private databaseProvider: DatabaseProvider, private call: CallNumber) {
    this.city = navParams.get('SelectedCity');
    this.SearchControl = new FormControl();
    this.language = "english";
    this.databaseProvider.getDatabaseState().subscribe(rdy => {
      if (rdy) {
        this.LoadSamajData();
        this.LoadCityData();
      }
    });
  }

  languageSelected(lang: string) {
    this.language = String(lang);
    this.LoadSamajData();
    this.LoadCityData();
  }


  LoadCityData() {
    this.databaseProvider.getAllCities(this.language).then(data => {
      this.all_cities = data;
      this.cities = this.all_cities;
    });
  }

  LoadSamajData() {
    this.databaseProvider.getAllSamaj(this.language).then(data => {
      this.all_samaj = data;
      this.samaj = this.all_samaj;
    });
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad SamajPage');

    this.setFilteredSamaj();
    this.SearchControl.valueChanges.subscribe(search => {
      this.searching = false;
      this.setFilteredSamaj();
    });
  }

  setFilteredSamaj() {
    // console.log('Inside setFilteredSamaj value of searching :' + this.searching );

    this.filterSamaj(this.SearchInput);
    //this.searching = false;
    // this.cities = this.filterItems(this.SearchInput);
  }

  filterSamaj(SearchInput) {
    // console.log('Inside filterItems');
    this.samaj = this.all_samaj.filter((item) => {
      if (SearchInput.length == 1) {
        if (item.city.toLowerCase().charAt() == SearchInput.toLowerCase())
          return true;
      }
      else if (item.city.toLowerCase().indexOf(SearchInput.toLowerCase()) > -1) {
        return true;
      }
      //return true;
    });

    // console.log('inside filterItems --> For Cities ');

  }

  onSearch(searchbar) {
    this.searching = true;
    this.cities = this.all_cities;
    // console.log('inside onSearch this.SearchInput : ' + this.SearchInput);

    this.cities = this.cities.filter((item) => {
      if (this.SearchInput.length == 1) {
        if (item.toLowerCase().charAt() == this.SearchInput.toLowerCase())
          return true;
      }
      else if (item.toLowerCase().indexOf(this.SearchInput.toLowerCase()) > -1) {
        return true;
      }
      else
        return false;
    });
    // console.log('this.cities : ' + this.cities);
    // console.log('onSearch completed');

    //this.setFilteredSamaj();
  }

  InputSelected(event, city) {
    this.SearchInput = city;
    // console.log('Inside InputSelected : ' + city);
    this.cities = this.cities.filter((item) => {
      // if(this.SearchInput.length == 1){
      //    if(item.toLowerCase().charAt()==this.SearchInput.toLowerCase())
      //      return true;
      //  }
      //  else if(item.toLowerCase().indexOf(this.SearchInput.toLowerCase()) > -1){
      //    return true;
      //  }
      //  else
      //return false;
      return;
    });
    //this.searching = false;
    // console.log('InputSelected completed : ' + this.cities);
    // this.searching = false;
    // this.setFilteredSamaj(); 
  }

  async callNumber(number: any): Promise<any> {
    try {
      await this.call.callNumber(String(number), true);
    }
    catch (e) {
      console.error(e);
    }
  }

}
