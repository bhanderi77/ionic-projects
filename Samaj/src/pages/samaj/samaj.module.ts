import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SamajPage } from './samaj';

@NgModule({
  declarations: [
    SamajPage,
  ],
  imports: [
    IonicPageModule.forChild(SamajPage),
  ],
})
export class SamajPageModule {}
