import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database'
import { SamajPage } from '../samaj/samaj' ;
/**
 * Generated class for the CityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-city',
  templateUrl: 'city.html',
})
export class CityPage {

	state : string;
	cities = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, private databaseProvider : DatabaseProvider) {
  	  	this.state = navParams.get('SelectedState');
  	  	this.LoadCityData();
  }

  LoadCityData(){
  	this.databaseProvider.getAllCities(this.state).then(data => {
  		this.cities = data;
  	});
  }

  CitySelected(city:string){
    this.navCtrl.push(SamajPage,{SelectedCity:city});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CityPage');
  }

}
