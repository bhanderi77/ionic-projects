var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database';
import { SamajPage } from '../samaj/samaj';
/**
 * Generated class for the CityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CityPage = /** @class */ (function () {
    function CityPage(navCtrl, navParams, databaseProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.databaseProvider = databaseProvider;
        this.cities = [];
        this.state = navParams.get('SelectedState');
        this.LoadCityData();
    }
    CityPage.prototype.LoadCityData = function () {
        var _this = this;
        this.databaseProvider.getAllCities(this.state).then(function (data) {
            _this.cities = data;
        });
    };
    CityPage.prototype.CitySelected = function (city) {
        this.navCtrl.push(SamajPage, { SelectedCity: city });
    };
    CityPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CityPage');
    };
    CityPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-city',
            templateUrl: 'city.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, DatabaseProvider])
    ], CityPage);
    return CityPage;
}());
export { CityPage };
//# sourceMappingURL=city.js.map