import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database'
import { CityPage } from '../city/city' ;
import { FormControl } from '@angular/forms';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	states = [];
  all_states = [];
  SearchControl : FormControl;
  SearchState : string = '';
  searching : any = false;
  constructor(public navCtrl: NavController , private databaseProvider : DatabaseProvider) {
    this.SearchControl = new FormControl();
    this.databaseProvider.getDatabaseState().subscribe(rdy =>{
  	  if(rdy){
  		  this.loadStatesData();
  	  }
    });
  }
  loadStatesData(){
  	this.databaseProvider.getAllStates().then(data => {
  		this.all_states = data;
      this.states = this.all_states;
  	});
  }
  ionViewDidLoad() {
      this.setFilteredStates();
      this.SearchControl.valueChanges.debounceTime(700).subscribe(search => {
          this.searching = false;
          this.setFilteredStates(); 
      });
  }
  
  setFilteredStates() {
    this.states = this.filterItems(this.SearchState);
  }
  
  filterItems(SearchState){
    return this.all_states.filter((item) => {
      console.log('inside filterItems ');
      console.log('Length of SearchState : '+SearchState.length);
      console.log('SearchState toLowerCase : '+SearchState.toLowerCase());
      console.log('Item toLowerCase : ' + item.toLowerCase().charAt());
      if(SearchState.length == 1){
        if(item.toLowerCase().charAt()==SearchState.toLowerCase())
          return item;
      }
      else{
        return item.toLowerCase().indexOf(SearchState.toLowerCase()) > -1;
      }
    });
  }
    
  StateSelected(state:string){
    this.navCtrl.push(CityPage,{SelectedState:state});
  }
  onSearchInput(){
    this.searching = true;
  }

}
