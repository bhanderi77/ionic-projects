var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DatabaseProvider } from './../../providers/database/database';
import { CityPage } from '../city/city';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, databaseProvider) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.databaseProvider = databaseProvider;
        this.states = [];
        this.all_states = [];
        this.SearchState = '';
        this.databaseProvider.getDatabaseState().subscribe(function (rdy) {
            if (rdy) {
                _this.loadStatesData();
            }
        });
    }
    HomePage.prototype.loadStatesData = function () {
        var _this = this;
        this.databaseProvider.getAllStates().then(function (data) {
            _this.all_states = data;
            _this.states = _this.all_states;
        });
    };
    HomePage.prototype.ionViewDidLoad = function () {
        this.setFilteredStates();
    };
    HomePage.prototype.setFilteredStates = function () {
        this.states = this.filterItems(this.SearchState);
    };
    HomePage.prototype.filterItems = function (SearchState) {
        return this.all_states.filter(function (item) {
            item.toLowerCase().
            ;
            return item.toLowerCase().indexOf(SearchState.toLowerCase()) > -1;
        });
    };
    HomePage.prototype.StateSelected = function (state) {
        this.navCtrl.push(CityPage, { SelectedState: state });
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [NavController, DatabaseProvider])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map