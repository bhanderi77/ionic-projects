import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { SQLite , SQLiteObject } from '@ionic-native/sqlite';
import { BehaviorSubject } from 'rxjs/Rx' ;
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {
	database: SQLiteObject;
	private databaseReady: BehaviorSubject<boolean>;
  	constructor(public http: Http , private sqlitePorter:SQLitePorter , private storage : Storage , private sqlite : SQLite , private platform : Platform) {
    	this.databaseReady = new BehaviorSubject(false);
    	this.platform.ready().then(() => {
    		// console.log('database provider constructor : creating db');
    		this.sqlite.create({
    			name: 'samaj.db',
    			location: 'default'
    		})
    		.then((db: SQLiteObject) => {
    			this.database = db;
    			this.storage.set("database_filled",false);
    			this.storage.get('database_filled').then(val =>{
    				if (val){
    					this.databaseReady.next(true);
    				}else{
    					this.fillDatabase();
    				}
    			})
    		})
    	});
  	}



getAllStates(){
  	// sole.log('Inside getAllStates ');
  	return this.database.executeSql("SELECT DISTINCT state FROM SamajDetails ORDER BY state",[]).then(data => {
  	let states = [];
	// console.log('inside getAllStates : data fetched');
  	if (data.rows.length > 0 ){
  		for ( var i = 0 ; i < data.rows.length; i++){
  			states.push(data.rows.item(i).state);
  			// console.log('get all states : ' + i);
  		}
  	}
  	return states;
  	});
}

getAllCities(language:string){
  // console.log('Inside getAllCities : ' + state);
    //return this.database.executeSql("SELECT DISTINCT city FROM SamajDetails WHERE state = ? ORDER BY city",state_data).then(data => {
    if(language === 'english'){

      return this.database.executeSql("SELECT DISTINCT city FROM SamajDetails_en",[]).then(data => {
      let cities = [];
    // console.log('inside getAllCities : data fetched');
      if (data.rows.length > 0 ){
        for ( var i = 0 ; i < data.rows.length; i++){
          cities.push(data.rows.item(i).city);
          // console.log('get all cities : ' + i);
        }
      }
      // console.log('Inside getAllCities : ' + cities);
      return cities;
      });
    }
    else{
      return this.database.executeSql("SELECT DISTINCT city FROM SamajDetails_gj",[]).then(data => {
      let cities = [];
    // console.log('inside getAllCities : data fetched');
      if (data.rows.length > 0 ){
        for ( var i = 0 ; i < data.rows.length; i++){
          cities.push(data.rows.item(i).city);
          // console.log('get all cities : ' + i);
        }
      }
      // console.log('Inside getAllCities : ' + cities);
      return cities;
      });
    }

}

getAllSamaj(language:string){
 // let city_data = [city];
  // console.log('Inside getAllSamaj : ' + city);
    //return this.database.executeSql("SELECT * FROM SamajDetails WHERE city = ?",city_data).then(data => {
      if(language === 'english'){

        return this.database.executeSql("SELECT * FROM SamajDetails_en ORDER BY city",[]).then(data => {
        let samaj = [];
      // console.log('inside getAllSamaj : data fetched');
        if (data.rows.length > 0 ){
          for ( var i = 0 ; i < data.rows.length; i++){
            samaj.push({name:data.rows.item(i).name , address:data.rows.item(i).address ,city:data.rows.item(i).city ,state:data.rows.item(i).state, pincode:data.rows.item(i).pincode ,phone1:data.rows.item(i).phone1 ,phone2:data.rows.item(i).phone2 ,email:data.rows.item(i).email ,website:data.rows.item(i).website});
            // console.log('get all samaj : ' + i);
          }
        }
        // console.log('Inside getAllSamaj : ' + samaj);
        return samaj;
        });
      }
      else{
         return this.database.executeSql("SELECT * FROM SamajDetails_gj ORDER BY city",[]).then(data => {
        let samaj = [];
      // console.log('inside getAllSamaj : data fetched');
        if (data.rows.length > 0 ){
          for ( var i = 0 ; i < data.rows.length; i++){
            samaj.push({name:data.rows.item(i).name , address:data.rows.item(i).address ,city:data.rows.item(i).city ,state:data.rows.item(i).state, pincode:data.rows.item(i).pincode ,phone1:data.rows.item(i).phone1 ,phone2:data.rows.item(i).phone2 ,email:data.rows.item(i).email ,website:data.rows.item(i).website});
            // console.log('get all samaj : ' + i);
          }
        }
        // console.log('Inside getAllSamaj : ' + samaj);
        return samaj;
        });


      }
}



fillDatabase(){
		// console.log('Inside FillDatabase');
		this.http.get('assets/dummyDB.sql')
		.map(res => res.text())
		.subscribe(sql => {
			// console.log('Inside fillDatabase : importing sql to db');
			this.sqlitePorter.importSqlToDb(this.database,sql)
			.then(data => {
				// console.log('Inside fillDatabase : data imported to db');
				this.databaseReady.next(true);
				this.storage.set('database_filled',true);
			})
		});
}

  getDatabaseState(){
  	return this.databaseReady.asObservable();
  }

}
