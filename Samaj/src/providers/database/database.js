var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { SQLite } from '@ionic-native/sqlite';
import { BehaviorSubject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var DatabaseProvider = /** @class */ (function () {
    function DatabaseProvider(http, sqlitePorter, storage, sqlite, platform) {
        var _this = this;
        this.http = http;
        this.sqlitePorter = sqlitePorter;
        this.storage = storage;
        this.sqlite = sqlite;
        this.platform = platform;
        this.databaseReady = new BehaviorSubject(false);
        this.platform.ready().then(function () {
            console.log('database provider constructor : creating db');
            _this.sqlite.create({
                name: 'samaj.db',
                location: 'default'
            })
                .then(function (db) {
                _this.database = db;
                _this.storage.set("database_filled", false);
                _this.storage.get('database_filled').then(function (val) {
                    if (val) {
                        _this.databaseReady.next(true);
                    }
                    else {
                        _this.fillDatabase();
                    }
                });
            });
        });
    }
    DatabaseProvider.prototype.getAllStates = function () {
        console.log('Inside getAllStates ');
        return this.database.executeSql("SELECT DISTINCT state FROM SamajDetails ORDER BY state", []).then(function (data) {
            var states = [];
            console.log('inside getAllStates : data fetched');
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    states.push(data.rows.item(i).state);
                    console.log('get all states : ' + i);
                }
            }
            return states;
        });
    };
    DatabaseProvider.prototype.getAllCities = function (state) {
        var state_data = [state];
        console.log('Inside getAllCities : ' + state);
        return this.database.executeSql("SELECT DISTINCT city FROM SamajDetails WHERE state = ? ORDER BY city", state_data).then(function (data) {
            var cities = [];
            console.log('inside getAllCities : data fetched');
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    cities.push(data.rows.item(i).city);
                    console.log('get all cities : ' + i);
                }
            }
            console.log('Inside getAllCities : ' + cities);
            return cities;
        });
    };
    DatabaseProvider.prototype.getAllSamaj = function (city) {
        var city_data = [city];
        console.log('Inside getAllSamaj : ' + city);
        return this.database.executeSql("SELECT * FROM SamajDetails WHERE city = ?", city_data).then(function (data) {
            var samaj = [];
            console.log('inside getAllSamaj : data fetched');
            if (data.rows.length > 0) {
                for (var i = 0; i < data.rows.length; i++) {
                    samaj.push({ name: data.rows.item(i).name, address: data.rows.item(i).address, city: data.rows.item(i).city, state: data.rows.item(i).state, pincode: data.rows.item(i).pincode, phone1: data.rows.item(i).phone1, phone2: data.rows.item(i).phone2, email: data.rows.item(i).email, website: data.rows.item(i).website });
                    console.log('get all samaj : ' + i);
                }
            }
            console.log('Inside getAllSamaj : ' + samaj);
            return samaj;
        });
    };
    DatabaseProvider.prototype.fillDatabase = function () {
        var _this = this;
        console.log('Inside FillDatabase');
        this.http.get('assets/dummyDB.sql')
            .map(function (res) { return res.text(); })
            .subscribe(function (sql) {
            console.log('Inside fillDatabase : importing sql to db');
            _this.sqlitePorter.importSqlToDb(_this.database, sql)
                .then(function (data) {
                console.log('Inside fillDatabase : data imported to db');
                _this.databaseReady.next(true);
                _this.storage.set('database_filled', true);
            });
        });
    };
    DatabaseProvider.prototype.getDatabaseState = function () {
        return this.databaseReady.asObservable();
    };
    DatabaseProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [Http, SQLitePorter, Storage, SQLite, Platform])
    ], DatabaseProvider);
    return DatabaseProvider;
}());
export { DatabaseProvider };
//# sourceMappingURL=database.js.map