import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AlertController } from 'ionic-angular';
import { SamajPage } from '../pages/samaj/samaj';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = SamajPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen , public alertCtrl: AlertController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  openInfo(){
      let alert = this.alertCtrl.create({
        title: '<b>Dev. Team</b> <br>',
        subTitle: 'Nilesh bhanderi <br> Hardik bhanderi <br> Rashmin bhanderi <br> <br> Email : samaj@gmail.com ',
        buttons: ['OK']
      });
      alert.present();
  }

  RateUs(){

  }  

  Share(){

  }

}

