DROP TABLE IF EXISTS SamajDetails;
DROP TABLE IF EXISTS SamajDetails_en;
DROP TABLE IF EXISTS SamajDetails_gj;
CREATE TABLE IF NOT EXISTS SamajDetails_en (id INTEGER PRIMARY KEY AUTOINCREMENT , name TEXT , address TEXT , city TEXT , state TEXT , pincode TEXT , phone1 TEXT , phone2 TEXT , email VARCHAR(35) , website VARCHAR(35) );
CREATE TABLE IF NOT EXISTS SamajDetails_gj (id INTEGER PRIMARY KEY AUTOINCREMENT , name TEXT , address TEXT , city TEXT , state TEXT , pincode TEXT , phone1 TEXT , phone2 TEXT , email VARCHAR(35) , website VARCHAR(35) );
INSERT INTO SamajDetails_en(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('SHRI GUJARATI SAMAJ','D.NO.47-15-8, DWARAKANAGAR','VISHAKHAPATNAM','ANDHRA PRADESH','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_en(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('Pune Pravasi Sangh','D.NO.47-15-8, DWARAKANAGAR','MUMBAI','MAHARASHTRA','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_en(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('SHREE MUMBAI BRAHMBHATT SAMAJ','D.NO.47-15-8, DWARAKANAGAR','KOLKATA','WEST BENGAL','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_en(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('PATEL SAMAJ','D.NO.47-15-8, DWARAKANAGAR','MUMBAI','MAHARASHTRA','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_en(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('KACCHHI SAMAJ','D.NO.47-15-8, DWARAKANAGAR','KOLKATA','WEST BENGAL','530016','9849498750','9441920461','choksi@jaimenee.in','');

INSERT INTO SamajDetails_gj(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('શ્રી ગુજરાતી સમાજ','દ.નો.47-15-8, દ્વર્કનગર','વિશાખાપત્નમ','આનધ્રા પ્રદેશ','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_gj(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('પુને પ્ર​વાસી સંઘ','દ.નો.47-15-8, દ્વર્કનગર','મુમ્બૈ','મહારાશ્ત્ર','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_gj(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('શ્રી મુમ્બૈ બ્રહ્મ્ભત્ત સમાજ ','દ.નો.47-15-8, દ્વર્કનગર','કોલ્કાતા','વેસ્ત બેંગાલ ','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_gj(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('પતેલ સમાજ ','દ.નો.47-15-8, દ્વર્કનગર','મુમ્બૈ','મહારાશ્ત્ર','530016','9849498750','9441920461','choksi@jaimenee.in','');
INSERT INTO SamajDetails_gj(name,address,city,state,pincode,phone1,phone2,email,website) VALUES('કછ્છી સમાજ ','દ.નો.47-15-8, દ્વર્કનગર','કોલ્કાતા','વેસ્ત બેંગાલ ','530016','9849498750','9441920461','choksi@jaimenee.in','');
