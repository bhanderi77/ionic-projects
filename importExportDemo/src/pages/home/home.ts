import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as XLSX from 'xlsx';
import { File, FileEntry, IFile, Entry } from '@ionic-native/file';
import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from '@ionic-native/file-path';

type AOA = any[][];
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // data: any[][] = [["Name","city","state","Phone number"],["hardik","mumbai","maharashtra","9820216862"],["rashmin","pune","maharashtra","9514236588"]];
  data :any[][];
  constructor(public filePath: FilePath , public navCtrl: NavController , public file : File , private fileChooser: FileChooser) {
    
  }

  read(bstr: string ){
    /* read workbook */
    const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

    /* grab first sheet */
    const wsname: string = wb.SheetNames[0];
    const ws: XLSX.WorkSheet = wb.Sheets[wsname];

    /* save data */
    this.data = <AOA>(XLSX.utils.sheet_to_json(ws, {header: 1}));
    console.log(this.data);
  }

  write(): XLSX.WorkBook {
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.data);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'SheetJS');

    return wb;
  };

  /* File Input element for browser */
  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      this.read(bstr);
    };
    reader.readAsBinaryString(target.files[0]);
  };

  /* Import button for mobile */
    import() {
    try {
      // let bstr:string;
      this.fileChooser.open()
      .then(uri => {
        console.log("uri : "+uri); 
        this.filePath.resolveNativePath(uri)
        .then( file =>{
          let filePath: string = file;
          console.log("file path : ",JSON.stringify(filePath));
          this.file.resolveLocalFilesystemUrl(filePath)
          .then((entry:Entry)=>{
            entry.getParent((directoryEntry : Entry)=>{
              this.file.readAsBinaryString(directoryEntry.nativeURL,entry.name)
              .then(bstr=>{
                console.log(entry.name);
                this.read(bstr);
              })
            })
          })
        })
      })
      .catch(e => console.log(e));
      }  
      catch(e) {
        const m: string = e.message;
        alert(m.match(/It was determined/) ? "Use File Input control" : `Error: ${m}`);
    }
  };

  /* Export button */
  async export() {
    const wb: XLSX.WorkBook = this.write();
    const filename: string = "SheetJSIonic.xlsx";
    try {
      /* generate Blob */
      const wbout: ArrayBuffer = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
      const blob: Blob = new Blob([wbout], {type: 'application/octet-stream'});

      /* find appropriate path for mobile */
      const target: string = this.file.documentsDirectory || this.file.externalDataDirectory || this.file.dataDirectory || '';
      const dentry = await this.file.resolveDirectoryUrl(target);
      const url: string = dentry.nativeURL || '';

      /* attempt to save blob to file */
      await this.file.writeFile(url, filename, blob, {replace: true});
      alert(`Wrote to SheetJSIonic.xlsx in ${url}`);
    } catch(e) {
      if(e.message.match(/It was determined/)) {
        /* in the browser, use writeFile */
        XLSX.writeFile(wb, filename);
      }
      else alert(`Error: ${e.message}`);
    }
  };

}
