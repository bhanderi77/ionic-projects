import { Component, ViewChild } from '@angular/core';
import { Platform , Nav, ModalController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { File } from '@ionic-native/file'
import { FingerprintAIO } from "@ionic-native/fingerprint-aio";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
//import { PartySummaryListPage } from '../pages/partySummaryList/partySummaryList';
//import { HomePage } from '../pages/home/home';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav:Nav;

  passCode:string;
  splash = true;
  rootPage:any = 'HomePage';
  pages: Array<{title: string, component: any}>;

  constructor(platform: Platform, statusBar: StatusBar,splashScreen: SplashScreen,
    modalCtrl: ModalController, private file: File, private faio: FingerprintAIO,
    private openNativeSettings: OpenNativeSettings) 
  { // 
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
  //    statusBar.styleDefault();
  //    splashScreen.hide();
      console.log("platform is ready");
      /*
      if(platform.is('android') || platform.is('ios') || platform.is('cordova'))
      {
        console.log("Platform : " + platform.platforms.name);
        this.faio.isAvailable()
        .then((res) => {
          console.log('Response: ' + res);
        },
        (err) => {
          alert('Please setup the fingerprint  first ');
          if(platform.is('ios'))
            this.openNativeSettings.open('touch');
          else if(platform.is('android'))
            this.openNativeSettings.open('security');
        });
      } */ 
      statusBar.styleDefault();
      splashScreen.hide();
      platform.resume.subscribe(() => {
        console.log("inside platform.resume.subscribe() ");
        let modal = modalCtrl.create('LockScreenPage'); //,{passCode: this.passCode}
        modal.present();
      });
    });
    
    /*
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Settings', component: AppSettingsPage}
    ];*/

  }

  
  /*openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }*/

}

