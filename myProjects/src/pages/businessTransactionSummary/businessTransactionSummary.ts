import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController, PopoverController } from 'ionic-angular';
//import { financeTransaction, partySummary, businessTrxSummary, getPartySummary, Global, partyProfile, AppSettings, businessTransaction } from '../../model/interface';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { EntityProvider } from '../../providers/entity/entity';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';

import { BusinessTransactionSummary,getBusinessTransactionSummary } from '../../model/partySummary';

import { BusinessContact } from '../../model/businessContact';
import { BusinessTransaction } from '../../model/businessTransaction';
import { FinanceTransaction } from '../../model/financeTransaction';
import { User,checkIfValid } from '../../model/user';
import { myConstants } from '../../model/myConstants';
//import { PartySummary,BusinessTransactionSummary,calculatePartySummary } from '../../model/partySummary';



/**
 * Generated class for the FinanceTransactionSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'businessTransactionSummary',
  templateUrl: 'businessTransactionSummary.html',
})
export class BusinessTransactionSummaryPage {

  //trxSummary: businessTrxSummary;
  private partyProfile:BusinessContact;
  private trxCurrency: string;
  private user:User;
  private bizTrxSummary: BusinessTransactionSummary;
  private bizTrxID:string;
  private finTrxList:Array<FinanceTransaction>;
  private bizTransaction:BusinessTransaction;
  private exchangeRate:number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl:AlertController,
    public viewCtrl: ViewController, 
    private entityProvider:EntityProvider,
    private transactionProvider: TransactionProvider,
    private settingsProvider:SettingsProvider,
    public popOverCtrl: PopoverController) {

    //this.bizTrxSummary.businessTransaction.trxType
    this.partyProfile = {} as BusinessContact;
    //this.bizTrxSummary = navParams.get('bizTrxSummary');
    this.bizTrxID = "";
    this.user = {} as User;
    this.user = navParams.get('User');
    this.trxCurrency = myConstants.localCurrency;
    this.bizTrxSummary = {} as BusinessTransactionSummary;
    this.bizTrxSummary.businessTransaction = {} as BusinessTransaction;
    this.bizTrxSummary.finTrxs = [];
    this.bizTransaction = {} as BusinessTransaction;
    this.finTrxList = [];
    this.exchangeRate = 1;
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad BusinessTransactionSummaryPage');
    //getPartySummary(this.databaseProvider,this.pSummary);
    console.log('bizTrxSummary int real in local' + this.bizTrxSummary.interestRealizedinLocal);
    //this.bizTrxSummary = this.pSummary.bizTrxSummary.filter(bizSummary => bizSummary.businessTransaction.ID === this.bizTrx.ID);
    console.log('diff days filter : ' + this.bizTrxSummary.diffDays);

    if(!checkIfValid(this.user))
    {
      alert("Invalid userID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      this.viewCtrl.dismiss();
    }
  }

  ionViewWillEnter()
  {
    console.log('ionViewWillEnter BusinessTransactionSummaryPage');
    this.initializeData();
  }

  ionViewDidEnter(){
    console.log('ionViewDidEnter BusinessTransactionSummaryPage');
    console.log('bizTrxSummary diff days' + this.bizTrxSummary.diffDays);
  }

  async initializeData()
  {
    console.log("Welcome to BusinessTransactionSummaryPage::initializeData()");
    //this.partyProfile = this.navParams.get('partyProfile');
    //this.bizTrxSummary = navParams.get('bizTrxSummary');
    this.bizTrxID = this.navParams.get('bizTrxID');
    
    this.partyProfile = null;  
    //this.bizTransaction = await this.transactionProvider.getBusinessTransaction(this.user,this.bizTrxID);
    this.transactionProvider.getBusinessTransaction(this.user,this.bizTrxID)
    .then(businessTransaction => {
      this.bizTransaction = businessTransaction;
      this.entityProvider.getBusinessContact(this.user,this.bizTransaction.trxPartyID)
      .then(contact => {
        this.partyProfile = contact;
      });
    });

    
    this.exchangeRate = await this.settingsProvider.getExchangeRate(this.user);

    this.getFinanceTrx();    
  }

  getFinanceTrx()
  {
    this.transactionProvider.getFinanceTransactionsListForBizTrxID(this.user,this.bizTrxID)
    .then( data => {
      this.finTrxList = data;
      console.log("this.finTrxs.length " + this.finTrxList.length);
      if(this.bizTransaction)
        this.bizTrxSummary = getBusinessTransactionSummary(this.bizTransaction,this.finTrxList,this.exchangeRate);
    });
  }

  deleteFinanceTransaction(index: number)
  {
    console.log("Inside BusinessTransactionSummaryPage::deleteFinanceTransaction()");
    console.log("this.finTrxs["+index+"] : "+this.bizTrxSummary.finTrxs[index].ID);
    
    //Delete DB record
    //alert for deleting selected finance transaction
    if(index > 0)
    {
      if(this.trxCurrency === myConstants.localCurrency)
      {
        let confirm = this.alertCtrl.create(
          {
            title: 'Delete Entry of Rs.' + this.bizTrxSummary.finTrxs[index].trxLocalValue,
            message: 'Do you want to delete this transaction dated ' + this.bizTrxSummary.finTrxs[index].trxDate + '?',
            buttons: [
              { text: 'No'},
              {
                text: 'Yes',
                handler: () => 
                {
                  this.transactionProvider.deleteFinanceTransaction(this.user,this.bizTrxSummary.finTrxs[index])
                  .then(res => 
                  {
                    console.log("Selected Finance Trx Deleted : " + res);
                    this.getFinanceTrx();
                  //  this.viewCtrl.dismiss();
                  });
                }
              }
            ]
          });    
          confirm.present();
      }
      else 
      {
        let confirm = this.alertCtrl.create(
        {
          title: 'Delete Entry of $' + this.bizTrxSummary.finTrxs[index].trxForiegnValue,
          message: 'Do you want to delete this transaction dated ' + this.bizTrxSummary.finTrxs[index].trxDate + '?',
          buttons: [
            { text: 'No'},
            {
              text: 'Yes',
              handler: () => 
              {
                this.transactionProvider.deleteFinanceTransaction(this.user,this.bizTrxSummary.finTrxs[index])
                .then(res => 
                {
                  console.log("Selected Finance Trx Deleted : " + res);
                  this.getFinanceTrx();                  
                });
              }
            }
          ]
        });    
        confirm.present();
      }
    }
  }

  openPopOver(event,trxSummary)
  {
    let popover = this.popOverCtrl.create('BusinessTransactionPopoverPage',{User:this.user,trxSummary: trxSummary,profile:this.partyProfile});
    popover.present({
      ev: event
    });
    popover.onDidDismiss(() => {
      this.viewCtrl.dismiss();
    });
  }

}
