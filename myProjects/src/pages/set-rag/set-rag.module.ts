import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetRAGPage } from './set-rag';

@NgModule({
  declarations: [
    SetRAGPage,
  ],
  imports: [
    IonicPageModule.forChild(SetRAGPage),
  ],
})
export class SetRagPageModule {}
