import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams,AlertController, ViewController ,Loading,LoadingController} from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';
import { Milestone } from '../../model/milestone';
import { Project } from '../../model/project';
import { Risk } from '../../model/risk';
import { Issue } from '../../model/issue';
import { Action } from '../../model/action';
import { myConstants } from '../../model/myConstants';

/**
 * Generated class for the SetRagPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-set-rag',
  templateUrl: 'set-rag.html',
})
export class SetRAGPage {

  private milestone:Milestone;
  private project:Project;
  private risk:Risk;  
  private issue:Issue;
  private action:Action;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private firebaseProvider:FirebaseProvider,
    private alertCtrl:AlertController,
    private loadingCtrl: LoadingController,    
    private viewCtrl: ViewController
  ) {
    this.risk = {} as Risk;
    this.risk.id = this.navParams.get('riskID');
    console.log("this.risk.id " + this.risk.id);

    this.issue = {} as Issue;
    this.issue.id = this.navParams.get('issueID');
    console.log("this.issue.id " + this.issue.id);

    this.action = {} as Action;
    this.action.id = this.navParams.get('actionID');
    console.log("this.action.id " + this.action.id);
    
    this.project = {} as Project;
    this.project.id = this.navParams.get('projectID');
    this.milestone = {} as Milestone;
    this.milestone.id = this.navParams.get('milestoneID');
    
    if(this.risk.id)
    {
      this.firebaseProvider.getRisk(this.project.id,this.milestone.id,this.risk.id)
      .then(data => {
          this.risk = data;
          console.log("Risk Target Closure Date " + this.risk.estimatedClosureDate);
      });
    }

    if(this.issue.id)
    {
      this.firebaseProvider.getIssue(this.project.id,this.milestone.id,this.issue.id)
      .then(data => {
          this.issue = data;
          console.log("issue estimated Closure Date " + this.issue.estimatedClosureDate);
      });
    }

    if(this.action.id)
    {
      this.firebaseProvider.getAction(this.project.id,this.milestone.id,this.action.id)
      .then(data => {
          this.action = data;
          console.log("action estimated Closure Date " + this.action.estimatedClosureDate);
      });
    }

    if(this.project.id)
    {
      this.firebaseProvider.getProject(this.project.id)
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
      });
      
      if(this.milestone.id)
      {
        this.firebaseProvider.getMilestone(this.project.id,this.milestone.id)
        .then(data => {
            this.milestone = data;
            console.log("Milestone Estimated Closure Date " + this.milestone.estimatedClosureDate);
        });
      }   
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetRagPage');
  }

  async setRED()
  {
    this.viewCtrl.dismiss();  
    if(this.risk.id)
    {
      this.risk.RAG = myConstants.RAG.RED;
        
      console.log("Calling firebaseProvider.updateRisk");
      await this.firebaseProvider.updateRisk(this.project.id,this.milestone.id,this.risk);
    }
    if(this.issue.id)
    {
      this.issue.RAG = myConstants.RAG.RED;
        
      console.log("Calling firebaseProvider.updateIssue");
      await this.firebaseProvider.updateIssue(this.project.id,this.milestone.id,this.issue);
    }
    if(this.action.id)
    {
      this.action.RAG = myConstants.RAG.RED;
        
      console.log("Calling firebaseProvider.updateAction");
      await this.firebaseProvider.updateAction(this.project.id,this.milestone.id,this.action);
    }
  }

  async setAMBER()
  {
    this.viewCtrl.dismiss();  
    if(this.risk.id)
    {
      this.risk.RAG = myConstants.RAG.AMBER;
      console.log("Calling firebaseProvider.updateRisk");
      await this.firebaseProvider.updateRisk(this.project.id,this.milestone.id,this.risk);
    }
    if(this.issue.id)
    {
      this.issue.RAG = myConstants.RAG.AMBER;
        
      console.log("Calling firebaseProvider.updateIssue");
      await this.firebaseProvider.updateIssue(this.project.id,this.milestone.id,this.issue);
    }
    if(this.action.id)
    {
      this.action.RAG = myConstants.RAG.AMBER;
        
      console.log("Calling firebaseProvider.updateAction");
      await this.firebaseProvider.updateAction(this.project.id,this.milestone.id,this.action);
    }
  }

  async setGREEN()
  {
    this.viewCtrl.dismiss();  
    if(this.risk.id)
    {
      this.risk.RAG = myConstants.RAG.GREEN;
      console.log("Calling firebaseProvider.updateRisk");
      await this.firebaseProvider.updateRisk(this.project.id,this.milestone.id,this.risk);      
    }
    if(this.issue.id)
    {
      this.issue.RAG = myConstants.RAG.GREEN;
        
      console.log("Calling firebaseProvider.updateIssue");
      await this.firebaseProvider.updateIssue(this.project.id,this.milestone.id,this.issue);
    }
    if(this.action.id)
    {
      this.action.RAG = myConstants.RAG.GREEN;
        
      console.log("Calling firebaseProvider.updateAction");
      await this.firebaseProvider.updateAction(this.project.id,this.milestone.id,this.action);
    }
  }
}
