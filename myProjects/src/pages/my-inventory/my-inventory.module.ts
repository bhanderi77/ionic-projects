import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyInventoryPage } from './my-inventory';

@NgModule({
  declarations: [
    MyInventoryPage,
  ],
  imports: [
    IonicPageModule.forChild(MyInventoryPage),
  ],
})
export class MyInventoryPageModule {}
