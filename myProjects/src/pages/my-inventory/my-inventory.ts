import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { FormControl } from '@angular/forms';
//import { Summary } from '@angular/compiler';
//import { AngularFirestoreCollection } from 'angularfire2/firestore';

import { InventoryProvider } from '../../providers/inventory/inventory';
import { SettingsProvider } from '../../providers/settings/settings';
//import { PackageRulesPage } from '../package-rules/package-rules';
import { taskSummary,summaryDetails } from '../../model/taskSummary';
import { PackagedItem } from '../../model/packagedItem';
import { PackageRule } from '../../model/packageRule';

import { User, checkIfValid } from '../../model/user';

import { myConstants, round } from '../../model/myConstants';
import { Subscription } from 'rxjs';


/**
 * Generated class for the MyInventoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-inventory',
  templateUrl: 'my-inventory.html',
})
export class MyInventoryPage {
  private user:User;
  private packageRuleList:Array<PackageRule>;

 // private packageRulesCollectionRef: AngularFirestoreCollection<PackageRule>;

  private taskSummaryList:Array<taskSummary>;
  private pItemList_AB:Array<PackagedItem>;
  private pItemList_BB:Array<PackagedItem>;

  private bSearching:boolean;
  private searchInput:string;
  private searchList:Array<string>;
  private searchSeriesID:string;

  private iTrxID:string;

  private mySubscriptions : Subscription;


  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    private settingsProvider:SettingsProvider,
    private inventoryProvider: InventoryProvider) {

      this.user = {} as User;
      this.user = navParams.get('User');
      this.taskSummaryList = [];
      this.pItemList_AB = [];
      this.pItemList_BB = [];

      //this.searchControl = new FormControl();
      this.searchInput = "";
      this.searchList = [];
      this.bSearching = false;
      this.searchSeriesID = "";

      this.iTrxID = "";
      
      this.initializeData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyInventoryPage');
  }

  ionViewWillEnter()
  {
    console.log("ionViewWillEnter MyInventoryPage");
    this.initializeData();  
  }

  async initializeData()
  {
    console.log("Welcome to MyInventoryPage::initilizeData");
    this.mySubscriptions = new Subscription();

    this.packageRuleList = await this.settingsProvider.getPackageRulesArray(this.user);
    
    this.mySubscriptions.add(this.inventoryProvider.getAll_AtoB_packagedItems(this.user).valueChanges()
    .subscribe(pList => {
      if(pList.length > 0)
      {
        this.pItemList_AB = pList;
        this.buildSummary();
      }      
    }));

    this.mySubscriptions.add(this.inventoryProvider.getAll_BtoB_packagedItems(this.user).valueChanges()
    .subscribe(pList => {
      if(pList.length > 0)
      {
        this.pItemList_BB = pList;
        this.buildSummary();
      }
    }));
  }

  ionViewWillLeave()
  {
    console.log("Welcome to MyInventoryPage::ionViewWillLeave()");
    this.mySubscriptions.unsubscribe();
  }

  async buildSummary()
  {
    console.log("Welcome to MyInventoryPage::buildSummary");
    //this.packagRuleList = this.inventoryProvider.getPackageRulesList().valueChanges();
    this.taskSummaryList = [];
    let taskCodeList:Array<string> = [];
    //await this.packagRuleList.subscribe(pRuleList => 
    //{
      
      for(let i=0;i<this.packageRuleList.length;i++)
      {
       // if(this.packageRuleList[i].nextTaskCode != myConstants.taskCode.SALES)
        {
          if(taskCodeList.filter(ts => ts === this.packageRuleList[i].nextTaskCode).length === 0)
          {//If unique
            console.log("Calling getTaskSummary for " + this.packageRuleList[i].nextTaskCode);
            this.getTaskSummary(this.packageRuleList[i].nextTaskCode);
            taskCodeList.push(this.packageRuleList[i].nextTaskCode);
          }
        }
      }     
    //}
  }

  getTaskSummary(taskCode:string)
  {
    console.log("Welcome to MyInventoryPage:getTaskSummary for " + taskCode);
    let taskSummaryRec = {} as taskSummary;
    taskSummaryRec.taskCode = taskCode;
    taskSummaryRec.Input = {} as summaryDetails;
    taskSummaryRec.Input.totalQty=0;
    taskSummaryRec.Input.totalWeight=0;
    taskSummaryRec.Input.taskCost=0;
    taskSummaryRec.Input.accumulatedTaskCost=0;

    taskSummaryRec.InProgress = {} as summaryDetails;
    taskSummaryRec.InProgress.totalQty=0;
    taskSummaryRec.InProgress.totalWeight=0;
    taskSummaryRec.InProgress.taskCost=0;
    taskSummaryRec.InProgress.accumulatedTaskCost=0;

    taskSummaryRec.Completed = {} as summaryDetails;
    taskSummaryRec.Completed.totalQty=0;
    taskSummaryRec.Completed.totalWeight=0;
    taskSummaryRec.Completed.taskCost=0;
    taskSummaryRec.Completed.accumulatedTaskCost=0;

    taskSummaryRec.Pending = {} as summaryDetails;
    taskSummaryRec.Pending.totalQty=0;
    taskSummaryRec.Pending.totalWeight=0;
    taskSummaryRec.Pending.taskCost=0;
    taskSummaryRec.Pending.accumulatedTaskCost=0;

    taskSummaryRec.Output = {} as summaryDetails;
    taskSummaryRec.Output.totalQty=0;
    taskSummaryRec.Output.totalWeight=0;
    taskSummaryRec.Output.taskCost=0;
    taskSummaryRec.Output.accumulatedTaskCost=0;

    let pItemListAtoB:Array<PackagedItem>,pItemListBtoC:Array<PackagedItem>,pItemListBtoB:Array<PackagedItem>;
    if(this.iTrxID)
    {
      pItemListAtoB = this.pItemList_AB.filter(pItem => (pItem.pRule.nextTaskCode === taskCode) && (pItem.trxID === this.iTrxID));
      pItemListBtoC = this.pItemList_AB.filter(pItem => (pItem.pRule.curTaskCode === taskCode)  && (pItem.trxID === this.iTrxID));
      pItemListBtoB = this.pItemList_BB.filter(pItem => (pItem.pRule.curTaskCode === taskCode)  && (pItem.trxID === this.iTrxID));
    }
    else
    {
      pItemListAtoB = this.pItemList_AB.filter(pItem => pItem.pRule.nextTaskCode === taskCode);
      pItemListBtoC = this.pItemList_AB.filter(pItem => pItem.pRule.curTaskCode === taskCode);
      pItemListBtoB = this.pItemList_BB.filter(pItem => pItem.pRule.curTaskCode === taskCode);
    }
    
    pItemListAtoB.forEach(pItem => {
      if((pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
      && ( (pItem.packageStatus === myConstants.packageStatus.RD)
        || (pItem.packageStatus === myConstants.packageStatus.PD)
        || (pItem.packageStatus === myConstants.packageStatus.IP)
        || (pItem.packageStatus === myConstants.packageStatus.CO)
        ))
      {//A->B and  either RD or PD or CO or IP
        taskSummaryRec.Input.totalQty += round(+pItem.itemQty,2);
        taskSummaryRec.Input.totalWeight += round(+pItem.itemWeight,2);
        taskSummaryRec.Input.taskCost += round(+pItem.taskCost,2);
        taskSummaryRec.Input.accumulatedTaskCost += round(+pItem.accumulatedTaskCost,2);

        if((pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_RETURN)
        && (pItem.packageStatus === myConstants.packageStatus.CO))
        {
          taskSummaryRec.Completed.totalQty += round(+pItem.itemQty,2);
          taskSummaryRec.Completed.totalWeight += round(+pItem.itemWeight,2);
          taskSummaryRec.Completed.taskCost += round(+pItem.taskCost,2);
          taskSummaryRec.Completed.accumulatedTaskCost += round(+pItem.accumulatedTaskCost,2);
          console.log("Completed for MFG_RETURN " + taskSummaryRec.Completed.totalWeight);
        }
      }
    });
    
    pItemListBtoB.forEach(pItem => {
      if((pItem.pRule.curTaskCode === pItem.pRule.nextTaskCode) && (pItem.packageStatus === myConstants.packageStatus.IP))
      {//B->B and IP 
        taskSummaryRec.InProgress.totalQty += round(+pItem.itemQty,2);
        taskSummaryRec.InProgress.totalWeight += round(+pItem.itemWeight,2);
        taskSummaryRec.InProgress.taskCost += round(+pItem.taskCost,2);
        taskSummaryRec.InProgress.accumulatedTaskCost += round(+pItem.accumulatedTaskCost,2);
      }
      else if((pItem.pRule.curTaskCode === pItem.pRule.nextTaskCode) && (pItem.packageStatus === myConstants.packageStatus.CO))
      {//B->B and CO
        taskSummaryRec.Completed.totalQty += round(+pItem.itemQty,2);
        taskSummaryRec.Completed.totalWeight += round(+pItem.itemWeight,2);
        taskSummaryRec.Completed.taskCost += round(+pItem.taskCost,2);
        taskSummaryRec.Completed.accumulatedTaskCost += round(+pItem.accumulatedTaskCost,2);
      }
    });
    
    pItemListBtoC.forEach(pItem => {
      if( (pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
      && ( (pItem.packageStatus === myConstants.packageStatus.RD)
        || (pItem.packageStatus === myConstants.packageStatus.PD)
        || (pItem.packageStatus === myConstants.packageStatus.CO)
        || (pItem.packageStatus === myConstants.packageStatus.IP)
        ))
        {
          taskSummaryRec.Output.totalQty += round(+pItem.itemQty,2);
          taskSummaryRec.Output.totalWeight += round(+pItem.itemWeight,2);
          taskSummaryRec.Output.taskCost += round(+pItem.taskCost,2);
          taskSummaryRec.Output.accumulatedTaskCost += round(+pItem.accumulatedTaskCost,2);
        }  
    });
    taskSummaryRec.Pending.totalQty = round((+taskSummaryRec.Input.totalQty - (+taskSummaryRec.InProgress.totalQty + +taskSummaryRec.Completed.totalQty)),2);
    taskSummaryRec.Pending.totalWeight = round((+taskSummaryRec.Input.totalWeight - (+taskSummaryRec.InProgress.totalWeight + +taskSummaryRec.Completed.totalWeight)),2);
    taskSummaryRec.Pending.taskCost = round((+taskSummaryRec.Input.taskCost - (+taskSummaryRec.InProgress.taskCost + +taskSummaryRec.Completed.taskCost)),2);
    taskSummaryRec.Pending.accumulatedTaskCost = round((+taskSummaryRec.Input.accumulatedTaskCost - (+taskSummaryRec.InProgress.accumulatedTaskCost + +taskSummaryRec.Completed.accumulatedTaskCost)),2);      
    
    this.taskSummaryList.push(taskSummaryRec);
  }
  /*
  async getTaskSummary(taskCode:string)
  {
    console.log("Welcome to MyInventoryPage:getTaskSummary for " + taskCode);
    let taskSummaryRec = {} as taskSummary;
    taskSummaryRec.taskCode = taskCode;
    taskSummaryRec.Input = {} as summaryDetails;
    taskSummaryRec.Input.totalQty=0;
    taskSummaryRec.Input.totalWeight=0;
    taskSummaryRec.Input.totalCost=0;
    taskSummaryRec.Input.totalTax=0;

    taskSummaryRec.InProgress = {} as summaryDetails;
    taskSummaryRec.InProgress.totalQty=0;
    taskSummaryRec.InProgress.totalWeight=0;
    taskSummaryRec.InProgress.totalCost=0;
    taskSummaryRec.InProgress.totalTax=0;

    taskSummaryRec.Completed = {} as summaryDetails;
    taskSummaryRec.Completed.totalQty=0;
    taskSummaryRec.Completed.totalWeight=0;
    taskSummaryRec.Completed.totalCost=0;
    taskSummaryRec.Completed.totalTax=0;

    taskSummaryRec.Pending = {} as summaryDetails;
    taskSummaryRec.Pending.totalQty=0;
    taskSummaryRec.Pending.totalWeight=0;
    taskSummaryRec.Pending.totalCost=0;
    taskSummaryRec.Pending.totalTax=0;

    taskSummaryRec.Output = {} as summaryDetails;
    taskSummaryRec.Output.totalQty=0;
    taskSummaryRec.Output.totalWeight=0;
    taskSummaryRec.Output.totalCost=0;
    taskSummaryRec.Output.totalTax=0;

    let pItemListAtoB:Observable<PackagedItem[]>,pItemListBtoC:Observable<PackagedItem[]>,pItemListBtoB:Observable<PackagedItem[]>;
    pItemListAtoB = await this.inventoryProvider.get_packagedItems_For_B(this.user,taskCode).valueChanges();
    pItemListBtoC = await this.inventoryProvider.get_packagedItems_For_A(this.user,taskCode).valueChanges();
    pItemListBtoB = await this.inventoryProvider.get_BtoB_packagedItems(this.user,taskCode).valueChanges();
    
    await pItemListAtoB.subscribe(pItemList => {
      if(pItemList.length > 0)
      {
        taskSummaryRec.Input.totalQty=0;
        taskSummaryRec.Input.totalWeight=0;
        taskSummaryRec.Input.totalCost=0;
        taskSummaryRec.Input.totalTax=0;

        if(taskCode === myConstants.taskCode.MFG_RETURN)
        {
          taskSummaryRec.Completed.totalQty = 0;
          taskSummaryRec.Completed.totalWeight = 0;
          taskSummaryRec.Completed.totalCost = 0;
          taskSummaryRec.Completed.totalTax = 0;
        }

        pItemList.forEach(pItem => {
          if((pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
          && ( (pItem.packageStatus === myConstants.packageStatus.RD)
            || (pItem.packageStatus === myConstants.packageStatus.PD)
            || (pItem.packageStatus === myConstants.packageStatus.IP)
            || (pItem.packageStatus === myConstants.packageStatus.CO)
            ))
          {//A->B and  either RD or PD or CO or IP
            taskSummaryRec.Input.totalQty += round(+pItem.itemQty,2);
            taskSummaryRec.Input.totalWeight += round(+pItem.itemWeight,2);
            taskSummaryRec.Input.totalCost += round(+pItem.itemCost,2);
            taskSummaryRec.Input.totalTax += round(+pItem.itemTax,2);

            if((pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_RETURN)
            && (pItem.packageStatus === myConstants.packageStatus.CO))
            {
              taskSummaryRec.Completed.totalQty += round(+pItem.itemQty,2);
              taskSummaryRec.Completed.totalWeight += round(+pItem.itemWeight,2);
              taskSummaryRec.Completed.totalCost += round(+pItem.itemCost,2);
              taskSummaryRec.Completed.totalTax += round(+pItem.itemTax,2);
              console.log("Completed for MFG_RETURN " + taskSummaryRec.Completed.totalWeight);
            }
          }
        });
        taskSummaryRec.Pending.totalQty = round((+taskSummaryRec.Input.totalQty - (+taskSummaryRec.InProgress.totalQty + +taskSummaryRec.Completed.totalQty)),2);
        taskSummaryRec.Pending.totalWeight = round((+taskSummaryRec.Input.totalWeight - (+taskSummaryRec.InProgress.totalWeight + +taskSummaryRec.Completed.totalWeight)),2);
        taskSummaryRec.Pending.totalCost = round((+taskSummaryRec.Input.totalCost - (+taskSummaryRec.InProgress.totalCost + +taskSummaryRec.Completed.totalCost)),2);
        taskSummaryRec.Pending.totalTax = round((+taskSummaryRec.Input.totalTax - (+taskSummaryRec.InProgress.totalTax + +taskSummaryRec.Completed.totalTax)),2);      
      }
      
    });

    await pItemListBtoB.subscribe(pItemList => {
      if(pItemList.length > 0)
      {
        taskSummaryRec.InProgress.totalQty=0;
        taskSummaryRec.InProgress.totalWeight=0;
        taskSummaryRec.InProgress.totalCost=0;
        taskSummaryRec.InProgress.totalTax=0;

        taskSummaryRec.Completed.totalQty=0;
        taskSummaryRec.Completed.totalWeight=0;
        taskSummaryRec.Completed.totalCost=0;
        taskSummaryRec.Completed.totalTax=0;

        pItemList.forEach(pItem => {
          if((pItem.pRule.curTaskCode === pItem.pRule.nextTaskCode) && (pItem.packageStatus === myConstants.packageStatus.IP))
          {//B->B and IP 
            taskSummaryRec.InProgress.totalQty += round(+pItem.itemQty,2);
            taskSummaryRec.InProgress.totalWeight += round(+pItem.itemWeight,2);
            taskSummaryRec.InProgress.totalCost += round(+pItem.itemCost,2);
            taskSummaryRec.InProgress.totalTax += round(+pItem.itemTax,2);
          }
          else if((pItem.pRule.curTaskCode === pItem.pRule.nextTaskCode) && (pItem.packageStatus === myConstants.packageStatus.CO))
          {//B->B and CO
            taskSummaryRec.Completed.totalQty += round(+pItem.itemQty,2);
            taskSummaryRec.Completed.totalWeight += round(+pItem.itemWeight,2);
            taskSummaryRec.Completed.totalCost += round(+pItem.itemCost,2);
            taskSummaryRec.Completed.totalTax += round(+pItem.itemTax,2);
          }
        });
        taskSummaryRec.Pending.totalQty = round((+taskSummaryRec.Input.totalQty - (+taskSummaryRec.InProgress.totalQty + +taskSummaryRec.Completed.totalQty)),2);
        taskSummaryRec.Pending.totalWeight = round((+taskSummaryRec.Input.totalWeight - (+taskSummaryRec.InProgress.totalWeight + +taskSummaryRec.Completed.totalWeight)),2);
        taskSummaryRec.Pending.totalCost = round((+taskSummaryRec.Input.totalCost - (+taskSummaryRec.InProgress.totalCost + +taskSummaryRec.Completed.totalCost)),2);
        taskSummaryRec.Pending.totalTax = round((+taskSummaryRec.Input.totalTax - (+taskSummaryRec.InProgress.totalTax + +taskSummaryRec.Completed.totalTax)),2);      
      }
    });

    await pItemListBtoC.subscribe(pItemList => {
      if(pItemList.length > 0)
      {
        taskSummaryRec.Output.totalQty=0;
        taskSummaryRec.Output.totalWeight=0;
        taskSummaryRec.Output.totalCost=0;
        taskSummaryRec.Output.totalTax=0;

        pItemList.forEach(pItem => {
          if( (pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
          && ( (pItem.packageStatus === myConstants.packageStatus.RD)
            || (pItem.packageStatus === myConstants.packageStatus.PD)
            || (pItem.packageStatus === myConstants.packageStatus.CO)
            || (pItem.packageStatus === myConstants.packageStatus.IP)
            ))
            {
              taskSummaryRec.Output.totalQty += round(+pItem.itemQty,2);
              taskSummaryRec.Output.totalWeight += round(+pItem.itemWeight,2);
              taskSummaryRec.Output.totalCost += round(+pItem.itemCost,2);
              taskSummaryRec.Output.totalTax += round(+pItem.itemTax,2);
            }  
        });
        taskSummaryRec.Pending.totalQty = round((+taskSummaryRec.Input.totalQty - (+taskSummaryRec.InProgress.totalQty + +taskSummaryRec.Completed.totalQty)),2);
        taskSummaryRec.Pending.totalWeight = round((+taskSummaryRec.Input.totalWeight - (+taskSummaryRec.InProgress.totalWeight + +taskSummaryRec.Completed.totalWeight)),2);
        taskSummaryRec.Pending.totalCost = round((+taskSummaryRec.Input.totalCost - (+taskSummaryRec.InProgress.totalCost + +taskSummaryRec.Completed.totalCost)),2);
        taskSummaryRec.Pending.totalTax = round((+taskSummaryRec.Input.totalTax - (+taskSummaryRec.InProgress.totalTax + +taskSummaryRec.Completed.totalTax)),2);      
      }      
    });
    this.taskSummaryList.push(taskSummaryRec);
  }
  */
  gotoPackageRules()
  {
    this.navCtrl.push('PackageRulesPage');
  }

  gotoTaskSummary(taskCode:string)
  {
    console.log('MyInventoryPage::gotoTaskSummary(taskCode) for ' + taskCode + this.iTrxID);
    this.navCtrl.push('TaskSummaryPage',{User:this.user,taskCode:taskCode,trxID:this.iTrxID});  
  }

  /*
  async addItem()
  {
    console.log("MyInventoryPage::addItem");
    let pRules:Array<PackageRule>;
    let pItem = {} as PackagedItem;
    this.inventoryProvider.getPackageRule_For_A("BUY")
    .then(data => {
      pRules = data;
      console.log("addItem=>pRules.length " + pRules.length);
    
      if(pRules.length === 1)
      {
        console.log("pRule " + pRules[0].curTaskCode + " " + pRules[0].nextTaskCode);
      
        let alert = this.alertCtrl.create(
        {
          title: "Buy Rough",
          inputs: [ { name: 'InputWeight', placeholder: 'Enter Weight'},
                    { name: 'InputCost', placeholder: 'Enter Cost'},
                    { name: 'InputTax', placeholder: 'Enter Tax'},
                  ],
          buttons:
                  [ { 
                      text: 'Cancel',
                      handler : data => { console.log('Cancel clicked');}
                    },
                    { 
                      text: 'Ok',
                      handler : data => 
                      { 
                        pItem.itemWeight = +data.InputWeight;
                        pItem.itemCost = +data.InputCost;
                        pItem.itemTax = +data.InputTax;
                        pItem.itemQty = 0;
                        pItem.itemRemarks = '';
                        pItem.pRule = pRules[0];
                        pItem.packageID = '';
                        pItem.parentID = "0";
                        pItem.packageGroupID = '';
                        pItem.packageStatus = myConstants.packageStatus.RD;

                        this.inventoryProvider.createPackagedItem_BtoC(pItem,pItem.parentID);
                      }
                    },
                  ]
        });
        alert.present();
      }
    }).catch(e => {
      console.log("Error from this.inventoryProvider.getBtoCPackageRules" + e);
    });      
  }*/

  prepareSearchList()
  {
    console.log("Welcome to MyInventoryPage:prepareSearchList() " + this.pItemList_AB.length);
    let purchasedItemList:Array<PackagedItem> = [];
    this.searchList = [];
    purchasedItemList = this.pItemList_AB.filter(pItem => pItem.pRule.curTaskCode === myConstants.taskCode.PURCHASE);
    for(let i = 0;(i<purchasedItemList.length);i++)
    {
      console.log("i " + i + " " + purchasedItemList[i].packageGroupID);
      let searchItem:string = "";
      searchItem += purchasedItemList[i].packageGroupID;
      
      if(purchasedItemList[i].trxPartyName)
      {
        searchItem += ","+purchasedItemList[i].trxPartyName;
      }
      if(purchasedItemList[i].trxDate)
      {
        searchItem += ","+purchasedItemList[i].trxDate;
      }
      if((purchasedItemList[i].trxWeight) && (purchasedItemList[i].trxRate) && (purchasedItemList[i].trxCurrency))
      {
        searchItem += ","+purchasedItemList[i].trxWeight+"ct X " + purchasedItemList[i].trxRate+purchasedItemList[i].trxCurrency;
      }
      if(this.searchList.filter(s => s === searchItem).length === 0)
      {//If unique
        this.searchList.push(searchItem);
      }
    }
  }

  onSearch(searchbar)
  {
    console.log("Welcome to MyInventoryPage:onSearch()");
    // console.log('inside onSearch this.SearchInput : ' + this.SearchInput);
    if((this.searchInput.length == 0) || (this.bSearching = false))
    {
      this.inputSelected(""); 
    }
    else
    {
      this.bSearching = true;
  	  this.prepareSearchList();
  	
      this.searchList = this.searchList.filter((item) => {
        if(item.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1)
        {       
          return true;
        }
        else
          return false;
      });
    }
  	
  }

  inputSelected(searchItem)
  {
    console.log("Welcome to MyInventoryPage:inputSelected()" + searchItem);
    let purchaseItemList:Array<PackagedItem> = [];
    this.searchList = [];
    this.searchSeriesID = ""+searchItem;
    this.searchInput = searchItem;
    this.bSearching = false;
    this.iTrxID = "";
        
    let searchParams:Array<string> = [];
    searchParams = this.searchSeriesID.split(",",4);
    console.log("searchParams.length " + searchParams.length);
    if(searchParams.length === 4)
    {
      purchaseItemList = this.pItemList_AB.filter(pItem => ((pItem.packageGroupID === searchParams[0])
                                                            && (pItem.trxPartyName === searchParams[1])
                                                            && (pItem.trxDate === searchParams[2])
                                                            && (true)));
      if(purchaseItemList.length)
      {
        this.iTrxID = purchaseItemList[0].trxID;
        this.buildSummary();  
      }
    }
    else if(searchParams.length === 3)
    {
      purchaseItemList = this.pItemList_AB.filter(pItem => ((pItem.packageGroupID === searchParams[0])
                                                            && (pItem.trxPartyName === searchParams[1])
                                                            && (pItem.trxDate === searchParams[2])));
      if(purchaseItemList.length)
      {
        this.iTrxID = purchaseItemList[0].trxID;
        this.buildSummary();  
      }
    }

    this.buildSummary();
  }

}
