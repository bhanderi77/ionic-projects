import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,Nav } from 'ionic-angular';
import { User } from '../../model/user';
import { MenuProvider } from "../../providers/menu/menu";
import { FinanceSettings } from '../../model/referenceData';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { EntityProvider } from '../../providers/entity/entity';
import { BusinessContact } from '../../model/businessContact';

/**
 * Generated class for the DrawerLayoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// export interface PageInterface {
//   title: string;
//   pageName: string;
//   icon: string;
// }

@IonicPage()
@Component({
  selector: 'page-drawer-layout',
  templateUrl: 'drawer-layout.html',
})
export class DrawerLayoutPage {

  rootPage:any;
  private user:User;
  private bInvalidUser:boolean;
  private isLoggedIn:boolean;
  private partyProfiles:Array<BusinessContact>;	
  private transactionType: string;
  //private financeSettings:FinanceSettings;
  @ViewChild(Nav) nav:Nav;
 
  //pages: PageInterface[];
  pages: any;
  selectedMenu: any;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public menuProvider: MenuProvider,
    private entityProvider: EntityProvider,
    public menuCtrl: MenuController) {
    this.user = navParams.get('User');
    this.bInvalidUser = navParams.get('bInvalidUser');
    this.isLoggedIn = navParams.get('isLoggedIn');
    this.partyProfiles = [];
    //this.financeSettings = navParams.get('financeSettings');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DrawerLayoutPage');
  }

  ionViewWillEnter() {
    if (this.user.ID !== '') {
      this.getSideMenuData();
      this.loadPartyList();  
      // this.pages = [
      //   { title: 'Home', pageName: 'HomePage', icon: 'home' },
      //   { title: 'Contact', pageName: 'PartyListPage', icon: 'contacts' },
      //   { title: 'Party Summary List', pageName: 'PartySummaryListPage', icon: 'shuffle' },
      //   { title: 'My Inventory', pageName: 'MyInventoryPage', icon: 'shuffle' },
      //   { title: 'PriceList', pageName: 'MyPricePage', icon: 'shuffle' },
      //   { title: 'Polish Stock', pageName: 'SalesStockSummaryPage', icon: 'shuffle' }
      // ];
      this.nav.setRoot('HomePage',{User:this.user,bInvalidUser:this.bInvalidUser,isLoggedIn: this.isLoggedIn})
    }
  }

  getSideMenuData() {

    this.pages = this.menuProvider.getSideMenus();
  
  }

  loadPartyList()
  {
    this.entityProvider.getAllBusinessContactList(this.user)
    .then(contactList => {
      this.partyProfiles = contactList;
    });

  }

  openPage(page, index) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    console.log('Inside openPage::Page: '+ page.pageName);
    console.log('Inside openPage::index: '+ index)
    if (page.pageName) {
      if(page.pageName === 'HomePage')
        this.nav.setRoot(page.pageName,{User:this.user,bInvalidUser:this.bInvalidUser,isLoggedIn: this.isLoggedIn});
      else if(page.pageName === 'PartyTransactionsPage')
      {
        if(page.title === 'Purchase')
        {
          this.transactionType = 'Buy'
        }
        else if(page.title === 'Sales')
        {
          this.transactionType = 'Sell'
        }
        this.nav.setRoot(page.pageName,{User:this.user,transactionType:this.transactionType});
      }
      else if(page.pageName === 'PartyListPage')
      {
        console.log('pageName: ' + page.pageName + '\n partyCategory: ' + page.title);
        this.nav.setRoot(page.pageName,{User:this.user,partyCategory:page.title});
      }
      else if(page.pageName === 'PartySummaryListPage')
      {
        console.log('pageName: ' + page.pageName + '\n partyCategory: ' + page.title);
        this.nav.setRoot(page.pageName,{User:this.user,partyCategory:page.title});
      }
      else if(page.pageName === 'TaskSummaryPage')
      {
        if(page.title === 'Planning')
        {
          this.nav.setRoot(page.pageName,{User:this.user,taskCode:'PLANNING'});
        }
        else if(page.title === 'In Mfg')
        {
          this.nav.setRoot(page.pageName,{User:this.user,taskCode:'MFG_ISSUE'});
        }
        else if(page.title === 'Mfg Return')
        {
          this.nav.setRoot(page.pageName,{User:this.user,taskCode:'MFG_RETURN'});
        }
        else if(page.title === 'Assortment')
        {
          this.nav.setRoot(page.pageName,{User:this.user,taskCode:'ASSORTMENT'});
        }
        else if(page.title === 'Sales')
        {
          this.nav.setRoot(page.pageName,{User:this.user,taskCode:'SALES'});
        }
      }
      else
        this.nav.setRoot(page.pageName,{User:this.user});
      //this.nav.setRoot(page.component);
      this.menuCtrl.close();
    }
    else 
    {
      if (this.selectedMenu) {
        this.selectedMenu = 0;
      } else {
        this.selectedMenu = index;
      }
    }
  }
}
