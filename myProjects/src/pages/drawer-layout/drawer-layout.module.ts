import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DrawerLayoutPage } from './drawer-layout';

@NgModule({
  declarations: [
    DrawerLayoutPage,
  ],
  imports: [
    IonicPageModule.forChild(DrawerLayoutPage),
  ],
})
export class DrawerLayoutPageModule {}
