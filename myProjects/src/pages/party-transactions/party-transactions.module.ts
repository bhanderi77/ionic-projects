import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PartyTransactionsPage } from './party-transactions';
import { PipesModule } from '../../pipes/pipes.module';
@NgModule({
  declarations: [
    PartyTransactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PartyTransactionsPage),
    PipesModule
  ],
})
export class PartyTransactionsPageModule {}
