import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,ModalController } from 'ionic-angular';
import { PartySummary,BusinessTransactionSummary,calculateBusinessSummary, BusinessSummary } from '../../model/partySummary';
import { FinanceSettings } from '../../model/referenceData';
import { FinanceTransaction } from '../../model/financeTransaction';
import { User, checkIfValid } from '../../model/user';
import { myConstants } from '../../model/myConstants';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
import { SettingsProvider } from '../../providers/settings/settings';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { Subscription } from 'rxjs';
import { BusinessContact } from '../../model/businessContact';


/**
 * Generated class for the PartyTransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-party-transactions',
  templateUrl: 'party-transactions.html',
})
export class PartyTransactionsPage {
  private pSummary:PartySummary;	
  private financeSettings: FinanceSettings;
  private user:User;  
  private transactionType:string;
  private bizTrxSummary:Array<BusinessTransactionSummary>;
  private finTrxs:Array<FinanceTransaction>;
  private isBizTrxn: boolean;
  private isFinTrxn: boolean;
  private trxCurrency: string;
  private confidentialityLevel:number;
  private mySubscriptions : Subscription;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private platform: Platform,
    public modalCtrl: ModalController,
    private settingsProvider:SettingsProvider,
    private transactionProvider:TransactionProvider,
    public popOverCtrl: PopoverController) 
  {
    this.pSummary = {} as PartySummary;
    this.pSummary.businessSummary = {} as BusinessSummary;
    this.pSummary.profile = navParams.get("partyProfile");
    this.transactionType = navParams.get('transactionType');
    this.financeSettings = {} as FinanceSettings;
    this.user = {} as User;
    this.user = navParams.get('User');
    this.trxCurrency = myConstants.localCurrency;
    this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartyTransactionsPage');
  }

  ionViewWillEnter()
  {
    console.log("ionViewWillEnter " + this.confidentialityLevel);
    this.initializeData();  
  }

  async initializeData()
  {
    console.log("Welcome to PartyTransactionsPage::initializeData() ");

    this.mySubscriptions = new Subscription();
    this.pSummary.businessSummary = {} as BusinessSummary;
    this.pSummary.businessSummary.bizTrxs = [];
    this.pSummary.businessSummary.finTrxs = [];
    this.pSummary.businessSummary.bizTrxSummary = [];

    this.financeSettings = await this.settingsProvider.getFinanceSettings(this.user);
    //if(this.transactionType === 'Sell' || this.transactionType === 'Buy' || this.transactionType === 'Payment')
    if(this.transactionType)
    {
      this.mySubscriptions.add(this.transactionProvider.getAllBusinessTransactions(this.user,this.transactionType).valueChanges()
      .subscribe(businessTransactionList => {
        console.log("Received businessTransactionList: " + businessTransactionList.length);
        this.pSummary.businessSummary.bizTrxs = [];
        this.pSummary.businessSummary.bizTrxs = businessTransactionList.filter(bizTrx => ((bizTrx.trxStatus === myConstants.bizTrxStatus.OPEN) || (bizTrx.trxStatus === myConstants.bizTrxStatus.CLOSED)));
        calculateBusinessSummary(this.pSummary.businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
        this.filterPartyTransactions();
      }));

      this.mySubscriptions.add(this.transactionProvider.getAllFinanceTransactions(this.user).valueChanges()
        .subscribe(financeTransactionList => {
          console.log("Received financeTransactionList: " + financeTransactionList.length);
          this.pSummary.businessSummary.finTrxs = [];
          this.pSummary.businessSummary.finTrxs = financeTransactionList;
          calculateBusinessSummary(this.pSummary.businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
          this.filterPartyTransactions();
        }));
    } 
    else if(this.pSummary.profile)
    {
      if(this.pSummary.profile.category === myConstants.partyCategory.BUYER)
      {
        this.transactionType = myConstants.trxType.SELL;
      }
      else if(this.pSummary.profile.category === myConstants.partyCategory.SUPPLIER)
      {
        this.transactionType = myConstants.trxType.BUY;
      }
      try 
      {
        this.mySubscriptions.add(this.transactionProvider.getBusinessTransactionsForPartyID(this.user,this.pSummary.profile.id).valueChanges()
        .subscribe(businessTransactionList => {
          console.log("Subscribed to getBusinessTransactionsForPartyID: " + businessTransactionList.length);
          this.pSummary.businessSummary.bizTrxs = [];
          this.pSummary.businessSummary.bizTrxs = businessTransactionList.filter(bizTrx => ((bizTrx.trxStatus === myConstants.bizTrxStatus.OPEN) || (bizTrx.trxStatus === myConstants.bizTrxStatus.CLOSED)));
          calculateBusinessSummary(this.pSummary.businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
          this.filterPartyTransactions();
        })); 
  
          this.mySubscriptions.add(this.transactionProvider.getFinanceTransactionsForPartyID(this.user,this.pSummary.profile.id).valueChanges()
        .subscribe(financeTransactionList => {
          console.log("Subscribed to getFinanceTransactionsForPartyID: " + financeTransactionList.length);
          this.pSummary.businessSummary.finTrxs = [];
          this.pSummary.businessSummary.finTrxs = financeTransactionList;
          calculateBusinessSummary(this.pSummary.businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
          this.filterPartyTransactions();
        }));
      } catch (error) {
        console.log("Error in initializeData: " +  error);
      }
    }
  }

  ionViewWillLeave()
  {
    console.log("Welcome to PartyTransactionsPage::ionViewWillLeave()");
    this.mySubscriptions.unsubscribe();
  }


  filterPartyTransactions()
  {
    //this.transactionType = value;
    console.log("transactionType " + this.transactionType);
    if(this.transactionType === myConstants.trxType.SELL)
    {
      console.log("filterPartyTransactions::Sell");
      this.isBizTrxn = true;
      this.isFinTrxn = false;     
      this.bizTrxSummary = this.pSummary.businessSummary.bizTrxSummary.filter(bizTransactionSummary => (bizTransactionSummary.businessTransaction.trxType === myConstants.trxType.SELL));
    }
    else if(this.transactionType === myConstants.trxType.BUY)
    {
      console.log("filterPartyTransactions::Buy");
      this.isBizTrxn = true;
      this.isFinTrxn = false;     
      this.bizTrxSummary = this.pSummary.businessSummary.bizTrxSummary.filter(bizTransactionSummary => (bizTransactionSummary.businessTransaction.trxType === myConstants.trxType.BUY));
    }
    else// if(this.transactionType === myConstants.trxType.)
    {
      console.log("filterPartyTransactions::Payment");
      this.isBizTrxn = false;
      this.isFinTrxn = true;      
      this.finTrxs = this.pSummary.businessSummary.finTrxs.filter(financeTransaction => ((financeTransaction.trxType === myConstants.trxType.IN_CREDIT) || (financeTransaction.trxType === myConstants.trxType.OUT_DEBIT)));
    }    
  }  

  openPopOver(event)
  {
    let popover = this.popOverCtrl.create('TransactionFilterPopoverPage');
    popover.present({
      ev: event
    });
    popover.onDidDismiss((trxnType:string) => {
      console.log("trxnType: " + trxnType);
      this.transactionType = trxnType;
      this.filterPartyTransactions();
    });
  }

  gotoBusinessTrxSummary(bizTrxSummary:BusinessTransactionSummary)
  { 
    if(bizTrxSummary.businessTransaction.trxType === myConstants.trxType.BUY)
    {
      this.navCtrl.push('BusinessTransactionSummaryPage',{User:this.user,bizTrxID:bizTrxSummary.businessTransaction.ID,partyProfile:this.pSummary.profile}); //,{finTrxSummary: finTrxSummary}
    }
    else if(bizTrxSummary.businessTransaction.trxType === myConstants.trxType.SELL)
    {
      if(bizTrxSummary.businessTransaction.trxStatus === myConstants.bizTrxStatus.DRAFT)
      {
        this.navCtrl.push('SalesTransactionItemsPage',{User:this.user,fromEntity:"TRANSACTION",bizTrxID:bizTrxSummary.businessTransaction.ID,confidentialityLevel:this.confidentialityLevel});
      }
      else
      {
        this.navCtrl.push('BusinessTransactionSummaryPage',{User:this.user,bizTrxID:bizTrxSummary.businessTransaction.ID,partyProfile:this.pSummary.profile}); //,{finTrxSummary: finTrxSummary}  
      }
    }
    
  }

  gotoBusinessTrxSummaryPage(finTrx:FinanceTransaction)
  { 
    this.navCtrl.push('BusinessTransactionSummaryPage',{User:this.user,partyProfile:this.pSummary.profile,bizTrxID:finTrx.BizTrxID}); //,{finTrxSummary: finTrxSummary}
  }

  onTitleClick() //Added by Rashmin - 09082018
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC) 
    && (this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova')))
    {
      let modal = this.modalCtrl.create('LockScreenPage');
        modal.present();
        modal.onDidDismiss((data) => {
          if(data === true)
          {
            this.switchConfidentialityLevel();
          }
          else
          {
            alert('Authentication Failed !!')
          }
        }); 
    }
    else //web flow
    {
      this.switchConfidentialityLevel();
    }    
  }

  switchConfidentialityLevel()
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC)
      && (true))//this.user.confidentialityLevel >= myConstants.confidentialityLevel.PUBLIC
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.CONFIDENTIAL;
    }
    else
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    }
    calculateBusinessSummary(this.pSummary.businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
    this.filterPartyTransactions();
  }
}
