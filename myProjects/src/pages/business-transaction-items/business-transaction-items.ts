import { Component } from '@angular/core';
import { IonicPage, NavController,ViewController, NavParams } from 'ionic-angular';
import { BusinessTransaction, BusinessItem } from '../../model/businessTransaction';
import { round } from '../../model/myConstants';

/**
 * Generated class for the BusinessTransactionItemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-business-transaction-items',
  templateUrl: 'business-transaction-items.html',
})
export class BusinessTransactionItemsPage {
  private bizTransaction:BusinessTransaction;
  private bizItems:Array<BusinessItem>;
  constructor(public navCtrl: NavController, 
    private viewCtrl: ViewController, public navParams: NavParams) {
      
      this.bizTransaction = navParams.get('businessTransaction');  
      //this.bizTransaction.trxItems = [];
      this.bizItems = [];
      this.addBusinessItem();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BusinessTransactionItemsPage');
  }

  TrackBy(index: number, obj: any):any
  {
  //  console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  addBusinessItem()
  {
    let item = {} as BusinessItem;
    item.itemCode = "";
    item.itemValue = 0;
    item.itemWeight = 0;
    item.itemRate = 0;
    item.itemRemarks = "";
    this.bizItems.push(item);
  }

  deleteBusinessItem(item:BusinessItem){
    this.bizItems.splice(this.bizItems.indexOf(item),1);
  }

  saveItems()
  {
    console.log("Welcome to BusinessTransactionItemsPage:saveItems() " + this.bizItems.length);
    let totalWeight:number = 0, totalCost:number = 0;
    if(this.bizItems.length > 0)
    {
      this.bizTransaction.trxItems = [];
      for(let i = 0 ; i < this.bizItems.length; i++)
      {
        let item = {} as BusinessItem;
        item.itemCode = this.bizItems[i].itemCode;
        item.itemWeight = this.bizItems[i].itemWeight;
        item.itemRate = this.bizItems[i].itemRate;
        item.itemRemarks = this.bizItems[i].itemRemarks;
        item.itemValue = (item.itemWeight * item.itemRate);
        
        totalWeight += +item.itemWeight;
        totalCost += +item.itemValue;
        this.bizTransaction.trxItems.push(item);
      }

      this.bizTransaction.trxWeight = totalWeight;
      this.bizTransaction.trxValue = round(totalCost,2);
      this.bizTransaction.trxRate = round((this.bizTransaction.trxValue/this.bizTransaction.trxWeight),2);
    }  
    this.viewCtrl.dismiss(); 
  }
}
