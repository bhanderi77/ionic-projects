import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,ViewController } from 'ionic-angular';
import { User, checkIfValid } from '../../model/user';
import { ItemQualityLevel } from '../../model/packagedItem';
import { SalesItem } from '../../model/salesItem';
import { myConstants, round } from '../../model/myConstants';

/**
 * Generated class for the AddPolishPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-polish',
  templateUrl: 'add-polish.html',
})
export class AddPolishPage {

  private user:User;
  private fromEntity:string;
  private itemQualityLevelList:Array<ItemQualityLevel>;
  private salesItemList:Array<SalesItem>;
  private totalWeight:number;
  private totalQty:number;
  
  constructor(public navCtrl: NavController, 
    public viewCtrl: ViewController,
    private modalCtrl: ModalController,
    public navParams: NavParams) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');
    this.fromEntity = "";
    this.fromEntity = this.navParams.get('fromEntity');
    this.itemQualityLevelList = [];
    this.salesItemList = [];
    this.totalWeight = 0;
    this.totalQty = 0;
    
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad AddPolishPage');
  }

  ionViewWillEnter()
  {
    console.log('ionViewWillEnter AddPolishPage');
    this.fromEntity = this.navParams.get('fromEntity');

    this.itemQualityLevelList = [];
    this.itemQualityLevelList = this.navParams.get('qualityLevelList');

    this.salesItemList = [];
    this.salesItemList = this.navParams.get('salesItemList');
    this.salesItemList.sort((a,b) => {
      if(a.itemQualityLevel.rank < b.itemQualityLevel.rank)
        return -1;
      else
        return 1;
    });
    this.calculateSummary();
  }
  validateQty(index:number)
  {
    console.log("Welcome to AddPolishPage::validateQty() "); 
    this.salesItemList[index].qty = round(this.salesItemList[index].qty,0);
    this.calculateSummary();
  }
  calculateSummary()
  {
    console.log("Welcome to AddPolishPage::calculateSummary() "); 
    this.totalWeight = 0;
    this.totalQty = 0;
    for(let i=0;i<this.salesItemList.length;i++)
    {
      this.totalWeight += +this.salesItemList[i].saleableWeight;
      this.totalQty += +this.salesItemList[i].qty;
    }
    this.totalWeight = round(this.totalWeight,3);
    this.totalQty = round(this.totalQty,0);
  }

  save()
  {
    this.viewCtrl.dismiss();
  }

  /*
  cancel()
  {
    this.viewCtrl.dismiss();
  }*/

}
