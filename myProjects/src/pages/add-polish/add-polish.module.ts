import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPolishPage } from './add-polish';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    AddPolishPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPolishPage),
    PipesModule
  ],
})
export class AddPolishPageModule {}
