import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,ModalController } from 'ionic-angular';
import { Loading,LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

//import { DateTime } from 'ionic-angular/components/datetime/datetime';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
//import { min } from 'rxjs/operator/min';
//import { DatabaseProvider } from '../../providers/database/database';
import { TransactionProvider } from '../../providers/transaction/transaction';
//import { Timestamp } from 'firebase/firestore/timestamp';
import { myConstants, round } from '../../model/myConstants';
//import { FormBuilder, Validators } from '@angular/common'

import { FinanceSettings } from '../../model/referenceData';
import { BusinessContact } from '../../model/businessContact';
import { BusinessTransaction } from '../../model/businessTransaction';
import { TaxRate,TaxAmount } from '../../model/tax';
import { User,checkIfValid } from '../../model/user';
//import { PartySummary, BusinessTransactionSummary } from '../../model/partySummary';
import { getDateString, getTimeString } from '../../model/utility';
import { EntityProvider } from '../../providers/entity/entity';


/**
 * Generated class for the BusinessTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageBusinessTransaction',
  templateUrl: 'businessTransaction.html',
})
export class BusinessTransactionPage {

  private trxnType: AbstractControl;
  private trxnDate: AbstractControl;
  private remarks: AbstractControl;
  private wtInCts: AbstractControl;
  private ratePerCts: AbstractControl; 
  private discount: AbstractControl; 
  private dueDays: AbstractControl;
  private trxCurrencyRate: AbstractControl;
  private trxCurrency: AbstractControl;
  private taxChecked: AbstractControl;
  private taxValue: AbstractControl;
  private invoiceId: AbstractControl;
  private brokerID: AbstractControl;
  private brokerageRate: AbstractControl;

  private exchangeRate: number;
  
  private pBusinessContact: BusinessContact;
  private bizTrx: BusinessTransaction;
  //editBizTrx: businessTransaction;
  private financeSettings:FinanceSettings;
  private user:User;
  private confidentialityLevel: number;
  private brokerList:Array<BusinessContact>;
  
  private formGroup: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public viewCtrl: ViewController,
    private modalCtrl:ModalController,
    private loadingCtrl: LoadingController,
    public formBuilder: FormBuilder, private transactionProvider: TransactionProvider,
    private network: Network, private entityProvider: EntityProvider) 
  {
    this.exchangeRate = 1;
    
    this.pBusinessContact = navParams.get('party');  
    //this.editBizTrx = navParams.get('trxSummary');
    this.financeSettings = navParams.get('financeSettings');
    this.confidentialityLevel = navParams.get('confidentialityLevel');
    this.user = {} as User;
    this.user = navParams.get('User');
    this.brokerList = [];
    if(this.pBusinessContact.category === myConstants.partyCategory.SUPPLIER)
    {
      this.formGroup = formBuilder.group({
        trxnType:['Buy',Validators.required],
        wtInCts:['',Validators.compose([Validators.min(0),Validators.required])],     //,Validators.min(0),Validators.required
        ratePerCts:['',Validators.compose([Validators.min(0),Validators.required])],
        discount:['0',Validators.compose([Validators.min(0),Validators.required])],  //RB1002
        trxnDate:['',Validators.required],
        dueDays:[this.financeSettings.dueDays,Validators.compose([Validators.min(0),Validators.required])],
        taxChecked:['false'],
        taxValue:[''],
        remarks:[''],
        invoiceId:[''],
        trxCurrency:['INR',Validators.required],  //RB2101
        trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])],
        brokerID:[],
        brokerageRate:['',Validators.compose([Validators.min(0),Validators.required])]
      });
    }
    else
    {
      this.formGroup = formBuilder.group({
        trxnType:['Sell',Validators.required],
        wtInCts:['',Validators.compose([Validators.min(0),Validators.required])],     //,Validators.min(0),Validators.required
        ratePerCts:['',Validators.compose([Validators.min(0),Validators.required])],
        discount:['0',Validators.compose([Validators.min(0),Validators.required])],  //RB1002
        trxnDate:['',Validators.required],
        dueDays:[this.financeSettings.dueDays,Validators.compose([Validators.min(0),Validators.required])],
        taxChecked:['false'],
        taxValue:[''],
        remarks:[''],
        invoiceId:[''],
        trxCurrency:['INR',Validators.required],  //RB2101
        trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])],
        brokerID:[],
        brokerageRate:['',Validators.compose([Validators.min(0),Validators.required])]
      });
    }  
        
    this.trxnType = this.formGroup.controls['trxnType'];
    this.wtInCts = this.formGroup.controls['wtInCts'];
    this.ratePerCts = this.formGroup.controls['ratePerCts'];
    this.discount = this.formGroup.controls['discount'];
    this.trxnDate = this.formGroup.controls['trxnDate'];
    this.dueDays = this.formGroup.controls['dueDays'];
    this.taxChecked = this.formGroup.controls['taxChecked'];
    this.taxValue = this.formGroup.controls['taxValue'];
    this.remarks = this.formGroup.controls['remarks'];
    this.trxCurrency = this.formGroup.controls['trxCurrency'];
    this.trxCurrencyRate = this.formGroup.controls['trxCurrencyRate'];
    this.invoiceId = this.formGroup.controls['invoiceId'];
    this.brokerID = this.formGroup.controls['brokerID'];
    this.brokerageRate = this.formGroup.controls['brokerageRate'];
    
    this.bizTrx = new BusinessTransaction();
    
  //  console.log("BusinessTransactionPage for PartyID : " + this.bizTrx.trxPartyID + "and PartyCategory is : "+ this.pBusinessContact.category);
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad BusinessTransactionPage');
    if(!checkIfValid(this.user))
    {
      alert("Invalid userID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      this.viewCtrl.dismiss();
    }
  }

  ionViewWillEnter() 
  {
    console.log('ionViewWillEnter BusinessTransactionPage');
    this.initializeData();
    {
      this.bizTrx = {} as BusinessTransaction;
    
      this.bizTrx.trxPartyID = this.pBusinessContact.id; 
      this.bizTrx.trxPartyName = this.pBusinessContact.fullName; 
  
      this.bizTrx.trxTaxRate = {} as TaxRate;
      this.bizTrx.trxTaxRate.rateCGST = this.financeSettings.cGSTRate;
      this.bizTrx.trxTaxRate.rateSGST = this.financeSettings.sGSTRate;
      this.bizTrx.trxTaxRate.rateIGST = this.financeSettings.iGSTRate;      
    }  
    
  }

  ionViewWillLeave() 
  {
    console.log('ionViewWillLeave BusinessTransactionPage');
    
  }

  initializeData()
  {
    console.log("Welcome to BusinessTransactionPage::initializeData() ");
    this.entityProvider.getAllBusinessContactList(this.user)
    .then(contactList => {
      this.brokerList = contactList.filter(contact => contact.category === myConstants.partyCategory.BROKER);
    });
  }

  goToSummaryPage() {
    this.viewCtrl.dismiss();
    console.log("BusinessTransactionPage dismiss");
  }

  saveTransaction()
  {
    console.log("Inside businessTransaction::saveTransaction");
    if(this.network.type === 'none')
    {
      alert('The internet connection appears to be offline! Please check your connection');
    }
    else
    {
      if(this.setBusinessTransaction())
      { 
        if(checkIfValid(this.user))
        {
          let loading: Loading;
          loading = this.loadingCtrl.create();
          loading.present();
    
          this.bizTrx.userID = this.user.ID;
          this.bizTrx.customerID = this.user.customerID;
        
          console.log("this.bizTrx.trxRemarks : " + this.bizTrx.trxRemarks);
          this.transactionProvider.createBusinessTransaction(this.user,this.bizTrx,null)
          .then(data => {
            console.log("Inserted: " + data);
            loading.dismiss();
            this.viewCtrl.dismiss();
          }).catch(e => console.log("Error from createBusinessTransaction:" + e));
        }
        else
        {
          alert("Invalid userID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
          this.viewCtrl.dismiss();
        }
      }
    }
  }

  setBusinessTransaction(): boolean
  { 
    console.log("Welcome to BusinessTranactionPage::setBusinessTransaction() ");

    this.bizTrx.trxDate =  this.trxnDate.value;
    let bizTrxDate = new Date(this.bizTrx.trxDate);
   
    this.bizTrx.trxType = <string> this.trxnType.value;
    this.bizTrx.trxParentID=""; //autogenerated for child trx
    
    this.bizTrx.confidentialityLevel = this.confidentialityLevel;
    //Invoice ID
    if(this.pBusinessContact.category === myConstants.partyCategory.SUPPLIER)
    {
      this.bizTrx.trxInvoiceID = <string> this.invoiceId.value;
    }
    else
    {
      let now = new Date();
     // this.bizTrx.trxInvoiceID = <string> ('LS/'+ bizTrxDate.getDate() + '-' + (bizTrxDate.getMonth()+1) + '-' + bizTrxDate.getFullYear() + '/' + now.getHours() + ':' + ((now.getMinutes()<10?'0':'') + now.getMinutes()) + ':' + ((now.getSeconds()<10?'0':'') + now.getSeconds()));
     this.bizTrx.trxInvoiceID = <string>  ('LS/' + (getDateString(bizTrxDate)) + '/' + (getTimeString(now)));
      console.log("INVOICE ID : " + this.bizTrx.trxInvoiceID);
    }
    
    
    //trxCurrency : string, //from user
    this.bizTrx.trxCurrency = <string> this.trxCurrency.value;
    //trxCurrencyRate : number, //from app settings or from user
    if(this.bizTrx.trxCurrency === myConstants.foreignCurrency)
    {
      this.bizTrx.trxCurrencyRate = <number> parseFloat(this.trxCurrencyRate.value);
    }
    else
    {
      this.bizTrx.trxCurrencyRate = this.financeSettings.exchangeRate; 
    }

    this.calculateTax();
    this.bizTrx.trxDueDays = <number> parseInt(this.dueDays.value);

    //remark
    this.bizTrx.trxRemarks = <string> this.remarks.value;

    //<Date>
    let dueDate:Date = new Date(this.bizTrx.trxDate);
    dueDate.setTime((dueDate).getTime()+(this.bizTrx.trxDueDays*24*60*60*1000));
    this.bizTrx.trxDueDate = getDateString(dueDate);
    
    this.bizTrx.trxROIForEarlyPayment=this.pBusinessContact.ROIForEarlyPayment; //from app settings
    this.bizTrx.trxROIForLatePayment=this.pBusinessContact.ROIForLatePayment;//from app settings
    this.bizTrx.trxStatus=myConstants.bizTrxStatus.OPEN; //'Open' or 'Closed'

    this.bizTrx.trxBrokerID = <string> this.brokerID.value;
    for(let i=0;i<this.brokerList.length;i++)
    {
      if(this.brokerList[i].id === this.bizTrx.trxBrokerID)
      {
        this.bizTrx.trxBrokerName = this.brokerList[i].fullName;
      }
    }
    this.bizTrx.trxBrokerageRate = <number> parseFloat(this.brokerageRate.value);
    this.bizTrx.trxBrokerageAmount = round((this.bizTrx.trxBrokerageRate*this.bizTrx.trxValue)/100.0,2);
    
    this.bizTrx.trxValue = round((this.bizTrx.trxRate*this.bizTrx.trxWeight),2);
    
    
    this.bizTrx.consoleLog();
    return true;
  }

  calculateTax()
  {
    console.log("Inside calculateTax ");
    if(!this.bizTrx)
    {
      return;
    }
    this.bizTrx.trxTaxRate = {} as TaxRate;
    this.bizTrx.trxTaxRate.rateCGST = this.financeSettings.cGSTRate;
    this.bizTrx.trxTaxRate.rateSGST = this.financeSettings.sGSTRate;
    this.bizTrx.trxTaxRate.rateIGST = this.financeSettings.iGSTRate;

    this.bizTrx.trxTaxAmount = {} as TaxAmount;
    this.bizTrx.trxTaxAmount.amtCGST = 0;
    this.bizTrx.trxTaxAmount.amtSGST = 0;
    this.bizTrx.trxTaxAmount.amtIGST = 0;
    this.bizTrx.trxTaxAmount.totalAmount = 0;    
    
    //if(this.taxChecked.value === true)
    {
      console.log("calculateTax:: stateRegion -> " + this.pBusinessContact.stateRegion);
          
      if(this.pBusinessContact.stateRegion === "Maharashtra - (27)")
      {
        this.bizTrx.trxTaxAmount.amtCGST = round(((this.bizTrx.trxValue*this.bizTrx.trxTaxRate.rateCGST)/100.0),2);
        this.bizTrx.trxTaxAmount.amtSGST = round(((this.bizTrx.trxValue*this.bizTrx.trxTaxRate.rateSGST)/100.0),2);
      }
      else
      {
        this.bizTrx.trxTaxAmount.amtIGST = round(((this.bizTrx.trxValue*this.bizTrx.trxTaxRate.rateIGST)/100.0),2);
      }
      this.bizTrx.trxTaxAmount.totalAmount = round((+this.bizTrx.trxTaxAmount.amtCGST + +this.bizTrx.trxTaxAmount.amtSGST + +this.bizTrx.trxTaxAmount.amtIGST),2);
    }
    this.bizTrx.trxGrossAmount = round((+this.bizTrx.trxValue + +this.bizTrx.trxTaxAmount.totalAmount),2);
    
    if(this.bizTrx.trxWeight)
    {
      this.bizTrx.trxGrossRate = round((this.bizTrx.trxGrossAmount/this.bizTrx.trxWeight),2);
    }
    else
    {
      this.bizTrx.trxGrossRate = this.bizTrx.trxRate;
    }
    
    
  }

  isCurrencySelected()
  {
    if(this.trxCurrency.value === myConstants.localCurrency)
    {
      this.exchangeRate = 1.0;
    }
    else{
      this.exchangeRate = this.financeSettings.exchangeRate;
    }
  }

  addBusinessItems()
  {
    console.log("Welcome to addBusinessItems");
    //this.bizTrx.trxType = <string> this.trxnType.value;

    //if(this.bizTrx.trxType === myConstants.trxType.BUY)
    if(this.pBusinessContact.category === myConstants.partyCategory.SUPPLIER)
    {
      const modal = this.modalCtrl.create('BusinessTransactionItemsPage',{User:this.user,businessTransaction:this.bizTrx}); //RB2101
      modal.present();
      modal.onDidDismiss((data) => {
        console.log("BusinessTransactionItemsPage closed");
        console.log("Calcualted trxQty " + this.bizTrx.trxWeight);
        console.log("Calcualted trxRate " + this.bizTrx.trxRate);
        console.log("Calcualted trxValue " + this.bizTrx.trxValue);    
        
        this.wtInCts.setValue(this.bizTrx.trxWeight);
        this.ratePerCts.setValue(this.bizTrx.trxRate);  
        this.calculateTax();          
      });
    }

  }
  
}

