import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessTransactionPage } from './businessTransaction';

@NgModule({
  declarations: [
    BusinessTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessTransactionPage),
  ],
})
export class BusinessTransactionPageModule {}
