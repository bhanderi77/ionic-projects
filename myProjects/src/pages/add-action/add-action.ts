import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Milestone } from '../../model/milestone';
import { Action } from '../../model/action';
import { Project } from '../../model/project';
import { myConstants } from '../../model/myConstants';

/**
 * Generated class for the AddActionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-action',
  templateUrl: 'add-action.html',
})
export class AddActionPage {

  //id:string;
  private milestone:Milestone;
  private project:Project;
  private action:Action;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,    
    private firebaseProvider:FirebaseProvider) 
  {
    this.action = {} as Action;
    this.action.description ="";
    this.action.owner = "";
    this.action.raisedDate = "";
    this.action.estimatedClosureDate = "";
    this.action.actualClosureDate = "";
    this.action.status = "";
    this.action.RAG = "";
    this.action.remarks = "";

    this.action.id = this.navParams.get('actionID');
    console.log("this.action.id " + this.action.id);
    
    this.project = {} as Project;
    this.project.id = navParams.get('projectID');
    this.milestone = {} as Milestone;
    this.milestone.id = this.navParams.get('milestoneID');

    if(this.action.id)
    {
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
     
      this.firebaseProvider.getAction(this.project.id,this.milestone.id,this.action.id)
      .then(data => {
          this.action = data;
          console.log("Action estimated Closure Date " + this.action.estimatedClosureDate);
          loading.dismiss();
      });
    }

    if(this.project.id)
    {
      this.firebaseProvider.getProject(this.project.id)
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
      });
      
      if(this.milestone.id)
      {
        this.firebaseProvider.getMilestone(this.project.id,this.milestone.id)
        .then(data => {
            this.milestone = data;
            console.log("Milestone Estimated Closure Date " + this.milestone.estimatedClosureDate);
        });
      }   
    }    
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddActionPage');
  }

  async saveAction()
  {
    console.log("Inside saveAction()");
    let today = new Date().toISOString();
    console.log("this.estimatedClosureDate " + this.action.estimatedClosureDate);
    console.log("today " + today);
    if(this.action.estimatedClosureDate < today)
    {
      alert("Estimated Closure Date can not be < today");
    }
    else if(this.action.estimatedClosureDate > this.milestone.estimatedClosureDate)
    {
      alert("Estimated Closure Date of ACTION can not be >= Estimated Closure Date of respective MILESTONE");
    }
    else
    {
      /*
      let a = {} as Action;
      a.description = this.description;
      let today = new Date();
      a.raisedDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-"+today.getDate();
      a.estimatedClosureDate = this.estimatedClosureDate;
      a.actualClosureDate = "";
      a.status = myConstants.Status.OPEN;
      a.owner = this.owner;
      a.RAG = myConstants.RAG.GREEN;
      a.remarks = ""; 
      */
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
    
      console.log("this.action.id " + this.action.id);
      if(this.action.id === "")
      {
        let today = new Date();
        this.action.raisedDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-"+today.getDate();
        this.action.status = myConstants.Status.OPEN;
        this.action.RAG = myConstants.RAG.GREEN;
       // this.action.remarks = "";
       
        console.log("Calling firebaseProvider.createAction");
        await this.firebaseProvider.createAction(this.project.id,this.milestone.id,this.action); 
        loading.dismiss();
        this.navCtrl.pop();  
      }
      else
      {
        console.log("Calling firebaseProvider.updateAction");
        console.log("this.risk.RAG " + this.action.RAG);
        await this.firebaseProvider.updateAction(this.project.id,this.milestone.id,this.action);
        loading.dismiss();
        this.navCtrl.pop();
      } 
    }
  }

}
