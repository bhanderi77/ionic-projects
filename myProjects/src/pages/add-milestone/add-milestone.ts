import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Milestone } from '../../model/milestone';
import { Project } from '../../model/project';
import { myConstants } from '../../model/myConstants';

/**
 * Generated class for the AddMilestonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-milestone',
  templateUrl: 'add-milestone.html',
})
export class AddMilestonePage {
  
  private description:string;
  private targetClosureDate:string;
  private estimatedClosureDate:string;
  private actualClosureDate:string;
  private completionPercentage:number;
  private RAG:string;
  private remarks:string;
  
  private project:Project;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,    
    private firebaseProvider:FirebaseProvider) {
      
      this.description ="";
      this.targetClosureDate ="";
      this.estimatedClosureDate ="";
      this.actualClosureDate = "";
      this.completionPercentage = 0;
      this.RAG = "";
      this.remarks = "";

      this.project = {} as Project;
      this.firebaseProvider.getProject(navParams.get('projectID'))
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
      });      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddMilestonePage');
  }

  async saveMilestone()
  {
    console.log("Inside saveMilestone()");
    console.log("project.endDate " + this.project.endDate);
    console.log("targetClosureDate " + this.targetClosureDate);
    console.log("estimatedClosureDate " + this.estimatedClosureDate);
    let today = new Date().toISOString();
    if(this.targetClosureDate < today)
    {
      alert("Target Closure Date can not be < today");
    }
    else if(this.targetClosureDate > this.project.endDate)
    {
      alert("Target Closure Date can not be GREATER than project End Date");
    }
    else
    {
      let m = {} as Milestone;
      m.description =  this.description;
      m.targetClosureDate = this.targetClosureDate;
      m.estimatedClosureDate = m.targetClosureDate;
      m.actualClosureDate = "";
      m.completionPercentage = 0;
      m.RAG = myConstants.RAG.GREEN;
      m.remarks = "";      
      
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
    
      await this.firebaseProvider.createMilestone(this.project.id,m); 
      loading.dismiss();
      this.navCtrl.pop();  
    }
  }
}
