import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddMilestonePage } from './add-milestone';

@NgModule({
  declarations: [
    AddMilestonePage,
  ],
  imports: [
    IonicPageModule.forChild(AddMilestonePage),
  ],
})
export class AddMilestonePageModule {}
