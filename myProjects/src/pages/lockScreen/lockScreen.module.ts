import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LockScreenPage } from './lockScreen';

@NgModule({
  declarations: [
    LockScreenPage,
  ],
  imports: [
    IonicPageModule.forChild(LockScreenPage),
  ],
})
export class LockScreenPageModule {}
