import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PartySummaryListPage } from './partySummaryList';
import { PipesModule } from '../../pipes/pipes.module'
import { ChartsModule } from 'ng2-charts'

@NgModule({
  declarations: [
    PartySummaryListPage
  ],
  imports: [
    ChartsModule,
    PipesModule,
    IonicPageModule.forChild(PartySummaryListPage),
  ],
})
export class PartySummaryListPageModule {}