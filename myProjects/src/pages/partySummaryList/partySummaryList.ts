import { Component, ViewChild } from '@angular/core';
import { ModalController, NavController, NavParams,Platform,IonicPage, Nav } from 'ionic-angular';
import { Loading,LoadingController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';
import { EntityProvider } from '../../providers/entity/entity';

import { AuthProvider } from '../../providers/auth/auth';
import { CallNumber } from '@ionic-native/call-number';
import { Contacts } from '@ionic-native/contacts';


//import { partyProfile, FinanceSummary,MySummary,number2text } from '../../model/interface'
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePipe } from '@angular/common';

import { FinanceSettings } from '../../model/referenceData';
import { BusinessContact } from '../../model/businessContact';
import { PartySummary, calculateBusinessSummary, BusinessSummary } from '../../model/partySummary';
import { myConstants } from '../../model/myConstants';
import { number2text } from '../../model/utility';
import { MySummary,FinanceSummary } from '../../model/financeTransaction';
import { User, checkIfValid } from '../../model/user';

import { Subscription } from 'rxjs';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';

pdfMake.vfs = pdfFonts.pdfMake.vfs;
@IonicPage()
@Component({
  selector: 'page-partySummaryList',
  templateUrl: 'partySummaryList.html'
})
export class PartySummaryListPage {

  private partyProfiles:Array<BusinessContact>;	
  private financeSettings: FinanceSettings;
  private partySummaryList:Array<PartySummary>;
  private filteredPartyList: Array<PartySummary>;
  private mySummary:MySummary;
  private user:User;
  private selectedTheme: String;
  private confidentialityLevel:number;
  private mySubscriptions:Subscription;
  private partyCategory: string;
  
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private transactionProvider: TransactionProvider,
              private settingsProvider: SettingsProvider,
              private entityProvider: EntityProvider,
              private authProvider: AuthProvider,
              private call : CallNumber,
              private platform: Platform,
              private file: File,
              private fileOpener: FileOpener,
              public datepipe: DatePipe,
              private contacts: Contacts,
              public popOverCtrl: PopoverController) 
  {    

    this.user = {} as User;
    this.user = this.navParams.get('User');
    this.partyCategory = this.navParams.get('partyCategory');
    this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    this.mySummary = {} as MySummary;
    this.mySummary.saleSummary = {} as FinanceSummary;
    this.mySummary.purchaseSummary = {} as FinanceSummary;
  //  this.bInvalidUser = false;
    this.partySummaryList = [];
    this.filteredPartyList = [];
   // this.themeSettings.getActiveTheme();
    console.log("platform is => " + this.platform._platforms);
  }
  
  ionViewWillEnter()
  {
    console.log("Welcome to PartySummaryListPage::ionViewWillEnter()");
    this.mySubscriptions = new Subscription();
    this.initializeData();
  }

  async initializeData()
  {
    if(checkIfValid(this.user) && this.authProvider.getAuthenticated())
    {
      this.financeSettings = await this.settingsProvider.getFinanceSettings(this.user);
      this.loadPartyDetails();  
    }    
  }

  ionViewWillLeave()
  {
    console.log("Welcome to PartySummaryListPage::ionViewWillLeave()");
    this.mySubscriptions.unsubscribe();
  }
  async callNumber(number:any):Promise<any>
  {
    try{
      await this.call.callNumber(String(number),true);
    }
    catch(e){
      console.error(e);
    }
  }

  ionViewDidLoad() 
  {
    console.log('HomePage::ionViewDidLoad');
    console.log('partyCategory' + this.partyCategory);
  }

  
  loadPartyDetails() 
  { 
    this.entityProvider.getAllBusinessContactList(this.user)
    .then(businessContactList => {
      console.log("Received businessContactList " + businessContactList.length);

      this.partyProfiles = [];
      this.partyProfiles = businessContactList;

      //Unsubscribe for BusinessTransaction & FinanceTransaction before this.partySummaryList = []
      this.partySummaryList = [];
      for(let i=0; i < businessContactList.length; i++)
      {
        let p = {} as PartySummary;
        p.businessSummary = {} as BusinessSummary;
        p.profile = {} as BusinessContact;
        p.profile = this.partyProfiles[i];
        console.log("this.partyProfiles[i].ID " + this.partyProfiles[i].id);

        p.businessSummary.bizTrxs = [];
        p.businessSummary.finTrxs = [];
        p.businessSummary.bizTrxSummary = [];        
        
        p.businessSummary.businessAmtTotalinForiegn = p.businessSummary.businessAmtTotalinLocal = 0;
        p.businessSummary.amtReceviedOrPaidinForiegn = p.businessSummary.amtReceviedOrPaidinLocal = 0;
        p.businessSummary.amtPendinginForiegn = p.businessSummary.amtPendinginLocal = 0;
        p.businessSummary.businessCurrency = "";
        p.businessSummary.interestRealizedinForiegn = p.businessSummary.interestRealizedinLocal = 0;
        p.businessSummary.interestUnRealizedinForiegn = p.businessSummary.interestUnRealizedinLocal = 0;        
       
        this.partySummaryList.push(p);
      }
      this.buildPartySummary();
      this.filterPartySummaryList();
    });
    
    
  }

  goToSummaryPage(p:PartySummary) {
    console.log('PartySummaryListPage::goToSummaryPage() : ' + p.profile.fullName);
    //this.navCtrl.push('PartySummaryPage',{User:this.user,pSummary: p, financeSettings: this.financeSettings});
    this.navCtrl.push('PartySummaryPage',{User:this.user,partyProfile:p.profile});
  }
  goToMySummary()
  {
    this.calculateMySummary();
    this.navCtrl.push('MySummaryPage',{User:this.user,mySummary: this.mySummary});
  }

  downloadPdf()
  {
    if(!checkIfValid(this.user))
    {
      alert("Invalid userID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      return;
    }
    if(this.partySummaryList.length < 1)
    {
      alert("No data !");
      return;
    }
    this.calculateMySummary();
    let today = new Date(); 
        
    function displayPartySummaryHeader(partySummary) 
    {
      let columns=[];
        
      if(partySummary.profile.category === myConstants.partyCategory.BUYER)
      {
        columns.push({text : partySummary.profile.fullName + ' : ' + partySummary.businessAmtTotalinLocal, color:'white',bold:true, fontSize : 15});
      }
      else if(partySummary.profile.category === myConstants.partyCategory.SUPPLIER)
      {
        columns.push({text : partySummary.profile.fullName + ' : ' + (partySummary.businessAmtTotalinLocal)*(-1), color:'white',bold:true, fontSize : 15});
      }
      return {
            alignment: 'center',
            table: {
             headerRows: 0,
             widths:['*'],
             body: [
               columns
               ],fillColor:'black'
             },
             layout: {
               fillColor: function (i, node) {
                 return (i  === 0) ? '#212121' : null;
               }               
             }           
        };
    }
    function displayPartySummaryFooter(partySummary) {
      let columns=[];
        
      if(partySummary.profile.category === myConstants.partyCategory.BUYER)
      {
        columns.push({text : partySummary.amtReceviedOrPaidinLocal , color:'green',bold:true, fontSize : 15},
        {text : partySummary.amtPendinginLocal, color:'red', bold:true, fontSize : 15}); 

        columns.push({text : partySummary.interestRealizedinLocal, color:'green',bold:true, fontSize : 15},
        {text : partySummary.interestUnRealizedinLocal, color:'red', bold:true, fontSize : 15}); 
      }
      else if(partySummary.profile.category === myConstants.partyCategory.SUPPLIER)
      {
        columns.push({text : partySummary.amtReceviedOrPaidinLocal*(-1) , color:'green',bold:true, fontSize : 15},
        {text : partySummary.amtPendinginLocal*(-1), color:'red', bold:true, fontSize : 15}); 

        columns.push({text : partySummary.interestRealizedinLocal*(-1) , color:'green',bold:true, fontSize : 15},
        {text : partySummary.interestUnRealizedinLocal*(-1), color:'red', bold:true, fontSize : 15}); 
      }
      return {
            alignment: 'center',
            table: {
             headerRows: 0,
             widths:['50%','50%'],
             body: [
               [columns[0],columns[1]],
               [columns[2],columns[3]],
               ],fillColor:'black'
             },
             layout: {
            /*
               fillColor: function (i, node) {
                 return (i  === 0) ? '#212121' : null;
               }  
            */             
             }           
        };
    } 
 
    
    function displayPartyList(partyList){
       let recs = [];
       for(var i=0 ; i < partyList.length ; i++){ 
         if(partyList[i].businessAmtTotalinLocal > 0)
         {
         recs.push(displayPartySummaryHeader(partyList[i]));
         recs.push(displayPartySummaryFooter(partyList[i]));  
         recs.push('\n'); 
         }       
       }
       return recs;
    }
       
   let datepipe:DatePipe = new DatePipe('en-US');
    let dd = {
        content: [
            {text: 'My Dashboard', bold:true ,alignment:'center' , fontSize : 20 },
            '\n',
            {text: 'Summary as on : ' + this.partySummaryList[0].sysDate.getDate()+'-'+(this.partySummaryList[0].sysDate.getMonth()+1)+'-'+this.partySummaryList[0].sysDate.getFullYear(), italic:true,alignment:'center' , fontSize : 10},
            //{text: 'Summary as on : ' + datepipe.transform(new Date().toISOString(),'medium'), italic:true,alignment:'center' , fontSize : 10},
            '\n',
            '\n',
            {
              alignment: 'center',
              columns: [
                {
                  text: 'SALE : ' + this.mySummary.saleSummary.amtTotal, bold:true ,alignment:'center' , fontSize : 15
                },            
                {
                  text: 'Received : ' + this.mySummary.saleSummary.amtReceviedOrPaid,alignment:'center',color:'green',fontSize : 15,bold:true
                },
                {
                  text: 'Remaining : ' + this.mySummary.saleSummary.amtPending,alignment:'center',color:'red',fontSize : 15,bold:true
                }
              ]
            },
            {
              alignment: 'center',
              columns: [
                {
                  text: 'PURCHASE : ' + this.mySummary.purchaseSummary.amtTotal*(-1), bold:true ,alignment:'center' , fontSize : 15
                },            
                {
                  text: 'Paid : ' + this.mySummary.purchaseSummary.amtReceviedOrPaid*(-1),alignment:'center',color:'green',fontSize : 15,bold:true
                },
                {
                  text: 'Pending : ' + this.mySummary.purchaseSummary.amtPending*(-1),alignment:'center',color:'red',fontSize : 15,bold:true
                }
              ]
            },            
            '\n',
            displayPartyList(this.partySummaryList)
        ]
    }
 
    let pdfObj = pdfMake.createPdf(dd);
    let date = today.getDate();
    let month = today.getMonth()+1;
    let year = today.getFullYear();
    let pdfName = 'Party List'+'_'+date+'_'+month+'_'+year+'.pdf';
    console.log('month : '+today.getMonth());
    //console.log("DD-MM-YYYY: " + this.datepipe.transform(this.pSummary.bizTrxSummary[0].bizTransaction.trxDueDate,'dd-MM-yyyy'))
    if (this.platform.is('cordova')) {
      pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory,pdfName, blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory+ pdfName, 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      pdfObj.download();
    }
    
  }

  openSettings()
  {
    const modal = this.modalCtrl.create('AppSettingsPage',{User:this.user,financeSettings:this.financeSettings});
    modal.present();    
  }

  buildPartySummary()
  {
    console.log("Inside buildPartySummary");
    if(checkIfValid(this.user))
    {
     // this.mySubscriptions.unsubscribe();
      for(let i=0;i<this.partySummaryList.length;i++)
      {
        this.mySubscriptions.add(this.transactionProvider.getBusinessTransactionsForPartyID(this.user,this.partySummaryList[i].profile.id).valueChanges()
        .subscribe(businessTransactionList => {
          console.log("Received businessTransactionList : " + businessTransactionList.length);
          this.partySummaryList[i].businessSummary.bizTrxs = [];
          if(businessTransactionList.length > 0)
          {
            for(let i=0;i<this.partySummaryList.length;i++)
            {
              console.log("Finding match for: " + businessTransactionList[0].trxPartyID + " " + this.partySummaryList[i].profile.id);
                
              if(this.partySummaryList[i].profile.id === businessTransactionList[0].trxPartyID ) 
              {
                console.log("businessTransactionList match found for ID: ");
                //this.partySummaryList[i].bizTrxs = [];
                this.partySummaryList[i].businessSummary.bizTrxs = businessTransactionList;
                //calculatePartySummary(this.partySummaryList[i],this.orgSettings.exchangeRate);
                break;
              }
            }
          }
          calculateBusinessSummary(this.partySummaryList[i].businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
        }));
      
        this.mySubscriptions.add(this.transactionProvider.getFinanceTransactionsForPartyID(this.user,this.partySummaryList[i].profile.id).valueChanges()
        .subscribe(financeTransactionList => {
          console.log("Received financeTransactionList : " + financeTransactionList.length);
          this.partySummaryList[i].businessSummary.finTrxs = [];
          if(financeTransactionList.length > 0)
          {
            for(let i=0;i<this.partySummaryList.length;i++)
            {
              console.log("Finding match for: " + financeTransactionList[0].trxPartyID + " " + this.partySummaryList[i].profile.id);
                
              if(this.partySummaryList[i].profile.id === financeTransactionList[0].trxPartyID ) 
              {
                console.log("financeTransactionList match found for ID: ");
                //this.partySummaryList[i].finTrxs = [];
                this.partySummaryList[i].businessSummary.finTrxs = financeTransactionList;
                //calculatePartySummary(this.partySummaryList[i],this.orgSettings.exchangeRate);
                break;
              }
            }
          }
          calculateBusinessSummary(this.partySummaryList[i].businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
        }));
      }
    }
    //setTimeout(3000);
    console.log("buildPartySummary:End");
  }

  calculateMySummary()
  {
    this.mySummary.saleSummary.Currency='';
    this.mySummary.saleSummary.amtTotal=0;
    this.mySummary.saleSummary.amtReceviedOrPaid=0;
    this.mySummary.saleSummary.amtPending=0;
    this.mySummary.saleSummary.interestRealized=0;
    this.mySummary.saleSummary.interestUnRealized=0;

    this.mySummary.purchaseSummary.Currency='';
    this.mySummary.purchaseSummary.amtTotal=0;
    this.mySummary.purchaseSummary.amtReceviedOrPaid=0;
    this.mySummary.purchaseSummary.amtPending=0;
    this.mySummary.purchaseSummary.interestRealized=0;
    this.mySummary.purchaseSummary.interestUnRealized=0;

    for(let i=0;i<this.partySummaryList.length;i++)
    {
      if(this.partySummaryList[i].profile.category === myConstants.partyCategory.BUYER)
      {
      this.mySummary.saleSummary.amtTotal += this.partySummaryList[i].businessSummary.businessAmtTotalinLocal;
      this.mySummary.saleSummary.amtReceviedOrPaid += this.partySummaryList[i].businessSummary.amtReceviedOrPaidinLocal;
      this.mySummary.saleSummary.amtPending += this.partySummaryList[i].businessSummary.amtPendinginLocal;
      this.mySummary.saleSummary.interestRealized += this.partySummaryList[i].businessSummary.interestRealizedinLocal;
      this.mySummary.saleSummary.interestUnRealized += this.partySummaryList[i].businessSummary.interestUnRealizedinLocal;
      }
      else if(this.partySummaryList[i].profile.category === myConstants.partyCategory.SUPPLIER)
      {
      this.mySummary.purchaseSummary.amtTotal += this.partySummaryList[i].businessSummary.businessAmtTotalinLocal;
      this.mySummary.purchaseSummary.amtReceviedOrPaid += this.partySummaryList[i].businessSummary.amtReceviedOrPaidinLocal;
      this.mySummary.purchaseSummary.amtPending += this.partySummaryList[i].businessSummary.amtPendinginLocal;
      this.mySummary.purchaseSummary.interestRealized += this.partySummaryList[i].businessSummary.interestRealizedinLocal;
      this.mySummary.purchaseSummary.interestUnRealized += this.partySummaryList[i].businessSummary.interestUnRealizedinLocal;      
      }     
    }    
  }

  onTitleClick() //Added by Rashmin - 09082018
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC) 
    && (this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova')))
    {
      let modal = this.modalCtrl.create('LockScreenPage');
        modal.present();
        modal.onDidDismiss((data) => {
          if(data === true)
          {
            this.switchConfidentialityLevel();
          }
          else
          {
            alert('Authentication Failed !!')
          }
        }); 
    }
    else //web flow
    {
      this.switchConfidentialityLevel();
    }    
  }

  switchConfidentialityLevel()
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC)
      && (true))//this.user.confidentialityLevel >= myConstants.confidentialityLevel.PUBLIC
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.CONFIDENTIAL;
    }
    else
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    }
    for(let i=0;i<this.partySummaryList.length;i++)
    {
      calculateBusinessSummary(this.partySummaryList[i].businessSummary,this.financeSettings.exchangeRate,this.confidentialityLevel);
    }
  }

  async loadOrgSettings() 
  {
   if(checkIfValid(this.user))
   {
    this.financeSettings = await this.settingsProvider.getFinanceSettings(this.user);
   } 
  }

  addParty() {
    console.log('addParty::partyName : ');
    console.log('addParty::phoneNumber : ');
    const modal = this.modalCtrl.create('AddPartyPage',{User:this.user,ID:""});
    modal.present();
    console.log("inside addParty method");
    modal.onDidDismiss((data) => {
      this.loadPartyList();
    });    
  }

  loadPartyList()
  {
    this.entityProvider.getAllBusinessContactList(this.user)
    .then(contactList => {
      this.partyProfiles = contactList;
    });
  }

  editOrDeletePartyProfile(p:BusinessContact){          // HB27/1/2018
    let name = p.fullName;
    let prompt = this.alertCtrl.create({
      title: name,
      buttons: [
        {
          text: 'Edit',
          handler : () => {
            this.editParty(p);
          }
        },

        {
          text: 'delete',
          handler : () => {
            this.deleteParty(p);
          }
        }
      ]
    });
    prompt.present();
  } 

  editParty(p:BusinessContact){    // HB27/1/2018
    console.log('Edit pressed');
    let modal = this.modalCtrl.create('AddPartyPage',{User:this.user,ID:p.id});
    modal.present();
    modal.onDidDismiss((data) => {
      this.loadPartyList();
    });
  }  // HB27/1/2018

  deleteParty(p:BusinessContact){    // HB28/1/2018
    console.log('Delete pressed');
    let name = p.fullName;
    let id = p.id;
    console.log("Party ID : " + id);
    let confirm = this.alertCtrl.create({
      title: 'Delete ' + name,
      message: 'Do you want to delete ' + name + '?',
      buttons: [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          handler: () => {
           /* this.databaseProvider.deleteSelectedParty(id).then(res=>{
              this.loadPartyDetails();
            })*/
          }
        }
      ]
    });
    confirm.present();
  }

  showPartyProfileDetails(profile)
  {
    const modal = this.modalCtrl.create('PartyListPage',{User:this.user,partyProfile:profile});
    modal.present();
  }

  openPopOver(event){
    let popover = this.popOverCtrl.create('PartySummaryListFilterPopoverPage');
    popover.present({
      ev: event
    });
    popover.onDidDismiss((category:string) => {
      console.log("partyCategory: " + category);
      this.partyCategory = category;
      this.filterPartySummaryList();
    });
  }

  filterButtonClick(){
    console.log("Show alert for filter PartySummaryListPage " );
    let alert = this.alertCtrl.create();
    alert.addInput({type: 'radio',
                  label: myConstants.partyCategory.BUYER,
                  value: myConstants.partyCategory.BUYER,
                  checked: false},
                  );
    alert.addInput({type: 'radio',
                  label: myConstants.partyCategory.SUPPLIER,
                  value: myConstants.partyCategory.SUPPLIER,
                  checked: false},
                  );
    alert.addInput({type: 'radio',
                  label: myConstants.partyCategory.BROKER,
                  value: myConstants.partyCategory.BROKER,
                  checked: false},
                  );
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        this.partyCategory = data;
        this.filterPartySummaryList();  
      }
    });
    alert.present();
  }

  filterPartySummaryList()
  {
    if(this.partyCategory === myConstants.partyCategory.BUYER)
    {
      this.filteredPartyList = this.partySummaryList.filter(partySummary => (partySummary.profile.category === myConstants.partyCategory.BUYER));
    }
    else if(this.partyCategory === myConstants.partyCategory.SUPPLIER)
    {
      this.filteredPartyList = this.partySummaryList.filter(partySummary => (partySummary.profile.category === myConstants.partyCategory.SUPPLIER));
    }
    else if(this.partyCategory === myConstants.partyCategory.BROKER)
    {
      this.filteredPartyList = this.partySummaryList.filter(partySummary => (partySummary.profile.category === myConstants.partyCategory.BROKER));
    }
    else
    {
      this.filteredPartyList = this.partySummaryList; 
    } 
  }
}

