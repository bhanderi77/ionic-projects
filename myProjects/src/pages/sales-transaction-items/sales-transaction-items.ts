import { Component,ViewChild } from '@angular/core';
import { NavController,AlertController,ViewController,ModalController,LoadingController } from 'ionic-angular';
import { IonicPage, Loading, NavParams,Slides,Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { EntityProvider } from '../../providers/entity/entity';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';
import { RateCardProvider } from '../../providers/rate-card/rate-card';

import { ItemType,ItemShape,ItemQualityLevel, ItemSieve } from '../../model/packagedItem';
import { FinanceSettings } from '../../model/referenceData';

import { SalesSummaryByTSSQ,SalesSummaryByTSS, SalesSummary } from '../../model/salesSummary';
//import { doInitialize,doRound,doTotal,doAverage } from '../../model/salesSummary';
import { SalesItem } from '../../model/salesItem';

import { RateCard } from '../../model/rateCard';


import { myConstants, round } from '../../model/myConstants';
import { getDateString,getTimeString } from '../../model/utility';

import { User, checkIfValid } from '../../model/user';

import { Subscription } from 'rxjs';
import { BusinessTransaction,BusinessItem } from '../../model/businessTransaction';
import { TaxRate } from '../../model/tax';
import { BusinessContact } from '../../model/businessContact';
import { Organisation } from '../../model/organisation';
import { MarketPrice } from '../../model/marketPrice';
/**
 * Generated class for the SalesTransactionItemsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sales-transaction-items',
  templateUrl: 'sales-transaction-items.html',
})
export class SalesTransactionItemsPage {
  private user:User; 
  private organisation:Organisation; 
  private fromEntity:string;
  private selectedTab:string;
  @ViewChild('slider') slider : Slides;
  private activeTabIndex :number;
  private bEdited:boolean;
  private financeSettings:FinanceSettings;

  private contactList:Array<BusinessContact>;
  private confidentialityLevel: number;

  private bizTransaction:BusinessTransaction;
  //private duePenaltyRate:number;
  private brokerageRate:number;
  private currencyExchangeRate:number;
  private trxDate:string;
  private trxValidityDays:number;
  private trxCurrency:string;
  private bInterState:boolean;  
  private applicableTaxRate:number;
  private salesSummaryByTSSList:Array<SalesSummaryByTSS>;
  private totalSalesSummary:SalesSummary;

  private mySubscriptions:Subscription;

  private itemTypeList:Array<ItemType>;
  private itemShapeList:Array<ItemShape>;
  private itemSieveList:Array<ItemSieve>;
  private itemQualityLevelList:Array<ItemQualityLevel>;
  

  constructor(public navCtrl: NavController, 
    private platform:Platform,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,    
    private viewCtrl:ViewController,
    private entityProvider: EntityProvider,
    private transactionProvider: TransactionProvider,
    private settingsProvider: SettingsProvider,
    private rateCardProvider:RateCardProvider,
    private network: Network) 
  {
    this.user = {} as User;
    this.organisation = {} as Organisation;
    this.user = navParams.get('User');
    this.fromEntity = "";
    this.selectedTab = "";
    this.financeSettings = {} as FinanceSettings;
    this.bizTransaction = new BusinessTransaction();
    
    this.salesSummaryByTSSList = [];
    this.totalSalesSummary = new SalesSummary();
    
    this.itemTypeList = [];
    this.itemShapeList = [];
    this.itemSieveList = [];
    this.itemQualityLevelList = [];
    this.contactList = [];
    this.trxDate = "";
    this.trxValidityDays = 0;
    this.trxCurrency = myConstants.localCurrency;
    this.brokerageRate = 0;
    this.applicableTaxRate = 0;

    this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    this.bEdited = false;
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad SalesTransactionItemsPage');
  }

  ionViewWillEnter()
  {
    console.log('ionViewWillEnter SalesTransactionItemsPage');
    this.initializeData();
    this.gotoTab(0);
    this.updateSegment();   
    
  }

  ionViewWillLeave() 
  {
    console.log('ionViewWillLeave SalesTransactionItemsPage');
    this.mySubscriptions.unsubscribe();
  }

  async initializeData()
  {
    console.log("Welcome to SalesTransactionItemsPage:initializeData()");
    let noOfCallbacks:number=0;
    this.mySubscriptions = new Subscription();
    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.setContent("Loading details..");
    loading.present();     
  
    //this.contactList = await this.entityProvider.getAllBusinessContactList(this.user);
    this.entityProvider.getAllBusinessContactList(this.user)
    .then(contactList => {
      this.contactList = contactList;
      /*
      noOfCallbacks--;
      if(noOfCallbacks <= 0)
      {
        loading.dismiss();
      }
      else
      {
        loading.setContent("Loading customer/broker details..");
      }*/
    });

    this.organisation = await this.entityProvider.getOrganisationDetails(this.user);
    loading.setContent("Loading settings..");
    this.financeSettings = await this.settingsProvider.getFinanceSettings(this.user);
    this.currencyExchangeRate = this.financeSettings.exchangeRate;
    loading.dismiss();

    this.fromEntity = this.navParams.get('fromEntity');
    
    if(this.fromEntity === 'QLIST')
    {
      this.bEdited = false;
      loading = this.loadingCtrl.create();
      loading.setContent("Loading transaction..");      
      loading.present();   
      
      noOfCallbacks = 2;
      let bizTrxID:string = this.navParams.get("bizTrxID");

      //this.bizTransaction = await this.transactionProvider.getBusinessTransaction(this.user,bizTrxID);
      this.transactionProvider.getBusinessTransaction(this.user,bizTrxID)
      .then(businessTransaction => {
        this.bizTransaction = businessTransaction;
        
        console.log("Received bizTransaction " + this.bizTransaction.trxItems.length);
      
        this.bizTransaction.confidentialityLevel = this.navParams.get('confidentialityLevel');
        this.brokerageRate = this.bizTransaction.trxBrokerageRate;
        this.trxDate = this.bizTransaction.trxDate;
        this.trxValidityDays = this.bizTransaction.trxValidityDays;

        this.trxCurrency = this.bizTransaction.trxCurrency;
        this.currencyExchangeRate = this.bizTransaction.trxCurrencyRate;

        this.applicableTaxRate = (+this.bizTransaction.trxTaxRate.rateCGST + +this.bizTransaction.trxTaxRate.rateSGST + this.bizTransaction.trxTaxRate.rateIGST);
        this.setDetailsAsPerPartyID(this.bizTransaction.trxPartyID);
        //this.calculateSummaryOfSaleable();

        noOfCallbacks--;
        if(noOfCallbacks <= 0)
        {
          loading.dismiss();
        }
        else
        {
          loading.setContent("Loading Sales Items..");
        }
      });
      
      
      this.transactionProvider.getSalesItemListForBizTrxID(this.user,bizTrxID)
      .then(salesItemList =>
      {
        
        this.salesSummaryByTSSList = [];
        let summaryTSSList:Array<SalesSummaryByTSS> = [];
        
        for(let i=0;i<salesItemList.length;i++)
        {
          let salesItem = new SalesItem();
          salesItem.init(salesItemList[i]);
          //console.log("Reading salesItemList[" + i + "]");
          console.log(salesItem.itemType.id + " " + salesItem.itemShape.id + " " + salesItem.itemSieve.id + " " + salesItem.itemQualityLevel.id);
          summaryTSSList = [];
          summaryTSSList = this.salesSummaryByTSSList.filter(summary => (summary.itemShape.id === salesItem.itemShape.id)
                                                                    && (summary.itemSieveMin.id === salesItem.itemSieve.id));
          if(summaryTSSList.length === 0)
          {
            //console.log("Creating new SalesSummaryByTSS");
            let salesSummaryByTSS = new SalesSummaryByTSS();
            salesSummaryByTSS.itemType = salesItem.itemType;
            salesSummaryByTSS.itemShape = salesItem.itemShape;
            salesSummaryByTSS.itemSieveMin = salesItem.itemSieve;
            salesSummaryByTSS.itemSieveMax = this.rateCardProvider.getNextSieve(salesItem.itemSieve);
            salesSummaryByTSS.salesItemList = [];
            salesSummaryByTSS.salesItemList.push(salesItem);            
            this.salesSummaryByTSSList.push(salesSummaryByTSS);
          }
          else if(summaryTSSList.length === 1)
          {
            console.log("Adding to SalesSummaryByTSS");
            summaryTSSList[0].salesItemList.push(salesItem);
          }        
        }
        this.calculateSummaryOfSaleable();

        noOfCallbacks--;
        if(noOfCallbacks <= 0)
        {
          loading.dismiss();
        }
        else
        {
          loading.setContent("Loading transaction..");
        }
      });//end of getSalesItemListForBizTrxID
    }//end of if(this.fromEntity === 'QLIST')
    else
    {
      this.bEdited = true;
      noOfCallbacks = 1;
      this.trxDate = getDateString(new Date());
      this.trxCurrency = myConstants.localCurrency;
      this.currencyExchangeRate = this.financeSettings.exchangeRate;
      this.brokerageRate = 0;

      this.bizTransaction.trxDueDays = myConstants.DEFALUT.DUE_DAYS;
      this.bizTransaction.trxBrokerageRate = this.brokerageRate;
      this.bizTransaction.trxCurrency = this.trxCurrency;
      this.bizTransaction.trxCurrencyRate = this.currencyExchangeRate;

      if(this.fromEntity === 'PARTY')
      {
        let c:BusinessContact = this.navParams.get('partyProfile');
        console.log("got partyProfile" + c.fullName);
        this.setDetailsAsPerPartyID(c.id);
        this.bizTransaction.confidentialityLevel = this.navParams.get('confidentialityLevel');
      }
    }   
    
    
    this.mySubscriptions.add(this.rateCardProvider.getRateCardList().subscribe(rcList => {
      console.log("Received rcList " + rcList.length);
      if(rcList.length)
      {
        this.itemTypeList = this.rateCardProvider.itemTypeList;
        this.itemShapeList = this.rateCardProvider.itemShapeList;
        this.itemSieveList = this.rateCardProvider.itemSieveList;
        this.itemQualityLevelList = this.rateCardProvider.itemQualityLevelList;
        this.calculateValueFactor();
      }      
    }));
  } 
  
  
  async addToSalesSummaryByTSSList(itemTypeID:string)
  {
    console.log('Welcome to SalesTransactionItemsPage:addToSalesSummaryByTSSList() ' + itemTypeID);
    this.bEdited = true;
    let salesSummaryByTSS = new SalesSummaryByTSS();
    salesSummaryByTSS.itemType = await this.settingsProvider.getItemType(this.user,itemTypeID);
    salesSummaryByTSS.itemShape = {} as ItemShape;
    salesSummaryByTSS.itemSieveMin = {} as ItemSieve;
    salesSummaryByTSS.itemSieveMax = {} as ItemSieve;
    
    //salesSummaryByTSS.salesSummary = {} as SalesSummary;
    //doInitialize(salesSummaryByTSS.salesSummary);
    salesSummaryByTSS.salesItemList = [];
    
    this.selectShape(salesSummaryByTSS);
  }

  removeFromSalesSummaryBySSList(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log('Welcome to SalesTransactionItemsPage:removeFromSalesSummaryBySSList() ');
    this.bEdited = true;
    this.salesSummaryByTSSList.splice(this.salesSummaryByTSSList.indexOf(salesSummaryByTSS),1);
    this.calculateValueFactor();
  //  this.calculateSalesSummaryByTSS(salesSummaryByTSS);
    this.calculateSummaryOfSaleable();                                                
  }

  selectShape(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Show alert for Shape " + this.itemShapeList.length);
    let alert = this.alertCtrl.create();
    alert.setTitle("Select Shape");
    for(let i=0; i<this.itemShapeList.length;i++)
    {
      alert.addInput({type: 'radio',
                    label: this.itemShapeList[i].name,
                    value: this.itemShapeList[i].id,
                    checked: false});
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        let selectedShapeList:Array<ItemShape> = [];
        selectedShapeList = this.itemShapeList.filter(itemShape => itemShape.id === data);
        if(selectedShapeList.length === 1)
        {
          salesSummaryByTSS.itemShape = selectedShapeList[0];
        }
        this.selectSieve(salesSummaryByTSS);    
      }
    });
    alert.present();
  }

  selectSieve(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Show alert for Sieve " + this.itemSieveList.length);
    let alert = this.alertCtrl.create();
    alert.setTitle("Select Sieve");
    let itemSieveList = this.itemSieveList.filter(itemSieve => itemSieve.itemTypeID === salesSummaryByTSS.itemType.id);
    for(let i=0; (i<itemSieveList.length-1);i++)
    {
      if(itemSieveList[i].itemTypeID === salesSummaryByTSS.itemType.id)
      {
        alert.addInput({type: 'radio',
                    label: "+"+itemSieveList[i].name+" -"+itemSieveList[i+1].name,
                    value: itemSieveList[i].id,
                    checked: false});
      }        
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        let selectedSieveList:Array<ItemSieve> = [];
        selectedSieveList = itemSieveList.filter(itemSieve => itemSieve.id === data);
        if(selectedSieveList.length === 1)
        {
          salesSummaryByTSS.itemSieveMin = selectedSieveList[0];
          salesSummaryByTSS.itemSieveMax = this.rateCardProvider.getNextSieve(salesSummaryByTSS.itemSieveMin);
        }
    
        for(let i=0;i<this.itemQualityLevelList.length;i++)
        {
          if(this.itemQualityLevelList[i].itemTypeID === salesSummaryByTSS.itemType.id)
          {
            let salesItem = new SalesItem();
            salesItem.itemType = salesSummaryByTSS.itemType;
            salesItem.itemShape = salesSummaryByTSS.itemShape;
            salesItem.itemSieve = salesSummaryByTSS.itemSieveMin;
            salesItem.itemQualityLevel = this.itemQualityLevelList[i];
            if(this.setCostAndPriceForSalesItem(salesItem))
            {
              console.log("Pushing into salesSummaryByTSS.salesItemList");
              salesSummaryByTSS.salesItemList.push(salesItem);  
            }            
          }            
        }
        this.salesSummaryByTSSList.push(salesSummaryByTSS); 
        this.calculateSummaryOfSaleable();       
      }
    });
    alert.present();
  }

  setCostAndPriceForSalesItem(salesItem:SalesItem):boolean
  {
    if(this.setCostForSalesItem(salesItem))
    {
      if(this.setPriceForSalesItem(salesItem))
      {
        return true;
      }      
    }
    return false;
  }

  /*
  getValidityDays():number
  {
    console.log("Welcome to SalesTransactionItemsPage::getValidityDays()");
   
    let validityDays:number=0;
    let today: Date = (new Date());
    let trxDate: Date = new Date(this.trxDate);
    let duration: number = trxDate.valueOf() - today.valueOf();
    validityDays = round((duration / (1000 * 3600 * 24)),0); 
    console.log("Diff: trxDate " + trxDate.toISOString() + " today: " + today.toISOString());
    console.log("Diff: duration " + duration + " Days: " + validityDays);
    return validityDays;
  }
  */
  
  setCostForSalesItem(salesItem:SalesItem):boolean
  {
    console.log("Welcome to SalesTransactionItemsPage::setCostForSalesItem()");

    let rateCard = {} as RateCard;
    rateCard = this.rateCardProvider.getRateCardByID(salesItem.itemType.id,salesItem.itemShape.id,salesItem.itemSieve.id,salesItem.itemQualityLevel.id);
    if(rateCard)
    {
      //Set Cost
      salesItem.avgPurchaseCost = round(rateCard.avgPurchaseCost,2);
      salesItem.avgInterestCost = round(rateCard.avgInterestCost,2);
      salesItem.avgMfgCost = round(rateCard.avgMfgCost,2);
      salesItem.avgGrossCost = round(rateCard.avgGrossCost,2);
      console.log("rateCard.avgInterestCost " + rateCard.avgInterestCost);
      console.log("rateCard.avgMfgCost " + rateCard.avgMfgCost);
      
      let duePenaltyRate:number = round(((((+this.bizTransaction.trxDueDays + +this.trxValidityDays))/30.0)*(+this.bizTransaction.trxROIForLatePayment)),10);
      
      salesItem.avgGrossCostAtDueTerm = round(((salesItem.avgGrossCost)*((duePenaltyRate/100) + 1)),2);
      
      salesItem.doTotal(2);
      salesItem.setCurrency(this.trxCurrency,this.currencyExchangeRate);
      return true;
    }//end of if(rateCard)
    else
    {
      console.log("returning false");
      return false;
    }
  }

  setPriceForSalesItem(salesItem:SalesItem):boolean
  {
    console.log("Welcome to SalesTransactionItemsPage::setCostAndPriceForSalesItem()");

    let rateCard = {} as RateCard;
    rateCard = this.rateCardProvider.getRateCardByID(salesItem.itemType.id,salesItem.itemShape.id,salesItem.itemSieve.id,salesItem.itemQualityLevel.id);
    if(rateCard)
    {
      let duePenaltyRate:number = round(((((+this.bizTransaction.trxDueDays + +this.trxValidityDays))/30.0)*(+this.bizTransaction.trxROIForLatePayment)),10);
      
      
      //Set Price
      salesItem.avgPriceRate = round(rateCard.avgMarketRate,2);    
      salesItem.avgSalePrice = round((salesItem.avgPriceRate*((duePenaltyRate/100) + 1)),2);
      salesItem.calculateTax(this.bInterState);
      salesItem.avgGrossPrice = round((salesItem.avgPriceRate + +salesItem.avgSalesTax.totalAmount),2);
      salesItem.avgSalesBrokerage = round((salesItem.avgSalePrice*(this.bizTransaction.trxBrokerageRate)),2);
      salesItem.avgNetPrice = round((salesItem.avgSalePrice - salesItem.avgSalesBrokerage),2);
      if(salesItem.avgGrossCostAtDueTerm > 0)
      {
        salesItem.avgProfit = round((salesItem.avgNetPrice - salesItem.avgGrossCostAtDueTerm),2);
        salesItem.avgProfitMargin = round((salesItem.avgProfit/salesItem.avgGrossCostAtDueTerm)*100,2);
      }
      else if(salesItem.avgGrossCost > 0)
      {
        salesItem.avgProfit = round((salesItem.avgNetPrice - salesItem.avgGrossCost),2);
        salesItem.avgProfitMargin = round((salesItem.avgProfit/salesItem.avgGrossCost)*100,2);
      }
      salesItem.doTotal(2);
      salesItem.setCurrency(this.trxCurrency,this.currencyExchangeRate);
      return true;
    }//end of if(rateCard)
    else
    {
      console.log("returning false");
      return false;
    }
  }

  addSalesItems(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Welcome to SalesTransactionItemsPage:addSalesItems() " + salesSummaryByTSS.salesItemList.length);
    this.bEdited = true;
    for(let i=0;i<this.itemQualityLevelList.length;i++)
    {
      let salesItemList:Array<SalesItem> = [];
      salesItemList = salesSummaryByTSS.salesItemList.filter(salesItem => salesItem.itemQualityLevel.id === this.itemQualityLevelList[i].id);
      if(salesItemList.length === 0)
      {//add blank entry
        let salesItem = new SalesItem();
        salesItem.itemType = salesSummaryByTSS.itemType;
        salesItem.itemShape = salesSummaryByTSS.itemShape;
        salesItem.itemSieve = salesSummaryByTSS.itemSieveMin;
        salesItem.itemQualityLevel = this.itemQualityLevelList[i];
        if(this.setCostAndPriceForSalesItem(salesItem))
        {//Found in RateCard i.e. available for sale
          console.log("Pushing into salesSummaryByTSS.salesItemList");
          salesSummaryByTSS.salesItemList.push(salesItem);
        }        
      }                  
    }
    const modal = this.modalCtrl.create('AddPolishPage',{User:this.user,fromEntity:"R-WP",qualityLevelList:this.itemQualityLevelList,salesItemList:salesSummaryByTSS.salesItemList});
    modal.present();
    modal.onDidDismiss(() => {
      //this.calculateSalesItem(salesSummaryByTSS.salesItemList);
      this.calculateValueFactor();
      this.calculateSalesSummaryByTSS(salesSummaryByTSS);
      this.calculateSummaryOfSaleable();  
    }); 
    //polishedItemsSummary.polishedItemList
  }

  calculateValueFactor()
  {
    console.log("Welcome to SalesTransactionItemsPage:calculateValueFactor() ");
    let totalMarketValue:number=0,totalSaleableWeight:number=0,avgMarketRate:number=0;
    let rateCard = {} as RateCard;
        
    for(let i=0;i<this.salesSummaryByTSSList.length;i++)
    {
      for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
      {
        rateCard = this.rateCardProvider.getRateCardByID(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id,this.salesSummaryByTSSList[i].salesItemList[j].itemShape.id,this.salesSummaryByTSSList[i].salesItemList[j].itemSieve.id,this.salesSummaryByTSSList[i].salesItemList[j].itemQualityLevel.id);
        if(rateCard)
        {
          //Set Cost
          if(this.trxCurrency === myConstants.localCurrency)
          {
            totalMarketValue += round((rateCard.avgMarketRate*this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight),2);
          }
          else if(this.trxCurrency === myConstants.foreignCurrency)
          {
            totalMarketValue += round((rateCard.avgMarketRate*this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight),2);
          }
          totalSaleableWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
        }        
      }
    }
    
    totalSaleableWeight = round(totalSaleableWeight,3);
    if(totalSaleableWeight > 0)
    {
      if(this.trxCurrency === myConstants.localCurrency)
      {
        avgMarketRate = round(totalMarketValue/totalSaleableWeight,2);
      }
      else if(this.trxCurrency === myConstants.foreignCurrency)
      {
        avgMarketRate = round(totalMarketValue/totalSaleableWeight,2);
      }
      
      if(avgMarketRate > 0)
      {
        for(let i=0;i<this.salesSummaryByTSSList.length;i++)
        {
          for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
          {
            rateCard = this.rateCardProvider.getRateCardByID(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id,this.salesSummaryByTSSList[i].salesItemList[j].itemShape.id,this.salesSummaryByTSSList[i].salesItemList[j].itemSieve.id,this.salesSummaryByTSSList[i].salesItemList[j].itemQualityLevel.id);
            if(rateCard)
            {
              if(this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight > 0)
              {
                this.salesSummaryByTSSList[i].salesItemList[j].valueFactor = round(rateCard.avgMarketRate/avgMarketRate,10);
              }
              else
              {
                this.salesSummaryByTSSList[i].salesItemList[j].valueFactor = 0;
              }
            }
          }
        }
      }//end of if(avgMarketRate > 0)   
    }//end of if(totalSaleableWeight > 0)    
  }
  
  calculateSalesItem(salesItem:SalesItem)
  {
    console.log("Welcome to SalesTransactionItemsPage:calculateSalesItem()");
    if(salesItem)
    {
      if(this.trxCurrency === myConstants.localCurrency)
      {
        salesItem.trxDate = (this.trxDate);
        salesItem.trxValidityDays = this.trxValidityDays;
        salesItem.setCurrency(this.trxCurrency,this.currencyExchangeRate);
        this.setCostForSalesItem(salesItem);
        //salesItem.avgGrossCostAtDueTerm = round(((salesItem.avgGrossCost)*((this.duePenaltyRate/100) + 1)),2);
        console.log("salesItem.avgInterestCost " + salesItem.avgInterestCost);
        console.log("salesItem.avgMfgCost " + salesItem.avgMfgCost);
        console.log("salesItem.avgPriceRate " + salesItem.avgPriceRate);
        console.log("salesItem.avgSalePrice " + salesItem.avgSalePrice);
   
        //this.duePenaltyRate = round(((this.bizTransaction.trxDueDays/30.0)*(+this.bizTransaction.trxROIForLatePayment)),10);
        
        //Set Price
        salesItem.trxDueDays = +this.bizTransaction.trxDueDays;
        salesItem.trxROIForLatePayment = +this.bizTransaction.trxROIForLatePayment;
        salesItem.trxBrokerageRate = this.bizTransaction.trxBrokerageRate;
        
        salesItem.taxRate.rateCGST = this.bizTransaction.trxTaxRate.rateCGST;
        salesItem.taxRate.rateSGST = this.bizTransaction.trxTaxRate.rateSGST;
        salesItem.taxRate.rateIGST = this.bizTransaction.trxTaxRate.rateIGST;
        
        /*
        salesItem.avgSalePrice = round((salesItem.avgPriceRate*(+this.duePenaltyRate + 1)),2);
        console.log("salesItem.avgPriceRate " + salesItem.avgPriceRate);
        console.log("salesItem.avgSalePrice " + salesItem.avgSalePrice);
   
        salesItem.calculateTax(true);
        console.log("salesItem.avgSalesTax " + salesItem.avgSalesTax.totalAmount);
        salesItem.avgGrossPrice = round((+salesItem.avgSalePrice + +salesItem.avgSalesTax.totalAmount),2);
        salesItem.avgSalesBrokerage = round((salesItem.avgSalePrice*(this.bizTransaction.trxBrokerageRate)),2);
        salesItem.avgNetPrice = round((salesItem.avgSalePrice - salesItem.avgSalesBrokerage),2);
        if(salesItem.avgGrossCostAtDueTerm > 0)
        {
          salesItem.avgProfit = round((salesItem.avgNetPrice - salesItem.avgGrossCostAtDueTerm),2);
          salesItem.avgProfitMargin = round((salesItem.avgProfit/salesItem.avgGrossCostAtDueTerm)*100,2);
        }
        else if(salesItem.avgGrossCost > 0)
        {
          salesItem.avgProfit = round((salesItem.avgNetPrice - salesItem.avgGrossCost),2);
          salesItem.avgProfitMargin = round((salesItem.avgProfit/salesItem.avgGrossCost)*100,2);
        }
        */
        salesItem.avgSalesGain = 0;
  
        salesItem.qty = 0;
        salesItem.doTotal(2);
        salesItem.doAverage(2);
        salesItem.doRound(2);        
      }
      else if(this.trxCurrency === myConstants.foreignCurrency)
      {
        salesItem.trxDate = (this.trxDate);
        salesItem.trxValidityDays = this.trxValidityDays;
        salesItem.setCurrency(this.trxCurrency,this.currencyExchangeRate);
        this.setCostForSalesItem(salesItem);
        //salesItem.avgGrossCostAtDueTerm = round(((salesItem.avgGrossCost)*((this.duePenaltyRate/100) + 1)),2);
        console.log("salesItem.avgPriceRate " + salesItem.avgPriceRate);
        console.log("salesItem.avgSalePrice " + salesItem.avgSalePrice);
   
        //Set Price
        salesItem.trxDueDays = +this.bizTransaction.trxDueDays;
        salesItem.trxROIForLatePayment = +this.bizTransaction.trxROIForLatePayment;
        salesItem.trxBrokerageRate = this.bizTransaction.trxBrokerageRate;
       
        
        salesItem.taxRate.rateCGST = this.bizTransaction.trxTaxRate.rateCGST;
        salesItem.taxRate.rateSGST = this.bizTransaction.trxTaxRate.rateSGST;
        salesItem.taxRate.rateIGST = this.bizTransaction.trxTaxRate.rateIGST;
        
        /*
        salesItem.avgSalePrice = round((salesItem.avgPriceRate*(+this.duePenaltyRate + 1)),2);
        console.log("salesItem.avgPriceRate " + salesItem.avgPriceRate);
        console.log("salesItem.avgSalePrice " + salesItem.avgSalePrice);
   
        salesItem.calculateTax(true);
        console.log("salesItem.avgSalesTax " + salesItem.avgSalesTax.totalAmount);
        salesItem.avgGrossPrice = round((+salesItem.avgSalePrice + +salesItem.avgSalesTax.totalAmount),2);
        salesItem.avgSalesBrokerage = round((salesItem.avgSalePrice*(this.bizTransaction.trxBrokerageRate)),2);
        salesItem.avgNetPrice = round((salesItem.avgSalePrice - salesItem.avgSalesBrokerage),2);
        if(salesItem.avgGrossCostAtDueTerm > 0)
        {
          salesItem.avgProfit = round((salesItem.avgNetPrice - salesItem.avgGrossCostAtDueTerm),2);
          salesItem.avgProfitMargin = round((salesItem.avgProfit/salesItem.avgGrossCostAtDueTerm)*100,2);
        }
        else if(salesItem.avgGrossCost > 0)
        {
          salesItem.avgProfit = round((salesItem.avgNetPrice - salesItem.avgGrossCost),2);
          salesItem.avgProfitMargin = round((salesItem.avgProfit/salesItem.avgGrossCost)*100,2);
        }
        */
        salesItem.avgSalesGain = 0;
  
        salesItem.qty = 0;
        salesItem.doTotal(2);
        salesItem.doAverage(2);
        salesItem.doRound(2);        
      }
      salesItem.consoleLog();
    }//end of If(salesItem)
  }

  calculateSalesSummaryByTSS(salesSummaryByTSS:SalesSummaryByTSS)
  {
    console.log("Welcome to SalesTransactionItemsPage::calculateSalesSummaryByTSS()");
    //doInitialize(salesSummaryByTSS.salesSummary);
    salesSummaryByTSS.initialize();
    
//    let rateCard = {} as RateCard;
    for(let i=0;i<salesSummaryByTSS.salesItemList.length;i++)
    {
      this.calculateSalesItem(salesSummaryByTSS.salesItemList[i]);
      
      salesSummaryByTSS.totalSaleableWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.POLISHED)
      {
        salesSummaryByTSS.totalSaleablePolishedWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      }
      else if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.ROUGH)
      {
        salesSummaryByTSS.totalSaleableRoughWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      }
      else if(salesSummaryByTSS.salesItemList[i].itemType.id === myConstants.itemType.TOPS)
      {
        salesSummaryByTSS.totalSaleableTopsWeight += +salesSummaryByTSS.salesItemList[i].saleableWeight;
      }
      //salesSummaryByTSS.salesSummary.totalPurchaseWeight += +salesSummaryByTSS.salesItemList[i].purchaseWeight;
      salesSummaryByTSS.totalQty += +salesSummaryByTSS.salesItemList[i].qty;
      
      salesSummaryByTSS.totalPurchaseCost += +salesSummaryByTSS.salesItemList[i].totalPurchaseCost;//+round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgPurchaseCost,2);
      salesSummaryByTSS.totalInterestCost += +salesSummaryByTSS.salesItemList[i].totalInterestCost;//+round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgInterestCost,2);
      salesSummaryByTSS.totalMfgCost += +salesSummaryByTSS.salesItemList[i].totalMfgCost;//+round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgMfgCost,2);
      salesSummaryByTSS.totalGrossCost += +salesSummaryByTSS.salesItemList[i].totalGrossCost;//+round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgGrossCost,2);
      salesSummaryByTSS.totalGrossCostAtDueTerm += +salesSummaryByTSS.salesItemList[i].totalGrossCostAtDueTerm;//+round(salesSummaryByTSS.salesItemList[i].saleableWeight*salesSummaryByTSS.salesItemList[i].avgGrossCostAtDueTerm,2);

      salesSummaryByTSS.totalPriceValue += salesSummaryByTSS.salesItemList[i].totalPriceValue;
      salesSummaryByTSS.totalSaleValue += salesSummaryByTSS.salesItemList[i].totalSaleValue;
      
      salesSummaryByTSS.totalSalesTax.amtCGST += salesSummaryByTSS.salesItemList[i].totalSalesTax.amtCGST;
      salesSummaryByTSS.totalSalesTax.amtSGST += salesSummaryByTSS.salesItemList[i].totalSalesTax.amtSGST;
      salesSummaryByTSS.totalSalesTax.amtIGST += salesSummaryByTSS.salesItemList[i].totalSalesTax.amtIGST;
      salesSummaryByTSS.totalSalesTax.totalAmount += salesSummaryByTSS.salesItemList[i].totalSalesTax.totalAmount;
      
      salesSummaryByTSS.totalGrossValue += salesSummaryByTSS.salesItemList[i].totalGrossValue;
      salesSummaryByTSS.totalSalesBrokerage += salesSummaryByTSS.salesItemList[i].totalSalesBrokerage;
      salesSummaryByTSS.totalNetValue += salesSummaryByTSS.salesItemList[i].totalNetValue;
      salesSummaryByTSS.totalSalesGain += salesSummaryByTSS.salesItemList[i].totalSalesGain;

      salesSummaryByTSS.totalProfit += salesSummaryByTSS.salesItemList[i].totalProfit;

      salesSummaryByTSS.totalProfitMargin = 0;
      if(salesSummaryByTSS.totalGrossCostAtDueTerm > 0)
      {
        salesSummaryByTSS.totalProfitMargin = round(((salesSummaryByTSS.totalProfit/salesSummaryByTSS.totalGrossCostAtDueTerm)*100),2);
      }      
    }
    if(this.trxCurrency === myConstants.localCurrency)
    {
      salesSummaryByTSS.doAverage(2);
      salesSummaryByTSS.doRound(2);      
    //  salesSummaryByTSS.doTotal(2);
    }
    else
    {
      salesSummaryByTSS.doAverage(2);
      salesSummaryByTSS.doRound(2);      
    //  salesSummaryByTSS.doTotal(2);
    }
    
    /*
    if(this.inventoryParams.avgMarketRateForSaleable)
    {
      salesSummaryByTSS.avgValueFactor = round(salesSummaryByTSS.avgPriceRate/this.inventoryParams.avgMarketRateForSaleable,10);
    }
    else
    {
      salesSummaryByTSS.avgValueFactor = 0.0;
    }
    */
  }

  calculateSummaryOfSaleable()
  {
    console.log('Welcome to SalesTransactionItemsPage:calculateSummaryOfSaleable()');
    //doInitialize(this.totalSalesSummary);
    this.totalSalesSummary.initialize();
    for(let i=0;i<this.salesSummaryByTSSList.length;i++)
    {
      this.calculateSalesSummaryByTSS(this.salesSummaryByTSSList[i]);
      for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
      {
        this.totalSalesSummary.totalSaleableWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
        if(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id === myConstants.itemType.POLISHED)
        {
          this.totalSalesSummary.totalSaleablePolishedWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
        }
        else if(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id === myConstants.itemType.TOPS)
        {
          this.totalSalesSummary.totalSaleableTopsWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
        }
        else if(this.salesSummaryByTSSList[i].salesItemList[j].itemType.id === myConstants.itemType.ROUGH)
        {
          this.totalSalesSummary.totalSaleableRoughWeight += +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
        }
        this.totalSalesSummary.totalPurchaseCost += +this.salesSummaryByTSSList[i].salesItemList[j].totalPurchaseCost;
        this.totalSalesSummary.totalInterestCost += +this.salesSummaryByTSSList[i].salesItemList[j].totalInterestCost;
        this.totalSalesSummary.totalMfgCost += +this.salesSummaryByTSSList[i].salesItemList[j].totalMfgCost;
        this.totalSalesSummary.totalGrossCost += +this.salesSummaryByTSSList[i].salesItemList[j].totalGrossCost;
        this.totalSalesSummary.totalGrossCostAtDueTerm += +this.salesSummaryByTSSList[i].salesItemList[j].totalGrossCostAtDueTerm;

        this.totalSalesSummary.totalPriceValue += +this.salesSummaryByTSSList[i].salesItemList[j].totalPriceValue;
        this.totalSalesSummary.totalSaleValue += +this.salesSummaryByTSSList[i].salesItemList[j].totalSaleValue;
        this.totalSalesSummary.totalSalesTax.amtCGST += +this.salesSummaryByTSSList[i].salesItemList[j].totalSalesTax.amtCGST;
        this.totalSalesSummary.totalSalesTax.amtSGST += +this.salesSummaryByTSSList[i].salesItemList[j].totalSalesTax.amtSGST;
        this.totalSalesSummary.totalSalesTax.amtIGST += +this.salesSummaryByTSSList[i].salesItemList[j].totalSalesTax.amtIGST;
        this.totalSalesSummary.totalSalesTax.totalAmount += +this.salesSummaryByTSSList[i].salesItemList[j].totalSalesTax.totalAmount;

        this.totalSalesSummary.totalGrossValue += +this.salesSummaryByTSSList[i].salesItemList[j].totalGrossValue;
        this.totalSalesSummary.totalSalesBrokerage += +this.salesSummaryByTSSList[i].salesItemList[j].totalSalesBrokerage;
        this.totalSalesSummary.totalNetValue += +this.salesSummaryByTSSList[i].salesItemList[j].totalNetValue;
        this.totalSalesSummary.totalSalesGain += +this.salesSummaryByTSSList[i].salesItemList[j].totalSalesGain;
        this.totalSalesSummary.totalProfit += +this.salesSummaryByTSSList[i].salesItemList[j].totalProfit;    

        this.totalSalesSummary.totalProfitMargin = 0;
        if(this.totalSalesSummary.totalGrossCostAtDueTerm > 0)
        {
          this.totalSalesSummary.totalProfitMargin = round(((this.totalSalesSummary.totalProfit/this.totalSalesSummary.totalGrossCostAtDueTerm)*100),2);
        }
      }//loop over this.salesSummaryByTSSList[i].salesItemList.length      
    }
    if(this.trxCurrency === myConstants.localCurrency)
    {
      this.totalSalesSummary.doAverage(2); 
      this.totalSalesSummary.doRound(2);
     // this.totalSalesSummary.doTotal(0);     
    }
    else
    {
      this.totalSalesSummary.doAverage(2);
      this.totalSalesSummary.doRound(2);
     // this.totalSalesSummary.doTotal(2);      
    }
    
  }

  changeTrxDate()
  {
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Quotation Date");
    
    alert.addInput({type: 'date',name: 'trxDate',value:""+this.trxDate});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.trxDate);         
          if(data.trxDate === "")
          {
            console.log("Select Date");
          }
          else
          {
            if(this.trxDate !== data.trxDate)
            {
              this.bEdited = true;
              this.trxDate = data.trxDate;
              console.log("trxDate " + this.trxDate);
              if(!this.checkIfExpired())
              {
                this.calculateSummaryOfSaleable();
              }
            }
          }
        }  
      });
    alert.present();    
  }

  checkIfExpired():boolean
  {
    console.log("Welcome to SalesTransactionItemsPage::checkIfExpired()");
    let validityDays:number=0;
    let today: Date = (new Date());
    let trxDate: Date = new Date(this.trxDate);
    let duration: number = today.valueOf() - trxDate.valueOf();
    validityDays = round((duration / (1000 * 3600 * 24)),0); 
    //console.log("Diff: trxDate " + trxDate.toISOString() + " today: " + today.toISOString());
    console.log("Diff: duration " + duration + " Days: " + validityDays);
    if(+validityDays <= +this.trxValidityDays)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  changeValidityDays()
  {
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Validity Days");
    
    alert.addInput({type: 'number',name: 'validityDays',value:""+this.trxValidityDays});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.validityDays);         
          if(+data.validityDays <= 0)
          {
            console.log("Enter value > 0");
          }
          else
          {
            if(this.trxValidityDays !== +data.validityDays)
            {
              this.bEdited = true;
              this.trxValidityDays = +data.validityDays;
              console.log("ValidityDays " + this.trxValidityDays);
              if(!this.checkIfExpired())
              {
                this.calculateSummaryOfSaleable();
              }              
            }
          }
        }  
      });
    alert.present();    
  }

  changeExchangeRate()
  {
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter USD to INR Rate");
    
    alert.addInput({type: 'number',placeholder:'USD to INR Rate',name: 'exchangeRate',value:""+this.financeSettings.exchangeRate});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.exchangeRate);
          
          if(+data.exchangeRate <= 0)
          {
            console.log("Enter value > 0");
          }
          else
          {
            if(this.currencyExchangeRate !== +data.exchangeRate)
            {
              this.bEdited = true;
              this.currencyExchangeRate = +data.exchangeRate;
              if(this.trxCurrency === myConstants.foreignCurrency)
              {
                this.calculateSummaryOfSaleable();
              }
            }
          }
        }  
      });
    alert.present();    
  }

  changeInCurrency()
  {
    console.log('Welcome to SalesTransactionItemsPage:changeInCurrency() ' + this.trxCurrency);
    this.bEdited = true;
    this.calculateSummaryOfSaleable();
  }

  onSubmit()
  {
    let alert = this.alertCtrl.create();
    alert.setTitle("Save As");
    alert.addInput({type: 'radio',label: "QUOTATION",value: "QUOTATION",checked: false});
    alert.addInput({type: 'radio',label: "SALES",value: "SALES",checked: false});
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        if(data === 'QUOTATION')
        {
          /*
          let today = new Date();
          this.bizTransaction.trxDate = getDateString(today);
          let dueDate:Date = new Date(this.bizTransaction.trxDate);
          dueDate.setTime((dueDate).getTime()+(this.bizTransaction.trxDueDays*24*60*60*1000));
          this.bizTransaction.trxDueDate = getDateString(dueDate);
          this.bizTransaction.trxInvoiceID =  ""+this.bizTransaction.trxCurrency + "/" + (getDateString(today)) + '/' + (getTimeString(today));
          this.bizTransaction.trxStatus = myConstants.bizTrxStatus.DRAFT;
          */
          let today = new Date();
          this.bizTransaction.trxDate = this.trxDate;
          this.bizTransaction.trxValidityDays = +this.trxValidityDays;
          let dueDate:Date = new Date(this.bizTransaction.trxDate);
          dueDate.setTime((dueDate).getTime()+(this.bizTransaction.trxDueDays*24*60*60*1000));
          this.bizTransaction.trxDueDate = getDateString(dueDate);
          this.bizTransaction.trxInvoiceID =  ""+this.bizTransaction.trxCurrency + "/" + (this.trxDate) + '/' + (getTimeString(today));
          this.bizTransaction.trxStatus = myConstants.bizTrxStatus.DRAFT;
          this.setBusinessTransaction();
        }
        else if(data === 'SALES')
        {
          let alert = this.alertCtrl.create();
          alert.setTitle("Enter Sales/Invoice Date");

          alert.addInput({type: 'date',placeholder:'sales date',name: 'salesDate'});
          alert.addButton('Cancel');
          alert.addButton(
            {
              text: 'OK',
              handler: (data) => {
                console.log("data " + data.salesDate);                
                this.bizTransaction.trxDate = (data.salesDate);
                this.bizTransaction.trxValidityDays = +this.trxValidityDays;
                let dueDate:Date = new Date(this.bizTransaction.trxDate);
                dueDate.setTime((dueDate).getTime()+(this.bizTransaction.trxDueDays*24*60*60*1000));
                this.bizTransaction.trxDueDate = getDateString(dueDate);

                let today = new Date();
                this.bizTransaction.trxInvoiceID =  ""+this.bizTransaction.trxCurrency + "/" + (this.bizTransaction.trxDate) + '/' + (getTimeString(today));
                this.bizTransaction.trxStatus = myConstants.bizTrxStatus.OPEN;

                this.setBusinessTransaction();
              }  
            });
          alert.present();    
        }           
      }
    });
    alert.present();
  }

  setBusinessTransaction()
  {
    
    this.bizTransaction.confidentialityLevel = this.confidentialityLevel;
    this.bizTransaction.trxType = myConstants.trxType.SELL;
    
       
    this.bizTransaction.trxWeight = +this.totalSalesSummary.totalSaleableWeight;
    this.bizTransaction.trxRate = +this.totalSalesSummary.avgSalePrice;
    this.bizTransaction.trxValue = +this.totalSalesSummary.totalSaleValue;

    this.bizTransaction.trxTaxAmount.amtCGST = +this.totalSalesSummary.totalSalesTax.amtCGST;
    this.bizTransaction.trxTaxAmount.amtSGST = +this.totalSalesSummary.totalSalesTax.amtSGST;
    this.bizTransaction.trxTaxAmount.amtIGST = +this.totalSalesSummary.totalSalesTax.amtIGST;
    this.bizTransaction.trxTaxAmount.totalAmount = +this.totalSalesSummary.totalSalesTax.totalAmount;
    
    this.bizTransaction.trxGrossAmount = +this.totalSalesSummary.totalGrossValue;
    this.bizTransaction.trxGrossRate = +this.totalSalesSummary.avgGrossPrice;

    this.bizTransaction.trxBrokerageAmount = +this.totalSalesSummary.totalSalesBrokerage;

    this.bizTransaction.trxItems = [];
    for(let i=0;i<this.salesSummaryByTSSList.length;i++)
    {
      for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
      {
        if(this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight > 0)
        {
          let businessItem = {} as BusinessItem;        
          businessItem.itemCode = this.salesSummaryByTSSList[i].salesItemList[j].itemType.id;
          businessItem.itemWeight = +this.salesSummaryByTSSList[i].salesItemList[j].saleableWeight;
          businessItem.itemRate = +this.salesSummaryByTSSList[i].salesItemList[j].avgSalePrice;
          businessItem.itemValue = +this.salesSummaryByTSSList[i].salesItemList[j].totalSaleValue;
          businessItem.itemRemarks = "";
          this.bizTransaction.trxItems.push(businessItem);   
          this.salesSummaryByTSSList[i].salesItemList[j].trxDate = this.bizTransaction.trxDate;
          this.salesSummaryByTSSList[i].salesItemList[j].trxStatus = this.bizTransaction.trxStatus;     
        }
      }
    }
    this.bizTransaction.consoleLog();

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();

    this.bizTransaction.userID = this.user.ID;
    this.bizTransaction.customerID = this.user.customerID;
    this.transactionProvider.createBusinessTransaction(this.user,this.bizTransaction,this.salesSummaryByTSSList)
    .then(data => {
      console.log("Inserted: " + data);
      loading.dismiss();
      this.navCtrl.pop();
    }).catch(e => console.log("Error from createBusinessTransaction:" + e));
  }

  onCancel()
  {
    this.navCtrl.pop();
  }

  onTitleClick() //Added by Rashmin - 09082018
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC) 
    && (this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova')))
    {
      let modal = this.modalCtrl.create('LockScreenPage');
        modal.present();
        modal.onDidDismiss((data) => {
          if(data === true)
          {
            this.switchConfidentialityLevel();
          }
          else
          {
            alert('Authentication Failed !!')
          }
        }); 
    }
    else //web flow
    {
      this.switchConfidentialityLevel();
    }    
  }

  switchConfidentialityLevel()
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC)
      && (true))//this.user.confidentialityLevel >= myConstants.confidentialityLevel.PUBLIC
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.CONFIDENTIAL;
    }
    else
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    }
    this.bEdited = true;
  }

  gotoTab(index:number)
  {
    this.slider.slideTo(index);
    this.activeTabIndex = index;
  }
  
  updateSegment()
  {
    console.log("Welcome to SalesTransactionItemsPage::updateSegment() ");
    let tabIndex = this.slider.getActiveIndex();
    console.log("tabIndex " + tabIndex);
    if(tabIndex === 0)
    {
      this.activeTabIndex = 0;
      this.selectedTab = "SALES_SUMMARY";      
    }
    else if(tabIndex === 1)
    {
      this.activeTabIndex = 1;
      this.selectedTab = "SALES_COST_VS_PRICE";
    }
    else if(tabIndex === 2)
    {
      this.activeTabIndex = 2;
      this.selectedTab = "SALES_ITEM";
    }
      
  }
  changeAvgSalePrice()
  {
    console.log("Welcome to SalesTransactionItemsPage::changeAvgSalePrice() ");
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Avg Sale Price");

    alert.addInput({type: 'number',placeholder:'Sale Price',name: 'avgSalePrice'});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.avgSalePrice);
          if(+data.avgSalePrice <= 0)
          {
            console.log("Enter value > 0");
          }
          else
          {
            if(this.totalSalesSummary.avgSalePrice !== +data.avgSalePrice)
            {
              this.bEdited = true;
              this.totalSalesSummary.avgSalePrice = +data.avgSalePrice;
              this.calculateSalePrice();
            }            
          }
        }  
      });
    alert.present();    
  }
  changeAvgProfit()
  {
    console.log("Welcome to SalesTransactionItemsPage::changeAvgProfit() ");
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Avg Profit");

    alert.addInput({type: 'number',placeholder:'Profit Amount',name: 'profitAmount'});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.profitAmount);
          if(+data.profitAmount < 0)
          {
            console.log("Enter value >= 0");
          }
          else
          {
            if(this.totalSalesSummary.avgProfit !== +data.profitAmount)
            {
              this.bEdited = true;
              this.totalSalesSummary.avgProfit = +data.profitAmount;
              this.calculateSalePriceForProfit();
            }            
          }
        }  
      });
    alert.present();    
  }

  
  changeAvgProfitMargin()
  {
    console.log("Welcome to SalesTransactionItemsPage::changeAvgProfitMargin() ");
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Avg Profit");

    alert.addInput({type: 'number',placeholder:'Profit %',name: 'profitMargin'});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.profitMargin);
          if(+data.profitMargin < 0)
          {
            console.log("Enter value >= 0");
          }
          else
          {
            if(this.totalSalesSummary.avgProfitMargin !== +data.profitMargin)
            {
              this.bEdited = true;
              this.totalSalesSummary.avgProfitMargin = +data.profitMargin;
              this.totalSalesSummary.avgProfit = round((+data.profitMargin/100)*(this.totalSalesSummary.avgGrossCostAtDueTerm),0);
              
              this.calculateSalePriceForProfit();
            }            
          }
        }  
      });
    alert.present();    
  }

  changeDueDays()
  {
    console.log("Welcome to SalesTransactionItemsPage::changeDueDays() ");
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Due Days");

    alert.addInput({type: 'number',placeholder:'Due Days',name: 'dueDays',value:""+this.bizTransaction.trxDueDays});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.dueDays);
          if(+data.dueDays < 0)
          {
            console.log("Enter value >= 0");
          }
          else
          {
            if(this.bizTransaction.trxDueDays !== +data.dueDays)
            {
              this.bEdited = true;
              this.bizTransaction.trxDueDays = +data.dueDays;
              this.calculateSummaryOfSaleable();
            }            
          }
        }  
      });
    alert.present();    
  }

  changeTaxRate()
  {
    console.log("Welcome to SalesTransactionItemsPage::changeTaxRate() ");
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Tax Rate");

    alert.addInput({type: 'number',label:"CGST Rate",placeholder:"CGST %",name: 'rateCGST',value:""+this.financeSettings.cGSTRate});
    alert.addInput({type: 'number',label:"SGST Rate",placeholder:'SGST %',name: 'rateSGST',value:""+this.financeSettings.sGSTRate});
    alert.addInput({type: 'number',label:"IGST Rate",placeholder:"IGST %",name: 'rateIGST',value:""+this.financeSettings.iGSTRate});

    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.rateCGST);
          if((+data.rateCGST < 0) || (+data.rateSGST < 0) || (+data.rateIGST < 0))
          {
            console.log("Enter value >= 0");
          }
          else
          { 
            console.log("this.bInterState " + this.bInterState);
            if(this.bInterState)
            {
              console.log("bizTransaction.trxTaxRate.rateCGST " + this.bizTransaction.trxTaxRate.rateCGST);
              console.log("bizTransaction.trxTaxRate.rateSGST " + this.bizTransaction.trxTaxRate.rateSGST);
              console.log("+data.rateCGST " + +data.rateCGST);
              console.log("+data.rateSGST " + +data.rateSGST);
              if((this.bizTransaction.trxTaxRate.rateCGST !== +data.rateCGST) 
              || (this.bizTransaction.trxTaxRate.rateSGST !== +data.rateSGST) )
              {
                this.bEdited = true;
                this.bizTransaction.trxTaxRate.rateCGST = +data.rateCGST;
                this.bizTransaction.trxTaxRate.rateSGST = +data.rateSGST;
                this.bizTransaction.trxTaxRate.rateIGST = 0;
                this.applicableTaxRate = this.bizTransaction.trxTaxRate.rateCGST + this.bizTransaction.trxTaxRate.rateSGST;
                this.calculateSummaryOfSaleable();
              }              
            }
            else
            {
              console.log("bizTransaction.trxTaxRate.rateIGST " + this.bizTransaction.trxTaxRate.rateIGST);
              console.log("+data.rateIGST " + +data.rateIGST);
              if(this.bizTransaction.trxTaxRate.rateIGST !== +data.rateIGST)
              {
                this.bEdited = true;
                this.bizTransaction.trxTaxRate.rateCGST = 0;
                this.bizTransaction.trxTaxRate.rateSGST = 0;
                this.bizTransaction.trxTaxRate.rateIGST = +data.rateIGST;
                this.applicableTaxRate = this.bizTransaction.trxTaxRate.rateIGST;
                this.calculateSummaryOfSaleable();
              }              
            }
          }
        }  
      });
    alert.present();    
  }

  changeBrokerageRate()
  {
    console.log("Welcome to SalesTransactionItemsPage::changeBrokerageRate() ");
    let alert = this.alertCtrl.create();
    alert.setTitle("Enter Brokerage Rate");

    alert.addInput({type: 'number',placeholder:'Brokerage Rate',name: 'brokerageRate',value: ""+this.brokerageRate});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.brokerageRate);
          if(+data.brokerageRate < 0)
          {
            console.log("Enter value >= 0");
          }
          else
          {
            if(this.brokerageRate !== +data.brokerageRate)
            {
              this.bEdited = true;
              this.brokerageRate = +data.brokerageRate;
              this.bizTransaction.trxBrokerageRate = (this.brokerageRate);
              this.calculateSummaryOfSaleable();
            }
          }
        }  
      });
    alert.present();    
  }

  calculateSalePriceForProfit()
  {
    console.log("Welcome to SalesTransactionItemsPage::calculateSalePriceForProfit() " + this.totalSalesSummary.avgProfit);
    this.totalSalesSummary.avgNetPrice = round((+this.totalSalesSummary.avgGrossCostAtDueTerm + +this.totalSalesSummary.avgProfit),2);
    
    let duePenaltyRate:number = round((((+this.bizTransaction.trxDueDays + this.trxValidityDays)/30.0)*(+this.bizTransaction.trxROIForLatePayment)),10);
    this.totalSalesSummary.avgPriceRate = round((this.totalSalesSummary.avgNetPrice/(1-(this.bizTransaction.trxBrokerageRate/100)))/(1+(duePenaltyRate/100)),2);

    console.log("totalSalesSummary.avgNetPrice " + this.totalSalesSummary.avgNetPrice);
    console.log("totalSalesSummary.avgPriceRate " + this.totalSalesSummary.avgPriceRate);

    for(let i=0;i<this.salesSummaryByTSSList.length;i++)
    {
      for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
      {
        console.log("salesItem.avgPriceRate " + this.salesSummaryByTSSList[i].salesItemList[j].avgPriceRate);
        this.salesSummaryByTSSList[i].salesItemList[j].avgPriceRate = round(this.salesSummaryByTSSList[i].salesItemList[j].valueFactor*this.totalSalesSummary.avgPriceRate,2);
        console.log("salesItem.avgPriceRate " + this.salesSummaryByTSSList[i].salesItemList[j].avgPriceRate);
      }
    }
    this.calculateSummaryOfSaleable();
  }

  calculateSalePrice()
  {
    console.log("Welcome to SalesTransactionItemsPage::calculateSalePrice() " + this.totalSalesSummary.avgSalePrice);
    
    let duePenaltyRate:number = round((((+this.bizTransaction.trxDueDays + +this.trxValidityDays)/30.0)*(+this.bizTransaction.trxROIForLatePayment)),10);
    this.totalSalesSummary.avgPriceRate = round((this.totalSalesSummary.avgSalePrice)/(1+(duePenaltyRate/100)),2);

    console.log("totalSalesSummary.avgPriceRate " + this.totalSalesSummary.avgPriceRate);

    for(let i=0;i<this.salesSummaryByTSSList.length;i++)
    {
      for(let j=0;j<this.salesSummaryByTSSList[i].salesItemList.length;j++)
      {
        console.log("salesItem.avgPriceRate " + this.salesSummaryByTSSList[i].salesItemList[j].avgPriceRate);
        this.salesSummaryByTSSList[i].salesItemList[j].avgPriceRate = round(this.salesSummaryByTSSList[i].salesItemList[j].valueFactor*this.totalSalesSummary.avgPriceRate,2);
        console.log("salesItem.avgPriceRate " + this.salesSummaryByTSSList[i].salesItemList[j].avgPriceRate);
      }
    }
    this.calculateSummaryOfSaleable();
  }

  setDetailsAsPerPartyID(partyID:string)
  {
    console.log("Welcome to SalesTransactionItemsPage::setPartyDetails() " + partyID);
    if(this.contactList.length)
    {
      for(let i=0;i<this.contactList.length;i++)
      {
        if(this.contactList[i].id === partyID)
        {
          this.bizTransaction.trxPartyID = this.contactList[i].id;
          this.bizTransaction.trxPartyName = this.contactList[i].fullName;
          this.bizTransaction.trxROIForEarlyPayment = this.contactList[i].ROIForEarlyPayment;
          this.bizTransaction.trxROIForLatePayment = this.contactList[i].ROIForLatePayment;
    
          if(this.contactList[i].stateRegion === this.organisation.stateCode)
          {
            this.bInterState = true;
            this.bizTransaction.trxTaxRate.rateCGST = this.financeSettings.cGSTRate;
            this.bizTransaction.trxTaxRate.rateSGST = this.financeSettings.sGSTRate;
            this.bizTransaction.trxTaxRate.rateIGST = 0;
            this.applicableTaxRate = this.bizTransaction.trxTaxRate.rateCGST + this.bizTransaction.trxTaxRate.rateSGST;
          }
          else
          {
            this.bInterState = false;
            this.bizTransaction.trxTaxRate.rateCGST = 0;
            this.bizTransaction.trxTaxRate.rateSGST = 0;
            this.bizTransaction.trxTaxRate.rateIGST = this.financeSettings.iGSTRate;
            this.applicableTaxRate = this.bizTransaction.trxTaxRate.rateIGST;
          }
          this.calculateSummaryOfSaleable();      
        }//if(this.contactList[i].id === partyID)
      }//end of FOR loop      
    }
  }

  selectCustomer()
  {
    console.log("Welcome to SalesTransactionItemsPage::selectCustomer() ");
    let alert = this.alertCtrl.create();
    for(let i=0; i<this.contactList.length;i++)
    {
      if(this.contactList[i].category === myConstants.partyCategory.BUYER)
      {
        alert.addInput({type: 'radio',
            label: this.contactList[i].fullName,
            value: ""+i,
            checked: false});
      }
    }
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        let index:number = +data;
        if(this.bizTransaction.trxPartyID !== this.contactList[index].id)
        {
          this.bEdited = true;
          this.setDetailsAsPerPartyID(this.contactList[index].id);
        }        
      }
    });
    alert.present();
  }

  selectBroker()
  {
    console.log("Welcome to SalesTransactionItemsPage::selectBroker() ");
    let alert = this.alertCtrl.create();
    for(let i=0; i<this.contactList.length;i++)
    {
      if(this.contactList[i].category === myConstants.partyCategory.BROKER)
      {
        alert.addInput({type: 'radio',
            label: this.contactList[i].fullName,
            value: ""+i,
            checked: false});
      }
    }
    alert.addInput({type: 'radio',label: "NONE",value: "NONE",checked: false});
    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: (data:string) => {
        console.log("data " + data);
        if((data === 'NONE') && (this.bizTransaction.trxBrokerID))
        {
          this.bEdited = true;
          this.bizTransaction.trxBrokerID = "";
          this.bizTransaction.trxBrokerName = "";
          this.brokerageRate = 0;
          this.bizTransaction.trxBrokerageRate = this.brokerageRate;
          this.calculateSummaryOfSaleable();
        }
        else if(data !== 'NONE')
        {
          let index:number = +data;
          if(this.contactList[index].id !== this.bizTransaction.trxBrokerID)
          {
            this.bEdited = true;
            this.bizTransaction.trxBrokerID = this.contactList[index].id;
            this.bizTransaction.trxBrokerName = this.contactList[index].fullName;
            this.brokerageRate = myConstants.DEFALUT.BROKERAGE_RATE;
            this.bizTransaction.trxBrokerageRate = this.brokerageRate;
            this.calculateSummaryOfSaleable();
          }          
        }        
      }
    });
    alert.present();
  }


  presentLoading()
  {
    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
  }

  
}
