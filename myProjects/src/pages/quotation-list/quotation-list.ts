import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform,ModalController } from 'ionic-angular';

import { TransactionProvider } from '../../providers/transaction/transaction';
import { Subscription } from 'rxjs';

import { User, checkIfValid } from '../../model/user';
import { myConstants } from '../../model/myConstants';
import { BusinessTransaction } from '../../model/businessTransaction';

/**
 * Generated class for the QuotationListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-quotation-list',
  templateUrl: 'quotation-list.html',
})
export class QuotationListPage {
  private user:User;  
  private mySubscriptions : Subscription;
  private confidentialityLevel:number;
  private quotationList:Array<BusinessTransaction>;
  private filteredQuotationList:Array<BusinessTransaction>;
  private totalWeight:number;
  private totalAmount:number;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private platform:Platform,
    private modalCtrl:ModalController,
    private transactionProvider:TransactionProvider) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');
    this.quotationList = [];
    this.filteredQuotationList = [];
    this.totalWeight = 0;
    this.totalAmount = 0;
    this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad QuotationListPage');
  }

  ionViewWillEnter()
  {
    console.log("ionViewWillEnter QuotationListPage");
    this.initializeData();  
  }

  async initializeData()
  {
    console.log("Welcome to QuotationListPage::initializeData() ");

    this.mySubscriptions = new Subscription();
    this.mySubscriptions.add(this.transactionProvider.getAllBusinessTransactions(this.user,myConstants.trxType.SELL).valueChanges()
    .subscribe(businessTransactionList => {
      console.log("Received businessTransactionList: " + businessTransactionList.length);
      this.quotationList = [];
      this.quotationList = businessTransactionList.filter(bizTrx => (bizTrx.trxStatus === myConstants.bizTrxStatus.DRAFT));
      this.filterQuotationList();
      this.calculateQuotationSummary();
    }));      
  }

  ionViewWillLeave()
  {
    console.log("Welcome to QuotationListPage::ionViewWillLeave()");
    this.mySubscriptions.unsubscribe();
  }

  filterQuotationList()
  {
    this.filteredQuotationList = this.quotationList.filter(quotation => quotation.confidentialityLevel <= this.confidentialityLevel);
    this.calculateQuotationSummary();
  }

  calculateQuotationSummary()
  {
    console.log("Welcome to QuotationListPage::calculateQuotationSummary() " + this.quotationList.length);
    this.totalWeight = 0;
    this.totalAmount = 0;
    for(let i=0;i<this.filteredQuotationList.length;i++)
    {
      this.totalWeight = this.filteredQuotationList[i].trxWeight;
      this.totalAmount = this.filteredQuotationList[i].trxGrossAmount;
    }
  }

  openQuotation(q:BusinessTransaction)
  {
    console.log("Welcome to QuotationListPage::openQuotation() " + q.ID);
    this.navCtrl.push('SalesTransactionItemsPage',{User:this.user,fromEntity:"QLIST",bizTrxID:q.ID,confidentialityLevel:q.confidentialityLevel});

  }

  addQuotation()
  {
    console.log("Welcome to QuotationListPage::addQuotation() ");
    this.navCtrl.push('SalesTransactionItemsPage',{User:this.user,fromEntity:"INVENTORY",confidentialityLevel:this.confidentialityLevel});
  }

  onTitleClick() //Added by Rashmin - 09082018
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC) 
    && (this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova')))
    {
      let modal = this.modalCtrl.create('LockScreenPage');
        modal.present();
        modal.onDidDismiss((data) => {
          if(data === true)
          {
            this.switchConfidentialityLevel();
          }
          else
          {
            alert('Authentication Failed !!')
          }
        }); 
    }
    else //web flow
    {
      this.switchConfidentialityLevel();
    }    
  }

  switchConfidentialityLevel()
  {
    if((this.confidentialityLevel === myConstants.confidentialityLevel.PUBLIC)
      && (true))//this.user.confidentialityLevel >= myConstants.confidentialityLevel.PUBLIC
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.CONFIDENTIAL;
    }
    else
    {
      this.confidentialityLevel = myConstants.confidentialityLevel.PUBLIC;
    }
    this.filterQuotationList();
  }

}
