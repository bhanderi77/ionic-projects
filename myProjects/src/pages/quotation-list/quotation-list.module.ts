import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuotationListPage } from './quotation-list';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    QuotationListPage,
  ],
  imports: [
    IonicPageModule.forChild(QuotationListPage),
    PipesModule
  ],
})
export class QuotationListPageModule {}
