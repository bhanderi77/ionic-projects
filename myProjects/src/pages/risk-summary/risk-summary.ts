import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams,AlertController, ViewController ,Loading,LoadingController,PopoverController} from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';
import { Milestone } from '../../model/milestone';
import { Project } from '../../model/project';
import { Risk } from '../../model/risk';
import { myConstants } from '../../model/myConstants';


/**
 * Generated class for the RiskSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-risk-summary',
  templateUrl: 'risk-summary.html',
})
export class RiskSummaryPage {

  private milestone:Milestone;
  private project:Project;
  //private riskList:Observable<Risk[]>;
  private riskList:Array<Risk>;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private firebaseProvider:FirebaseProvider,
    private alertCtrl:AlertController,
    private loadingCtrl: LoadingController,   
    private popOverCtrl:PopoverController,
    private viewCtrl: ViewController
  ) 
  {
    this.project = {} as Project;
    this.project.id = "";
    this.milestone = {} as Milestone;
    this.milestone.id = "";

    this.project.id = navParams.get('projectID');
    if(this.project.id)
    {
      this.firebaseProvider.getProject(this.project.id)
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
        this.milestone.id = this.navParams.get('milestoneID');  
        if(this.milestone.id)
        {
          console.log("Calling getRiskListForMilestone");
          this.firebaseProvider.getRiskListForMilestone(this.project.id,this.milestone.id).valueChanges()
          .subscribe(rList => {
            this.riskList = rList;
           // this.riskList = rList.sort();
          });      
        }
        else
        {
          console.log("Call getRiskListForProject");
          this.firebaseProvider.getRiskListForProject(this.project.id).valueChanges()
          .subscribe(rList => {
            this.riskList = rList;
           // this.riskList = rList.sort();
          });      
        
        }
      });
    }
    else
    {
     console.log("Get all risk across the projects");
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RiskSummaryPage');
  }

  addRisk()
  {
     const modal = this.modalCtrl.create('AddRiskPage',{projectID:this.project.id,milestoneID:this.milestone.id,riskID:""});
      modal.present();
      modal.onDidDismiss((data) => {
      }); 
  }

  editRisk(r:Risk)
  {
     const modal = this.modalCtrl.create('AddRiskPage',{projectID:this.project.id,milestoneID:this.milestone.id,riskID:r.id});
      modal.present();
      modal.onDidDismiss((data) => {
      }); 
  }

  async close(r:Risk)
  {
    r.actualClosureDate = new Date().toISOString();
    r.status = myConstants.Status.CLOSED;

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
      
    console.log("Calling firebaseProvider.updateRisk");
    await this.firebaseProvider.updateRisk(this.project.id,this.milestone.id,r);
    loading.dismiss();
  }

  async open(r:Risk)
  {
    r.actualClosureDate = "";
    r.status = myConstants.Status.OPEN;

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
      
    console.log("Calling firebaseProvider.updateRisk");
    await this.firebaseProvider.updateRisk(this.project.id,this.milestone.id,r);
    loading.dismiss();
  }

  setRAG(event,r:Risk)
  {
    console.log("setRAG for risk : " + r.description);
    let popover = this.popOverCtrl.create("SetRAGPage",{projectID: this.project.id,milestoneID:this.milestone.id, riskID:r.id});
    popover.present({
      ev: event
    });
  }

  goBack()
  {
    this.viewCtrl.dismiss();
  }

}
