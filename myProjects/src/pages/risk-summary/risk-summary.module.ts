import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RiskSummaryPage } from './risk-summary';

@NgModule({
  declarations: [
    RiskSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(RiskSummaryPage),
  ],
})
export class RiskSummaryPageModule {}
