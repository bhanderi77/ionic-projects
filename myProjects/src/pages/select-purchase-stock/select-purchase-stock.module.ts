import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectPurchaseStockPage } from './select-purchase-stock';

@NgModule({
  declarations: [
    SelectPurchaseStockPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectPurchaseStockPage),
  ],
})
export class SelectPurchaseStockPageModule {}
