import { Component } from '@angular/core';
import { ModalController, NavController, Platform,IonicPage, NavParams } from 'ionic-angular';
import { Loading,LoadingController } from 'ionic-angular';
//import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';

import { AuthProvider } from '../../providers/auth/auth';
import { User, checkIfValid } from '../../model/user';
import { FinanceSettings } from '../../model/referenceData';
import { File } from "@ionic-native/file";

import { myConstants } from '../../model/myConstants';
import { Observable } from 'rxjs/Observable';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { OpenNativeSettings } from '../../../node_modules/@ionic-native/open-native-settings';
import { App } from 'ionic-angular/components/app/app';
//import { ThemesProvider } from "../../providers/themes/themes";
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private user:User;
  private bInvalidUser:boolean;
  private isLoggedIn: boolean;
  private financeSettings:FinanceSettings
  private passCode: string;
  //private selectedTheme: String;
  constructor(public navCtrl: NavController,
    private settingsProvider: SettingsProvider,
    private authProvider: AuthProvider,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private platform: Platform,
    private faio: FingerprintAIO,
    private file: File,
    private openNativeSettings: OpenNativeSettings,
    private navParams: NavParams,
    private appCtrl: App
    //private themeSettings: ThemesProvider
    ) 
  {

    this.passCode = '';
    // this.user = navParams.get('User');
    this.user = {} as User;
    this.user.ID = '';
    this.user.customerID = '';
    this.bInvalidUser = false;
    this.isLoggedIn = navParams.get('isLoggedIn');
    if(this.isLoggedIn)
    {
      this.user = navParams.get('User');
      this.bInvalidUser = navParams.get('bInvalidUser');
    }
    console.log("platform is => " + this.platform._platforms);
   
  }

  ionViewDidLoad() {
    console.log('HomePage::ionViewDidLoad');
    //console.log('user ID: ' + this.user.ID)
    //this.login();
  }

  ionViewWillEnter() {
    console.log('HomePage::ionViewDidEnter');
    this.initilizeData();       
  }

  ionViewDidEnter() {
    console.log('HomePage::ionViewDidEnter');
    //this.initilizeData();       
  }

  ionViewWillUnload()
  {
    console.log('HomePage::ionViewWillUnload');
    //this.logout();
  }

  async initilizeData()
  {
    console.log("Welcome to HomePage::initilizeData");
    this.loadOrgSettings();
  }


  //RB19082018 - function to navigate to Party List Page
  gotoPartyListPage(partyCategory:string)
  {
    console.log('Ínside gotoPartyListPage::User.ID=> ' + this.user.ID)
    this.navCtrl.push('PartyListPage',{User:this.user,partyCategory:partyCategory});
  }

  gotoPartySummaryListPage(partyCategory:string)
  {
    this.navCtrl.push('PartySummaryListPage',{User:this.user,partyCategory:partyCategory});
  }

  gotoTransactionList(trxType:string)
  {
    this.navCtrl.push('PartyTransactionsPage',{User:this.user,transactionType:trxType});
  }

  gotoQuotationList()
  {
    this.navCtrl.push('QuotationListPage',{User:this.user});
  }

  gotoMyInventoryPage()
  {
    this.navCtrl.push('MyInventoryPage',{User:this.user}); 
  }

  gotoPurchaseStockSummaryPage()
  {
    this.navCtrl.push('PurchaseStockSummaryPage',{User:this.user}); 
  }

  gotoSalesStockSummaryPage()
  {
    this.navCtrl.push('SalesStockSummaryPage',{User:this.user}); 
  }

  gotoMyPricePage()
  {
    this.navCtrl.push('MyPricePage',{User:this.user}); 
  }

  login()
  {
    console.log("Welcome to HomePage:login()" );
    this.loginToMyBusiness();
  }
  async loginToMyBusiness()
  {
    console.log("Welcome to HomePage:Login()");
    console.log("this.user.ID " + this.user.ID);
    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
    
    this.authProvider.getUser()
    .then(currentUser => {
      this.user = currentUser;
      console.log("User found : " + this.user.ID + " " + this.user.firstName + " " + this.user.mobileNo);
      if((this.user.ID === '') || (this.user.ID === 'M') || (this.user.customerID === ''))
      {
        console.log("Invalid user");
        this.bInvalidUser = true;
        this.logout();
        loading.dismiss();
      }
      else if((this.user.ID !== "") && (this.user.customerID !== ''))
      {
        this.isLoggedIn = true;
        console.log('is User logged in: ' + this.isLoggedIn);
        this.loadOrgSettings();
        loading.dismiss();
        this.navCtrl.setRoot('DrawerLayoutPage',{User:this.user,bInvalidUser:this.bInvalidUser,isLoggedIn: this.isLoggedIn})
      }
    }).catch(e => {
      console.log("Error from this.authProvider.getUser()");
      loading.dismiss();
    });
    
  }

  async logout()
  {
    await this.authProvider.logoutGoogle();
   // .then(res => {
      this.isLoggedIn = false;
     // this.bInvalidUser = true;
      this.user.ID = '';
      this.user.customerID = '';
      this.appCtrl.getRootNav().setRoot('HomePage');
      //this.navCtrl.setRoot('HomePage',{User:this.user,bInvalidUser:this.bInvalidUser,isLoggedIn: this.isLoggedIn});
      console.log("this.user.ID " + this.user.ID);
   // });    
  }

  async loadOrgSettings() 
  {
   if(checkIfValid(this.user))
   {
    this.financeSettings = await this.settingsProvider.getFinanceSettings(this.user);
   } 
  }

  // toggleAppTheme() {
  //   console.log('inside toggleAppTheme()::selectedTheme' + this.selectedTheme);
  //   if(this.selectedTheme === 'dark-theme')
  //   {
  //     this.themeSettings.setActiveTheme('light-theme');
  //   }
  //   else
  //   {
  //     this.themeSettings.setActiveTheme('dark-theme');
  //   }
  // }
}
