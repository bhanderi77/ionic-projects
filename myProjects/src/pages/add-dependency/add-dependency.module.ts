import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDependencyPage } from './add-dependency';

@NgModule({
  declarations: [
    AddDependencyPage,
  ],
  imports: [
    IonicPageModule.forChild(AddDependencyPage),
  ],
})
export class AddDependencyPageModule {}
