import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams,AlertController, ViewController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Milestone } from '../../model/milestone';
import { Project } from '../../model/project';


/**
 * Generated class for the AddRAIDPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-RAID',
  templateUrl: 'add-RAID.html',
})
export class AddRAIDPage {
  private milestone:Milestone;
  private project:Project;
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private firebaseProvider:FirebaseProvider,
    private alertCtrl:AlertController,
    private viewCtrl: ViewController
  ) {
    this.project = {} as Project;
    this.project.id = this.navParams.get('projectID');

    this.milestone = {} as Milestone;
    this.milestone.id = this.navParams.get('milestoneID');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddRaidPage');
  }

  addRisk()
  {
    this.viewCtrl.dismiss();
    console.log("Welcome to addRisk() for project " + this.project.id);
    const modal = this.modalCtrl.create('RiskSummaryPage',{projectID:this.project.id,milestoneID:this.milestone.id});
      modal.present();
      modal.onDidDismiss((data) => {
      });
  }

  addAction()
  {
    this.viewCtrl.dismiss();  
    console.log("Welcome to addAction() for project " + this.project.id);
    const modal = this.modalCtrl.create('ActionSummaryPage',{projectID:this.project.id,milestoneID:this.milestone.id});
    //const modal = this.modalCtrl.create('ActionSummaryPage',{projectID:this.project.id,milestoneID:""});
      modal.present();
      modal.onDidDismiss((data) => {
      });
  }

  addIssue()
  {
    this.viewCtrl.dismiss();
    console.log("Welcome to addIssue() for project " + this.project.id);
    const modal = this.modalCtrl.create('IssueSummaryPage',{projectID:this.project.id,milestoneID:this.milestone.id});
      modal.present();
      modal.onDidDismiss((data) => {
      });
  }

  addDependency()
  {
    this.viewCtrl.dismiss();
    console.log("Welcome to addDependency() for project " + this.project.id);
    const modal = this.modalCtrl.create('DependencySummaryPage',{projectID:this.project.id,milestoneID:this.milestone.id});
      modal.present();
      modal.onDidDismiss((data) => {
      });
  }

}
