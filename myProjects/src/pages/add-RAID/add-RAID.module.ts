import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRAIDPage } from './add-raid';

@NgModule({
  declarations: [
    AddRAIDPage,
  ],
  imports: [
    IonicPageModule.forChild(AddRAIDPage),
  ],
})
export class AddRaidPageModule {}
