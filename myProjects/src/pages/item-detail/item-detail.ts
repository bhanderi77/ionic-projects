import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController,ViewController, NavParams,AlertController,Loading,LoadingController } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { EntityProvider } from '../../providers/entity/entity';

import { BusinessContact } from '../../model/businessContact';
import { PackagedItem, ItemShape, ItemSieve, ItemQualityLevel, ItemValuation } from '../../model/packagedItem';
import { PackageRule } from '../../model/packageRule';
import { myConstants,round } from '../../model/myConstants';
import { BusinessTransaction } from '../../model/businessTransaction';
import { User,checkIfValid } from '../../model/user';
import { summaryDetails } from '../../model/taskSummary';


/**
 * Generated class for the ItemDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html',
})
export class ItemDetailPage {
  private partyProfile:BusinessContact;
  private pItem:PackagedItem;
  private actionText:string;
  private pItemListForTrxID:Array<PackagedItem>;
  private ancestorsPackagedItemList:Array<PackagedItem>;
  private descendantPackagedItemList:Array<PackagedItem>;
  private businessTransaction:BusinessTransaction;
  private bizTrxInterest:number;
  private daysForDue:number;
  private interestAmountForItem:number;
  private currencyExchangeRate:number;
  private user:User;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,    
    public viewCtrl: ViewController,
    private navParams: NavParams,
    private transactionProvider: TransactionProvider,
    private settingsProvider: SettingsProvider,
    private entityProvider:EntityProvider,
    private inventoryProvider: InventoryProvider) 
  {
    this.partyProfile = {} as BusinessContact;
    this.pItem = {} as PackagedItem;
    this.pItem = this.navParams.get("pItem");
  
    this.user = {} as User;
    this.user = navParams.get('User');
    this.businessTransaction = {} as BusinessTransaction;
    this.currencyExchangeRate = 1;
    this.pItemListForTrxID = [];
    this.ancestorsPackagedItemList = [];
    this.descendantPackagedItemList = [];
    this.actionText = "";
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad ItemDetailPage');
  }

  ionViewWillEnter()
  {
    console.log('ionViewWillEnter ItemDetailPage');
    this.pItem = this.navParams.get("pItem");
    this.initializeData();
  }

  async initializeData()
  {
    console.log("Welcome to ItemDetailPage::initializeData()");
    this.findActionText();
    this.currencyExchangeRate = 1;
    if(checkIfValid(this.user))
    {
      this.currencyExchangeRate = await this.settingsProvider.getExchangeRate(this.user);

      this.partyProfile = await this.entityProvider.getBusinessContact(this.user,this.pItem.trxPartyID);
      
      this.businessTransaction = {} as BusinessTransaction;
     // this.businessTransaction = await 
      this.transactionProvider.getBusinessTransaction(this.user,this.pItem.trxID)
      .then(bizTrx => {
        this.businessTransaction = bizTrx;
        this.daysForDue = 0;
        if(this.businessTransaction)
        {
          let today: Date = new Date();
          let dueDate: Date = (new Date(this.businessTransaction.trxDueDate));
          let duration: number = dueDate.valueOf() - today.valueOf();
          this.daysForDue = Math.floor(duration / (1000 * 3600 * 24)); 
        } 
      });

      this.bizTrxInterest = 0;
      this.transactionProvider.getInterestForBizTrxID(this.user,this.pItem.trxPartyID,this.pItem.trxID,this.currencyExchangeRate)
      .then(interest => {
        this.bizTrxInterest = interest;
        this.interestAmountForItem = round((this.bizTrxInterest * this.pItem.trxShare/100.0),2);
      });

      this.pItemListForTrxID = [];
      this.inventoryProvider.getPackagedItemsListForBizTrxID(this.user,this.pItem.trxID)
      .then(pItemList => {
       console.log("Received pItemList.length " + pItemList.length); 
       this.pItemListForTrxID = pItemList;
        //this.ancestorsPackagedItemList = [];
        //this.descendantPackagedItemList = [];
        this.createTrace();
      });
    }
  }


  createTrace()
  {
    console.log("Welcome to ItemDetailPage::createTrace() " + this.pItemListForTrxID.length);
    if(this.pItemListForTrxID.length <=0 )
    {
      return;
    }
    console.log(this.pItem.id + ":" + this.pItem.parentID + "=>" + this.pItem.packageID + " " + this.pItem.pRule.curTaskCode + " " +  this.pItem.packageStatus + " " + this.pItem.pRule.nextTaskCode + " " + this.pItem.itemWeight);
    this.ancestorsPackagedItemList = [];
    this.descendantPackagedItemList = [];
   
    let traceID:string='';
    
    //Trace Parent
    console.log("Parents:");
    traceID=this.pItem.parentID;             
    
    while((traceID))
    {
      let currentItem:Array<PackagedItem>;
      currentItem = [];
      console.log(" traceID:" + traceID);
      currentItem = this.pItemListForTrxID.filter(packagedItem=>(packagedItem.id === traceID));
      console.log("currentItem.length: " + currentItem.length);
      if(currentItem.length === 1)
      {
        this.ancestorsPackagedItemList.push(currentItem[0]);            
        traceID=currentItem[0].parentID;
      }
      else
      {
        traceID='';
      }
    }
    this.ancestorsPackagedItemList.reverse();
    //Add selected package to trace
    
    //Trace Child    
  }

  findActionText()
  {
    console.log("Welcome to ItemDetailPage::findActionText() "); 
    
    if(this.pItem.packageStatus === myConstants.packageStatus.CO)
    {
      this.actionText = "NONE";
    }
    else if((this.pItem.packageStatus === myConstants.packageStatus.RD) || (this.pItem.packageStatus === myConstants.packageStatus.PD))
    {
      if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.PURCHASE))
      {
        this.actionText = "Receive";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.PLANNING))
      {
        this.actionText = "Start Planning";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_ISSUE))
      {
        this.actionText = "Issue to Manufacturer";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_RETURN))
      {
        this.actionText = "Return From Manufacturer";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.ASSORTMENT))
      {
        this.actionText = "Start Assortment";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.SALES))
      {
        this.actionText = "NONE";
      }
    }
    else if(this.pItem.packageStatus === myConstants.packageStatus.IP)
    {
      if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.PURCHASE))
      {
        this.actionText = "INVALID";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.PLANNING))
      {
        this.actionText = "Complete Planning";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_ISSUE))
      {
        this.actionText = "Agreed";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_RETURN))
      {
        this.actionText = "INVALID";
      }
      else if((this.pItem.pRule.nextTaskCode === myConstants.taskCode.ASSORTMENT))
      {
        this.actionText = "Complete Assortment";
      }
    }
  }

  takeAction()
  {
    console.log("Welcome to ItemDetailPage::takeAction() ");
   
    if((this.pItem.packageStatus === myConstants.packageStatus.RD) || (this.pItem.packageStatus === myConstants.packageStatus.PD))
    {
      this.startTask(this.pItem);
    }
    else if((this.pItem.packageStatus === myConstants.packageStatus.IP))
    {
      this.completeTask(this.pItem);
    }
  }

  doUndo()
  {
    console.log("Welcome to ItemDetailPage::doUndo() ");
    if(this.pItem.pRule.curTaskCode === this.pItem.pRule.nextTaskCode)
    {
      if(this.pItem.packageStatus === myConstants.packageStatus.IP)
      {
        this.inventoryProvider.undo_BtoB_IP_packagedItem(this.user,this.pItem)
        .then(result => {
          console.log("result from this.inventoryProvider.undo_BtoB_IP_packagedItem() " + result);
          if(result)
          {
            this.viewCtrl.dismiss();
          }
        }).catch(e => {
          console.log("Error from this.inventoryProvider.undo_BtoB_IP_packagedItem() " + e);
        });
      }
      else if(this.pItem.packageStatus === myConstants.packageStatus.CO)
      {
        /*
        Do not handle this scenario.
        Altenative way is to undo all B-C packagedItem generated from this B-B item and is in RD status
        */
      }
    }
    else
    {
      if(this.pItem.packageStatus === myConstants.packageStatus.RD)
      {
        this.inventoryProvider.undo_BtoC_RD_packagedItem(this.user,this.pItem)
        .then(result => {
          console.log("result from this.inventoryProvider.undo_BtoC_RD_packagedItem() " + result);
          if(result)
          {
            this.viewCtrl.dismiss();
          }
        }).catch(e => {
          console.log("Error from this.inventoryProvider.undo_BtoC_RD_packagedItem() " + e);
        });
      }
    }
  }

  async startTask(selPackagedItem:PackagedItem)
  {
    console.log("Inside ItemDetailPage::startTask()");
    console.log("Selected Item=> PackageID: " + selPackagedItem.packageID + " Status: " + selPackagedItem.packageStatus 
          + " Weight: " + selPackagedItem.itemWeight);


    if(   (selPackagedItem.pRule.curTaskCode === selPackagedItem.pRule.nextTaskCode)
      ||  ( (selPackagedItem.pRule.curTaskCode !== selPackagedItem.pRule.nextTaskCode)
          && !(  (selPackagedItem.packageStatus === myConstants.packageStatus.RD)
              ||(selPackagedItem.packageStatus === myConstants.packageStatus.PD))
          )
      )
    {//Invalid entries
      alert(selPackagedItem.pRule.curTaskCode + " " + selPackagedItem.packageStatus + " " + selPackagedItem.pRule.nextTaskCode + " CAN NOT be started");
      return;
    }

    //if( !(selPackagedItem.pRule.bGroupRead || selPackagedItem.pRule.bPartialAssign))
    {//A->B RD for everthing to be started in one go

      if(selPackagedItem.pRule.bIncursCost)//This flag to be renamed as bInProgress
      {
        return this.generateSingleBtoBForSelected(selPackagedItem);     
      }
      else
      {
        console.log("Directly complete it!!");
        //packagedItemBtoB.id = docID;     
        this.completeTask(selPackagedItem);
        this.actionText = "NONE";        
      }
    }
    /*      
    else if( (!selPackagedItem.pRule.bGroupRead) && (selPackagedItem.pRule.bPartialAssign))
    {//A->B RD/PD entries where entire input is not supposed to be started together i.e.Manager assign part of input to someone in team
      console.log("Valid entries i.e. A->B RD/IP");
      let pending = {} as summaryDetails;
      pending.totalQty = +selPackagedItem.itemQty;
      pending.totalWeight = +selPackagedItem.itemWeight;
      pending.totalCost = +selPackagedItem.itemCost;
      pending.totalTax = +selPackagedItem.itemTax;
      pending.totalBizTrxShare = +selPackagedItem.trxShare;

      let relevantPackagedItemList:Array<PackagedItem>;
      let pItemListBtoB:Array<PackagedItem>;
      pItemListBtoB  = await this.inventoryProvider.get_BtoB_IP_packagedItemsList(this.user,selPackagedItem.pRule.nextTaskCode);  
      //pItemListBtoB.subscribe(pItemList => 
      if(pItemListBtoB.length > 0)
      {
        relevantPackagedItemList = pItemListBtoB.filter(packagedItem=>packagedItem.parentID === selPackagedItem.id);
        relevantPackagedItemList.forEach(relevantPackagedItem => {
          console.log("relevantPackagedItem: " + relevantPackagedItem.packageID
                      + " " + relevantPackagedItem.pRule.curTaskCode 
                      + " " + relevantPackagedItem.packageStatus
                      + " " + relevantPackagedItem.pRule.nextTaskCode
                      + " " + relevantPackagedItem.itemWeight);
          if((relevantPackagedItem.pRule.curTaskCode === selPackagedItem.pRule.nextTaskCode)
            &&(relevantPackagedItem.pRule.curTaskCode === relevantPackagedItem.pRule.nextTaskCode))
          {//if B->B
            if((relevantPackagedItem.packageStatus === myConstants.packageStatus.RD) || (relevantPackagedItem.packageStatus === myConstants.packageStatus.IP) || (relevantPackagedItem.packageStatus === myConstants.packageStatus.CO))
            {
              pending.totalQty -= +relevantPackagedItem.itemQty;
              pending.totalWeight -= +relevantPackagedItem.itemWeight;
              pending.totalCost -= +relevantPackagedItem.itemCost;
              pending.totalTax -= +relevantPackagedItem.itemTax;
              pending.totalBizTrxShare -= +relevantPackagedItem.trxShare;
            }
          }
        });//end of loop over relevantPackagedItemList
      }//)
      //alert("packagedItem Weight: " + selPackagedItem.itemWeight + " Available: " + pending.totalWeight);
      //Choose how much to complete
      let alert = this.alertCtrl.create({
        title: "Enter WEIGHT(Max " + pending.totalWeight + ") & QTY(Max " + pending.totalQty + ")",
        inputs: [ { name: 'ToBeWeight', placeholder: 'Enter Weight'},
                  { name: 'ToBeQty', placeholder: 'Enter Qty'}
                ],
        buttons:
                [ { 
                    text: 'Cancel',
                    handler : data => { console.log('Cancel clicked');}
                  },
                  { 
                    text: 'Ok',
                    handler : data => 
                    { 
                      console.log('Ok clicked : ' + data.ToBeWeight + " " + data.ToBeQty + " " + pending.totalWeight + " " + pending.totalQty);
                      if((+data.ToBeWeight > +pending.totalWeight) || (+data.ToBeQty > +pending.totalQty ))
                      {
                        return false;
                      }
                      else
                      {
                          
                        console.log("CREATE NEW ENTRY B->B in IP status");
                        //(01) Create new object of packagedItem
                        let packagedItemBtoB = {} as PackagedItem;
                        //(02) Set item Details and packageID related fields                            
                        packagedItemBtoB.itemQty = +data.ToBeQty;
                        packagedItemBtoB.itemWeight = +data.ToBeWeight;
                        packagedItemBtoB.itemCost = round((+pending.totalCost * (+data.ToBeWeight/+pending.totalWeight)),2);
                        packagedItemBtoB.itemTax = round((+pending.totalTax * (+data.ToBeWeight/+pending.totalWeight)),2);
                        packagedItemBtoB.itemRemarks = selPackagedItem.itemRemarks;
                        
                        packagedItemBtoB.trxID = selPackagedItem.trxID;
                        packagedItemBtoB.trxDate = selPackagedItem.trxDate;
                        packagedItemBtoB.trxPartyID = selPackagedItem.trxPartyID;
                        packagedItemBtoB.trxPartyName = selPackagedItem.trxPartyName;
                        packagedItemBtoB.trxInvoiceID = selPackagedItem.trxInvoiceID;
                        packagedItemBtoB.trxShare = round((+pending.totalBizTrxShare * (+data.ToBeWeight/+pending.totalWeight)),2);
                        packagedItemBtoB.confidentialityLevel = selPackagedItem.confidentialityLevel;
                        
                        if(+packagedItemBtoB.itemWeight == +pending.totalWeight)
                        {
                          packagedItemBtoB.packageID = selPackagedItem.packageID + "|";// + packagedItemBtoB.itemWeight;
                        }
                        else
                        {
                          packagedItemBtoB.packageID = selPackagedItem.packageID + "|<" + packagedItemBtoB.itemWeight;
                        }

                        packagedItemBtoB.parentID = selPackagedItem.id;
                        packagedItemBtoB.packageGroupID = selPackagedItem.packageGroupID;
                        packagedItemBtoB.packageGroupName = selPackagedItem.packageGroupName;                     
                        

                        //(03.1) Set package rule
                        packagedItemBtoB.pRule = selPackagedItem.pRule;
                        packagedItemBtoB.pRule.curTaskCode = selPackagedItem.pRule.nextTaskCode;
                        packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
                        //(03.2) set owner details                              
                        packagedItemBtoB.packageCurTaskOwner = selPackagedItem.packageNextTaskOwner;
                        packagedItemBtoB.packageNextTaskOwner = selPackagedItem.packageNextTaskOwner;
                        
                        packagedItemBtoB.itemShape = selPackagedItem.itemShape;
                        packagedItemBtoB.itemSieve = selPackagedItem.itemSieve;
                        packagedItemBtoB.itemQualityLevel = selPackagedItem.itemQualityLevel;
                        //(04) add new entry B->B with IP status to DB


                        let loading: Loading;
                        loading = this.loadingCtrl.create();
                        loading.present();

                        this.inventoryProvider.createPackagedItem_BtoB(this.user,packagedItemBtoB,selPackagedItem)
                        .then(()=>{
                          if(packagedItemBtoB.itemWeight == pending.totalWeight)
                          {//everything is started
                            //(05) Change status to CO for selPackagedItem
                            this.inventoryProvider.updateStatus_For_AtoB_PackagedItem(this.user,selPackagedItem.id,myConstants.packageStatus.CO).then(res=>{
                              //(06) refresh data for this page
                              //this.initializeData();
                              loading.dismiss();
                              this.actionText = "NONE";
                            })
                            .catch(e => {
                              console.log("Error from this.databaseProvider.updatePackagedItemStatus() to CO : " + e);
                            });
                          }
                          else if(packagedItemBtoB.itemWeight < pending.totalWeight)
                          {//NOT everything is started
                            //(05) Change status to PD for selPackagedItem
                            this.inventoryProvider.updateStatus_For_AtoB_PackagedItem(this.user,selPackagedItem.id,myConstants.packageStatus.PD).then(res=>{
                              //(06) refresh data for this page
                              //this.initializeData();
                              loading.dismiss();
                              this.actionText = "NONE";
                            })
                            .catch(e => {
                              console.log("Error from this.databaseProvider.updatePackagedItemStatus() to PD : " + e);
                            });
                          }
                        })
                        .catch(e=>{
                          console.log("Error from this.databaseProvider.addPackagedItem(packagedItemBtoB) : " + e);
                        });    
                      }
                  }
                }
              ]
        }); 
      alert.present();
    }
    else if(selPackagedItem.pRule.bGroupRead)
    {
      alert("Read Multiple Entries");
    }*/
  }

  
  async generateSingleBtoBForSelected(pItem:PackagedItem)
  {
    console.log("Welcome to ItemDetailPage::generateSingleBtoBForSelected(pItem) " + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode + " " +pItem.itemWeight )
    /*CREATE NEW ENTRY B->B in IP status and updated status of A->B from RD to CO
    (01) Create new object of packagedItem
    (02) Set item Details and packageID related fields
    (03.1) Set package rule
    (03.2) set owner details
    (04) add new entry to DB 
    (05) Change status to CO for selPackagedItem
    (06) refresh data for this page
    */
    console.log("CREATE NEW ENTRY B->B in IP status");

    //(01) Create new object of packagedItem
    let packagedItemBtoB = {} as PackagedItem;
    //(02) Set item Details and packageID related fields                            
    packagedItemBtoB.itemQty = +pItem.itemQty;
    packagedItemBtoB.itemWeight = +pItem.itemWeight;
   
    packagedItemBtoB.itemRemarks = pItem.itemRemarks;
    packagedItemBtoB.itemValuation = pItem.itemValuation;

    packagedItemBtoB.roughWeight = +pItem.roughWeight;
    packagedItemBtoB.taskCost = 0;
    packagedItemBtoB.accumulatedTaskCost = +pItem.accumulatedTaskCost;
    
    packagedItemBtoB.itemShape = pItem.itemShape;
    packagedItemBtoB.itemSieve = pItem.itemSieve;
    packagedItemBtoB.itemQualityLevel = pItem.itemQualityLevel;

    packagedItemBtoB.trxID = pItem.trxID;
    packagedItemBtoB.trxDate = pItem.trxDate;
    packagedItemBtoB.trxPartyID = pItem.trxPartyID;
    packagedItemBtoB.trxPartyName = pItem.trxPartyName;
    packagedItemBtoB.trxWeight = pItem.trxWeight;
    packagedItemBtoB.trxRate = pItem.trxRate;
    packagedItemBtoB.trxCurrency = pItem.trxCurrency;
    packagedItemBtoB.trxShare = pItem.trxShare;

    packagedItemBtoB.confidentialityLevel = pItem.confidentialityLevel;
                        
    packagedItemBtoB.pRule = {} as PackageRule;
    
    if(!(pItem.pRule.bPartialAssign || pItem.pRule.bReducedOutput))
    {
      packagedItemBtoB.packageID = pItem.packageID;;
    }
    else
    {
      packagedItemBtoB.packageID = pItem.packageID + "|";// + packagedItemBtoB.itemWeight;
    }
    packagedItemBtoB.parentID = pItem.id;
    packagedItemBtoB.packageGroupID = pItem.packageGroupID; 
    packagedItemBtoB.packageGroupName = pItem.packageGroupName;
  
    //(03.1) Set package rule
    //packagedItemBtoB.pRule = new PackagedRule(pItem.pRule;

    packagedItemBtoB.pRule.curTaskCode = pItem.pRule.nextTaskCode;
    packagedItemBtoB.pRule.nextTaskCode = pItem.pRule.nextTaskCode;
    packagedItemBtoB.pRule.sequenceID = pItem.pRule.sequenceID;
    packagedItemBtoB.pRule.bGroupRead = pItem.pRule.bGroupRead;
    packagedItemBtoB.pRule.bIncursCost = pItem.pRule.bIncursCost;
    packagedItemBtoB.pRule.bMultipleOutput = pItem.pRule.bMultipleOutput;
    packagedItemBtoB.pRule.bPartialAssign = pItem.pRule.bPartialAssign;
    packagedItemBtoB.pRule.bReducedOutput = pItem.pRule.bReducedOutput;
    
    packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
  
    //(03.2) set owner details //TBD                             
    packagedItemBtoB.packageCurTaskOwner = pItem.packageNextTaskOwner;
    packagedItemBtoB.packageNextTaskOwner = pItem.packageNextTaskOwner;


    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();

    //(04) add new entry B->B with IP status to DB
    const docID = await this.inventoryProvider.createPackagedItem_BtoB(this.user,packagedItemBtoB,pItem);
    console.log("docID " + docID);
    //(05) Change status to CO for pItem
    await loading.dismiss();
    
    this.actionText = "NONE";
    this.viewCtrl.dismiss();    
      
  }


  /****************************************************************
  This method is invoked either 
  from B->B IP entries
  OR A-B entries where packageInProgressApplicable is false
  ****************************************************************/
  async completeTask(selPackagedItem:PackagedItem)
  {//A->B or B->B
    console.log("Inside ItemDetailPage::completeTask()");
    console.log("Selected Item=> PackageID: " + selPackagedItem.packageID + " Status: " + selPackagedItem.packageStatus 
          + " Weight: " + selPackagedItem.itemWeight);

    if(selPackagedItem.packageStatus === myConstants.packageStatus.CO)
    {//Already in CO status
      alert("This is ALREADY completed");      
      return;
    }    
    //Check if there are B->B entries with IP status or A->B entries with RD/PD/IP status
    if( (selPackagedItem.pRule.bMultipleOutput) || (selPackagedItem.pRule.bReducedOutput))
    {//Multiple or Reduced Output
      if(selPackagedItem.pRule.nextTaskCode === myConstants.taskCode.ASSORTMENT)
      {
        const modal = this.modalCtrl.create('AddPolishToSalesPage',{User:this.user,pItem:selPackagedItem});
        modal.present();
        modal.onDidDismiss((data) => {
        // this.initializeData();  
        this.actionText = "NONE";
        this.viewCtrl.dismiss();
        });
        
      }
      else
      {
        const modal = this.modalCtrl.create('TaskOutputPage',{User:this.user,pItem:selPackagedItem});
        modal.present();
        modal.onDidDismiss((data) => {
        // this.initializeData();  
        this.actionText = "NONE";
        this.viewCtrl.dismiss();
        });
      }
    }
    else if(!(selPackagedItem.pRule.bMultipleOutput || selPackagedItem.pRule.bReducedOutput))
    {//A->B where packagePartialAssignment=false OR B->B IP for single o/p same as original
      console.log("CREATE NEW ENTRY B->C in RD status");
      //(01) Create new object of packagedItem
      let packagedItemBtoC = {} as PackagedItem;
      
      //(02) Set item Details and packageID related fields                            
      packagedItemBtoC.itemQty = +selPackagedItem.itemQty;
      packagedItemBtoC.itemWeight = +selPackagedItem.itemWeight;
      
      packagedItemBtoC.itemRemarks = selPackagedItem.itemRemarks;
      packagedItemBtoC.itemValuation = selPackagedItem.itemValuation;

      packagedItemBtoC.roughWeight = +selPackagedItem.roughWeight;
      packagedItemBtoC.taskCost = 0;
      packagedItemBtoC.accumulatedTaskCost = round((+selPackagedItem.accumulatedTaskCost + packagedItemBtoC.taskCost),2);

      packagedItemBtoC.itemShape = selPackagedItem.itemShape;
      packagedItemBtoC.itemSieve = selPackagedItem.itemSieve;
      packagedItemBtoC.itemQualityLevel = selPackagedItem.itemQualityLevel;
      
      packagedItemBtoC.trxID = selPackagedItem.trxID;
      packagedItemBtoC.trxDate = selPackagedItem.trxDate;
      packagedItemBtoC.trxPartyID = selPackagedItem.trxPartyID;
      packagedItemBtoC.trxPartyName = selPackagedItem.trxPartyName;
      packagedItemBtoC.trxWeight = selPackagedItem.trxWeight;
      packagedItemBtoC.trxRate = selPackagedItem.trxRate;
      packagedItemBtoC.trxCurrency = selPackagedItem.trxCurrency;
      packagedItemBtoC.trxShare = selPackagedItem.trxShare;

      packagedItemBtoC.confidentialityLevel = selPackagedItem.confidentialityLevel;
      
      packagedItemBtoC.packageID = selPackagedItem.packageID;
      packagedItemBtoC.parentID = selPackagedItem.parentID;
      packagedItemBtoC.packageGroupID = selPackagedItem.packageGroupID;
      packagedItemBtoC.packageGroupName = selPackagedItem.packageGroupName;
      
      //(03) Find out applicable package rule
      try
      {
        let packageRuleListBtoC:Array<PackageRule> = [];
        console.log("Calling this.settingsProvider.getPackageRule_For_A for " + selPackagedItem.pRule.nextTaskCode);
        packageRuleListBtoC = await this.settingsProvider.getPackageRule_For_A(this.user,selPackagedItem.pRule.nextTaskCode);
        if(packageRuleListBtoC.length === 1)
        {
          //(03.1) Set package rule
          packagedItemBtoC.pRule = packageRuleListBtoC[0];
          packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
          
          //(03.2) set owner details                              
          packagedItemBtoC.packageCurTaskOwner = selPackagedItem.packageNextTaskOwner;
          packagedItemBtoC.packageNextTaskOwner = selPackagedItem.packageNextTaskOwner;
          
          //(04) add new entry B->C with RD status to DB
          let loading: Loading;
          loading = this.loadingCtrl.create();
          loading.present();

          await this.inventoryProvider.createPackagedItem_BtoC(this.user,packagedItemBtoC,selPackagedItem);
          console.log("B->C added");
          //await this.inventoryProvider.updatePackagedItemStatus(selPackagedItem.id,myConstants.packageStatus.CO);
          await loading.dismiss();
          this.actionText = "NONE";
          this.viewCtrl.dismiss();
        }
        else
        {
          alert("Zero or More than one, B->C rules with RD status, exists for " + selPackagedItem.pRule.nextTaskCode );
        }
      }
      catch(e)
      {
        console.log("Error in inventoryProvider.getBtoCPackageRules " + e);
      };     
    }
  }

  doSplit()
  {
    console.log("Inside ItemDetailPage::doSplit()");    
    //let out_weight_1:number=0;
    let alert = this.alertCtrl.create();
    alert.setTitle(""+this.pItem.itemWeight);

    alert.addInput({type: 'number',placeholder:'Enter Weight',name: 'out_weight_1'});
    alert.addInput({type: 'number',placeholder:'Enter Weight',name: 'out_weight_2'});
    alert.addButton('Cancel');
    alert.addButton(
      {
        text: 'OK',
        handler: (data) => {
          console.log("data " + data.out_weight_1 + " " + data.out_weight_2);
          let total:number = +data.out_weight_1 + +data.out_weight_2;
          if(+total === +this.pItem.itemWeight)
          {
            let packagedItemBtoCList:Array<PackagedItem> = [];
            let packagedItemBtoC_1 = {} as PackagedItem;
            let packagedItemBtoC_2 = {} as PackagedItem;
            let ratio_1:number = +data.out_weight_1/+total;
            let ratio_2:number = +data.out_weight_2/+total;

            packagedItemBtoC_1 = JSON.parse(JSON.stringify(this.pItem));
            packagedItemBtoC_2 = JSON.parse(JSON.stringify(this.pItem));
            
            packagedItemBtoC_1.itemWeight = +(data.out_weight_1);
            packagedItemBtoC_1.trxShare = round(((ratio_1)*(+this.pItem.trxShare)),2);

            packagedItemBtoC_1.roughWeight = round(((ratio_1)*(+this.pItem.roughWeight)),2);
            packagedItemBtoC_1.taskCost = round(((ratio_1)*(+this.pItem.taskCost)),2);
            packagedItemBtoC_1.accumulatedTaskCost = round(((ratio_1)*(+this.pItem.accumulatedTaskCost)),2);
            
            packagedItemBtoC_2.itemWeight = +(data.out_weight_2);
            packagedItemBtoC_2.trxShare = round(((ratio_2)*(+this.pItem.trxShare)),2);
            
            packagedItemBtoC_2.roughWeight = round(((ratio_1)*(+this.pItem.roughWeight)),2);
            packagedItemBtoC_2.taskCost = round(((ratio_1)*(+this.pItem.taskCost)),2);
            packagedItemBtoC_2.accumulatedTaskCost = round(((ratio_1)*(+this.pItem.accumulatedTaskCost)),2);
            
            packagedItemBtoC_1.itemValuation.value = round((packagedItemBtoC_1.itemWeight * packagedItemBtoC_1.itemValuation.rate),2);
            packagedItemBtoC_1.itemValuation.interestCost = round((ratio_1*this.pItem.itemValuation.interestCost),2);
            packagedItemBtoC_1.itemValuation.totalCost = round((ratio_1*this.pItem.itemValuation.totalCost),2);
            packagedItemBtoC_1.itemValuation.profit = round((ratio_1*this.pItem.itemValuation.profit),2);
            if(packagedItemBtoC_1.itemValuation.totalCost > 0)
              packagedItemBtoC_1.itemValuation.profitMargin = round((packagedItemBtoC_1.itemValuation.profit/packagedItemBtoC_1.itemValuation.totalCost)*100,2);
            else
              packagedItemBtoC_1.itemValuation.profitMargin = 0;

            packagedItemBtoC_2.itemValuation.value = round((packagedItemBtoC_2.itemWeight * packagedItemBtoC_2.itemValuation.rate),2);
            packagedItemBtoC_2.itemValuation.interestCost = round((ratio_2*this.pItem.itemValuation.interestCost),2);
            packagedItemBtoC_2.itemValuation.totalCost = round((ratio_2*this.pItem.itemValuation.totalCost),2);
            packagedItemBtoC_2.itemValuation.profit = round((ratio_2*this.pItem.itemValuation.profit),2);
            if(packagedItemBtoC_2.itemValuation.totalCost > 0)
              packagedItemBtoC_2.itemValuation.profitMargin = round((packagedItemBtoC_2.itemValuation.profit/packagedItemBtoC_2.itemValuation.totalCost)*100,2);
            else
              packagedItemBtoC_2.itemValuation.profitMargin = 0;

            packagedItemBtoCList.push(packagedItemBtoC_1);
            packagedItemBtoCList.push(packagedItemBtoC_2);
            this.inventoryProvider.createPackagedItemArray_BtoC(this.user,packagedItemBtoCList,this.pItem)
            .then(result => {
              this.inventoryProvider.deletePackagedItem(this.user,this.pItem)
              .then(result => {
                console.log("packagedItem deleted : " + this.pItem.id);
              });
              this.viewCtrl.dismiss();
            }).catch(e => {
              console.log("Error from this.inventoryProvider.createPackagedItemArray_BtoC() " + e);
            });            
          }
        }
      });
    alert.present();
  }

}
