import { Component } from '@angular/core';
import { IonicPage, 
  NavController, NavParams,
  AlertController,
  Loading, LoadingController } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';
import { PackageRule } from '../../model/packageRule';

//import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { User, checkIfValid } from '../../model/user';


/**
 * Generated class for the PackageRulesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-package-rules',
  templateUrl: 'package-rules.html',
})
export class PackageRulesPage {
  private user:User;
  private packagRuleList:Observable<PackageRule[]>;
  private packageRulesCollectionRef: AngularFirestoreCollection<PackageRule>;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private settingsProvider: SettingsProvider) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackageRulesPage');
    this.packagRuleList = this.settingsProvider.getPackageRulesList(this.user).valueChanges();
    /*
    this.packagRuleList = this.inventoryProvider.getPackageRulesList().snapshotChanges().map(actions => {
      console.log("snapshotChanged");
      actions.map(a => {
        console.log("a:");
        const data = a.payload.doc.data() as packageRule;
        const id = a.payload.doc.id;
        return {id,data};
      })
    });*/

  }
  ionViewDidEnter() {
    console.log('ionViewDidEnter PackageRulesPage');    
  }

  /*
  initilizeData()
  {
    this.packagRuleList = [];
    this.databaseProvider.getPackageRules()
    .then(data => {
      this.packagRuleList = data;    

      this.packagRuleList.forEach(rule => {
          console.log("ID: " + rule.id + "\n"
          + "curGrouped: " + rule.bGroupRead + "\n"
          + "curTaskCode: " + rule.curTaskCode + "\n"
          + "packageStatus: " + rule.packageStatus + "\n"
          + "bMultipleOutput: " + rule.bMultipleOutput + "\n"
          + "bPartialAssign: " + rule.bPartialAssign + "\n"
          + "bReducedOutput: " + rule.bReducedOutput + "\n"
          + "bIncursCost: " + rule.bIncursCost + "\n") ;
      });      
    }); 
  }*/


  addRule()
  {
    console.log('HomePage::addRule() : ');
    let alert = this.alertCtrl.create({
      message: 'New Rule',
      inputs: [
        { name: 'curTaskCode',placeholder:'Current Task Code', type: 'boolean' },
      //  { name: 'packageStatus',placeholder:'Status',type: 'string' },
        { name: 'nextTaskCode',placeholder:'Next Task Code',type: 'string' },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          },
        },
        {
          text: 'Add',
          handler: data => {
            let p = {} as PackageRule;
            p.curTaskCode = data.curTaskCode;
            //p.packageStatus = data.packageStatus;
            p.nextTaskCode = data.nextTaskCode;
            p.bFirst = false;
            p.bGroupRead = false;
            p.bMultipleOutput = true;
            p.bPartialAssign = false;
            p.bReducedOutput = true;
            p.bIncursCost = true;
            p.bLast = false;
            p.sequenceID = 1;
            
            this.settingsProvider.createPackageRule(this.user,p).then((newDoc) => {
              console.log("Package Rule Created !!");
            });
          },
        },
      ],
    });
    alert.present();
  }

  async deleteRule(pID:string)
  {
    console.log("Welcome to PackageRulesPage:deleteRule(packageRule): " + pID);
    let pRule = {} as PackageRule;
    let loading: Loading = this.loadingCtrl.create();
    loading.present();

    try
    {
      pRule = <PackageRule>await this.settingsProvider.getPackageRule(this.user,pID);
      if(pRule)
      {//Rule exists
        await this.settingsProvider.deletePackageRule(this.user,pID);
        await loading.dismiss();   
      }
      else
      {//Rule does not exists
        await loading.dismiss();      
        alert("PackageRule " + pID + " does not exist");
      }
    }
    catch(e)
    {
      await loading.dismiss();   
    }
  }

  async updateRule(p:PackageRule)
  {
    console.log("Welcome to PackageRulesPage:updateRule(packageRule): " + p.id);
    await this.settingsProvider.updatePackageRule(this.user,p);
  }
}
