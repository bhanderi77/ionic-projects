import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DependencySummaryPage } from './dependency-summary';

@NgModule({
  declarations: [
    DependencySummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(DependencySummaryPage),
  ],
})
export class DependencySummaryPageModule {}
