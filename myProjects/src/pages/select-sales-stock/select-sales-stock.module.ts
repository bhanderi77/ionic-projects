import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectSalesStockPage } from './select-sales-stock';

@NgModule({
  declarations: [
    SelectSalesStockPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectSalesStockPage),
  ],
})
export class SelectSalesStockPageModule {}
