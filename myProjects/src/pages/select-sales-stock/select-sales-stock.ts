import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';

import { ItemShape,ItemQualityLevel, ItemSieve } from '../../model/packagedItem';
import { User, checkIfValid } from '../../model/user';


/**
 * Generated class for the SelectSalesStockPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-sales-stock',
  templateUrl: 'select-sales-stock.html',
})
export class SelectSalesStockPage {
  private user:User;  

  private polishShapeList:Array<ItemShape>;
  private polishSieveList:Array<ItemSieve>;
  private polishQualityLevelList:Array<ItemQualityLevel>;
  private iShape:ItemShape;
  private iSieveMin:ItemSieve;
  private iSieveMax:ItemSieve;
  private iQualityLevelMin:ItemQualityLevel;
  private iQualityLevelMax:ItemQualityLevel;
  
  

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private settingsProvider: SettingsProvider) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');
        
    this.polishShapeList = [];
    this.polishShapeList = navParams.get('shapeList');
    this.polishSieveList = [];
    this.polishSieveList = navParams.get('sieveList');
    this.polishQualityLevelList = [];
    this.polishQualityLevelList = navParams.get('qualityLevelList');

    this.iShape = {} as ItemShape;
    this.iSieveMin = {} as ItemSieve;
    this.iSieveMax = {} as ItemSieve;
    this.iQualityLevelMin = {} as ItemQualityLevel;
    this.iQualityLevelMax = {} as ItemQualityLevel;
    this.initializeData();      
  }

  async initializeData()
  {
    console.log("Welcome to SelectSalesStockPage:initializeData()");
    if(this.polishShapeList.length > 0)
    {
      this.iShape = this.polishShapeList[0];
    }
    if(this.polishSieveList.length > 0)
    {
      this.iSieveMin = this.polishSieveList[0];
      this.iSieveMax = this.polishSieveList[this.polishSieveList.length-1];
    }
    if(this.polishQualityLevelList.length > 0)
    {
      this.iQualityLevelMin = this.polishQualityLevelList[0];
      this.iQualityLevelMax = this.polishQualityLevelList[this.polishQualityLevelList.length-1];
    }
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectSalesStockPage');
  }

  changeInFilterCriteria()
  {
    console.log('Welcome to SelectSalesStockPage::changeInFilterCriteria()');
  }
  applyFilter()
  {
    console.log("Welcome to SelectPurchaseStockPage:applyFilter() ");
    console.log("Selected Shape " + this.iShape.id);
    console.log("Min Sieve " + this.iSieveMin.id +  " " + this.iSieveMin.rank);
    console.log("Max Sieve " + this.iSieveMax.id +  " " + this.iSieveMax.rank);
  
    this.viewCtrl.dismiss({iShape:this.iShape,iSieveMin:this.iSieveMin,iSieveMax:this.iSieveMax,iQualityLevelMin:this.iQualityLevelMin,iQualityLevelMax:this.iQualityLevelMax}); 
  }

}
