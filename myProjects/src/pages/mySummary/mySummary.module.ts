import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MySummaryPage } from './mySummary';
import { PipesModule } from '../../pipes/pipes.module';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    MySummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(MySummaryPage),
    PipesModule,
    ChartsModule
  ],
})
export class MySummaryPageModule {}
