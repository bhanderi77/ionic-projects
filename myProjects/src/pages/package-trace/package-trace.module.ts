import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackageTracePage } from './package-trace';

@NgModule({
  declarations: [
    PackageTracePage,
  ],
  imports: [
    IonicPageModule.forChild(PackageTracePage),
  ],
})
export class PackageTracePageModule {}
