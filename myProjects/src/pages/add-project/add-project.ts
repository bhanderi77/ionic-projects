import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';

import { Project } from '../../model/project';


/**
 * Generated class for the AddProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-project',
  templateUrl: 'add-project.html',
})
export class AddProjectPage {
  private startDate:string
  private endDate:string;
  private description:string;
  constructor(private navCtrl: NavController, private navParams: NavParams,
    private loadingCtrl: LoadingController,    
    private firebaseProvider:FirebaseProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddProjectPage');
  }

  async saveProject()
  {
    let p = {} as Project;
    p.id ="";
    p.startDate = this.startDate;
    p.endDate = this.endDate;
    p.description = this.description;

    console.log("Saving project:");
    console.log("startDate:" + p.startDate);
    console.log("endDate:" + p.endDate);
    console.log("description:" + p.description);

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
    await this.firebaseProvider.createProject(p);
    await loading.dismiss();
    this.navCtrl.pop();  
  }

}
