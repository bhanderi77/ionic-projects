import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinanceTransactionPage } from './financeTransaction';

@NgModule({
  declarations: [
    FinanceTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(FinanceTransactionPage),
  ],
})
export class FinanceTransactionPageModule {}
