import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Milestone } from '../../model/milestone';
import { Risk } from '../../model/risk';
import { Project } from '../../model/project';
import { myConstants } from '../../model/myConstants';


/**
 * Generated class for the AddRiskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-risk',
  templateUrl: 'add-risk.html',
})
export class AddRiskPage {

  //id:string;
  
  private risk:Risk;
  private milestone:Milestone;
  private project:Project;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,    
    private firebaseProvider:FirebaseProvider) 
  {
    this.risk = {} as Risk;
    this.risk.description ="";
    this.risk.impact = "";
    this.risk.mitigation = "";
    this.risk.plannedClosureDate = "";
    this.risk.estimatedClosureDate = "";
    this.risk.actualClosureDate = "";
    this.risk.status = "";
    this.risk.RAG = "";
    this.risk.remarks = "";
    this.risk.id = this.navParams.get('riskID');
    console.log("this.risk.id " + this.risk.id);
    
    this.project = {} as Project;
    this.project.id = this.navParams.get('projectID');
    this.milestone = {} as Milestone;
    this.milestone.id = this.navParams.get('milestoneID');
    
    if(this.risk.id)
    {
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
     
      this.firebaseProvider.getRisk(this.project.id,this.milestone.id,this.risk.id)
      .then(data => {
          this.risk = data;
          console.log("Risk Target Closure Date " + this.risk.estimatedClosureDate);
          loading.dismiss();
      });
    }

    if(this.project.id)
    {
      this.firebaseProvider.getProject(this.project.id)
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
      });
      
      if(this.milestone.id)
      {
        this.firebaseProvider.getMilestone(this.project.id,this.milestone.id)
        .then(data => {
            this.milestone = data;
            console.log("Milestone Estimated Closure Date " + this.milestone.estimatedClosureDate);
        });
      }   
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddRiskPage');
  }

  async saveRisk()
  {
    console.log("Inside saveRisk()");
    let today = new Date().toISOString();
    console.log("today " + today);
    if(this.risk.estimatedClosureDate < today)
    {
      alert("For Risk, Target Closure Date can not be < today");
    }
    else if(this.risk.estimatedClosureDate > this.milestone.estimatedClosureDate)
    {
      alert("Target Closure Date of RISK can not be >= Estimated Closure Date of respective MILESTONE");
    }
    else
    {
      /*
      let r = {} as Risk;
      r.description = this.description;
      r.impact = this.impact;
      r.mitigation = this.mitigation;
      r.plannedClosureDate = this.plannedClosureDate;
      r.estimatedClosureDate = r.plannedClosureDate;
      r.actualClosureDate = "";
      r.status = myConstants.Status.OPEN;
      r.RAG = myConstants.RAG.GREEN;
      r.remarks = ""; 
      */
      
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
      
      console.log("this.risk.id " + this.risk.id);
      if(this.risk.id === "")
      {
        this.risk.plannedClosureDate = "";
        this.risk.status = myConstants.Status.OPEN;
        //this.risk.RAG = myConstants.RAG.GREEN;
        this.risk.remarks = "";
       
        console.log("Calling firebaseProvider.createRisk");
        await this.firebaseProvider.createRisk(this.project.id,this.milestone.id,this.risk);
        loading.dismiss();
        this.navCtrl.pop();  
      }
      else
      {
        console.log("Calling firebaseProvider.updateRisk");
        console.log("this.risk.RAG " + this.risk.RAG);
        await this.firebaseProvider.updateRisk(this.project.id,this.milestone.id,this.risk);
        loading.dismiss();
        this.navCtrl.pop();
      }
    }
  }

}
