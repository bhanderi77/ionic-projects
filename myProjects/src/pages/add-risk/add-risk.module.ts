import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRiskPage } from './add-risk';

@NgModule({
  declarations: [
    AddRiskPage,
  ],
  imports: [
    IonicPageModule.forChild(AddRiskPage),
  ],
})
export class AddRiskPageModule {}
