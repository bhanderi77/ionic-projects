import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActionSummaryPage } from './action-summary';

@NgModule({
  declarations: [
    ActionSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(ActionSummaryPage),
  ],
})
export class ActionSummaryPageModule {}
