import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams,AlertController, ViewController ,Loading,LoadingController,PopoverController} from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';
import { Milestone } from '../../model/milestone';
import { Project } from '../../model/project';
import { myConstants } from '../../model/myConstants';
import { Action } from '../../model/action';

/**
 * Generated class for the ActionSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-action-summary',
  templateUrl: 'action-summary.html',
})
export class ActionSummaryPage {

  private milestone:Milestone;
  private project:Project;
  //private actionList:Observable<Action[]>;
  private actionList:Array<Action>;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private firebaseProvider:FirebaseProvider,
    private alertCtrl:AlertController,
    private loadingCtrl: LoadingController,
    private popOverCtrl:PopoverController,
    private viewCtrl: ViewController
  ) 
  {
    this.project = {} as Project;
    this.project.id = "";
    this.milestone = {} as Milestone;
    this.milestone.id = "";

    this.project.id = navParams.get('projectID');
    if(this.project.id)
    {
      this.firebaseProvider.getProject(this.project.id)
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
        this.milestone.id = this.navParams.get('milestoneID');  
        if(this.milestone.id)
        {
          console.log("Calling getActionListForMilestone");
          this.firebaseProvider.getActionListForMilestone(this.project.id,this.milestone.id).valueChanges()
          .subscribe(aList => {
            this.actionList = aList;
            console.log("actionList populated " + this.actionList.length);
          });      
        }
        else
        {
          console.log("Call getActionList for Project");
          this.firebaseProvider.getActionListForProject(this.project.id).valueChanges()
          .subscribe(aList => {
            this.actionList = aList;
            console.log("actionList populated " + this.actionList.length);
          });      
        }
      });
    }
    else
    {
     console.log("Get all actions across the projects");
    }  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ActionSummaryPage');
  }

  addAction()
  {
     const modal = this.modalCtrl.create('AddActionPage',{projectID:this.project.id,milestoneID:this.milestone.id,actionID:""});
      modal.present();
      modal.onDidDismiss((data) => {
      }); 
  }

  editAction(a:Action)
  {
     const modal = this.modalCtrl.create('AddActionPage',{projectID:this.project.id,milestoneID:this.milestone.id,actionID:a.id});
      modal.present();
      modal.onDidDismiss((data) => {
      }); 
  }

  setRAG(event,a:Action)
  {
    console.log("setRAG for action : " + a.description);
    let popover = this.popOverCtrl.create("SetRAGPage",{projectID: this.project.id,milestoneID:this.milestone.id, actionID:a.id});
    popover.present({
      ev: event
    });
  }

  async close(a:Action)
  {
    a.actualClosureDate = new Date().toLocaleDateString();
    a.status = myConstants.Status.CLOSED;

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
      
    console.log("Calling firebaseProvider.updateAction");
    await this.firebaseProvider.updateAction(this.project.id,this.milestone.id,a);
    loading.dismiss();
  }

  async open(a:Action)
  {
    a.actualClosureDate = "";
    a.status = myConstants.Status.OPEN;

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
      
    console.log("Calling firebaseProvider.updateAction");
    await this.firebaseProvider.updateAction(this.project.id,this.milestone.id,a);
    loading.dismiss();
  }

  goBack()
  {
    this.viewCtrl.dismiss();
  }

}