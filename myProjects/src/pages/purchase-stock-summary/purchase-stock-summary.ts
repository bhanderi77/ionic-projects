import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController, NavParams,AlertController,Loading,LoadingController } from 'ionic-angular';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';
import { FinanceTransaction } from '../../model/financeTransaction';
import { BusinessTransaction } from '../../model/businessTransaction';


import { taskSummary,summaryDetails } from '../../model/taskSummary';
import { PackagedItem } from '../../model/packagedItem';
import { PackageRule } from '../../model/packageRule';
import { myConstants,round } from '../../model/myConstants';
import { User, checkIfValid } from '../../model/user';

interface RoughStockSummary {
  totalWeight:number;
 // totalCost:number;
 // totalTax:number;
  totalInterest:number;
}

/**
 * Generated class for the PurchaseStockSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-purchase-stock-summary',
  templateUrl: 'purchase-stock-summary.html',
})
export class PurchaseStockSummaryPage {
  private user:User;
  private filterValue:string;

  private packagedItemList_AB:Array<PackagedItem>;
  private packagedItemList_BB:Array<PackagedItem>;
  private filteredItemList:Array<PackagedItem>;
  private roughStockSummary:RoughStockSummary;
  private currencyExchangeRate:number;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,    
    private navParams: NavParams,
    private inventoryProvider: InventoryProvider,
    private transactionProvider: TransactionProvider,
    private settingsProvider:SettingsProvider) {
    
    this.user = {} as User;
    this.user = navParams.get('User');
  
    //this.taskCode = myConstants.taskCode.ROUGH;
    this.packagedItemList_AB = [];
    this.packagedItemList_BB = [];
    this.resetSummary();   
    this.initializeData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PurchaseStockSummaryPage');
    
  }

  TrackBy(index: number, obj: any):any
  {
    console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  async initializeData()
  {
    console.log("Welcome to PurchaseStockSummaryPage::initializeData()");
    this.currencyExchangeRate = 1;
    this.currencyExchangeRate = await this.settingsProvider.getExchangeRate(this.user);
    
    this.filterValue = "FOR_PLANNING";

    this.inventoryProvider.getAll_AtoB_packagedItems(this.user).valueChanges().subscribe(pItemList => {
      console.log("Received AtoB items " + pItemList.length);
      //this.packagedItemList_AB = pItemList.filter(pItem => pItem.pRule.nextTaskCode !== myConstants.taskCode.SALES);
      this.packagedItemList_AB = pItemList;
      console.log("# of AtoB Item for Rough Stocks " + this.packagedItemList_AB.length);
      this.filterPackagedItems(this.filterValue);
      this.calculateInterest();
    }); 

    this.inventoryProvider.getAll_BtoB_packagedItems(this.user).valueChanges().subscribe(pItemList => {
      console.log("Received BtoB items " + pItemList.length);
      //this.packagedItemList_BB = pItemList.filter(pItem => pItem.pRule.nextTaskCode !== myConstants.taskCode.SALES);
      this.packagedItemList_BB = pItemList;
      console.log("# of BtoB Items for Rough Stocks " + this.packagedItemList_BB.length);
      this.filterPackagedItems(this.filterValue);
      this.calculateInterest();
    }); 
  }

  
  buildSummary()
  {
    console.log("Welcome to PurchaseStockSummaryPage::buildSummary()");
    this.roughStockSummary = {} as RoughStockSummary;
    this.roughStockSummary.totalWeight = 0;
    //this.roughStockSummary.totalCost = 0;
    //this.roughStockSummary.totalTax = 0;
    this.roughStockSummary.totalInterest = 0;
    
    for(let i=0;i<this.filteredItemList.length;i++)
    {
      this.roughStockSummary.totalWeight += +this.filteredItemList[i].itemWeight;
   //   this.roughStockSummary.taskCost += +this.filteredItemList[i].taskCost;
   //   this.roughStockSummary.accumulatedTaskCost += +this.filteredItemList[i].accumulatedTaskCost;
      this.roughStockSummary.totalInterest += +this.filteredItemList[i].itemValuation.interestCost;
    }
    
    this.roughStockSummary.totalWeight = round(this.roughStockSummary.totalWeight,2);
    //this.roughStockSummary.totalCost = round(this.roughStockSummary.totalCost,2);
    //this.roughStockSummary.totalTax = round(this.roughStockSummary.totalTax,2);
    this.roughStockSummary.totalInterest = round(this.roughStockSummary.totalInterest,2);
  }
  
  resetSummary()
  {
    console.log("Welcome to PurchaseStockSummaryPage::resetSummary()");
  
    this.roughStockSummary = {} as RoughStockSummary;
    this.roughStockSummary.totalWeight = 0;
    //this.roughStockSummary.totalCost = 0;
    //this.roughStockSummary.totalTax = 0;
    this.roughStockSummary.totalInterest = 0;
    
  }

  async calculateInterest()
  {
    console.log("Welcome to PurchaseStockSummaryPage::calculateInterest() ");
    let today: Date = new Date();
    let interestRealizedLocal:number = 0;
    let interestUnRealizedLocal:number = 0;
    let bizTrx = {} as BusinessTransaction;
        
    for(let i=0;i<this.filteredItemList.length;i++)
    {
      let interest:number=0;
      this.packagedItemList_AB[i].itemValuation.interestCost = 0;
      console.log("getBusinessTransaction for i " + i);
      //bizTrx = await 
      this.transactionProvider.getBusinessTransaction(this.user,this.filteredItemList[i].trxID)
      .then(businessTransaction => 
      {
        bizTrx = businessTransaction;
        let finTrxList:Array<FinanceTransaction> = [];
        this.transactionProvider.getFinanceTransactionsListForBizTrxID(this.user,bizTrx.ID)
        .then(trxList => 
        {
          finTrxList = trxList;
          //console.log("finTrxList.length " + finTrxList.length);
          interestRealizedLocal = 0;
          for(let j = 0;j<finTrxList.length;j++)
          {
            interestRealizedLocal += round((+finTrxList[j].trxInterestRealizedinLocal),2);
          //  console.log("finTrxList[j].trxInterestRealizedinLocal " + finTrxList[j].trxInterestRealizedinLocal + " interestRealizedLocal " + interestRealizedLocal);
          }
          interestUnRealizedLocal = 0;
          let dueDate: Date = (new Date(bizTrx.trxDueDate));
          let duration: number = today.valueOf() - dueDate.valueOf();
          let diffDays:number = Math.floor(duration / (1000 * 3600 * 24)); 
          if(bizTrx.trxCurrency === myConstants.localCurrency)
          {
            if(diffDays > 0)
            {
              interestUnRealizedLocal = round(((finTrxList[0].trxClosingBalance*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
            else
            {
              interestUnRealizedLocal = round(((finTrxList[0].trxClosingBalance*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
          }
          else if(bizTrx.trxCurrency === myConstants.foreignCurrency)
          {
            
            if(diffDays > 0)
            {
              interestUnRealizedLocal = round(((finTrxList[0].trxClosingBalance*this.currencyExchangeRate*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
            else
            {
              interestUnRealizedLocal = round(((finTrxList[0].trxClosingBalance*this.currencyExchangeRate*bizTrx.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
            }
          }

        //  let packagedItems:Array<PackagedItem> = [];
        //  packagedItems = this.packagedItemList.filter(pItem => ((pItem.trxPartyID === bizTrx.trxPartyID) && (pItem.trxID === bizTrx.ID)));
        //  if(packagedItems.length)

          let interestForBizTrx = round((+interestUnRealizedLocal + +interestRealizedLocal),2); 
          interest=round(((interestForBizTrx*this.filteredItemList[i].trxShare)/100.0),2)
          
          
          this.filteredItemList[i].itemValuation.interestCost = interest;
          this.buildSummary();          
        }).catch(e => {
          console.log("Error from this.firebaseProvider.getFinanceTransactionsListForBizTrxID() " + e);
        });
      }).catch(e => {
        console.log("Error from this.firebaseProvider.getBusinessTransaction " + e);
      });      
    }//end of FOR loop
  }


  
  filterPackagedItems(value)
  {
    console.log("Welcome to PurchaseStockSummaryPage::filterPackagedItems(): " + value);
   // console.log("this.packagedItemList.length " + this.packagedItemList.length);
    switch(value)
    {
      
      case "FROM_PURCHASE": {
        this.filteredItemList = this.packagedItemList_AB.filter(pItem => ((pItem.pRule.curTaskCode === myConstants.taskCode.PURCHASE)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "FOR_PLANNING": {
        this.filteredItemList = this.packagedItemList_AB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.PLANNING) && (pItem.packageStatus === myConstants.packageStatus.RD)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "IN_PLANNING": {
        this.filteredItemList = this.packagedItemList_BB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.PLANNING) && (pItem.packageStatus === myConstants.packageStatus.IP)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "FOR_MFG_ISSUE": {
        this.filteredItemList = this.packagedItemList_AB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_ISSUE) && (pItem.packageStatus === myConstants.packageStatus.RD)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "IN_MFG_ISSUE": {
        this.filteredItemList = this.packagedItemList_BB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_ISSUE) && (pItem.packageStatus === myConstants.packageStatus.IP)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "FOR_MFG_RETURN": {
        this.filteredItemList = this.packagedItemList_AB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.MFG_RETURN) && (pItem.packageStatus === myConstants.packageStatus.RD)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "FROM_MFG_RETURN": {
        this.filteredItemList = this.packagedItemList_AB.filter(pItem => ((pItem.pRule.curTaskCode === myConstants.taskCode.MFG_RETURN)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "FOR_ASSORTMENT": {
        this.filteredItemList = this.packagedItemList_AB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.ASSORTMENT) && (pItem.packageStatus === myConstants.packageStatus.RD)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "IN_ASSORTMENT": {
        this.filteredItemList = this.packagedItemList_BB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.ASSORTMENT) && (pItem.packageStatus === myConstants.packageStatus.IP)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      case "FOR_SALES": {
        this.filteredItemList = this.packagedItemList_AB.filter(pItem => ((pItem.pRule.nextTaskCode === myConstants.taskCode.SALES)));
        this.calculateInterest();
        this.buildSummary();
        break;
      }
      default:
    }
  }

  onPackageClicked(pSelectedItem:PackagedItem)
  {
    console.log("Welcome to PurchaseStockSummaryPage:onPackageClicked()");
    console.log("curTaskCode " + pSelectedItem.pRule.curTaskCode);
    console.log("nextTaskCode " + pSelectedItem.pRule.nextTaskCode);
    console.log("id " + pSelectedItem.id);

    this.navCtrl.push('ItemDetailPage',{User:this.user,pItem:pSelectedItem}); 
    
  }

  selectFilter()
  {
    const modal = this.modalCtrl.create('SelectPurchaseStockPage',{User:this.user});
    modal.present();
    modal.onDidDismiss((selectValue) => {
      this.filterValue = selectValue;      
      this.filterPackagedItems(this.filterValue);
    }); 
  }


}
