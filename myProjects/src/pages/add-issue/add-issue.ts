import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Milestone } from '../../model/milestone';
import { Issue } from '../../model/issue';
import { Project } from '../../model/project';
import { myConstants } from '../../model/myConstants';


/**
 * Generated class for the AddIssuePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-issue',
  templateUrl: 'add-issue.html',
})
export class AddIssuePage {

  //id:string;

  private milestone:Milestone;
  private project:Project;
  private issue:Issue;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,    
    private firebaseProvider:FirebaseProvider) 
  {

    this.issue = {} as Issue;
    this.issue.description ="";
    this.issue.impact = "";
    this.issue.raisedDate = "";
    this.issue.estimatedClosureDate = "";
    this.issue.actualClosureDate = "";
    this.issue.status = "";
    this.issue.RAG = "";
    this.issue.remarks = "";

    this.issue.id = this.navParams.get('issueID');
    console.log("this.issue.id " + this.issue.id);
    
    this.project = {} as Project;
    this.project.id = navParams.get('projectID');
    this.milestone = {} as Milestone;
    this.milestone.id = this.navParams.get('milestoneID');

    if(this.issue.id)
    {
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
     
      this.firebaseProvider.getIssue(this.project.id,this.milestone.id,this.issue.id)
      .then(data => {
          this.issue = data;
          console.log("Issue estimated Closure Date " + this.issue.estimatedClosureDate);
          loading.dismiss();
      });
    }

    if(this.project.id)
    {
      this.firebaseProvider.getProject(this.project.id)
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
      });
      
      if(this.milestone.id)
      {
        this.firebaseProvider.getMilestone(this.project.id,this.milestone.id)
        .then(data => {
            this.milestone = data;
            console.log("Milestone Estimated Closure Date " + this.milestone.estimatedClosureDate);
        });
      }   
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddIssuePage');
  }

  async saveIssue()
  {
    console.log("Inside saveIssue()");
    let today = new Date().toISOString();
    console.log("this.estimatedClosureDate " + this.issue.estimatedClosureDate);
    console.log("today " + today);
    console.log("this.estimatedClosureDate " + this.issue.estimatedClosureDate);
    if(this.issue.estimatedClosureDate < today)
    {
      alert("For Issue, Estimated Closure Date can not be <= today");
    }
    else if(this.issue.estimatedClosureDate > this.milestone.estimatedClosureDate)
    {
      alert("Estimated Closure Date of ISSUE can not be >= Estimated Closure Date of respective MILESTONE");
    }
    else
    {
      /*
      let i = {} as Issue;
      i.description = this.description;
      i.impact = this.impact;
      let today = new Date();
      i.raisedDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-"+today.getDate();
      console.log("getFullYear " + today.getFullYear());
      console.log("getMonth " + today.getMonth());
      console.log("getDay " + today.getDate());
      i.estimatedClosureDate = this.estimatedClosureDate;
      i.actualClosureDate = "";
      i.status = myConstants.Status.OPEN;
      console.log("this.RAG " + this.RAG);
      switch(this.RAG)
      {
        case "RAG_RED" : {i.RAG = myConstants.RAG.RED;break;}
        case "RAG_AMBER" : {i.RAG = myConstants.RAG.AMBER;break;}
        case "RAG_GREEN" : {i.RAG = myConstants.RAG.GREEN;break;}
      }  
      i.remarks = ""; 
      */
      let loading: Loading;
      loading = this.loadingCtrl.create();
      loading.present();
      
      console.log("this.issue.id " + this.issue.id);
      if(this.issue.id === "")
      {
        let today = new Date();
        this.issue.raisedDate = today.getFullYear()+"-"+(today.getMonth()+1)+"-"+today.getDate();
        this.issue.status = myConstants.Status.OPEN;
        this.issue.RAG = myConstants.RAG.GREEN;
        this.issue.remarks = "";
       
        console.log("Calling firebaseProvider.createIssue");
        await this.firebaseProvider.createIssue(this.project.id,this.milestone.id,this.issue); 
        loading.dismiss();
        this.navCtrl.pop();  
      }
      else
      {
        console.log("Calling firebaseProvider.updateIssue");
        console.log("this.risk.RAG " + this.issue.RAG);
        await this.firebaseProvider.updateIssue(this.project.id,this.milestone.id,this.issue);
        loading.dismiss();
        this.navCtrl.pop();
      }
    }
  }
}
