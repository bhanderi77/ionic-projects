import { Component } from '@angular/core';
import { IonicPage, NavController,AlertController, NavParams,ModalController,Loading,LoadingController,PopoverController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';
import { Project } from '../../model/project';
import { Milestone } from '../../model/milestone';
import { Issue } from '../../model/issue';
import { myConstants } from '../../model/myConstants';
import { AddRAIDPage } from '../../pages/add-RAID/add-RAID';
/**
 * Generated class for the ProjectSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-project-summary',
  templateUrl: 'project-summary.html',
})
export class ProjectSummaryPage {

  private projectID:string;
  private project:Project;
  //private milestoneList:Observable<Milestone[]>;
  private milestoneList:Array<Milestone>;
  private today:string;
  

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public popOverCtrl: PopoverController,
    private firebaseProvider:FirebaseProvider    
  ) {
    this.project = {} as Project;
    this.project.id = navParams.get('projectID');
    this.projectID = navParams.get('projectID'); 
    console.log("Project ID : " + this.projectID);

    this.milestoneList = [];
    
    this.today = new Date().toISOString(); 
    console.log("today:" + this.today);
    
    this.firebaseProvider.getProject(navParams.get('projectID'))
    .then(data => {
      this.project = data;
      console.log("Project End Date " + this.project.endDate);

      this.firebaseProvider.getMilestoneList(this.projectID).valueChanges().subscribe(mList => {
        this.milestoneList = mList;
        this.milestoneList.forEach(m => {
          let etc:Date;
          etc = new Date(m.estimatedClosureDate);
          let diff = Math.floor(((new Date()).valueOf() - etc.valueOf()) / (1000 * 3600 * 24));
          console.log("diff " + diff);
          if(diff > 2)
          {
            m.RAG = myConstants.RAG.RED;
          }
          else if(diff > 0)
          {
            m.RAG = myConstants.RAG.AMBER;          
          }
        });
      });
    });
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProjectSummaryPage');
  }

  addMilestone() 
  {
    const modal = this.modalCtrl.create('AddMilestonePage',{projectID:this.project.id});
      modal.present();
      modal.onDidDismiss((data) => {
      });
    /*
    let alert = this.alertCtrl.create(
      {
        title: "Add New Milestone",
        inputs: [ { name: 'description', placeholder: 'Description'},
                  { name: 'targetClosureDate', placeholder: 'Target Closure Date'}
                ],
        buttons:
                [ { 
                    text: 'Cancel',
                    handler : data => { console.log('Cancel clicked');}
                  },
                  { 
                    text: 'Ok',
                    handler : data => 
                    {
                      if(data.targetClosureDate > this.project.endDate)
                      {
                        return false;
                      }
                      else
                      {
                        let m = {} as Milestone;
                        m.description =  data.description;
                        m.targetClosureDate = data.targetClosureDate;
                        m.estimatedClosureDate = m.targetClosureDate;
                        m.actualClosureDate = "";
                        m.completionPercentage = 0;
                        m.RAG = myConstants.RAG.GREEN;
                        m.remarks = "";      
                        
                        this.firebaseProvider.createMilestone(this.project.id,m);
                      }
                    }
                  }
                ]
      });
    alert.present();
    */
  }

  addRAID(event,milestone:Milestone)
  {
    console.log("addRAID for milestone : " + milestone.description);
    let popover = this.popOverCtrl.create("AddRAIDPage",{projectID: this.project.id,milestoneID:milestone.id});
    popover.present({
      ev: event
    });
  }

  gotoRiskSummary(m:Milestone)
  {
    if(m)
    {
      console.log("gotoRiskSummary for milestone");
      const modal = this.modalCtrl.create('RiskSummaryPage',{projectID:this.project.id,milestoneID:m.id});
        modal.present();
        modal.onDidDismiss((data) => {
        });
    }
    else
    {
      console.log("gotoRiskSummary for all");
      const modal = this.modalCtrl.create('RiskSummaryPage',{projectID:this.project.id,milestoneID:""});
        modal.present();
        modal.onDidDismiss((data) => {
        });
    }
  }
  gotoActionSummary(m:Milestone)
  {
    if(m)
    {
      const modal = this.modalCtrl.create('ActionSummaryPage',{projectID:this.project.id,milestoneID:m.id});
        modal.present();
        modal.onDidDismiss((data) => {
        });
    }
    else
    {
      const modal = this.modalCtrl.create('ActionSummaryPage',{projectID:this.project.id,milestoneID:""});
        modal.present();
        modal.onDidDismiss((data) => {
        });
    } 
  }
  gotoIssueSummary(m:Milestone)
  {
    if(m)
    {
      const modal = this.modalCtrl.create('IssueSummaryPage',{projectID:this.project.id,milestoneID:m.id});
        modal.present();
        modal.onDidDismiss((data) => {
        });
    }
    else
    {
      const modal = this.modalCtrl.create('IssueSummaryPage',{projectID:this.project.id,milestoneID:""});
      modal.present();
      modal.onDidDismiss((data) => {
      });
    }
  }
  gotoDependencySummary(m:Milestone)
  {
    if(m)
    {
      const modal = this.modalCtrl.create('DependencySummaryPage',{projectID:this.project.id,milestoneID:m.id});
        modal.present();
        modal.onDidDismiss((data) => {
        });
    }
    else
    {
      const modal = this.modalCtrl.create('DependencySummaryPage',{projectID:this.project.id,milestoneID:""});
      modal.present();
      modal.onDidDismiss((data) => { 
      });
    }
  }

}
