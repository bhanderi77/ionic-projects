import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectSummaryPage } from './project-summary';

@NgModule({
  declarations: [
    ProjectSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectSummaryPage),
  ],
})
export class ProjectSummaryPageModule {}
