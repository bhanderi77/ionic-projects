import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams,AlertController, ViewController ,Loading,LoadingController,PopoverController} from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Observable } from 'rxjs/Observable';
import { Milestone } from '../../model/milestone';
import { Project } from '../../model/project';
import { Issue } from '../../model/issue';
import { myConstants } from '../../model/myConstants';


/**
 * Generated class for the IssueSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-issue-summary',
  templateUrl: 'issue-summary.html',
})
export class IssueSummaryPage {

  private milestone:Milestone;
  private project:Project;
  //private issueList:Observable<Issue[]>;
  private issueList:Array<Issue>;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private firebaseProvider:FirebaseProvider,
    private alertCtrl:AlertController,
    private loadingCtrl: LoadingController,
    private popOverCtrl:PopoverController,
    private viewCtrl: ViewController
  ) 
  {
    this.project = {} as Project;
    this.project.id = "";
    this.milestone = {} as Milestone;
    this.milestone.id = "";

    this.project.id = navParams.get('projectID');
    if(this.project.id)
    {
      this.firebaseProvider.getProject(this.project.id)
      .then(data => {
        this.project = data;
        console.log("Project End Date " + this.project.endDate);
        this.milestone.id = this.navParams.get('milestoneID');  
        if(this.milestone.id)
        {
          console.log("Calling getIssueListForMilestone");
          this.firebaseProvider.getIssueListForMilestone(this.project.id,this.milestone.id).valueChanges()
          .subscribe(iList => {
            this.issueList = iList;
            console.log("issueList populated " + this.issueList.length);
          });      
        }
        else
        {
          console.log("Call getIssueListForProject");
          this.firebaseProvider.getIssueListForProject(this.project.id).valueChanges()
          .subscribe(iList => {
            this.issueList = iList;
            console.log("issueList populated " + this.issueList.length);
          });      

        }
      });
    }
    else
    {
     console.log("Get all issues across the projects");
    }  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IssueSummaryPage');
  }

  addIssue()
  {
     const modal = this.modalCtrl.create('AddIssuePage',{projectID:this.project.id,milestoneID:this.milestone.id,issueID:""});
      modal.present();
      modal.onDidDismiss((data) => {
      }); 
  }

  editIssue(i:Issue)
  {
     const modal = this.modalCtrl.create('AddIssuePage',{projectID:this.project.id,milestoneID:this.milestone.id,issueID:i.id});
      modal.present();
      modal.onDidDismiss((data) => {
      }); 
  }

  setRAG(event,i:Issue)
  {
    console.log("setRAG for issue : " + i.description);
    let popover = this.popOverCtrl.create("SetRAGPage",{projectID: this.project.id,milestoneID:this.milestone.id, issueID:i.id});
    popover.present({
      ev: event
    });
  }

  async close(i:Issue)
  {
    i.actualClosureDate = new Date().toISOString();
    i.status = myConstants.Status.CLOSED;

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
      
    console.log("Calling firebaseProvider.updateIssue");
    await this.firebaseProvider.updateIssue(this.project.id,this.milestone.id,i);
    loading.dismiss();
  }

  async open(i:Issue)
  {
    i.actualClosureDate = "";
    i.status = myConstants.Status.OPEN;

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
      
    console.log("Calling firebaseProvider.updateIssue");
    await this.firebaseProvider.updateIssue(this.project.id,this.milestone.id,i);
    loading.dismiss();
  }

  goBack()
  {
    this.viewCtrl.dismiss();
  }

}
