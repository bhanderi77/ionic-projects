import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IssueSummaryPage } from './issue-summary';

@NgModule({
  declarations: [
    IssueSummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(IssueSummaryPage),
  ],
})
export class IssueSummaryPageModule {}
