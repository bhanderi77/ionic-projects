import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PasscodeRegisterPage } from './passcodeRegister';

@NgModule({
  declarations: [
    PasscodeRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(PasscodeRegisterPage),
  ],
})
export class PasscodeRegisterPageModule {}
