import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Loading,LoadingController } from 'ionic-angular';

import { SettingsProvider } from '../../providers/settings/settings';

import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { FinanceSettings } from '../../model/referenceData';
import { User, checkIfValid } from '../../model/user';

/**
 * Generated class for the AppSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageAppSettings',
  templateUrl: 'appSettings.html',
})
export class AppSettingsPage {

  
  private formGroup: FormGroup;
  private foriegnCurrency: AbstractControl;
  private localCurrency: AbstractControl;
  private conversionRate: AbstractControl;
  private dueDays: AbstractControl;
  private CGST: AbstractControl;
  private SGST: AbstractControl;
  private IGST: AbstractControl;
  
  private editFinanceSettings: FinanceSettings;
  private financeSettings: FinanceSettings;
  private user:User;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, 
    //private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private settingsProvider: SettingsProvider,
    public formBuilder: FormBuilder) {

      //this.editAppSettings = {foreignCurrency:'USD',localCurrency:'INR',exchangeRate:null,dueDays:null,cGSTRate:null,sGSTRate:null,iGSTRate:null};
      this.formGroup = formBuilder.group({
        foriegnCurrency:['',Validators.required],
        localCurrency:['',Validators.required],        
        conversionRate:['',Validators.required],
        dueDays:['',Validators.required],
        CGST:['',Validators.required],
        SGST:['',Validators.required],
        IGST:['',Validators.required]
      });
  
      this.foriegnCurrency = this.formGroup.controls['foriegnCurrency'];
      this.localCurrency = this.formGroup.controls['localCurrency'];
      this.conversionRate = this.formGroup.controls['conversionRate'];
      this.dueDays = this.formGroup.controls['dueDays'];
      this.CGST = this.formGroup.controls['CGST'];
      this.SGST = this.formGroup.controls['SGST'];
      this.IGST = this.formGroup.controls['IGST'];

      this.financeSettings = {} as FinanceSettings;
      this.editFinanceSettings = {} as FinanceSettings;
      this.user = {} as User;
      this.editFinanceSettings = navParams.get('orgSettings');  //RB13052018
      this.user = navParams.get('User');  
      
      /*
      this.databaseProvider.getDatabaseState().subscribe(rdy => {
        console.log("inside HomePage::constructor -> rdy " + rdy);
        if (rdy) {
          this.loadAppSettings();
        }
      });
      */
  }

  loadAppSettings() {
    /*
    this.databaseProvider.getAppSettings()
      .then(data => {
        this.editAppSettings = data;
      });
    */
  }
  goToHomePage(){
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() 
  {
    if(!checkIfValid(this.user))
    {
      alert("Invalid UserID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      this.viewCtrl.dismiss();
    }    
  }


  async updateAppSettings(){
   
    this.financeSettings.foreignCurrency = <string> this.foriegnCurrency.value;
    this.financeSettings.localCurrency = <string> this.localCurrency.value;
    this.financeSettings.exchangeRate = <number> this.conversionRate.value;
    this.financeSettings.dueDays = <number> this.dueDays.value;
    this.financeSettings.cGSTRate = <number> this.CGST.value;
    this.financeSettings.sGSTRate = <number> this.SGST.value;
    this.financeSettings.iGSTRate = <number> this.IGST.value;
    /*
    this.databaseProvider.updateAppSettings(this.appSettings).then(res => {
      this.goToHomePage();
    });
    */
    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
    await this.settingsProvider.updateFinanceSettings(this.user,this.financeSettings);
    loading.dismiss();
    this.viewCtrl.dismiss();  
      
  }
}
