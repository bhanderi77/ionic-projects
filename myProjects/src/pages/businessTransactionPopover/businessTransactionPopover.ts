import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { businessTransaction, businessTrxSummary, getPartySummary, partySummary, AppSettings, number2text } from '../../model/interface';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
//import { DatabaseProvider } from '../../providers/database/database';
import { TransactionProvider } from '../../providers/transaction/transaction';

import { SettingsProvider } from '../../providers/settings/settings';
import { EntityProvider } from '../../providers/entity/entity';

import { ViewController } from 'ionic-angular/navigation/view-controller';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { DatePipe } from '@angular/common';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
//import { SocialSharing } from '@ionic-native/social-sharing';
import { BusinessContact } from '../../model/businessContact';
import { BusinessTransaction } from '../../model/businessTransaction';
import { PartySummary,BusinessTransactionSummary } from '../../model/partySummary';
import { number2text } from '../../model/utility';
import { User,checkIfValid } from '../../model/user';
import { Organisation } from '../../model/organisation';
import { App } from 'ionic-angular/components/app/app';
import { myConstants } from '../../model/myConstants';


/**
 * Generated class for the PartySummaryPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageBusinessTransactionPopover',
  templateUrl: 'businessTransactionPopover.html',
})
export class BusinessTransactionPopoverPage {

  private trxSummary: BusinessTransactionSummary;
  private profile: BusinessContact;
  private user:User;
  private orgDetails: Organisation;
  private pdfObj: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl:AlertController,
    private transactionProvider: TransactionProvider,
    private settingsProvider: SettingsProvider,
    private entityProvider:EntityProvider,
    private viewCtrl: ViewController,
    public modalCtrl: ModalController,
    public appCtrl: App,
    private platform: Platform,
    private file: File,
    private fileOpener: FileOpener,
    public datepipe: DatePipe)
  {
    this.trxSummary = this.navParams.get('trxSummary');
    this.profile = this.navParams.get('profile');
    //this.orgDetails = this.navParams.get('orgDetails');
    //this.orgSettings = this.navParams.get('orgSettings');

    this.user = {} as User;
    this.user = this.navParams.get('User');
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad PartySummaryPopoverPage');
    
    if(!checkIfValid(this.user))
    {
      alert("Invalid userID : " + this.user.ID + " and/or organisationID : " + this.user.customerID);
      this.viewCtrl.dismiss();
    }
  }

  ionViewWillEnter()
  {
    console.log('ionViewWillEnter PartySummaryPopoverPage');
    this.loadOrgDetails();
  //  this.trxSummary = this.navParams.get('trxSummary');
  //  this.profile = this.navParams.get('profile');
  }


  deleteBusinessTransaction()
  {
     console.log("Inside partySummaryPopover:deleteBusinessTransaction() " + this.trxSummary.businessTransaction.trxPartyID + this.trxSummary.businessTransaction.ID);
    let confirm = this.alertCtrl.create(
      {
        title: 'Delete ' + this.trxSummary.businessTransaction.trxWeight + 'cts x ' + this.trxSummary.businessTransaction.trxRate + this.trxSummary.businessTransaction.trxCurrency,
        message: 'Do you want to delete this transaction dated ' + this.trxSummary.businessTransaction.trxDate + '?',
        buttons: [
          {text: 'No'},
          {
            text: 'Yes',
            handler: () => {
              this.transactionProvider.deleteBusinessTransaction(this.user, this.trxSummary.businessTransaction.ID)
              .then(res => {
                console.log("Business Trx Deleted : " + res);
                this.viewCtrl.dismiss('true');
              });
            }
          }
        ]
      });
    confirm.present();
  }

  compilemsg(index):string{
    var msg = "Hello!!!" ;
    return msg.concat(" \n -Sent from my Awesome App !");
  }

  editBusinessTransaction(){
    /*
    this.viewCtrl.dismiss();
    // let modal = this.modalCtrl.create('BusinessTransactionPage',{trxSummary:this.trxSummary});
    // modal.present();
    // console.log("Inside editBusinessTransaction()");
    //whatsappShare(index){
      let index=1;
      var msg  = this.compilemsg(index);
       this.socialSharing.shareViaWhatsApp(msg, null, null);
     //}
    */
  }

  async loadOrgDetails()
  {
    if(checkIfValid(this.user))
   {
    this.orgDetails = await this.entityProvider.getOrganisationDetails(this.user);
   // setTimeout(3000);
   }
  }

  generateInvoice(){
    this.viewCtrl.dismiss();
    console.log('inside generateInvoice()');
    let datepipe:DatePipe = new DatePipe('en-US');
    let dd = {
       content: [
          {text: 'TAX INVOICE', bold:true , alignment:'center' , fontSize : 15 }, 
          '\n',
          '\n',
          {
            columns:[
              {text: 'INV NO: ' + this.trxSummary.businessTransaction.trxInvoiceID, bold:true , alignment:'left' , fontSize : 11},
              '\t',
              {text: 'Date: ' + datepipe.transform(this.trxSummary.businessTransaction.trxDate,'dd-MM-yyyy'), bold:true , alignment:'left' , fontSize : 11},
            ]
          },
          '\n',
          '\n',
          {
            columns:[
              {text: 'M/S. ' + this.profile.fullName, bold:true , alignment:'left' , fontSize : 11},
              '\t',
              {text: 'TERMS: ' + this.trxSummary.businessTransaction.trxDueDays, bold:true , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              {text: this.profile.addressLine1, bold:false , alignment:'left' , fontSize : 11},
              '\t',
              {text: 'OUR BANKERS: ' , bold:true , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              {text: this.profile.addressLine2, bold:false , alignment:'left' , fontSize : 11},
              '\t',
              {text: this.orgDetails.bankName , bold:false , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              {text: this.profile.city + '-' + this.profile.pincode, bold:false , alignment:'left' , fontSize : 11},
              '\t',
              {text: this.orgDetails.bankBranch , bold:false , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              {text: '' , bold:false , alignment:'left' , fontSize : 11},
              '\t',
              {text: this.orgDetails.city +' - '+ this.orgDetails.pinCode , bold:false , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              '\t',
              '\t',
              {text: 'CURRENT A/C.NO.: ' + this.orgDetails.bankAccountID , bold:true , alignment:'left' , fontSize : 13},
            ]
          },
          {
            columns:[
              {text: 'State Code: ' + this.profile.stateRegion, bold:true , alignment:'left' , fontSize : 11},
              '\t',
              {text: 'IFSC CODE: ' +this.orgDetails.bankIFSC , bold:true , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              {text: 'GSTIN: ' + this.profile.GSTIN, bold:true , alignment:'left' , fontSize : 11},
              '\t',
              {text: 'GSTIN: ' + this.orgDetails.GSTIN, bold:true , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              {text: 'PAN: ' + this.profile.PAN, bold:true , alignment:'left' , fontSize : 11},
              '\t',
              {text: 'PAN: ' + this.orgDetails.PAN, bold:true , alignment:'left' , fontSize : 11},
            ]
          },
          {
            columns:[
              {text: 'Place Of Supply: ' + this.profile.city, bold:true , alignment:'left' , fontSize : 11}
            ]
          },
          {
            alignment: 'center',
            table: {
              headerRows: 0,
              widths:['10%','20%','15%','15%','20%','20%'],
              body: 
              [
                //buildBusinessTrxBody(),
                [
                  {text : 'Sr. No.', fontSize : 11, bold:true},
                  {text : 'DESCRIPTION OF GOODS',fontSize : 11, bold:true},
                  {text : 'HSN CODE', fontSize : 11,bold:true},
                  {text : 'WEIGHT IN CTS', fontSize : 11,bold:true},
                  {text : 'RATE PER CTS', fontSize : 11,bold:true},
                  {text : 'AMOUNT', fontSize : 11,bold:true},
                ]
              ]
            }
          },
          displayBizTrxDetails(this.trxSummary.businessTransaction),
          {
            alignment: 'center',
            table: {
              headerRows: 0,
              widths:['10%','20%','15%','15%','20%','20%'],
              body: 
              [
                //buildBusinessTrxBody(),
                [
                  {text : '', fontSize : 11, bold:true},
                  {text : '',fontSize : 11, bold:true},
                  {text : 'TOTAL', fontSize : 11,bold:true},
                  {text : this.trxSummary.businessTransaction.trxWeight, fontSize : 11,bold:true},
                  {text : '', fontSize : 11,bold:true},
                  {text : this.trxSummary.businessTransaction.trxValue, fontSize : 11,bold:true},
                ]
              ]
            }
          },
          displayBizTrxTaxDetails(this.trxSummary.businessTransaction,this.profile,this.orgDetails),
          {
            alignment: 'center',
            table: {
              headerRows: 0,
              widths:['10%','20%','15%','15%','20%','20%'],
              body: 
              [
                //buildBusinessTrxBody(),
                [
                  {text : '', fontSize : 11, bold:true},
                  {text : '',fontSize : 11, bold:true},
                  {text : '', fontSize : 11,bold:true},
                  {text : '', fontSize : 11,bold:true},
                  {text : 'TOTAL TAX', fontSize : 11,bold:true},
                  {text : this.trxSummary.businessTransaction.trxTaxAmount.totalAmount, fontSize : 11,bold:true},
                ]
              ]
            }
          },
          {
            alignment: 'center',
            table: {
              headerRows: 0,
              widths:['10%','20%','15%','15%','20%','20%'],
              body: 
              [
                //buildBusinessTrxBody(),
                [
                  {text : '', fontSize : 11, bold:true},
                  {text : '',fontSize : 11, bold:true},
                  {text : '', fontSize : 11,bold:true},
                  {text : '', fontSize : 11,bold:true},
                  {text : 'GRAND TOTAL', fontSize : 11,bold:true},
                  {text : (this.trxSummary.businessTransaction.trxGrossAmount), fontSize : 11,bold:true},
                ]
              ]
            }
          },
          {
            table: {
              alignment: 'center',
              headerRows: 0,
              widths:['100%'],
              body:
              [
                [
                  {text : 'TOTAL AMOUNT : ' + number2text(this.trxSummary.trxLocalAmount),alignment:'center', fontSize : 11,bold:true},
                ]
              ]
            }
          },
          {
            columns:[
                {text: 'PAYMENT INSTRUCTIONS:  ', bold:true , alignment:'left' , fontSize : 11}
              ]
          },
          {
            columns:[
                {text: 'PLEASE PAY IN FAVOUR OF :-' + '"' + this.orgDetails.name + '",' + this.orgDetails.bankBranch + ',' + this.orgDetails.city + '-' + this.orgDetails.pinCode , bold:true , alignment:'left' , fontSize : 11}
              ]
          },
          {
            columns:[
                {text: 'CURRENT A/C.NO.: ' + this.orgDetails.bankAccountID, bold:true , alignment:'left' , fontSize : 11},
                {text: 'IFSC CODE: ' +this.orgDetails.bankIFSC, bold:true , alignment:'left' , fontSize : 11}
              ]
          },
          '\n',
          '\n',
          {
            columns:[
                {text: '“I/we hereby certify that my/our registration Certificate under GST Tax Act, 2017 is in force on the date on which sale of goods specified in this tax invoice is made by me/us and that the transaction of sale covered by this tax invoice has been effected by me/us and it shall be accounted for in the turnover of sales while filing of return and the due tax, if any, payable on the sale has been paid or shall be paid. ”', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: 'The diamonds herein invoiced have been purchased from legitimate sources not involved in funding conflict and in complete with United Nations resolutions. The seller hereby guarantee that these diamonds are conflict free based on personal knowledge and/ or written guarantees provided by the supplier of these diamonds.', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: 'The diamonds herein invoiced are natural diamonds. We guarantee the originality of the diamonds supplied are free from synthetic or treated diamonds.', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: 'To  the  best  of  our  knowledge  and/or  written  assurance  from  our  supplier, we  state that "Diamonds invoiced have not been otained in violation of applicable National Laws and/or sanctioned by the US Department of Treasury'+"'s"+'Office of Foreign Assets Control (OFAC) and have not Originated from the Marange Resources of Zimbabawe.', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: 'The diamonds herein invoiced are exclusively of natural origin and untreated based on personal knowledgeand/or written guarantees provided by the supplier of these diamonds.', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: '"The acceptance of goods herein invoiced will be as per WFDB guidelines."', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: 'SUBJECT TO MUMBAI JURISDICATION.', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: 'GOODS SOLD AND DELIVERY AT MUMBAI', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          {
            columns:[
                {text: 'INTEREST AT 18% PER YEAR WILL BE CHARGED ON ALL A/C REMAINING UNPAID AFTER DUE DATE.', bold:true , alignment:'left' , fontSize : 6}
              ]
          },
          '\n',
          '\n',
          {
            columns:[
                {text: 'CUSTOMERS SIGN  & SEAL', bold:true , alignment:'left' , fontSize : 10},
                '\t',
                {text: 'FOR ' + this.orgDetails.name, bold:true , alignment:'center' , fontSize : 10}
              ]
          },
          '\n',
          '\n',
          '\n',
          '\n',
          {
            columns:[
                {text: '________________', bold:true , alignment:'left' , fontSize : 10},
                '\t',
                {text: 'PARTNER', bold:true , alignment:'center' , fontSize : 10}
              ]
          }
      ]
    }
    this.pdfObj = pdfMake.createPdf(dd);
    let pdfName = this.profile.fullName+'.pdf';
    if (this.platform.is('cordova')) {
      console.log('is Plt ios? : ' + this.platform.is('ios'));
      this.pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });

        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory,pdfName, blob, { replace: true })
        .then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory+ pdfName, 'application/pdf');
        })
      });
    } 
    else
    {
     // On a browser simply use download!
      this.pdfObj.download();
    }

    // function displayBizTrx(businessTransaction){
    //   console.log('inside displayBizTrx');
    //   let recs = [];
    //   recs.push(displayBizTrxDetails(businessTransaction));
    //   return recs;
    // }
    function displayBizTrxDetails(businessTransaction){
      console.log('inside displayBizTrxDetails');
      return {
        alignment: 'center',
        table: {
            headerRows: 0,
            widths:['10%','20%','15%','15%','20%','20%'],
            body:buildBusinessTrxDetailsBody(businessTransaction) 
        }
      };
    }

    function buildBusinessTrxDetailsBody(businessTransaction)
    {
      let body = [];
      let i=1;
      console.log('inside buildBusinessTrxDetailsBody');
      businessTransaction.trxItems.forEach(function(row){
        let dataRow = [];
        dataRow.push(i);
        dataRow.push(row.itemRemarks);
        dataRow.push('71023910');
        dataRow.push(row.itemQty);
        dataRow.push(row.itemRate);
        dataRow.push(row.itemValue);
        body.push(dataRow);
        i=i+1;
      });
      return body;
    }

    function displayBizTrxTaxDetails(businessTransaction,profile,orgDetails){
      console.log('inside displayBizTrxTaxDetails');
      return {
        alignment: 'center',
        table: {
            headerRows: 0,
            widths:['10%','20%','15%','15%','20%','20%'],
            body:buildBusinessTrxTaxDetailsBody(businessTransaction,profile,orgDetails) 
        }
      }; 
    }

    function buildBusinessTrxTaxDetailsBody(businessTransaction,profile,orgDetails)
    {
      let body=[];
      
      if(profile.stateRegion === orgDetails.stateCode)
      {
        console.log('profile and org state are same');
        console.log('start of for loop');
        for(let i=0; i<2;i++)
        {
          let dataRow = [];
          console.log('Inside For Loop, value of i=> ' + i);
          dataRow.push('');
          dataRow.push('');
          dataRow.push('');
          dataRow.push('');
          if(i === 0)
          {
            console.log('displaying cgst');
            dataRow.push('CGST @ 0.125%');
            dataRow.push(businessTransaction.trxCGSTValue);
            body.push(dataRow);
          }
          else
          {
            console.log('displaying sgst');
            dataRow.push('SGST @ 0.125%');
            dataRow.push(businessTransaction.trxSGSTValue); 
            body.push(dataRow);
          }
        }
        console.log('End of for Loop');
      }
      else
      {
        console.log('profile and org state are different');
        let dataRow = [];
        dataRow.push('');
        dataRow.push('');
        dataRow.push('');
        dataRow.push('');
        dataRow.push('IGST @ 0.25%');
        dataRow.push(businessTransaction.trxIGSTValue); 
        body.push(dataRow);
      }
      return body;
    }
  }

  generateSummary(){
    this.viewCtrl.dismiss();
    let dd={
      content:[
        displayInvoiceHeader(this.trxSummary),
        '\n',
        displayBusinessTrxDetails1(this.trxSummary),
        displayBusinessTrxDetails2(this.trxSummary),
        displayBusinessTrxDetails3(this.trxSummary),
        '\n',
        displayBusinessTrxFooterDetails1(this.trxSummary,this.profile),
        displayBusinessTrxFooterDetails2(this.trxSummary),
        '\n',
        displayFinanceHeader(),
        displayFinanceTrxDetails(this.trxSummary.finTrxs,this.trxSummary.businessTransaction.trxType),
      ]
    }

    function displayInvoiceHeader(trxSummary){
      if((trxSummary.diffDays>0) && (trxSummary.amtPendinginLocal>0))
      {
        return {
          alignment: 'center',
          table: {
              headerRows: 0,
              widths:['100%'],
              body: buildInvoiceHeaderBody(trxSummary)
          },
          layout: {
            fillColor: function (i, node) {
              return (true) ? '#F44336' : null;
            }
          }
        };
      }
      else
      {
        return {
          alignment: 'center',
          table: {
              headerRows: 0,
              widths:['100%'],
              body: buildInvoiceHeaderBody(trxSummary)
          },
          layout: {
            fillColor: function (i, node) {
              return (true) ? '#4CAF50' : null;
            }
          }
        };
      } 
    }

    function displayBusinessTrxDetails1(trxSummary){
      return {
        alignment: 'center',
        table: {
            headerRows: 0,
            widths:['33%','34%','33%'],
            body: buildBusinessTrxDetails1Body(trxSummary)
        }
      };
    }

    function displayBusinessTrxDetails2(trxSummary){
      return {
        alignment: 'center',
        table: {
            headerRows: 0,
            widths:['33%','34%','33%'],
            body: buildBusinessTrxDetails2Body(trxSummary)
        }
      };
    }

    function displayBusinessTrxDetails3(trxSummary){
      return {
        alignment: 'center',
        table: {
            headerRows: 0,
            widths:['33%','34%','33%'],
            body: buildBusinessTrxDetails3Body(trxSummary)
        }
      };
    }

    function displayBusinessTrxFooterDetails1(trxSummary,profile){
      if((trxSummary.amtPendinginLocal>0)||(trxSummary.amtPendinginForiegn>0))
      {
        return {
          alignment: 'center',
          table: {
              headerRows: 0,
              widths:['33%','34%','33%'],
              body: buildBusinessTrxFooterDetails1Body(trxSummary,profile)
          },
          layout: {
            fillColor: function (i, node) {
              return (true) ? '#FF8A80' : null;
            }
          }
        };
      }
      else
      {
        return {
          alignment: 'center',
          table: {
              headerRows: 0,
              widths:['33%','34%','33%'],
              body: buildBusinessTrxFooterDetails1Body(trxSummary,profile)
          },
          layout: {
            fillColor: function (i, node) {
              return (true) ? '#B9F6CA' : null;
            }
          }
        };
      }
      
    }

    function displayBusinessTrxFooterDetails2(trxSummary){
      if((trxSummary.amtPendinginLocal>0)||(trxSummary.amtPendinginForiegn>0))
      {
        return {
          alignment: 'center',
          table: {
              headerRows: 0,
              widths:['33%','34%','33%'],
              body: buildBusinessTrxFooterDetails2Body(trxSummary)
          },
          layout: {
            fillColor: function (i, node) {
              return (true) ? '#FF8A80' : null;
            }
          }
        };
      }
      else
      {
        return {
          alignment: 'center',
          table: {
              headerRows: 0,
              widths:['33%','34%','33%'],
              body: buildBusinessTrxFooterDetails2Body(trxSummary)
          },
          layout: {
            fillColor: function (i, node) {
              return (true) ? '#B9F6CA' : null;
            }
          }
        };
      }
      
    }

    function displayFinanceHeader(){
      return {
        alignment: 'center',
        table: {
            headerRows: 0,
            widths:['14.66%','6%','14.66%','14.66%','14.66%','6%','14.66%','14.66%'],
            body: buildFinanceHeaderBody()
        },
        layout: {
          fillColor: function (i, node) {
            return (true) ? '#9E9E9E' : null;
          }
        }
      };
    }

    function displayFinanceTrxDetails(finTrxs,bizTrxType){
      return {
        alignment: 'center',
        table: {
            headerRows: 0,
            widths:['14.66%','6%','14.66%','14.66%','14.66%','6%','14.66%','14.66%'],
            body: buildFinanceTrxDetailsBody(finTrxs,bizTrxType)
        }
      };
    }

    function buildInvoiceHeaderBody(trxSummary){
      let body=[];
      let dataRow = [];
      dataRow.push(trxSummary.businessTransaction.trxInvoiceID)
      body.push(dataRow)
      return body;
    }

    function buildBusinessTrxDetails1Body(trxSummary){
      let body=[];
      let dataRow = [];
      dataRow.push('Trx Date: \n' + trxSummary.businessTransaction.trxDate)
      if(trxSummary.diffDays > 0)
      {
        dataRow.push('Overdue: \n' + trxSummary.diffDays)
      }
      else
      {
        dataRow.push('Due in: \n' + trxSummary.diffDays)
      }
      dataRow.push('Trx Due Date: \n' + trxSummary.businessTransaction.trxDueDate)
      body.push(dataRow)
      return body;
    }

    function buildBusinessTrxDetails2Body(trxSummary){
      let body=[];
      let dataRow = [];
      dataRow.push('QTY: \n' + trxSummary.businessTransaction.trxQty)
      if((trxSummary.businessTransaction.trxCurrency === 'INR'))
      {
        dataRow.push('RATE: \n' + Number(trxSummary.businessTransaction.trxRate).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
        dataRow.push('Amount: \n' + Number(trxSummary.businessTransaction.trxValue).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
      }
      else
      {
        dataRow.push('RATE: \n' + Number(trxSummary.businessTransaction.trxRate).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
        dataRow.push('Amount: \n' + Number(trxSummary.businessTransaction.trxValue).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
      }
      
      body.push(dataRow)
      return body;
    }

    function buildBusinessTrxDetails3Body(trxSummary){
      let body=[];
      let dataRow = [];
      dataRow.push('TERMS: \n' + trxSummary.businessTransaction.trxDueDays)
      dataRow.push('BROKERAGE: \n' + trxSummary.businessTransaction.trxBrokerageRate)
      if((trxSummary.businessTransaction.trxCurrency === myConstants.localCurrency))
      {
        dataRow.push('TAX: \n' + Number(trxSummary.businessTransaction.trxTaxAmount.totalAmount).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
      }
      else
      {
        dataRow.push('TAX: \n' + Number(trxSummary.businessTransaction.trxTaxAmount.totalAmount).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
      }
      body.push(dataRow)
      return body;
    }

    function buildBusinessTrxFooterDetails1Body(trxSummary,profile)
    {
      console.log('Inside BusinessTransactionPopOverPage::buildBusinessTrxFooterDetails1Body()')
      let body=[];
      let dataRow = [];
      if(trxSummary.businessTransaction.trxCurrency === myConstants.localCurrency)
      {
        dataRow.push('Grand Total: \n' +  Number(trxSummary.trxLocalAmount).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
        if(trxSummary.businessTransaction.trxType ===myConstants.trxType.SELL)
        {
          dataRow.push('Recieved: \n' + Number(trxSummary.amtReceviedOrPaidinLocal).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
          dataRow.push('Pending: \n' + Number(trxSummary.amtPendinginLocal).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
        }
        else
        {
          dataRow.push('Paid: \n' + Number(trxSummary.amtReceviedOrPaidinLocal).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
          dataRow.push('Pending: \n' + Number(trxSummary.amtPendinginLocal).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
        }
      }
      else
      {
        dataRow.push('Grand Total: \n' +  Number(trxSummary.trxForiegnAmount).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
        if(trxSummary.businessTransaction.trxType ===myConstants.trxType.BUY)
        {
          dataRow.push('Recieved: \n' + Number(trxSummary.amtReceviedOrPaidinForiegn).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
          dataRow.push('Pending: \n' + Number(trxSummary.amtPendinginForiegn).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
        }
        else
        {
          dataRow.push('Paid: \n' + Number(trxSummary.amtReceviedOrPaidinForiegn).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
          dataRow.push('Pending: \n' + Number(trxSummary.amtPendinginForiegn).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
        }
      }
      body.push(dataRow)
      return body;
    }

    function buildBusinessTrxFooterDetails2Body(trxSummary){
      let body=[];
      let dataRow = [];
      dataRow.push('INTEREST ===>')
      if((trxSummary.businessTransaction.trxCurrency === 'INR'))
      {
        dataRow.push('Interest Realised: \n' + Number(trxSummary.interestRealizedinLocal).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
        dataRow.push('Interest Unrealised: \n' + Number(trxSummary.interestUnRealizedinLocal).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }))
      }
      else
      {
        dataRow.push('Interest Realised: \n' + Number(trxSummary.interestRealizedinLocal).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
        dataRow.push('Interest Unrealised: \n' + Number(trxSummary.interestUnRealizedinLocal).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 }))
      }
      body.push(dataRow)
      return body;
    }

    function buildFinanceHeaderBody(){
      let body=[];
      let dataRow = [];
      dataRow.push('Date'),
      dataRow.push('In/Out'),
      dataRow.push('Amount'),
      dataRow.push('Opening Balance'),
      dataRow.push('Closing Balance'),
      dataRow.push('Late Days'),
      dataRow.push('Interest'),
      dataRow.push('Payment Method')
      body.push(dataRow)
      return body;
    }

    function buildFinanceTrxDetailsBody(finTrxs,bizTrxType)
    {
      let body=[];
      finTrxs.forEach(function(row){
        let dataRow = [];
        dataRow.push(row.trxDate);
        if((row.trxType === 'Due_Credit') || (row.trxType === 'Due_Debit'))
        {
          dataRow.push('Due');
        }
        else if((row.trxType === 'In_Credit') && (bizTrxType === 'Sell'))
        {
          dataRow.push('+');
        }
        else if((row.trxType === 'In_Credit') && (bizTrxType === 'Buy'))
        {
          dataRow.push('+');
        }
        else if((row.trxType === 'Out_Debit') && (bizTrxType === 'Sell'))
        {
          dataRow.push('-');
        }
        else if((row.trxType === 'Out_Debit') && (bizTrxType === 'Buy'))
        {
          dataRow.push('-');
        }
        if((row.trxCurrency === 'INR'))
        {
          dataRow.push(Number(row.trxLocalValue).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }));
          dataRow.push(Number(row.trxOpeningBalance).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }));
          dataRow.push(Number(row.trxClosingBalance).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }));
          dataRow.push(row.trxDiffDays);
          dataRow.push(Number(row.trxInterestRealizedinLocal).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:0,maximumFractionDigits:0 }));
        }
        else
        {
          dataRow.push(Number(row.trxForiegnValue).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 })); 
          dataRow.push(Number(row.trxOpeningBalance).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 })); 
          dataRow.push(Number(row.trxClosingBalance).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 })); 
          dataRow.push(row.trxDiffDays);
          dataRow.push(Number(row.trxInterestRealizedinForiegn).toLocaleString('en-US',{ style: 'currency', currency: "USD",minimumFractionDigits:0,maximumFractionDigits:0 })); 
        }
        dataRow.push(row.trxPaymentMethod);
        body.push(dataRow);
      });

      return body; 
    }


  this.pdfObj = pdfMake.createPdf(dd);
  let pdfName = this.profile.fullName+'.pdf';
  if (this.platform.is('cordova')) {
    console.log('is Plt ios? : ' + this.platform.is('ios'));
    this.pdfObj.getBuffer((buffer) => {
      var blob = new Blob([buffer], { type: 'application/pdf' });

      // Save the PDF to the data Directory of our App
      this.file.writeFile(this.file.dataDirectory,pdfName, blob, { replace: true })
      .then(fileEntry => {
        // Open the PDf with the correct OS tools
        this.fileOpener.open(this.file.dataDirectory+ pdfName, 'application/pdf');
      })
    });
  } 
  else
  {
   // On a browser simply use download!
    this.pdfObj.download();
  }
  }

  
}
