import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TaskOutputPage } from './task-output';

@NgModule({
  declarations: [
    TaskOutputPage,
  ],
  imports: [
    IonicPageModule.forChild(TaskOutputPage),
  ],
})
export class TaskOutputPageModule {}
