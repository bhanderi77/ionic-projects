import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Loading,LoadingController } from 'ionic-angular';
import { MarketPrice } from '../../model/marketPrice';
import { myConstants } from '../../model/myConstants';

import { SettingsProvider } from '../../providers/settings/settings';
import { RateCardProvider } from '../../providers/rate-card/rate-card';
import { ItemType,ItemShape,ItemQualityLevel, ItemSieve } from '../../model/packagedItem';
import { User } from '../../model/user';



/**
 * Generated class for the MyPricePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-price',
  templateUrl: 'my-price.html',
})
export class MyPricePage {
  private user:User;
  private iType:string;
  private iShape:string;
  private iSieve:string;
  //private iSieveMax:ItemSieve;
  private itemTypeList:Array<ItemType>;
  private itemShapeList:Array<ItemShape>;
  private itemSieveList:Array<ItemSieve>;
  private itemFilteredSieveList:Array<ItemSieve>;
  private itemPriceList:Array<MarketPrice>;
  private editedPriceIDList:Array<string>;
  private bSave:boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private rateCardProvider:RateCardProvider,
    private settingsProvider: SettingsProvider) {
    
    this.user = {} as User;
    this.user = navParams.get('User');  
     
    this.bSave = false;
    this.itemPriceList = [];
    this.itemFilteredSieveList = [];
    this.itemTypeList = [];
    this.itemShapeList = [];
    this.itemSieveList = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyPricePage');
  }

  TrackBy(index: number, obj: any):any
  {
    return index;
  }

  ionViewWillEnter()
  {
    console.log('ionViewDidLoad MyPricePage');   
    this.initializeData();
  }

  async initializeData()
  {
    console.log("Welcome to MyPricePage::initializeData()");
    this.iType = myConstants.itemType.POLISHED;

    this.itemTypeList = [];
    this.iType = "";
    this.itemTypeList = await this.settingsProvider.getItemTypeList(this.user,true);
    console.log("itemTypeList.length " + this.itemTypeList.length);
    if(this.itemTypeList.length > 0 )
    {
      this.iType = this.itemTypeList[0].id;
    }

    this.itemShapeList = [];
    this.iShape = "";
    this.itemShapeList = await this.settingsProvider.getItemShapeList(this.user,true);
    console.log("itemShapeList.length " + this.itemShapeList.length);
    if(this.itemShapeList.length > 0 )
    {
      this.iShape = this.itemShapeList[0].id;
    }
  
    this.iSieve = "";
    this.itemSieveList = [];
    this.itemSieveList = await this.settingsProvider.getItemSieveList(this.user,myConstants.itemType.ALL,true);
    console.log("itemSieveList.length " + this.itemSieveList.length);
    this.onItemTypeChange();

    let itemQualityLevelList:Array<ItemQualityLevel>;
    itemQualityLevelList = [];
    itemQualityLevelList = await this.settingsProvider.getItemQualityLevelList(this.user,myConstants.itemType.ALL,true);
    console.log("itemQualityLevelList.length " + itemQualityLevelList.length);

    for(let a = 0; a < this.itemTypeList.length ; a++)
    {
      console.log("itemTypeList[a].id " + this.itemTypeList[a].id);
      for(let i = 0; i < this.itemShapeList.length; i++)
      {
        console.log("itemShapeList[i].id " + this.itemShapeList[i].id);
        for(let j = 0; (j < this.itemSieveList.length); j++)
        {
          console.log("itemSieveList[j].id " + this.itemSieveList[j].id);
          if(this.itemSieveList[j].itemTypeID === this.itemTypeList[a].id)
          {
            for(let k = 0; (k < itemQualityLevelList.length); k++)
            {
      //        console.log("itemQualityLevelList[k].id " + itemQualityLevelList[k].id);
              if(itemQualityLevelList[k].itemTypeID === this.itemTypeList[a].id)
              {
                //if(this.itemSieveList[j].itemType === itemQualityLevelList[k].itemType)
                let marketPrice = {} as MarketPrice;
                marketPrice.id = "";
                marketPrice.creation_time = "";
                marketPrice.itemTypeID = this.itemTypeList[a].id;
                marketPrice.shapeID = this.itemShapeList[i].id;
                marketPrice.sieveID = this.itemSieveList[j].id;
                marketPrice.qualityLevelID = itemQualityLevelList[k].id;
                marketPrice.qualityLevelRank = itemQualityLevelList[k].rank;
                marketPrice.rate = 0;

                console.log("Inserting " + marketPrice.itemTypeID + " " + marketPrice.shapeID + " " + marketPrice.sieveID + " " + marketPrice.qualityLevelID);
                this.itemPriceList.push(marketPrice);
              }//if(itemQualityLevelList[k].itemType === this.itemTypeList[a].id)              
            }//end of loop over itemQualityLevelList                    
          }//if(this.itemSieveList[j].itemType === this.itemTypeList[a].id)
        }//end of loop over itemPolishSieveList        
      }//end of loop over itemShapeList
    }//end of loop over itemTypeList
    
    console.log("this.itemPriceList.length " + this.itemPriceList.length);
    this.getMarketPriceList(); 
  }

  onItemTypeChange()
  {
    this.itemFilteredSieveList = this.itemSieveList.filter(itemSieve => itemSieve.itemTypeID === this.iType);
    if(this.itemFilteredSieveList.length > 0)
    {
      this.iSieve = this.itemFilteredSieveList[0].id;
    }
    else
    {
      this.iSieve = ""; 
    }
    this.getMarketPriceList();
  }

  async getMarketPriceList()
  {
    console.log("Welcome to MyPricePage:getMarketPriceList() " + this.iShape + " " + this.iSieve);
    if((this.iType) && (this.iShape) && (this.iSieve))
    {
      let priceList:Array<MarketPrice>;
      priceList = [];
      priceList = await this.settingsProvider.getMarketPriceListForTSS(this.user,this.iType,this.iShape,this.iSieve);
      console.log("priceList.length " + priceList.length);
      console.log("itemPriceList.length " + this.itemPriceList.length);
      for(let i=0;i<priceList.length;i++)
      {
      //  console.log("priceList[i]: " + priceList[i].itemType + " " + priceList[i].shapeID + " " + priceList[i].sieveID + " " + priceList[i].qualityLevelID);
        for(let j=0;j<this.itemPriceList.length;j++)
        {
       //   console.log("itemPriceList[j] " + this.itemPriceList[j].itemType + " " + this.itemPriceList[j].shapeID + " " + this.itemPriceList[j].sieveID + " " + this.itemPriceList[j].qualityLevelID);
          if( (this.itemPriceList[j].itemTypeID === priceList[i].itemTypeID) 
          && (this.itemPriceList[j].shapeID === priceList[i].shapeID) 
          && (this.itemPriceList[j].sieveID === priceList[i].sieveID) 
          && (this.itemPriceList[j].qualityLevelID === priceList[i].qualityLevelID))
          {
            console.log("Rate updated from " + this.itemPriceList[j].rate + " to " + priceList[i].rate);
            this.itemPriceList[j].rate = priceList[i].rate;
          }
        }
      }
      this.bSave = false;
    }
  }

  onShapeChange(value)
  {
    console.log("Welcome to MyPricePage:onShapeChange() " + value);
    this.getMarketPriceList();
  }

  onSieveChange(value)
  {
    console.log("Welcome to MyPricePage:onSieveChange() " + value);
    this.getMarketPriceList();    
  }

  onPriceChange(i:number)
  {
    this.bSave = true;
  }

  async onSave()
  {
    console.log("Welcome to MyPricePage:onSave() ");
    let updatedPriceList:Array<MarketPrice>;
    updatedPriceList = [];
    updatedPriceList = this.itemPriceList.filter(itemPrice => (itemPrice.itemTypeID === this.iType && itemPrice.shapeID === this.iShape && itemPrice.sieveID === this.iSieve));
    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();
    
    await this.settingsProvider.saveMarketPriceList(this.user,updatedPriceList);
    loading.dismiss();
    this.bSave = false;
  }

}
