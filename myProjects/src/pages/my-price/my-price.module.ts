import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyPricePage } from './my-price';

@NgModule({
  declarations: [
    MyPricePage,
  ],
  imports: [
    IonicPageModule.forChild(MyPricePage),
  ],
})
export class MyPricePageModule {}
