import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,Loading,LoadingController } from 'ionic-angular';
import { FormControl } from '@angular/forms';

import { EntityProvider } from '../../providers/entity/entity';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';
import { RateCardProvider } from '../../providers/rate-card/rate-card';
import { RateCard,InventoryParams } from '../../model/rateCard';

import { ItemType,ItemShape,ItemQualityLevel, ItemSieve } from '../../model/packagedItem';
import { BusinessTransactionSummary,getBusinessTransactionSummary } from '../../model/partySummary';
import { PackagedItem } from '../../model/packagedItem';
import { SalesSummaryByTSSQ,SalesSummaryByTSS, SalesSummary } from '../../model/salesSummary';
//import { doInitialize,doRound,doTotal,doAverage } from '../../model/salesSummary';


import { myConstants, round } from '../../model/myConstants';
import { MarketPrice } from '../../model/marketPrice';

import { User, checkIfValid } from '../../model/user';
import { FinanceTransaction } from '../../model/financeTransaction';
import { BusinessTransaction } from '../../model/businessTransaction';

import { Subscription } from 'rxjs';
import { BusinessContact } from '../../model/businessContact';

/**
 * Generated class for the SalesStockSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sales-stock-summary',
  templateUrl: 'sales-stock-summary.html',
})
export class SalesStockSummaryPage {
  private user:User;  
  private rateCardList:Array<RateCard>;
  private inventoryParams:InventoryParams;
 
  private itemTypeList:Array<ItemType>;
  private itemShapeList:Array<ItemShape>;
  private itemSieveList:Array<ItemSieve>;
  private itemFilteredSieveList:Array<ItemSieve>;
 // private itemRoughSieveList:Array<ItemSieve>;
  private itemQualityLevelList:Array<ItemQualityLevel>;
  private itemFilteredQualityLevelList:Array<ItemQualityLevel>;
 // private itemRoughQualityLevelList:Array<ItemQualityLevel>;
  
  private salesSummaryByTSSQList:Array<SalesSummaryByTSSQ>;
  private salesSummaryByTSSList:Array<SalesSummaryByTSS>;
  private totalSalesSummary:SalesSummary;

  private iType:ItemType;
  private iShape:ItemShape;
  private iSieveMin:ItemSieve;
  private iSieveMax:ItemSieve;
  private iQualityLevelMin:ItemQualityLevel;
  private iQualityLevelMax:ItemQualityLevel;
  
  private iSeriesID:string;
  private iSeriesName:string;
  private iTrxPartyName:string;
  private iTrxDate:string;
  private iTrxWeight:number;
  private iTrxRate:number;
  private iTrxCurrency:string;
  private itemPriceList:Array<MarketPrice>;
  
  private packagedItemList:Array<PackagedItem>;
  private filteredItemList:Array<PackagedItem>;
  private currencyExchangeRate:number;
  
//  private iSieveRange:any;
//  private rangeSieveMin:number;
//  private rangeSieveMax:number;
  private mySubscriptions:Subscription;

  private bSearching:boolean;
  private searchInput:string;
  private searchControl:FormControl;
  private searchList:Array<string>;
  //private searchSeriesID:string;
  
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private loadingCtrl:LoadingController,
   // private entityProvider: EntityProvider,
   // private inventoryProvider: InventoryProvider,
   // private transactionProvider: TransactionProvider,
   // private settingsProvider: SettingsProvider,
    private rateCardProvider:RateCardProvider) 
  {   
    console.log("Welcome to SalesStockSummaryPage::constructor()");
    this.user = {} as User;
    this.user = navParams.get('User');

    this.iType = {} as ItemType;
    this.iType.id = "";

    this.searchControl = new FormControl();
    this.searchInput = "";
    this.searchList = [];
    this.bSearching = false;
    //this.searchSeriesID = "";
    //this.rangeSieveMin = 0;
    //this.rangeSieveMax = 0;
    this.itemPriceList = [];
    this.itemTypeList = [];
    this.itemShapeList = [];
    this.itemSieveList = [];
    this.itemFilteredSieveList = [];
   // this.itemRoughSieveList = [];
    this.itemQualityLevelList = [];
    this.itemFilteredQualityLevelList = [];
   // this.itemRoughQualityLevelList = [];
    this.packagedItemList = [];
    this.filteredItemList = [];
    
    this.salesSummaryByTSSQList = [];
    this.salesSummaryByTSSList = [];
    this.totalSalesSummary = new SalesSummary();    
    
    this.inventoryParams = {} as InventoryParams;
    this.inventoryParams.avgP2SGain = 0.0;
    this.inventoryParams.avgMfgGain = 0.0;
    
    this.inventoryParams.purchaseCostPerSaleableUnit = 0.0;
    this.inventoryParams.interestCostPerSaleableUnit = 0.0;

    this.inventoryParams.totalMarketValueForSaleable = 0.0;
    this.inventoryParams.totalSaleablePolishedWeight = 0.0;
    this.inventoryParams.totalSaleableRoughWeight = 0.0;
    this.inventoryParams.totalSaleableWeight = 0.0;
    this.inventoryParams.totalPurchaseWeight = 0.0;
    this.inventoryParams.avgMarketRateForSaleable = 0.0; 
    this.inventoryParams.avgMfgCostForPolished = 0.0;
    this.inventoryParams.avgMfgCostForRough = 0.0;
        
   // this.iType = myConstants.itemType.POLISHED;
    this.rateCardList = [];
  }

  async initializeData()
  {
    console.log("Welcome to SalesStockSummaryPage:initializeData()");
    this.mySubscriptions = new Subscription();
    let noOfCallbacks:number = 0;
    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.setContent("Calculating stock details..");
    loading.present();     

    this.iType = {} as ItemType;
    this.iShape = {} as ItemShape;
    this.iSieveMin = {} as ItemSieve;
    this.iSieveMax = {} as ItemSieve;
    this.iQualityLevelMin = {} as ItemQualityLevel;
    this.iQualityLevelMax = {} as ItemQualityLevel;

    this.iType.id = "";
    this.iShape.id = "";
    this.iSieveMin.id = "";
    this.iSieveMax.id = "";
    this.iQualityLevelMin.id = "";
    this.iQualityLevelMax.id = "";
    
    this.iSeriesID = "";
    this.iSeriesName = "";
    this.iTrxPartyName = "";
    this.iTrxDate = '';
    this.iTrxWeight = 0;
    this.iTrxRate = 0;
    this.iTrxCurrency = "";

    /*
    this.currencyExchangeRate = 1;
    this.currencyExchangeRate = await this.settingsProvider.getExchangeRate(this.user);
    
    this.itemShapeList = [];
    //this.polishShapeList = await this.settingsProvider.getItemShapeList(false);
    this.settingsProvider.getItemShapeList(this.user,true)
    .then(pList => {
      this.itemShapeList = pList;
      if(this.itemShapeList.length > 0)
      {
        this.iShape = this.itemShapeList[0];
        this.filterPackagedItem();  
      }
    });
    
    this.itemSieveList = [];
    this.iType = myConstants.itemType.POLISHED;
    this.itemPolishSieveList = [];
    this.itemRoughSieveList = [];
    
    this.settingsProvider.getItemSieveList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemPolishSieveList = pList.filter(itemSieve => itemSieve.itemType === myConstants.itemType.POLISHED);
        this.itemRoughSieveList = pList.filter(itemSieve => itemSieve.itemType === myConstants.itemType.ROUGH);
        this.onItemTypeChange();
      }      
    });
    
    this.itemQualityLevelList = [];
    this.itemPolishQualityLevelList = [];
    this.itemRoughQualityLevelList = [];
    
    this.settingsProvider.getItemQualityLevelList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemPolishQualityLevelList = pList.filter(itemQualityLevel => itemQualityLevel.itemType === myConstants.itemType.POLISHED);
        this.itemRoughQualityLevelList = pList.filter(itemQualityLevel => itemQualityLevel.itemType === myConstants.itemType.ROUGH);
        this.onItemTypeChange();
      }      
    });
    */

    noOfCallbacks = 1;
    this.mySubscriptions.add(this.rateCardProvider.getRateCardList().subscribe(rcList => {
      console.log("Received rcList " + rcList.length);
      if(rcList.length)
      {
        this.rateCardList = rcList;
        this.packagedItemList = this.rateCardProvider.packagedItemList;
        
        this.inventoryParams = this.rateCardProvider.getInventoryParams();
        this.currencyExchangeRate = this.rateCardProvider.currencyExchangeRate;
        this.itemTypeList = this.rateCardProvider.itemTypeList;
        this.itemShapeList = this.rateCardProvider.itemShapeList;
        this.itemSieveList = this.rateCardProvider.itemSieveList;
        this.itemQualityLevelList = this.rateCardProvider.itemQualityLevelList;
          
        this.iShape = this.itemShapeList[0];
        this.iType = this.itemTypeList[0];
        this.onItemTypeChange();

        noOfCallbacks--;
        if(noOfCallbacks <= 0)
        {
          loading.dismiss();
        }
        else
        {
          loading.setContent("Loading RateCard details..");
        }
      }            
    })); 

    /*
    do
    {
      if(noOfCallbacks <= 0)
      {
        loading.dismiss();
      }
      else
      {
        loading.setContent("Loading RateCard details..");
      }
    }while(noOfCallbacks >= 0);
    */
    
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad SalesStockSummaryPage');
  }

  ionViewWillEnter() 
  {
    console.log('ionViewWillEnter SalesStockSummaryPage');
    this.initializeData();
    this.calculateSalesStockSummary();  
  }

  ionViewWillLeave() 
  {
    console.log('ionViewWillLeave SalesStockSummaryPage');
    this.mySubscriptions.unsubscribe();
  }

  gotoSalesStockList()
  {
    const modal = this.modalCtrl.create('SalesStockListPage',{User:this.user,iType:this.iType,iShape:this.iShape,iSieveMin:this.iSieveMin,iSieveMax:this.iSieveMax,iQualityLevelMin:this.iQualityLevelMin,iQualityLevelMax:this.iQualityLevelMax,iSeriesID:this.iSeriesID,iSeriesName:this.iSeriesName,iTrxPartyName:this.iTrxPartyName,iTrxDate:this.iTrxDate,iTrxWeight:this.iTrxWeight,iTrxRate:this.iTrxRate,iTrxCurrency:this.iTrxCurrency});
    modal.present();
    modal.onDidDismiss((selectValue) => {
      this.iShape = selectValue.iShape;  
      this.iSieveMin = selectValue.iSieveMin;   
      this.iSieveMax = selectValue.iSieveMax; 
      this.iQualityLevelMin = selectValue.iQualityLevelMin;  
      this.iQualityLevelMax = selectValue.iQualityLevelMax;    
      this.filterPackagedItem();
    }); 
  }

  onItemTypeChange()
  {
    this.itemFilteredSieveList = this.itemSieveList.filter(itemSieve => itemSieve.itemTypeID === this.iType.id);
    this.itemFilteredQualityLevelList = this.itemQualityLevelList.filter(itemQualityLevel => itemQualityLevel.itemTypeID === this.iType.id);
    
    if(this.itemFilteredSieveList.length > 0)
    {
      this.iSieveMin = this.itemFilteredSieveList[0];
      this.iSieveMax = this.itemFilteredSieveList[this.itemFilteredSieveList.length-1];
    }

    if(this.itemFilteredQualityLevelList.length > 0)
    {
      this.iQualityLevelMin = this.itemFilteredQualityLevelList[0];
      this.iQualityLevelMax = this.itemFilteredQualityLevelList[this.itemFilteredQualityLevelList.length-1];
    }
    this.filterPackagedItem();    
  }
  
  filterPackagedItem()
  {
    console.log("Welcome to SalesStockSummaryPage:filterPackagedItem() ");
    /*
    console.log("Selected Shape " + this.iShape.id);
    console.log("Min Sieve " + this.iSieveMin.id +  " " + this.iSieveMin.rank);
    console.log("Max Sieve " + this.iSieveMax.id +  " " + this.iSieveMax.rank);
    console.log("Min Quality Rank " + this.iQualityLevelMin.id +  " " + this.iQualityLevelMin.rank);
    console.log("Max Quality Rank " + this.iQualityLevelMax.id +  " " + this.iQualityLevelMax.rank);
    */
    this.filteredItemList = this.packagedItemList;
    console.log("this.filteredItemList.length " + this.filteredItemList.length);
    if(this.iType.id)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.itemType.id === this.iType.id));  
    }
    console.log("this.filteredItemList.length " + this.filteredItemList.length);
    
    if((this.iShape.id) && (this.iShape.id !== myConstants.itemShape.ALL))
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.itemShape.id === this.iShape.id));
    }
    if((this.iSieveMin.id) && (this.iSieveMax.id))
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => ((pItem.itemSieve.rank >= this.iSieveMin.rank) && (pItem.itemSieve.rank < this.iSieveMax.rank)));
    }

    if(((this.iType.id === myConstants.itemType.ROUGH) || (this.iType.id === myConstants.itemType.TOPS)) && (this.iQualityLevelMin.id))
    {
      this.iQualityLevelMax = this.iQualityLevelMin;
    }
    if((this.iQualityLevelMin.id) && (this.iQualityLevelMax.id))
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => ((pItem.itemQualityLevel.rank >= this.iQualityLevelMin.rank) && (pItem.itemQualityLevel.rank <= this.iQualityLevelMax.rank)));
    }
    
    if(this.iSeriesID)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.packageGroupID === this.iSeriesID));
    }
    if(this.iSeriesName)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.packageGroupName === this.iSeriesName));
    }
    if(this.iTrxPartyName)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxPartyName === this.iTrxPartyName));
    }
    if(this.iTrxDate)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxDate === this.iTrxDate));
    }
    if(this.iTrxWeight)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxWeight === this.iTrxWeight));
    }
    if(this.iTrxRate)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxRate === this.iTrxRate));
    }
    if(this.iTrxCurrency)
    {
      this.filteredItemList = this.filteredItemList.filter(pItem => (pItem.trxCurrency === this.iTrxCurrency));
    }
      
    console.log("this.filteredItemList.length " + this.filteredItemList.length);
    this.calculateSalesStockSummary();
  }

  calculateSalesStockSummary()
  {
    console.log("Welcome SalesStockSummaryPage:calculateSalesStockSummary() ");
    console.log("SSQP "+ this.rateCardProvider.itemShapeList.length + " " + this.rateCardProvider.itemSieveList.length + " " + this.rateCardProvider.itemQualityLevelList.length + " " + this.packagedItemList.length);
    this.salesSummaryByTSSQList = [];
    this.salesSummaryByTSSList = [];
    this.totalSalesSummary.initialize();
    
    if(this.rateCardList.length && this.packagedItemList.length)
    {
      console.log("calculateSalesStockSummary started");
      let rateCard = {} as RateCard;
      
      let summaryTSSList:Array<SalesSummaryByTSS> = [];
      for(let i=0;i<this.rateCardList.length;i++)
      {      
        let salesSummaryByTSSQ = new SalesSummaryByTSSQ();
        salesSummaryByTSSQ.itemType = this.rateCardList[i].itemType;
        salesSummaryByTSSQ.itemShape = this.rateCardList[i].itemShape;
        salesSummaryByTSSQ.itemSieve = this.rateCardList[i].itemSieve;
        salesSummaryByTSSQ.itemQualityLevel = this.rateCardList[i].itemQualityLevel;
        //salesSummaryByTSSQ.salesSummary = {} as SalesSummary;
        //doInitialize(salesSummaryByTSSQ.salesSummary);
        
        for(let index=0;((index<this.filteredItemList.length));index++)
        {
          if((this.filteredItemList[index].itemQualityLevel.id === salesSummaryByTSSQ.itemQualityLevel.id)
          && (this.filteredItemList[index].itemSieve.id === salesSummaryByTSSQ.itemSieve.id)
          && (this.filteredItemList[index].itemShape.id === salesSummaryByTSSQ.itemShape.id))
          {
            salesSummaryByTSSQ.totalSaleableWeight += +this.filteredItemList[index].itemWeight;
            salesSummaryByTSSQ.totalPurchaseWeight += +this.filteredItemList[index].roughWeight;
            salesSummaryByTSSQ.totalQty += +this.filteredItemList[index].itemQty;
            
            this.totalSalesSummary.totalPurchaseWeight += +this.filteredItemList[index].roughWeight;
            
            rateCard = null;
            rateCard = this.rateCardProvider.getRateCardByID(salesSummaryByTSSQ.itemType.id,salesSummaryByTSSQ.itemShape.id,salesSummaryByTSSQ.itemSieve.id,salesSummaryByTSSQ.itemQualityLevel.id);
            if(rateCard)
            {
              salesSummaryByTSSQ.avgPurchaseCost = round(rateCard.avgPurchaseCost,0);
              salesSummaryByTSSQ.avgInterestCost = round(rateCard.avgInterestCost,0)
              salesSummaryByTSSQ.avgMfgCost = round(rateCard.avgMfgCost,0);
              salesSummaryByTSSQ.avgGrossCost = round(rateCard.avgGrossCost,0);
              salesSummaryByTSSQ.avgPriceRate = round(rateCard.avgMarketRate,0);
              salesSummaryByTSSQ.avgValueFactor = rateCard.valueFactor;

              console.log("rateCard.avgGrossCost " + rateCard.avgGrossCost);
             // console.log("salesSummaryByTSSQ.avgPurchaseCost " + salesSummaryByTSSQ.avgPurchaseCost);
            }
            else
            {
              salesSummaryByTSSQ.avgPurchaseCost = 0.0;
              salesSummaryByTSSQ.avgInterestCost = 0.0;
              salesSummaryByTSSQ.avgMfgCost = 0.0;
              salesSummaryByTSSQ.avgGrossCost = 0.0;
              salesSummaryByTSSQ.avgPriceRate = 0.0;
              salesSummaryByTSSQ.avgValueFactor = 0;
            }                    
          }//end of IF                  
        }//end of FOR loop
        //doRound(salesSummaryByTSSQ.salesSummary,0);
        salesSummaryByTSSQ.doRound(0);
        //doTotal(salesSummaryByTSSQ.salesSummary,0);
        salesSummaryByTSSQ.doTotal(0);
        if(salesSummaryByTSSQ.totalSaleableWeight)
        {
          console.log("this.salesSummaryByTSSQList.push " + salesSummaryByTSSQ.totalSaleableWeight);
          this.salesSummaryByTSSQList.push(salesSummaryByTSSQ);  
        }
  
        summaryTSSList = [];
        summaryTSSList = this.salesSummaryByTSSList.filter(summary => (summary.itemShape.id === salesSummaryByTSSQ.itemShape.id)
                                                                    && (summary.itemSieveMin.id === salesSummaryByTSSQ.itemSieve.id));
        if(summaryTSSList.length === 0)
        {
          let salesSummaryByTSS = new SalesSummaryByTSS();
          salesSummaryByTSS.itemType = salesSummaryByTSSQ.itemType;
          salesSummaryByTSS.itemShape = salesSummaryByTSSQ.itemShape;
          salesSummaryByTSS.itemSieveMin = salesSummaryByTSSQ.itemSieve;
          salesSummaryByTSS.itemSieveMax = this.rateCardProvider.getNextSieve(salesSummaryByTSSQ.itemSieve);
                 
          
          salesSummaryByTSS.totalSaleableWeight += (+salesSummaryByTSSQ.totalSaleableWeight);
          salesSummaryByTSS.totalPurchaseWeight += (+salesSummaryByTSSQ.totalPurchaseWeight);
  
          salesSummaryByTSS.totalPurchaseCost += (+salesSummaryByTSSQ.totalPurchaseCost);
          salesSummaryByTSS.totalInterestCost += (+salesSummaryByTSSQ.totalInterestCost);
          salesSummaryByTSS.totalMfgCost += (+salesSummaryByTSSQ.totalMfgCost);
          salesSummaryByTSS.totalGrossCost += (+salesSummaryByTSSQ.totalGrossCost);
          
          salesSummaryByTSS.totalPriceValue += (+salesSummaryByTSSQ.totalPriceValue);
          salesSummaryByTSS.totalProfit += (+salesSummaryByTSSQ.totalProfit);
  
          //doAverage(salesSummaryByTSS.salesSummary,0);
          salesSummaryByTSS.doAverage(0);
          //doRound(salesSummaryByTSS.salesSummary,0);
          salesSummaryByTSS.doRound(0);
          if(salesSummaryByTSS.totalSaleableWeight)
          {
            console.log("this.salesSummaryByTSSList.push " + salesSummaryByTSS.totalSaleableWeight);
            this.salesSummaryByTSSList.push(salesSummaryByTSS);
          } 
        }
        else if(summaryTSSList.length === 1)
        {
          summaryTSSList[0].totalSaleableWeight += (+salesSummaryByTSSQ.totalSaleableWeight);
          summaryTSSList[0].totalPurchaseWeight += (+salesSummaryByTSSQ.totalPurchaseWeight);
  
          summaryTSSList[0].totalPurchaseCost += (+salesSummaryByTSSQ.totalPurchaseCost);
          summaryTSSList[0].totalInterestCost += (+salesSummaryByTSSQ.totalInterestCost);
          summaryTSSList[0].totalMfgCost += (+salesSummaryByTSSQ.totalMfgCost);
          summaryTSSList[0].totalGrossCost += (+salesSummaryByTSSQ.totalGrossCost);
          
          summaryTSSList[0].totalPriceValue += (+salesSummaryByTSSQ.totalPriceValue);
          summaryTSSList[0].totalProfit += (+salesSummaryByTSSQ.totalProfit);
        
          //doAverage(summaryTSSList[0].salesSummary,0);
          summaryTSSList[0].doAverage(0);
          //doRound(summaryTSSList[0].salesSummary,0);
          summaryTSSList[0].doRound(0);
        }  
        this.totalSalesSummary.totalSaleableWeight += (+salesSummaryByTSSQ.totalSaleableWeight);
        this.totalSalesSummary.totalPurchaseWeight += (+salesSummaryByTSSQ.totalPurchaseWeight);
        this.totalSalesSummary.totalPurchaseCost += (+salesSummaryByTSSQ.totalPurchaseCost);
        this.totalSalesSummary.totalInterestCost += (+salesSummaryByTSSQ.totalInterestCost);
        this.totalSalesSummary.totalMfgCost += (+salesSummaryByTSSQ.totalMfgCost);
        this.totalSalesSummary.totalGrossCost += (+salesSummaryByTSSQ.totalGrossCost);
        this.totalSalesSummary.totalPriceValue += (+salesSummaryByTSSQ.totalPriceValue);
        this.totalSalesSummary.totalProfit += (+salesSummaryByTSSQ.totalProfit);
      }
      //doRound(this.totalSalesSummary,0);
      this.totalSalesSummary.doRound(0);
      //doAverage(this.totalSalesSummary,0);  
      this.totalSalesSummary.doAverage(0);  
    }    
  }

  /*
  calculateCostPerSaleableUnit()
  {
    this.purchaseCostPerSaleableUnit = 0.0;
    this.interestCostPerSaleableUnit = 0.0;
    //this.mfgCostPerSaleableUnit = 0.0;
    //this.avgCostPerSaleableUnit = 0.0;

    this.purchaseCostPerSaleableUnit = round((this.trxAvgPurchaseCost/this.avgP2SGain)*100,10);
    this.interestCostPerSaleableUnit = round((this.trxAvgInterestCost/this.avgP2SGain)*100,10);
    //this.mfgCostPerSaleableUnit = round((this.avgMfgCostForSaleable/this.avgMfgGain)*100,10);
   // this.mfgCostPerSaleableUnit = round((this.avgMfgCostForSaleable/1)*100,10);
    //this.avgCostPerSaleableUnit = round((this.purchaseCostPerSaleableUnit + this.interestCostPerSaleableUnit + this.mfgCostPerSaleableUnit),10);    
    
  }
  
  buildSalesStockSummary()
  {
    console.log("Welcome SalesStockSummaryPage:buildSalesStockSummary() " + this.packagedItemList.length);
    this.salesSummaryByTSSQList = [];
    this.salesSummaryByTSSList = [];

    doInitialize(this.totalSalesSummary);
    
    for(let i=0;i<this.itemShapeList.length;i++)
    {
      if(((this.iShape.id === myConstants.itemShape.ALL) || (this.iShape.id === this.itemShapeList[i].id))
      && (this.itemShapeList[i].rank >= 1 && this.itemShapeList[i].rank < 999))
      {
        for(let j=0;j<this.itemSieveList.length-1;j++)
        {
          if((this.itemSieveList[j].rank >= this.iSieveMin.rank)
          && (this.itemSieveList[j].rank < this.iSieveMax.rank))
          {
            let salesSummaryByTSS = {} as SalesSummaryByTSS;
            salesSummaryByTSS.itemType = this.iType;
            //salesSummaryByTSS.itemShape = {} as ItemShape;
            //salesSummaryByTSS.itemSieveMin = {} as ItemSieve;
            //salesSummaryByTSS.itemSieveMax = {} as ItemSieve;
            
            salesSummaryByTSS.itemShape = this.itemShapeList[i];
            salesSummaryByTSS.itemSieveMin = this.itemSieveList[j];
            salesSummaryByTSS.itemSieveMax = this.itemSieveList[j+1];
            salesSummaryByTSS.salesItemList = [];

            salesSummaryByTSS.salesSummary = {} as SalesSummary;
            doInitialize(salesSummaryByTSS.salesSummary);
            for(let k=0;k<this.itemQualityLevelList.length;k++)
            {
              if((this.itemQualityLevelList[k].rank >= this.iQualityLevelMin.rank)
              && (this.itemQualityLevelList[k].rank <= this.iQualityLevelMax.rank))
              {
                let salesSummaryByTSSQ = {} as SalesSummaryByTSSQ;

                salesSummaryByTSSQ.itemType = salesSummaryByTSS.itemType;
                //salesSummaryByTSSQ.itemShape = {} as ItemShape;
                //salesSummaryByTSSQ.itemSieve = {} as ItemSieve;
                
                salesSummaryByTSSQ.itemShape = this.itemShapeList[i];
                salesSummaryByTSSQ.itemSieve = this.itemSieveList[j];
                salesSummaryByTSSQ.itemQualityLevel = this.itemQualityLevelList[k];
                salesSummaryByTSSQ.salesItemList = [];
                
                salesSummaryByTSSQ.salesSummary = {} as SalesSummary;
                doInitialize(salesSummaryByTSSQ.salesSummary);
                for(let index=0;((index<this.filteredItemList.length));index++)
                {
                  if((this.filteredItemList[index].itemQualityLevel.id === this.itemQualityLevelList[k].id)
                  && (this.filteredItemList[index].itemSieve.id === this.itemSieveList[j].id)
                  && (this.filteredItemList[index].itemShape.id === this.itemShapeList[i].id))
                  {
                    salesSummaryByTSSQ.salesSummary.totalSaleableWeight += +this.filteredItemList[index].itemWeight;
                    salesSummaryByTSSQ.salesSummary.totalPurchaseWeight += +this.filteredItemList[index].roughWeight;
                    this.totalSalesSummary.totalPurchaseWeight += +this.filteredItemList[index].roughWeight;
             //     
                    salesSummaryByTSSQ.salesSummary.totalQty += +this.filteredItemList[index].itemQty;
                    salesSummaryByTSSQ.salesSummary.totalMarketValue += round((+this.filteredItemList[index].itemValuation.value),10);

                    salesSummaryByTSSQ.salesSummary.avgPurchaseCost = round(this.purchaseCostPerSaleableUnit*this.filteredItemList[index].itemValuation.costFactor,10);
                    salesSummaryByTSSQ.salesSummary.avgInterestCost = round(this.interestCostPerSaleableUnit*this.filteredItemList[index].itemValuation.costFactor,10)
                   
                    salesSummaryByTSSQ.salesSummary.avgMfgCost = round(+this.filteredItemList[index].accumulatedTaskCost/+this.filteredItemList[index].itemWeight,10);
                    
                    //salesSummaryBySSQ.salesSummary.avgCost = round(this.avgCostPerSaleableUnit*this.filteredItemList[index].itemValuation.costFactor,10)
                    salesSummaryByTSSQ.salesSummary.avgCost = round((salesSummaryByTSSQ.salesSummary.avgPurchaseCost + salesSummaryByTSSQ.salesSummary.avgInterestCost + salesSummaryByTSSQ.salesSummary.avgMfgCost),10);
                  }//end of IF                  
                }//end of FOR loop
                
                doRound(salesSummaryByTSSQ.salesSummary,10);

                salesSummaryByTSSQ.salesSummary.avgMarketRate = 0;
                if(salesSummaryByTSSQ.salesSummary.totalSaleableWeight > 0)
                {
                  salesSummaryByTSSQ.salesSummary.avgMarketRate = round((salesSummaryByTSSQ.salesSummary.totalMarketValue/salesSummaryByTSSQ.salesSummary.totalSaleableWeight),10);
                }
                
                salesSummaryByTSSQ.salesSummary.totalProfitMargin = 0;
                if(salesSummaryByTSSQ.salesSummary.totalCost > 0)
                {
                  salesSummaryByTSSQ.salesSummary.totalProfitMargin = round(((salesSummaryByTSSQ.salesSummary.totalProfit/salesSummaryByTSSQ.salesSummary.totalCost)*100),10);
                }
                
                doTotal(salesSummaryByTSSQ.salesSummary,10);
                
                //console.log("Pushing salesStockSummary for " + salesStockSummary. + " " + salesStockSummary.sieveID + " " + salesStockSummary.qualityLevelID);
                salesSummaryByTSS.salesSummary.totalSaleableWeight += (+salesSummaryByTSSQ.salesSummary.totalSaleableWeight);
                salesSummaryByTSS.salesSummary.totalPurchaseWeight += (+salesSummaryByTSSQ.salesSummary.totalPurchaseWeight);

                salesSummaryByTSS.salesSummary.totalPurchaseCost += (+salesSummaryByTSSQ.salesSummary.totalPurchaseCost);
                salesSummaryByTSS.salesSummary.totalInterestCost += (+salesSummaryByTSSQ.salesSummary.totalInterestCost);
                salesSummaryByTSS.salesSummary.totalMfgCost += (+salesSummaryByTSSQ.salesSummary.totalMfgCost);
                salesSummaryByTSS.salesSummary.totalCost += (+salesSummaryByTSSQ.salesSummary.totalCost);
                
                salesSummaryByTSS.salesSummary.totalMarketValue += (+salesSummaryByTSSQ.salesSummary.totalMarketValue);
                salesSummaryByTSS.salesSummary.totalProfit += (+salesSummaryByTSSQ.salesSummary.totalProfit);
                
                
                this.totalSalesSummary.totalSaleableWeight += (+salesSummaryByTSSQ.salesSummary.totalSaleableWeight);
                this.totalSalesSummary.totalPurchaseWeight += (+salesSummaryByTSSQ.salesSummary.totalPurchaseWeight);
                this.totalSalesSummary.totalPurchaseCost += (+salesSummaryByTSSQ.salesSummary.totalPurchaseCost);
                this.totalSalesSummary.totalInterestCost += (+salesSummaryByTSSQ.salesSummary.totalInterestCost);
                this.totalSalesSummary.totalMfgCost += (+salesSummaryByTSSQ.salesSummary.totalMfgCost);
                this.totalSalesSummary.totalCost += (+salesSummaryByTSSQ.salesSummary.totalCost);
                this.totalSalesSummary.totalMarketValue += (+salesSummaryByTSSQ.salesSummary.totalMarketValue);
                this.totalSalesSummary.totalProfit += (+salesSummaryByTSSQ.salesSummary.totalProfit);
                
                this.salesSummaryByTSSQList.push(salesSummaryByTSSQ);  
              }            
            }//end of loop on itemQualityLevelList

            doAverage(salesSummaryByTSS.salesSummary,10);
            doRound(salesSummaryByTSS.salesSummary,10);
            //console.log("Pushing salesSummary for " + salesSummaryBySS.itemShape.id + " " + salesSummaryBySS.itemSieve.name + " " + salesSummaryBySS.salesSummary.totalWeight);
            this.salesSummaryByTSSList.push(salesSummaryByTSS);
          }        
        }//end of loop on itemSieveList
      }      
    }//end of loop on itemShapeList
    doRound(this.totalSalesSummary,10);
    doAverage(this.totalSalesSummary,10);
  }

  
  getMarketRateForItem(p:PackagedItem):number
  {
    //console.log("Welcome to SalesStockSummaryPage:getMarketRate() " + p.id);
    if(this.itemPriceList.length > 0)
    {
      for(let i=0;i<this.itemPriceList.length;i++)
      {
        if( (this.itemPriceList[i].shapeID === p.itemShape.id)
          && (this.itemPriceList[i].sieveID === p.itemSieve.id)
          && (this.itemPriceList[i].qualityLevelID === p.itemQualityLevel.id))
        {
          return this.itemPriceList[i].rate;
        }
      }
    }
    else
    {
      return -1;
    }
  }

  getMarketValueForItem(p:PackagedItem):number
  {
   // console.log("Welcome to SalesStockSummaryPage:getMarketValueForItem() " + p.id);
    let marketRate:number=0;
    marketRate = this.getMarketRateForItem(p);
    return round((marketRate * p.itemWeight),2);
  }

  
  calculateMfgParameters()
  {
    console.log("Welcome to SalesStockSummaryPage::calculateMfgParameters() ");
    //this.avgMfgCostForSaleable = 0;
    this.avgP2SGain = 0.0;
    this.avgPolishGain = 0.0;
    this.avgRoughGain = 0.0;
    let totalRoughWeight:number=0;
    let totalSaleablePolishedWeight:number=0;
    let totalSaleableRoughWeight:number=0;
    let totalSaleableWeight:number=0;

    for(let i=0;i<this.packagedItemList.length;i++)
    {
      if((this.iSeriesID) && (this.iTrxPartyName) && (this.iTrxDate) && (this.iTrxWeight) && (this.iTrxRate) && (this.iTrxCurrency))
      {
        console.log("Calculating Gain for Series");
        if((this.packagedItemList[i].packageGroupID === this.iSeriesID)
            && (this.packagedItemList[i].trxPartyName === this.iTrxPartyName)
            && (this.packagedItemList[i].trxDate === this.iTrxDate)
            && (this.packagedItemList[i].trxWeight === this.iTrxWeight)
            && (this.packagedItemList[i].trxRate === this.iTrxRate)
            && (this.packagedItemList[i].trxCurrency === this.iTrxCurrency))
        {
         // this.avgMfgCostForSaleable += +this.packagedItemList[i].accumulatedTaskCost;
          if(this.packagedItemList[i].itemType === myConstants.itemType.POLISHED)
          {
            totalSaleablePolishedWeight += +this.packagedItemList[i].itemWeight;
          }
          else
          {
            totalSaleableRoughWeight += +this.packagedItemList[i].itemWeight;
          }
          totalSaleableWeight += +this.packagedItemList[i].itemWeight;
          totalRoughWeight += +this.packagedItemList[i].roughWeight;                
        }
      }
      else
      {
       // this.avgMfgCostForSaleable += +this.packagedItemList[i].accumulatedTaskCost;
        if(this.packagedItemList[i].itemType === myConstants.itemType.POLISHED)
        {
          totalSaleablePolishedWeight += +this.packagedItemList[i].itemWeight;
        }
        else
        {
          totalSaleableRoughWeight += +this.packagedItemList[i].itemWeight;
        }
        totalSaleableWeight += +this.packagedItemList[i].itemWeight;
        totalRoughWeight += +this.packagedItemList[i].roughWeight;       
      }
      console.log("totalSaleablePolishedWeight " + totalSaleablePolishedWeight);
      console.log("totalSaleableWeight " + totalSaleableWeight);
      console.log("totalRoughWeight " + totalRoughWeight);
    }
    if(totalRoughWeight > 0)
    {
      //this.avgMfgCostForSaleable = round((this.avgMfgCostForSaleable/totalPolishedWeight),10);
      this.avgP2SGain = round((totalSaleableWeight/totalRoughWeight)*100,10);
      if((+totalSaleableWeight - +totalSaleablePolishedWeight) !== (totalRoughWeight))
      {
        this.avgPolishGain = round((totalSaleablePolishedWeight/(totalRoughWeight-totalSaleableWeight+totalSaleablePolishedWeight))*100,10);
        this.avgRoughGain = round(((totalSaleableRoughWeight)/(totalSaleableWeight-totalSaleablePolishedWeight))*100,10);
      }
      else
      {
        this.avgPolishGain = 0.0;
        this.avgRoughGain = 0.0;
      //  this.avgMfgCostForSaleable = 0.0
      }      
    }
    else
    {
    //  this.avgMfgCostForSaleable = 0;
      this.avgP2SGain = 0;
      this.avgPolishGain = 0;
      this.avgRoughGain = 0;
    }
    console.log("this.avgP2SGain " + this.avgP2SGain);
    this.calculateCostPerSaleableUnit();
  }

  calculatePurchaseCostAndInterest()
  {
    console.log("Welcome to SalesStockSummaryPage::calculatePurchaseCostAndInterest() ");
    if((this.finTrxList.length > 0) && (this.purchaseTrxList.length > 0))
    {
      let supplierList:Array<BusinessContact> = [];
      let finTrxList:Array<FinanceTransaction> = [];
      let bizTrxList:Array<BusinessTransaction> = [];

      if((this.iSeriesID) && (this.iTrxPartyName) && (this.iTrxDate) && (this.iTrxWeight) && (this.iTrxRate) && (this.iTrxCurrency))
      {//calculate average as per purchase trx of seried selected
        let index = 0
        for(index = 0;index<this.packagedItemList.length;index++)
        {
          if((this.packagedItemList[index].packageGroupID === this.iSeriesID)
          && (this.packagedItemList[index].trxPartyName === this.iTrxPartyName)
          && (this.packagedItemList[index].trxDate === this.iTrxDate)
          && (this.packagedItemList[index].trxWeight === this.iTrxWeight)
          && (this.packagedItemList[index].trxRate === this.iTrxRate)
          && (this.packagedItemList[index].trxCurrency === this.iTrxCurrency))
          {
            break;
          }
        }
        console.log("index " + index);
        if(index < this.packagedItemList.length)
        {
          bizTrxList = this.purchaseTrxList.filter(bizTrx => (bizTrx.ID === this.packagedItemList[index].trxID));
        }
      }
      else
      {//calculate average as per ALL purchase trx.
        bizTrxList = this.purchaseTrxList;          
      } 
      console.log("bizTrxList.length " + bizTrxList.length);  
      let bizTrxSummary = {} as BusinessTransactionSummary;
      let totalPurchaseWeight = 0;
      this.trxAvgPurchaseCost = 0;
      this.trxAvgInterestCost = 0;
      
      for(let i=0;i<bizTrxList.length;i++)
      {
        totalPurchaseWeight += +bizTrxList[i].trxQty;
        
        finTrxList = this.finTrxList.filter(finTrx => finTrx.BizTrxID === bizTrxList[i].ID);
        //partyList = supplierList.filter(p => p.id === bizTrxList[i].trxPartyID);
        if((bizTrxList[i]) && (true) && (finTrxList.length>0))
        {
          bizTrxSummary = getBusinessTransactionSummary(bizTrxList[i],finTrxList,this.currencyExchangeRate);
          if(bizTrxSummary)
          {
            console.log("bizTrxSummary.amtReceviedOrPaidinLocal " + bizTrxSummary.amtReceviedOrPaidinLocal);
            console.log("bizTrxSummary.interestRealizedinLocal " + bizTrxSummary.interestRealizedinLocal);
            console.log("bizTrxSummary.amtPendinginLocal " + bizTrxSummary.amtPendinginLocal);
            console.log("bizTrxSummary.interestUnRealizedinLocal " + bizTrxSummary.interestUnRealizedinLocal);
            this.trxAvgPurchaseCost += round((+bizTrxSummary.amtReceviedOrPaidinLocal + +bizTrxSummary.amtPendinginLocal),10);
            this.trxAvgInterestCost += round((+bizTrxSummary.interestRealizedinLocal + +bizTrxSummary.interestUnRealizedinLocal),10);
            }
        }//end of if((this.purchaseTrxList[i]) && (partyList.length > 0) && (finTrxList.length>0))
      //  console.log("AVG:this.avgPurchaseCost/totalPurchaseWeight " + this.avgPurchaseCost + "/" + totalPurchaseWeight);
      }//end of for(let i=0;i<this.purchaseTrxList.length;i++)
      this.trxAvgPurchaseCost = round((this.trxAvgPurchaseCost/totalPurchaseWeight),10);
      this.trxAvgInterestCost = round((this.trxAvgInterestCost/totalPurchaseWeight),10);
      console.log("this.trxAvgPurchaseCost " + this.trxAvgPurchaseCost);
      console.log("this.trxAvgInterestCost " + this.trxAvgInterestCost);

      this.calculateCostPerSaleableUnit();
    }
  }

  calculateCostFactor()
  {
    console.log("Welcome to SalesStockSummaryPage::calculateCostFactor() ");
    let totalMarketValueForSaleable:number=0;
    let totalSaleableWeight=0;
    let avgMarketRateForSaleable:number=0;

    for(let i=0;i<this.packagedItemList.length;i++)
    {
      let interest:number=0;
      if((this.iSeriesID) && (this.iTrxPartyName) && (this.iTrxDate) && (this.iTrxWeight) && (this.iTrxRate) && (this.iTrxCurrency))
      {
        if((this.packagedItemList[i].packageGroupID === this.iSeriesID)
          && (this.packagedItemList[i].trxPartyName === this.iTrxPartyName)
          && (this.packagedItemList[i].trxDate === this.iTrxDate)
          && (this.packagedItemList[i].trxWeight === this.iTrxWeight)
          && (this.packagedItemList[i].trxRate === this.iTrxRate)
          && (this.packagedItemList[i].trxCurrency === this.iTrxCurrency))
        {
          this.packagedItemList[i].itemValuation.costFactor = 0;
          totalSaleableWeight += +this.packagedItemList[i].itemWeight;
          totalMarketValueForSaleable += round(this.packagedItemList[i].itemValuation.rate*this.packagedItemList[i].itemWeight,10);
        }        
      }
      else
      {
        this.packagedItemList[i].itemValuation.costFactor = 0;
        totalSaleableWeight += +this.packagedItemList[i].itemWeight;
        totalMarketValueForSaleable += round(this.packagedItemList[i].itemValuation.rate*this.packagedItemList[i].itemWeight,10);
      }      
    }//for(let i=0;i<this.packagedItemList.length;i++)
    if(totalSaleableWeight > 0)
    {
      avgMarketRateForSaleable = round(totalMarketValueForSaleable/totalSaleableWeight,10);
      if(avgMarketRateForSaleable > 0 )
      {
        for(let i=0;i<this.packagedItemList.length;i++)
        {
          if((this.iSeriesID) && (this.iTrxPartyName) && (this.iTrxDate) && (this.iTrxWeight) && (this.iTrxRate) && (this.iTrxCurrency))
          {
            this.packagedItemList[i].itemValuation.costFactor = round(this.packagedItemList[i].itemValuation.rate/avgMarketRateForSaleable,10);
          }
          else
          {
            this.packagedItemList[i].itemValuation.costFactor = round(this.packagedItemList[i].itemValuation.rate/avgMarketRateForSaleable,10);
          }    
          console.log("this.packagedItemList[i].itemValuation.costFactor " + this.packagedItemList[i].itemValuation.costFactor);
        }
      }      
    }
    this.buildSalesStockSummary();          
  }

  calculateMarketValueOfSaleable()
  {
    console.log("Welcome to SalesStockSummaryPage::calculateMarketValueOfSaleable() ");
    let totalMarketValueForSaleable:number=0;
    let totalSaleableWeight=0;
    let avgMarketRateForSaleable:number=0;

    for(let i=0;i<this.packagedItemList.length;i++)
    {
      let interest:number=0;
      this.packagedItemList[i].itemValuation.rate = +this.getMarketRateForItem(this.packagedItemList[i]);
      this.packagedItemList[i].itemValuation.value = round((+this.packagedItemList[i].itemValuation.rate * +this.packagedItemList[i].itemWeight),10);
      this.packagedItemList[i].itemValuation.costFactor = 0;
      totalSaleableWeight += +this.packagedItemList[i].itemWeight;
      totalMarketValueForSaleable += round(this.packagedItemList[i].itemValuation.rate*this.packagedItemList[i].itemWeight,10);
    }//for(let i=0;i<this.packagedItemList.length;i++)
    if(totalSaleableWeight > 0)
    {
      avgMarketRateForSaleable = round(totalMarketValueForSaleable/totalSaleableWeight,10);
      if(avgMarketRateForSaleable > 0 )
      {
        for(let i=0;i<this.packagedItemList.length;i++)
        {
          this.packagedItemList[i].itemValuation.costFactor = round(this.packagedItemList[i].itemValuation.rate/avgMarketRateForSaleable,10);
          console.log("this.packagedItemList[i].itemValuation.costFactor " + this.packagedItemList[i].itemValuation.costFactor);
        }
      }      
    }
    this.calculateCostFactor();      
  }
  */
  onPackageClicked(pSelectedItem:PackagedItem)
  {
    console.log("Welcome to SalesStockSummaryPage:onPackageClicked()");
    console.log("curTaskCode " + pSelectedItem.pRule.curTaskCode);
    console.log("nextTaskCode " + pSelectedItem.pRule.nextTaskCode);
    console.log("id " + pSelectedItem.id);
    this.navCtrl.push('ItemDetailPage',{User:this.user,pItem:pSelectedItem}); 
  }

  prepareSearchList()
  {
    console.log("Welcome to SalesStockSummaryPage:prepareSearchList()");
    this.searchList = this.rateCardProvider.getSeriesList();
  }

  onSearch()
  {
    console.log("Welcome to SalesStockSummaryPage:onSearch()");
    // console.log('inside onSearch this.SearchInput : ' + this.SearchInput);
    if((this.searchInput.length == 0) || (this.bSearching = false))
    {
      this.searchList = [];
     // this.searchSeriesID = "";
      this.iSeriesID = "";
      this.iSeriesName = "";
      this.iTrxPartyName = "";
      this.iTrxDate = "";
      this.iTrxWeight=null;
      this.iTrxRate=null;
      this.iTrxCurrency="";

      this.rateCardProvider.resetSeries();
    }
    else
    {
      this.bSearching = true;
  	  this.prepareSearchList();
  	
      this.searchList = this.searchList.filter((item) => {
        if(item.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1)
        {       
          return true;
        }
        else
          return false;
      });
    }
  	
  }

  inputSelected(searchItem)
  {
    console.log("Welcome to SalesStockSummaryPage:inputSelected()" + searchItem);
    this.searchList = [];
//    this.searchSeriesID = ""+searchItem;
    this.searchInput = searchItem;
    this.bSearching = false;
    this.rateCardProvider.searchSeries(this.searchInput);  
    console.log("this.rateCardProvider.seriesID " + this.rateCardProvider.seriesID);
    this.iSeriesID = this.rateCardProvider.seriesID;
    this.iSeriesName = this.rateCardProvider.seriesName;
    this.iTrxPartyName = this.rateCardProvider.trxPartyName;
    this.iTrxDate = this.rateCardProvider.trxDate;
    this.iTrxRate = this.rateCardProvider.trxRate;
    this.iTrxWeight = this.rateCardProvider.trxWeight;
    this.iTrxCurrency = this.rateCardProvider.trxCurrency;  
    this.filterPackagedItem();
  }

  addPolishToSales()
  {
    console.log("Welcome to SalesStockSummaryPage:addPolishToSales()");

    /*
    const modal = this.modalCtrl.create('AddPolishToSalesPage',{User:this.user, shapeList:this.itemShapeList, sieveList:this.itemSieveList, qualityLevelList:this.itemQualityLevelList});
    modal.present();
    modal.onDidDismiss((selectValue) => {
    }); 
    */
   this.navCtrl.push('AddPolishToSalesPage',{User:this.user,pItem:null});    
  }

  gotoSalesTransactionItems()
  {
    console.log("Welcome to SalesStockSummaryPage:gotoSalesTransactionItems()");

    /*
    const modal = this.modalCtrl.create('AddPolishToSalesPage',{User:this.user, shapeList:this.itemShapeList, sieveList:this.itemSieveList, qualityLevelList:this.itemQualityLevelList});
    modal.present();
    modal.onDidDismiss((selectValue) => {
    }); 
    */
   this.navCtrl.push('SalesTransactionItemsPage',{User:this.user,fromEntity:"INVENTORY"});

  }

}
