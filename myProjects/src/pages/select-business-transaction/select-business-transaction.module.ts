import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectBusinessTransactionPage } from './select-business-transaction';

@NgModule({
  declarations: [
    SelectBusinessTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(SelectBusinessTransactionPage),
  ],
})
export class SelectBusinessTransactionPageModule {}
