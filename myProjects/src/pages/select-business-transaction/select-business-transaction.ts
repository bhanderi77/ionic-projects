import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

import { TransactionProvider } from '../../providers/transaction/transaction';
import { BusinessTransaction } from '../../model/businessTransaction';

import { User, checkIfValid } from '../../model/user';
import { Subscription } from 'rxjs';
import { myConstants, round } from '../../model/myConstants';

/**
 * Generated class for the SelectBusinessTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-business-transaction',
  templateUrl: 'select-business-transaction.html',
})
export class SelectBusinessTransactionPage {
  private user:User;  
  private bizTrxList:Array<BusinessTransaction>;
  private mySubscriptions:Subscription;

  constructor(public navCtrl: NavController, 
    private viewCtrl: ViewController,
    private transactionProvider: TransactionProvider,
    public navParams: NavParams) 
  {
    this.user = {} as User;
    this.user = navParams.get('User');
    this.bizTrxList = []
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad SelectBusinessTransactionPage');
  }

  ionViewWillEnter() 
  {
    console.log('ionViewWillEnter SelectBusinessTransactionPage');
    this.initializeData();
  }

  ionViewWillLeave() 
  {
    console.log('ionViewWillLeave SelectBusinessTransactionPage');
    this.mySubscriptions.unsubscribe();
  }

  initializeData()
  {
    console.log('Welcome to SelectBusinessTransactionPage::initializeData() ');
    this.mySubscriptions = new Subscription();
    this.mySubscriptions.add(this.transactionProvider.getAllBusinessTransactions(this.user,myConstants.trxType.BUY).valueChanges()
    .subscribe(businessTransactionList => {
      console.log("Subscribed to getAllBusinessTransactions: " + businessTransactionList.length);
      this.bizTrxList = businessTransactionList;
    }));

  }

  select(bizTrx:BusinessTransaction)
  {
    console.log("Welcome to SelectBusinessTransactionPage::(bizTrx) " + bizTrx.trxPartyID);
    //this.viewCtrl.dismiss({trxPartyID:bizTrx.trxPartyID,trxID:bizTrx.ID}); 
    this.viewCtrl.dismiss({selectedBizTrx:bizTrx});   
  }

  close()
  {
    this.viewCtrl.dismiss({selectedBizTrx:null});  
  }

}
