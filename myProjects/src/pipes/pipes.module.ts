import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IndianCurrency } from './IndianCurrency.pipe';


@NgModule({
  declarations: [
    IndianCurrency
  ],
  imports: [
  ],
  exports:[
      IndianCurrency
  ]
})
export class PipesModule {}
