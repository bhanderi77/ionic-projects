export interface TaxRate {
    rateCGST: number;
    rateSGST: number;
    rateIGST: number;    
}

export interface TaxAmount {
    amtCGST: number;
    amtSGST: number;
    amtIGST: number;
    totalAmount: number;
}