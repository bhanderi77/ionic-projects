export interface Action {
    id:string;
    milestoneID:string;
    description:string;
    status:string;
    owner:string;
    RAG:string;
    raisedDate:string;
    estimatedClosureDate:string;
    actualClosureDate:string;    
    remarks:string;
}