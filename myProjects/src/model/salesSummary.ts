import { myConstants, round } from './myConstants';
import { ItemType,ItemShape,ItemSieve,ItemQualityLevel } from './packagedItem';
import { SalesItem } from './salesItem';
import { TaxAmount } from './tax';

export class SalesSummary {
    totalSaleableWeight:number;
    totalPurchaseWeight:number;

    totalSaleablePolishedWeight:number;
    totalSaleableRoughWeight:number;
    totalSaleableTopsWeight:number;

    totalQty:number;

    avgValueFactor:number;
    
    avgPriceRate:number;
    avgSalePrice:number;
    avgSalesTax:TaxAmount;
    avgGrossPrice:number;
    avgSalesBrokerage:number;
    avgNetPrice:number;
    avgSalesGain:number;

    avgPurchaseCost:number;
    avgInterestCost:number;
    avgMfgCost:number;
    avgGrossCost:number;
    avgGrossCostAtDueTerm:number;
    
    avgProfit:number;
    avgProfitMargin:number;

    totalPriceValue:number;
    totalSaleValue:number;
    totalSalesTax:TaxAmount;
    totalGrossValue:number;
    totalSalesBrokerage:number;
    totalNetValue:number;
    totalSalesGain:number;

    totalPurchaseCost:number;
    totalInterestCost:number;
    totalMfgCost:number;
    totalGrossCost:number;
    totalGrossCostAtDueTerm:number;
    
    totalProfit:number;
    totalProfitMargin:number;

    constructor()
    {
        this.initialize();
    }
    
    initialize()
    {
        this.totalSaleableWeight = 0;
        this.totalPurchaseWeight = 0;
        
        this.totalSaleablePolishedWeight = 0;
        this.totalSaleableRoughWeight = 0;
        this.totalSaleableTopsWeight = 0;

        this.totalQty = 0;
        this.avgValueFactor = 0;

        this.avgPurchaseCost = 0;
        this.avgInterestCost = 0;
        this.avgMfgCost = 0;
        this.avgGrossCost = 0;
        this.avgGrossCostAtDueTerm = 0;

        this.avgPriceRate = 0;
        this.avgSalePrice = 0;
        this.avgSalesTax = {} as TaxAmount;
        this.avgSalesTax.amtCGST = 0;
        this.avgSalesTax.amtSGST = 0;
        this.avgSalesTax.amtIGST = 0;
        this.avgSalesTax.totalAmount = 0;

        this.avgGrossPrice = 0;
        this.avgSalesBrokerage = 0;
        this.avgNetPrice = 0;
        this.avgSalesGain = 0;

        this.avgProfit = 0;
        this.avgProfitMargin = 0;
        
        this.totalPurchaseCost = 0;
        this.totalInterestCost = 0;
        this.totalMfgCost = 0;
        this.totalGrossCost = 0;
        this.totalGrossCostAtDueTerm = 0;

        this.totalPriceValue = 0;
        this.totalSaleValue = 0;
        this.totalSalesTax = {} as TaxAmount;
        this.totalSalesTax.amtCGST = 0;
        this.totalSalesTax.amtSGST = 0;
        this.totalSalesTax.amtIGST = 0;
        this.totalSalesTax.totalAmount = 0;
       
        this.totalGrossValue = 0;
        this.totalSalesBrokerage = 0;
        this.totalNetValue = 0;
        this.totalSalesGain = 0;
        
        this.totalProfit = 0;
        this.totalProfitMargin = 0;    
    }
    
    doRound(digits:number)
    {
        if(digits >= 0)
        {
            this.totalSaleableWeight = round(this.totalSaleableWeight,3);
            this.totalPurchaseWeight = round(this.totalPurchaseWeight,3);
            this.totalQty = round(this.totalQty,0);
            this.totalSaleablePolishedWeight = round(this.totalSaleablePolishedWeight,3);
            this.totalSaleableRoughWeight = round(this.totalSaleableRoughWeight,3);
            this.totalSaleableTopsWeight = round(this.totalSaleableTopsWeight,3);
    
            //this.avgValueFactor = this.avgValueFactor;

            this.totalPriceValue = round(this.totalPriceValue,digits);
            this.totalSaleValue = round(this.totalSaleValue,digits);
            this.totalSalesTax.amtCGST = round(this.totalSalesTax.amtCGST,digits);
            this.totalSalesTax.amtSGST = round(this.totalSalesTax.amtSGST,digits);
            this.totalSalesTax.amtIGST = round(this.totalSalesTax.amtIGST,digits);
            this.totalSalesTax.totalAmount = round(this.totalSalesTax.totalAmount,digits);
            this.totalGrossValue = round(this.totalGrossValue,digits);
            this.totalSalesBrokerage = round(this.totalSalesBrokerage,digits);
            this.totalNetValue = round(this.totalNetValue,digits);
            this.totalSalesGain = round(this.totalSalesGain,digits);
            
            this.avgPriceRate = round(this.avgPriceRate,digits);
            this.avgSalePrice = round(this.avgSalePrice,digits);
            this.avgSalesTax.amtCGST = round(this.avgSalesTax.amtCGST,2);
            this.avgSalesTax.amtSGST = round(this.avgSalesTax.amtSGST,2);
            this.avgSalesTax.amtIGST = round(this.avgSalesTax.amtIGST,2);
            this.avgSalesTax.totalAmount = round(this.avgSalesTax.totalAmount,2);
            this.avgGrossPrice = round(this.avgGrossPrice,digits);
            this.avgSalesBrokerage = round(this.avgSalesBrokerage,2);
            this.avgNetPrice = round(this.avgNetPrice,digits);
            this.avgSalesGain = round(this.avgSalesGain,digits);

            this.avgPurchaseCost = round(this.avgPurchaseCost,digits);
            this.avgInterestCost = round(this.avgInterestCost,digits);
            this.avgMfgCost = round(this.avgMfgCost,digits);
            this.avgGrossCost = round(this.avgGrossCost,digits);
            this.avgGrossCostAtDueTerm = round(this.avgGrossCostAtDueTerm,digits);
            
            this.avgProfit = round(this.avgProfit,digits);
            this.avgProfitMargin = round(this.avgProfitMargin,2);

            this.totalPurchaseCost = round(this.totalPurchaseCost,digits);
            this.totalInterestCost = round(this.totalInterestCost,digits);
            this.totalMfgCost = round(this.totalMfgCost,digits);
            this.totalGrossCost = round(this.totalGrossCost,digits);
            this.totalGrossCostAtDueTerm = round(this.totalGrossCostAtDueTerm,digits);
            
            this.totalProfit = round(this.totalProfit,digits);
            this.totalProfitMargin = round(this.totalProfitMargin,2);
        }    
    }
    
    doTotal(digits:number)
    {
        if(digits >= 0)
        {
            this.totalPurchaseCost = round(this.avgPurchaseCost*this.totalSaleableWeight,digits);
            this.totalInterestCost = round(this.avgInterestCost*this.totalSaleableWeight,digits);
            this.totalMfgCost = round(this.avgMfgCost*this.totalSaleableWeight,digits);
            this.totalGrossCost = round(this.avgGrossCost*this.totalSaleableWeight,digits);
            this.totalGrossCostAtDueTerm = round(this.avgGrossCostAtDueTerm*this.totalSaleableWeight,digits);

            this.totalPriceValue = round(this.avgPriceRate*this.totalSaleableWeight,digits);
            this.totalSaleValue = round(this.avgSalePrice*this.totalSaleableWeight,digits);
            this.totalSalesTax.amtCGST = round(this.avgSalesTax.amtCGST*this.totalSaleableWeight,digits);
            this.totalSalesTax.amtSGST = round(this.avgSalesTax.amtSGST*this.totalSaleableWeight,digits);
            this.totalSalesTax.amtIGST = round(this.avgSalesTax.amtIGST*this.totalSaleableWeight,digits);
            this.totalSalesTax.totalAmount = round(this.avgSalesTax.totalAmount*this.totalSaleableWeight,digits);
            this.totalGrossValue = round(this.avgGrossPrice*this.totalSaleableWeight,digits);
            this.totalSalesBrokerage = round(this.avgSalesBrokerage*this.totalSaleableWeight,digits);
            this.totalNetValue = round(this.avgNetPrice*this.totalSaleableWeight,digits);
            this.totalSalesGain = round(this.avgSalesGain*this.totalSaleableWeight,digits);
            this.totalProfit = round(this.avgProfit*this.totalSaleableWeight,digits);
            this.totalProfitMargin = this.avgProfitMargin;
            
            /*
            if(this.totalGrossCostAtDueTerm)
            {//For Sales transaction
                this.totalProfit = round(this.totalNetValue-this.totalGrossCostAtDueTerm,digits);
                this.totalProfitMargin = round(((this.totalProfit/this.totalGrossCostAtDueTerm)*100),2);         
            }
            else if(this.totalGrossCost)
            {//Not from Sales Transaction
                this.totalProfit = round(this.totalPriceValue-this.totalGrossCost,digits);
                this.totalProfitMargin = round(((this.totalProfit/this.totalGrossCost)*100),2);         
            }
            */
        }    
    }
    
    doAverage(digits:number)
    {
        console.log("Welcome to SalesSummary:doAverage(digits) " + digits);
        if(digits >= 0)
        {
            this.avgPurchaseCost = 0;
            this.avgInterestCost = 0;
            this.avgMfgCost = 0;
            this.avgGrossCost = 0;
            this.avgGrossCostAtDueTerm = 0;

            this.avgPriceRate = 0;
            this.avgSalePrice = 0;
            this.avgSalesTax = {} as TaxAmount;
            this.avgSalesTax.amtCGST = 0;
            this.avgSalesTax.amtSGST = 0;
            this.avgSalesTax.amtIGST = 0;
            this.avgSalesTax.totalAmount = 0;

            this.avgGrossPrice = 0;
            this.avgSalesBrokerage = 0;
            this.avgNetPrice = 0;
            this.avgSalesGain = 0;

            this.avgProfit = 0;
            this.avgProfitMargin = 0;
            
            if(this.totalSaleableWeight)
            {
                this.avgPurchaseCost = round((this.totalPurchaseCost/this.totalSaleableWeight),2);
                this.avgInterestCost = round((this.totalInterestCost/this.totalSaleableWeight),2);
                this.avgMfgCost = round((this.totalMfgCost/this.totalSaleableWeight),2);
                this.avgGrossCost = round((this.totalGrossCost/this.totalSaleableWeight),2);
                this.avgGrossCostAtDueTerm = round((this.totalGrossCostAtDueTerm/this.totalSaleableWeight),2);

                this.avgPriceRate = round((this.totalPriceValue/this.totalSaleableWeight),digits);
                this.avgSalePrice = round((this.totalSaleValue/this.totalSaleableWeight),digits);
                this.avgSalesTax.amtCGST = round((this.totalSalesTax.amtCGST/this.totalSaleableWeight),2);
                this.avgSalesTax.amtSGST = round((this.totalSalesTax.amtSGST/this.totalSaleableWeight),2);
                this.avgSalesTax.amtSGST = round((this.totalSalesTax.amtIGST/this.totalSaleableWeight),2);
                this.avgSalesTax.totalAmount = round((this.totalSalesTax.totalAmount/this.totalSaleableWeight),2);
                this.avgGrossPrice = round((this.totalGrossValue/this.totalSaleableWeight),digits);
                this.avgSalesBrokerage = round((this.totalSalesBrokerage/this.totalSaleableWeight),2);
                this.avgNetPrice = round((this.totalNetValue/this.totalSaleableWeight),digits);
                this.avgSalesGain = round((this.totalSalesGain/this.totalSaleableWeight),digits);

                this.avgProfit = round((this.totalProfit/this.totalSaleableWeight),digits);
                if(this.avgGrossCostAtDueTerm > 0)
                {
                    this.avgProfitMargin = round(((this.avgProfit/this.avgGrossCostAtDueTerm)*100),2);
                }
                else if(this.avgGrossCost > 0)
                {
                    this.avgProfitMargin = round(((this.avgProfit/this.avgGrossCost)*100),2);
                }                
            }
            
            /*
            this.totalProfitMargin = 0;
            if(this.totalGrossCostAtDueTerm > 0)
            {
                this.totalProfit = round(this.totalNetValue-this.totalGrossCostAtDueTerm,digits);
                this.totalProfitMargin = round(((this.totalProfit/this.totalGrossCostAtDueTerm)*100),2);
            }
            else if(this.totalGrossCost > 0)
            {
                this.totalProfit = round(this.totalNetValue-this.totalGrossCost,digits);
                this.totalProfitMargin = round(((this.totalProfit/this.totalGrossCost)*100),2);
            }*/
        }    
    }
}
  
export class SalesSummaryByTSSQ extends SalesSummary {
    itemType:ItemType;
    itemShape:ItemShape;
    itemSieve:ItemSieve;
    itemQualityLevel:ItemQualityLevel;
    //salesSummary:SalesSummary;
    salesItemList:Array<SalesItem>;

    constructor()
    {
        super();
        this.itemType = {} as ItemType;
        this.itemShape = {} as ItemShape;
        this.itemSieve = {} as ItemSieve;
        this.itemQualityLevel = {} as ItemQualityLevel;
        this.salesItemList = [];
    }
}

export class SalesSummaryByTSS extends SalesSummary{
    itemType:ItemType;
    itemShape:ItemShape;
    itemSieveMin:ItemSieve;
    itemSieveMax:ItemSieve;
    //salesSummary:SalesSummary;
    salesItemList:Array<SalesItem>;

    constructor()
    {
        super();
        this.itemType = {} as ItemType;
        this.itemShape = {} as ItemShape;
        this.itemSieveMin = {} as ItemSieve;
        this.itemSieveMax = {} as ItemSieve;
        this.salesItemList = [];
    }
}