export interface Organisation {
    id: string;
    name:string;
    email:string;
    phone_1:string;
    phone_2:string;
    address_1:string;
    address_2:string;
    city:string;
    stateCode:string;
    pinCode:string;
    PAN:string;
    GSTIN:string;
    bankAccountID:string;
    bankAccountName:string;
    bankName:string;
    bankBranch:string;
    bankIFSC:string;
}