export interface appSettings {
    asForiegnCurrency: string;
    asLocalCurrency: string;
    asConversionRate: number;
    asDueDays: number;
    asCGST: number;
    asSGST: number;
    asIGST: number;
    //asRateOfInterest: number;
}
