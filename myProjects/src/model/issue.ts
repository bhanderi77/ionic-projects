export interface Issue {
    id:string;
    milestoneID:string;
    description:string;
    impact:string;
    owner:string;
    raisedDate:string;
    estimatedClosureDate:string;
    actualClosureDate:string;
    status:string;
    RAG:string;
    remarks:string;
}