export interface Risk {
    id:string;
    milestoneID:string;
    description:string;
    impact:string;
    mitigation:string;
    owner:string;
    plannedClosureDate:string;
    estimatedClosureDate:string;
    actualClosureDate:string;
    status:string;
    RAG:string;
    remarks:string;
}