import { myConstants } from '../model/myConstants';
//import { datetime } from '';

export interface PackageRule {
    id:string;
    creation_time:string;
    curTaskCode: string;
    //packageStatus: myConstants.packageStatus;    
    nextTaskCode: string;
    sequenceID: number;
    bGroupRead: boolean;    
    bMultipleOutput: boolean;
    bPartialAssign: boolean;
    bReducedOutput:boolean; 
    bIncursCost:boolean;
    bFirst:boolean;
    bLast:boolean;   
}
