export interface Milestone {
    id:string;
    description:string;
    targetClosureDate:string;
    estimatedClosureDate:string;
    actualClosureDate:string;
    completionPercentage:number;
    RAG:string;
    remarks:string;
}