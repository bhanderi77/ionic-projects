export namespace myConstants {
    export const currenyRateForeignToLocal = 66;
    export const foreignCurrency = "USD";
    export const localCurrency = "INR";
    export const enum bizTrxStatus {
        DRAFT = "DRAFT",
        OPEN = "OPEN",
        CLOSED = "CLOSED"
    };
    export const  ROI: number = 1.5;
    export const enum partyCategory {
        BUYER = "Buyer",
        SUPPLIER = "Supplier",
        BROKER = "Broker",
        LENDER = "Lender",
        BORROWER = "Borrower",
        MANUFACTURER = "Manufacturer"
    };
    export const enum itemType {
        ROUGH = "ROUGH",
        POLISHED = "POLISHED",
        TOPS = "TOPS",
        ALL = "All"
    }
    export const enum trxType {
        BUY = "Buy",
        SELL = "Sell",
        MANUFACTURING = "In_Manufacturing",
        DUE_DEBIT = "Due_Debit",
        DUE_CREDIT = "Due_Credit",
        IN_CREDIT = "In_Credit",
        OUT_DEBIT = "Out_Debit"
    };
    export const enum DEFALUT {
        DUE_DAYS = 90,
        BROKERAGE_RATE = 1.00
    };
    export const enum paymentMethod {
        CASH = "cash",
        CHEQUE = "cheq",
        COURIER = "cour"
    }
    export const enum packageStatus {
        RD = 'RD',
        PD = 'PD',
        IP = 'IP',
        CO = 'CO'
      };
    export const enum taskCode {
        PURCHASE = 'PURCHASE',        
        PLANNING = 'PLANNING',
        MFG_ISSUE = 'MFG_ISSUE',
        MFG_RETURN = 'MFG_RETURN',
        ASSORTMENT = 'ASSORTMENT',
        SALES = 'SALES',
        MANUAL = 'MANUAL',
        NONE = 'NONE'
    };
    export const enum itemShape {
        ROUND = 'Round',
        FANCY = 'Fancy',
        ALL = 'ALL',
        NOTDEFINED = 'ND'
    };
    export const enum itemShapeRank {
        ALL = 0,
        FANCY = 100,
        TOPS = 200,
        ND = 999
    };
    export const enum itemSieveRank {
        ALL = 0,
        LOWEST = 1,
        HIGHEST = 99,
        ND = 998
    };
    export const enum itemQualityLevelRank {
        ALL = 0,
        LOWEST = 1,
        HIGHEST = 99,
        ND = 998
    };

    export const enum itemSieve {
        ALL = 'ALL',
        NOTDEFINED = 'ND'
    };
    export const enum polishQualityLevel {
        ALL = 'ALL',
        NOTDEFINED = 'ND'
    };
        
    export const enum uom {
        CTS = 'Carat',
        DAY = 'Day',
        HOUR = 'Hour'
    }
    export const enum confidentialityLevel {
        NONE = 0,
        PUBLIC = 1,
        CONFIDENTIAL = 2
    }
    export const TRUE=1;
    export const FALSE=0;
    export const TASK_OUTPUT_MAX_REC=3;
    export const FIREBASE_ID_NOT_EXISTS='-1';
}

export function round(value, exp) 
{
    if (typeof exp === 'undefined' || +exp === 0)
      return Math.round(value);
    value = +value;
    exp = +exp;
  
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
      return NaN;
  
    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));
  
    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
  }