import { BusinessContact } from '../model/businessContact';
import { BusinessTransaction } from '../model/businessTransaction';
import { FinanceTransaction } from '../model/financeTransaction';
import { myConstants, round } from '../model/myConstants';

export interface BusinessTransactionSummary {
  businessTransaction: BusinessTransaction,
  trxLocalAmount: number,
  trxForiegnAmount: number,
  amtReceviedOrPaidinLocal: number,
  amtReceviedOrPaidinForiegn: number,
  amtPendinginLocal: number,
  amtPendinginForiegn: number,
  interestRealizedinLocal: number,
  interestRealizedinForiegn: number,
  interestUnRealizedinLocal: number,
  interestUnRealizedinForiegn: number,
  diffDays: number,
  finTrxs:Array<FinanceTransaction>
}

export interface PartySummary {
  sysDate: Date,
  profile:BusinessContact,
  businessSummary: BusinessSummary
}

export interface BusinessSummary{
  businessAmtTotalinLocal: number,
  //strBusinessAmtTotalinLocal: string,
  businessAmtTotalinForiegn: number,
  businessCurrency : string,
  amtReceviedOrPaidinLocal: number,
  amtReceviedOrPaidinForiegn: number,
  amtPendinginLocal: number,
  amtPendinginForiegn: number,
  interestRealizedinLocal: number,
  interestUnRealizedinLocal: number,
  interestRealizedinForiegn: number,
  interestUnRealizedinForiegn: number,

  bizTrxs:Array<BusinessTransaction>,    
  finTrxs:Array<FinanceTransaction>,
  bizTrxSummary:Array<BusinessTransactionSummary>
}

export function getBusinessTransactionSummary(bizTransaction:BusinessTransaction,finTrxs:Array<FinanceTransaction>,currentExchangeRate:number):BusinessTransactionSummary
{
  let bizTrxSummaryRec = {} as BusinessTransactionSummary;
  //p.bizTrxs[i].summary = {} as BusinessTransactionSummary;
  bizTrxSummaryRec.trxForiegnAmount = 0;
  bizTrxSummaryRec.trxLocalAmount = 0;
  bizTrxSummaryRec.amtReceviedOrPaidinForiegn = 0;      
  bizTrxSummaryRec.amtReceviedOrPaidinLocal = 0;
  bizTrxSummaryRec.amtPendinginForiegn = 0;      
  bizTrxSummaryRec.amtPendinginLocal = 0;
  bizTrxSummaryRec.interestRealizedinForiegn = 0;
  bizTrxSummaryRec.interestRealizedinLocal = 0;
  bizTrxSummaryRec.interestUnRealizedinForiegn = 0;
  bizTrxSummaryRec.interestUnRealizedinLocal = 0;
  
  bizTrxSummaryRec.businessTransaction = {} as BusinessTransaction;
  bizTrxSummaryRec.businessTransaction = bizTransaction;

  bizTrxSummaryRec.finTrxs = finTrxs;
  //console.log("No. of Relevant Fin Trx : " + bizTrxSummaryRec.finTrxs.length + " trxID : " + bizTrxSummaryRec.businessTransaction.ID);

  for(let j=0; j < bizTrxSummaryRec.finTrxs.length; j++)
  {
    //console.log("finTrxsForBizTrxID[j].trxType: " + bizTrxSummaryRec.finTrxs[j].trxType); 
    
    if((bizTrxSummaryRec.finTrxs[j].trxType === myConstants.trxType.DUE_DEBIT) || (bizTrxSummaryRec.finTrxs[j].trxType === myConstants.trxType.DUE_CREDIT))
    {
      bizTrxSummaryRec.trxLocalAmount = (bizTrxSummaryRec.finTrxs[j].trxLocalValue);        
      bizTrxSummaryRec.trxForiegnAmount = (bizTrxSummaryRec.finTrxs[j].trxForiegnValue); //Math.round(bizTrxSummaryRec.finTrxs[j].trxLocalValue/bizTrxSummaryRec.finTrxs[j].trxCurrencyRate);        
    }
    else if((bizTrxSummaryRec.finTrxs[j].trxType === myConstants.trxType.OUT_DEBIT))  
    {
      //if(partyCategory === myConstants.partyCategory.BUYER)
      if(bizTransaction.trxType === myConstants.trxType.SELL)
      {
        bizTrxSummaryRec.amtReceviedOrPaidinForiegn -= (bizTrxSummaryRec.finTrxs[j].trxForiegnValue);
        bizTrxSummaryRec.interestRealizedinForiegn -= (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinForiegn);

        bizTrxSummaryRec.amtReceviedOrPaidinLocal -= (bizTrxSummaryRec.finTrxs[j].trxLocalValue);
        bizTrxSummaryRec.interestRealizedinLocal -= (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinLocal);
      }
      //else if((partyCategory === myConstants.partyCategory.SUPPLIER))
      else if(bizTransaction.trxType === myConstants.trxType.BUY)
      {
        bizTrxSummaryRec.amtReceviedOrPaidinForiegn += (bizTrxSummaryRec.finTrxs[j].trxForiegnValue);
        bizTrxSummaryRec.interestRealizedinForiegn += (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinForiegn);

        bizTrxSummaryRec.amtReceviedOrPaidinLocal += (bizTrxSummaryRec.finTrxs[j].trxLocalValue);
        bizTrxSummaryRec.interestRealizedinLocal += (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinLocal);
        
      }
    }
    else if((bizTrxSummaryRec.finTrxs[j].trxType === myConstants.trxType.IN_CREDIT))  
    {
      //if(partyCategory === myConstants.partyCategory.BUYER)
      if(bizTransaction.trxType === myConstants.trxType.SELL)
      {
        bizTrxSummaryRec.amtReceviedOrPaidinForiegn += (bizTrxSummaryRec.finTrxs[j].trxForiegnValue);
        bizTrxSummaryRec.interestRealizedinForiegn += (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinForiegn);

        bizTrxSummaryRec.amtReceviedOrPaidinLocal += (bizTrxSummaryRec.finTrxs[j].trxLocalValue);
        bizTrxSummaryRec.interestRealizedinLocal += (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinLocal);

      }
      //else if((partyCategory === myConstants.partyCategory.SUPPLIER))
      else if(bizTransaction.trxType === myConstants.trxType.BUY)
      {
        bizTrxSummaryRec.amtReceviedOrPaidinForiegn -= (bizTrxSummaryRec.finTrxs[j].trxForiegnValue);
        bizTrxSummaryRec.interestRealizedinForiegn -= (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinForiegn);

        bizTrxSummaryRec.amtReceviedOrPaidinLocal -= (bizTrxSummaryRec.finTrxs[j].trxLocalValue);
        bizTrxSummaryRec.interestRealizedinLocal -= (bizTrxSummaryRec.finTrxs[j].trxInterestRealizedinLocal);

      }           
    }// end of if on trxType
    if(bizTrxSummaryRec.businessTransaction.trxCurrency === myConstants.foreignCurrency)
    {
      bizTrxSummaryRec.amtPendinginForiegn = (bizTrxSummaryRec.finTrxs[j].trxClosingBalance);
      bizTrxSummaryRec.amtPendinginLocal = Math.round(bizTrxSummaryRec.finTrxs[j].trxClosingBalance*currentExchangeRate); //*bizTrxSummaryRec.finTrxs[j].trxCurrencyRate
    }
    else
    {
      bizTrxSummaryRec.amtPendinginForiegn = round(bizTrxSummaryRec.finTrxs[j].trxClosingBalance/currentExchangeRate,2);
      bizTrxSummaryRec.amtPendinginLocal = Math.round(bizTrxSummaryRec.finTrxs[j].trxClosingBalance);
    }
  }//end of for loop on finTrxs
  
  let today:Date = new Date();
  let dueDate: Date = (new Date(bizTransaction.trxDueDate));
  let duration: number = today.valueOf() - dueDate.valueOf();
  let diffDays = Math.floor(duration / (1000 * 3600 * 24)); 
  //console.log("Calculating interest on today: " + today.toISOString() + " -> " + today.valueOf());
  //console.log("DueDate: " + bizTransaction.trxDueDate + " -> " + dueDate.valueOf());
  //console.log("Diff: duration " + duration + " Days: " + diffDays);
  bizTrxSummaryRec.diffDays = diffDays;
  if(diffDays > 0)
  {
    bizTrxSummaryRec.interestUnRealizedinForiegn = round(((bizTrxSummaryRec.amtPendinginForiegn*bizTransaction.trxROIForLatePayment*diffDays*12.0)/36500.0),2);
    bizTrxSummaryRec.interestUnRealizedinLocal = round(((bizTrxSummaryRec.amtPendinginLocal*bizTransaction.trxROIForLatePayment*diffDays*12.0)/36500.0),0);
  }
  else 
  {
    bizTrxSummaryRec.interestUnRealizedinForiegn = round(((bizTrxSummaryRec.amtPendinginForiegn*bizTransaction.trxROIForEarlyPayment*diffDays*12.0)/36500.0),2);
    bizTrxSummaryRec.interestUnRealizedinLocal = round(((bizTrxSummaryRec.amtPendinginLocal*bizTransaction.trxROIForEarlyPayment*diffDays*12.0)/36500.0),0);
  }
  return bizTrxSummaryRec;
}

export function calculateBusinessSummary(bizSummary:BusinessSummary,currentExchangeRate:number,iConfidentialityLevel:number)
{
  
  if((bizSummary.bizTrxs.length <= 0) || (bizSummary.finTrxs.length <= 0))
  {//BusinessTransaction details not available
      bizSummary.businessCurrency="";//TBD            
      bizSummary.businessAmtTotalinForiegn=0;
      bizSummary.amtReceviedOrPaidinForiegn=0;
      bizSummary.amtPendinginForiegn=0;
      bizSummary.interestRealizedinForiegn=0;
      bizSummary.interestUnRealizedinForiegn=0;  
      bizSummary.businessAmtTotalinLocal=0; 
      bizSummary.amtReceviedOrPaidinLocal=0;
      bizSummary.amtPendinginLocal=0;
      bizSummary.interestRealizedinLocal=0;
      bizSummary.interestUnRealizedinLocal=0; 

      bizSummary.bizTrxSummary = [];
      return;
  }
  console.log("calculating bizSummary");
  let today: Date = new Date();
  bizSummary.businessCurrency="";//TBD            
  bizSummary.businessAmtTotalinForiegn=0;
  bizSummary.businessAmtTotalinLocal=0; 
  bizSummary.amtReceviedOrPaidinForiegn=0;
  bizSummary.amtReceviedOrPaidinLocal=0;
  bizSummary.amtPendinginForiegn=0;
  bizSummary.amtPendinginLocal=0;
  bizSummary.interestRealizedinForiegn=0;
  bizSummary.interestRealizedinLocal=0;
  bizSummary.interestUnRealizedinForiegn=0;  
  bizSummary.interestUnRealizedinLocal=0; 

  bizSummary.bizTrxSummary = [];
  // p.bizTrxs = firebaseProvider.getBusinessTransactionsForPartyID(p.profile.ID).valueChanges();

  //console.log("No of recs in bizTrxSummary Before push:" + bizSummary.bizTrxSummary.length);
  let relevantFinTrxs:Array<FinanceTransaction> = [];
  for(let i=0;(i < bizSummary.bizTrxs.length);i++)
  {
    //console.log("Handing " + i + " this.bizTrxs");
    if(bizSummary.bizTrxs[i].confidentialityLevel > iConfidentialityLevel)
    {
      continue;
    }
    let bizTrxSummaryRec = {} as BusinessTransactionSummary;
    relevantFinTrxs = bizSummary.finTrxs.filter(financeTransaction => financeTransaction.BizTrxID == bizSummary.bizTrxs[i].ID);
    bizTrxSummaryRec.businessTransaction = bizSummary.bizTrxs[i];
    bizTrxSummaryRec.finTrxs = relevantFinTrxs;

    bizTrxSummaryRec = getBusinessTransactionSummary(bizTrxSummaryRec.businessTransaction,bizTrxSummaryRec.finTrxs,currentExchangeRate);

    bizSummary.businessAmtTotalinForiegn +=  bizTrxSummaryRec.trxForiegnAmount;
    bizSummary.businessAmtTotalinLocal +=  bizTrxSummaryRec.trxLocalAmount;

    bizSummary.amtReceviedOrPaidinForiegn  +=  bizTrxSummaryRec.amtReceviedOrPaidinForiegn;
    bizSummary.amtReceviedOrPaidinLocal  +=  bizTrxSummaryRec.amtReceviedOrPaidinLocal;

    bizSummary.amtPendinginForiegn +=  bizTrxSummaryRec.amtPendinginForiegn;
    bizSummary.amtPendinginLocal +=  bizTrxSummaryRec.amtPendinginLocal;

    bizSummary.interestRealizedinForiegn +=  bizTrxSummaryRec.interestRealizedinForiegn;
    bizSummary.interestRealizedinLocal +=  bizTrxSummaryRec.interestRealizedinLocal;

    bizSummary.interestUnRealizedinForiegn +=  bizTrxSummaryRec.interestUnRealizedinForiegn;
    bizSummary.interestUnRealizedinLocal +=  bizTrxSummaryRec.interestUnRealizedinLocal;
    
    bizSummary.bizTrxSummary.push(bizTrxSummaryRec); 
  }//end of for loop over p.bizTrxs
}
