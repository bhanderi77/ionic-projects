export interface StateRegion{
    id:string;
    creation_time:string;
    name: string;
    TIN: number;
    code: string;
  }

export interface FinanceSettings {
    creation_time:string;
    foreignCurrency: string;
    localCurrency: string;
    exchangeRate: number;
    dueDays: number;
    cGSTRate: number;
    sGSTRate: number;
    iGSTRate: number;
}