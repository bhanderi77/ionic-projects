import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { InventoryProvider } from '../../providers/inventory/inventory';
import { TransactionProvider } from '../../providers/transaction/transaction';
import { SettingsProvider } from '../../providers/settings/settings';
import { AuthProvider } from '../../providers/auth/auth';
import { User, checkIfValid } from '../../model/user';


import { RateCard,InventoryParams } from '../../model/rateCard';
import { myConstants, round } from '../../model/myConstants';
import { ItemType,ItemShape,ItemSieve,ItemQualityLevel } from '../../model/packagedItem';
import { BusinessTransaction } from '../../model/businessTransaction';
import { FinanceTransaction } from '../../model/financeTransaction';
import { BusinessTransactionSummary,getBusinessTransactionSummary } from '../../model/partySummary';
import { PackagedItem } from '../../model/packagedItem';
import { MarketPrice } from '../../model/marketPrice';

import { Subscription,Observable,Observer,BehaviorSubject  } from 'rxjs';
//import { of } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

/*
  Generated class for the RateCardProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RateCardProvider {
  private user:User;  
  //public currency:string;
  public currencyExchangeRate:number;//This is required to calculate business trx summary
  private rateCardList:Array<RateCard>;
  public rateCardList$:BehaviorSubject<RateCard[]>;
  
  private purchaseTrxList:Array<BusinessTransaction>;
  private finTrxList:Array<FinanceTransaction>;
  private trxAvgPurchaseCost:number;
  private trxAvgInterestCost:number;
  
  public packagedItemList:Array<PackagedItem>;
  private itemPriceList:Array<MarketPrice>;

  private inventoryParams:InventoryParams;
  public itemTypeList:Array<ItemType>;
  public itemShapeList:Array<ItemShape>;
  public itemSieveList:Array<ItemSieve>;
  public itemQualityLevelList:Array<ItemQualityLevel>;

  private seriesList:Array<string>;
  public seriesID:string;
  public seriesName:string;
  public trxPartyName:string;
  public trxDate:string;
  public trxWeight:number;
  public trxRate:number;
  public trxCurrency:string;

  private mySubscriptions:Subscription;

  constructor(private authProvider: AuthProvider,
    private inventoryProvider: InventoryProvider,
    private transactionProvider: TransactionProvider,
    private settingsProvider: SettingsProvider) 
  {
    console.log('Hello RateCardProvider::constructor()');
    this.rateCardList = [];
    this.purchaseTrxList = [];
    this.finTrxList = [];

    this.trxAvgPurchaseCost = 0;
    this.trxAvgInterestCost = 0;

    this.itemTypeList = [];
    this.itemShapeList = [];
    this.itemSieveList = [];
  //  this.itemPolishSieveList = [];
  //  this.itemRoughSieveList = [];
    this.itemQualityLevelList = [];
    //this.itemPolishQualityLevelList = [];
    //this.itemRoughQualityLevelList = [];

    this.packagedItemList = [];
    this.itemPriceList = [];

    this.inventoryParams = {} as InventoryParams;
    this.inventoryParams.avgP2SGain = 0.0;
    this.inventoryParams.avgMfgGain = 0.0;
    
    this.inventoryParams.purchaseCostPerSaleableUnit = 0.0;
    this.inventoryParams.interestCostPerSaleableUnit = 0.0;

    this.inventoryParams.totalMarketValueForSaleable = 0.0;
    this.inventoryParams.totalSaleablePolishedWeight = 0.0;
    this.inventoryParams.totalSaleableRoughWeight = 0.0;
    this.inventoryParams.totalSaleableTopsWeight = 0.0;
    this.inventoryParams.totalSaleableWeight = 0.0;
    this.inventoryParams.totalPurchaseWeight = 0.0;
    this.inventoryParams.avgMarketRateForSaleable = 0.0; 
    this.inventoryParams.avgMfgCostForPolished = 0.0;
    this.inventoryParams.avgMfgCostForRough = 0.0;
    this.inventoryParams.avgMfgCostForTops = 0.0;
    
    this.seriesID = "";
    this.seriesName = "";
    this.trxPartyName = "";
    this.trxDate = "";
    this.trxWeight = 0;
    this.trxRate = 0;
    this.trxCurrency = "";    
    this.seriesList = [];

    this.rateCardList$ = new BehaviorSubject<RateCard[]>([]);
   // this.currency = myConstants.localCurrency;
    this.initializeData();
  }  
  
  async initializeData()
  {
    console.log("Welcome to RateCardProvider:initializeData()");
    this.mySubscriptions = new Subscription();
    
    this.user = await this.authProvider.getUser();    
  
    this.currencyExchangeRate = 1;
    this.currencyExchangeRate = await this.settingsProvider.getExchangeRate(this.user);
    
    this.itemTypeList = [];
    this.settingsProvider.getItemTypeList(this.user,true)
    .then(pList => {
      this.itemTypeList = pList;
      this.buildRateCardList();
    });

    this.itemShapeList = [];
    //this.polishShapeList = await this.settingsProvider.getItemShapeList(false);
    this.settingsProvider.getItemShapeList(this.user,true)
    .then(pList => {
      this.itemShapeList = pList;
      this.buildRateCardList();
    });
    
    this.itemSieveList = [];
    
    this.settingsProvider.getItemSieveList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemSieveList = pList;
        this.buildRateCardList();
      }      
    });
    
    this.itemQualityLevelList = [];
    
    this.settingsProvider.getItemQualityLevelList(this.user,myConstants.itemType.ALL,true)
    .then(pList => {
      if(pList.length > 0)
      {
        this.itemQualityLevelList = pList;  
        this.buildRateCardList();
      }      
    });
    
    this.itemPriceList = [];
    this.mySubscriptions.add(this.settingsProvider.getMarketPrice$(this.user).valueChanges()
    .subscribe(priceList => {
      console.log("received latest priceList");
      this.itemPriceList = priceList;
      this.buildRateCardList();
    }));

    this.packagedItemList = [];
    this.mySubscriptions.add(this.inventoryProvider.get_packagedItems_For_B(this.user,myConstants.taskCode.SALES).valueChanges().subscribe(pItemList => {
      console.log("received latest packagedItemList");
      this.packagedItemList = [];
      this.packagedItemList = pItemList;      
      this.calculateInventoryParameters();
   //   this.buildRateCardList();
    }));   

    this.mySubscriptions.add(this.transactionProvider.getAllBusinessTransactions(this.user,myConstants.trxType.BUY).valueChanges()
    .subscribe(businessTransactionList => {
      console.log("Received latest businessTransactionList: " + businessTransactionList.length);
      this.purchaseTrxList = [];
      this.purchaseTrxList = businessTransactionList;//.filter(bizTrx => (bizTrx.trxType === myConstants.trxType.ROUGH_BUY) || (bizTrx.trxType === myConstants.trxType.ROUGH_IMPORT) || (bizTrx.trxType === myConstants.trxType.POLISH_BUY));
      this.calculatePurchaseCostAndInterest();
//      this.buildRateCardList();
    }));

    this.mySubscriptions.add(this.transactionProvider.getAllFinanceTransactions(this.user).valueChanges()
    .subscribe(financeTransactionList => {
      console.log("Received latest financeTransactionList: " + financeTransactionList.length);
      this.finTrxList = [];
      this.finTrxList = financeTransactionList;
      this.calculatePurchaseCostAndInterest();    
  //    this.buildRateCardList();
    }));
  }

  setSeries(iSeriesID:string,iSeriesName:string,iTrxPartyName:string,iTrxDate:string,
                      iTrxWeight:number,iTrxRate:number,iTrxCurrency:string)
  {
      this.seriesID = iSeriesID;
      this.seriesName = iSeriesName;
      this.trxPartyName = iTrxPartyName;
      this.trxDate = iTrxDate;
      this.trxWeight = iTrxWeight;
      this.trxRate = iTrxRate;
      this.trxCurrency = iTrxCurrency;
      this.calculatePurchaseCostAndInterest();
      this.calculateInventoryParameters();
      
      this.buildRateCardList();        
  }

  searchSeries(searchItem)
  {
    console.log("Welcome to RateCardProvider:searchSeries()" + searchItem);
    
    this.seriesID = "";
    this.seriesName = "";
    this.trxPartyName = "";
    this.trxDate = "";
    this.trxWeight = 0;
    this.trxRate = 0;
    this.trxCurrency = "";   
 
    let weightAndRateAndCurrency:string="";
    let rateAndCurrency:string="";
    let searchParams:Array<string> = [];
    searchParams = searchItem.split(",",4);
    if(searchParams.length === 5)
    {
      this.seriesID = searchParams[0];
      this.seriesName = searchParams[1];
      this.trxPartyName = searchParams[2];
      this.trxDate = searchParams[3];
      weightAndRateAndCurrency = searchParams[4];
      
      searchParams = weightAndRateAndCurrency.split("ct X ",2);
      this.trxWeight = +searchParams[0];
      rateAndCurrency = searchParams[1];//RateCurrency i.e. 530.12USD or 3124INR
      
      this.trxRate = +rateAndCurrency.slice(0,this.trxCurrency.length-3);
      this.trxCurrency = rateAndCurrency.slice(this.trxCurrency.length-3);//Get INR from 3134INR
    }
    else if(searchParams.length === 4)
    {
      this.seriesID = searchParams[0];
      this.trxPartyName = searchParams[1];
      this.trxDate = searchParams[2];
      weightAndRateAndCurrency = searchParams[3];
      
      searchParams = weightAndRateAndCurrency.split("ct X ",2);
      this.trxWeight = +searchParams[0];
      rateAndCurrency = searchParams[1];//RateCurrency i.e. 530.12USD or 3124INR
      
      this.trxRate = +rateAndCurrency.slice(0,this.trxCurrency.length-3);
      this.trxCurrency = rateAndCurrency.slice(this.trxCurrency.length-3);//Get INR from 3134INR
    }
    this.calculatePurchaseCostAndInterest();
    this.calculateInventoryParameters();    
    this.buildRateCardList();        
  }

  resetSeries()
  {
      this.seriesID = "";
      this.seriesName = "";
      this.trxPartyName = "";
      this.trxDate = "";
      this.trxWeight = 0;
      this.trxRate = 0;
      this.trxCurrency = "";   
      this.calculatePurchaseCostAndInterest();
      this.calculateInventoryParameters();
      
      this.buildRateCardList();        
  }

  
  calculatePurchaseCostAndInterest():void
  {
    console.log("Welcome to RateCardProvider::calculatePurchaseCostAndInterest() ");
    if((this.finTrxList.length > 0) && (this.purchaseTrxList.length > 0))
    {
      let finTrxList:Array<FinanceTransaction> = [];
      let bizTrxList:Array<BusinessTransaction> = [];

      if((this.seriesID) && (this.trxPartyName) && (this.trxDate) && (this.trxWeight) && (this.trxRate) && (this.trxCurrency))
      {//calculate average as per purchase trx of seried selected
        let index = 0
        for(index = 0;index<this.packagedItemList.length;index++)
        {
            if((this.packagedItemList[index].packageGroupID === this.seriesID)
            && (this.packagedItemList[index].trxPartyName === this.trxPartyName)
            && (this.packagedItemList[index].trxDate === this.trxDate)
            && (this.packagedItemList[index].trxWeight === this.trxWeight)
            && (this.packagedItemList[index].trxRate === this.trxRate)
            && (this.packagedItemList[index].trxCurrency === this.trxCurrency))
            {
                break;
            }
        }
        if(index < this.packagedItemList.length)
        {
            bizTrxList = this.purchaseTrxList.filter(bizTrx => (bizTrx.ID === this.packagedItemList[index].trxID));
        }
      }
      else
      {//calculate average as per ALL purchase trx.
          bizTrxList = this.purchaseTrxList;          
      } 
      let bizTrxSummary = {} as BusinessTransactionSummary;
      let totalPurchaseWeight = 0;
      this.trxAvgPurchaseCost = 0;
      this.trxAvgInterestCost = 0;
      
      for(let i=0;i<bizTrxList.length;i++)
      {
          totalPurchaseWeight += +bizTrxList[i].trxWeight;
          
          finTrxList = this.finTrxList.filter(finTrx => finTrx.BizTrxID === bizTrxList[i].ID);
          //partyList = supplierList.filter(p => p.id === bizTrxList[i].trxPartyID);
          if((bizTrxList[i]) && (/*partyList.length === 1*/true) && (finTrxList.length>0))
          {
              bizTrxSummary = getBusinessTransactionSummary(bizTrxList[i],finTrxList,this.currencyExchangeRate);
              if(bizTrxSummary)
              {
                  /*
                  console.log("bizTrxSummary.amtReceviedOrPaidinLocal " + bizTrxSummary.amtReceviedOrPaidinLocal);
                  console.log("bizTrxSummary.interestRealizedinLocal " + bizTrxSummary.interestRealizedinLocal);
                  console.log("bizTrxSummary.amtPendinginLocal " + bizTrxSummary.amtPendinginLocal);
                  console.log("bizTrxSummary.interestUnRealizedinLocal " + bizTrxSummary.interestUnRealizedinLocal);
                  */
                  this.trxAvgPurchaseCost += round((+bizTrxSummary.amtReceviedOrPaidinLocal + +bizTrxSummary.amtPendinginLocal),10);
                  this.trxAvgInterestCost += round((+bizTrxSummary.interestRealizedinLocal + +bizTrxSummary.interestUnRealizedinLocal),10);
              }
          }//end of if((this.purchaseTrxList[i]) && (partyList.length > 0) && (finTrxList.length>0))
      }//end of for(let i=0;i<this.purchaseTrxList.length;i++)
      if(totalPurchaseWeight)
      {
          //console.log(this.trxAvgPurchaseCost + " / " + totalPurchaseWeight);
          this.trxAvgPurchaseCost = round((this.trxAvgPurchaseCost/totalPurchaseWeight),0);
          this.trxAvgInterestCost = round((this.trxAvgInterestCost/totalPurchaseWeight),0);
          this.calculateCostPerSaleableUnit();
      }            
    }
  }

  calculateInventoryParameters():void
  {
    console.log("Welcome to RateCardProvider:calculateInventoryParameters() ");
    
    this.inventoryParams.avgP2SGain = 0.0;
    this.inventoryParams.avgMfgGain = 0.0;
    
    this.inventoryParams.totalMarketValueForSaleable = 0.0;
    this.inventoryParams.totalSaleablePolishedWeight = 0.0;
    this.inventoryParams.totalSaleableRoughWeight = 0.0;
    this.inventoryParams.totalSaleableTopsWeight = 0.0;
    this.inventoryParams.totalSaleableWeight = 0.0;
    this.inventoryParams.totalPurchaseWeight = 0.0;
    this.inventoryParams.avgMarketRateForSaleable = 0.0; 
    this.inventoryParams.avgMfgCostForPolished = 0.0;
    this.inventoryParams.avgMfgCostForRough = 0.0;
    this.inventoryParams.avgMfgCostForTops = 0.0;       
    
    let totalMfgCost:number=0;
    let marketRate:number = 0;
    for(let i=0;i<this.packagedItemList.length;i++)
    {
      if((this.seriesID) && (this.trxPartyName) && (this.trxDate) && (this.trxWeight) && (this.trxRate) && (this.trxCurrency))
      {
        console.log("Calculating Gain for Series");
        if((this.packagedItemList[i].packageGroupID === this.seriesID)
            && (this.packagedItemList[i].trxPartyName === this.trxPartyName)
            && (this.packagedItemList[i].trxDate === this.trxDate)
            && (this.packagedItemList[i].trxWeight === this.trxWeight)
            && (this.packagedItemList[i].trxRate === this.trxRate)
            && (this.packagedItemList[i].trxCurrency === this.trxCurrency))
        {
          if(this.packagedItemList[i].itemType.id === myConstants.itemType.POLISHED)
          {
            this.inventoryParams.totalSaleablePolishedWeight += +this.packagedItemList[i].itemWeight;
            totalMfgCost += +this.packagedItemList[i].accumulatedTaskCost;
          }
          else if(this.packagedItemList[i].itemType.id === myConstants.itemType.TOPS)
          {
            this.inventoryParams.totalSaleableTopsWeight += +this.packagedItemList[i].itemWeight;
          }
          else if(this.packagedItemList[i].itemType.id === myConstants.itemType.ROUGH)
          {
            this.inventoryParams.totalSaleableRoughWeight += +this.packagedItemList[i].itemWeight;
          }
          this.inventoryParams.totalSaleableWeight += +this.packagedItemList[i].itemWeight;
          this.inventoryParams.totalPurchaseWeight += +this.packagedItemList[i].roughWeight;
          marketRate = this.getMarketRateForTSSQ(this.packagedItemList[i].itemType.id,this.packagedItemList[i].itemShape.id,this.packagedItemList[i].itemSieve.id,this.packagedItemList[i].itemQualityLevel.id);
          this.inventoryParams.totalMarketValueForSaleable += round(marketRate*this.packagedItemList[i].itemWeight,0);
        }
      }
      else
      {
        if(this.packagedItemList[i].itemType.id === myConstants.itemType.POLISHED)
        {
          this.inventoryParams.totalSaleablePolishedWeight += +this.packagedItemList[i].itemWeight;
          totalMfgCost += +this.packagedItemList[i].accumulatedTaskCost;
        }
        else if(this.packagedItemList[i].itemType.id === myConstants.itemType.TOPS)
        {
          this.inventoryParams.totalSaleableTopsWeight += +this.packagedItemList[i].itemWeight;
        }
        else if(this.packagedItemList[i].itemType.id === myConstants.itemType.ROUGH)
        {
          this.inventoryParams.totalSaleableRoughWeight += +this.packagedItemList[i].itemWeight;
        }
        this.inventoryParams.totalSaleableWeight += +this.packagedItemList[i].itemWeight;
        this.inventoryParams.totalPurchaseWeight += +this.packagedItemList[i].roughWeight;      
        marketRate = this.getMarketRateForTSSQ(this.packagedItemList[i].itemType.id,this.packagedItemList[i].itemShape.id,this.packagedItemList[i].itemSieve.id,this.packagedItemList[i].itemQualityLevel.id);
        this.inventoryParams.totalMarketValueForSaleable += round(marketRate*this.packagedItemList[i].itemWeight,0);
      }
    }
    
    if(this.inventoryParams.totalPurchaseWeight > 0)
    {
      //this.avgMfgCostForSaleable = round((this.avgMfgCostForSaleable/totalPolishedWeight),10);
      this.inventoryParams.avgP2SGain = round((this.inventoryParams.totalSaleableWeight/this.inventoryParams.totalPurchaseWeight)*100,2);
      
      let totalMfgInput:number = +this.inventoryParams.totalPurchaseWeight - +this.inventoryParams.totalSaleableRoughWeight;
      let totalMfgOutput:number = +this.inventoryParams.totalSaleablePolishedWeight + +this.inventoryParams.totalSaleableTopsWeight;
      /*
      console.log("totalPurchaseWeight " + this.inventoryParams.totalPurchaseWeight);
      console.log("totalSaleableRoughWeight " + this.inventoryParams.totalSaleableRoughWeight);
      console.log("totalMfgInput " + totalMfgInput);
      console.log("totalMfgOutput " + totalMfgOutput);
      console.log("totalSaleablePolishedWeight " + this.inventoryParams.totalSaleablePolishedWeight);
      console.log("totalMfgCost " + totalMfgCost);
      */
      if(totalMfgInput)
      {
        this.inventoryParams.avgMfgGain = round((totalMfgOutput/totalMfgInput)*100,2);
      }

      if(this.inventoryParams.totalSaleableWeight)
      {
        this.inventoryParams.avgMfgCostForPolished = round(totalMfgCost/this.inventoryParams.totalSaleablePolishedWeight,0);
        this.inventoryParams.avgMfgCostForRough = 0;
        this.inventoryParams.avgMfgCostForTops = 0;
        this.inventoryParams.avgMarketRateForSaleable = round(this.inventoryParams.totalMarketValueForSaleable/this.inventoryParams.totalSaleableWeight,0);
        /*
        console.log("totalMarketValueForSaleable " + this.inventoryParams.totalMarketValueForSaleable);
        console.log("totalSaleableWeight " + this.inventoryParams.totalSaleableWeight);
        console.log("avgMarketRateForSaleable " + this.inventoryParams.avgMarketRateForSaleable);
        */
      }
    }    
    //console.log("this.avgP2SGain " + this.inventoryParams.avgP2SGain);
    this.calculateCostPerSaleableUnit();
  }

  private calculateCostPerSaleableUnit():void
  {
    this.inventoryParams.purchaseCostPerSaleableUnit = 0.0;
    this.inventoryParams.interestCostPerSaleableUnit = 0.0;
    
    if((this.inventoryParams.avgP2SGain > 0.0) && (this.trxAvgPurchaseCost > 0.0))
    {
      this.inventoryParams.purchaseCostPerSaleableUnit = round((this.trxAvgPurchaseCost/this.inventoryParams.avgP2SGain)*100,0);
      this.inventoryParams.interestCostPerSaleableUnit = round((this.trxAvgInterestCost/this.inventoryParams.avgP2SGain)*100,0); 
    }
    
    console.log("this.trxAvgPurchaseCost " + this.trxAvgPurchaseCost);
    console.log("avgP2SGain " + this.inventoryParams.avgP2SGain);
    console.log("purchaseCostPerSaleableUnit " + this.inventoryParams.purchaseCostPerSaleableUnit);

    this.buildRateCardList();
  }

  buildRateCardList()//:Observable<RateCard[]>
  {
    console.log("Welcome RateCardProvider:buildRateCardList() ");
    
    if((!this.itemTypeList.length) || (!this.itemShapeList.length) || (!this.itemSieveList.length) 
      || (!this.itemQualityLevelList.length)
      || (!this.inventoryParams.purchaseCostPerSaleableUnit)
      || (!this.inventoryParams.avgMarketRateForSaleable))
    {
        this.rateCardList = [];  
        this.rateCardList$.next(this.rateCardList);      
        return; 
    }
    console.log("building..");
    console.log("TSSQ " + this.itemTypeList.length + " " + this.itemShapeList.length + " " + this.itemSieveList.length + " " + this.itemQualityLevelList.length);
    console.log("purchaseCostPerSaleableUnit " + this.inventoryParams.purchaseCostPerSaleableUnit);
    console.log("avgMarketRateForSaleable " + this.inventoryParams.avgMarketRateForSaleable);
    this.rateCardList = [];
    let pItemList:Array<PackagedItem> = [];
    for(let a=0;a<this.itemTypeList.length;a++)
    {
      for(let i=0;i<this.itemShapeList.length;i++)
      {
        for(let j=0;j<this.itemSieveList.length-1;j++)
        {
          for(let k=0;k<this.itemQualityLevelList.length;k++)
          {
            if((this.itemSieveList[j].itemTypeID === this.itemQualityLevelList[k].itemTypeID)
            && (this.itemSieveList[j].itemTypeID === this.itemTypeList[a].id))
            {
              pItemList = this.packagedItemList.filter(pItem => ((pItem.itemType.id === this.itemQualityLevelList[k].itemTypeID)
                                                              && (pItem.itemShape.id === this.itemShapeList[i].id)
                                                              && (pItem.itemSieve.id === this.itemSieveList[j].id)
                                                              && (pItem.itemQualityLevel.id === this.itemQualityLevelList[k].id)));
              if(pItemList.length > 0)
              {
                let rateCard = {} as RateCard;

                rateCard.itemType = this.itemTypeList[a];//this.itemQualityLevelList[k].itemTypeID;
                
                rateCard.itemShape = this.itemShapeList[i];
                rateCard.itemSieve = this.itemSieveList[j];
                rateCard.itemQualityLevel = this.itemQualityLevelList[k];
                
                rateCard.avgMarketRate = +this.getMarketRateForTSSQ(rateCard.itemType.id,rateCard.itemShape.id,rateCard.itemSieve.id,rateCard.itemQualityLevel.id);

                if((this.inventoryParams.avgMarketRateForSaleable))
                {
                  rateCard.valueFactor = round(rateCard.avgMarketRate/this.inventoryParams.avgMarketRateForSaleable,10);
                }
                else
                {
                  rateCard.valueFactor = 0.0;
                }
                  
                rateCard.avgPurchaseCost = round(this.inventoryParams.purchaseCostPerSaleableUnit*rateCard.valueFactor,0);
                rateCard.avgInterestCost = round(this.inventoryParams.interestCostPerSaleableUnit*rateCard.valueFactor,0)
                rateCard.avgSalesGain = 0;
                if(rateCard.itemType.id === myConstants.itemType.POLISHED)
                {
                  rateCard.avgMfgCost = round(+this.inventoryParams.avgMfgCostForPolished,0);
                }
                else
                {
                  rateCard.avgMfgCost = round(+this.inventoryParams.avgMfgCostForRough,0);
                }
                  
                rateCard.avgGrossCost = round((rateCard.avgPurchaseCost + rateCard.avgInterestCost + rateCard.avgMfgCost),0);
                console.log(rateCard.itemType.name + " " + rateCard.itemShape.name + " " + rateCard.itemSieve.id + " " + rateCard.itemQualityLevel.id);
                console.log(rateCard.avgMarketRate + " " + rateCard.avgPurchaseCost + " " + rateCard.avgInterestCost + " " + rateCard.avgMfgCost);

                this.rateCardList.push(rateCard);  
              }                        
            }//end of if(this.itemSieveList[j].itemType === this.itemQualityLevelList[k].itemType)
          }//end of loop on itemQualityLevelList
        }//end of loop on itemSieveList
      }//end of loop on itemShapeList
    }//end of loop on itemTypeList
    
    // this.getRateCardList();
    this.rateCardList$.next(this.rateCardList); 
  }

  getRateCardList():BehaviorSubject<RateCard[]>
  {
    //console.log("this.rateCardListObservable$.next(this.rateCardList)");      
    return this.rateCardList$; 
  }
    
  private getMarketRateForTSSQ(iType:string,shapeID:string,sieveID:string,qualityLevelID:string):number
  {
    if(this.itemPriceList.length > 0)
    {
      for(let i=0;i<this.itemPriceList.length;i++)
      {
        if( (this.itemPriceList[i].itemTypeID === iType) 
        && (this.itemPriceList[i].shapeID === shapeID)
        && (this.itemPriceList[i].sieveID === sieveID)
        && (this.itemPriceList[i].qualityLevelID === qualityLevelID))
        {
          return this.itemPriceList[i].rate;
        }
      }
    }
    else
    {
    return 0;
    }
  }
      
  public getRateCardByName(iType:string,iShapeName:string,iSieveName:string,iQualityLevelName:string):RateCard
  {
    for(let i=0;i<this.rateCardList.length;i++)
    {
      if((this.rateCardList[i].itemType.name === iType)
      && (this.rateCardList[i].itemShape.name === iShapeName)
      && (this.rateCardList[i].itemSieve.name === iSieveName)
      && (this.rateCardList[i].itemQualityLevel.name === iQualityLevelName))
      {
        return this.rateCardList[i];
      }
    }
    return null;
  }

  getRateCardByID(iType:string,iShapeID:string,iSieveID:string,iQualityLevelID:string):RateCard
  {
    //console.log("Welcome to RateCardProvider::getRateCardByID() " + this.rateCardList.length);
    //console.log(iType + " " + iShapeID + " " + iSieveID + " " + iQualityLevelID);
    
    for(let i=0;i<this.rateCardList.length;i++)
    {
      //console.log(i + " " + this.rateCardList[i].itemType.id + " " + this.rateCardList[i].itemShape.id + " " + this.rateCardList[i].itemSieve.id + " " + this.rateCardList[i].itemQualityLevel.id);
  
      if((this.rateCardList[i].itemType.id === iType)
      && (this.rateCardList[i].itemShape.id === iShapeID)
      && (this.rateCardList[i].itemSieve.id === iSieveID)
      && (this.rateCardList[i].itemQualityLevel.id === iQualityLevelID))
      {
        console.log("returning RateCard " + this.rateCardList[i].avgMarketRate + " " + this.rateCardList[i].avgPurchaseCost);
        return this.rateCardList[i];
      }
    }
    return null;
  }
  
  public getInventoryParams():InventoryParams
  {
      return this.inventoryParams;
  }

  getNextSieve(itemSieve:ItemSieve)
  {
    let i:number = 0;
    for(i=0;i<this.itemSieveList.length;i++)
    {
      if((this.itemSieveList[i].itemTypeID === itemSieve.itemTypeID)
      && (this.itemSieveList[i].id === itemSieve.id))
      {
        break;
      }
    }
    if(i < this.itemSieveList.length-1)
      return this.itemSieveList[i+1]
    else
      return itemSieve;
  }

  getSeriesList():Array<string>
  {
    console.log("Welcome to RateCardProvider:prepareSearchList()");
    this.seriesList = [];
    for(let i = 0;i<this.packagedItemList.length;i++)
    {
      let seriesItem:string = "";
      seriesItem += this.packagedItemList[i].packageGroupID;
      if(this.packagedItemList[i].packageGroupName)
      {
        seriesItem += ","+this.packagedItemList[i].packageGroupName;
      }
      
      if(this.packagedItemList[i].trxPartyName)
      {
        seriesItem += ","+this.packagedItemList[i].trxPartyName;
      }
      if(this.packagedItemList[i].trxDate)
      {
        seriesItem += ","+this.packagedItemList[i].trxDate;
      }
      if((this.packagedItemList[i].trxWeight) && (this.packagedItemList[i].trxRate) && (this.packagedItemList[i].trxCurrency))
      {
        seriesItem += ","+this.packagedItemList[i].trxWeight+"ct X "+this.packagedItemList[i].trxRate+this.packagedItemList[i].trxCurrency;
      }
      if(this.seriesList.filter(s => s === seriesItem).length === 0)
      {//If unique
        this.seriesList.push(seriesItem);
      }
    }
    return this.seriesList;
  }

}
