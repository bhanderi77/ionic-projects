import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
import { QuerySnapshot, DocumentReference, WriteBatch } from '@firebase/firestore-types';

import { Injectable } from '@angular/core';
import { Project } from '../../model/project';
import { Milestone } from '../../model/milestone';
import { Risk } from '../../model/risk';
import { Issue } from '../../model/issue';
import { Action } from '../../model/action';

/*
  Generated class for the FirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FirebaseProvider {

  constructor(private afs:AngularFirestore) {
    console.log('Hello FirebaseProvider Provider');
  }

  getProjectList():AngularFirestoreCollection<Project>
  {
    return this.afs.collection<Project>('/projectList', ref => ref.orderBy('creationt_time')); 
  }

  getMilestoneList(projectID:string):AngularFirestoreCollection<Milestone>
  {
    console.log("Welcome to FirebaseProvider::getMilestoneList(projectID) " + projectID);
    return this.afs.collection<Milestone>(`/projectList/${projectID}/milestoneList`, ref => ref.orderBy('estimatedClosureDate')); 
  }

  
  getRiskListForMilestone(projectID:string,mID:string):AngularFirestoreCollection<Risk>
  {
    console.log("Welcome to FirebaseProvider::getRiskListForMilestone(projectID,mID) " + projectID + " " + mID);
    return this.afs.collection<Risk>(`/projectList/${projectID}/riskList`, ref => ref.where('milestoneID','==', mID).orderBy('estimatedClosureDate')); 
    //return this.afs.collection<Risk>(`/projectList/${projectID}/riskList`, ref => ref.where('milestoneID','==', mID)); 
  }

  getRiskListForProject(projectID:string):AngularFirestoreCollection<Risk>
  {
    console.log("Welcome to FirebaseProvider::getRiskListForProject(projectID) " + projectID);
    return this.afs.collection<Risk>(`/projectList/${projectID}/riskList`, ref => ref.orderBy('estimatedClosureDate')); 
  }

  getActionListForMilestone(projectID:string,mID:string):AngularFirestoreCollection<Action>
  {
    console.log("Welcome to FirebaseProvider::getActionListForMilestone(projectID,mID) " + projectID + " " + mID);
    return this.afs.collection<Action>(`/projectList/${projectID}/actionList`, ref => ref.where('milestoneID','==', mID).orderBy('estimatedClosureDate')); 
  }

  getActionListForProject(projectID:string):AngularFirestoreCollection<Action>
  {
    console.log("Welcome to FirebaseProvider::getRiskListForProject(projectID) " + projectID);
    return this.afs.collection<Action>(`/projectList/${projectID}/actionList`, ref => ref.orderBy('estimatedClosureDate')); 
  }

  getIssueListForMilestone(projectID:string,mID:string):AngularFirestoreCollection<Issue>
  {
    console.log("Welcome to FirebaseProvider::getIssueListForMilestone(projectID,mID) " + projectID + " " + mID);
    return this.afs.collection<Issue>(`/projectList/${projectID}/issueList`, ref => ref.where('milestoneID','==', mID).orderBy('estimatedClosureDate')); 
  }

  getIssueListForProject(projectID:string):AngularFirestoreCollection<Issue>
  {
    console.log("Welcome to FirebaseProvider::getRiskListForProject(projectID) " + projectID);
    return this.afs.collection<Issue>(`/projectList/${projectID}/issueList`, ref => ref.orderBy('estimatedClosureDate')); 
  }

  async createProject(p:Project):Promise<void>
  {
    console.log("Welcome to FirebaseProvider:createProject(p:project)")
    //const ruleID: string = this.afs.createId();
    const current_time:string = new Date().toISOString();
    const projectID: string = "PRJ" + "_" + (current_time);
    const milestoneID : string = "MLS" + "_" + (current_time);
    let batch:WriteBatch = this.afs.firestore.batch();
    let projectRef:DocumentReference = this.afs.doc(`/projectList/${projectID}`).ref;
    
    if(projectRef)
    {
      await batch.set(projectRef,{
        id:projectID,
        server_time:firebase.firestore.FieldValue.serverTimestamp(),
        creationt_time:current_time,    
        description: p.description,
        startDate:p.startDate,
        endDate:p.endDate                   
      });
    }

    let milestoneRef:DocumentReference = this.afs.doc(`/projectList/${projectID}/milestoneList/${milestoneID}`).ref;
    if(milestoneRef)
    {
      await batch.set(milestoneRef,{
        id:milestoneID,
        server_time:firebase.firestore.FieldValue.serverTimestamp(),
        creationt_time:current_time,    
        description: "Project Completion",
        targetClosureDate:p.endDate,
        estimatedClosureDate:p.endDate,
        actualClosureDate:"",
        completionPercentage:0,
        RAG:"GREEN",
        remarks:"Last Milestone of project"                
      });
    }
    await batch.commit();
  }

  async createMilestone(projectID:string,m:Milestone):Promise<void>
  {
    const current_time:string = new Date().toISOString();
    const milestoneID : string = "MLS" + "_" + (current_time);
    
    let milestoneRef:DocumentReference = this.afs.doc(`/projectList/${projectID}/milestoneList/${milestoneID}`).ref;
    if(milestoneRef)
    {
      return milestoneRef.set({
        id:milestoneID,
        server_time:firebase.firestore.FieldValue.serverTimestamp(),
        creationt_time:current_time,    
        description: m.description,
        targetClosureDate:m.targetClosureDate,
        estimatedClosureDate:m.estimatedClosureDate,
        actualClosureDate:m.actualClosureDate,
        completionPercentage:m.completionPercentage,
        RAG:m.RAG,
        remarks:m.remarks
      });     
    }
    return null;
  }

  async createRisk(pID:string,mID:string,r:Risk):Promise<void>
  {
    const current_time:string = new Date().toISOString();
    const riskID : string = "RSK" + "_" + (current_time);
    
    let riskRef:DocumentReference = this.afs.doc(`/projectList/${pID}/riskList/${riskID}`).ref;
    if(riskRef)
    {
      return riskRef.set({
        id:riskID,
        milestoneID:mID,
        server_time:firebase.firestore.FieldValue.serverTimestamp(),
        creationt_time:current_time,    
        description: r.description,
        impact: r.impact,
        mitigation: r.mitigation,
        owner:r.owner,
        plannedClosureDate:r.plannedClosureDate,
        estimatedClosureDate:r.estimatedClosureDate,
        actualClosureDate:r.actualClosureDate,
        status:r.status,
        RAG:r.RAG,
        remarks:r.remarks
      });     
    }
    return null;
  }

  async updateRisk(pID:string,mID:string,r:Risk):Promise<void>
  {
    
    let riskRef:DocumentReference = this.afs.doc(`/projectList/${pID}/riskList/${r.id}`).ref;
    if(riskRef)
    {
      return riskRef.update({
        description: r.description,
        impact: r.impact,
        mitigation: r.mitigation,
        owner:r.owner,
        plannedClosureDate:r.plannedClosureDate,
        estimatedClosureDate:r.estimatedClosureDate,
        actualClosureDate:r.actualClosureDate,
        status:r.status,
        RAG:r.RAG,
        remarks:r.remarks
      });     
    }
    return null;
  }  

  async createIssue(pID:string,mID:string,i:Issue):Promise<void>
  {
    const current_time:string = new Date().toISOString();
    const issueID : string = "ISU" + "_" + (current_time);
    
    let issueRef:DocumentReference = this.afs.doc(`/projectList/${pID}/issueList/${issueID}`).ref;
    if(issueRef)
    {
      console.log("FirebaseProvider:Set Issue");
      return issueRef.set({
        id:issueID,
        milestoneID:mID,
        server_time:firebase.firestore.FieldValue.serverTimestamp(),
        creationt_time:current_time,    
        description: i.description,
        impact: i.impact,
        owner:i.owner,
        raisedDate:i.raisedDate,
        estimatedClosureDate:i.estimatedClosureDate,
        actualClosureDate:i.actualClosureDate,
        status:i.status,
        RAG:i.RAG,
        remarks:i.remarks
      });     
    }
    return null;
  }

  async updateIssue(pID:string,mID:string,i:Issue):Promise<void>
  {    
    let issueRef:DocumentReference = this.afs.doc(`/projectList/${pID}/issueList/${i.id}`).ref;
    if(issueRef)
    {
      return issueRef.update({
        description: i.description,
        impact: i.impact,
        owner:i.owner,
        raisedDate:i.raisedDate,
        estimatedClosureDate:i.estimatedClosureDate,
        actualClosureDate:i.actualClosureDate,
        status:i.status,
        RAG:i.RAG,
        remarks:i.remarks
      });     
    }
    return null;
  }

  async updateAction(pID:string,mID:string,a:Action):Promise<void>
  {    
    let actionRef:DocumentReference = this.afs.doc(`/projectList/${pID}/actionList/${a.id}`).ref;
    if(actionRef)
    {
      return actionRef.update({
        description: a.description,
        owner:a.owner,
        raisedDate:a.raisedDate,
        estimatedClosureDate:a.estimatedClosureDate,
        actualClosureDate:a.actualClosureDate,
        status:a.status,
        RAG:a.RAG,
        remarks:a.remarks
      });     
    }
    return null;
  }

  async createAction(pID:string,mID:string,a:Action):Promise<void>
  {
    const current_time:string = new Date().toISOString();
    const actionID : string = "ACN" + "_" + (current_time);
    
    let actionRef:DocumentReference = this.afs.doc(`/projectList/${pID}/actionList/${actionID}`).ref;
    if(actionRef)
    {
      console.log("FirebaseProvider:Set Action");
      return actionRef.set({
        id:actionID,
        milestoneID:mID,
        server_time:firebase.firestore.FieldValue.serverTimestamp(),
        creationt_time:current_time,    
        description: a.description,
        owner: a.owner,
        raisedDate:a.raisedDate,
        estimatedClosureDate:a.estimatedClosureDate,
        actualClosureDate:a.actualClosureDate,
        status:a.status,
        RAG:a.RAG,
        remarks:a.remarks
      });     
    }
    return null;
  }

  getProject(pID:string):Promise<any>
  { 
    let project = {} as Project;            
    let docRef = this.afs.doc(`/projectList/${pID}`).ref;
    return docRef.get().then(doc => {
        if (!doc.exists) {
            console.log('No such document!' + doc.id);
            return null;
        } else {
            project.id = doc.id;
            project.description = doc.data().description;
            project.startDate = doc.data().startDate;
            project.endDate = doc.data().endDate;
            project.creationt_time = doc.data().creationt_time;
            return project;
        }
    })
    .catch(err => {
        console.log('Error getting PROJECT document', err);
    });
  }

  getMilestone(pID:string,mID:string):Promise<any>
  { 
    let milestone = {} as Milestone;            
    let docRef = this.afs.doc(`/projectList/${pID}/milestoneList/${mID}`).ref;
    return docRef.get().then(doc => {
        if (!doc.exists) {
            console.log('No such document!' + doc.id);
            return null;
        } else {
          milestone.id = doc.id;
          milestone.description = doc.data().description;
          milestone.targetClosureDate = doc.data().targetClosureDate;
          milestone.completionPercentage = doc.data().completionPercentage;
          milestone.targetClosureDate = doc.data().targetClosureDate;
          milestone.estimatedClosureDate = doc.data().estimatedClosureDate;
          milestone.actualClosureDate = doc.data().actualClosureDate;
          milestone.RAG = doc.data().RAG;
          milestone.remarks = doc.data().remarks;
      
          return milestone;
        }
    })
    .catch(err => {
        console.log('Error getting MILESTONE document', err);
    });
  }

  getRisk(pID:string,mID:string,rID:string):Promise<any>
  { 
    let risk = {} as Risk;            
    let docRef = this.afs.doc(`/projectList/${pID}/riskList/${rID}`).ref;
    return docRef.get().then(doc => {
        if (!doc.exists) {
            console.log('No such document!' + doc.id);
            return null;
        } else {
          risk.id = doc.id;
          risk.milestoneID = doc.data().milestoneID;
          risk.description = doc.data().description;
          risk.impact = doc.data().impact;
          risk.mitigation = doc.data().mitigation; 
          risk.owner = doc.data().owner;          
          risk.plannedClosureDate = doc.data().plannedClosureDate;
          risk.estimatedClosureDate = doc.data().estimatedClosureDate;
          risk.actualClosureDate = doc.data().actualClosureDate;
          risk.status = doc.data().status;
          risk.RAG = doc.data().RAG;
          risk.remarks = doc.data().remarks;
      
          return risk;
        }
    })
    .catch(err => {
        console.log('Error getting RISK document', err);
    });
  }

  getIssue(pID:string,mID:string,iID:string):Promise<any>
  { 
    let issue = {} as Issue;            
    let docRef = this.afs.doc(`/projectList/${pID}/issueList/${iID}`).ref;
    return docRef.get().then(doc => {
        if (!doc.exists) {
            console.log('No such document!' + doc.id);
            return null;
        } else {
          issue.id = doc.id;
          issue.milestoneID = doc.data().milestoneID;
          issue.description = doc.data().description;
          issue.impact = doc.data().impact;
          issue.owner = doc.data().owner;          
          issue.raisedDate = doc.data().raisedDate;
          issue.estimatedClosureDate = doc.data().estimatedClosureDate;
          issue.actualClosureDate = doc.data().actualClosureDate;
          issue.status = doc.data().status;
          issue.RAG = doc.data().RAG;
          issue.remarks = doc.data().remarks;
      
          return issue;
        }
    })
    .catch(err => {
        console.log('Error getting ISSUE document', err);
    });
  }

  getAction(pID:string,mID:string,aID:string):Promise<any>
  { 
    let action = {} as Action;            
    let docRef = this.afs.doc(`/projectList/${pID}/actionList/${aID}`).ref;
    return docRef.get().then(doc => {
        if (!doc.exists) {
            console.log('No such document!' + doc.id);
            return null;
        } else {
          action.id = doc.id;
          action.milestoneID = doc.data().milestoneID;
          action.description = doc.data().description;
          action.owner = doc.data().owner;
          action.raisedDate = doc.data().raisedDate;
          action.estimatedClosureDate = doc.data().estimatedClosureDate;
          action.actualClosureDate = doc.data().actualClosureDate;
          action.status = doc.data().status;
          action.RAG = doc.data().RAG;
          action.remarks = doc.data().remarks;
      
          return action;
        }
    })
    .catch(err => {
        console.log('Error getting ACTION document', err);
    });
  }

}
