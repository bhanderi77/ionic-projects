// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http/';
import { SQLiteObject, SQLite } from '@ionic-native/sqlite';
import 'rxjs/add/operator/map'
import { BehaviorSubject } from 'rxjs/Rx'
import { Storage } from '@ionic/storage'
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Platform } from 'ionic-angular/platform/platform';
import { AppSettings, businessTransaction, financeTransaction, partyProfile, Global} from '../../model/interface'

/*
  Generated class for the DatabaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DatabaseProvider {

  database: SQLiteObject;
  private databaseReady: BehaviorSubject<boolean>;

  constructor(public http: Http, private sqlitePorter: SQLitePorter, private storage: Storage, private sqlite: SQLite, private platform: Platform ) {
    this.databaseReady = new BehaviorSubject(false);
    this.platform.ready().then(() => {
    	this.sqlite.create({
    		name: 'hisaab.db',
    		location: 'default'
    	}).then((db: SQLiteObject) => {
			this.database = db;
			//this.storage.set("database_filled", false);
    		this.storage.get('database_filled').then(val => {
    			if(val) {
    				this.databaseReady.next(true);
    			} else { //insert initial records in database & set database_filled to true
    				this.http.get('assets/hisaabdiary.sql')
    									.map(res => res.text())
    									.subscribe(sql => {
    										this.sqlitePorter.importSqlToDb(this.database,sql)
    										.then(data => {
    											this.databaseReady.next(true);
    											this.storage.set('database_filled',true);	
    										})
    										.catch(e => console.log(e));
    									})
    			}
    		})
   		})
    });
    
  }

  getDatabaseState() {
  	return this.databaseReady.asObservable();
  }

  addPartyProfile(p:partyProfile):Promise<any> {
  //let data = [p.fullName,p.category,p.phone_1,p.email_1,p.ROIForEarlyPayment,p.ROIForLatePayment];
  let data = [p.fullName,p.category,p.phone_1,p.email_1,p.ROIForEarlyPayment,p.ROIForLatePayment,p.addressLine1,p.addressLine2,p.city,p.pincode,p.stateRegion,p.GSTIN,p.PAN];
  console.log("Data : " + data[0] + "\n data[11] : "+ data[11]);
  return new Promise((resolve,reject)=>{
    //this.database.executeSql("INSERT INTO PartyProfile(fullName,category,phone_1,email_1,ROIForEarlyPayment,ROIForLatePayment) VALUES (?,?,?,?,?,?)",data)
    this.database.executeSql("INSERT INTO PartyProfile(fullName,category,phone_1,email_1,ROIForEarlyPayment,ROIForLatePayment,addressLine1,addressLine2,city,pincode,stateRegion,GSTIN,PAN) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",data)
			.then(res => { resolve(res);})
			.catch(e=>console.log("Error in executeSql:INSERT INTO PartyProfile " + e));
	});
  }

getAllPartyProfile():Promise<any> {
  return this.database.executeSql("SELECT * FROM PartyProfile",[])
   .then(data => {
		let party = [];
		if(data.rows.length > 0 ) {
			for(var i=0;i<data.rows.length;i++) {
				party.push({
          ID: data.rows.item(i).ID, 
          fullName: data.rows.item(i).fullName,
          category: data.rows.item(i).category, 
          phone_1: data.rows.item(i).phone_1, 
          email_1: data.rows.item(i).email_1,
          ROIForEarlyPayment: data.rows.item(i).ROIForEarlyPayment,
          ROIForLatePayment: data.rows.item(i).ROIForLatePayment,
          addressLine1: data.rows.item(i).addressLine1,
          addressLine2: data.rows.item(i).addressLine2,
          city: data.rows.item(i).city,
          pincode: data.rows.item(i).pincode,
          stateRegion: data.rows.item(i).stateRegion,
          GSTIN: data.rows.item(i).GSTIN,
          PAN: data.rows.item(i).PAN

         });
			}
		  return party;
		}
		return [];  		
	 });
  }
  
  deleteSelectedParty(id):Promise<any>{
    return this.database.executeSql("DELETE FROM PartyProfile WHERE ID = ?",[id]).then(res=>{
      return res;
    })
  }

  getSelectedPartyProfile(id):Promise<any>{  // HB27/1/2018
    return this.database.executeSql("SELECT * FROM PartyProfile WHERE ID=?",[id]).then(data=>{
        let oneParty = [];
        if(data.rows.length > 0 ) {
            oneParty.push({
            ID: data.rows.item(0).ID,
            fullName: data.rows.item(0).fullName,
            category: data.rows.item(0).category, 
            phone_1: data.rows.item(0).phone_1, 
            email_1: data.rows.item(0).email_1,
            ROIForEarlyPayment: data.rows.item(0).ROIForEarlyPayment,
            ROIForLatePayment: data.rows.item(0).ROIForLatePayment,
            addressLine1: data.rows.item(0).addressLine1,
            addressLine2: data.rows.item(0).addressLine2,
            city: data.rows.item(0).city,
            pincode: data.rows.item(0).pincode,
            stateRegion: data.rows.item(0).stateRegion,
            GSTIN: data.rows.item(0).GSTIN,
            PAN: data.rows.item(0).PAN

          });
          return oneParty;
        }
        return [];
      });
  }  // HB27/1/2018

  //RB1702
  // getSelectedFinanceTransaction(finsTrxId):Promise<any>{//trxType,trxDate,trxCurrency,trxCurrencyRate,trxValue,trxPaymentMethod,trxRemarks
  //   console.log("Inside Database::getSelectedFinanceTransaction()")
  //   return this.database.executeSql("SELECT trxType,trxDate,trxCurrency,trxCurrencyRate,trxValue,trxPaymentMethod,trxRemarks FROM FinanceTransaction WHERE trxID=?",[finsTrxId])
  //   .then(data=>{
  //     let finsTrx = <financeTransaction>{};
  //       // if(data.rows.length > 0 ) {
  //           finsTrx.trxType = data.rows.item(0).trxType,
  //           finsTrx.trxDate = data.rows.item(0).trxDate,
  //           finsTrx.trxCurrency = data.rows.item(0).trxCurrency, 
  //           finsTrx.trxCurrencyRate = data.rows.item(0).trxCurrencyRate, 
  //           finsTrx.trxValue = data.rows.item(0).trxValue,
  //           finsTrx.trxPaymentMethod = data.rows.item(0).trxPaymentMethod,
  //           finsTrx.trxRemarks = data.rows.item(0).trxRemarks
  //           return finsTrx;
  //       // }
  //       // return null;
  //     },
  //     err => {
  //       console.log('Error: ', err);
  //       return null;
  //     });
  // }

  editPartyProfile(p:partyProfile,partyID:number):Promise<any>{      // HB27/1/2018
      let data = [p.fullName,p.category,p.phone_1,p.email_1,p.ROIForEarlyPayment,p.ROIForLatePayment,p.addressLine1,p.addressLine2,p.city,p.pincode,p.stateRegion,p.GSTIN,p.PAN,partyID];
      return this.database.executeSql("UPDATE PartyProfile SET fullName = ?, category = ?, phone_1 = ?, email_1 = ? , ROIForEarlyPayment = ? , ROIForLatePayment = ? , addressLine1 = ? , addressLine2 = ? , city = ? , pincode = ? , stateRegion = ? , GSTIN = ? , PAN = ? WHERE ID = ?",data)
    .then(res => {
      console.log("Inside database::editPartyProfile: res " + res);
      return res;
    });
  }// HB27/1/2018

  addBusinessTransaction(myBizTrx: businessTransaction):Promise<any> {
	let data = [myBizTrx.trxDate,
		myBizTrx.trxType,
		myBizTrx.trxPartyID,
    myBizTrx.trxParentID,
    myBizTrx.trxInvoiceID,
		myBizTrx.trxQty,
    myBizTrx.trxRate,
    myBizTrx.trxDiscount,
    myBizTrx.trxDiscountValue,
    myBizTrx.trxValue,
    myBizTrx.trxCGSTValue,
    myBizTrx.trxSGSTValue,
    myBizTrx.trxIGSTValue,
    myBizTrx.trxTax,
		myBizTrx.trxCurrency,
		myBizTrx.trxCurrencyRate,
		myBizTrx.trxDueDays,
		myBizTrx.trxDueDate.toISOString(),
		myBizTrx.trxROIForEarlyPayment,
		myBizTrx.trxROIForLatePayment,
		myBizTrx.trxStatus,
		myBizTrx.trxRemarks];
  
    console.log("INSERT INTO BusinessTransaction:"
    + "\n trxCurrency: " + myBizTrx.trxCurrency
    + "\n trxCurrencyRate: " + myBizTrx.trxCurrencyRate
    + "\n trxCGSTValue: " + myBizTrx.trxCGSTValue
    + "\n trxSGSTValue: " + myBizTrx.trxSGSTValue
    + "\n trxIGSTValue: " + myBizTrx.trxIGSTValue
    + "\n trxDiscountValue: " + myBizTrx.trxDiscountValue	
    );
    /*
	return this.database.executeSql("INSERT INTO BusinessTransaction(trxDate,trxType,trxPartyID,trxParentID,trxQty,trxRate,trxValue,trxCurrency,trxCurrencyRate,trxDueDays,trxDueDate,trxROIForEarlyPayment,trxROIForLatePayment,trxStatus,trxRemarks) VALUES (datetime(?),?,?,?,?,?,?,?,?,?,datetime(?),?,?,?,?)",data)
		.then(res => {return res;});
	*/
	return new Promise((resolve,reject) => {
		this.database.executeSql("INSERT INTO BusinessTransaction(trxDate,trxType,trxPartyID,trxParentID,trxInvoiceID,trxQty,trxRate,trxDiscount,trxDiscountValue,trxValue,trxCGSTValue,trxSGSTValue,trxIGSTValue,trxTax,trxCurrency,trxCurrencyRate,trxDueDays,trxDueDate,trxROIForEarlyPayment,trxROIForLatePayment,trxStatus,trxRemarks) VALUES (date(?),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,date(?),?,?,?,?)",data)
		.then(res => {
			resolve(res);
			reject("Reject from executeSql 'INSERT INTO BusinessTransaction");
		}).catch(e => console.log("Error from executeSql 'INSERT INTO BusinessTransaction'" + e));
	});
  }

  getBusinessTransactionsForPartyID(partyID: number)
  {
	let where = [partyID];  
	console.log("Inside LocalDbProvider::getBusinessTransactionsForPartyID(PartyID) " + partyID);
	return this.database.executeSql("SELECT * FROM BusinessTransaction WHERE trxPartyID = ? ORDER BY trxPartyID ASC, trxDueDate ASC, trxValue DESC",where).then(data => {
		let respBizTrx = [];
		//let respBizTrx = [<businessTransaction>{}]; //This is initializeding array with 1 blank element
		console.log("No. of Records returned: " + data.rows.length);
		if(data.rows.length > 0 ) {
			for(var i=0;i<data.rows.length;i++) {
				respBizTrx.push({trxID: data.rows.item(i).trxID,
					trxDate: data.rows.item(i).trxDate, 
					trxType: data.rows.item(i).trxType, 
					trxPartyID: data.rows.item(i).trxPartyID, 
          trxParentID: data.rows.item(i).trxParentID, 
          trxInvoiceID: data.rows.item(i).trxInvoiceID, 
					trxQty: data.rows.item(i).trxQty, 
          trxRate: data.rows.item(i).trxRate, 
          trxDiscount: data.rows.item(i).trxDiscount,
          trxDiscountValue: data.rows.item(i).trxDiscountValue,
          trxValue: data.rows.item(i).trxValue, 
          trxCGSTValue: data.rows.item(i).trxCGSTValue,
          trxSGSTValue: data.rows.item(i).trxSGSTValue,
          trxIGSTValue: data.rows.item(i).trxIGSTValue,
          trxTax: data.rows.item(i).trxTax,
					trxCurrency: data.rows.item(i).trxCurrency, 
					trxCurrencyRate: data.rows.item(i).trxCurrencyRate, 
					trxDueDays: data.rows.item(i).trxDueDays, 
					trxDueDate: data.rows.item(i).trxDueDate,
					trxROIForEarlyPayment: data.rows.item(i).trxROIForEarlyPayment,
					trxROIForLatePayment: data.rows.item(i).trxROIForLatePayment,
					trxStatus: data.rows.item(i).trxStatus,
					trxRemarks: data.rows.item(i).trxRemarks});
			}
			console.log("# of recs returning " + respBizTrx.length );
		return respBizTrx;
		}
		return [];  		
	});
  }

  getOpenBusinessTrxsForPartyID(partyID: number)
  {
	let where = [partyID,Global.bizTrxStatus.OPEN];  
	console.log("Inside LocalDbProvider::getOpenBusinessTrxsForPartyID(PartyID) " + partyID);
	return this.database.executeSql("SELECT * FROM BusinessTransaction WHERE trxPartyID = ? AND trxStatus = ? ORDER BY trxDueDate ASC, trxValue DESC",where).then(data => {
		let respBizTrx = [];
		//let respBizTrx = [<businessTransaction>{}]; //This is initializeding array with 1 blank element
		console.log("No. of Records returned: " + data.rows.length);
		if(data.rows.length > 0 ) {
			for(var i=0;i<data.rows.length;i++) {
				console.log("data.rows.item(i).trxID : " + data.rows.item(i).trxID);
				respBizTrx.push({trxID: data.rows.item(i).trxID,
					trxDate: data.rows.item(i).trxDate, 
					trxType: data.rows.item(i).trxType, 
					trxPartyID: data.rows.item(i).trxPartyID, 
          trxParentID: data.rows.item(i).trxParentID, 
          trxInvoiceID: data.rows.item(i).trxInvoiceID,
					trxQty: data.rows.item(i).trxQty, 
          trxRate: data.rows.item(i).trxRate,
          trxDiscount: data.rows.item.trxDiscount,
          trxDiscountValue: data.rows.item.trxDiscountValue, 
          trxValue: data.rows.item(i).trxValue, 
          trxCGSTValue: data.rows.item(i).trxCGSTValue,
          trxSGSTValue: data.rows.item(i).trxSGSTValue,
          trxIGSTValue: data.rows.item(i).trxIGSTValue,
          trxTax: data.rows.item(i).trxTax,
					trxCurrency: data.rows.item(i).trxCurrency, 
					trxCurrencyRate: data.rows.item(i).trxCurrencyRate, 
					trxDueDays: data.rows.item(i).trxDueDays, 
					trxDueDate: data.rows.item(i).trxDueDate,
					trxROIForEarlyPayment: data.rows.item(i).trxROIForEarlyPayment,
					trxROIForLatePayment: data.rows.item(i).trxROIForLatePayment,
					trxStatus: data.rows.item(i).trxStatus,
					trxRemarks: data.rows.item(i).trxRemarks});
			}
			console.log("# of recs returning " + respBizTrx.length );
		return respBizTrx;
		}
		return [];  		
	});
  }

  addFinanceTransaction(myFinTrx: financeTransaction):Promise<any> {
    let data = [myFinTrx.BizTrxID,
      myFinTrx.trxDate,
      myFinTrx.trxType,
      myFinTrx.trxPartyID,
      myFinTrx.trxForiegnValue,
      myFinTrx.trxLocalValue,
      myFinTrx.trxCurrency,
      myFinTrx.trxCurrencyRate,
      myFinTrx.trxInterestRealizedinLocal,
      myFinTrx.trxInterestRealizedinForiegn,
      myFinTrx.trxDiffDays,
      myFinTrx.trxOpeningBalance,
      myFinTrx.trxClosingBalance,
      myFinTrx.trxPaymentMethod,
      myFinTrx.trxRemarks
    ];
	//let data = ["1","1"];
	 console.log("INSERT INTO FinanceTransaction:"
			+ "\n trxPartyID: " + myFinTrx.trxPartyID
			+ "\n BizTrxID: " + myFinTrx.BizTrxID
			+ "\n trxType: " + myFinTrx.trxType
			+ "\n trxDate: " + myFinTrx.trxDate
			+ "\n trxValue: " + myFinTrx.trxLocalValue
			+ "\n trxCurrency: " + myFinTrx.trxCurrency
			+ "\n trxCurrencyRate: " + myFinTrx.trxCurrencyRate
      + "\n trxInterestRealizedinLocal: " + myFinTrx.trxInterestRealizedinLocal
      + "\n trxInterestRealizedinForiegn: " + myFinTrx.trxInterestRealizedinForiegn
			+ "\n trxOpeningBalance: " + myFinTrx.trxOpeningBalance
			+ "\n trxClosingBalance: " + myFinTrx.trxClosingBalance
			+ "\n trxRemarks: " + myFinTrx.trxRemarks			
			);
	// return new Promise((resolve,reject) => {
		return this.database.executeSql("INSERT INTO FinanceTransaction(BizTrxID,trxDate,trxType,trxPartyID,trxForiegnValue,trxLocalValue,trxCurrency,trxCurrencyRate,trxInterestRealizedinLocal,trxInterestRealizedinForiegn,trxDiffDays,trxOpeningBalance,trxClosingBalance,trxPaymentMethod,trxRemarks) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",data)
		.then(res => {
				console.log("INSERT INTO FinanceTransaction: res " + res.trxID);
				return res;
		}).catch(e=>console.log("Error from executeSql: INSERT INTO FinanceTransaction" + e));		
	// });	
  }

  getFinanceTransactionsForPartyID(partyID: number)
  {
    let where = [partyID];  
    console.log("Inside LocalDbProvider::getFinanceTransactionsForPartyID(PartyID,bizTrxID) " + partyID);
    return this.database.executeSql("SELECT * FROM FinanceTransaction WHERE trxPartyID = ? ORDER BY trxPartyID ASC,BizTrxID ASC,date_created ASC",where).then(data => {
      //let respFinTrx = [<financeTransaction>{}];
      let respFinTrx = [];
      console.log("No. of Records returned: " + data.rows.length);
      console.log("# of records in respFinTrx: " + respFinTrx.length);
      //respFinTrx.reduce()
      if(data.rows.length > 0 ) {
        for(var i=0;i<data.rows.length;i++) {
          respFinTrx.push({trxID: data.rows.item(i).trxID,
            BizTrxID: data.rows.item(i).BizTrxID, 
            trxDate: data.rows.item(i).trxDate, 
            trxType: data.rows.item(i).trxType, 
            trxPartyID: data.rows.item(i).trxPartyID, 
            trxForiegnValue: data.rows.item(i).trxForiegnValue,
            trxLocalValue: data.rows.item(i).trxLocalValue, 
            trxCurrency: data.rows.item(i).trxCurrency, 
            trxCurrencyRate: data.rows.item(i).trxCurrencyRate, 
            trxInterestRealizedinLocal: data.rows.item(i).trxInterestRealizedinLocal,
            trxInterestRealizedinForiegn: data.rows.item(i).trxInterestRealizedinForiegn, 
            trxDiffDays: data.rows.item(i).trxDiffDays,
            trxOpeningBalance: data.rows.item(i).trxOpeningBalance, 
            trxClosingBalance: data.rows.item(i).trxClosingBalance,
            trxPaymentMethod: data.rows.item(i).trxPaymentMethod,
            trxRemarks: data.rows.item(i).trxRemarks});
        }
      console.log("# of recs returning " + respFinTrx.length );
      return respFinTrx;
      }
      return [];  		
    });
  }
 
  getFinanceTransactionsForBizTrxID(partyID: number, bizTrxID: number)
  {
    let where = [partyID,bizTrxID];  
    console.log("Inside LocalDbProvider::getFinanceTransactionsForBizTrxID(PartyID,bizTrxID) " + partyID + " " + bizTrxID);
    return this.database.executeSql("SELECT * FROM FinanceTransaction WHERE trxPartyID = ? AND BizTrxID = ?",where).then(data => {
      //let respFinTrx = [<financeTransaction>{}];
      let respFinTrx = [];
      console.log("No. of Records returned: " + data.rows.length);
      console.log("# of records in respFinTrx: " + respFinTrx.length);
      //respFinTrx.reduce()
      if(data.rows.length > 0 ) {
        for(var i=0;i<data.rows.length;i++) {
          respFinTrx.push({trxID: data.rows.item(i).trxID,
            BizTrxID: data.rows.item(i).BizTrxID, 
            trxDate: data.rows.item(i).trxDate, 
            trxType: data.rows.item(i).trxType, 
            trxPartyID: data.rows.item(i).trxPartyID, 
            trxForiegnValue: data.rows.item(i).trxForiegnValue,
            trxLocalValue: data.rows.item(i).trxLocalValue,
            trxCurrency: data.rows.item(i).trxCurrency, 
            trxCurrencyRate: data.rows.item(i).trxCurrencyRate, 
            trxInterestRealized: data.rows.item(i).trxInterestRealized, 
            trxDiffDays: data.rows.item(i).trxDiffDays,
            trxOpeningBalance: data.rows.item(i).trxOpeningBalance, 
            trxClosingBalance: data.rows.item(i).trxClosingBalance,
            trxPaymentMethod: data.rows.item(i).trxPaymentMethod,
            trxRemarks: data.rows.item(i).trxRemarks});
        }
      console.log("# of recs returning " + respFinTrx.length );
      return respFinTrx;
      }
      return [];  		
    });
  }


  getBusinessTrxID(partyID: number, trxDate: Date) {
    let where = [partyID,trxDate]; 

    console.log("Inside LocalDbProvider::getBusinessTrxID(PartyID,trxDate) " + partyID + " " + trxDate);
    return this.database.executeSql("SELECT trxID FROM BusinessTransaction WHERE trxPartyID = ? AND trxDate = date(?) ORDER BY date_created DESC",where).then(data =>{
      console.log("Returning Biz trxID: " + data.rows.item(0).trxID);
      //console.log("Returning Biz trxID: ");
      return data.rows.item(0).trxID;//.rows.item(0).trxID;
    }).catch(e => console.log(e));	
  }	

  getLastBusinessTrxID(){
    return this.database.executeSql("SELECT trxID FROM BusinessTransaction ORDER BY trxID DESC LIMIT 1",[]).then(data =>{
      console.log("Returning Last Biz trxID: " + data.rows.item(0).trxID);
      //console.log("Returning Biz trxID: ");
      return data.rows.item(0).trxID;//.rows.item(0).trxID;
    }).catch(e => console.log(e));	
  }
  closeBusinessTransaction(partyID: number, trxID: number)
  {
	  let where = [Global.bizTrxStatus.CLOSED,partyID, trxID];
	  console.log("Inside LocalDbProvider::closeBusinessTransaction(partyID,trxID) " + partyID + " " + trxID);
    return this.database.executeSql("UPDATE BusinessTransaction SET trxStatus = ? WHERE trxPartyID = ? AND trxID = ?",where).then(data =>{
      console.log("Update SQL Done: " + data);
      //console.log("Returning Biz trxID: ");
      return data;//.rows.item(0).trxID;
    }).catch(e => console.log(e));	
  }

  getAppSettings(){
    return this.database.executeSql("SELECT * FROM AppSettings", [])
    .then(data => {
      console.log("Inside database:getAppSettings: data.rows.length " + data.rows.length);
      //let appSettings = [];
      let appSettings = <AppSettings>{};
      appSettings.asForiegnCurrency = data.rows.item(0).foriegnCurrency;
      appSettings.asLocalCurrency = data.rows.item(0).localCurrency;
      appSettings.asConversionRate = data.rows.item(0).conversionRate;
      appSettings.asDueDays = data.rows.item(0).dueDays;
      appSettings.asCGST =  data.rows.item(0).CGST;
      appSettings.asSGST =  data.rows.item(0).SGST;
      appSettings.asIGST =  data.rows.item(0).IGST;
      return appSettings;
    }, err => {
      console.log('Error: ', err);
      return null;
    })    
  }

  updateAppSettings(a:AppSettings){
    let settings = [a.asForiegnCurrency, a.asLocalCurrency, a.asConversionRate,a.asDueDays,a.asCGST,a.asSGST,a.asIGST];
    console.log("Inside database::updateAppSettings: data " + settings[2]);
    //return this.database.executeSql("UPDATE appsettings SET foriegnCurrency = " + settings[0] + "localCurrency = " + settings[1] + "conversionRate = " + settings[2] + "dueDays = " + settings[3] + "rateOfInterest = " + settings[4] + "WHERE ID = 1",null)
    return this.database.executeSql("UPDATE AppSettings SET foriegnCurrency = ?, localCurrency = ?, conversionRate = ?, dueDays = ?, CGST = ?, SGST = ?, IGST = ? WHERE ID = 1",settings)
    .then(res => {
      console.log("Inside database::updateAppSettings: res " + res);
      
      return res;
    });
  }
  

  deleteBusinessTransaction(partyID: number, BizTrxID:number)
  {
    let where = [partyID,BizTrxID];
    return this.database.executeSql("DELETE FROM BusinessTransaction WHERE trxPartyID = ? AND trxID = ?",where)
    .then(res => {
      console.log("Inside deleteBusinessTransaction: res " + res);
      return res;
    }, err => {
      console.log('Error: DELETE BusinessTransaction for '+ partyID + ' ' + BizTrxID + err);
      return null;
    }).catch(e => console.log('Exception: DELETE  BusinessTransaction for  '+ partyID + ' ' + BizTrxID + e));
  }

  deleteFinanceTransaction(partyID: number, BizTrxID:number)
  {
    let where = [partyID,BizTrxID];
    return this.database.executeSql("DELETE FROM FinanceTransaction WHERE trxPartyID = ? AND BizTrxID = ?",where)
    .then(res => {
      console.log("Inside deleteFinanceTransaction: res " + res);
      return res;
    }, err => {
      console.log('Error: DELETE FinanceTransaction for '+ partyID + ' ' + BizTrxID + err);
      return null;
    }).catch(e => console.log('Exception: DELETE  FinanceTransaction for  '+ partyID + ' ' + BizTrxID + e));
  }

  deleteSelectedFinanceTransaction(finTrxID:number){
    console.log("Inside Database::deleteSelectedFinanceTransaction");
    let where = [finTrxID];
    return this.database.executeSql("DELETE FROM FinanceTransaction WHERE trxID = ?",where)
    .then(res => {
      console.log("Inside deleteSelectedFinanceTransaction: res " + res);
      return res;
    }, err => {
      console.log('Error: DELETE FinanceTransaction for FinTrxID :'+ finTrxID + err);
      return null;
    }).catch(e => console.log('Exception: DELETE  FinanceTransaction for  '+ finTrxID + e));
  }

  updateFinanceTransaction(finTrxID:number,finTrxOpeningBalance:number,finTrxClosingBalance:number){
    console.log("Inside Database::updateFinanceTransaction");
    let set=[finTrxOpeningBalance,finTrxClosingBalance,finTrxID];
    return this.database.executeSql("UPDATE FinanceTransaction SET trxOpeningBalance = ?, trxClosingBalance = ? WHERE trxID = ? ",set)
    .then(res => {
      console.log("Inside database::updateFinanceTransaction: res " + res);
      return res;
    }, err => {
      console.log('Error: Update FinanceTransaction for FinTrxID : '+ finTrxID + err);
      return null;
    }).catch(e => console.log('Exception: Update  FinanceTransaction for  '+ finTrxID + e));
  }

  updateBizTrxnStatus(bizTrxID:number,bizTrxStatus:string){
    console.log("Inside Database::updateBizTrxnStatus");
    let set=[bizTrxStatus,bizTrxID];
    return this.database.executeSql("UPDATE BusinessTransaction SET trxStatus = ? WHERE trxID = ? ",set)
    .then(res => {
      console.log("Inside database::updateBizTrxnStatus: res " + res);
      return res;
    }, err => {
      console.log('Error: Update BusinessTransaction for FinTrxID : '+ bizTrxID + err);
      return null;
    }).catch(e => console.log('Exception: Update  BusinessTransaction for  '+ bizTrxID + e));
  }

  getAllStates(){
    console.log("Inside Database::getAllStates");
    return this.database.executeSql("SELECT * FROM States", [])
    .then(data => {
      let States = [];
      if(data.rows.length > 0 ) {
        for(var i=0;i<data.rows.length;i++) {
          States.push({
            stateName: data.rows.item(i).stateName, 
            stateTinNumber: data.rows.item(i).stateTinNumber,
            stateCode: data.rows.item(i).stateCode 
          });
        }
        return States;
      }
      return [];  		
     });
  }
}
