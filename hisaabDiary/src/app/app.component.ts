import { Component, ViewChild } from '@angular/core';
import { Platform , Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DatabaseProvider } from '../providers/database/database';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav:Nav;

  splash = true;
  rootPage:any = 'HomePage';
  pages: Array<{title: string, component: any}>;

  constructor(platform: Platform, statusBar: StatusBar,splashScreen: SplashScreen) { // 
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      //setTimeout(() => this.splash = false, 4000);
    });
  }
}

