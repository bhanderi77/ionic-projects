import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { CallNumber} from '@ionic-native/call-number';
import { DatePipe } from '@angular/common';

import { MyApp } from './app.component';
import { DatabaseProvider } from '../providers/database/database';

import { IonicStorageModule } from '@ionic/storage'

import { SQLitePorter } from '@ionic-native/sqlite-porter'
import { SQLite } from '@ionic-native/sqlite'
import { DatafinderProvider } from '../providers/datafinder/datafinder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { HttpModule } from '@angular/http'


import { FormsModule } from '@angular/forms'
import { CustomFormsModule } from 'ng2-validation'
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CustomFormsModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp 
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider,
    SQLitePorter,
    SQLite,
    DatafinderProvider,
    File,
    FileOpener,
    CallNumber,
    DatePipe,
    SocialSharing
  ]
})
export class AppModule {}
