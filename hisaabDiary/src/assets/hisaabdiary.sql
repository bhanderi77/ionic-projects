DROP TABLE IF EXISTS PartyProfile;
DROP TABLE IF EXISTS BusinessTransaction;
DROP TABLE IF EXISTS FinanceTransaction;
DROP TABLE IF EXISTS States;

CREATE TABLE IF NOT EXISTS PartyProfile(ID INTEGER PRIMARY KEY AUTOINCREMENT,
fullName TEXT NOT NULL,
category TEXT NOT NULL,
phone_1 TEXT NOT NULL,
email_1 TEXT,
ROIForEarlyPayment NUMERIC,
ROIForLatePayment NUMERIC,
addressLine1 TEXT NOT NULL,
addressLine2 TEXT NOT NULL,
city TEXT NOT NULL,
pincode NUMERIC NOT NULL,
stateRegion TEXT NOT NULL,
GSTIN TEXT NOT NULL,
PAN TEXT NOT NULL
); 

CREATE TABLE IF NOT EXISTS AppSettings(ID INTEGER PRIMARY KEY AUTOINCREMENT,
foriegnCurrency TEXT NOT NULL,
localCurrency TEXT NOT NULL,
conversionRate NUMERIC NOT NULL,
dueDays NUMERIC NOT NULL,
CGST NUMERIC NOT NULL,
SGST NUMERIC NOT NULL,
IGST NUMERIC NOT NULL);
INSERT INTO AppSettings (foriegnCurrency,localCurrency,conversionRate,dueDays,CGST,SGST,IGST) VALUES ("usd","inr",64.50,90,0.125,0.125,0.25);


CREATE TABLE IF NOT EXISTS BusinessTransaction(trxID INTEGER PRIMARY KEY AUTOINCREMENT,
  date_created datetime default current_timestamp,
  last_updated datetime,
  operator_id TEXT,
  trxDate TEXT,
  trxType TEXT,
  trxPartyID INTEGER,
  trxParentID INTEGER,
  trxInvoiceID TEXT,
  trxQty NUMERIC,
  trxRate NUMERIC,
  trxDiscount NUMERIC,
  trxDiscountValue NUMERIC,
  trxValue NUMERIC,
  trxCGSTValue NUMERIC,
  trxSGSTValue NUMERIC,
  trxIGSTValue NUMERIC,
  trxTax NUMERIC,
  trxCurrency TEXT,
  trxCurrencyRate NUMERIC,
  trxDueDays INTEGER,
  trxDueDate TEXT,
  trxROIForEarlyPayment NUMERIC,
  trxROIForLatePayment NUMERIC,
  trxStatus TEXT,
  trxRemarks TEXT
);
CREATE TABLE IF NOT EXISTS FinanceTransaction(trxID INTEGER PRIMARY KEY AUTOINCREMENT,
  date_created datetime default current_timestamp,
  last_updated datetime,
  operator_id TEXT,
  BizTrxID INTEGER,
  trxDate TEXT,
  trxType TEXT,
  trxPartyID INTEGER,
  trxForiegnValue NUMERIC,
  trxLocalValue NUMERIC,
  trxCurrency TEXT,
  trxCurrencyRate NUMERIC,
  trxInterestRealizedinLocal NUMERIC,
  trxInterestRealizedinForiegn NUMERIC,
  trxDiffDays NUMERIC,
  trxOpeningBalance NUMERIC,
  trxClosingBalance NUMERIC,
  trxPaymentMethod TEXT,
  trxRemarks TEXT
);

CREATE TABLE IF NOT EXISTS States(stateID INTEGER PRIMARY KEY AUTOINCREMENT,
stateName TEXT NOT NULL,
stateTinNumber NUMERIC NOT NULL,
stateCode TEXT NOT NULL);

INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Andaman and Nicobar Islands",35,"AN");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Andhra Pradesh",28,"AD");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Arunachal Pradesh",12,"AR");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Assam",18,"AS");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Bihar",10,"BR");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Chandigarh",04,"CH");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Chattisgarh",22,"CG");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Dadra and Nagar Haveli",26,"DN");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Daman and Diu",25,"DD");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Delhi",07,"DL");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Goa",30,"GA");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Gujarat",24,"GJ");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Haryana",06,"HR");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Himachal Pradesh",02,"HP");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Jammu and Kashmir",01,"JK");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Jharkhand",20,"JH");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Karnataka",29,"KA");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Kerala",32,"KL");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Lakshadweep Islands",31,"LD");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Madhya Pradesh",23,"MP");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Maharashtra",27,"MH");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Manipur",14,"MN");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Meghalaya",17,"ML");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Mizoram",15,"MZ");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Nagaland",13,"NL");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Odisha",21,"OD");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Pondicherry",34,"PY");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Punjab",03,"PB");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Rajasthan",08,"RJ");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Sikkim",11,"SK");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Tamil Nadu",33,"TN");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Telangana",36,"TS");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Tripura",16,"TR");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Uttar Pradesh",09,"UP");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("Uttarakhand",05,"UK");
INSERT INTO States (stateName,stateTinNumber,stateCode) VALUES ("West Bengal",19,"WB");