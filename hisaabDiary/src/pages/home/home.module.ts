import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { PipesModule } from '../../pipes/pipes.module'
import { ChartsModule } from 'ng2-charts'

@NgModule({
  declarations: [
    HomePage
  ],
  imports: [
    ChartsModule,
    PipesModule,
    IonicPageModule.forChild(HomePage),
  ],
})
export class HomePageModule {}