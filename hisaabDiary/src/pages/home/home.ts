import { Component } from '@angular/core';
import { ModalController, NavController, Platform, AlertController, IonicPage } from 'ionic-angular';
import { AddPartyPage } from '../addParty/addparty';
import { AppSettingsPage } from '../appSettings/appSettings'
import { DatabaseProvider } from '../../providers/database/database';
import { PartySummaryPage } from '../partySummary/partySummary';
import { MySummaryPage } from '../mySummary/mySummary';
import { CallNumber } from '@ionic-native/call-number';
//import {} from


import { AppSettings,partyProfile, partySummary,FinanceSummary,MySummary,Global,getPartySummary} from '../../model/interface'
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { DatePipe } from '@angular/common';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  

  partyProfiles:Array<partyProfile>;	
  public doughnutChartType:string = 'doughnut';
 

  appSettings: AppSettings;
  partyList:Array<partySummary>;
  public mySummary:MySummary;
  
  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    private databaseProvider: DatabaseProvider,
    private alertCtrl: AlertController,
    private call : CallNumber,
    private plt: Platform,
    private file: File,
    private fileOpener: FileOpener,
    public datepipe: DatePipe) {
    this.partyList = [];
    this.databaseProvider.getDatabaseState().subscribe(rdy => {
      console.log("inside HomePage::constructor -> rdy " + rdy);
      if(rdy) {
        this.loadPartyDetails();  
        this.loadAppSettings();      
      }
    });
    // this.databaseProvider.getDatabaseState().subscribe(rdy => {
    //   console.log("inside HomePage::constructor -> rdy " + rdy);
    //   if (rdy) {
    //     this.loadAppSettings();
    //   }
    // });
    this.mySummary = {} as MySummary;
    this.mySummary.saleSummary = {} as FinanceSummary;
    this.mySummary.purchaseSummary = {} as FinanceSummary;
  }

  async callNumber(number:any):Promise<any>{
    try{
      await this.call.callNumber(String(number),true);
    }
    catch(e){
      console.error(e);
    }
  }

  ionViewDidLoad() { 
    
    console.log('HomwPage::ionViewDidLoad');
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter HomePage');
  //  this.loadPartyDetails();  
  }
  
  loadPartyDetails() {
    this.partyProfiles = [];
    this.partyList = [];
    this.databaseProvider.getAllPartyProfile()
    .then(data => {
      this.partyProfiles = data;
      for(let i = 0; i < data.length; i++)
      {
      let p = {} as partySummary;
      p.party = this.partyProfiles[i]
      p.bizTrxSummary = [];
      this.partyList.push(p);  
      }
      this.buildPartySummary();  
    });
  }

  addParty() {
    const modal = this.modalCtrl.create('AddPartyPage',{ID:0});
    modal.present();
    console.log("inside addParty method");
    modal.onDidDismiss((data) => {
      this.loadPartyDetails();
    });    
  }

  GoToSummaryPage(p:partySummary) {
    console.log('HomePage::GoToSummaryPage() : ' + p.party.fullName);
    this.navCtrl.push('PartySummaryPage',{pSummary: p,appSettings: this.appSettings});
  }
  GoToMySummary()
  {
    this.calculateMySummary();
    this.navCtrl.push('MySummaryPage',{mySummary: this.mySummary});
  }

  downloadPdf(){
    this.calculateMySummary();
    let today = new Date(); 
        
    function displayPartySummaryHeader(partySummary) {
      let columns=[];
        
      if(partySummary.party.category === Global.partyCategory.BUYER)
      {
        columns.push({text : partySummary.party.fullName + ' : ' + partySummary.businessAmtTotal, color:'white',bold:true, fontSize : 15});
      }
      else if(partySummary.party.category === Global.partyCategory.SUPPLIER)
      {
        columns.push({text : partySummary.party.fullName + ' : ' + (partySummary.businessAmtTotal)*(-1), color:'white',bold:true, fontSize : 15});
      }
      return {
            alignment: 'center',
            table: {
             headerRows: 0,
             widths:['*'],
             body: [
               columns
               ],fillColor:'black'
             },
             layout: {
               fillColor: function (i, node) {
                 return (i  === 0) ? '#212121' : null;
               }               
             }           
        };
    }
    function displayPartySummaryFooter(partySummary) {
      let columns=[];
        
      if(partySummary.party.category === Global.partyCategory.BUYER)
      {
        columns.push({text : partySummary.amtReceviedOrPaid , color:'green',bold:true, fontSize : 15},
        {text : partySummary.amtPending, color:'red', bold:true, fontSize : 15}); 

        columns.push({text : partySummary.interestRealized, color:'green',bold:true, fontSize : 15},
        {text : partySummary.interestUnRealized, color:'red', bold:true, fontSize : 15}); 
      }
      else if(partySummary.party.category === Global.partyCategory.SUPPLIER)
      {
        columns.push({text : partySummary.amtReceviedOrPaid*(-1) , color:'green',bold:true, fontSize : 15},
        {text : partySummary.amtPending*(-1), color:'red', bold:true, fontSize : 15}); 

        columns.push({text : partySummary.interestRealized*(-1) , color:'green',bold:true, fontSize : 15},
        {text : partySummary.interestUnRealized*(-1), color:'red', bold:true, fontSize : 15}); 
      }
      return {
            alignment: 'center',
            table: {
             headerRows: 0,
             widths:['50%','50%'],
             body: [
               [columns[0],columns[1]],
               [columns[2],columns[3]],
               ],fillColor:'black'
             },
             layout: {
            /*
               fillColor: function (i, node) {
                 return (i  === 0) ? '#212121' : null;
               }  
            */             
             }           
        };
    } 
 
    
    function displayPartyList(partyList){
       let recs = [];
       for(var i=0 ; i < partyList.length ; i++){ 
         if(partyList[i].businessAmtTotal > 0)
         {
         recs.push(displayPartySummaryHeader(partyList[i]));
         recs.push(displayPartySummaryFooter(partyList[i]));  
         recs.push('\n'); 
         }       
       }
       return recs;
    }
       
   let datepipe:DatePipe = new DatePipe('en-US');
    let dd = {
        content: [
            {text: 'My Dashboard', bold:true ,alignment:'center' , fontSize : 20 },
            '\n',
            {text: 'Summary as on : ' + this.partyList[0].sysDate.getDate()+'-'+(this.partyList[0].sysDate.getMonth()+1)+'-'+this.partyList[0].sysDate.getFullYear(), italic:true,alignment:'center' , fontSize : 10},
            //{text: 'Summary as on : ' + datepipe.transform(new Date().toISOString(),'medium'), italic:true,alignment:'center' , fontSize : 10},
            '\n',
            '\n',
            {
              alignment: 'center',
              columns: [
                {
                  text: 'SALE : ' + this.mySummary.saleSummary.amtTotal, bold:true ,alignment:'center' , fontSize : 15
                },            
                {
                  text: 'Received : ' + this.mySummary.saleSummary.amtReceviedOrPaid,alignment:'center',color:'green',fontSize : 15,bold:true
                },
                {
                  text: 'Remaining : ' + this.mySummary.saleSummary.amtPending,alignment:'center',color:'red',fontSize : 15,bold:true
                }
              ]
            },
            {
              alignment: 'center',
              columns: [
                {
                  text: 'PURCHASE : ' + this.mySummary.purchaseSummary.amtTotal*(-1), bold:true ,alignment:'center' , fontSize : 15
                },            
                {
                  text: 'Paid : ' + this.mySummary.purchaseSummary.amtReceviedOrPaid*(-1),alignment:'center',color:'green',fontSize : 15,bold:true
                },
                {
                  text: 'Pending : ' + this.mySummary.purchaseSummary.amtPending*(-1),alignment:'center',color:'red',fontSize : 15,bold:true
                }
              ]
            },            
            '\n',
            displayPartyList(this.partyList)
        ]
    }
 
    let pdfObj = pdfMake.createPdf(dd);
    let date = today.getDate();
    let month = today.getMonth()+1;
    let year = today.getFullYear();
    let pdfName = 'Party List'+'_'+date+'_'+month+'_'+year+'.pdf';
    console.log('month : '+today.getMonth());
    //console.log("DD-MM-YYYY: " + this.datepipe.transform(this.pSummary.bizTrxSummary[0].bizTransaction.trxDueDate,'dd-MM-yyyy'))
    if (this.plt.is('cordova')) {
      pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory,pdfName, blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory+ pdfName, 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      pdfObj.download();
   }
  }

  openSettings(){
    const modal = this.modalCtrl.create('AppSettingsPage',{appSettings:this.appSettings});
    modal.present();    
  }

  buildPartySummary()
  {
    console.log("Inside buildPartySummary");
    for(let i=0;i<this.partyList.length;i++)
    {
      getPartySummary(this.databaseProvider,this.partyList[i]);
    }
    //setTimeout(3000);
    console.log("buildPartySummary:End");
  }

  calculateMySummary()
  {

    this.mySummary.saleSummary.Currency='';
    this.mySummary.saleSummary.amtTotal=0;
    this.mySummary.saleSummary.amtReceviedOrPaid=0;
    this.mySummary.saleSummary.amtPending=0;
    this.mySummary.saleSummary.interestRealized=0;
    this.mySummary.saleSummary.interestUnRealized=0;

    this.mySummary.purchaseSummary.Currency='';
    this.mySummary.purchaseSummary.amtTotal=0;
    this.mySummary.purchaseSummary.amtReceviedOrPaid=0;
    this.mySummary.purchaseSummary.amtPending=0;
    this.mySummary.purchaseSummary.interestRealized=0;
    this.mySummary.purchaseSummary.interestUnRealized=0;

    for(let i=0;i<this.partyList.length;i++)
    {
      if(this.partyList[i].party.category === Global.partyCategory.BUYER)
      {
      this.mySummary.saleSummary.amtTotal += this.partyList[i].businessAmtTotalinLocal;
      this.mySummary.saleSummary.amtReceviedOrPaid += this.partyList[i].amtReceviedOrPaidinLocal;
      this.mySummary.saleSummary.amtPending += this.partyList[i].amtPendinginLocal;
      this.mySummary.saleSummary.interestRealized += this.partyList[i].interestRealizedinLocal;
      this.mySummary.saleSummary.interestUnRealized += this.partyList[i].interestUnRealizedinLocal;
      }
      else if(this.partyList[i].party.category === Global.partyCategory.SUPPLIER)
      {
      this.mySummary.purchaseSummary.amtTotal += this.partyList[i].businessAmtTotalinLocal;
      this.mySummary.purchaseSummary.amtReceviedOrPaid += this.partyList[i].amtReceviedOrPaidinLocal;
      this.mySummary.purchaseSummary.amtPending += this.partyList[i].amtPendinginLocal;
      this.mySummary.purchaseSummary.interestRealized += this.partyList[i].interestRealizedinLocal;
      this.mySummary.purchaseSummary.interestUnRealized += this.partyList[i].interestUnRealizedinLocal;      
      }     
    }    
  }

  editParty(p:partyProfile){    // HB27/1/2018
    console.log('Edit pressed');
    let id = p.ID;
    let modal = this.modalCtrl.create('AddPartyPage',{ID:id});
    modal.present();
    modal.onDidDismiss((data) => {
      this.loadPartyDetails();
    });
  }  // HB27/1/2018

  deleteParty(p:partyProfile){    // HB28/1/2018
    console.log('Delete pressed');
    let name = p.fullName;
    let id = p.ID;
    console.log("Party ID : " + id);
    let confirm = this.alertCtrl.create({
      title: 'Delete ' + name,
      message: 'Do you want to delete ' + name + '?',
      buttons: [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          handler: () => {
            this.databaseProvider.deleteSelectedParty(id).then(res=>{
              this.loadPartyDetails();
            })
          }
        }
      ]
    });
    confirm.present();
  }    // HB28/1/2018

  editOrDeletePartyProfile(p:partyProfile){          // HB27/1/2018
    let name = p.fullName;
    let prompt = this.alertCtrl.create({
      title: name,
      buttons: [
        {
          text: 'Edit',
          handler : () => {
            this.editParty(p);
          }
        },

        {
          text: 'delete',
          handler : () => {
            this.deleteParty(p);
          }
        }
      ]
    });
    prompt.present();
  }  // HB27/1/2018

  loadAppSettings() {
   
    this.databaseProvider.getAppSettings()
      .then(data => {
        this.appSettings = data;
        
      });    
  }
}
