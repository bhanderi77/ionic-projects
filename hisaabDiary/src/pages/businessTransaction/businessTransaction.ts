import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
// import { DateTime } from 'ionic-angular/components/datetime/datetime';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
// import { min } from 'rxjs/operator/min';
import { DatabaseProvider } from '../../providers/database/database';
import { AppSettings,businessTransaction,financeTransaction, Global, partyProfile} from '../../model/interface'
//import { FormBuilder, Validators } from '@angular/common'

/**
 * Generated class for the BusinessTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageBusinessTransaction',
  templateUrl: 'businessTransaction.html',
})
export class BusinessTransactionPage {

  trxnType: AbstractControl;
  trxnDate: AbstractControl;
  remarks: AbstractControl;
  wtInCts: AbstractControl;
  ratePerCts: AbstractControl; 
  discount: AbstractControl; 
  dueDays: AbstractControl;
  trxCurrencyRate: AbstractControl;
  trxCurrency: AbstractControl;
  taxChecked: AbstractControl;
  taxValue: AbstractControl;
  invoiceid: AbstractControl;

  taxAmount: number;
  lastBizTrxID: number;
  exchangeRate: number;
  CGSTValue: number;
  SGSTValue: number;
  IGSTValue: number;
  discountValue: number;
  discountAmount: number;

  party: partyProfile;
  invoiceIDValue:any;
  bizTrx: businessTransaction;
  editBizTrx: businessTransaction;
  appSettings:AppSettings;
  
  formGroup: FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public formBuilder: FormBuilder, private databaseProvider: DatabaseProvider) 
  {
    this.taxAmount = 0;
    this.exchangeRate = 1;
    this.party = navParams.get('party');  
    this.editBizTrx = navParams.get('trxSummary');
    this.appSettings = navParams.get('appSettings');
    // this.databaseProvider.getAppSettings()
    // .then(data => 
    // {
    //   this.appSetting = data; 
      if(this.party.category === "Supplier")
      {
        this.formGroup = formBuilder.group({
          trxnType:['Buy_Rough',Validators.required],
          wtInCts:['',Validators.compose([Validators.min(0),Validators.required])],     //,Validators.min(0),Validators.required
          ratePerCts:['',Validators.compose([Validators.min(0),Validators.required])],
          discount:['',Validators.compose([Validators.min(0),Validators.required])],  //RB1002
          trxnDate:['',Validators.required],
          dueDays:[this.appSettings.asDueDays,Validators.compose([Validators.min(0),Validators.required])],
          taxChecked:['false'],
          taxValue:[''],
          remarks:[''],
          invoiceid:[''],
          trxCurrency:['INR',Validators.required],  //RB2101
          trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])]
        });
      }
      else
      {
        this.formGroup = formBuilder.group({
          trxnType:['Sell_Polish',Validators.required],
          wtInCts:['',Validators.compose([Validators.min(0),Validators.required])],     //,Validators.min(0),Validators.required
          ratePerCts:['',Validators.compose([Validators.min(0),Validators.required])],
          discount:['',Validators.compose([Validators.min(0),Validators.required])],  //RB1002
          trxnDate:['',Validators.required],
          dueDays:[this.appSettings.asDueDays,Validators.compose([Validators.min(0),Validators.required])],
          taxChecked:['false'],
          taxValue:[''],
          remarks:[''],
          invoiceid:[''],
          trxCurrency:['INR',Validators.required],  //RB2101
          trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])]
        });
      }  
        
      this.trxnType = this.formGroup.controls['trxnType'];
      this.wtInCts = this.formGroup.controls['wtInCts'];
      this.ratePerCts = this.formGroup.controls['ratePerCts'];
      this.discount = this.formGroup.controls['discount'];
      this.trxnDate = this.formGroup.controls['trxnDate'];
      this.dueDays = this.formGroup.controls['dueDays'];
      this.taxChecked = this.formGroup.controls['taxChecked'];
      this.taxValue = this.formGroup.controls['taxValue'];
      this.remarks = this.formGroup.controls['remarks'];
      this.trxCurrency = this.formGroup.controls['trxCurrency'];
      this.trxCurrencyRate = this.formGroup.controls['trxCurrencyRate'];
      this.invoiceid = this.formGroup.controls['invoiceid'];
    //});//end of .then for getAppSetting

    this.bizTrx = {} as businessTransaction;
    
    this.bizTrx.trxPartyID = this.party.ID; 
    this.bizTrx.trxDate = new Date();
    console.log("BusinessTransactionPage for PartyID : " + this.bizTrx.trxPartyID + "and PartyCategory is : "+ this.party.category);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BusinessTransactionPage');
  }

  goToSummaryPage() {
    this.viewCtrl.dismiss();
    console.log("BusinessTransactionPage dismiss");
  }

  saveTransaction(){
    console.log("Inside businessTransaction::saveTransaction");
    if(this.setBusinessTransaction())
    {
      this.databaseProvider.addBusinessTransaction(this.bizTrx)
      .then(data => {
        console.log("Inserted: " + data);
       
        this.databaseProvider.getBusinessTrxID(this.bizTrx.trxPartyID,this.bizTrx.trxDate)
        .then(id => {
          console.log("add To FinanceTransaction against BizTrxID: " + id);
          let finTrx = {} as financeTransaction;

          finTrx.trxPartyID = this.bizTrx.trxPartyID;
          finTrx.BizTrxID = id;
          finTrx.trxDate = this.bizTrx.trxDate;
          if((this.bizTrx.trxType === "Import_Rough") || (this.bizTrx.trxType === "Buy_Rough") || (this.bizTrx.trxType === "Buy_Polish")){
            finTrx.trxType = Global.trxType.DUE_DEBIT;
          }
          else if((this.bizTrx.trxType === "Sell_Rough" )|| (this.bizTrx.trxType === "Sell_Polish")){
            finTrx.trxType = Global.trxType.DUE_CREDIT;
          }          
          else{
            finTrx.trxType = Global.trxType.MANUFACTURING;
          }

          if(this.bizTrx.trxCurrency === "USD")
          {
            finTrx.trxForiegnValue = this.bizTrx.trxValue + this.bizTrx.trxTax;
          }
          else  //if INR
          {
            finTrx.trxForiegnValue = ((this.bizTrx.trxValue/this.appSettings.asConversionRate) + (this.bizTrx.trxTax/this.appSettings.asConversionRate));
          }
          
          if(this.bizTrx.trxCurrency === "USD")
          {
            finTrx.trxLocalValue = (this.bizTrx.trxValue * this.bizTrx.trxCurrencyRate);
            
            finTrx.trxLocalValue = finTrx.trxLocalValue + (this.bizTrx.trxTax*this.bizTrx.trxCurrencyRate);
          }
          else  //if INR
          {
            finTrx.trxLocalValue = this.bizTrx.trxValue + this.bizTrx.trxTax;
          }
          
          finTrx.trxCurrency = this.bizTrx.trxCurrency;//Global.LocalCurrency; //get from app.settings
          finTrx.trxCurrencyRate = this.bizTrx.trxCurrencyRate; //get from app.settings        

          finTrx.trxInterestRealizedinLocal = 0;
          finTrx.trxInterestRealizedinForiegn = 0;
          finTrx.trxOpeningBalance=0;
          finTrx.trxClosingBalance=finTrx.trxLocalValue

          if(this.bizTrx.trxCurrency === "USD")
          {
            finTrx.trxClosingBalance=finTrx.trxLocalValue/this.bizTrx.trxCurrencyRate;
          }          
          else
          {
            finTrx.trxClosingBalance=finTrx.trxLocalValue
          }
          
          

          finTrx.trxRemarks = this.bizTrx.trxRemarks;

          console.log("TrxDate: " + finTrx.trxDate 
                      + "\n partyID:" + finTrx.trxPartyID
                      + "\n BizTrxID:" + finTrx.BizTrxID
                      + "\n trxDate:" + finTrx.trxDate
                      + "\n TrxType:" + finTrx.trxType
                      + "\n TrxValue: " +  finTrx.trxLocalValue 
                      + "\n trxCurrency: " +  finTrx.trxCurrency 
                      + "\n trxCurrencyRate: " +  finTrx.trxCurrencyRate 
                      + "\n trxInterestRealized:" + finTrx.trxInterestRealizedinLocal
                      + "\n trxInterestRealizedinForiegn:" + finTrx.trxInterestRealizedinForiegn
                      + "\n trxOpeningBalance: " +  finTrx.trxOpeningBalance 
                      + "\n trxClosingBalance: " +  finTrx.trxClosingBalance 
                      + "\n trxRemarks: " +  finTrx.trxRemarks 
                      );

          this.databaseProvider.addFinanceTransaction(finTrx)
          .then(data => {
            console.log("FinTrx inserted for BizTrxID: " + id);
            this.goToSummaryPage();  
          }).catch(e => console.log("Error from addFinanceTransaction:" + e));
        }).catch(e => console.log("Error from getBusinessTrxID:" + e));      
      }).catch(e => console.log("Error from addBusinessTransaction:" + e));
    }
  }

  setBusinessTransaction(): boolean
  {
    //trxID number //autogenerated
    //trxDate Date //systemdate or came from user
    /*let today: Date = new Date();
    console.log("Current Time is " + today.toTimeString());
    console.log("Before Update : trxDate  " + this.bizTrx.trxDate);
    */
   // this.bizTrx.trxDate.setMilliseconds(1);

    //this.bizTrx.trxDate.setTime(this.bizTrx.trxDueDate.getTime()+(today.getMilliseconds()));
    //console.log("After Update : trxDAte  " + this.bizTrx.trxDate);
    this.bizTrx.trxDate =  <Date> this.trxnDate.value;
    let bizTrxDate = new Date(this.bizTrx.trxDate);
    //trxType: string, //from user
    this.bizTrx.trxType = <string> this.trxnType.value;

    //trxPartyID: number, //set in constructor
    this.bizTrx.trxParentID=0; //autogenerated for child trx
    
    //Invoice ID
    if(this.party.category === "Supplier")
    {
      this.bizTrx.trxInvoiceID = <string> this.invoiceid.value;
    }
    else{
      //<string> this.invoiceid.value +'2017-18/'+ (this.lastBizTrxID + 1);
      //this.bizTrx.trxInvoiceID = <string> this.invoiceid.value +'2017-18/'+ (this.lastBizTrxID);trxnDateTime.getDate()+'-'+trxnDateTime.getMonth()+'-'+trxnDateTime.getFullYear()+
      let trxnDateTime = new Date();
      this.bizTrx.trxInvoiceID = <string> ('LS/'+ bizTrxDate.getDate() + '-' + (bizTrxDate.getMonth()+1) + '-' + bizTrxDate.getFullYear() + '/' +trxnDateTime.getHours()+':'+((trxnDateTime.getMinutes()<10?'0':'')+trxnDateTime.getMinutes())+':'+((trxnDateTime.getSeconds()<10?'0':'')+trxnDateTime.getSeconds()));
      console.log("INVOICE ID : " + this.bizTrx.trxInvoiceID);
      console.log("Date : " + bizTrxDate.toDateString());
      console.log("Date : " + bizTrxDate.toLocaleDateString());
      console.log("Time : " + trxnDateTime.toTimeString());
      console.log("Time : " + trxnDateTime.toLocaleTimeString());
      //this.bizTrx.trxInvoiceID = 
    }
    
    //trxQty: number, //from user
    this.bizTrx.trxQty = <number> parseFloat(this.wtInCts.value);

    //trxCurrency : string, //from user
    this.bizTrx.trxCurrency = <string> this.trxCurrency.value;
    //trxCurrencyRate : number, //from app settings or from user
    this.bizTrx.trxCurrencyRate = <number> parseFloat(this.trxCurrencyRate.value);

    //trxRate: number, //from user
    // if(this.bizTrx.trxCurrency === "USD")
    // {
      this.bizTrx.trxRate = <number> parseFloat(this.ratePerCts.value); //-(((<number> this.ratePerCts.value)*(<number> this.discount.value))/100)
    // }
      
    //trxDiscount: number, //from user
    this.bizTrx.trxDiscount = <number> parseFloat(this.discount.value);
    this.bizTrx.trxDiscountValue = this.discountValue;
    console.log("Discount Value : " + this.discountValue)
    //trxValue = QTY*RateperQty
    this.bizTrx.trxRate = (this.bizTrx.trxRate - (this.bizTrx.trxRate*this.bizTrx.trxDiscount)/100); //trxRate = TrxRate - Discount%
    this.bizTrx.trxValue = <number> Math.round(this.bizTrx.trxQty * this.bizTrx.trxRate);
    //this.bizTrx.trxValue = (this.bizTrx.trxValue-(this.bizTrx.trxValue*this.bizTrx.trxDiscount)/100);

    //trxTax = trxValue * GST //calculated below
    this.bizTrx.trxCGSTValue = this.CGSTValue;
    this.bizTrx.trxSGSTValue = this.SGSTValue;
    this.bizTrx.trxIGSTValue = this.IGSTValue;
    console.log("CGST Value : " + this.bizTrx.trxCGSTValue);
    console.log("SGST Value : " + this.bizTrx.trxSGSTValue);
    console.log("IGST Value : " + this.bizTrx.trxIGSTValue);
    this.bizTrx.trxTax = <number> parseFloat(this.taxValue.value);
    
    
    
    // trxDueDays: number, //from user
    this.bizTrx.trxDueDays = <number> parseInt(this.dueDays.value);

    //remark
    this.bizTrx.trxRemarks = <string> this.remarks.value;

    //<Date>
    this.bizTrx.trxDueDate = new Date(this.bizTrx.trxDate);
    this.bizTrx.trxDueDate.setTime(this.bizTrx.trxDueDate.getTime()+(this.bizTrx.trxDueDays*24*60*60*1000));
    console.log("DueDate: " + this.bizTrx.trxDueDate);
    console.log("DueDate.toLocaleString: " + this.bizTrx.trxDueDate.toLocaleString());
    console.log("DueDate.toLocaleDateString: " + this.bizTrx.trxDueDate.toLocaleDateString());
    console.log("DueDate.toISOString: " + this.bizTrx.trxDueDate.toISOString());
    console.log("DueDate.toJSON: " + this.bizTrx.trxDueDate.toJSON());
    
    this.bizTrx.trxROIForEarlyPayment=this.party.ROIForEarlyPayment; //from app settings
    this.bizTrx.trxROIForLatePayment=this.party.ROIForLatePayment;//from app settings
    this.bizTrx.trxStatus=Global.bizTrxStatus.OPEN; //'Open' or 'Closed'

    console.log("TrxDate: " + this.bizTrx.trxDate
                + "\n TrxType:" + this.bizTrx.trxType
                + "\n partyID:" + this.bizTrx.trxPartyID
                + "\n trxParentID:" + this.bizTrx.trxParentID
                + "\n trxQty:" + this.bizTrx.trxQty
                + "\n trxRate:" + this.bizTrx.trxRate
                + "\n TrxValue: " +  this.bizTrx.trxValue
                + "\n TrxValue: " +  this.bizTrx.trxTax 
                + "\n trxCurrency: " +  this.bizTrx.trxCurrency 
                + "\n trxCurrencyRate: " +  this.bizTrx.trxCurrencyRate 
                + "\n DueDays: " + this.bizTrx.trxDueDays
                + "\n DueDate: " + this.bizTrx.trxDueDate.toISOString()
                + "\n trxROIForEarlyPayment: " + this.bizTrx.trxROIForEarlyPayment 
                + "\n trxROIForLatePayment: " + this.bizTrx.trxROIForLatePayment 
                + "\n trxRemarks: " + this.bizTrx.trxRemarks 
                + "\n trxStatus: " + this.bizTrx.trxStatus 
                );
      return true;
  }

  // loadAppSettings() {
    
  //   this.databaseProvider.getAppSettings()
  //     .then(data => {
  //       this.appSetting = data;         
  //     });    
  // }


  isTaxChecked(){
    if(this.taxChecked.value === true)
    {
      if(this.party.stateRegion === "Maharashtra - (27)")
      {
        console.log("isTaxChecked:: stateRegion -> "+this.party.stateRegion);
        let trxAmount = Math.round(this.wtInCts.value * this.ratePerCts.value);
        let discount = this.discount.value;
        this.discountValue = ((trxAmount*discount)/100);
        this.discountAmount = (trxAmount-this.discountValue);
        this.CGSTValue = Math.round(this.discountAmount*this.appSettings.asCGST/100);
        this.SGSTValue = Math.round(this.discountAmount*this.appSettings.asSGST/100);
        console.log("isTaxChecked:: CGSTValue : " + this.CGSTValue);
        console.log("isTaxChecked:: SGSTValue : " + this.SGSTValue);
        this.taxAmount = (this.CGSTValue + this.SGSTValue);
        console.log("discount Amount Value : " + this.discountAmount);
        console.log("TAX Value : " + this.taxAmount);
        //this.bizTrx.trxTax = this.taxAmount;
        console.log("State Region : " +  this.party.stateRegion);
      }
      else
      {
        console.log("isTaxChecked:: stateRegion -> "+this.party.stateRegion);
        let trxAmount = Math.round(this.wtInCts.value * this.ratePerCts.value);
        let discount = this.discount.value;
        this.discountValue = ((trxAmount*discount)/100);
        this.discountAmount = (trxAmount-this.discountValue);
        this.IGSTValue = Math.round(this.discountAmount*this.appSettings.asIGST/100);
        console.log("isTaxChecked:: IGSTValue : " + this.IGSTValue);
        this.taxAmount = this.IGSTValue;
        //this.bizTrx.trxTax = this.taxAmount;
        console.log("discount Amount Value : " + this.discountAmount);
        console.log("TAX Value : " + this.taxAmount);
        console.log("State Region : " +  this.party.stateRegion);
      }
    }
    else{
      this.CGSTValue = 0;
      this.SGSTValue = 0;
      this.IGSTValue = 0;
      this.taxAmount = 0;
      console.log("TAX Value : " + this.taxAmount);
    }
  }

  isCurrencySelected()
  {
    if(this.trxCurrency.value === 'INR')
    {
      this.exchangeRate = 1;
    }
    else{
      this.exchangeRate = this.appSettings.asConversionRate;
    }
  }
}
