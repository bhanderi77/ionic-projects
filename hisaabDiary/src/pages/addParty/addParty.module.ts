import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddPartyPage } from './addparty';

@NgModule({
  declarations: [
    AddPartyPage,
  ],
  imports: [
    IonicPageModule.forChild(AddPartyPage),
  ],
})
export class AddPartyPageModule {}
