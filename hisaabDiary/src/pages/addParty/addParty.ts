import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
// import { min } from 'rxjs/operator/min';
// import { HomePage } from '../home/home';
import { partyProfile, States } from '../../model/interface';
/**
 * Generated class for the AddpartyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageAddParty',
  templateUrl: 'addParty.html',
})
export class AddPartyPage {

  formGroup: FormGroup;
  party: partyProfile;
  editParty : partyProfile; // HB27/1/2018
  fullName: AbstractControl;
  category: AbstractControl;
  phone1: AbstractControl;
  email1: AbstractControl;
  ROIForEarlyPayment: AbstractControl;
  ROIForLatePayment: AbstractControl;
  addressLine1: AbstractControl;
  addressLine2: AbstractControl;
  city: AbstractControl;
  pincode: AbstractControl;
  stateRegion: AbstractControl;
  GSTIN: AbstractControl;
  PAN: AbstractControl;
  searching : any = false;
  allStates: Array<States>;
  States = [];
  SearchInput : string = '';
  ID=0;
  

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private databaseProvider: DatabaseProvider, private viewCtrl: ViewController,
    public formBuilder: FormBuilder) {
      this.editParty={ID:0,fullName:'',category:'Buyer',phone_1:null,email_1:'',ROIForEarlyPayment:1.5,ROIForLatePayment:1.5,addressLine1:'',addressLine2:'',city:'',pincode:'',stateRegion:'',GSTIN:'',PAN:''}; // HB27/1/2018
      this.ID = navParams.get('ID');
      console.log("ID : " + this.ID);
      this.formGroup = formBuilder.group({
        fullName:['',Validators.required],
        category:['',Validators.required],
        phone1:['',Validators.compose([Validators.minLength(10),Validators.maxLength(10),Validators.required])],
        email1:[''],
        ROIForEarlyPayment:['',Validators.compose([Validators.min(0)])],
        ROIForLatePayment:['',Validators.compose([Validators.min(0)])],
        addressLine1:['',Validators.required],
        addressLine2:['',Validators.required],
        city:['',Validators.required],
        pincode:['',Validators.required],
        stateRegion:['',Validators.required], 
        GSTIN:['',Validators.required],
        PAN:['',Validators.required]      
      });

      
      this.fullName = this.formGroup.controls['fullName'];
      this.category = this.formGroup.controls['category'];
      this.phone1 = this.formGroup.controls['phone1'];
      this.email1 = this.formGroup.controls['email1'];
      this.ROIForEarlyPayment = this.formGroup.controls['ROIForEarlyPayment'];
      this.ROIForLatePayment = this.formGroup.controls['ROIForLatePayment'];
      this.addressLine1 = this.formGroup.controls['addressLine1'];
      this.addressLine2 = this.formGroup.controls['addressLine2'];
      this.city = this.formGroup.controls['city'];
      this.pincode = this.formGroup.controls['pincode'];
      this.stateRegion = this.formGroup.controls['stateRegion'];
      this.GSTIN = this.formGroup.controls['GSTIN'];
      this.PAN = this.formGroup.controls['PAN'];

      this.party = {} as partyProfile;
      if(this.ID != 0){ // HB27/1/2018
        databaseProvider.getSelectedPartyProfile(this.ID).then(data=>{
          this.editParty = data[0];
        });
      } // HB27/1/2018

      this.getStates();


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddpartyPage');
  }

  savePartyProfile() { // HB27/1/2018
    this.party.fullName = <string> (this.fullName.value);
    this.party.category = <string> (this.category.value);
    this.party.phone_1 = <string> (this.phone1.value);
    this.party.email_1 = <string> (this.email1.value);
    this.party.ROIForEarlyPayment = <number> (this.ROIForEarlyPayment.value);
    this.party.ROIForLatePayment = <number> (this.ROIForLatePayment.value);
    this.party.addressLine1 = <string> (this.addressLine1.value);
    this.party.addressLine2 = <string> (this.addressLine2.value);
    this.party.city = <string> (this.city.value);
    this.party.pincode = <string> (this.pincode.value);
    this.party.stateRegion = <string> (this.stateRegion.value);
    this.party.GSTIN = <string> (this.GSTIN.value);
    this.party.PAN = <string> (this.PAN.value);
    //console.log(//"party.addressLine1 : "+this.party.addressLine1
  // +"\n party.addressLine2 "+this.party.addressLine2
  // +"\n party.addressLine3 "+this.party.addressLine3
  //"\n party.partyState "+this.party.partyState);
  // +"\n party.GSTIN "+this.party.GSTIN
  // +"\n party.PAN "+this.party.PAN);
      console.log("Inside addPartyProfile: " + this.party.fullName);
      if(this.ID === 0){ 
        this.databaseProvider.addPartyProfile(this.party)
        .then(res => {
          console.log("PartyProfile Saved Successfully");
          this.viewCtrl.dismiss();
        });
      }
      else{
        this.databaseProvider.editPartyProfile(this.party,this.ID).then(res=>{
          console.log("PartyProfile edited successfully");
          this.viewCtrl.dismiss();
        })
      }
  } // HB27/1/2018

  getStates(){
    console.log("Inside getStates()");
    this.allStates = [];
    //let gstStates = [];
    this.databaseProvider.getAllStates()
    .then( data => {
      this.allStates = data;
      console.log("allStates[0] : " + this.allStates);
    });
  }

  goToRootPage(){
    this.viewCtrl.dismiss();
  }
}
