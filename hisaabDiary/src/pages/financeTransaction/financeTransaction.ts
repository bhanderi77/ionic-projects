import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, Validators, AbstractControl } from '@angular/forms';
// import { min } from 'rxjs/operator/min';

import { DatabaseProvider } from '../../providers/database/database';
import { AppSettings,businessTransaction,financeTransaction, Global, partyProfile } from '../../model/interface'

/**
 * Generated class for the FinanceTransactionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageFinanceTransaction',
  templateUrl: 'financeTransaction.html',
})
export class FinanceTransactionPage {

  trxnType: AbstractControl;
  currType: AbstractControl;
  trxCurrencyRate: AbstractControl;
  trxnDate: AbstractControl;
  amount: AbstractControl;
  //tax: AbstractControl;
  paymentMethod: AbstractControl;
  remarks: AbstractControl;
  formGroup: any;

  party: partyProfile;
  appSettings:AppSettings;

  myFinTrx : financeTransaction;
  editFinsTrx: financeTransaction;
  isNewTrxn:boolean;

  exchangeRate:number;
	
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, public formBuilder: FormBuilder, private databaseProvider: DatabaseProvider) {


      this.party = navParams.get('party');  //RB2101
      this.isNewTrxn = navParams.get('isNewTrxn');
      this.appSettings = navParams.get('appSettings');
      this.myFinTrx = {} as financeTransaction;
      console.log("isNewTrxn : " + this.isNewTrxn);
      this.editFinsTrx = {} as financeTransaction;
      if(this.isNewTrxn != true)
      {//edit trx
        console.log("isNewTrxn : " + this.isNewTrxn);
        this.editFinsTrx = navParams.get('finsTrx'); //RB1702
        console.log("EditFinsTrx ID : " + this.editFinsTrx.trxID
        +"\n EditFinsTrx.trxType : "+this.editFinsTrx.trxType
        +"\n EditFinsTrx.trxDate : "+this.editFinsTrx.trxDate
        +"\n EditFinsTrx.trxCurrency : "+this.editFinsTrx.trxCurrency
        +"\n EditFinsTrx.trxCurrencyRate : "+this.editFinsTrx.trxCurrencyRate
        +"\n EditFinsTrx.trxValue : "+this.editFinsTrx.trxLocalValue
        +"\n EditFinsTrx.trxPaymentMethod : "+this.editFinsTrx.trxPaymentMethod
        +"\n EditFinsTrx.trxRemarks : "+this.editFinsTrx.trxRemarks);
      }
    
      if(this.party.category === "Supplier")
      {
        this.editFinsTrx.trxType = 'Out_Debit';
        this.editFinsTrx.trxCurrency = 'INR';
        this.editFinsTrx.trxPaymentMethod = 'cheq';
        this.formGroup = formBuilder.group({
          trxnType:['Out_Credit',Validators.required],
          currType:['INR',Validators.required], //RB2101
          trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])],
          trxnDate:['',Validators.required],
          amount:['',Validators.compose([Validators.min(0),Validators.required])],
          //tax:['',Validators.compose([Validators.min(0),Validators.required])],
          paymentMethod:['cheq',Validators.required],
          remarks:['']
        });
      }
      else{
        this.editFinsTrx.trxType = 'In_Credit';
        this.editFinsTrx.trxCurrency = 'INR';
        this.editFinsTrx.trxPaymentMethod = 'cheq';
        this.formGroup = formBuilder.group({
          trxnType:['In_Credit',Validators.required],
          currType:['INR',Validators.required], //RB2101
          trxCurrencyRate:['',Validators.compose([Validators.min(0),Validators.required])],
          trxnDate:['',Validators.required],
          amount:['',Validators.compose([Validators.min(0),Validators.required])],
          //tax:['',Validators.compose([Validators.min(0),Validators.required])],
          paymentMethod:['cheq',Validators.required],
          remarks:['']
        });
      }
      this.trxnType = this.formGroup.controls['trxnType'];
      this.currType = this.formGroup.controls['currType'];
      this.trxCurrencyRate = this.formGroup.controls['trxCurrencyRate'];
      this.trxnDate = this.formGroup.controls['trxnDate'];
      this.amount = this.formGroup.controls['amount'];
      //this.tax = this.formGroup.controls['tax'];
      this.paymentMethod = this.formGroup.controls['paymentMethod'];
      this.remarks = this.formGroup.controls['remarks'];
      
      this.myFinTrx.trxPartyID = this.party.ID;
      //this.editFinsTrx.trxPartyID = this.party.ID;
      console.log("FinanceTransactionPage for PartyID : " + this.myFinTrx.trxPartyID);
   
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinanceTransactionPage');
  }

  goToSummaryPage() {
    this.viewCtrl.dismiss();
  }

  saveTransaction()
  {
    console.log("Inside financeTransaction::saveTransaction");
    let bizOpenTrxs = [<businessTransaction>{}];
    let finTrxsForPartyID = [<financeTransaction>{}];
    // let today: Date = new Date();

    this.myFinTrx.trxType = <string> this.trxnType.value;
    this.myFinTrx.trxCurrency = <string> this.currType.value;
    this.myFinTrx.trxCurrencyRate = <number> parseFloat(this.trxCurrencyRate.value);
    this.myFinTrx.trxDate = <Date> this.trxnDate.value;  //<Date>

    //if finTrxCurrency -> USD then foriegnValue is same as amount entered by user     
    if(this.myFinTrx.trxCurrency === "USD")
    {
      this.myFinTrx.trxForiegnValue = <number> parseFloat(this.amount.value);
    }
    else //finTrxCurrency is INR; store the same value in ForiegnValue since we don't have Exchange rate; so we will convert this value at runtime
    {
      this.myFinTrx.trxForiegnValue = <number> parseFloat(this.amount.value)/this.appSettings.asConversionRate;
    }

    /*if finTrxCurrency -> USD then localValue = amount/exchangeRate
      else localValue -> amount
    */
    if(this.myFinTrx.trxCurrency === "USD")
    {
      this.myFinTrx.trxLocalValue = <number> parseFloat(this.amount.value) * this.myFinTrx.trxCurrencyRate;  //this.myFinTrx.trxForiegnValue * this.myFinTrx.trxCurrencyRate;
    }
    else //finTrxCurrency -> INR
    {
      this.myFinTrx.trxLocalValue = <number> parseFloat(this.amount.value);
    }
      
      
    this.myFinTrx.trxPaymentMethod = <string> this.paymentMethod.value;
    this.myFinTrx.trxRemarks = <string> this.remarks.value;

    this.databaseProvider.getOpenBusinessTrxsForPartyID(this.myFinTrx.trxPartyID)
    .then(data => {
      bizOpenTrxs = data;
      console.log("No. of OPEN business Trxs are : " + bizOpenTrxs.length);
      if(bizOpenTrxs.length === 0)  //if openBizTrxn are 0; => all payments are done
      {
        alert("There are no pending amount to be settled");
        return;
      }
     
      this.databaseProvider.getFinanceTransactionsForPartyID(this.myFinTrx.trxPartyID)
        .then(data => {
          finTrxsForPartyID = data;
          //let bContinue: boolean = true;
          let bValidInput: boolean = false;
          let bCloseBizTrx: boolean = false;
          for(var i=0;((i<bizOpenTrxs.length) && (bizOpenTrxs[i].trxStatus == Global.bizTrxStatus.OPEN));i++) 
          {
            console.log("bizOpenTrxs[i].trxID: " + bizOpenTrxs[i].trxID);
            let finTrxsForBizTrxID = [<financeTransaction>{}];
            finTrxsForBizTrxID = finTrxsForPartyID.filter(financeTransaction=>financeTransaction.BizTrxID == bizOpenTrxs[i].trxID ); 
            let indexFinTrx: number =  finTrxsForBizTrxID.length-1;         
            if(finTrxsForBizTrxID[indexFinTrx].trxClosingBalance !== 0)
            {//This business trx is not yet closed, so adjust this payment against same
              
              let myFinTrxValue:number = 0;
              if(this.myFinTrx.trxCurrency === "USD")
              {
                myFinTrxValue = this.myFinTrx.trxForiegnValue;
              }
              else
              {
                myFinTrxValue = this.myFinTrx.trxLocalValue;
              }
              if(finTrxsForBizTrxID[indexFinTrx].trxClosingBalance < myFinTrxValue)
              {//Partial adjustment of input payment
                bValidInput = false;
                alert("Enter value less than $" + finTrxsForBizTrxID[indexFinTrx].trxClosingBalance);
                break;                
              }
              else
              {//Adjust entire input payment with this bizOpenTrxs[i]
                bValidInput = true;
                this.myFinTrx.BizTrxID=finTrxsForBizTrxID[indexFinTrx].BizTrxID;
                this.myFinTrx.trxOpeningBalance=finTrxsForBizTrxID[indexFinTrx].trxClosingBalance; //To be calculated

                if(this.myFinTrx.trxType === Global.trxType.IN_CREDIT)
                {
                  if(this.party.category === Global.partyCategory.BUYER)
                  {
                    this.myFinTrx.trxClosingBalance=finTrxsForBizTrxID[indexFinTrx].trxClosingBalance-(myFinTrxValue); //*this.myFinTrx.trxCurrencyRate
                    console.log("Buyer In :: USD to INR Closing Balance -> " + this.myFinTrx.trxClosingBalance);                    
                  }
                  else if(this.party.category === Global.partyCategory.SUPPLIER)
                  {
                    this.myFinTrx.trxClosingBalance=finTrxsForBizTrxID[indexFinTrx].trxClosingBalance+(myFinTrxValue); //*this.myFinTrx.trxCurrencyRate
                  }
                }
                else if(this.myFinTrx.trxType === Global.trxType.OUT_DEBIT)
                {
                  if(this.party.category === Global.partyCategory.BUYER)
                  {
                    this.myFinTrx.trxClosingBalance=finTrxsForBizTrxID[indexFinTrx].trxClosingBalance+(myFinTrxValue); //*this.myFinTrx.trxCurrencyRate
                  }
                  else if(this.party.category === Global.partyCategory.SUPPLIER)
                  {
                    this.myFinTrx.trxClosingBalance=finTrxsForBizTrxID[indexFinTrx].trxClosingBalance-(myFinTrxValue); //*this.myFinTrx.trxCurrencyRate
                  }
                }
                
                this.myFinTrx.trxInterestRealizedinLocal=0; //Reset to 0 as it need to be Calculated
                this.myFinTrx.trxInterestRealizedinForiegn=0;
                let trxDate: Date = (new Date(this.myFinTrx.trxDate));
                let dueDate: Date = (new Date(bizOpenTrxs[i].trxDueDate));
                let duration: number = trxDate.valueOf() - dueDate.valueOf();
                let diffDays = Math.floor(duration / (1000 * 3600 * 24)); 
                console.log("Calculating interest on trxDate: " + this.myFinTrx.trxDate + " -> " + trxDate.valueOf());
                console.log("DueDate: " + bizOpenTrxs[i].trxDueDate + " -> " + dueDate.valueOf());
                console.log("Diff: duration " + duration + " Days: " + diffDays);
                this.myFinTrx.trxDiffDays = diffDays;
                if(diffDays<0)
                {
                  this.myFinTrx.trxInterestRealizedinLocal = Math.round(((this.myFinTrx.trxLocalValue)*bizOpenTrxs[i].trxROIForEarlyPayment*diffDays*12)/36500);
                  this.myFinTrx.trxInterestRealizedinForiegn = Math.round(((this.myFinTrx.trxForiegnValue)*bizOpenTrxs[i].trxROIForEarlyPayment*diffDays*12)/36500);
                }
                else
                {
                  this.myFinTrx.trxInterestRealizedinLocal = Math.round(((this.myFinTrx.trxLocalValue)*bizOpenTrxs[i].trxROIForLatePayment*diffDays*12)/36500);
                  this.myFinTrx.trxInterestRealizedinForiegn = Math.round(((this.myFinTrx.trxForiegnValue)*bizOpenTrxs[i].trxROIForLatePayment*diffDays*12)/36500);
                }

                if(this.myFinTrx.trxClosingBalance === 0)
                {
                  bCloseBizTrx = true;
                }
              }              
            }
            break;
          }
          if(bValidInput)
          {
            this.databaseProvider.addFinanceTransaction(this.myFinTrx)
            .then(data => {
              console.log("Inserted: " + data);
              if(bCloseBizTrx === true)
              {
                this.databaseProvider.closeBusinessTransaction(bizOpenTrxs[i].trxPartyID,bizOpenTrxs[i].trxID)
                .then(res => {
                console.log("Closing BizTrx: " + bizOpenTrxs[i].trxID);
                });
              }
              this.navCtrl.pop();    
            }).catch(e => console.log("Error:this.databaseProvider.addFinanceTransaction " + e));
          }
        }).catch(e => console.log("Error:this.databaseProvider.getFinanceTransactionsForOpenBizTrxID " + e));
    });    
  }

  isTrxChanged()
  {
    if(this.currType.value === 'INR')
    {
      this.exchangeRate = 1;
    }
    else{
      this.exchangeRate = this.appSettings.asConversionRate;
    }
  }
}
