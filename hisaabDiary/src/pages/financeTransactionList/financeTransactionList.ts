import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { financeTransaction, partySummary, getPartySummary, Global, AppSettings, businessTransaction } from '../../model/interface';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { DatabaseProvider } from '../../providers/database/database';


/**
 * Generated class for the FinanceTransactionSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pagefinanceTransactionList',
  templateUrl: 'financeTransactionList.html',
})
export class FinanceTransactionListPage {

  //trxSummary: businessTrxSummary;
  pSummary:partySummary;
  bizTrx:businessTransaction;
  bizTrxID: number;
  trxCurrency: string;
  finTrxs:Array<financeTransaction>;
  appSettings:AppSettings;
  
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl:AlertController,
    private databaseProvider: DatabaseProvider) {
    //this.trxSummary = {} as businessTrxSummary;

    this.bizTrxID = navParams.get('bizTrxID');
    this.bizTrx = navParams.get('bizTrx');
    this.pSummary = {} as partySummary;
    this.pSummary = navParams.get('pSummary');
    this.appSettings = navParams.get('appSettings');
    this.trxCurrency = 'INR';
    this.finTrxs = [];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FinanceTransactionSummaryPage');
    getPartySummary(this.databaseProvider,this.pSummary);
  }

  ionViewDidEnter(){
    console.log('ionViewDidEnter FinanceTransactionSummaryPage');
    //getPartySummary(this.databaseProvider,this.pSummary);
    this.databaseProvider.getFinanceTransactionsForBizTrxID(this.pSummary.party.ID,this.bizTrx.trxID)
    .then( data => {
      this.finTrxs = data;
      console.log()
    });
  }


  deleteFinanceTransaction(index: number){
    
    console.log("Inside PartySummary::deleteFinanceTransaction()");
    console.log("this.finTrxs["+index+"] : "+this.finTrxs[index].trxID);
    let selectedRec = 0;
    for(let i=0;i<this.finTrxs.length;i++)
    {
      if(this.finTrxs[index].trxID == this.finTrxs[i].trxID)
      {
        selectedRec = i;
      }
    }

    //Delete DB record
    //alert for deleting selected finance transaction

    let confirm = this.alertCtrl.create(
    {
      title: 'Delete Entry of Rs.' + this.finTrxs[index].trxForiegnValue,
      message: 'Do you want to delete this transaction dated ' + this.finTrxs[index].trxDate + '?',
      buttons: [
        {text: 'No'},
        {
          text: 'Yes',
          handler: () => {
            
            this.databaseProvider.deleteSelectedFinanceTransaction(this.finTrxs[index].trxID)
            .then(res => {
              console.log("Selected Finance Trx Deleted : " + res);
              let previousRec=0;
              console.log("trxSummary.finTrxs.length : "+this.finTrxs.length);
              for(let i=selectedRec+1;i < this.finTrxs.length;i++)
              {
                if(i===selectedRec+1)
                  previousRec = i-2;
                else
                  previousRec = i-1;      
                  this.finTrxs[i].trxOpeningBalance = this.finTrxs[previousRec].trxClosingBalance;
                  this.finTrxs[i].trxClosingBalance = this.finTrxs[i].trxOpeningBalance - (this.finTrxs[i].trxForiegnValue);
                console.log("trxSummary.finTrxs["+i+"].trxOpeningBalance : "+this.finTrxs[i].trxOpeningBalance);
                console.log("trxSummary.finTrxs["+i+"].trxClosingBalance : "+this.finTrxs[i].trxClosingBalance);
                //Update DB Record
                this.databaseProvider.updateFinanceTransaction(this.finTrxs[i].trxID,this.finTrxs[i].trxOpeningBalance,this.finTrxs[i].trxClosingBalance)
                .then(res => {
                  console.log("Finance Trx Updated : " + res);
                });
              }
              console.log("After Deletion and Updation trxSummary.finTrxs.length : "+this.finTrxs.length);
              console.log("trxSummary.finTrxs[trxSummary.finTrxs.length].trxClosingBalance : "+this.finTrxs[this.finTrxs.length-1].trxClosingBalance);
              if(this.finTrxs[this.finTrxs.length-1].trxClosingBalance!=0)
              {
                //OpenBizTrxn getBizTrxnId and update its status as Open
                this.databaseProvider.updateBizTrxnStatus(this.finTrxs[this.finTrxs.length-1].BizTrxID,Global.bizTrxStatus.OPEN)
                .then(res => {
                  console.log("Business Trx Status Updated : " + res);
                })
              }
              setTimeout(1000);
              this.ionViewDidEnter();
            });    
          }
        }
      ]
    });    
    confirm.present();
  }
}
