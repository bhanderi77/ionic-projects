import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinanceTransactionListPage } from './financeTransactionList';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    FinanceTransactionListPage,
  ],
  imports: [
    IonicPageModule.forChild(FinanceTransactionListPage),
    PipesModule
  ],
})
export class FinanceTransactionSummaryPageModule {}
