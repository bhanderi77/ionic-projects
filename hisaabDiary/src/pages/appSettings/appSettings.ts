import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { AppSettings } from '../../model/interface'
// import { DatafinderProvider } from '../../providers/datafinder/datafinder';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
/**
 * Generated class for the AppSettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageAppSettings',
  templateUrl: 'appSettings.html',
})
export class AppSettingsPage {

  
  formGroup: FormGroup;
  foriegnCurrency: AbstractControl;
  localCurrency: AbstractControl;
  conversionRate: AbstractControl;
  dueDays: AbstractControl;
  CGST: AbstractControl;
  SGST: AbstractControl;
  IGST: AbstractControl;
  
  editAppSettings: AppSettings;
  appSettings: AppSettings;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController, private databaseProvider: DatabaseProvider,
    public formBuilder: FormBuilder) {

      this.editAppSettings = {asForiegnCurrency:'',asLocalCurrency:'',asConversionRate:null,asDueDays:null,asCGST:null,asSGST:null,asIGST:null};
      this.formGroup = formBuilder.group({
        foriegnCurrency:['',Validators.required],
        localCurrency:['',Validators.required],
        conversionRate:['',Validators.required],
        dueDays:['',Validators.required],
        CGST:['',Validators.required],
        SGST:['',Validators.required],
        IGST:['',Validators.required]
      });
  
      this.foriegnCurrency = this.formGroup.controls['foriegnCurrency'];
      this.localCurrency = this.formGroup.controls['localCurrency'];
      this.conversionRate = this.formGroup.controls['conversionRate'];
      this.dueDays = this.formGroup.controls['dueDays'];
      this.CGST = this.formGroup.controls['CGST'];
      this.SGST = this.formGroup.controls['SGST'];
      this.IGST = this.formGroup.controls['IGST'];

      this.appSettings = {} as AppSettings;
      this.databaseProvider.getDatabaseState().subscribe(rdy => {
        console.log("inside HomePage::constructor -> rdy " + rdy);
        if (rdy) {
          this.loadAppSettings();
        }
      });
  }

  loadAppSettings() {
    this.databaseProvider.getAppSettings()
      .then(data => {
        this.editAppSettings = data;
      });
  }
  goToHomePage(){
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    
  }


  updateAppSettings(){
   
    this.appSettings.asForiegnCurrency = <string> this.foriegnCurrency.value;
    this.appSettings.asLocalCurrency = <string> this.localCurrency.value;
    this.appSettings.asConversionRate = <number> this.conversionRate.value;
    this.appSettings.asDueDays = <number> this.dueDays.value;
    this.appSettings.asCGST = <number> this.CGST.value;
    this.appSettings.asSGST = <number> this.SGST.value;
    this.appSettings.asIGST = <number> this.IGST.value;
    this.databaseProvider.updateAppSettings(this.appSettings).then(res => {
      this.goToHomePage();
    });
    
  }
}
