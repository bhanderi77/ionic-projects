import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { businessTrxSummary, getPartySummary, partySummary, AppSettings, number2text } from '../../model/interface';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { DatabaseProvider } from '../../providers/database/database';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { ModalController } from 'ionic-angular/components/modal/modal-controller';
import { DatePipe } from '@angular/common';
import pdfMake from 'pdfmake/build/pdfmake';
// import pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Platform } from 'ionic-angular/platform/platform';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the PartySummaryPopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'pageBusinessTransactionPopover',
  templateUrl: 'businessTransactionPopover.html',
})
export class BusinessTransactionPopoverPage {

  trxSummary: businessTrxSummary;
  pSummary = <partySummary>{};

  appSettings: AppSettings;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl:AlertController,
    private databaseProvider: DatabaseProvider,
    private viewCtrl: ViewController,
    public modalCtrl: ModalController,
    private plt: Platform,
    private file: File,
    private fileOpener: FileOpener,
    public datepipe: DatePipe,
    private socialSharing: SocialSharing) 
  {
    this.trxSummary = this.navParams.get('trxSummary');
    this.pSummary = this.navParams.get('pSummary');
    this.appSettings = this.navParams.get('appSettings');
    console.log("PopOverPage::trxSummary => "+this.trxSummary);
    console.log("PopOverPage::pSummary => "+this.trxSummary);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartySummaryPopoverPage');
  }


  deleteBusinessTransaction()
  {
    this.viewCtrl.dismiss();
     console.log("Inside partySummaryPopover:deleteBusinessTransaction() " + this.trxSummary.bizTransaction.trxPartyID + this.trxSummary.bizTransaction.trxID);
    let confirm = this.alertCtrl.create(
      {
        title: 'Delete ' + this.trxSummary.bizTransaction.trxQty + 'cts x ' + this.trxSummary.bizTransaction.trxRate + this.trxSummary.bizTransaction.trxCurrency,
        message: 'Do you want to delete this transaction dated ' + this.trxSummary.bizTransaction.trxDate + '?',
        buttons: [
          {text: 'No'},
          {
            text: 'Yes',
            handler: () => {
              this.databaseProvider.deleteBusinessTransaction(this.trxSummary.bizTransaction.trxPartyID, this.trxSummary.bizTransaction.trxID)
              .then(res => {
                console.log("Business Trx Deleted : " + res);
                this.databaseProvider.deleteFinanceTransaction(this.trxSummary.bizTransaction.trxPartyID, this.trxSummary.bizTransaction.trxID)
                .then(res => {
                  console.log("Finance Trx Deleted");
                  getPartySummary(this.databaseProvider,this.pSummary);
                });
              });
            }
          }
        ]
      });
    confirm.present();
  }

  compilemsg(index):string{
    var msg = "Hello!!!" ;
    return msg.concat(" \n -Sent from my Awesome App !");
  }

  editBusinessTransaction(){
    this.viewCtrl.dismiss();
    // let modal = this.modalCtrl.create('BusinessTransactionPage',{trxSummary:this.trxSummary});
    // modal.present();
    // console.log("Inside editBusinessTransaction()");
    //whatsappShare(index){
      let index=1;
      var msg  = this.compilemsg(index);
       this.socialSharing.shareViaWhatsApp(msg, null, null);
     //}
  }

  generateInvoice(){
    console.log("Inside partySummaryPopOver::generateInvoice()");
    let datepipe:DatePipe = new DatePipe('en-US');
    let dd = {
      // a string or { width: number, height: number } 
        //pageSize: 'A4',

        // by default we use portrait, you can change it to landscape if you wish 
        //pageOrientation: 'landscape',

        // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins 
        pageMargins: [ 20, 100, 20, 0 ],

        //Pdf Content starts here...
        content: [
            // '\n',
            // '\n',
            // '\n',
            // '\n',
            // '\n',
            // '\n',
            {text: 'TAX INVOICE', bold:true , alignment:'center' , fontSize : 15 },
            '\n',
            '\n',
            {
              columns:[
                {text: 'INV NO: ' + this.trxSummary.bizTransaction.trxInvoiceID, bold:true , alignment:'left' , fontSize : 11},
                '\t',      
                {text: 'Date: ' + datepipe.transform(this.trxSummary.bizTransaction.trxDate,'dd-MM-yyyy'), bold:true , alignment:'left' , fontSize : 11},
              ]
            },
            '\n',
            '\n',
            {
              columns:[
                {text: 'M/S. ' + this.pSummary.party.fullName, bold:true , alignment:'left' , fontSize : 11},
                // '\n',
                // {text: this.pSummary.party.addressLine1, bold:true , alignment:'center' , fontSize : 11},
                // '\n',
                // {text: this.pSummary.party.addressLine2, bold:true , alignment:'center' , fontSize : 11},
                // '\n',
                // {text: this.pSummary.party.city, bold:true , alignment:'center' , fontSize : 11},
                '\t',      
                {text: 'TERMS: ' + this.trxSummary.bizTransaction.trxDueDays, bold:true , alignment:'left' , fontSize : 11},
              ]
            },
            {
              columns:[
                {text: this.pSummary.party.addressLine1, bold:false , alignment:'left' , fontSize : 11},
                // '\n',
                // {text: this.pSummary.party.addressLine2, bold:true , alignment:'center' , fontSize : 11},
                // '\n',
                // {text: this.pSummary.party.city, bold:true , alignment:'center' , fontSize : 11},
                '\t',      
                {text: 'OUR BANKERS: ' , bold:true , alignment:'left' , fontSize : 11},
              ]
            },
            {
              columns:[
                {text: this.pSummary.party.addressLine2, bold:false , alignment:'left' , fontSize : 11},
                // '\n',
                // {text: this.pSummary.party.city, bold:true , alignment:'center' , fontSize : 11},
                '\t',      
                {text: 'DCB BANK LTD' , bold:false , alignment:'left' , fontSize : 11},
              ]
            }, 
            {
              columns:[
                {text: this.pSummary.party.city + '-' + this.pSummary.party.pincode, bold:false , alignment:'left' , fontSize : 11},
                '\t',      
                {text: 'DAHISAR(007)BRANCH' , bold:false , alignment:'left' , fontSize : 11},
              ]
            },
            {
              columns:[
                {text: '' , bold:false , alignment:'left' , fontSize : 11},
                '\t',      
                {text: 'MUMBAI-400068' , bold:false , alignment:'left' , fontSize : 11},
              ]
            },
            {
              columns:[
                '\t',
                '\t',      
                {text: 'CURRENT A/C No: 00742600000055' , bold:true , alignment:'left' , fontSize : 15},
              ]
            },
            {
              columns:[
                {text: 'State Code: ' + this.pSummary.party.stateRegion, bold:true , alignment:'left' , fontSize : 11},
                '\t',      
                {text: 'IFSC Code: DCBL0000007' , bold:true , alignment:'left' , fontSize : 11},
              ]
            },
            {
              columns:[
                {text: 'GSTIN: ' + this.pSummary.party.GSTIN, bold:true , alignment:'left' , fontSize : 11},
                '\t',      
                {text: 'GSTIN: ' + this.pSummary.party.GSTIN, bold:true , alignment:'left' , fontSize : 11},
              ]
            },
            {
              columns:[
                {text: 'PAN: ' + this.pSummary.party.PAN, bold:true , alignment:'left' , fontSize : 11},
                '\t',      
                {text: 'PAN: ' + this.pSummary.party.PAN, bold:true , alignment:'left' , fontSize : 11},
              ]
            },
            {
              columns:[
                {text: 'Place Of Supply: ' + this.pSummary.party.city, bold:true , alignment:'left' , fontSize : 11}
              ]
            },
            '\n',
            '\n',
            {
              alignment: 'center',
              table: {
                  headerRows: 0,
                  widths:['10%','20%','15%','15%','20%','20%'],
                  body: 
                  [
                    //buildBusinessTrxBody(),
                    [
                      {text : 'Sr. No.', fontSize : 11, bold:true},
                      {text : 'DESCRIPTION OF GOODS',fontSize : 11, bold:true},
                      {text : 'HSN CODE', fontSize : 11,bold:true},
                      {text : 'WEIGHT IN CTS', fontSize : 11,bold:true},
                      {text : 'RATE PER CTS', fontSize : 11,bold:true},
                      {text : 'AMOUNT', fontSize : 11,bold:true},
                     ],
                     [
                      {text : '1', fontSize : 11, bold:true},
                      {text : 'CUT & POLISHED DIAMONDS',fontSize : 11, bold:true},
                      {text : '71023910', fontSize : 11,bold:true},
                      {text :this.trxSummary.bizTransaction.trxQty, fontSize : 11,bold:true},
                      {text : Number(this.trxSummary.bizTransaction.trxRate).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:2,maximumFractionDigits:2 }), fontSize : 11,bold:true},
                      {text : Number(this.trxSummary.bizTransaction.trxValue).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:2,maximumFractionDigits:2 }), fontSize : 11,bold:true},
                     ],
                     /*[
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                     ],
                     [
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                     ],
                     [
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                     ],*/
                     [
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : 'TOTAL', fontSize : 11,bold:true},
                      {text : this.trxSummary.bizTransaction.trxQty, fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                     ],
                     [
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : 'CGST @ '+this.appSettings.asCGST + '%', fontSize : 11,bold:true},
                      {text : Number(this.trxSummary.bizTransaction.trxCGSTValue).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:2,maximumFractionDigits:2 }), fontSize : 11,bold:true},
                     ],
                     [
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : 'SGST @ '+this.appSettings.asSGST + '%', fontSize : 11,bold:true},
                      {text : Number(this.trxSummary.bizTransaction.trxSGSTValue).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:2,maximumFractionDigits:2 }), fontSize : 11,bold:true},
                     ],
                     [
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : 'Total Tax', fontSize : 11,bold:true},
                      {text : Number(this.trxSummary.bizTransaction.trxTax).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:2,maximumFractionDigits:2 }), fontSize : 11,bold:true},
                     ],
                     [
                      {text : '', fontSize : 11, bold:true},
                      {text : '',fontSize : 11, bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : 'Grand Total', fontSize : 11,bold:true},
                      {text : '', fontSize : 11,bold:true},
                      {text : Number(this.trxSummary.finTrxs[0].trxLocalValue).toLocaleString('en-IN',{ style: 'currency', currency: "INR",minimumFractionDigits:2,maximumFractionDigits:2 }), fontSize : 11,bold:true},
                     ]
                  ]
                  
              },
            },
            {
              table: {
                  alignment: 'center',
                  headerRows: 0,
                  widths:['100%'],
                  body: 
                  [
                    [
                      {text : 'TOTAL AMOUNT : ' + number2text(this.trxSummary.finTrxs[0].trxForiegnValue),alignment:'center', fontSize : 11,bold:true},
                    ]
                  ]
  
                }
              // columns:[
              //   {text: 'Place Of Supply: ' + this.pSummary.party.city, bold:true , alignment:'left' , fontSize : 11}
              // ]
            },
            {
              columns:[
                  {text: 'PAYMENT INSTRUCTIONS:  ', bold:true , alignment:'left' , fontSize : 11}
                ]
            },
            {
              columns:[
                  {text: 'PLEASE PAY IN FAVOUR OF :-"KAVYA EXPORTS" DCB BANK LTD ,BRANCH :- DAHISAR(007)BRANCH, MUMBAI-400068.', bold:true , alignment:'left' , fontSize : 11}
                ]
            },
            {
              columns:[
                  {text: 'CURRENT A/C.NO.: 00742600000055', bold:true , alignment:'left' , fontSize : 11},
                  '\t',
                  {text: 'IFSC CODE:- DCBL0000007', bold:true , alignment:'left' , fontSize : 11}
                ]
            },
            '\n',
            '\n',
            {
              columns:[
                  {text: '“I/we hereby certify that my/our registration Certificate under GST Tax Act, 2017 is in force on the date on which sale of goods specified in this tax invoice is made by me/us and that the transaction of sale covered by this tax invoice has been effected by me/us and it shall be accounted for in the turnover of sales while filing of return and the due tax, if any, payable on the sale has been paid or shall be paid. ”', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: 'The diamonds herein invoiced have been purchased from legitimate sources not involved in funding conflict and in complete with United Nations resolutions. The seller hereby guarantee that these diamonds are conflict free based on personal knowledge and/ or written guarantees provided by the supplier of these diamonds.', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: 'The diamonds herein invoiced are natural diamonds. We guarantee the originality of the diamonds supplied are free from synthetic or treated diamonds.', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: 'To  the  best  of  our  knowledge  and/or  written  assurance  from  our  supplier, we  state that "Diamonds invoiced have not been otained in violation of applicable National Laws and/or sanctioned by the US Department of Treasury'+"'s"+'Office of Foreign Assets Control (OFAC) and have not Originated from the Marange Resources of Zimbabawe.', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: 'The diamonds herein invoiced are exclusively of natural origin and untreated based on personal knowledgeand/or written guarantees provided by the supplier of these diamonds.', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: '"The acceptance of goods herein invoiced will be as per WFDB guidelines."', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: 'SUBJECT TO MUMBAI JURISDICATION.', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: 'GOODS SOLD AND DELIVERY AT MUMBAI', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            {
              columns:[
                  {text: 'INTEREST AT 18% PER YEAR WILL BE CHARGED ON ALL A/C REMAINING UNPAID AFTER DUE DATE.', bold:true , alignment:'left' , fontSize : 6}
                ]
            },
            '\n',
            '\n',
            {
              columns:[
                  {text: 'CUSTOMERS SIGN  & SEAL', bold:true , alignment:'left' , fontSize : 10},
                  '\t',
                  {text: 'FOR KAVYA EXPORTS', bold:true , alignment:'center' , fontSize : 10}
                ]
            },
            '\n',
            '\n',
            '\n',
            '\n',
            {
              columns:[
                  {text: '________________', bold:true , alignment:'left' , fontSize : 10},
                  '\t',
                  {text: 'PARTNER', bold:true , alignment:'center' , fontSize : 10}
                ]
            }
          ]
      }

      // function buildBusinessTrxBody()
      // {
      //   let body = [];
      //   let columns = ['Sr. No.', 'DESCRIPTION OF GOODS','HSN CODE','WEIGHT IN CTS','RATE PER CTS','AMOUNT'];
      //   let datarow = [];
      // }
 
    let pdfObj = pdfMake.createPdf(dd);
    let pdfName =this.trxSummary.bizTransaction.trxID + " - " + this.pSummary.party.fullName+'.pdf';
    console.log("pdfName is : " + pdfName);
    //console.log("DD-MM-YYYY: " + this.datepipe.transform(this.pSummary.bizTrxSummary[0].bizTransaction.trxDueDate,'dd-MM-yyyy'))
    if (this.plt.is('cordova')) {
      pdfObj.getBuffer((buffer) => {
        var blob = new Blob([buffer], { type: 'application/pdf' });
 
        // Save the PDF to the data Directory of our App
        this.file.writeFile(this.file.dataDirectory,pdfName, blob, { replace: true }).then(fileEntry => {
          // Open the PDf with the correct OS tools
          this.fileOpener.open(this.file.dataDirectory+ pdfName, 'application/pdf');
        })
      });
    } else {
      // On a browser simply use download!
      pdfObj.download();
   }
  }
}
