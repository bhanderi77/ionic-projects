import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PartySummaryPage } from './partySummary';
import { PipesModule } from '../../pipes/pipes.module'

@NgModule({
  declarations: [
    PartySummaryPage,
  ],
  imports: [
    IonicPageModule.forChild(PartySummaryPage),
    PipesModule
  ],
})
export class SummaryPageModule {}
