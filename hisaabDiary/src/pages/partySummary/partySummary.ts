import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController,Platform} from 'ionic-angular';
import { DatePipe } from '@angular/common';
// import { FinanceTransactionPage } from '../financeTransaction/financeTransaction';
// import { BusinessTransactionPage } from '../businessTransaction/businessTransaction';

import { DatabaseProvider } from '../../providers/database/database';
import { AppSettings,businessTransaction,partySummary, Global, partyProfile,getPartySummary } from '../../model/interface';

import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { PopoverController } from 'ionic-angular/components/popover/popover-controller';
// import { BusinessTransactionPopoverPage } from '../businessTransactionPopover/businessTransactionPopover'
// import { FinanceTransactionListPage } from '../financeTransactionList/financeTransactionList';

/**
 * Generated class for the SummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

pdfMake.vfs = pdfFonts.pdfMake.vfs;

@IonicPage()
@Component({
  selector: 'pagePartySummary',
  templateUrl: 'partySummary.html',
})
export class PartySummaryPage {

  pSummary = <partySummary>{};
  pdfObj = null;
  trxCurrency: string;
  // iDueDays: number;
  // foriegnCurrency: string;
  // localCurrency: string;
  // conversionRate: number;
  // rateOfInterest: number;
  // CGST: number;
  // IGST: number;
  // SGST: number;
  exchangeRate: number;
  appSettings: AppSettings;
  
	
  constructor(public navCtrl: NavController, public navParams: NavParams,
     public modalCtrl: ModalController,
     private databaseProvider: DatabaseProvider,
     private plt: Platform,
     private file: File,
     private fileOpener: FileOpener,
     public datepipe: DatePipe,
     public popOverCtrl: PopoverController)
  {      
    this.trxCurrency = 'INR'
    this.pSummary = navParams.get("pSummary");
    this.appSettings = navParams.get('appSettings');
  }

  ionViewDidLoad() {
    console.log('SummaryPage::ionViewDidLoad : PartyName : '+ this.pSummary.party.fullName);
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter partySummaryPage');
    getPartySummary(this.databaseProvider,this.pSummary);  
  }

  addFinanceTransaction() {
    const modal = this.modalCtrl.create('FinanceTransactionPage',{party: 
      this.pSummary.party,isNewTrxn:true,appSettings:this.appSettings}); //RB2101
    modal.present();
    console.log("inside SummaryPage::addFinanceTrxn()");
    modal.onDidDismiss((data) => {
      getPartySummary(this.databaseProvider,this.pSummary);  
    });
  }

  addBusinessTransaction() {
    const modal = this.modalCtrl.create('BusinessTransactionPage',{party: 
                  this.pSummary.party,appSettings:this.appSettings}); //RB2101
    modal.present();
    console.log("inside SummaryPage::addBusinessTrxn() : pSummary.partyID => " + this.pSummary.party.ID);
    modal.onDidDismiss(data => {
      console.log('addBusinessTrnx::onDidDismiss()');
      getPartySummary(this.databaseProvider,this.pSummary);  
    });
  }

   
  
  

  openPopOver(event,trxSummary){
    let popover = this.popOverCtrl.create('BusinessTransactionPopoverPage',{trxSummary: trxSummary,pSummary:this.pSummary,appSettings:this.appSettings});
    popover.present({
      ev: event
    });
  }

  downloadPdf(){
   let today = new Date();

  //  function buildTableBody(data, columns) {
  //    let body = [];
  //    console.log("data length " + data.length);
  //    console.log("column length " + columns.length);

  //    body.push(columns);

  //    data.forEach(function(row) {
  //        var dataRow = [];

  //        columns.forEach(function(column) {
  //            dataRow.push(row[column]);
  //        })

  //        body.push(dataRow);
  //    });

  //    return body;
  //  }

  //  function table(data, columns) {
  //      return {
  //          alignment: 'center',
  //          table: {
  //              headerRows: 1,
  //              widths:['40%','20%','20%','20%'],
  //              body: buildTableBody(data, columns),fillColor:'black'
  //          },
  //          layout: {
  //            fillColor: function (i, node) {
  //              return (i  === 0) ? '#CCCCCC' : null;
  //            }
  //          }
  //      };
  //  }

   function displayBusinessTrxHeader1(businessTrxSummary) {
      let datepipe:DatePipe = new DatePipe('en-US'); 
      return {
           alignment: 'center',
           table: {
            headerRows: 0,
            widths:['40%','20%','40%'],
            body: [
              [
              {text : datepipe.transform(businessTrxSummary.bizTransaction.trxDate,'dd-MM-yyyy'), color:'white',bold:true, fontSize : 15},
              {text : businessTrxSummary.bizTransaction.trxDueDays + ' days' , color:'white',bold:true, fontSize : 15},
              {text : datepipe.transform(businessTrxSummary.bizTransaction.trxDueDate,'dd-MM-yyyy'), color:'white', bold:true, fontSize : 15}
              ]
            ],fillColor:'black'
            },
            layout: {
              fillColor: function (i, node) {
                return (i  === 0) ? '#212121' : null;
              }
            }           
       };
   }

   function displayBusinessTrxHeader2(businessTrxSummary) {
    return {
        alignment: 'center',
        table: {
         headerRows: 0,
         widths:['*'],
         body: [
           [
             {text : businessTrxSummary.bizTransaction.trxQty + ' cts x ' + businessTrxSummary.bizTransaction.trxRate + ' ' + businessTrxSummary.bizTransaction.trxCurrency, color:'white', bold:true, fontSize : 15}
           ]
          ]
         },
         layout: {
           fillColor: function (i, node) {
             return (i  === 0) ? '#212121' : null;
           }
         }           
    };
   }


   function displayBusinessTrxFooter(businessTrxSummary) {
    return {
        alignment: 'center',
        table: {
         headerRows: 0,
         widths:['34%','33%','33%'],
         body: [
           [
            {text : businessTrxSummary.trxLocalAmount, fontSize : 15, bold:true, italic:true},
            {text : businessTrxSummary.amtReceviedOrPaidinLocal, color:'green', fontSize : 15, bold:true, italic:true},
            {text : businessTrxSummary.amtPendinginLocal, color:'red', fontSize : 15,bold:true, italic:true}
           ],
           [
            {text : 'Interest ==> ', alignment:'right', fontSize : 15, italic:true},
            {text : '('+ businessTrxSummary.interestRealizedinLocal +')', color:'black',fontSize:15,italic:true},
            {text : '('+ businessTrxSummary.interestUnRealizedinLocal +')', color:'black',fontSize : 15,italic:true}
           ]
          ],fillColor:'red'
         },
         layout: {
           fillColor: function (i, node) {
             return (true) ? '#CCCCCC' : null;
           }
         }
    };
  }
   function displayTransactions(businessTrxSummary,partyProfile){
      let recs = [];
      for(var i=0 ; i < businessTrxSummary.length ; i++){        
        recs.push(displayBusinessTrxHeader1(businessTrxSummary[i]));        
        recs.push(displayBusinessTrxHeader2(businessTrxSummary[i]));                
        recs.push('\n');
        recs.push(displayFinanceTrx(businessTrxSummary[i].finTrxs,partyProfile));
        recs.push('\n');
        recs.push(displayBusinessTrxFooter(businessTrxSummary[i]));
        recs.push('\n\n');
      }
      return recs;
   }
  
   function displayFinanceTrx(finTrxs,partyProfile){
    return {
      alignment: 'center',
      table: {
          headerRows: 0,
          widths:['20%','20%','20%','20%','20%'],
          body: buildFinanceBody(finTrxs,partyProfile),fillColor:'black'
      },
    };
   }

   function buildFinanceBody(finTrxs,partyProfile) {
    let body = [];
    // let columns = ['Date', 'Type','Amount','Interest','Remarks']
    let datepipe:DatePipe = new DatePipe('en-US');
    console.log("PDF:No. of Fin Trx " + finTrxs.length);
    
    //body.push(columns);

    finTrxs.forEach(function(row) {
        let dataRow = [];
        dataRow.push(datepipe.transform(row.trxDate,'dd-MM-yyyy'));
        console.log("PDF: Category - " + partyProfile.category + " trxType - " + row.trxType);
        if(partyProfile.category === Global.partyCategory.BUYER)
        {
          if(row.trxType === Global.trxType.IN_CREDIT)
          {  dataRow.push("R (-)"); }
          else if (row.trxType === Global.trxType.OUT_DEBIT)
          { dataRow.push("P (+)");  }
          else if ((row.trxType === Global.trxType.DUE_DEBIT) || (row.trxType === Global.trxType.DUE_CREDIT))
          { dataRow.push("Due"); }
        }
        else if(partyProfile.category === Global.partyCategory.SUPPLIER)
        {
          if(row.trxType === Global.trxType.IN_CREDIT)
          {  dataRow.push("P (+)"); }
          else if (row.trxType === Global.trxType.OUT_DEBIT)
          { dataRow.push("R (-)");  }
          else if ((row.trxType === Global.trxType.DUE_DEBIT) || (row.trxType === Global.trxType.DUE_CREDIT))
          { dataRow.push("Due");}
        }
        dataRow.push(row.trxLocalValue);
        dataRow.push(row.trxInterestRealizedinLocal); 
        dataRow.push(row.trxRemarks); 
        body.push(dataRow);
    });
    return body;
  }

  // let datepipe:DatePipe = new DatePipe('en-US');
    let dd = {
       content: [
           {text: this.pSummary.party.fullName, bold:true ,alignment:'center' , fontSize : 20 },
           '\n',
           {text: 'Summary as on : ' + this.pSummary.sysDate.getDate()+'-'+(this.pSummary.sysDate.getMonth()+1)+'-'+this.pSummary.sysDate.getFullYear(), italic:true,alignment:'center' , fontSize : 10},
           //{text: 'Summary as on : ' + datepipe.transform(this.pSummary.sysDate.toISOString(),'medium'), italic:true,alignment:'center' , fontSize : 10},
           '\n',
           {text: 'Total : ' + this.pSummary.businessCurrency + " " + this.pSummary.businessAmtTotalinLocal, bold:true ,alignment:'center' , fontSize : 15},
           '\n',
           {
             alignment: 'center',
             columns: [
               {
                 text: 'Received : ' + this.pSummary.amtReceviedOrPaidinLocal,alignment:'center',color:'green',fontSize : 15,bold:true
               },
               {
                 text: 'Pending : ' + this.pSummary.amtPendinginLocal,alignment:'center',color:'red',fontSize : 15,bold:true
               }
             ]
           },
           {
             alignment: 'center',
             columns: [
               {
                 text: 'Interest on Received : ' + this.pSummary.interestRealizedinLocal,alignment:'center',color:'black',fontSize : 15,bold:true
               },
               {
                 text: 'Interest on Remaining : ' + this.pSummary.interestUnRealizedinLocal, color:'black', fontSize : 15,bold:true
               }
             ]
           },
           '\n',
           displayTransactions(this.pSummary.bizTrxSummary,this.pSummary.party)
       ]
    }

    this.pdfObj = pdfMake.createPdf(dd);
    let date = today.getDate();
    let month = today.getMonth()+1;
    let year = today.getFullYear();
    let pdfName = this.pSummary.party.fullName+'_'+date+'_'+month+'_'+year+'.pdf';
    console.log('month : '+today.getMonth());
    //console.log("DD-MM-YYYY: " + this.datepipe.transform(this.pSummary.bizTrxSummary[0].bizTransaction.trxDueDate,'dd-MM-yyyy'))
    if (this.plt.is('cordova')) {
     this.pdfObj.getBuffer((buffer) => {
       var blob = new Blob([buffer], { type: 'application/pdf' });

       // Save the PDF to the data Directory of our App
       this.file.writeFile(this.file.dataDirectory,pdfName, blob, { replace: true }).then(fileEntry => {
         // Open the PDf with the correct OS tools
         this.fileOpener.open(this.file.dataDirectory+ pdfName, 'application/pdf');
       })
     });
    } 
    else
    {
     // On a browser simply use download!
      this.pdfObj.download();
    }
 }

  gotoFinanceTrxList(bizTrx:businessTransaction,pSummary:partyProfile){
    this.navCtrl.push('FinanceTransactionListPage',{bizTrx: bizTrx,pSummary:pSummary,appSettings:this.appSettings}); //,{finTrxSummary: finTrxSummary}
  }

  
}
