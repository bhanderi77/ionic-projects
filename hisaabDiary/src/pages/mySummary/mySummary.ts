import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MySummary } from '../../model/interface'


/**
 * Generated class for the MySummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mySummary',
  templateUrl: 'mySummary.html',
})
export class MySummaryPage {
  public mySummary:MySummary;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.mySummary = navParams.get("mySummary");
    console.log("Inside MySummaryPage constructor");
    console.log("this.mySummary.saleSummary.amtTotal: " + this.mySummary.saleSummary.amtTotal);
    console.log("this.mySummary.saleSummary.amtReceviedOrPaid: " + this.mySummary.saleSummary.amtReceviedOrPaid);
    console.log("this.mySummary.saleSummary.amtPending: " + this.mySummary.saleSummary.amtPending);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MySummaryPage');
  }

}
