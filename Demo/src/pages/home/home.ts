import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  public barChartOptions1:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
            xAxes: [{stacked: true}],
            yAxes: [{stacked: true}]
    }
  };

  //Chart Labels
  public barChartLabels1:string[] = ['Input'];
  public barChartType1:string = 'horizontalBar';
  public barChartLegend1:boolean=true;
 
  //Chart data
  public barChartData1:any[] = [
    {data: [66], label: 'Pending'},
    {data: [29], label: 'In progress'}
  ];


  public barChartOptions2:any = {
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
            xAxes: [{stacked: true}],
            yAxes: [{stacked: true}]
    }
  };

  //Chart Labels
  public barChartLabels2:string[] = ['Output'];
  public barChartType2:string = 'horizontalBar';
  public barChartLegend2:boolean=true;
 
  //Chart data
  public barChartData2:any[] = [
    {data: [50], label: 'Completed'},
    {data: [45], label: 'In progress'}
  ];


}
