import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private dummyData;

  constructor(public navCtrl: NavController) {
    this.dummyData = [{"name":"hardik", "r_no": "06" , "city":"pune" ,"expand":false},
    {"name":"rashmin", "r_no": "08" , "city":"mumbai" , "expand":false},
    {"name":"brinda", "r_no": "04" , "city":"surat" , "expand":false }];
  }
  expand(detail){
    detail.expand = !detail.expand;
  }
}
