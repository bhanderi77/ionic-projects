export interface FarmerDetails {
	ID: string;
	name: string;
	crops: Array<{ name: string }>;
	city: string;
	state: string;
	phoneNumber: string;
	landSize: number;
}

export interface FpoDetails {
	ID: string;
	name: string;
	locality: string;
	crops: Array<{ name: string }>;
	phoneNumber: string;
	farmers: Array<FarmerDetails>;
	numberOfFarmers:number;
}