export interface User {
    ID: string;
    name: string;
    email: string;
    role: string;
    isAdmin: boolean;
}