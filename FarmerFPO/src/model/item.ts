import { TaxRate, TaxAmount } from "./tax";


export interface SalesItem {
    ID: string;
    itemName: string;
    itemType: string;
    itemDescription: string;
    itemPrice: number;
    itemUOM: string;
    itemQuantity: number;
    imageURL: string;
    reservedQuantity: number;
    totalAmount:number;
    taxRate:TaxRate;
    taxAmount:TaxAmount;
}

