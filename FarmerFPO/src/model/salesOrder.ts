import { SalesItem } from "./item";
import { BusinessContact } from "./contact";
import { TaxAmount } from "./tax";

export interface SalesOrder {
    ID: string;
    creation_time:string;
    creation_date:string;
    salesItems: Array<SalesItem>;
    sellerContactDetails: BusinessContact;
    buyerContactDetails: BusinessContact;
    // cartTotal: number;
    orderStatus: string;
    orderAmount:number;
    grandTotal:number;
    taxAmount: TaxAmount;
    // taxRateCGST:number; 
    // taxRateSGST:number;
    // taxRateIGST:number;
    // taxAmountCGST:number;
    // taxAmountSGST:number;
    // taxAmountIGST:number;
    // public constructor() {
    //     this.initialize();
    // }

    // initialize() {
    //     this.ID = "";
    //     this.salesItems = [];
    //     this.sellerContactDetails = {} as SellerContact;
    //     this.buyerContactDetails = {} as BuyerContact;
    //     this.sellerContactDetails.ID = "";
    //     this.sellerContactDetails.email = "";
    //     this.sellerContactDetails.phoneNumber = "";
    //     this.cartTotal = 0;

    //     this.buyerContactDetails.firstName = "";
    //     this.buyerContactDetails.lastName = "";
    //     this.buyerContactDetails.ID = "";
    //     this.buyerContactDetails.addressLine1 = "";
    //     this.buyerContactDetails.addressLine2 = "";
    //     this.buyerContactDetails.city = "";
    //     this.buyerContactDetails.email = "";
    //     this.buyerContactDetails.phoneNumber = "";
    //     this.buyerContactDetails.pinCode = "";
    // }

    // copy(salesOrder: SalesOrder) {
    //     this.ID = salesOrder.ID;
    //     this.salesItems = salesOrder.salesItems;
    //     this.sellerContactDetails = salesOrder.sellerContactDetails;
    //     this.buyerContactDetails = salesOrder.buyerContactDetails;
    // }

    // clear() {
    //     // this.ID = "";
    //     // this.salesItems = [];
    //     // console.log("inside clear() salesitem length " + this.salesItems.length);
    //     this.initialize();
    // }

    // calculateCartTotal() {
    //     console.log("Inside calculateCartTotal()");
    //     let total: number = 0;
    //     for (let i = 0; i < this.salesItems.length; i++) {
    //         let sum = this.salesItems[i].itemQuantity * this.salesItems[i].itemPrice;
    //         total += sum;
    //     }
    //     return total;
    // }

    // addItemToSalesOrder(salesItem: SalesItem, quantity: number) {
    //     let tempSalesItem = {} as SalesItem;
    //     tempSalesItem.ID = salesItem.ID;
    //     tempSalesItem.itemPrice = salesItem.itemPrice;
    //     tempSalesItem.itemName = salesItem.itemName;
    //     tempSalesItem.itemType = salesItem.itemType;
    //     tempSalesItem.itemUOM = salesItem.itemUOM;
    //     tempSalesItem.itemQuantity = quantity;
    //     this.salesItems.push(tempSalesItem);
    // }

    // removeItemFromSalesOrder(index: number) {
    //     this.salesItems.splice(index, 1);
    // }

    // updateItemFromSalesOrder(index: number, quantity: number) {
    //     this.salesItems[index].itemQuantity = quantity;
    // }

}