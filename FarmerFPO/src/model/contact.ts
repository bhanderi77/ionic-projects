export interface BusinessContact {
    ID: string;
    firstName: string;
    lastName: string;
    phoneNumber: string;
    email: string;
    addressLine1: string;
    addressLine2: string;
    city: string;
    state: string;
    pinCode: string;
}
