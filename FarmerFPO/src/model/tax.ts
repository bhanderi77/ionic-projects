export interface TaxRate{
    rateCGST:number;
    rateSGST:number;
    rateIGST:number;
}

export interface TaxAmount{
    amountCGST:number;
    amountSGST:number;
    amountIGST:number;
}