export namespace myConstants {

    export const enum orderStatus {
        NEW = "New",
        IN_PROGRESS = "In Progress",
        DISPATCHED = "Dispatched",
        DELIVERED = "Delivered"
    }

}