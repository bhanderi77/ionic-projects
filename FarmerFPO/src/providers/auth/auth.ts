// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform, LoadingController } from 'ionic-angular';
import { AngularFirestore, DocumentReference } from 'angularfire2/firestore';
import { GooglePlus } from '@ionic-native/google-plus/';
import * as firebase from 'firebase/app';
import { User } from '../../model/user';

@Injectable()
export class AuthProvider {

	private firebaseUser: firebase.User;

	constructor(private afs: AngularFirestore,
		private platform: Platform,
		public loadingController: LoadingController,
		private gplus: GooglePlus) {
		console.log('Hello AuthProvider Provider');
		firebase.auth().onAuthStateChanged(user => {
			console.log("inside onAuthStateChanged");
			if (user) {
				this.firebaseUser = user;
				console.log(this.firebaseUser);
				console.log("this.firebaseUser is set " + user.email);
			}
			else {
				this.firebaseUser = null;
				console.log("this.firebaseUser is null");
			}
		});
	}

	async getUser(): Promise<User> {
		console.log("Welcome to AuthProvider::getUser()");
		let currentUser = {} as User;
		currentUser.ID = "";
		currentUser.isAdmin = false;
		currentUser.name = "";
		currentUser.email = "";
		currentUser.role = "buyer";
		await this.loginGoogle();

		if (this.firebaseUser) {
			let colRef = this.afs.collection<User>(`/UserList/`).ref;
			const query = colRef.where('email', '==', this.firebaseUser.email);
			return query.get()
				.then(async (querySnapshots) => { 
					if(querySnapshots.size === 0){
						let userID = this.firebaseUser.uid;
						let docRef: DocumentReference = this.afs.doc(`/UserList/${userID}`).ref;
						await docRef.set({
							ID: userID,
							name: this.firebaseUser.displayName,
							email:this.firebaseUser.email,
							role:"buyer",
							isAdmin:false
						});
						currentUser.ID = userID;
						currentUser.name = this.firebaseUser.displayName;
						currentUser.email = this.firebaseUser.email;
						currentUser.role = "buyer";
						currentUser.isAdmin = false;
					}
					else{
						currentUser.ID = <string> querySnapshots.docs[0].data().ID;
						currentUser.email = <string> querySnapshots.docs[0].data().email;
						currentUser.name = <string> querySnapshots.docs[0].data().name;
						currentUser.role = <string> querySnapshots.docs[0].data().role;
						currentUser.isAdmin = <boolean> querySnapshots.docs[0].data().isAdmin;
					}
					return currentUser;
				})
		}
	}

	async loginGoogle(): Promise<void> {
		console.log("Welcome to AuthProvider::loginGoogle()");
		if (this.firebaseUser) {
			console.log(this.firebaseUser.email + " already logged in ");
			//   this.logoutGoogle();
			//   return;
		}

		if (!this.firebaseUser) {
			console.log(" No user has logged in...invoking login flow ");
			if (this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova')) {
				try {
					console.log("inside try of cordova");
					const gplusUser = await this.gplus.login({
						'webClientId': '761464317602-hqoj8sq1kb3o8gvt1ls7hui8kves2tjd.apps.googleusercontent.com',
						'offline': false,
						'scopes': 'profile email'
					})
					const loading = this.loadingController.create({ content: "Logging in....." });
					console.log("Invoking signInAndRetrieveDataWithCredential");
					const userCredentials = await firebase.auth().signInAndRetrieveDataWithCredential(firebase.auth.GoogleAuthProvider.credential(gplusUser.idToken));
					console.log("completed signInAndRetrieveDataWithCredential");
					//.then(userCredentials => {
					console.log("userCredentials.user.phoneNumber : " + userCredentials.user.phoneNumber);
					console.log("userCredentials.user.displayName " + userCredentials.user.displayName);
					console.log("userCredentials.user.uid " + userCredentials.user.uid);
					loading.dismissAll();
					// });
					// this.gplus.login({
					// 	'webClientId': '761464317602-hqoj8sq1kb3o8gvt1ls7hui8kves2tjd.apps.googleusercontent.com',
					// 	'offline': true
					// }).then(res => console.log(res))
					// 	.catch(err => console.error(err));
				}
				catch (err) {
					console.log("Error in Native flow  signInAndRetrieveDataWithCredential " + err);
				}
			}
			else {
				try {
					const provider = new firebase.auth.GoogleAuthProvider();

					const credential = await firebase.auth().signInWithPopup(provider);
					console.log("credential.user.displayName " + credential.user.displayName);
					console.log("credential.user.uid " + credential.user.uid);

				} catch (err) {
					console.log("Error in Web flow signInWithPopup " + err);
				}
			}
		}
	}

	async logoutGoogle() {
		console.log("Welcome to AuthProvider::logoutGoogle");

		try {
			await firebase.auth().signOut();
			if (this.platform.is('android') || this.platform.is('ios') || this.platform.is('cordova')) {
				await this.gplus.logout();
			}
			else {

			}
		} catch (e) {
			console.log("Error from firebase.auth().signOut " + e);
		}
	}

}
