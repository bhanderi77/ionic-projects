import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore'
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { DocumentReference, WriteBatch } from '@firebase/firestore-types';
import { SalesItem } from '../../model/item';
import { SalesOrder } from '../../model/salesOrder';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { State } from '../../model/state';

@Injectable()
export class FirebaseProvider {

    constructor(private afs: AngularFirestore) {
        console.log('Hello FirebaseProvider Provider');

    }

    getFpoList(): AngularFirestoreCollection<FpoDetails> {
        return this.afs.collection<FpoDetails>(`/FpoList`, ref => ref.orderBy('name'));
    }

    getItemsList(): AngularFirestoreCollection<SalesItem> {
        return this.afs.collection<SalesItem>(`/SalesItemList`, ref => ref.orderBy('itemName'));
    }

    getFarmerList(fpoID: string): AngularFirestoreCollection<FarmerDetails> {
        return this.afs.collection<FarmerDetails>(`/FpoList/${fpoID}/FarmerList`, ref => ref.orderBy('name'));
    }

    getSalesOrderList(): AngularFirestoreCollection<SalesOrder> {
        return this.afs.collection<SalesOrder>(`/SalesOrderList`, ref => ref.orderBy('buyerContactDetails.firstName'));
    }

    getStates(): AngularFirestoreCollection<State> {
        return this.afs.collection<State>(`/Settings/20190216/StateList`, ref => ref.orderBy('ID'));
    }

    async createFpo(fpo: FpoDetails) {
        console.log("Entering createFpo()");
        let fpoID = this.afs.createId();
        let docRef: DocumentReference = this.afs.doc(`/FpoList/${fpoID}`).ref;
        if (docRef) {
            return docRef.set({
                ID: fpoID,
                name: fpo.name,
                locality: fpo.locality,
                phoneNumber: fpo.phoneNumber,
                crops:fpo.crops
            })
        }
        console.log("Leaving createFpo()");
    }

    async createFarmer(fpoID: string, farmer: FarmerDetails): Promise<void> {
        let farmerID = this.afs.createId();
        let docRef: DocumentReference = this.afs.doc(`/FpoList/${fpoID}/FarmerList/${farmerID}`).ref;
        if (docRef) {
            return docRef.set({
                ID: farmerID,
                name: farmer.name,
                city: farmer.city,
                crops: farmer.crops,
                state: farmer.state,
                phoneNumber: farmer.phoneNumber,
                landSize: farmer.landSize
            });
        }
        return null;
    }

    // async addOrder(salesOrder: SalesOrder) {
    // 	let orderID = salesOrder.buyerContactDetails.firstName + '_' + salesOrder.buyerContactDetails.lastName + '_' + this.afs.createId();
    // 	let docRef: DocumentReference = this.afs.doc(`/SalesOrderList/${orderID}`).ref;
    // 	if (docRef) {
    // 		return docRef.set({
    // 			ID: orderID,    
    // 			salesItems: salesOrder.salesItems,
    // 			sellerContactDetails: salesOrder.sellerContactDetails,
    // 			buyerContactDetails: salesOrder.buyerContactDetails
    // 		})
    // 	}
    // }

    addItem(item: SalesItem): Promise<boolean> {
        console.log("Entering addItem()");
        let itemID = item.itemType + "_" + item.itemName + "_" + this.afs.createId();
        return new Promise((resolve, reject) => {
            this.uploadImage(item, itemID).then(() => {
                let imageRef = firebase.storage().ref().child('saletems').child(itemID + '.png');
                console.log("image ref inside addItem() " + imageRef);
                console.log("item ID : " + itemID);
                let docRef: DocumentReference = this.afs.doc(`/SalesItemList/${itemID}`).ref;
                if (docRef) {
                    imageRef.getDownloadURL().then((url) => {
                        console.log("URL inside addItem() uploadImage() :" + item.imageURL);
                        item.imageURL = url;
                        console.log("URL inside addItem() uploadImage() :" + item.imageURL);
                        docRef.set({
                            ID: itemID,
                            itemName: item.itemName,
                            itemType: item.itemType,
                            itemPrice: item.itemPrice,
                            itemUOM: item.itemUOM,
                            itemQuantity: item.itemQuantity,
                            imageURL: item.imageURL,
                            itemDescription: item.itemDescription,
                            reservedQuantity: item.reservedQuantity,
                            taxRate: item.taxRate
                        }).then(() => {
                            resolve(true);
                        }).catch(() => {
                            console.log("Error in docRef.set()");
                            resolve(false);
                        });
                    }).catch(() => {
                        console.log("Error in getDownloadURL()");
                        resolve(false);
                    });
                }
            }).catch(() => {
                console.log("Error in uploadImage()");
                resolve(false);
            });
        });
    }

    editItemDetails(item: SalesItem): Promise<boolean> {
        return new Promise((resolve, reject) => {
            let itemID = item.ID;
            let docRef: DocumentReference = this.afs.doc(`/SalesItemList/${itemID}`).ref;
            if (docRef) {
                docRef.update({
                    itemName: item.itemName,
                    itemType: item.itemType,
                    itemPrice: item.itemPrice,
                    itemUOM: item.itemUOM,
                    itemQuantity: item.itemQuantity,
                    itemDescription: item.itemDescription,
                    taxRate: item.taxRate
                }).then(() => {
                    resolve(true);
                }).catch(() => {
                    console.log("Error in editItemDetails() docRef.update()");
                });
            }
        });
    }

    editItemImage(item: SalesItem): Promise<boolean> {
        console.log("Entering editItemImage()");
        let itemID = item.ID;
        return new Promise((resolve, reject) => {
            this.uploadImage(item, itemID).then(() => {
                let imageRef = firebase.storage().ref().child('saletems').child(itemID + '.png');
                console.log("image ref inside editItemImage() " + imageRef);
                // let imageRef = item.imageReference;
                let docRef: DocumentReference = this.afs.doc(`/SalesItemList/${itemID}`).ref;
                if (docRef) {
                    imageRef.getDownloadURL().then((url) => {
                        console.log("URL inside uploadImage() :" + item.imageURL);
                        item.imageURL = url;
                        console.log("URL inside uploadImage() :" + item.imageURL);
                        docRef.update({
                            imageURL: item.imageURL
                        }).then(() => {
                            resolve(true);
                        }).catch(() => {
                            console.log("Error in docRef.set()");
                            resolve(false);
                        });
                    }).catch(() => {
                        console.log("Error in getDownloadURL()");
                        resolve(false);
                    });
                }
            }).catch(() => {
                console.log("Error in uploadImage()");
                resolve(false);
            });
        });
    }

    uploadImage(item: SalesItem, itemID: string) {
        console.log("inside uploadImage()");
        return new Promise<any>((resolve, reject) => {
            let storageRef = firebase.storage().ref();
            let ref = storageRef.child('saletems').child(itemID + '.png');
            console.log("image ref inside uploadImage() imageRef = false " + ref);
            ref.getDownloadURL().then((url) => {
                console.log("URL inside uploadImage() imageRef = false :" + url);
            })
            this.encodeImageUri(item.imageURL, function (image64) {
                console.log("this.encodeImageUri function ");
                ref.putString(image64, 'data_url')
                    .then(snapshot => {
                        resolve(snapshot.downloadURL);
                    }, err => {
                        reject(err);
                    })
            })
        })
    }

    encodeImageUri(imageUri, callback) {
        var c = document.createElement('canvas');
        var ctx = c.getContext("2d");
        var img = new Image();
        img.onload = function () {
            var aux: any = this;
            console.log("aux " + aux);
            c.width = aux.width;
            c.height = aux.height;
            ctx.drawImage(img, 0, 0);
            var dataURL = c.toDataURL("image/jpeg");
            console.log("dataURL" + dataURL);
            callback(dataURL);
        };
        img.src = imageUri;
    }

}
