import { Injectable } from '@angular/core';
import { FirebaseProvider } from '../firebase/firebase';
import { SalesOrder } from '../../model/salesOrder';
import { getDateTimeString, getDateString } from '../../model/utility';
import { SalesItem } from '../../model/item';
import { myConstants } from '../../model/myConstants'
import { BusinessContact } from "../../model/contact";
import { DocumentReference, WriteBatch } from '@firebase/firestore-types';
import { AngularFirestore, AngularFirestoreCollection, DocumentSnapshot } from 'angularfire2/firestore'
import { TaxAmount, TaxRate } from '../../model/tax';

@Injectable()
export class OrderProvider {

	public salesOrder: SalesOrder;

	constructor(private firebaseProvider: FirebaseProvider,
		private afs: AngularFirestore) {
		console.log('Hello OrderProvider Provider');
		this.salesOrder = {} as SalesOrder;
		this.initialize();
	}

	initialize() {
		this.salesOrder.ID = "";
		this.salesOrder.creation_time = "";
		this.salesOrder.creation_date = "";
		this.salesOrder.salesItems = [];
		this.salesOrder.orderStatus = "";
		this.salesOrder.orderAmount = 0;
		this.salesOrder.grandTotal = 0;

		this.salesOrder.sellerContactDetails = {} as BusinessContact;
		this.salesOrder.buyerContactDetails = {} as BusinessContact;
		this.salesOrder.taxAmount = {} as TaxAmount;

		this.salesOrder.taxAmount.amountCGST = 0;
		this.salesOrder.taxAmount.amountIGST = 0;
		this.salesOrder.taxAmount.amountSGST = 0;
		this.salesOrder.sellerContactDetails.ID = "";
		this.salesOrder.sellerContactDetails.firstName = "";
		this.salesOrder.sellerContactDetails.lastName = "";
		this.salesOrder.sellerContactDetails.addressLine1 = "";
		this.salesOrder.sellerContactDetails.addressLine2 = "";
		this.salesOrder.sellerContactDetails.city = "";
		this.salesOrder.sellerContactDetails.state = "";
		this.salesOrder.sellerContactDetails.email = "";
		this.salesOrder.sellerContactDetails.phoneNumber = "";
		this.salesOrder.sellerContactDetails.pinCode = "";

		this.salesOrder.buyerContactDetails.ID = "";
		this.salesOrder.buyerContactDetails.firstName = "";
		this.salesOrder.buyerContactDetails.lastName = "";
		this.salesOrder.buyerContactDetails.addressLine1 = "";
		this.salesOrder.buyerContactDetails.addressLine2 = "";
		this.salesOrder.buyerContactDetails.city = "";
		this.salesOrder.buyerContactDetails.state = "";
		this.salesOrder.buyerContactDetails.email = "";
		this.salesOrder.buyerContactDetails.phoneNumber = "";
		this.salesOrder.buyerContactDetails.pinCode = "";
	}

	copy(salesOrder: SalesOrder) {
		this.salesOrder.ID = salesOrder.ID;
		this.salesOrder.salesItems = salesOrder.salesItems;
		this.salesOrder.sellerContactDetails = salesOrder.sellerContactDetails;
		this.salesOrder.buyerContactDetails = salesOrder.buyerContactDetails;
		this.salesOrder.orderStatus = salesOrder.orderStatus;
		this.salesOrder.orderAmount = salesOrder.orderAmount;
		this.salesOrder.grandTotal = salesOrder.grandTotal;
		this.salesOrder.taxAmount.amountCGST = salesOrder.taxAmount.amountCGST;
		this.salesOrder.taxAmount.amountSGST = salesOrder.taxAmount.amountSGST;
		this.salesOrder.taxAmount.amountIGST = salesOrder.taxAmount.amountIGST;
	}

	calculateCartTotal() {
		console.log("Inside calculateCartTotal()");
		let totalTax = {} as TaxAmount;
		this.salesOrder.grandTotal = 0;
		this.salesOrder.orderAmount = 0;
		this.salesOrder.taxAmount.amountCGST = 0;
		this.salesOrder.taxAmount.amountSGST = 0;
		this.salesOrder.taxAmount.amountIGST = 0;

		for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
			this.salesOrder.salesItems[i].totalAmount = this.salesOrder.salesItems[i].itemQuantity * this.salesOrder.salesItems[i].itemPrice;
			console.log("this.salesOrder.salesItems[" + i + "] total amount : " + this.salesOrder.salesItems[i].totalAmount);
			this.salesOrder.orderAmount += this.salesOrder.salesItems[i].totalAmount;
			console.log("this.salesOrder.salesItems[" + i + "] : " + this.salesOrder.orderAmount);
			this.salesOrder.salesItems[i].taxAmount.amountCGST = (this.salesOrder.salesItems[i].totalAmount * this.salesOrder.salesItems[i].taxRate.rateCGST) / 100;
			console.log("this.salesOrder.salesItems[" + i + "] taxAmount.amountCGST : " + this.salesOrder.salesItems[i].taxAmount.amountCGST);
			this.salesOrder.salesItems[i].taxAmount.amountSGST = (this.salesOrder.salesItems[i].totalAmount * this.salesOrder.salesItems[i].taxRate.rateSGST) / 100;
			console.log("this.salesOrder.salesItems[" + i + "] taxAmount.amountSGST : " + this.salesOrder.salesItems[i].taxAmount.amountSGST);
			this.salesOrder.salesItems[i].taxAmount.amountIGST = (this.salesOrder.salesItems[i].totalAmount * this.salesOrder.salesItems[i].taxRate.rateIGST) / 100;
			console.log("this.salesOrder.salesItems[" + i + "] taxAmount.amountIGST : " + this.salesOrder.salesItems[i].taxAmount.amountIGST);
		}
		// this.calculateTax();
		if (this.salesOrder.buyerContactDetails.state === this.salesOrder.sellerContactDetails.state) {
			for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
				this.salesOrder.taxAmount.amountCGST += this.salesOrder.salesItems[i].taxAmount.amountCGST;
				console.log("this.salesOrder.taxAmount.amountCGST " + this.salesOrder.taxAmount.amountCGST);
				this.salesOrder.taxAmount.amountSGST += this.salesOrder.salesItems[i].taxAmount.amountSGST;
				console.log("this.salesOrder.taxAmount.amountSGST " + this.salesOrder.taxAmount.amountSGST);
			}
			this.salesOrder.taxAmount.amountIGST = 0;
			this.salesOrder.grandTotal = this.salesOrder.orderAmount + this.salesOrder.taxAmount.amountCGST + this.salesOrder.taxAmount.amountSGST;
			console.log("this.salesOrder.grandTotal : " + this.salesOrder.grandTotal);

		} else {
			for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
				this.salesOrder.taxAmount.amountIGST += this.salesOrder.salesItems[i].taxAmount.amountIGST;
				console.log("else : this.salesOrder.taxAmount.amountIGST " + this.salesOrder.taxAmount.amountIGST);
			}
			this.salesOrder.grandTotal = this.salesOrder.orderAmount + this.salesOrder.taxAmount.amountIGST;
			this.salesOrder.taxAmount.amountCGST = 0;
			this.salesOrder.taxAmount.amountSGST = 0;
		}
	}

	// calculateTax() {

	// 	if (this.salesOrder.buyerContactDetails.state === this.salesOrder.sellerContactDetails.state) {
	// 		// this.salesOrder.taxAmount.amountCGST = (this.salesOrder.orderAmount * this.salesOrder.taxRateCGST) / 100;
	// 		// this.salesOrder.taxAmount.amountSGST = (this.salesOrder.orderAmount * this.salesOrder.taxRateSGST) / 100;
	// 		// this.salesOrder.grandTotal = this.salesOrder.orderAmount + this.salesOrder.taxAmount.amountCGST + this.salesOrder.taxAmount.amountSGST;
	// 		// this.salesOrder.taxAmount.amountIGST = 0;
	// 	} else {
	// 		// this.salesOrder.taxAmount.amountIGST = (this.salesOrder.orderAmount * this.salesOrder.taxAmount.amountIGST) / 100;
	// 		// this.salesOrder.grandTotal = this.salesOrder.orderAmount + this.salesOrder.taxAmount.amountIGST;
	// 		// this.salesOrder.taxAmount.amountCGST = 0;
	// 		// this.salesOrder.taxAmount.amountSGST = 0;
	// 	}
	// }

	addItemToSalesOrder(salesItem: SalesItem, quantity: number) {
		let tempSalesItem = {} as SalesItem;
		tempSalesItem.taxRate = {} as TaxRate;
		tempSalesItem.taxAmount = {} as TaxAmount;
		tempSalesItem.ID = salesItem.ID;
		tempSalesItem.itemPrice = salesItem.itemPrice;
		tempSalesItem.itemName = salesItem.itemName;
		tempSalesItem.itemType = salesItem.itemType;
		tempSalesItem.itemUOM = salesItem.itemUOM;
		tempSalesItem.itemQuantity = quantity;
		tempSalesItem.reservedQuantity = salesItem.reservedQuantity;
		tempSalesItem.taxRate = salesItem.taxRate;
		this.salesOrder.salesItems.push(tempSalesItem);
	}

	removeItemFromSalesOrder(index: number) {
		this.salesOrder.salesItems.splice(index, 1);
	}

	updateItemFromSalesOrder(index: number, quantity: number) {
		this.salesOrder.salesItems[index].itemQuantity = quantity;
	}

	// async addOrder(): Promise<boolean> {

	// 	await this.AO();
	// 	if (docRef) {
	// 		await docRef.set({
	// 			ID: orderID,
	// 			creation_time: time,
	// 			creation_date: getDateString(new Date()),
	// 			salesItems: this.salesOrder.salesItems,
	// 			sellerContactDetails: this.salesOrder.sellerContactDetails,
	// 			buyerContactDetails: this.salesOrder.buyerContactDetails,
	// 			orderAmount: this.salesOrder.orderAmount,
	// 			grandTotal: this.salesOrder.grandTotal,
	// 			taxAmountCGST: this.salesOrder.taxAmountCGST,
	// 			taxAmountSGST: this.salesOrder.taxAmountCGST,
	// 			taxAmountIGST: this.salesOrder.taxAmountIGST,
	// 			orderStatus: myConstants.orderStatus.NEW
	// 		});
	// 		this.initialize();
	// 		return true;
	// 	} else {
	// 		return false;
	// 	}
	// }

	async addOrder(): Promise<boolean> {

		console.log("Inside addOrder()");

		let itemRef: Array<DocumentReference> = [];
		const updatedReservedQuantity: Array<number> = [];

		let time = getDateTimeString(new Date());
		let orderID = this.salesOrder.buyerContactDetails.firstName + '_' + this.salesOrder.buyerContactDetails.lastName + '_' + time;
		let orderRef: DocumentReference = this.afs.doc(`/SalesOrderList/${orderID}`).ref;

		for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
			itemRef[i] = this.afs.doc(`/SalesItemList/${this.salesOrder.salesItems[i].ID}`).ref;
		}

		for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
			updatedReservedQuantity[i] = this.salesOrder.salesItems[i].itemQuantity;
			// reservedQuantityOld[i] += this.salesOrder.salesItems[i].itemQuantity;
			console.log("Before updatedReservedQuantity[" + i + "] = " + updatedReservedQuantity[i]);
		}

		await this.afs.firestore.runTransaction(async transaction => {

			const docSnapshot: Array<any> = [];
			const oldReservedQuantity: Array<number> = [];

			for (let i = 0; i < itemRef.length; i++) {
				docSnapshot[i] = await transaction.get(itemRef[i]);
				console.log(docSnapshot[i]);
				oldReservedQuantity.push(docSnapshot[i].data().reservedQuantity);
				console.log(docSnapshot[i].data().reservedQuantity);
				updatedReservedQuantity[i] += oldReservedQuantity[i];
				console.log("After updatedReservedQuantity[" + i + "] = " + updatedReservedQuantity[i]);
			}

			for (let i = 0; i < itemRef.length; i++) {
				await transaction.update(itemRef[i], { reservedQuantity: updatedReservedQuantity[i] });
			}

			await transaction.set(orderRef, {
				ID: orderID,
				creation_time: time,
				creation_date: getDateString(new Date()),
				salesItems: this.salesOrder.salesItems,
				sellerContactDetails: this.salesOrder.sellerContactDetails,
				buyerContactDetails: this.salesOrder.buyerContactDetails,
				orderAmount: this.salesOrder.orderAmount,
				grandTotal: this.salesOrder.grandTotal,
				taxAmount: this.salesOrder.taxAmount,
				orderStatus: myConstants.orderStatus.NEW
			});
		});
		this.initialize();
		return true;
	}

	changeStatusOfOrder(status: string): Promise<boolean> {
		let orderID = this.salesOrder.ID;
		let docRef: DocumentReference = this.afs.doc(`/SalesOrderList/${orderID}`).ref
		return new Promise((resolve, reject) => {
			if (docRef) {
				docRef.update({
					orderStatus: status
				}).then(() => {
					resolve(true);
				}).catch(() => {
					console.log("Error in docRef.update()");
					resolve(false);
				})
			}
		});
	}

	async orderDelivered(status: string): Promise<boolean> {

		console.log("Inside orderDelivered()");

		let itemRef: Array<DocumentReference> = [];
		const purchaseQuantity: Array<number> = [];

		let orderID = this.salesOrder.ID;
		let orderRef: DocumentReference = this.afs.doc(`/SalesOrderList/${orderID}`).ref

		for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
			itemRef[i] = this.afs.doc(`/SalesItemList/${this.salesOrder.salesItems[i].ID}`).ref;
		}
		for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
			purchaseQuantity[i] = this.salesOrder.salesItems[i].itemQuantity;
			// reservedQuantityOld[i] += this.salesOrder.salesItems[i].itemQuantity;
			console.log(" purchaseQuantity[" + i + "] = " + purchaseQuantity[i]);
		}

		await this.afs.firestore.runTransaction(async transaction => {
			const docSnapshot: Array<any> = [];
			const newTotalQuantity: Array<number> = [];
			const newReservedQuantity: Array<number> = [];

			for (let i = 0; i < itemRef.length; i++) {
				docSnapshot[i] = await transaction.get(itemRef[i]);
				console.log(docSnapshot[i]);
				newReservedQuantity.push(docSnapshot[i].data().reservedQuantity);
				newTotalQuantity.push(docSnapshot[i].data().itemQuantity);
				console.log(docSnapshot[i].data().reservedQuantity);
				newReservedQuantity[i] -= purchaseQuantity[i];
				newTotalQuantity[i] -= purchaseQuantity[i];
				console.log("newReservedQuantity[" + i + "] = " + newReservedQuantity[i]);
				console.log("newTotalQuantity[" + i + "] = " + newTotalQuantity[i]);
			}

			for (let i = 0; i < itemRef.length; i++) {
				await transaction.update(itemRef[i], {
					reservedQuantity: newReservedQuantity[i],
					itemQuantity: newTotalQuantity[i]
				});
			}

			await transaction.update(orderRef, {
				orderStatus: status
			});
		});

		return true;
	}

	async deleteOrder() {
		console.log("Inside deleteOrder()");
		let itemRef: Array<DocumentReference> = [];
		const updatedReservedQuantity: Array<number> = [];

		let orderID = this.salesOrder.ID;
		let orderRef: DocumentReference = this.afs.doc(`/SalesOrderList/${orderID}`).ref;
		for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
			itemRef[i] = this.afs.doc(`/SalesItemList/${this.salesOrder.salesItems[i].ID}`).ref;
		}
		for (let i = 0; i < this.salesOrder.salesItems.length; i++) {
			updatedReservedQuantity[i] = this.salesOrder.salesItems[i].itemQuantity;
			console.log(" updatedReservedQuantity[" + i + "] = " + updatedReservedQuantity[i]);
		}
		await this.afs.firestore.runTransaction(async transaction => {
			const docSnapshot: Array<any> = [];
			const oldReservedQuantity: Array<number> = [];

			for (let i = 0; i < itemRef.length; i++) {
				docSnapshot[i] = await transaction.get(itemRef[i]);
				console.log(docSnapshot[i]);
				oldReservedQuantity.push(docSnapshot[i].data().reservedQuantity);
				console.log(docSnapshot[i].data().reservedQuantity);
				updatedReservedQuantity[i] -= oldReservedQuantity[i];
				console.log("newReservedQuantity[" + i + "] = " + updatedReservedQuantity[i]);
			}

			for (let i = 0; i < itemRef.length; i++) {
				await transaction.update(itemRef[i], {
					reservedQuantity: updatedReservedQuantity[i]
				});
			}
			await transaction.delete(orderRef);
		});

		return true;
	}

}
