import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SalesItem } from '../../model/item';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { User } from '../../model/user';


@IonicPage()
@Component({
    selector: 'page-item-info',
    templateUrl: 'item-info.html',
})
export class ItemInfoPage {

    private item = {} as SalesItem;
    private user : User;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public imagePicker: ImagePicker,
        public cropService: Crop,
        private firebaseProvider: FirebaseProvider,
        public viewCtrl: ViewController) {
        this.item = navParams.get("item");
        this.user = navParams.get("user");
        console.log(this.item);
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ItemInfoPage');
    }

    goToRootPage() {
        this.viewCtrl.dismiss();
    }

    editImage() {
        this.imagePicker.hasReadPermission().then(
            (result) => {
                if (result == false) {
                    // no callbacks required as this opens a popup which returns async
                    this.imagePicker.requestReadPermission();
                }
                else if (result == true) {
                    this.imagePicker.getPictures({
                        maximumImagesCount: 1
                    }).then(
                        (results) => {
                            console.log("results " + results);
                            for (var i = 0; i < results.length; i++) {
                                this.cropService.crop(results[i], { quality: 10, targetHeight: 100, targetWidth: 100 }).then(
                                    newImage => {
                                        console.log("selected image " + newImage);
                                        this.item.imageURL = newImage;
                                        this.firebaseProvider.editItemImage(this.item).then((response) => {
                                            if (response === true)
                                                this.viewCtrl.dismiss();
                                        });
                                    },
                                    error => console.error("Error cropping image", error)
                                );
                            }
                        }, (err) => console.log(err)
                    );
                }
            }, (err) => {
                console.log(err);
            });
    }

}
