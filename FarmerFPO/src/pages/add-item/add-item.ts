import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Item, LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { SalesItem } from '../../model/item';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { iterateListLike } from '@angular/core/src/change_detection/change_detection_util';


@IonicPage()
@Component({
	selector: 'page-add-item',
	templateUrl: 'add-item.html',
})
export class AddItemPage {

	private item = {} as SalesItem;
	private previewImageURL: string;
	private passedFrom: string;

	constructor(public navCtrl: NavController,
		private firebaseProvider: FirebaseProvider,
		public viewCtrl: ViewController,
		public imagePicker: ImagePicker,
		public loadingController: LoadingController,
		public cropService: Crop,
		public navParams: NavParams) {
		this.item = navParams.get("item");
		this.passedFrom = navParams.get("passedFrom");
		console.log(this.item);

	}

	ionViewWillEnter() {
		this.previewImageURL = "";
		console.log('ionViewWillEnter AddItemPage');
	}

	selectImage() {
		this.imagePicker.hasReadPermission().then(
			(result) => {
				if (result == false) {
					// no callbacks required as this opens a popup which returns async
					this.imagePicker.requestReadPermission();
				}
				else if (result == true) {
					this.imagePicker.getPictures({
						maximumImagesCount: 1
					}).then(
						(results) => {
							console.log("results " + results);
							for (var i = 0; i < results.length; i++) {
								this.cropService.crop(results[i], { quality: 15, targetHeight: 100, targetWidth: 100 }).then(
									newImage => {
										console.log("selected image " + newImage);
										this.item.imageURL = newImage;
										this.previewImageURL = newImage;
										// this.previewImageURL.replace('file://', '');
										console.log("this.previewImageURL : " + this.previewImageURL);
									},
									error => console.error("Error cropping image", error)
								);
							}
						}, (err) => console.log(err)
					);
				}
			}, (err) => {
				console.log(err);
			});
	}

	saveItem() {
		if (this.passedFrom === "addItem") {
			const loading = this.loadingController.create({ content: "Adding item....." });
			loading.present();
			this.firebaseProvider.addItem(this.item).then((response) => {
				if (response === true) {
					loading.dismissAll();
					this.viewCtrl.dismiss();
				}
			});
		}
		if (this.passedFrom === "editItem") {
			const loading = this.loadingController.create({ content: "Saving changes....." });
			loading.present();
			this.firebaseProvider.editItemDetails(this.item).then((response) => {
				if (response === true) {
					loading.dismissAll();
					this.viewCtrl.dismiss();
				}
			});
		}
	}

	saveEditedImage() {
		const loading = this.loadingController.create({ content: "Saving image changes....." });
		loading.present();
		this.firebaseProvider.editItemImage(this.item).then((response) => {
			if (response === true) {
				loading.dismissAll();
				this.viewCtrl.dismiss();
			}
		});
	}

	goToRootPage() {
		this.viewCtrl.dismiss();
	}
}
