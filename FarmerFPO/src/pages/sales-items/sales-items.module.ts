import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SalesItemsPage } from './sales-items';

@NgModule({
  declarations: [
    SalesItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(SalesItemsPage),
  ],
})
export class SalesItemsPageModule {}
