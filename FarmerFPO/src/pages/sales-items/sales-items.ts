import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, ViewController, UrlSerializer, LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { OrderProvider } from '../../providers/order/order';
import { SalesItem } from '../../model/item';
import { TaxAmount, TaxRate } from '../../model/tax';
import { SalesOrder } from '../../model/salesOrder';
import { Subscription } from 'rxjs';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { SalesOrderPage } from '../sales-order/sales-order'
import { AuthProvider } from '../../providers/auth/auth'
import { User } from '../../model/user';
@IonicPage()
@Component({
	selector: 'page-sales-items',
	templateUrl: 'sales-items.html',
})
export class SalesItemsPage {

	private itemList: Array<SalesItem>;
	private searchedItemList: Array<SalesItem>;
	// private salesOrder: SalesOrder;
	// private totalPrice: number;
	private user: User;

	private mySubscriptions: Subscription;
	private searchInput: string;

	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public loadingController: LoadingController,
		public alertCtrl: AlertController,
		private viewCtrl: ViewController,
		private authProvider: AuthProvider,
		private modalCtrl: ModalController,
		public imagePicker: ImagePicker,
		public cropService: Crop,
		private orderProvider: OrderProvider,
		private firebaseProvider: FirebaseProvider) {

		this.user = {} as User;
		this.user.ID = "";
		this.user.name = "";
		this.user.email = "";
		this.user.role = "buyer";
		this.user.isAdmin = false;

		this.searchInput = "";
		// this.totalPrice = 0;
		this.searchedItemList = [];

		// let tempSalesOrder = new SalesOrder();
		// tempSalesOrder.ID = "";
		// tempSalesOrder.salesItems = [];
		// tempSalesOrder.sellerContactDetails = {} as SellerContact;
		// tempSalesOrder.buyerContactDetails = {} as BuyerContact;
		// tempSalesOrder.initialize();
		// this.salesOrder = new SalesOrder();

		console.log("SalesItemsPage constructor() ");
	}

	ionViewWillEnter() {
		console.log('ionViewWillEnter SalesItemsPage');
		this.loadItems();
		console.log(this.searchedItemList);
	}

	ionViewWillLeave() {
		console.log('ionViewWillLeave SalesItemsPage');
		this.mySubscriptions.unsubscribe();
	}

	async login() {
		this.authProvider.getUser().then((currentUser) => {
			this.user.ID = currentUser.ID;
			this.user.name = currentUser.name;
			this.user.email = currentUser.email;
			this.user.isAdmin = currentUser.isAdmin;
			this.user.role = currentUser.role;
			console.log("user : " + this.user.role + " " + this.user.name + " " + this.user.email);
		});
	}

	async logout() {
		const loading = this.loadingController.create({ content: "Logging out....." });
		await this.authProvider.logoutGoogle();
		this.user.ID = '';
		this.user.email = '';
		this.user.name = '';
		this.user.role = "buyer";
		this.user.isAdmin = false;
		loading.dismissAll();
	}

	loadItems() {
		this.mySubscriptions = new Subscription();
		this.mySubscriptions.add(this.firebaseProvider.getItemsList().valueChanges()
			.subscribe((itemListFromDB) => {
				this.itemList = [];
				this.searchedItemList = [];
				for (let i = 0; i < itemListFromDB.length; i++) {
					let item = {} as SalesItem;
					item.taxRate = {} as TaxRate;
					item.ID = itemListFromDB[i].ID;
					item.itemName = itemListFromDB[i].itemName;
					item.itemType = itemListFromDB[i].itemType;
					item.itemPrice = itemListFromDB[i].itemPrice;
					item.itemUOM = itemListFromDB[i].itemUOM;
					item.itemQuantity = itemListFromDB[i].itemQuantity;
					item.imageURL = itemListFromDB[i].imageURL;
					item.reservedQuantity = itemListFromDB[i].reservedQuantity;
					item.itemDescription = itemListFromDB[i].itemDescription;
					item.taxRate = itemListFromDB[i].taxRate;
					this.itemList.push(item);
					this.searchedItemList.push(item);
				}
			}));
	}

	onSearch() {
		this.searchedItemList = this.itemList.filter((item) => {
			if (item.itemName.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1) {
				return true;
			}
			else
				return false;
		});
	}

	addToCart(item: SalesItem) {
		const prompt = this.alertCtrl.create({
			title: 'Quantity',
			inputs: [
				{
					name: 'quantity',
					placeholder: 'Quantity',
					type: 'number'
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Add',
					handler: data => {
						this.orderProvider.addItemToSalesOrder(item, + data.quantity);
						this.orderProvider.calculateCartTotal();
					}
				}
			]
		});
		prompt.present();
	}

	addItem() {
		let item = {} as SalesItem;
		item.taxAmount = {} as TaxAmount;
		item.taxRate = {} as TaxRate;
		item.ID = "";
		item.itemName = "";
		item.itemPrice = null;
		item.itemQuantity = null;
		item.itemDescription = "";
		item.itemType = "";
		item.itemUOM = "";
		item.imageURL = "";
		item.reservedQuantity = 0;
		item.totalAmount = 0;
		item.taxRate.rateCGST = null;
		item.taxRate.rateSGST = null;
		item.taxRate.rateIGST = null;
		item.taxAmount.amountCGST = 0;
		item.taxAmount.amountSGST = 0;
		item.taxAmount.amountIGST = 0;
		const modal = this.modalCtrl.create('AddItemPage', { "item": item, "passedFrom": "addItem" });
		modal.present();
	}

	editItem(item: SalesItem) {
		const modal = this.modalCtrl.create('AddItemPage', { "item": item, "passedFrom": "editItem" });
		modal.present();
	}

	openInfoPage(item: SalesItem) {
		const modal = this.modalCtrl.create('ItemInfoPage', { "item": item , "user" : this.user });
		modal.present();
	}

	editImage(item: SalesItem) {
		this.imagePicker.hasReadPermission().then(
			(result) => {
				if (result == false) {
					// no callbacks required as this opens a popup which returns async
					this.imagePicker.requestReadPermission();
				}
				else if (result == true) {
					this.imagePicker.getPictures({
						maximumImagesCount: 1
					}).then(
						(results) => {
							console.log("results " + results);
							for (var i = 0; i < results.length; i++) {
								this.cropService.crop(results[i], { quality: 10, targetHeight: 100, targetWidth: 100 }).then(
									newImage => {
										console.log("selected image " + newImage);
										item.imageURL = newImage;
										this.firebaseProvider.editItemImage(item).then((response) => {
											if (response === true)
												this.viewCtrl.dismiss();
										});
									},
									error => console.error("Error cropping image", error)
								);
							}
						}, (err) => console.log(err)
					);
				}
			}, (err) => {
				console.log(err);
			});
	}

	goToSalesOrderPage() {
		// const modal = this.modalCtrl.create('SalesOrderPage');
		// modal.present();
		this.navCtrl.push(SalesOrderPage);
	}

	goToCheckOutPage() {
		const modal = this.modalCtrl.create('CheckoutPage', { "calledFrom": 'salesItemPage' });
		modal.present();
		modal.onDidDismiss((data) => {
			// console.log("onDidDismiss " + data.purchaseSuccess);
			// if (data.purchaseSuccess == true) {
			// 	console.log("Order placed successfully");
			// }
			this.orderProvider.calculateCartTotal();
		})
	}

}
