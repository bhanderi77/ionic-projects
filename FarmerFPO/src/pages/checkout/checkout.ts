import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ViewController, LoadingController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { OrderProvider } from '../../providers/order/order';
import { myConstants } from '../../model/myConstants';
import { State } from '../../model/state';
import { Subscription } from 'rxjs';

@IonicPage()
@Component({
	selector: 'page-checkout',
	templateUrl: 'checkout.html',
})
export class CheckoutPage {

	// private salesOrder: SalesOrder;
	public states: Array<State>;
	private calledFrom: string;
	private mySubscriptions: Subscription;
	private selectedState: string;

	constructor(public navCtrl: NavController,
		private firebaseProvider: FirebaseProvider,
		private orderProvider: OrderProvider,
		public viewCtrl: ViewController,
		public loadingController:LoadingController,
		public alertCtrl: AlertController,
		public navParams: NavParams) {

		// let tempSalesOrder: SalesOrder = navParams.get("salesOrder");
		this.calledFrom = navParams.get("calledFrom");
		console.log("Called from : " + this.calledFrom);
		// this.salesOrder = new SalesOrder();
		// this.salesOrder.copy(tempSalesOrder);
		// this.totalPrice = this.salesOrder.calculateCartTotal();
		orderProvider.calculateCartTotal();
		if (this.calledFrom === 'salesItemPage') {
			this.orderProvider.salesOrder.sellerContactDetails.addressLine1 = "C-42, Sheetal Complex";
			this.orderProvider.salesOrder.sellerContactDetails.addressLine2 = "SV Road, Dahisar(E)";
			this.orderProvider.salesOrder.sellerContactDetails.firstName = "Hardik";
			this.orderProvider.salesOrder.sellerContactDetails.lastName = "Bhanderi";
			this.orderProvider.salesOrder.sellerContactDetails.city = "Mumbai";
			this.orderProvider.salesOrder.sellerContactDetails.state = "Maharashtra";
			this.orderProvider.salesOrder.sellerContactDetails.pinCode = "400068"
			this.orderProvider.salesOrder.sellerContactDetails.email = "bhanderi77@gmail.com";
			this.orderProvider.salesOrder.sellerContactDetails.phoneNumber = "9820216862";
			this.selectedState = "";
			console.log("CheckoutPage constructor " + this.orderProvider.salesOrder);
		} else {
			this.selectedState = orderProvider.salesOrder.buyerContactDetails.state;
		}
	}

	ionViewWillEnter() {
		console.log('ionViewWillEnter CheckoutPage');
		this.mySubscriptions = new Subscription();
		this.mySubscriptions.add(this.firebaseProvider.getStates().valueChanges()
			.subscribe((statesFromDB) => {
				this.states = [];
				this.states = statesFromDB;
			}));
	}

	ionViewWillLeave() {
		if (this.calledFrom === 'salesOrderPage')
			this.orderProvider.initialize();
		console.log('ionViewWillLeave CheckoutPage');
		this.mySubscriptions.unsubscribe();
		// this.viewCtrl.dismiss().catch(() => {
		// 	console.log("Error catched");
		// });
	}

	goToRootPage(){
		this.viewCtrl.dismiss();
	}

	stateSelected() {
		this.orderProvider.salesOrder.buyerContactDetails.state = this.selectedState;
		this.orderProvider.calculateCartTotal();
		console.log(this.orderProvider.salesOrder);
	}

	async placeOrder() {
		console.log("inside placeOrder()" + this.orderProvider.salesOrder);
		const loading = this.loadingController.create({ content: "Placing order" });
		loading.present();
		await this.orderProvider.addOrder();
		loading.dismissAll();
		console.log("Order placed successfully.");
		// await this.firebaseProvider.addOrder(this.salesOrder);
		// await this.viewCtrl.dismiss({salesOrder:this.salesOrder});
		this.viewCtrl.dismiss();
	}

	changeStatus(status: string) {
		switch (status) {
			case 'New':
				this.orderProvider.changeStatusOfOrder('In Progress').then((response) => {
					if (response === true) {
						console.log("Status changed successfully");
						this.orderProvider.salesOrder.orderStatus = 'In Progress';
					}
				});
				break;

			case 'In Progress':
				this.orderProvider.changeStatusOfOrder('Dispatched').then((response) => {
					if (response === true) {
						console.log("Status changed successfully");
						this.orderProvider.salesOrder.orderStatus = 'Dispatched';
					}
				});
				break;

			case 'Dispatched':
				this.orderProvider.orderDelivered('Delivered').then((response) => {
					if (response === true) {
						console.log("Status changed successfully");
						this.orderProvider.salesOrder.orderStatus = 'Delivered';
					}
				});
				break;

			default:
				break;
		}

		// let alert = this.alertCtrl.create();
		// alert.setTitle('Order status');

		// alert.addInput({
		// 	type: 'radio',
		// 	label: myConstants.orderStatus.NEW,
		// 	value: myConstants.orderStatus.NEW,
		// 	checked: false
		// });

		// alert.addInput({
		// 	type: 'radio',
		// 	label: myConstants.orderStatus.IN_PROGRESS,
		// 	value: myConstants.orderStatus.IN_PROGRESS,
		// 	checked: false
		// });

		// alert.addInput({
		// 	type: 'radio',
		// 	label: myConstants.orderStatus.DISPATCHED,
		// 	value: myConstants.orderStatus.DISPATCHED,
		// 	checked: false
		// });

		// alert.addInput({
		// 	type: 'radio',
		// 	label: myConstants.orderStatus.DELIVERED,
		// 	value: myConstants.orderStatus.DELIVERED,
		// 	checked: false
		// });

		// alert.addButton('Cancel');
		// alert.addButton({
		// 	text: 'Save',
		// 	handler: data => {
		// 		console.log("Status : " + data);
		// 		this.orderProvider.changeStatusOfOrder(data).then((response) => {
		// 			if (response === true) {
		// 				console.log("Status changed successfully");
		// 				this.orderProvider.salesOrder.orderStatus = data;
		// 			}
		// 		})
		// 	}
		// });
		// alert.present();
	}

	cancelOrder() {
		const confirm = this.alertCtrl.create({
			title: 'Are you sure to delete this order?',
			buttons: [
				{
					text: 'NO',
					handler: () => {
						console.log('NO clicked');
					}
				},
				{
					text: 'YES',
					handler: () => {
						console.log('YES clicked');
						this.orderProvider.deleteOrder().then((response) => {
							if (response === true)
								this.viewCtrl.dismiss();
						});
					}
				}
			]
		});
		confirm.present();
	}

	deleteItem(index: number) {
		const confirm = this.alertCtrl.create({
			title: 'Remove ' + this.orderProvider.salesOrder.salesItems[index].itemName + ' from cart?',
			buttons: [
				{
					text: 'NO',
					handler: () => {
						console.log('Disagree clicked');
					}
				},
				{
					text: 'YES',
					handler: () => {
						console.log('Agree clicked');
						// this.salesOrder.removeItemFromSalesOrder(index);
						this.orderProvider.removeItemFromSalesOrder(index);
						// this.totalPrice = this.salesOrder.calculateCartTotal();
						this.orderProvider.calculateCartTotal();
					}
				}
			]
		});
		confirm.present();
	}

	updateItem(quantity: number, index: number) {
		const prompt = this.alertCtrl.create({
			title: 'Quantity',
			inputs: [
				{
					name: 'quantity',
					placeholder: 'Quantity',
					type: 'number',
					value: '' + quantity
				},
			],
			buttons: [
				{
					text: 'Cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Update',
					handler: data => {
						console.log(data);
						// this.salesOrder.updateItemFromSalesOrder(index, +data.quantity);
						this.orderProvider.updateItemFromSalesOrder(index, +data.quantity);
						// this.totalPrice = this.salesOrder.calculateCartTotal();
						this.orderProvider.calculateCartTotal();
					}
				}
			]
		});
		prompt.present();
	}
}
