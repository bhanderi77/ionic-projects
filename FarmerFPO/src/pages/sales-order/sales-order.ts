import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, AlertController } from 'ionic-angular';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { SalesOrder } from '../../model/salesOrder';
import { Subscription } from 'rxjs';
import { OrderProvider } from '../../providers/order/order';
import { myConstants } from '../../model/myConstants';
import { DatePipe } from '@angular/common';


@IonicPage()
@Component({
	selector: 'page-sales-order',
	templateUrl: 'sales-order.html',
})
export class SalesOrderPage {

	private salesOrderList: Array<SalesOrder>;
	private filteredSalesOrderList: Array<SalesOrder>;

	private mySubscriptions: Subscription;
	private filter: string;

	constructor(public navCtrl: NavController,
		private viewCtrl: ViewController,
		private modalCtrl: ModalController,
		public alertCtrl: AlertController,
		public datepipe: DatePipe,
		private firebaseProvider: FirebaseProvider,
		private orderProvider: OrderProvider,
		public navParams: NavParams) {

		this.salesOrderList = [];
		this.filteredSalesOrderList = [];
		this.filter = "";
	}

	ionViewWillEnter() {
		this.loadOrders();
		console.log(this.salesOrderList);
	}

	ionViewWillLeave() {
		console.log('ionViewWillLeave SalesOrderPage');
		this.mySubscriptions.unsubscribe();

	}

	loadOrders() {
		this.mySubscriptions = new Subscription();
		this.mySubscriptions.add(this.firebaseProvider.getSalesOrderList().valueChanges()
			.subscribe((salesOrderListFromDB) => {
				console.log(salesOrderListFromDB.length);
				this.salesOrderList = [];
				this.filteredSalesOrderList = [];
				if (salesOrderListFromDB.length > 0) {
					this.salesOrderList = salesOrderListFromDB;
					this.filteredSalesOrderList = salesOrderListFromDB;
					console.log("loadOrders() :filteredSalesOrderList : " + this.filteredSalesOrderList);
					if (this.filter)
						this.filteredSalesOrderList = this.salesOrderList.filter(order => order.orderStatus === this.filter);
					console.log(this.salesOrderList);
				}
				// for (let i = 0; i < salesOrderListFromDB.length; i++) {
				// 	let order = {} as SalesOrder;
				// 	order.ID = salesOrderListFromDB[i].ID;
				// 	order.salesItems = salesOrderListFromDB[i].salesItems;
				// 	order.buyerContactDetails = salesOrderListFromDB[i].buyerContactDetails;
				// 	order.sellerContactDetails = salesOrderListFromDB[i].sellerContactDetails;
				// 	order.cartTotal = salesOrderListFromDB[i].cartTotal;
				// 	order.orderStatus = salesOrderListFromDB[i].orderStatus;
				// 	this.salesOrderList.push(order);
				// 	this.filteredSalesOrderList.push(order);
				// 	console.log(order);
				// }
			}));
	}

	checkOrder(salesOrder: SalesOrder) {
		this.orderProvider.copy(salesOrder);
		const modal = this.modalCtrl.create('CheckoutPage', { "calledFrom": 'salesOrderPage' });
		modal.present();
		// modal.onDidDismiss(() => {
		// 	console.log("Inside onDidDismiss()");
		// 	this.loadOrders();
		// });
	}

	filterOrder() {
		let alert = this.alertCtrl.create();
		alert.setTitle('Order status');

		alert.addInput({
			type: 'radio',
			label: myConstants.orderStatus.NEW,
			value: myConstants.orderStatus.NEW,
			checked: false
		});

		alert.addInput({
			type: 'radio',
			label: myConstants.orderStatus.IN_PROGRESS,
			value: myConstants.orderStatus.IN_PROGRESS,
			checked: false
		});

		alert.addInput({
			type: 'radio',
			label: myConstants.orderStatus.DISPATCHED,
			value: myConstants.orderStatus.DISPATCHED,
			checked: false
		});

		alert.addInput({
			type: 'radio',
			label: myConstants.orderStatus.DELIVERED,
			value: myConstants.orderStatus.DELIVERED,
			checked: false
		});

		alert.addButton('Cancel');
		alert.addButton({
			text: 'Save',
			handler: data => {
				this.filter = data;
				console.log("Status : " + data);
				this.filteredSalesOrderList = this.salesOrderList.filter(order => order.orderStatus === this.filter);
			}
		});
		alert.present();
	}

}
