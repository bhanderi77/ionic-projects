import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, normalizeURL, ToastController } from 'ionic-angular';
import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { FirebaseProvider } from '../../providers/firebase/firebase';


@IonicPage()
@Component({
	selector: 'page-image',
	templateUrl: 'image.html',
})
export class ImagePage {

	source: string;

	constructor(
		public navCtrl: NavController,
		public imagePicker: ImagePicker,
		public cropService: Crop,
		public toastCtrl: ToastController,
		private firebaseProvider: FirebaseProvider
	) {
		this.source = "";
	}

	openImagePickerCrop() {
		this.imagePicker.hasReadPermission().then(
			(result) => {
				if (result == false) {
					// no callbacks	 required as this opens a popup which returns async
					this.imagePicker.requestReadPermission();
				}
				else if (result == true) {
					this.imagePicker.getPictures({
						maximumImagesCount: 1
					}).then(
						(results) => {
							console.log("results " + results);
							for (var i = 0; i < results.length; i++) {
								this.cropService.crop(results[i], { quality: 25, targetHeight: 50, targetWidth: 50 }).then(
									newImage => {
										//this.uploadImageToFirebase(newImage);
									},
									error => console.error("Error cropping image", error)
								);
							}
						}, (err) => console.log(err)
					);
				}
			}, (err) => {
				console.log(err);
			});
	}

	openImagePicker() {
		this.imagePicker.hasReadPermission().then(
			(result) => {
				if (result == false) {
					// no callbacks required as this opens a popup which returns async
					this.imagePicker.requestReadPermission();
				}
				else if (result == true) {
					this.imagePicker.getPictures({
						maximumImagesCount: 1
					}).then(
						(results) => {
							for (var i = 0; i < results.length; i++) {
								// this.uploadImageToFirebase(results[i]);
							}
						}, (err) => console.log(err)
					);
				}
			}, (err) => {
				console.log(err);
			});
	}

	uploadImageToFirebase(image) {
		// image = normalizeURL(image);
		// //uploads img to firebase storage
		// this.firebaseProvider.uploadImage(image)
		// 	.then(photoURL => {
		// 		console.log("photo URL " + photoURL);
		// 		let toast = this.toastCtrl.create({
		// 			message: 'Image was updated successfully',
		// 			duration: 3000
		// 		});
		// 		toast.present();
		// 	})
	}
}
