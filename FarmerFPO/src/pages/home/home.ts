import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { FpoPage } from '../../pages/fpo/fpo';
@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	farmerList: Array<FarmerDetails>;
	fpoList: Array<FpoDetails>;
	constructor(private navCtrl: NavController,
		private modalCtrl: ModalController,
		private firebaseProvider: FirebaseProvider) {
		this.farmerList = [];
		console.log("constructor farmerList " + this.fpoList);
	}

	ionViewDidEnter() {
		this.loadFpo();
	}

	loadFpo() {
		let count: number = 0;
		this.firebaseProvider.getFpoList().valueChanges()
			.subscribe((fpoListFromDB) => {
				this.fpoList = [];
				count = fpoListFromDB.length;
				// console.log("length = " + count);
				for (let i = 0; i < fpoListFromDB.length; i++) {
					let fpo = {} as FpoDetails;
					fpo.ID = fpoListFromDB[i].ID;
					fpo.name = fpoListFromDB[i].name;
					fpo.farmers = fpoListFromDB[i].farmers;
					fpo.locality = fpoListFromDB[i].locality;
					fpo.phoneNumber = fpoListFromDB[i].phoneNumber;
					this.fpoList.push(fpo);
				}
			})
	}

	addFpo() {
		console.log("Entered addFpo()");
		const modal = this.modalCtrl.create('AddFpoPage');
		modal.present();
		console.log("Leaving addFpo()");
	}

	openFpo(fpo: FpoDetails) {
		console.log("Entered openFpo()");
		this.navCtrl.push(FpoPage, { "fpo": fpo });
		console.log("Leaving openFpo()");
	}


}
