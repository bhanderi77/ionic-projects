import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';

@IonicPage()
@Component({
	selector: 'page-add-farmer',
	templateUrl: 'add-farmer.html',
})
export class AddFarmerPage {

	farmer = {} as FarmerDetails;
	fpo = {} as FpoDetails;
	constructor(public navCtrl: NavController,
		public viewCtrl: ViewController,
		public firebaseProvider: FirebaseProvider,
		public navParams: NavParams) {
		this.fpo = navParams.get("fpo");
		console.log(this.fpo);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AddFarmerPage');
		this.farmer.name = "";
		this.farmer.phoneNumber = "";
		this.farmer.crops = [];
		this.farmer.city = "";
		this.farmer.state = "";
		this.farmer.landSize = null;
		this.farmer.crops.push({"name":""});
		console.log(this.farmer);
	}

	async saveFarmer() {
		console.log(this.farmer.crops);
		await this.firebaseProvider.createFarmer(this.fpo.ID, this.farmer);
		this.viewCtrl.dismiss();
	}

	addCrop(){
		this.farmer.crops.push({"name":""});
	}

	removeCrop(){
		this.farmer.crops.pop();
	}

	goToRootPage() {
		this.viewCtrl.dismiss();
	}

}
