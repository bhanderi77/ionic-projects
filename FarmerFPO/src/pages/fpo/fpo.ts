import { Component, ViewChild, trigger, state, style, transition, animate } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the FpoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-fpo',
    templateUrl: 'fpo.html',
    animations: [
        trigger('fadeInOut', [
            state('void', style({ opacity: '0' })),
            state('*', style({ opacity: '1' })),
            transition('void <=> *', animate('500ms linear'))
        ])
    ]
})
export class FpoPage {

    fpo = {} as FpoDetails;
    numberOfFarmers: number;
    constructor(public navCtrl: NavController,
        private modalCtrl: ModalController,
        private firebaseProvider: FirebaseProvider,
        public navParams: NavParams) {
        this.fpo = navParams.get("fpo");
        console.log(this.fpo);
    }

    ionViewWillEnter() {
        console.log('ionViewWillEnter FpoPage');
        this.loadFarmers();
    }

    loadFarmers() {
        this.firebaseProvider.getFarmerList(this.fpo.ID).valueChanges()
            .subscribe(farmerListFromDB => {
                this.fpo.farmers = [];
                this.numberOfFarmers = farmerListFromDB.length;
                for (let i = 0; i < farmerListFromDB.length; i++) {
                    let farmer = {} as FarmerDetails;
                    farmer.name = farmerListFromDB[i].name;
                    farmer.city = farmerListFromDB[i].city;
                    farmer.state = farmerListFromDB[i].state;
                    farmer.phoneNumber = farmerListFromDB[i].phoneNumber;
                    farmer.landSize = farmerListFromDB[i].landSize;
                    this.fpo.farmers.push(farmer);
                }
                console.log('farmerlist : ', this.fpo.farmers);
            })
    }

    addFarmer() {
        const modal = this.modalCtrl.create('AddFarmerPage', { "fpo": this.fpo });
        modal.present();
    }

}

