import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { firebaseConfig } from './firebaseCredentials';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { GooglePlus } from '@ionic-native/google-plus/';
import firebase from 'firebase';

import { MyApp } from './app.component';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { FirebaseProvider } from '../providers/firebase/firebase';
import { HttpModule } from '@angular/http';
import { DatePipe } from '@angular/common';

import { HomePage } from '../pages/home/home';
import { FpoPage } from '../pages/fpo/fpo';
import { SalesItemsPage } from '../pages/sales-items/sales-items';
import { SalesOrderPage } from '../pages/sales-order/sales-order';

import { ImagePicker } from '@ionic-native/image-picker';
import { Crop } from '@ionic-native/crop';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { OrderProvider } from '../providers/order/order';
import { AuthProvider } from '../providers/auth/auth';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FpoPage,
    SalesItemsPage,
    SalesOrderPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FpoPage,
    SalesItemsPage,
    SalesOrderPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    GooglePlus,
    ImagePicker,
    Crop,
    DatePipe,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FirebaseProvider,
    OrderProvider,
    AuthProvider
  ]
})
export class AppModule {}
