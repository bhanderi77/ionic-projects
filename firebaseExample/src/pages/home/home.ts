import { Component } from '@angular/core';
import { NavController  } from 'ionic-angular';
import { Item } from '../../model/item';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  item: Item;

  constructor(public navCtrl: NavController) {
    this.item = {} as Item;
    this.item.name = "";
    console.log(this.item.name);
  }

}
