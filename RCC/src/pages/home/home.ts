import { Component , ViewChild } from '@angular/core';
import { NavController , Slides } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private total : number;
  private activeTabIndex :number;
  private usdRate : number;
  @ViewChild('slider') slider : Slides;
  private tab:string;
  constructor(public navCtrl: NavController) {
    this.total = 0;
    this.tab="purity";
    this.usdRate = 65;
    this.activeTabIndex = 0;
  }

  updateSegment(){
    if(this.slider.getActiveIndex() === 0){
      this.activeTabIndex = 0;
      this.tab = "purity"
    }
    else{
      this.activeTabIndex = 0;
      this.tab = "size"
    }
      
  }

  selectedTab(index:number){
    this.slider.slideTo(index);
    this.activeTabIndex = index;
  }

}
