import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from 'angularfire2/firestore';
import { FpoDetails, FarmerDetails } from '../../model/farmer';
import { Crop } from '../../model/crops';

@Injectable()
export class FirebaseProvider {

    constructor(private afs: AngularFirestore) {
        console.log('Hello FirebaseProvider Provider');
    }

    getFpoList(): AngularFirestoreCollection<FpoDetails> {
        return this.afs.collection<FpoDetails>(`/FpoList`, ref => ref.orderBy('name'));
    }

    getFarmerList(fpoID: string): AngularFirestoreCollection<FarmerDetails> {
        return this.afs.collection<FarmerDetails>(`/FpoList/${fpoID}/FarmerList`, ref => ref.orderBy('name'));
    }

    createFpo(fpo: FpoDetails): Promise<boolean> {
        console.log("Entering createFpo()");
        return new Promise((resolve, reject) => {
            let fpoID = this.afs.createId();
            let docRef: DocumentReference = this.afs.doc(`/FpoList/${fpoID}`).ref;
            if (docRef) {
                docRef.set({
                    ID: fpoID,
                    name: fpo.name,
                    locality: fpo.locality,
                    phoneNumber: fpo.phoneNumber
                }).then(() => {
                    resolve(true);
                }).catch(() => {
                    resolve(false);
                });
            }
            console.log("Leaving createFpo()");
        });
    }

    async createFarmer(fpo: FpoDetails, farmer: FarmerDetails) {
        let farmerID = this.afs.createId();
        let fpoRef: DocumentReference = this.afs.doc(`/FpoList/${fpo.ID}`).ref;
        let farmerRef: DocumentReference = this.afs.doc(`/FpoList/${fpo.ID}/FarmerList/${farmerID}`).ref;

        await this.afs.firestore.runTransaction(async transaction => {

            let docSnapshot: any;
            let existingCrop: Array<Crop>;
            docSnapshot = await transaction.get(fpoRef);
            existingCrop = docSnapshot.data().crops as Crop[];

            if (existingCrop) {

                for (let i = 0; i < farmer.crops.length; i++) {
                    const found = existingCrop.some(crop => crop.name === farmer.crops[i].name);

                    if (!found) {
                        let tempCrop = {} as Crop;
                        tempCrop.name = farmer.crops[i].name;
                        tempCrop.quantity = +farmer.crops[i].quantity;
                        existingCrop.push(tempCrop);
                    } else {
                        for (let j = 0; j < existingCrop.length; j++) {
                            if (farmer.crops[i].name === existingCrop[j].name) {
                                existingCrop[j].quantity = +farmer.crops[i].quantity + +existingCrop[j].quantity;
                                // existingCrop[j].quantity += +farmer.crops[i].quantity;
                                console.log("existingCrop[" + j + "] " + existingCrop[j].quantity)
                            }
                        }
                    }
                }
            } else {
                existingCrop = farmer.crops;
            }

            await transaction.update(fpoRef, { crops: existingCrop });
            console.log(existingCrop);
            await transaction.set(farmerRef, {
                ID: farmerID,
                name: farmer.name,
                city: farmer.city,
                crops: farmer.crops,
                state: farmer.state,
                phoneNumber: farmer.phoneNumber,
                landSize: farmer.landSize
            });
        });
    }

}
