import { Crop } from './crops';

export interface FarmerDetails {
	ID: string;
	name: string;
	crops: Array<Crop>;
	city: string;
	state: string;
	phoneNumber: string;
	landSize: number;
}

export interface FpoDetails {
	ID: string;
	name: string;
	locality: string;
	crops: Array<Crop>;
	phoneNumber: string;
	// numberOfFarmers: number;
}