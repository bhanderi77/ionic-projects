export interface Crop {
    name: string;
    quantity: number;
}