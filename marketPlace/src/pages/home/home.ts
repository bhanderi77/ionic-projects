import { Component } from '@angular/core';
import { NavController, ModalController, NavParams } from 'ionic-angular';
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { FpoPage } from '../../pages/fpo/fpo';
import { User } from '../../model/user';
import { AuthProvider } from '../../providers/auth/auth';
import { InvestorPage } from '../investor/investor';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomePage {

	fpoList: Array<FpoDetails>;
	user : User;

	constructor(private navCtrl: NavController,
		private modalCtrl: ModalController,
		public navParams: NavParams,
		private authProvider: AuthProvider,
		private firebaseProvider: FirebaseProvider) {
		
		this.user =	navParams.get("user");
	}

	ionViewWillEnter() {
		this.loadFpo();
	}

	async logout() {
		await this.authProvider.logoutGoogle();
		this.user.ID = '';
		this.user.email = '';
		this.user.name = '';
		this.user.role = "buyer";
		this.user.isAdmin = false;

        this.navCtrl.setRoot(InvestorPage);

    }

	loadFpo() {
		let count: number = 0;
		this.firebaseProvider.getFpoList().valueChanges()
			.subscribe((fpoListFromDB) => {
				this.fpoList = [];
				count = fpoListFromDB.length;
				// console.log("length = " + count);
				for (let i = 0; i < fpoListFromDB.length; i++) {
					let fpo = {} as FpoDetails;
					fpo.ID = fpoListFromDB[i].ID;
					fpo.name = fpoListFromDB[i].name;
					fpo.crops = fpoListFromDB[i].crops;
					fpo.locality = fpoListFromDB[i].locality;
					fpo.phoneNumber = fpoListFromDB[i].phoneNumber;
					this.fpoList.push(fpo);
				}
				console.log(this.fpoList);
			})
	}

	addFpo() {
		console.log("Entered addFpo()");
		const modal = this.modalCtrl.create('AddFpoPage');
		modal.present();
		console.log("Leaving addFpo()");
	}

	openFpo(fpo: FpoDetails) {
		console.log("Entered openFpo()");
		this.navCtrl.push(FpoPage, { "fpo": fpo , "sentFrom" : "homePage"});
		console.log("Leaving openFpo()");
	}

}
