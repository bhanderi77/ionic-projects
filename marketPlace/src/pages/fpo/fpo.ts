import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';

/**
 * Generated class for the FpoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-fpo',
    templateUrl: 'fpo.html',
})
export class FpoPage {

    fpo = {} as FpoDetails;
    numberOfFarmers: number;
    farmerList: Array<FarmerDetails>;
    displayList: string;
    sentFrom: string;


    constructor(public navCtrl: NavController,
        private modalCtrl: ModalController,
        private firebaseProvider: FirebaseProvider,
        public navParams: NavParams) {

        this.sentFrom = navParams.get("sentFrom");
        this.displayList = "farmers";
        this.fpo = navParams.get("fpo");
        console.log(this.fpo);

    }

    ionViewWillEnter() {
        console.log('ionViewWillEnter FpoPage');
        this.loadFarmers();
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad FpoPage');
    }

    loadFarmers() {
        this.firebaseProvider.getFarmerList(this.fpo.ID).valueChanges()
            .subscribe(farmerListFromDB => {
                this.farmerList = [];
                this.numberOfFarmers = farmerListFromDB.length;
                for (let i = 0; i < farmerListFromDB.length; i++) {
                    let farmer = {} as FarmerDetails;
                    farmer.name = farmerListFromDB[i].name;
                    farmer.city = farmerListFromDB[i].city;
                    farmer.state = farmerListFromDB[i].state;
                    farmer.crops = farmerListFromDB[i].crops;
                    farmer.phoneNumber = farmerListFromDB[i].phoneNumber;
                    farmer.landSize = farmerListFromDB[i].landSize;
                    this.farmerList.push(farmer);
                }
                console.log('farmerlist : ', this.farmerList);
            })
    }

    addFarmer() {
        const modal = this.modalCtrl.create('AddFarmerPage', { "fpo": this.fpo });
        modal.present();
    }

}
