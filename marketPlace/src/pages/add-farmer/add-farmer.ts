import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { Crop } from '../../model/crops';

@IonicPage()
@Component({
    selector: 'page-add-farmer',
    templateUrl: 'add-farmer.html',
})
export class AddFarmerPage {

    farmer = {} as FarmerDetails;
    fpo = {} as FpoDetails;
    constructor(public navCtrl: NavController,
        public viewCtrl: ViewController,
        public firebaseProvider: FirebaseProvider,
        public navParams: NavParams) {

        this.fpo = navParams.get("fpo");
        console.log(this.fpo);

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AddFarmerPage');
        let tempCrop = {} as Crop;
        tempCrop.name = "";
        tempCrop.quantity = null;
        this.farmer.name = "";
        this.farmer.phoneNumber = "";
        this.farmer.crops = [];
        this.farmer.city = "";
        this.farmer.state = "";
        this.farmer.landSize = null;
        this.farmer.crops = [];
        this.farmer.crops.push(tempCrop);
        console.log(this.farmer);
    }

    async saveFarmer() {
        console.log(this.farmer.crops);
        console.log(this.farmer);
        await this.firebaseProvider.createFarmer(this.fpo, this.farmer);
        this.viewCtrl.dismiss();
        // .then((result) => {
        //     if (result === true)
        //         this.viewCtrl.dismiss();
        // });
    }

    addCrop() {
        let tempCrop = {} as Crop;
        tempCrop.name = "";
        tempCrop.quantity = null;
        this.farmer.crops.push(tempCrop);
        console.log(this.farmer.crops);
    }

    removeCrop() {
        this.farmer.crops.pop();
    }

    goToRootPage() {
        this.viewCtrl.dismiss();
    }


}
