import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { FpoPage } from '../fpo/fpo';

@IonicPage()
@Component({
    selector: 'page-add-fpo',
    templateUrl: 'add-fpo.html',
})
export class AddFpoPage {

    fpo = {} as FpoDetails;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        private firebaseProvider: FirebaseProvider) {

    }

    ionViewWillLoad() {
        this.fpo.ID = "";
        this.fpo.name = "";
        this.fpo.locality = "";
        this.fpo.phoneNumber = "";
        this.fpo.crops = [];
        // this.fpo.numberOfFarmers = 0;
        console.log('ionViewDidLoad AddFpoPage');
    }

    saveFpo() {
        console.log("Entering saveFpo()");
        this.firebaseProvider.createFpo(this.fpo).then((result) => {
            if (result === true)
                this.viewCtrl.dismiss();
        });
        console.log("Leaving saveFpo()");
    }

    goToRootPage() {
        this.viewCtrl.dismiss();
    }

}
