import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddFpoPage } from './add-fpo';

@NgModule({
  declarations: [
    AddFpoPage,
  ],
  imports: [
    IonicPageModule.forChild(AddFpoPage),
  ],
})
export class AddFpoPageModule {}
