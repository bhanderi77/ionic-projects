import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FarmerDetails, FpoDetails } from '../../model/farmer';
import { FirebaseProvider } from '../../providers/firebase/firebase';
import { FpoPage } from '../../pages/fpo/fpo';
import { AuthProvider } from '../../providers/auth/auth';
import { User } from '../../model/user';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
    selector: 'page-investor',
    templateUrl: 'investor.html',
})
export class InvestorPage {

    private user: User;

    fpoList: Array<FpoDetails>;
    searchedFpoList: Array<FpoDetails>;
    searchInput: string;
    constructor(public navCtrl: NavController,
        private authProvider: AuthProvider,
        public navParams: NavParams,
        private firebaseProvider: FirebaseProvider) {

        this.searchInput = "";

        this.user = {} as User;
		this.user.ID = "";
		this.user.name = "";
		this.user.email = "";
		this.user.role = "investor";
		this.user.isAdmin = false;

    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad InvestorPage');
    }

    ionViewWillEnter() {
        this.loadFpo();
    }

    loadFpo() {
        this.firebaseProvider.getFpoList().valueChanges()
            .subscribe((fpoListFromDB) => {
                this.fpoList = [];
                this.searchedFpoList = [];
                for (let i = 0; i < fpoListFromDB.length; i++) {
                    let fpo = {} as FpoDetails;
                    fpo.ID = fpoListFromDB[i].ID;
                    fpo.name = fpoListFromDB[i].name;
                    fpo.crops = fpoListFromDB[i].crops;
                    fpo.locality = fpoListFromDB[i].locality;
                    fpo.phoneNumber = fpoListFromDB[i].phoneNumber;
                    this.fpoList.push(fpo);
                    this.searchedFpoList.push(fpo);
                }
            })
    }

    async login() {
		this.authProvider.getUser().then((currentUser) => {
			this.user.ID = currentUser.ID;
			this.user.name = currentUser.name;
			this.user.email = currentUser.email;
			this.user.isAdmin = currentUser.isAdmin;
			this.user.role = currentUser.role;
			console.log("user : " + this.user.role + " " + this.user.name + " " + this.user.email);
        
            if(this.user.isAdmin === true){
                this.navCtrl.setRoot(HomePage,{"user":this.user});
            }
            
        });
    
    }

    async logout() {
		await this.authProvider.logoutGoogle();
		this.user.ID = '';
		this.user.email = '';
		this.user.name = '';
		this.user.role = "buyer";
		this.user.isAdmin = false;

        this.navCtrl.setRoot(InvestorPage);

    }

    onSearch() {
        this.searchedFpoList = this.fpoList.filter((item) => {
            for (let i = 0; i < item.crops.length; i++) {
                if (item.crops[i].name.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1) {
                    return true;
                }
            }
        });
        console.log(this.searchedFpoList);
    }

    openFpo(fpo: FpoDetails) {
        console.log("Entered openFpo()");
        this.navCtrl.push(FpoPage, { "fpo": fpo, "sentFrom": "investorPage" });
        console.log("Leaving openFpo()");
    }

}
