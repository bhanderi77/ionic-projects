//import { AngularFireModule } from 'angularfire2';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import * as firebase from 'firebase';
//import { AngularFireDatabase } from 'angularfire2/database';
//import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';

import { packageRule } from '../../model/packageRule';
import { DateTime } from 'ionic-angular/components/datetime/datetime';
import { QuerySnapshot, DocumentReference, WriteBatch } from '@firebase/firestore-types';
import { packagedItem } from '../../model/packagedItem';
import { myConstants } from '../../model/myConstants';


/*
  Generated class for the InventoryProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class InventoryProvider {
  //private fs:any;
  constructor(private afs:AngularFirestore) {
   console.log('Hello InventoryProvider Provider');
  }


  getPackageRulesList():AngularFirestoreCollection<packageRule>
  {
    return this.afs.collection<packageRule>('/packageRulesList', ref => ref.orderBy('creationt_time')); 
  }

  createPackageRule(p:packageRule): Promise<void>
  {
    console.log("Welcome to InventoryProvider:createPackageRule(packageRule)")
    const ruleID: string = this.afs.createId();
   // console.log("FieldValue.serverTimestamp():" + firebase.firestore.FieldValue.serverTimestamp());
    console.log("new Date().toLocaleString():" + new Date().toLocaleString());
    return this.afs.doc(`/packageRulesList/${ruleID}`)
      .set({
        id:ruleID,
        server_time:firebase.firestore.FieldValue.serverTimestamp(),
        creationt_time:new Date().toLocaleString(),    
        curTaskCode: p.curTaskCode,
        //packageStatus: p.packageStatus,
        nextTaskCode: p.nextTaskCode,
        bGroupRead: p.bGroupRead,
        bMultipleOutput: p.bMultipleOutput ,
        bPartialAssign: p.bPartialAssign,
        bReducedOutput: p.bReducedOutput,
        bIncursCost: p.bIncursCost            
      }); 
  }
  
  async createPackageRuleArray(pRules:Array<packageRule>): Promise<void>
  {
    console.log("Welcome to InventoryProvider:createPackageRuleArray(...)")
    for(let i=0;i<pRules.length;i++)
    {
      const ruleID: string = this.afs.createId();
      let newDoc:AngularFirestoreDocument<packageRule>; 
      await this.afs.doc(`/packageRulesList/${ruleID}`)
        .set({
          id:ruleID,
          curTaskCode: pRules[i].curTaskCode,
          //packageStatus: pRules[i].packageStatus,
          nextTaskCode: pRules[i].nextTaskCode,
          bGroupRead: pRules[i].bGroupRead,
          bMultipleOutput: pRules[i].bMultipleOutput ,
          bPartialAssign: pRules[i].bPartialAssign,
          bReducedOutput: pRules[i].bReducedOutput,
          bIncursCost: pRules[i].bIncursCost            
        });
      console.log("added for i: " + i);
    };
     
  }

  getPackageRule(pID:string):Promise<any>
  { 
    let pRule = {} as packageRule;            
    let docRef = this.afs.doc(`/packageRulesList/${pID}`).ref;
    return docRef.get().then(doc => {
        if (!doc.exists) {
            console.log('No such document!' + doc.id);
            return null;
        } else {
            pRule.id = doc.id;
            pRule.curTaskCode = doc.data().curTaskCode;
            return pRule;
        }
    })
    .catch(err => {
        console.log('Error getting document', err);
    });
  }

  getPackageRule_For_A(taskCode:string):Promise<packageRule[]>
  { 
    console.log("Welcome to InventoryProvider::getPackageRule_For_A(nextTaskCode) for " + taskCode);
    let pRules:Array<packageRule>;
    pRules = [];
    return new Promise((resolve,reject)=>{
      this.afs.collection<packageRule>('/packageRulesList')
      .ref.where('curTaskCode','==',taskCode)
      .onSnapshot(querySnapshot => {
        console.log("querySnapshot.size: " + querySnapshot.size);
        querySnapshot.forEach(doc => {
          console.log("querySnapshot.doc: " + doc.data().id);
          let pRule = {} as packageRule;
          pRule.id = doc.data().id;
          pRule.creationt_time = doc.data().creationt_time;
          pRule.curTaskCode = doc.data().curTaskCode;
          //pRule.packageStatus = doc.data().packageStatus;    
          pRule.nextTaskCode = doc.data().nextTaskCode;
          pRule.bGroupRead = doc.data().bGroupRead;
          pRule.bMultipleOutput = doc.data().bMultipleOutput;
          pRule.bPartialAssign = doc.data().bPartialAssign;
          pRule.bReducedOutput = doc.data().bReducedOutput;
          pRule.bIncursCost = doc.data().bIncursCost;
        
          pRules.push(pRule);
        });
        console.log("pRules.length " + pRules.length);
        resolve(pRules);
      });
    });
  }


  /*
  get_AtoB_packagedItems_For_A(curTaskCode:string):AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList',
                                            ref => ref.where('pRule.curTaskCode','==',curTaskCode)
                                                      .where('pRule.nextTaskCode','!=',curTaskCode)); 
  }*/

  get_AtoB_packagedItems():AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList_AB'); 
  }

  get_packagedItems_For_B(taskCode:string):AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList_AB',
                                            ref => ref.where('pRule.nextTaskCode','==',taskCode)); 
  }

  get_packagedItems_For_A(taskCode:string):AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList_AB',
                                            ref => ref.where('pRule.curTaskCode','==',taskCode)); 
  }

  get_BtoB_IP_packagedItems(taskCode:string):AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList_BB',
                                            ref => ref.where('pRule.curTaskCode','==',taskCode)
                                                      .where('pRule.nextTaskCode','==',taskCode)
                                                      .where('packageStatus','==',myConstants.packageStatus.IP)); 
  }

  get_BtoB_CO_packagedItems(taskCode:string):AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList_BB',
                                            ref => ref.where('pRule.curTaskCode','==',taskCode)
                                                      .where('pRule.nextTaskCode','==',taskCode)
                                                      .where('packageStatus','==',myConstants.packageStatus.CO)); 
  }

  get_BtoB_packagedItems(taskCode:string):AngularFirestoreCollection<packagedItem>
  { 
    return this.afs.collection<packagedItem>('/packagedItemList_BB',
                                            ref => ref.where('pRule.curTaskCode','==',taskCode)
                                                      .where('pRule.nextTaskCode','==',taskCode)); 
  }

  deletePackageRule(pID:string):Promise<void>
  { 
    return this.afs.doc(`/packageRulesList/${pID}`).delete();
  }
  
  updatePackageRule(p:packageRule):Promise<void>
  {
    console.log("Welcome to InventoryProvider:updatePackageRule(packageRule) " + p.id + " " + p.bGroupRead);
    const docRef:AngularFirestoreDocument<packageRule> = this.afs.doc(`/packageRulesList/${p.id}`);
    return docRef.update({bGroupRead: !p.bGroupRead});
  }

  async createPackagedItem_BtoC(pItem:packagedItem,ppID:string): Promise<void>
  {
    console.log("Welcome to InventoryProvider:createPackagedItem(packagedItem)")
    console.log("pItem=>" + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
    console.log("pItem=>" + pItem.itemWeight + " " + pItem.itemQty + " " + pItem.itemCost + " " + pItem.itemTax);
    console.log("pItem=>" + pItem.parentPackageID + " " + pItem.packageID + " " + pItem.packageGroupID);
    
    //const ID: string = this.afs.createId();
    const ID: string = pItem.pRule.curTaskCode + "_" + pItem.pRule.nextTaskCode + "_" + (new Date().toISOString());
    
   // console.log("FieldValue.serverTimestamp():" + firebase.firestore.FieldValue.serverTimestamp());
    console.log("ID " + ID);
    //return new Promise((resolve,reject)=>{  
      let docRef:DocumentReference = this.afs.doc(`/packagedItemList_AB/${ID}`).ref;
      console.log("docRef: " + docRef.id);
      let batch:WriteBatch = this.afs.firestore.batch();
      if(docRef)
      {
        console.log("Calling batch.set for docRef " + docRef.id);
        await batch.set(docRef,{
          id:ID,
          creationt_time:new Date().toLocaleString(),
          packageID:pItem.packageID,
          parentPackageID:pItem.parentPackageID,
          packageGroupID:pItem.packageGroupID,
          packageStatus:pItem.packageStatus,
          pRule:pItem.pRule,
          itemWeight:pItem.itemWeight,
          itemQty:pItem.itemQty,
          itemCost:pItem.itemCost,
          itemTax:pItem.itemTax,
          itemRemarks:pItem.itemRemarks    
        });
      }  

      if(ppID !== "0")
      {
        console.log("Parent exists");
        let parentDocRef:DocumentReference = this.afs.doc(`/packagedItemList_BB/${ppID}`).ref
        await batch.update(parentDocRef,{packageStatus:myConstants.packageStatus.CO});
      }
      await batch.commit();
      console.log("committed");
      /*
      return this.afs.doc(`/packagedItemList/${ID}`)
        .set({
          id:ID,
          creationt_time:new Date().toLocaleString(),
          packageID:pItem.packageID,
          parentPackageID:pItem.parentPackageID,
          packageGroupID:pItem.packageGroupID,
          packageStatus:pItem.packageStatus,
          pRule:pItem.pRule,
          itemWeight:pItem.itemWeight,
          itemQty:pItem.itemQty,
          itemCost:pItem.itemCost,
          itemTax:pItem.itemTax,
          itemRemarks:pItem.itemRemarks    
        }).then(() => {
          if(pItem.parentPackageID)
          {

          }
        });*/ 
  }

  async createPackagedItem_BtoB(pItem:packagedItem,ppIDList:Array<packagedItem>): Promise<void>
  {

    console.log("Welcome to InventoryProvider:createPackagedItem(packagedItem)")
    console.log("pItem=>" + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
    console.log("pItem=>" + pItem.itemWeight + " " + pItem.itemQty + " " + pItem.itemCost + " " + pItem.itemTax);
    console.log("pItem=>" + pItem.parentPackageID + " " + pItem.packageID + " " + pItem.packageGroupID);
    //const ID: string = this.afs.createId();
    const ID: string = pItem.pRule.curTaskCode + "_" + pItem.pRule.nextTaskCode + "_" + (new Date().toISOString());
   // console.log("FieldValue.serverTimestamp():" + firebase.firestore.FieldValue.serverTimestamp());
    console.log("ID " + ID);
    //return new Promise((resolve,reject)=>{  
    let docRef:DocumentReference = this.afs.doc(`/packagedItemList_BB/${ID}`).ref;
    console.log("docRef: " + docRef.id);
    let batch:WriteBatch = this.afs.firestore.batch();
    if(docRef)
    {
      console.log("Calling batch.set for docRef " + docRef.id);
      await batch.set(docRef,{
        id:ID,
        creationt_time:new Date().toLocaleString(),
        packageID:pItem.packageID,
        parentPackageID:pItem.parentPackageID,
        packageGroupID:pItem.packageGroupID,
        packageStatus:pItem.packageStatus,
        pRule:pItem.pRule,
        itemWeight:pItem.itemWeight,
        itemQty:pItem.itemQty,
        itemCost:pItem.itemCost,
        itemTax:pItem.itemTax,
        itemRemarks:pItem.itemRemarks    
      });
    }  

    if(ppIDList)
    {
      for ( let i = 0 ; i < ppIDList.length ; i++ ){
        console.log("Parent exists : " + ppIDList[i].id);
        let parentDocRef:DocumentReference = this.afs.doc(`/packagedItemList_AB/${ppIDList[i].id}`).ref;
        console.log("parent Doc ref : " + parentDocRef.id);
        await batch.update(parentDocRef,{packageStatus:myConstants.packageStatus.CO});
      } 
      
    }
    await batch.commit();
    console.log("committed");
  }

  // async createPackagedItemArray_BtoB(pItemList:Array<packagedItem>,ppID:string):Promise<void> 
  // {
  //   console.log("Welcome to InventoryProvider:createPackagedItemArray_BtoB(packagedItem)")
  //   console.log("length of pItemList " + pItemList.length);
  //   let batch : WriteBatch = this.afs.firestore.batch();
  //   for ( let i = 0 ; i < pItemList.length ; i++ )
  //   {
  //     const ID : string = pItemList[i].pRule.curTaskCode + "_" + pItemList[i].pRule.nextTaskCode + "_" + this.afs.createId();
  //     let docRef : DocumentReference = this.afs.doc(`/packagedItemList_BB/${ID}`).ref;
  //     console.log("docRef : " + docRef.id);
  //     if(docRef){
  //       console.log("Calling batch.set for docRef " + docRef.id);
  //       await batch.set(docRef,{
  //         id:ID,
  //         creationt_time:new Date().toLocaleString(),
  //         packageID:pItemList[i].packageID,
  //         parentPackageID:pItemList[i].parentPackageID,
  //         packageGroupID:pItemList[i].packageGroupID,
  //         packageStatus:pItemList[i].packageStatus,
  //         pRule:pItemList[i].pRule,
  //         itemWeight:pItemList[i].itemWeight,
  //         itemQty:pItemList[i].itemQty,
  //         itemCost:pItemList[i].itemCost,
  //         itemTax:pItemList[i].itemTax,
  //         itemRemarks:pItemList[i].itemRemarks
  //       });
  //     }
  //   };
  //   if( ppID !== "" ){
  //     console.log("Parents exists");
  //     let parentDocRef:DocumentReference = this.afs.doc(`/packagedItemList_AB`).ref
  //     await batch.update(parentDocRef,{packageStatus:myConstants.packageStatus.CO});
  //   }
  //   await batch.commit();
  //   console.log("committed");
  // }

  async createPackagedItemArray_BtoC(pItemList:Array<packagedItem>,ppID:string):Promise<void>
  {
    console.log("Welcome to InventoryProvider:createPackagedItemArray_BtoC(packagedItem)")
    console.log("length of pItemList " + pItemList.length);
    //await pItemList.forEach(pItem => 
    let batch:WriteBatch = this.afs.firestore.batch();
    for(let i=0;i<pItemList.length;i++)
    {
    //  const ID: string = this.afs.createId();
    const ID: string = pItemList[i].pRule.curTaskCode + "_" + pItemList[i].pRule.nextTaskCode + "_" + this.afs.createId();
      // const ID: string = pItemList[i].pRule.curTaskCode + "_" + pItemList[i].pRule.nextTaskCode + "_" + (new Date().toISOString());
      // console.log('before timeout : '+new Date().valueOf());
      // await setTimeout(3000);
      // console.log('after timeout : '+new Date().valueOf());

      let docRef:DocumentReference = this.afs.doc(`/packagedItemList_AB/${ID}`).ref;
      console.log("docRef: " + docRef.id);
      if(docRef)
      {
        console.log("Calling batch.set for docRef " + docRef.id);
        await batch.set(docRef,{
          id:ID,
          creationt_time:new Date().toLocaleString(),
          packageID:pItemList[i].packageID,
          parentPackageID:pItemList[i].parentPackageID,
          packageGroupID:pItemList[i].packageGroupID,
          packageStatus:pItemList[i].packageStatus,
          pRule:pItemList[i].pRule,
          itemWeight:pItemList[i].itemWeight,
          itemQty:pItemList[i].itemQty,
          itemCost:pItemList[i].itemCost,
          itemTax:pItemList[i].itemTax,
          itemRemarks:pItemList[i].itemRemarks    
        });
      }
    };
    if(ppID !== "")
    {
      console.log("Parent exists");
      let parentDocRef:DocumentReference = this.afs.doc(`/packagedItemList_BB/${ppID}`).ref
      await batch.update(parentDocRef,{packageStatus:myConstants.packageStatus.CO});
    }
    await batch.commit();
    console.log("committed");  
    return;
  }
  

  updateStatus_For_AtoB_PackagedItem(pItemID:string,status:myConstants.packageStatus):Promise<void>
  {
    console.log("Welcome to InventoryProvider:updatePackageRule(packageRule) " + pItemID);
    const docRef:AngularFirestoreDocument<packagedItem> = this.afs.doc(`/packagedItemList_AB/${pItemID}`);
    return docRef.update({packageStatus : status});
  }

  async updateStatus_For_AtoB_PackagedItemList(pItemList:Array<packagedItem>,group:string,status:myConstants.packageStatus):Promise<any>
  {
    console.log("Welcome to InventoryProvider:updatePackagedItemList(group) " + group);

    //TBD : This need to be transaction
    pItemList.forEach(pItem => {
      const docRef:AngularFirestoreDocument<packagedItem> = this.afs.doc(`/packagedItemList_AB/${pItem.id}`);
      docRef.update({packageStatus : status});
    });
    
  }
}
