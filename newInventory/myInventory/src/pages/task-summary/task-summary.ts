import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController, NavParams,AlertController,Loading,LoadingController } from 'ionic-angular';
//import { DatabaseProvider } from '../../providers/database/database';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { taskSummary,summaryDetails } from '../../model/taskSummary';
import { packagedItem } from '../../model/packagedItem';
import { packageRule } from '../../model/packageRule';
import { myConstants } from '../../model/myConstants';
import { Observable } from 'rxjs/Observable';
//import { Observable } from 'rxjs/Rx';
import 'rxjs/add/Observable/of';
//import { ReplaySubject } from 'rxjs';


@IonicPage()
@Component({
  selector: 'page-task-summary',
  templateUrl: 'task-summary.html',
})
export class TaskSummaryPage {
  private taskCode:string;
  private filterValue:string;
  private allPackagedItemList:Array<packagedItem>;
//  private packagedItemList:Observable<packagedItem[]>;
  private packagedItemList:Array<packagedItem>;
//  private packagedItemList:Observable<packagedItem>;

/*
  private pItemListAtoB:Observable<packagedItem[]>;
  private pItemListBtoB_IP:Observable<packagedItem[]>;
  private pItemListBtoB_CO:Observable<packagedItem[]>;
  private pItemListBtoC:Observable<packagedItem[]>;
*/
  private pItemListAtoB:Array<packagedItem>;
  private pItemListBtoB_IP:Array<packagedItem>;
  private pItemListBtoB_CO:Array<packagedItem>;
  private pItemListBtoC:Array<packagedItem>;
/*
  private pItemListAtoB:Observable<packagedItem>;
  private pItemListBtoB_IP:Observable<packagedItem>;
  private pItemListBtoB_CO:Observable<packagedItem>;
  private pItemListBtoC:Observable<packagedItem>;
*/
  private summary:taskSummary;
  private selectedPackageIDList:Array<number>;

  constructor(public navCtrl: NavController, private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,    
    private navParams: NavParams,private inventoryProvider: InventoryProvider) {
    this.taskCode = navParams.get("taskCode");
    this.selectedPackageIDList = [];
    //this.packagedItemList = [];
    this.allPackagedItemList = [];
    this.resetSummary();   
  
  /*
    this.pItemListAtoB = this.inventoryProvider.get_packagedItems_For_B(this.taskCode).valueChanges();
    this.pItemListBtoC = this.inventoryProvider.get_packagedItems_For_A(this.taskCode).valueChanges();
    this.pItemListBtoB_IP = this.inventoryProvider.get_BtoB_IP_packagedItems(this.taskCode).valueChanges();
    this.pItemListBtoB_CO = this.inventoryProvider.get_BtoB_CO_packagedItems(this.taskCode).valueChanges();
  */
    this.inventoryProvider.get_packagedItems_For_B(this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged");
      this.pItemListAtoB = pItemList;
      this.filterPackagedItems(this.filterValue);
    });
    this.inventoryProvider.get_packagedItems_For_A(this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged");
      this.pItemListBtoC = pItemList;
      this.filterPackagedItems(this.filterValue);
    });
    this.inventoryProvider.get_BtoB_IP_packagedItems(this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged");    
      this.pItemListBtoB_IP = pItemList;
      this.filterPackagedItems(this.filterValue);
    });
    this.inventoryProvider.get_BtoB_CO_packagedItems(this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged");    
      this.pItemListBtoB_CO = pItemList;
      this.filterPackagedItems(this.filterValue);
    });
  /*
    this.inventoryProvider.get_packagedItems_For_B(this.taskCode).valueChanges().subscribe(pItemList => {
      console.log("valueChanged");
      this.pItemListAtoB = Observable.from(pItemList);
      });
    this.inventoryProvider.get_packagedItems_For_A(this.taskCode).valueChanges().subscribe(pItemList => {this.pItemListBtoC = Observable.from(pItemList);console.log("valueChanged");});
    this.inventoryProvider.get_BtoB_IP_packagedItems(this.taskCode).valueChanges().subscribe(pItemList => {this.pItemListBtoB_IP = Observable.from(pItemList);console.log("valueChanged");});
    this.inventoryProvider.get_BtoB_CO_packagedItems(this.taskCode).valueChanges().subscribe(pItemList => {this.pItemListBtoB_CO = Observable.from(pItemList);console.log("valueChanged");});
  */
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskSummaryPage');
    this.filterValue = "A_B";
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter TaskSummaryPage');
  //  this.filterValue = "A_B";            
    // this.initializeData();    
//     this.filterPackagedItems(this.filterValue);
  }

  TrackBy(index: number, obj: any):any
  {
    console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  async filterPackagedItems(value)
  {
    console.log("Welcome to filterPackagedItems(value): " + value);
   // console.log("this.packagedItemList.length " + this.packagedItemList.length);
    switch(value)
    {
      case "A_B": {
        /*
        this.pItemListAtoB = await this.inventoryProvider.get_packagedItems_For_B(this.taskCode).valueChanges();
    
        this.pItemListAtoB.subscribe(pItemList => {
          console.log("pItemListAtoB.length " + pItemList.length);
          this.packagedItemList = [];        
          this.packagedItemList = pItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode !== packagedItem.pRule.curTaskCode) && (packagedItem.pRule.nextTaskCode === this.taskCode)));
        });
        */
       this.packagedItemList = this.pItemListAtoB;
        break;
      }
      case "B_B_IP": {
        /*
        //this.packagedItemList = this.allPackagedItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode === packagedItem.pRule.curTaskCode) && (packagedItem.pRule.nextTaskCode === this.taskCode) && (packagedItem.packageStatus === myConstants.packageStatus.IP)));
        this.pItemListBtoB = await this.inventoryProvider.get_BtoB_packagedItems(this.taskCode).valueChanges();
        
        this.pItemListBtoB.subscribe(pItemList => {
          console.log("pItemListBtoB.length IP " + pItemList.length);
          this.packagedItemList = [];        
          this.packagedItemList = pItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode === packagedItem.pRule.curTaskCode) && (packagedItem.pRule.nextTaskCode === this.taskCode) && (packagedItem.packageStatus === myConstants.packageStatus.IP)));
        });
        */
       this.packagedItemList = this.pItemListBtoB_IP;
        break;
      }
      case "B_B_CO": {
        /*
        //this.packagedItemList = this.allPackagedItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode === packagedItem.pRule.curTaskCode) && (packagedItem.pRule.nextTaskCode === this.taskCode) && (packagedItem.packageStatus === myConstants.packageStatus.CO)));
        this.pItemListBtoB = await this.inventoryProvider.get_BtoB_packagedItems(this.taskCode).valueChanges();
    
        this.pItemListBtoB.subscribe(pItemList => {
          console.log("pItemListBtoB.length CO " + pItemList.length);
          this.packagedItemList = [];
          this.packagedItemList = pItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode === packagedItem.pRule.curTaskCode) && (packagedItem.pRule.nextTaskCode === this.taskCode) && (packagedItem.packageStatus === myConstants.packageStatus.CO)));
        });
        */
       this.packagedItemList = this.pItemListBtoB_CO;
        break;
      }
      case "B_C": {
        /*
        //this.packagedItemList = this.allPackagedItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode !== packagedItem.pRule.curTaskCode) && (packagedItem.pRule.curTaskCode === this.taskCode)));
        this.pItemListBtoC = await this.inventoryProvider.get_packagedItems_For_A(this.taskCode).valueChanges();
    
        this.pItemListBtoC.subscribe(pItemList => {
          console.log("pItemListBtoC.length " + pItemList.length);
          this.packagedItemList = [];        
          this.packagedItemList = pItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode !== packagedItem.pRule.curTaskCode) && (packagedItem.pRule.curTaskCode === this.taskCode)));
        });*/
        this.packagedItemList = this.pItemListBtoC;    
        break;
      }
      default:
    }
  }

  resetSummary()
  {
    this.summary = {} as taskSummary;
    this.summary.taskCode = this.taskCode;
    this.summary.Input = {} as summaryDetails;
    this.summary.Input.totalQty=0;
    this.summary.Input.totalWeight=0;
    this.summary.Input.totalCost=0;
    this.summary.Input.totalTax=0;

    this.summary.InProgress = {} as summaryDetails;
    this.summary.InProgress.totalQty=0;
    this.summary.InProgress.totalWeight=0;
    this.summary.InProgress.totalCost=0;
    this.summary.InProgress.totalTax=0;

    this.summary.Completed = {} as summaryDetails;
    this.summary.Completed.totalQty=0;
    this.summary.Completed.totalWeight=0;
    this.summary.Completed.totalCost=0;
    this.summary.Completed.totalTax=0;

    this.summary.Pending = {} as summaryDetails;
    this.summary.Pending.totalQty=0;
    this.summary.Pending.totalWeight=0;
    this.summary.Pending.totalCost=0;
    this.summary.Pending.totalTax=0;

    this.summary.Output = {} as summaryDetails;
    this.summary.Output.totalQty=0;
    this.summary.Output.totalWeight=0;
    this.summary.Output.totalCost=0;
    this.summary.Output.totalTax=0;
  }

  async initializeData()
  {
    
    //let rule = {} as packageRule;
     /*    
    this.pItemListAtoB = await this.inventoryProvider.get_packagedItems_For_B(this.taskCode).valueChanges();
    this.pItemListBtoC = await this.inventoryProvider.get_packagedItems_For_A(this.taskCode).valueChanges();
    this.pItemListBtoB = await this.inventoryProvider.get_BtoB_IP_packagedItems(this.taskCode).valueChanges();
   
    await this.pItemListAtoB.subscribe(pItemList => {
      console.log("pItemListAtoB.length " + pItemList.length);
      this.packagedItemList = [];        
      this.packagedItemList = pItemList.filter(packagedItem=>((packagedItem.pRule.nextTaskCode !== packagedItem.pRule.curTaskCode) && (packagedItem.pRule.nextTaskCode === this.taskCode)));
    });
    */
    /*
    await this.pItemListAtoB.subscribe(pItemList => {
      this.selectedPackageIDList = [];
      this.selectedPackageIDList = new Array(pItemList.length);
      console.log("Inside pItemListAtoB.subscribe :" + pItemList.length);

      pItemList.forEach(pItem => {
        console.log(pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
        if((pItem.pRule.nextTaskCode !== pItem.pRule.curTaskCode) 
        && (pItem.pRule.nextTaskCode === this.taskCode))
        {
          this.allPackagedItemList.push(pItem);          
        }
     //   this.buildSummary(); 
      //  this.filterPackagedItems(this.filterValue);
      });
    });
    
    await this.pItemListBtoC.subscribe(pItemList => {
      console.log("Inside pItemListBtoC.subscribe :" + pItemList.length);
      
      pItemList.forEach(pItem => {
        console.log(pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
        if((pItem.pRule.nextTaskCode !== pItem.pRule.curTaskCode) 
        && (pItem.pRule.curTaskCode === this.taskCode))
        {
          this.allPackagedItemList.push(pItem);
        }
        //this.buildSummary(); 
     //   this.filterPackagedItems(this.filterValue);
      });
    });

    await this.pItemListBtoB.subscribe(pItemList => {
      console.log("Inside pItemListBtoB.subscribe :" + pItemList.length);
      
      pItemList.forEach(pItem => {
        console.log(pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode);
        if((pItem.pRule.nextTaskCode === pItem.pRule.curTaskCode) 
        && (pItem.pRule.curTaskCode === this.taskCode))
        {
          this.allPackagedItemList.push(pItem);
        }
        //this.buildSummary(); 
   //     this.filterPackagedItems(this.filterValue);
      });
    });*/
    
  }

  buildSummary()
  {/*
    console.log("TaskSummaryPage: Welcome to buildSummary()");
    this.resetSummary();
    //for(let i=0;i<this.pItemListAtoB.)
    this.pItemListAtoB.subscribe(pItemList => {
      pItemList.forEach(pItem => {
        if((pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
        && ( (pItem.packageStatus === myConstants.packageStatus.RD)
          || (pItem.packageStatus === myConstants.packageStatus.PD)
          || (pItem.packageStatus === myConstants.packageStatus.IP)                
          || (pItem.packageStatus === myConstants.packageStatus.CO)
          ))
        {
          this.summary.Input.totalQty += pItem.itemQty;
          this.summary.Input.totalWeight += pItem.itemWeight;
          this.summary.Input.totalCost += pItem.itemCost;
          this.summary.Input.totalTax += pItem.itemTax;            
        }
      });
    });

    this.pItemListBtoB.subscribe(pItemList => {
      pItemList.forEach(pItem => {
        if((pItem.pRule.curTaskCode === pItem.pRule.nextTaskCode)
        && (pItem.packageStatus === myConstants.packageStatus.IP))
        {//B->B and IP 
          this.summary.InProgress.totalQty += pItem.itemQty;
          this.summary.InProgress.totalWeight += pItem.itemWeight;
          this.summary.InProgress.totalCost += pItem.itemCost;
          this.summary.InProgress.totalTax += pItem.itemTax;
        }
        else if((pItem.pRule.curTaskCode === pItem.pRule.nextTaskCode) 
            && (pItem.packageStatus === myConstants.packageStatus.CO))
        {//B->B and CO
          this.summary.Completed.totalQty += pItem.itemQty;
          this.summary.Completed.totalWeight += pItem.itemWeight;
          this.summary.Completed.totalCost += pItem.itemCost;
          this.summary.Completed.totalTax += pItem.itemTax;
        }
      });
    });

    this.pItemListBtoC.subscribe(pItemList => {
      pItemList.forEach(pItem => {
        if( (pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
        &&  (  (pItem.packageStatus === myConstants.packageStatus.RD)
            || (pItem.packageStatus === myConstants.packageStatus.PD)
            || (pItem.packageStatus === myConstants.packageStatus.CO)
            || (pItem.packageStatus === myConstants.packageStatus.IP)
            ))
          {
            this.summary.Output.totalQty += pItem.itemQty;
            this.summary.Output.totalWeight += pItem.itemWeight;
            this.summary.Output.totalCost += pItem.itemCost;
            this.summary.Output.totalTax += pItem.itemTax;
          }  
      });
    });

    this.summary.Pending.totalQty = this.summary.Input.totalQty - (this.summary.InProgress.totalQty + this.summary.Completed.totalQty);
    this.summary.Pending.totalWeight = this.summary.Input.totalWeight - (this.summary.InProgress.totalWeight + this.summary.Completed.totalWeight);
    this.summary.Pending.totalCost = this.summary.Input.totalCost - (this.summary.InProgress.totalCost + this.summary.Completed.totalCost);
    this.summary.Pending.totalTax = this.summary.Input.totalTax - (this.summary.InProgress.totalTax + this.summary.Completed.totalTax);
  */}

  /****************************************************************
  This method is invoked either 
  Input: A->B RD/PD entries where packageInProgressApplicable is true
  Output : B->C entries with RD status
  ****************************************************************/
  startTask(selPackagedItem:packagedItem)
  {
    console.log("Inside TaskSummaryPage::startTask()");
    console.log("Selected Item=> PackageID: " + selPackagedItem.packageID + " Status: " + selPackagedItem.packageStatus 
          + " Weight: " + selPackagedItem.itemWeight);


    if(   (selPackagedItem.pRule.curTaskCode === selPackagedItem.pRule.nextTaskCode)
      ||  ( (selPackagedItem.pRule.curTaskCode !== selPackagedItem.pRule.nextTaskCode)
          && !(  (selPackagedItem.packageStatus === myConstants.packageStatus.RD)
              ||(selPackagedItem.packageStatus === myConstants.packageStatus.PD))
          )
      )
    {//Invalid entries
      alert(selPackagedItem.pRule.curTaskCode + " " + selPackagedItem.packageStatus + " " + selPackagedItem.pRule.nextTaskCode + " CAN NOT be started");
      return;
    }

    if( !(selPackagedItem.pRule.bGroupRead || selPackagedItem.pRule.bPartialAssign))
    {//A->B RD for everthing to be started in one go
      return this.generateSingleBtoBForSelected(selPackagedItem);     
    }      
    else if( (!selPackagedItem.pRule.bGroupRead) && (selPackagedItem.pRule.bPartialAssign))
    {//A->B RD/PD entries where entire input is not supposed to be started together i.e.Manager assign part of input to someone in team
      console.log("Valid entries i.e. A->B RD/IP");
      let pending = {} as summaryDetails;
      pending.totalQty = selPackagedItem.itemQty;
      pending.totalWeight = selPackagedItem.itemWeight;
      pending.totalCost = selPackagedItem.itemCost;
      pending.totalTax = selPackagedItem.itemTax;

      let relevantPackagedItemList:Array<packagedItem>;
      let pItemListBtoB  = this.inventoryProvider.get_BtoB_packagedItems(selPackagedItem.pRule.nextTaskCode).valueChanges();  
      pItemListBtoB.subscribe(pItemList => {
        relevantPackagedItemList = pItemList.filter(packagedItem=>packagedItem.parentPackageID === selPackagedItem.id);
        relevantPackagedItemList.forEach(relevantPackagedItem => {
          console.log("relevantPackagedItem: " + relevantPackagedItem.packageID
                      + " " + relevantPackagedItem.pRule.curTaskCode 
                      + " " + relevantPackagedItem.packageStatus
                      + " " + relevantPackagedItem.pRule.nextTaskCode
                      + " " + relevantPackagedItem.itemWeight);
          if((relevantPackagedItem.pRule.curTaskCode === selPackagedItem.pRule.nextTaskCode)
            &&(relevantPackagedItem.pRule.curTaskCode === relevantPackagedItem.pRule.nextTaskCode))
          {//if B->B
            if((relevantPackagedItem.packageStatus === myConstants.packageStatus.RD) || (relevantPackagedItem.packageStatus === myConstants.packageStatus.IP) || (relevantPackagedItem.packageStatus === myConstants.packageStatus.CO))
            {
            pending.totalQty -= relevantPackagedItem.itemQty;
            pending.totalWeight -= relevantPackagedItem.itemWeight;
            pending.totalCost -= relevantPackagedItem.itemCost;
            pending.totalTax -= relevantPackagedItem.itemTax;
            }
          }
        });//end of loop over relevantPackagedItemList
      })
      //alert("packagedItem Weight: " + selPackagedItem.itemWeight + " Available: " + pending.totalWeight);
      //Choose how much to complete
      let alert = this.alertCtrl.create({
        title: "Enter WEIGHT(Max " + pending.totalWeight + ") & QTY(Max " + pending.totalQty + ")",
        inputs: [ { name: 'ToBeWeight', placeholder: 'Enter Weight'},
                  { name: 'ToBeQty', placeholder: 'Enter Qty'}
                ],
        buttons:
                [ { 
                    text: 'Cancel',
                    handler : data => { console.log('Cancel clicked');}
                  },
                  { 
                    text: 'Ok',
                    handler : data => 
                    { 
                      console.log('Ok clicked : ' + data.ToBeWeight);
                      if((data.ToBeWeight > pending.totalWeight) || (data.ToBeQty > pending.totalQty ))
                      {
                        return false;
                      }
                      else
                      {
                          
                          /*CREATE NEW ENTRY B->B in IP status and updated status of A->B from RD/PD to PD/CO
                          (01) Create new object of packagedItem
                          (02) Set item Details and packageID related fields
                          (03.1) Set package rule
                          (03.2) set owner details
                          (04) add new entry to DB 
                          (05) Change status to PD/IP for selPackagedItem
                          (06) refresh data for this page
                        */
                        console.log("CREATE NEW ENTRY B->B in IP status");
                        //(01) Create new object of packagedItem
                        let packagedItemBtoB = {} as packagedItem;
                        //(02) Set item Details and packageID related fields                            
                        packagedItemBtoB.itemQty = data.ToBeQty;
                        packagedItemBtoB.itemWeight = data.ToBeWeight;
                        packagedItemBtoB.itemCost = pending.totalCost * (data.ToBeWeight/pending.totalWeight);
                        packagedItemBtoB.itemTax = pending.totalTax * (data.ToBeWeight/pending.totalWeight);
                        packagedItemBtoB.itemRemarks = "";
                        if(packagedItemBtoB.itemWeight == pending.totalWeight)
                        {
                          packagedItemBtoB.packageID = selPackagedItem.packageID + "|";// + packagedItemBtoB.itemWeight;
                          packagedItemBtoB.parentPackageID = selPackagedItem.id;
                          packagedItemBtoB.packageGroupID = selPackagedItem.packageGroupID;
                        }
                        else
                        {
                          packagedItemBtoB.packageID = selPackagedItem.packageID + "|<" + packagedItemBtoB.itemWeight;
                          packagedItemBtoB.parentPackageID = selPackagedItem.id;
                          packagedItemBtoB.packageGroupID = selPackagedItem.packageGroupID;                         
                        }
                        

                        //(03.1) Set package rule
                        packagedItemBtoB.pRule = selPackagedItem.pRule;
                        packagedItemBtoB.pRule.curTaskCode = selPackagedItem.pRule.nextTaskCode;
                        packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
                        //(03.2) set owner details                              
                        packagedItemBtoB.packageCurTaskOwner = selPackagedItem.packageNextTaskOwner;
                        packagedItemBtoB.packageNextTaskOwner = selPackagedItem.packageNextTaskOwner;
                        //(04) add new entry B->B with IP status to DB

                        let loading: Loading;
                        loading = this.loadingCtrl.create();
                        loading.present();

                        this.inventoryProvider.createPackagedItem_BtoB(packagedItemBtoB,[selPackagedItem])
                        .then(()=>{
                          if(packagedItemBtoB.itemWeight == pending.totalWeight)
                          {//everything is started
                            //(05) Change status to CO for selPackagedItem
                            this.inventoryProvider.updateStatus_For_AtoB_PackagedItem(selPackagedItem.id,myConstants.packageStatus.CO).then(res=>{
                              //(06) refresh data for this page
                              //this.initializeData();
                              loading.dismiss();
                              this.filterValue="B_B_IP";
                              this.filterPackagedItems(this.filterValue);              
                            })
                            .catch(e => {
                              console.log("Error from this.databaseProvider.updatePackagedItemStatus() to CO : " + e);
                            });
                          }
                          else if(packagedItemBtoB.itemWeight < pending.totalWeight)
                          {//NOT everything is started
                            //(05) Change status to PD for selPackagedItem
                            this.inventoryProvider.updateStatus_For_AtoB_PackagedItem(selPackagedItem.id,myConstants.packageStatus.PD).then(res=>{
                              //(06) refresh data for this page
                              //this.initializeData();
                              loading.dismiss();
                              this.filterValue="A_B";
                              this.filterPackagedItems(this.filterValue);
                            })
                            .catch(e => {
                              console.log("Error from this.databaseProvider.updatePackagedItemStatus() to PD : " + e);
                            });
                          }
                        })
                        .catch(e=>{
                          console.log("Error from this.databaseProvider.addPackagedItem(packagedItemBtoB) : " + e);
                        });    
                      }
                  }
                }
              ]
        }); 
      alert.present();
    }
    else if(selPackagedItem.pRule.bGroupRead)
    {
      alert("Read Multiple Entries");
    }
  }

  
  async generateSingleBtoBForSelected(pItem:packagedItem)
  {
    console.log("Welcome to TaskSummaryPage::generateSingleBtoBForSelected(pItem) " + pItem.pRule.curTaskCode + " " + pItem.packageStatus + " " + pItem.pRule.nextTaskCode + " " +pItem.itemWeight )
    /*CREATE NEW ENTRY B->B in IP status and updated status of A->B from RD to CO
    (01) Create new object of packagedItem
    (02) Set item Details and packageID related fields
    (03.1) Set package rule
    (03.2) set owner details
    (04) add new entry to DB 
    (05) Change status to CO for selPackagedItem
    (06) refresh data for this page
    */
    console.log("CREATE NEW ENTRY B->B in IP status");

    //(01) Create new object of packagedItem
    let packagedItemBtoB = {} as packagedItem;
    //(02) Set item Details and packageID related fields                            
    packagedItemBtoB.itemQty = pItem.itemQty;
    packagedItemBtoB.itemWeight = pItem.itemWeight;
    packagedItemBtoB.itemCost = pItem.itemCost;
    packagedItemBtoB.itemTax = pItem.itemTax;
    packagedItemBtoB.itemRemarks = pItem.itemRemarks;
    packagedItemBtoB.pRule = {} as packageRule;
    
    if(!(pItem.pRule.bPartialAssign || pItem.pRule.bReducedOutput))
    {
      packagedItemBtoB.packageID = pItem.packageID;;
      packagedItemBtoB.parentPackageID = pItem.id;
      packagedItemBtoB.packageGroupID = pItem.packageGroupID; 
    }
    else
    {
      packagedItemBtoB.packageID = pItem.packageID + "|";// + packagedItemBtoB.itemWeight;
      packagedItemBtoB.parentPackageID = pItem.id;
      packagedItemBtoB.packageGroupID = pItem.packageGroupID;    
    }
    

    //(03.1) Set package rule
    //packagedItemBtoB.pRule = new PackagedRule(pItem.pRule;

    packagedItemBtoB.pRule.curTaskCode = pItem.pRule.nextTaskCode;
    packagedItemBtoB.pRule.nextTaskCode = pItem.pRule.nextTaskCode;
    packagedItemBtoB.pRule.bGroupRead = pItem.pRule.bGroupRead;
    packagedItemBtoB.pRule.bIncursCost = pItem.pRule.bIncursCost;
    packagedItemBtoB.pRule.bMultipleOutput = pItem.pRule.bMultipleOutput;
    packagedItemBtoB.pRule.bPartialAssign = pItem.pRule.bPartialAssign;
    packagedItemBtoB.pRule.bReducedOutput = pItem.pRule.bReducedOutput;
    
    packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
   
    //(03.2) set owner details //TBD                             
    packagedItemBtoB.packageCurTaskOwner = pItem.packageNextTaskOwner;
    packagedItemBtoB.packageNextTaskOwner = pItem.packageNextTaskOwner;

    let loading: Loading;
    loading = this.loadingCtrl.create();
    loading.present();

    //(04) add new entry B->B with IP status to DB
    await this.inventoryProvider.createPackagedItem_BtoB(packagedItemBtoB,[pItem]);
    //(05) Change status to CO for pItem
    await loading.dismiss();
  }


  /****************************************************************
  This method is invoked either 
  from B->B IP entries
  OR A-B entries where packageInProgressApplicable is false
  ****************************************************************/
  async completeTask(selPackagedItem:packagedItem)
  {//A->B or B->B
    console.log("Inside TaskSummaryPage::completeTask()");
    console.log("Selected Item=> PackageID: " + selPackagedItem.packageID + " Status: " + selPackagedItem.packageStatus 
          + " Weight: " + selPackagedItem.itemWeight);

    if(selPackagedItem.packageStatus === myConstants.packageStatus.CO)
    {//Already in CO status
      alert("This is ALREADY completed");
      return;
    }    
    //Check if there are B->B entries with IP status or A->B entries with RD/PD/IP status
    if((selPackagedItem.pRule.curTaskCode === selPackagedItem.pRule.nextTaskCode)
    && (selPackagedItem.packageStatus === myConstants.packageStatus.IP)
    && ((selPackagedItem.pRule.bMultipleOutput) || (selPackagedItem.pRule.bReducedOutput)))
    {//B->B IP 
      const modal = this.modalCtrl.create('TaskOutputPage',{pItem:selPackagedItem});
      modal.present();
      modal.onDidDismiss((data) => {
        //this.initializeData();  
        //this.filterValue="B_B_CO";
        //this.filterPackagedItems(this.filterValue);
      });
    }
    else if((selPackagedItem.pRule.curTaskCode === selPackagedItem.pRule.nextTaskCode)
            && (selPackagedItem.packageStatus === myConstants.packageStatus.IP)
            && (!selPackagedItem.pRule.bMultipleOutput) 
            && (!selPackagedItem.pRule.bReducedOutput))
    {//A->B where packagePartialAssignment=false OR B->B IP for single o/p same as original
      /*CREATE NEW ENTRY B->C in RD status and B->B in CO status
      (01) Create new object of packagedItem
      (02) Set item Details and packageID related fields
      (03) Find out applicable package rule
      (03.1) Set package rule
      (03.2) set owner details
      (04) add new entry to DB 
      (05) Change status to CO for selPackagedItem
      (06) refresh data for this page
      */
      console.log("CREATE NEW ENTRY B->C in RD status");
      //(01) Create new object of packagedItem
      let packagedItemBtoC = {} as packagedItem;
      
      //(02) Set item Details and packageID related fields                            
      packagedItemBtoC.itemQty = selPackagedItem.itemQty;
      packagedItemBtoC.itemWeight = selPackagedItem.itemWeight;
      packagedItemBtoC.itemCost = selPackagedItem.itemCost;
      packagedItemBtoC.itemTax = selPackagedItem.itemTax;
      packagedItemBtoC.itemRemarks = selPackagedItem.itemRemarks;
      
      packagedItemBtoC.packageID = selPackagedItem.packageID;
      packagedItemBtoC.parentPackageID = selPackagedItem.parentPackageID;
      packagedItemBtoC.packageGroupID = selPackagedItem.packageGroupID;
      
      //(03) Find out applicable package rule
      try
      {
        let packageRuleListBtoC:Array<packageRule> = [];
        packageRuleListBtoC = await this.inventoryProvider.getPackageRule_For_A(selPackagedItem.pRule.nextTaskCode);
        if(packageRuleListBtoC.length === 1)
          {
            //(03.1) Set package rule
            packagedItemBtoC.pRule = packageRuleListBtoC[0];
            packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
            
            //(03.2) set owner details                              
            packagedItemBtoC.packageCurTaskOwner = selPackagedItem.packageNextTaskOwner;
            packagedItemBtoC.packageNextTaskOwner = selPackagedItem.packageNextTaskOwner;
            
            //(04) add new entry B->C with RD status to DB
            let loading: Loading;
            loading = this.loadingCtrl.create();
            loading.present();

            await this.inventoryProvider.createPackagedItem_BtoC(packagedItemBtoC,selPackagedItem.id);
            console.log("B->C added");
            //await this.inventoryProvider.updatePackagedItemStatus(selPackagedItem.id,myConstants.packageStatus.CO);
            await loading.dismiss();
            this.filterValue="B_B_CO";
            this.filterPackagedItems(this.filterValue);
          }
          else
          {
            console.log("More than one, B->C rules with RD status, exists for " + selPackagedItem.pRule.nextTaskCode );
          }
        }
        catch(e)
        {
          console.log("Error in inventoryProvider.getBtoCPackageRules " + e);
        };     
    }    
  }

  
  submitTask(pItem:packagedItem)
  {
    alert("PackageID: " + pItem.packageID + " Status: " + pItem.packageStatus + " Weight: " + pItem.itemWeight);
  }

  async mergeInputs()
  {
    
    const modal = this.modalCtrl.create('MergePage',{pItem:this.packagedItemList.filter(item => (item.packageStatus === "RD"))});
    modal.present();
    // console.log("Welecome TaskSummaryPage::mergeInputs() " + this.selectedPackageIDList.length);
    // let totalSelected:number=0;
    // let selectedPackagedItemList:Array<packagedItem>;
    // selectedPackagedItemList = [];
    // let packagedItemBtoB = {} as packagedItem;
    // packagedItemBtoB.itemQty = 0;
    // packagedItemBtoB.itemWeight = 0;
    // packagedItemBtoB.itemCost = 0;
    // packagedItemBtoB.itemTax = 0;
    // packagedItemBtoB.itemRemarks = '';
    // packagedItemBtoB.pRule = {} as packageRule;

    // for(let i=0;i<this.selectedPackageIDList.length;i++)
    // {
    //   console.log("selectedPackageIDList[" + i + "]" + this.selectedPackageIDList[i]);
    //   //let selectedItem:ReplaySubject<packagedItem[]>;
    //   //selectedItem = this.packagedItemList;
    //   if(this.selectedPackageIDList[i])
    //   {
    //     totalSelected++;
    //     //selectedItem = this.packagedItemList.filter
    //     //this.packagedItemList.concatMapTo()
    //     packagedItemBtoB.itemQty += +this.packagedItemList[i].itemQty;
    //     packagedItemBtoB.itemWeight += +this.packagedItemList[i].itemWeight;
    //     packagedItemBtoB.itemCost += +this.packagedItemList[i].itemCost;
    //     packagedItemBtoB.itemTax += +this.packagedItemList[i].itemTax;

    //    // packagedItemBtoB.pRule = this.packagedItemList[i].pRule;
    //     packagedItemBtoB.pRule.curTaskCode = this.packagedItemList[i].pRule.nextTaskCode;
    //     packagedItemBtoB.pRule.nextTaskCode = this.packagedItemList[i].pRule.nextTaskCode;
    //     packagedItemBtoB.pRule.bGroupRead = this.packagedItemList[i].pRule.bGroupRead;
    //     packagedItemBtoB.pRule.bIncursCost = this.packagedItemList[i].pRule.bIncursCost;
    //     packagedItemBtoB.pRule.bMultipleOutput = this.packagedItemList[i].pRule.bMultipleOutput;
    //     packagedItemBtoB.pRule.bPartialAssign = this.packagedItemList[i].pRule.bPartialAssign;
    //     packagedItemBtoB.pRule.bReducedOutput = this.packagedItemList[i].pRule.bReducedOutput;

    //     packagedItemBtoB.packageStatus = myConstants.packageStatus.IP;
        
    //     packagedItemBtoB.packageCurTaskOwner = this.packagedItemList[i].packageNextTaskOwner;
    //     packagedItemBtoB.packageNextTaskOwner = this.packagedItemList[i].packageNextTaskOwner;        

    //     packagedItemBtoB.parentPackageID = "0";
    //     packagedItemBtoB.packageGroupID = this.packagedItemList[i].packageGroupID;

    //     selectedPackagedItemList.push(this.packagedItemList[i]);
    //   }
    // }
    // packagedItemBtoB.packageID =  packagedItemBtoB.itemWeight + "_" + totalSelected;   

    // let loading: Loading;
    // loading = this.loadingCtrl.create();
    // loading.present();

    // console.log("packagedItemBtoB.packageGroupID: " + packagedItemBtoB.packageGroupID);
    // this.inventoryProvider.createPackagedItem_BtoB(packagedItemBtoB,"")
    // .then(() =>{
    //   this.inventoryProvider.updateStatus_For_AtoB_PackagedItemList(selectedPackagedItemList,packagedItemBtoB.packageGroupID,myConstants.packageStatus.CO)
    //   .then(res =>{
    //     console.log("SUCCESS!!!!");
    //     loading.dismiss();   
    //   //  this.initializeData();
    //     this.filterValue="B_B_IP";
    //     this.filterPackagedItems(this.filterValue);
    //   })
    //   .catch(e => {
    //     console.log("Error from inventoryProvider.updateStatus_For_AtoB_PackagedItemList(...)" + e)
    //   });
    // })
    // .catch(e => {
    //   console.log("Error from databaseProvider.createPackagedItem_BtoB(packagedItemBtoB)" + e)
    // });
        
  }

  showTrace(pSelectedItem:packagedItem)
  {
    const modal = this.modalCtrl.create('PackageTracePage',{pItem:pSelectedItem});
    modal.present();
    /*
    modal.onDidDismiss((data) => {
      this.initializeData();  
    }); 
    */     
  }
}
