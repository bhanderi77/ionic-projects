import { Component } from '@angular/core';
import { NavController,AlertController } from 'ionic-angular';
import { Summary } from '@angular/compiler';
import { AngularFirestoreCollection } from 'angularfire2/firestore';

import { InventoryProvider } from '../../providers/inventory/inventory';
import { PackageRulesPage } from '../package-rules/package-rules';
import { taskSummary,summaryDetails } from '../../model/taskSummary';
import { packagedItem } from '../../model/packagedItem';
import { packageRule } from '../../model/packageRule';

import { myConstants } from '../../model/myConstants';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private packagRuleList:Observable<packageRule[]>;
  private packageRulesCollectionRef: AngularFirestoreCollection<packageRule>;

  private taskSummaryList:Array<taskSummary>;
  private pItemNew:packagedItem;

  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
    private inventoryProvider: InventoryProvider) {

  }

  ionViewDidLoad() {
    console.log('HomePage::ionViewDidLoad');
  }

  ionViewDidEnter() {
    console.log('HomePage::ionViewDidEnter');
    this.initilizeData();       
   
  }

  async initilizeData()
  {
    console.log("Welcome to HomePage::initilizeData");
    this.packagRuleList = this.inventoryProvider.getPackageRulesList().valueChanges();
    this.taskSummaryList = [];
    await this.packagRuleList.subscribe(pRuleList => {
      for(let i=0;i<pRuleList.length;i++)
      {
        if(pRuleList[i].nextTaskCode != myConstants.taskCode.NONE)
        {
        console.log("Calling getTaskSummary for " + pRuleList[i].nextTaskCode)
        this.getTaskSummary(pRuleList[i].nextTaskCode);
        }
      }     
    });
  }

  async getTaskSummary(taskCode:string)
  {
    let taskSummaryRec = {} as taskSummary;
    taskSummaryRec.taskCode = taskCode;
    taskSummaryRec.Input = {} as summaryDetails;
    taskSummaryRec.Input.totalQty=0;
    taskSummaryRec.Input.totalWeight=0;
    taskSummaryRec.Input.totalCost=0;
    taskSummaryRec.Input.totalTax=0;

    taskSummaryRec.InProgress = {} as summaryDetails;
    taskSummaryRec.InProgress.totalQty=0;
    taskSummaryRec.InProgress.totalWeight=0;
    taskSummaryRec.InProgress.totalCost=0;
    taskSummaryRec.InProgress.totalTax=0;

    taskSummaryRec.Completed = {} as summaryDetails;
    taskSummaryRec.Completed.totalQty=0;
    taskSummaryRec.Completed.totalWeight=0;
    taskSummaryRec.Completed.totalCost=0;
    taskSummaryRec.Completed.totalTax=0;

    taskSummaryRec.Pending = {} as summaryDetails;
    taskSummaryRec.Pending.totalQty=0;
    taskSummaryRec.Pending.totalWeight=0;
    taskSummaryRec.Pending.totalCost=0;
    taskSummaryRec.Pending.totalTax=0;

    taskSummaryRec.Output = {} as summaryDetails;
    taskSummaryRec.Output.totalQty=0;
    taskSummaryRec.Output.totalWeight=0;
    taskSummaryRec.Output.totalCost=0;
    taskSummaryRec.Output.totalTax=0;

    let pItemListAtoB:Observable<packagedItem[]>,pItemListBtoC:Observable<packagedItem[]>,pItemListBtoB:Observable<packagedItem[]>;
    pItemListAtoB = await this.inventoryProvider.get_packagedItems_For_B(taskCode).valueChanges();
    pItemListBtoC = await this.inventoryProvider.get_packagedItems_For_A(taskCode).valueChanges();
    pItemListBtoB = await this.inventoryProvider.get_BtoB_packagedItems(taskCode).valueChanges();
    
    await pItemListAtoB.subscribe(pItemList => {
       pItemList.forEach(pItem => {
        if((pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
        && ( (pItem.packageStatus === myConstants.packageStatus.RD)
          || (pItem.packageStatus === myConstants.packageStatus.PD)
          || (pItem.packageStatus === myConstants.packageStatus.IP)
          || (pItem.packageStatus === myConstants.packageStatus.CO)
          ))
        {//A->B and  either RD or PD or CO or IP
          taskSummaryRec.Input.totalQty += pItem.itemQty;
          taskSummaryRec.Input.totalWeight += pItem.itemWeight;
          taskSummaryRec.Input.totalCost += pItem.itemCost;
          taskSummaryRec.Input.totalTax += pItem.itemTax;
        }
      });
    });

    await pItemListBtoB.subscribe(pItemList => {
       pItemList.forEach(pItem => {
        if((pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode) && (pItem.packageStatus === myConstants.packageStatus.IP))
        {//B->B and IP 
          taskSummaryRec.InProgress.totalQty += pItem.itemQty;
          taskSummaryRec.InProgress.totalWeight += pItem.itemWeight;
          taskSummaryRec.InProgress.totalCost += pItem.itemCost;
          taskSummaryRec.InProgress.totalTax += pItem.itemTax;
        }
        else if((pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode) && (pItem.packageStatus === myConstants.packageStatus.CO))
        {//B->B and CO
          taskSummaryRec.Completed.totalQty += pItem.itemQty;
          taskSummaryRec.Completed.totalWeight += pItem.itemWeight;
          taskSummaryRec.Completed.totalCost += pItem.itemCost;
          taskSummaryRec.Completed.totalTax += pItem.itemTax;
        }
      });
    });

    await pItemListBtoC.subscribe(pItemList => {
       pItemList.forEach(pItem => {
        if( (pItem.pRule.curTaskCode !== pItem.pRule.nextTaskCode)
        && ( (pItem.packageStatus === myConstants.packageStatus.RD)
          || (pItem.packageStatus === myConstants.packageStatus.PD)
          || (pItem.packageStatus === myConstants.packageStatus.CO)
          || (pItem.packageStatus === myConstants.packageStatus.IP)
           ))
          {
            taskSummaryRec.Output.totalQty += pItem.itemQty;
            taskSummaryRec.Output.totalWeight += pItem.itemWeight;
            taskSummaryRec.Output.totalCost += pItem.itemCost;
            taskSummaryRec.Output.totalTax += pItem.itemTax;
          }  
      });
    });

    taskSummaryRec.Pending.totalQty = taskSummaryRec.Input.totalQty - (taskSummaryRec.InProgress.totalQty + taskSummaryRec.Completed.totalQty);
    taskSummaryRec.Pending.totalWeight = taskSummaryRec.Input.totalWeight - (taskSummaryRec.InProgress.totalWeight + taskSummaryRec.Completed.totalWeight);
    taskSummaryRec.Pending.totalCost = taskSummaryRec.Input.totalCost - (taskSummaryRec.InProgress.totalCost + taskSummaryRec.Completed.totalCost);
    taskSummaryRec.Pending.totalTax = taskSummaryRec.Input.totalTax - (taskSummaryRec.InProgress.totalTax + taskSummaryRec.Completed.totalTax);
    this.taskSummaryList.push(taskSummaryRec);
  }

  gotoPackageRules()
  {
    this.navCtrl.push('PackageRulesPage');
  }

  gotoTaskSummary(taskCode:string)
  {
    console.log('HomePage::gotoTaskSummary(taskCode) for ' + taskCode);
    this.navCtrl.push('TaskSummaryPage',{taskCode:taskCode});  
  }

  async addItem()
  {
    console.log("HomePage::addItem");
    let pRules:Array<packageRule>;
    let pItem = {} as packagedItem;
    this.inventoryProvider.getPackageRule_For_A("BUY")
    .then(data => {
      pRules = data;
      console.log("addItem=>pRules.length " + pRules.length);
    
      if(pRules.length === 1)
      {
        console.log("pRule " + pRules[0].curTaskCode + " " + pRules[0].nextTaskCode);
      
        let alert = this.alertCtrl.create(
        {
          title: "Buy Rough",
          inputs: [ { name: 'InputWeight', placeholder: 'Enter Weight'},
                    { name: 'InputCost', placeholder: 'Enter Cost'},
                    { name: 'InputTax', placeholder: 'Enter Tax'},
                  ],
          buttons:
                  [ { 
                      text: 'Cancel',
                      handler : data => { console.log('Cancel clicked');}
                    },
                    { 
                      text: 'Ok',
                      handler : data => 
                      { 
                        pItem.itemWeight = +data.InputWeight;
                        pItem.itemCost = +data.InputCost;
                        pItem.itemTax = +data.InputTax;
                        pItem.itemQty = 0;
                        pItem.itemRemarks = "";
                        pItem.pRule = pRules[0];
                        pItem.packageID = "0";
                        pItem.parentPackageID = "0";
                        pItem.packageGroupID = "0";
                        pItem.packageStatus = myConstants.packageStatus.RD;

                        this.inventoryProvider.createPackagedItem_BtoC(pItem,pItem.parentPackageID);
                      }
                    },
                  ]
        });
        alert.present();
      }
    }).catch(e => {
      console.log("Error from this.inventoryProvider.getBtoCPackageRules" + e);
    });      
  }

}
