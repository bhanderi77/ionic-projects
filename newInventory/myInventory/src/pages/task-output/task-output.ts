import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Loading,LoadingController,ItemSliding,ViewController} from 'ionic-angular';
//import { DatabaseProvider } from '../../providers/database/database';
import { InventoryProvider } from '../../providers/inventory/inventory';

import { packagedItem } from '../../model/packagedItem';
import { packageRule } from '../../model/packageRule';
import { myConstants } from '../../model/myConstants';
import { taskOutput } from '../../model/taskOutput';
/**
 * Generated class for the TaskOutputPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-task-output',
  templateUrl: 'task-output.html',
})
export class TaskOutputPage {

  bValidOutput:boolean;
  private totalOutputWeightFlag:boolean;
  private totalOutputQtyFlag:boolean;
  private outputWeightFlag:boolean;
  private outputQtyFlag:boolean;

  private totalOutputQty:number;
  private pItem:packagedItem;
  private output:Array<taskOutput>;
  private maxInput:number;
  private inputWeight:number;
  private totalOutputWeight:number;

  constructor(private navCtrl: NavController,
    private viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private inventoryProvider: InventoryProvider,
    private navParams: NavParams ) {
      this.pItem = navParams.get("pItem");     
      // this.outputWeight = new Array(myConstants.TASK_OUTPUT_MAX_REC);
      // this.outputWeight.length=1;
      this.totalOutputWeightFlag=true;
      this.totalOutputQtyFlag=true;
      this.outputQtyFlag=true;
      this.outputWeightFlag=true;

      this.bValidOutput=false;
      this.inputWeight= +this.pItem.itemWeight;
      this.output=[];
      this.totalOutputWeight=0;
      this.totalOutputQty=0;
      this.initializeData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskOutputPage');
  }

  dismissModel(){
    this.viewCtrl.dismiss();
  }

  TrackBy(index: number, obj: any):any
  {
    console.log("TrackBy(index,obj) " + index + " " + obj);
    return index;
  }

  initializeData(){
    let o = {} as taskOutput;
    o.weight= +0;
    o.qty= +0;
    o.remark=this.pItem.itemRemarks;
    o.validWeight=false;
    o.validQty=false;
    this.output.push(o);
  }

  deleteInputRow(o:taskOutput,item: ItemSliding){
    this.output.splice(this.output.indexOf(o),1);
    this.validateOutput();
    
  }


  addInputRow(){
    let o= {} as taskOutput;
    o.weight= +0;
    o.qty= +0;
    o.remark="";
    o.validWeight=false;
    o.validQty=false;
    this.output.push(o);
    this.bValidOutput=false;
  }

  validateWeight(o:taskOutput)
  {
    console.log("Welcome to validateWeight : " + o.weight + " " + o.qty);
    
    if(( +o.weight > +this.inputWeight) || (+o.weight <= 0))
    {
      console.log("o.weight " + o.weight + " this.inputWeight " + this.inputWeight);
      o.validWeight=false;
      this.outputWeightFlag = false;
    }
    else
    {
      o.validWeight=true;
      this.outputWeightFlag = true;
    }
   
    this.validateOutput();
  }

  validateQty(o:taskOutput){
    if(+o.qty <= 0)
    {
      o.validQty = false;
      this.outputQtyFlag = false;
    }
    else
    {
      o.validQty = true;
      this.outputQtyFlag = true;
    }

    
    this.validateOutput();
  }


  validateOutput(){
    this.totalOutputWeight=0;
    this.totalOutputQty=0;

    for(let i = 0 ; i < this.output.length ;i++)
    {
      this.totalOutputWeight += +(this.output[i].weight);
      this.totalOutputQty += +(this.output[i].qty);
    }
    
    if(+this.totalOutputWeight <= +this.inputWeight)
      this.totalOutputWeightFlag = true;
    else
      this.totalOutputWeightFlag = false;

    if(+this.totalOutputQty < +this.pItem.itemQty)
      this.totalOutputQtyFlag = false;
    else
      this.totalOutputQtyFlag = true;

    if( (this.totalOutputWeightFlag === true) && (this.outputWeightFlag === true)
     && (this.totalOutputQtyFlag === true) && (this.outputQtyFlag === true ))
    {
      this.bValidOutput = true;
      for(let i=0;i<this.output.length;i++){
        if(!(this.output[i].validWeight) || !(this.output[i].validQty)){
          this.bValidOutput = false;
          break;
        }
      }
    }
    else
    {
      this.bValidOutput = false;
    }
  }


  async saveOutput()
  {
    /*CREATE NEW ENTRY B->C in RD status and update B->B to CO status
    (01) Create new object of packagedItem
    (02) Set item Details and packageID related fields
    (03) Find out applicable package rule
    (03.1) Set package rule
    (03.2) set owner details
    (04) add new entry to DB 
    (05) Change status to CO for selPackagedItem
    (06) refresh data for this page
    */
   console.log("TaskSummaryPage: Welcome to saveOutput() ");
   
   let bMultipleOutput:boolean=false;
   //let totalOutput:number=0;
   // totalOutput = this.output[0].weight;
   for(let i=1;(i<this.output.length && this.output[i].weight>0);i++)
      {
        bMultipleOutput=true;
        // totalOutput += +(this.output[i].weight);    
      }
  /*
  if((this.pItem.packageMultipleOutput) && (!bMultipleOutput))
  {
    alert("Enter multiple outputs");
    return;
  }  
  else */
  if((!this.pItem.pRule.bMultipleOutput) && (bMultipleOutput))
  {
    alert("Enter single output");
    return;
  }
  

  if((this.totalOutputWeight === this.pItem.itemWeight) && (this.pItem.pRule.bReducedOutput))
  {
    alert("Total Output can not be same as input");
    return;
  }
   console.log("bMultipleOutput: " + bMultipleOutput);  
   console.log("totalOutput: " + this.totalOutputWeight); 
   
   let loading: Loading;
  loading = this.loadingCtrl.create();
  loading.present();

   //(03) Find out applicable package rule
   let packageRuleListBtoC:Array<packageRule> = [];
   return this.inventoryProvider.getPackageRule_For_A(this.pItem.pRule.nextTaskCode)
   .then(data => 
   {
    //loading.dismiss();
        
     //console.log("Await completed for this.databaseProvider.getBtoCPackageRules")
    packageRuleListBtoC = data;
    if(packageRuleListBtoC.length === 1)
    {
      let packagedItemBtoCList:Array<packagedItem> = [];
      console.log("this.outputWeight.length " + this.output.length);
      
      for(let i=0;(i<this.output.length && (this.output[i].weight)>0 && (this.output[i].qty)>0);i++)
      {
        console.log("this.outputWeight[" + i + "]: " + (this.output[i].weight));
        let packagedItemBtoC = {} as packagedItem;
        
        packagedItemBtoC.itemRemarks ="";
        //(02) Set item Details and packageID related fields                            
        packagedItemBtoC.itemQty = (this.output[i].qty);
        packagedItemBtoC.itemWeight = (this.output[i].weight);
        
        //TBC
        if(bMultipleOutput)
        {
          packagedItemBtoC.itemCost = Math.ceil((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.itemCost));
          packagedItemBtoC.itemTax = Math.ceil((packagedItemBtoC.itemWeight/this.totalOutputWeight) * (this.pItem.itemTax));
        }
        else
        {
          packagedItemBtoC.itemCost = (this.pItem.itemCost);
          packagedItemBtoC.itemTax = (this.pItem.itemTax);
        }  
        if(this.pItem.pRule.bIncursCost)
        {
          packagedItemBtoC.itemCost += 200;
          packagedItemBtoC.itemTax += 5;
        }
        
        if((!bMultipleOutput) && (this.totalOutputWeight === this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "=" + packagedItemBtoC.itemWeight;        
        }
        else if((!bMultipleOutput) && (this.totalOutputWeight !== this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "-" + packagedItemBtoC.itemWeight;        
        }
        else if((bMultipleOutput) && (this.totalOutputWeight === this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "<=" + packagedItemBtoC.itemWeight;        
        }
        else if((bMultipleOutput) && (this.totalOutputWeight !== this.pItem.itemWeight))
        {
          packagedItemBtoC.packageID = this.pItem.packageID + "<-" + packagedItemBtoC.itemWeight;        
        }
        if(bMultipleOutput)
        {
          packagedItemBtoC.packageGroupID = this.pItem.packageID + "_" + this.output.length;
        }
        else
        {
          packagedItemBtoC.packageGroupID = this.pItem.packageGroupID;
        }
        
        packagedItemBtoC.parentPackageID = this.pItem.parentPackageID;
        
        /*
        if(this.pItem.packagePartialAssignment)
        {
          packagedItemBtoC.parentPackageID = this.pItem.id;
        }
        else
        {
          packagedItemBtoC.parentPackageID = this.pItem.parentPackageID;
        }
        */
        
        //(03.1) Set package rule  
        packagedItemBtoC.pRule = packageRuleListBtoC[0];
        packagedItemBtoC.packageStatus = myConstants.packageStatus.RD;
        //(03.2) set owner details                              
        packagedItemBtoC.packageCurTaskOwner = this.pItem.packageNextTaskOwner;
        packagedItemBtoC.packageNextTaskOwner = this.pItem.packageNextTaskOwner;
        
        packagedItemBtoCList.push(packagedItemBtoC);       
      }
      // console.log("length of packagedItemBtoCList = " + );
      loading.present();
       //(04) add new entry B->C with RD status to DB
      return this.inventoryProvider.createPackagedItemArray_BtoC(packagedItemBtoCList,this.pItem.id)
      .then(()=>{
        console.log("B->C added");
        loading.dismiss();        
        this.navCtrl.pop();  
        /*  
        return this.inventoryProvider.updatePackagedItemStatus(this.pItem.id,myConstants.packageStatus.CO)
        .then(res=>{
          console.log("B->B updated to CO"); 
                  
        })
        .catch(e=>{
          console.log("Error in this.inventoryProvider.updatePackagedItemStatus()" + e);
        });*/
      }).catch(e=>{
        console.log("Error in this.inventoryProvider.createPackagedItemArray(packagedItemBtoCList)" + e);
      });
    }//end of if(packageRuleListBtoC.length === 1)
    else if(packageRuleListBtoC.length === 0)
    {
      alert("TaskOutputPage:No rule exists to save output");
    }
    else if(packageRuleListBtoC.length > 1)
    {
      alert("TaskOutputPage:Multiple rule exists to save output");
    }
   })
   .catch(e=>{
     console.log("Error in this.inventoryProvider.getPackageRule_For_A()" + e);
   });
  }
}
