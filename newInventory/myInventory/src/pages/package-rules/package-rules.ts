import { Component } from '@angular/core';
import { IonicPage, 
  NavController, NavParams,
  AlertController,
  Loading, LoadingController } from 'ionic-angular';
import { InventoryProvider } from '../../providers/inventory/inventory';
import { packageRule } from '../../model/packageRule';

//import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { myConstants } from '../../model/myConstants';

/**
 * Generated class for the PackageRulesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-package-rules',
  templateUrl: 'package-rules.html',
})
export class PackageRulesPage {
  private packagRuleList:Observable<packageRule[]>;
  private packageRulesCollectionRef: AngularFirestoreCollection<packageRule>;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private inventoryProvider: InventoryProvider) {
      //this.packagRuleList = [];    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackageRulesPage');
    this.packagRuleList = this.inventoryProvider.getPackageRulesList().valueChanges();
    /*
    this.packagRuleList = this.inventoryProvider.getPackageRulesList().snapshotChanges().map(actions => {
      console.log("snapshotChanged");
      actions.map(a => {
        console.log("a:");
        const data = a.payload.doc.data() as packageRule;
        const id = a.payload.doc.id;
        return {id,data};
      })
    });*/

  }
  ionViewDidEnter() {
    console.log('ionViewDidEnter PackageRulesPage');    
  }

  /*
  initilizeData()
  {
    this.packagRuleList = [];
    this.databaseProvider.getPackageRules()
    .then(data => {
      this.packagRuleList = data;    

      this.packagRuleList.forEach(rule => {
          console.log("ID: " + rule.id + "\n"
          + "curGrouped: " + rule.bGroupRead + "\n"
          + "curTaskCode: " + rule.curTaskCode + "\n"
          + "packageStatus: " + rule.packageStatus + "\n"
          + "bMultipleOutput: " + rule.bMultipleOutput + "\n"
          + "bPartialAssign: " + rule.bPartialAssign + "\n"
          + "bReducedOutput: " + rule.bReducedOutput + "\n"
          + "bIncursCost: " + rule.bIncursCost + "\n") ;
      });      
    }); 
  }*/


  addRule()
  {
    console.log('HomePage::addRule() : ');
    let alert = this.alertCtrl.create({
      message: 'New Rule',
      inputs: [
        { name: 'curTaskCode',placeholder:'Current Task Code', type: 'boolean' },
      //  { name: 'packageStatus',placeholder:'Status',type: 'string' },
        { name: 'nextTaskCode',placeholder:'Next Task Code',type: 'string' },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          },
        },
        {
          text: 'Add',
          handler: data => {
            let p = {} as packageRule;
            p.curTaskCode = data.curTaskCode;
            //p.packageStatus = data.packageStatus;
            p.nextTaskCode = data.nextTaskCode;
            p.bGroupRead = false;
            p.bMultipleOutput = true;
            p.bPartialAssign = false;
            p.bReducedOutput = true;
            p.bIncursCost = true;
            
            this.inventoryProvider.createPackageRule(p).then((newDoc) => {
              console.log("Package Rule Created !!");
            });
          },
        },
      ],
    });
    alert.present();
  }

  async deleteRule(pID:string)
  {
    console.log("Welcome to PackageRulesPage:deleteRule(packageRule): " + pID);
    let pRule = {} as packageRule;
    let loading: Loading = this.loadingCtrl.create();
    loading.present();

    try
    {
      pRule = <packageRule>await this.inventoryProvider.getPackageRule(pID);
      if(pRule)
      {//Rule exists
        await this.inventoryProvider.deletePackageRule(pID);
        await loading.dismiss();   
      }
      else
      {//Rule does not exists
        await loading.dismiss();      
        alert("PackageRule " + pID + " does not exist");
      }
    }
    catch(e)
    {
      await loading.dismiss();   
    }
  }

  async updateRule(p:packageRule)
  {
    console.log("Welcome to PackageRulesPage:updateRule(packageRule): " + p.id);
    await this.inventoryProvider.updatePackageRule(p);
  }
}
