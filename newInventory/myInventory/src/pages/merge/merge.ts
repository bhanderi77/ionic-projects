import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , Loading , LoadingController , ViewController } from 'ionic-angular';
import { packagedItem , itemGroup } from '../../model/packagedItem';
import { packageRule } from '../../model/packageRule';
import { myConstants } from '../../model/myConstants';
import { taskOutput } from '../../model/taskOutput';
import { InventoryProvider } from '../../providers/inventory/inventory';

/**
 * Generated class for the MergePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-merge',
  templateUrl: 'merge.html',
})
export class MergePage {
  private pItems:Array<packagedItem>;
  private pItemGroups :Array<itemGroup>;
  private selectedGroup : itemGroup;
  
  constructor(public navCtrl: NavController,    
    private loadingCtrl: LoadingController,    
    public navParams: NavParams,
    private viewCtrl: ViewController ,
    private inventoryProvider: InventoryProvider) {
    this.pItems = navParams.get("pItem");
    this.pItemGroups = [];
    this.selectedGroup=null;
    let result:Array<string> = [];

    //for finding unique packageGroupID
    for ( let i = 0 ; i < this.pItems.length ; i++ )
    {
      if(result.indexOf(this.pItems[i].packageGroupID) > -1)
        continue;
      else
        result.push(this.pItems[i].packageGroupID);
    }
    //for expected number of packages
    for( let i = 0 ; i < result.length ; i++ ){
      let iGroup = {} as itemGroup;
      iGroup.groupID = result[i];
      iGroup.expectedNumberOfPackage = parseInt(result[i].substring(result[i].indexOf("_")+1));
      iGroup.actualNumberOfPackage = 0;
      iGroup.disabled = false;
      //iGroup.bSelected = false;
      this.pItemGroups.push(iGroup);
      console.log("group item " + this.pItemGroups[i].expectedNumberOfPackage +" "+ this.pItemGroups[i].groupID);
    }

    //for actual number of packages & disable checkbox
    for( let i = 0 ; i < this.pItemGroups.length ; i++ )
    {
      this.pItemGroups[i].actualNumberOfPackage = this.pItems.filter(item => (item.packageGroupID === this.pItemGroups[i].groupID)).length;
      if(this.pItemGroups[i].expectedNumberOfPackage === this.pItemGroups[i].actualNumberOfPackage)
        this.pItemGroups[i].disabled = false;
      else
        this.pItemGroups[i].disabled = true;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MergePage');
  }

  dismissModel(){
    this.viewCtrl.dismiss();
  }

  mergePackages()
  {
    console.log("selected group : " + this.selectedGroup.groupID );
  //   let selectedGroups:Array<itemGroup>=[];
    let pItemsToBeMerged:Array<packagedItem>=[];
    let pMergedItem = {} as packagedItem;
    //selectedGroups = this.pItemGroups.filter(group => (group.bSelected) === true );
  //   console.log("selected groups : " + selectedGroups);
    // for( let i = 0 ; i < selectedGroups.length ; i++)
  //   {
      
      pItemsToBeMerged = this.pItems.filter(item => (item.packageGroupID === this.selectedGroup.groupID));
      console.log("itemsToBeMerged : " + pItemsToBeMerged);
      pMergedItem.itemCost = 0 ;
      pMergedItem.itemWeight = 0 ;
      pMergedItem.itemQty = 0 ;
      pMergedItem.itemTax = 0 ;
      pMergedItem.itemRemarks = '' ;
      pMergedItem.pRule = {} as packageRule;

      for ( let i = 0 ; i < pItemsToBeMerged.length ; i++ )
      {
        pMergedItem.itemWeight += +pItemsToBeMerged[0].itemWeight;
        pMergedItem.itemQty += +pItemsToBeMerged[0].itemQty;
        pMergedItem.itemTax += +pItemsToBeMerged[0].itemTax;
        pMergedItem.itemCost += +pItemsToBeMerged[0].itemCost;
      }
      pMergedItem.pRule.curTaskCode = pItemsToBeMerged[0].pRule.nextTaskCode;
      pMergedItem.pRule.nextTaskCode = pItemsToBeMerged[0].pRule.nextTaskCode;
      pMergedItem.pRule.bGroupRead = pItemsToBeMerged[0].pRule.bGroupRead;
      pMergedItem.pRule.bIncursCost = pItemsToBeMerged[0].pRule.bIncursCost;
      pMergedItem.pRule.bMultipleOutput = pItemsToBeMerged[0].pRule.bMultipleOutput;
      pMergedItem.pRule.bPartialAssign = pItemsToBeMerged[0].pRule.bPartialAssign;
      pMergedItem.pRule.bReducedOutput = pItemsToBeMerged[0].pRule.bReducedOutput;
      
      pMergedItem.packageStatus = myConstants.packageStatus.IP;
      pMergedItem.packageCurTaskOwner = pItemsToBeMerged[0].packageNextTaskOwner;
      pMergedItem.packageNextTaskOwner = pItemsToBeMerged[0].packageNextTaskOwner;

      pMergedItem.parentPackageID = "0";
      pMergedItem.packageGroupID = pItemsToBeMerged[0].packageGroupID;
      
      pMergedItem.packageID =  pMergedItem.itemWeight + "_" + pItemsToBeMerged.length;

  //     /*
  //     1) Create object for BtoB item
  //     2) Set all fields of BtoB item , by sum of relevant fields from itemsToBeMerged
  //     3) Push object into array of packagedItemBtoBArray
  //     */
      // pItemsBtoB.push(pMergedItem);
    // }
    console.log("pItemsBtoB" + pMergedItem);
    //Call Firebase
    let loading:Loading;
    loading = this.loadingCtrl.create();
    loading.present();
    this.inventoryProvider.createPackagedItem_BtoB(pMergedItem,pItemsToBeMerged)
    .then(()=>{
      console.log("Inserted BtoB array");
      loading.dismiss();
      this.viewCtrl.dismiss();
    });

  }

}
