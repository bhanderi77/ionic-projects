import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ViewController } from 'ionic-angular';

//import { DatabaseProvider } from '../../providers/database/database';
import { InventoryProvider } from '../../providers/inventory/inventory';

import { packagedItem } from '../../model/packagedItem';
import { myConstants } from '../../model/myConstants';

/**
 * Generated class for the PackageTracePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-package-trace',
  templateUrl: 'package-trace.html',
})
export class PackageTracePage {
  private pItem:packagedItem;
  private ancestorsPackagedItemList:Array<packagedItem>;
  private descendantPackagedItemList:Array<packagedItem>;
  
  constructor(public navCtrl: NavController, 
    private inventoryProvider: InventoryProvider,
    private viewCtrl: ViewController,
    public navParams: NavParams) {

    this.pItem = navParams.get("pItem");
    this.ancestorsPackagedItemList = [];   
    this.descendantPackagedItemList = [];    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackageTracePage');
  }
  ionViewDidEnter()
  {
    console.log('ionViewDidEnter PackageTracePage');
    this.getTrace();  
  }

  dismissModel(){
    this.viewCtrl.dismiss();
  }

  getTrace()
  {
    console.log("Welcome to PackageTracePage::getTrace()");
    console.log(this.pItem.id + ":" + this.pItem.parentPackageID + "=>" + this.pItem.packageID + " " + this.pItem.pRule.curTaskCode + " " +  this.pItem.packageStatus + " " + this.pItem.pRule.nextTaskCode + " " + this.pItem.itemWeight);
    let traceID:string='0';
    let traceGroupID:string='0';
    let bTraceGroup:boolean=false;
    let packagedItemList:Array<packagedItem>;
    packagedItemList = [];

    this.inventoryProvider.get_AtoB_packagedItems().valueChanges()
    .subscribe(pItemList => {
    
      packagedItemList = pItemList;

      //Trace Parent
      console.log("Parents:");
      if((this.pItem.parentPackageID==='0') && (this.pItem.packageGroupID != '0'))
      {
        bTraceGroup=true;
        traceGroupID = this.pItem.packageGroupID;
        traceID = this.pItem.id;
      }
      else
      {
        bTraceGroup=false;
        traceGroupID='0'; 
        traceID=this.pItem.parentPackageID;             
      }
      
      while((traceID!=='0') || (bTraceGroup))
      {
        //console.log("traceID:" + traceID);    
        //currentItem = new Array(1);
        let currentItem:Array<packagedItem>;
        currentItem = [];
        console.log(" traceID:" + traceID + " bTraceGroup: " + bTraceGroup + " traceGroupID:" + traceGroupID);
        if(bTraceGroup)
        {
          currentItem = packagedItemList.filter(packagedItem=>((packagedItem.packageGroupID === traceGroupID) && (packagedItem.id !== traceID)));
        }
        else
        {
          currentItem = packagedItemList.filter(packagedItem=>(packagedItem.id === traceID));
        }
        console.log("currentItem.length: " + currentItem.length);
        if(currentItem.length >= 1)
        {
          for(let i=0;(i<currentItem.length);i++)
          {
            if((currentItem[i].pRule.curTaskCode !== currentItem[i].pRule.nextTaskCode) || (currentItem[i].pRule.bPartialAssign))
            {
              console.log(currentItem[i].id + ":" + currentItem[i].parentPackageID + "=>" + currentItem[i].packageID + " " + currentItem[0].pRule.curTaskCode + " " +  currentItem[i].packageStatus + " " + currentItem[i].pRule.nextTaskCode + " " + currentItem[i].itemWeight);
              this.ancestorsPackagedItemList.push(currentItem[i]);            
            }
            if(currentItem[i].parentPackageID==='0')
            {
              if(currentItem[i].packageGroupID ==='0')
              {
                traceID='0';
                traceGroupID='0';
                bTraceGroup=false;
              }
              else
              {
                traceGroupID=currentItem[i].packageGroupID;
                traceID = currentItem[i].id;
                bTraceGroup=true;
              }
            }
            else
            {
              traceID=currentItem[i].parentPackageID;
              bTraceGroup=false;          
            }
          }//end of for loop
          if(currentItem.length > 1)
          {
            traceID='0';
            traceGroupID='0';
            bTraceGroup=false;          
          }
        }
        else
        {
          traceID='0';
          traceGroupID='0'; 
          bTraceGroup=false;               
        }
      }
      this.ancestorsPackagedItemList.reverse();
      //Add selected package to trace
      
      //Trace Child
      console.log("Childrens:");
      traceID=this.pItem.id;
      while(traceID!=='0')
      {
        console.log("ppID:" + traceID);  
          
        //currentItem = new Array(1);
        let currentItem:Array<packagedItem>;
        currentItem = [];
    
        currentItem = packagedItemList.filter(packagedItem=>(packagedItem.parentPackageID === traceID));
        console.log("currentItem.length: " + currentItem.length);
        if(currentItem.length)
        {
          for(let i=0;(i<currentItem.length);i++)
          {
            if((currentItem[i].pRule.curTaskCode !== currentItem[i].pRule.nextTaskCode) || (currentItem[i].pRule.bPartialAssign))
            {
              console.log(currentItem[i].id + ":" + currentItem[i].parentPackageID + "=>" + currentItem[i].packageID + " " + currentItem[i].pRule.curTaskCode + " " +  currentItem[i].packageStatus + " " + currentItem[i].pRule.nextTaskCode + " " + currentItem[i].itemWeight);
              this.descendantPackagedItemList.push(currentItem[i]);
              traceID=currentItem[i].id;   
            }
            else
            {
              traceID=currentItem[i].id;   
            }
          }          
        }
        else
        {
          traceID='0';
        }
      }
    });
  }

}
