import { myConstants } from '../model/myConstants';
import { packageRule } from '../model/packageRule';

export interface packagedItem {
    id:string;
    packageID:string;
    parentPackageID:string;
    packageGroupID:string;
    packageStatus: myConstants.packageStatus;
    packageCurTaskOwner: string;
    packageNextTaskOwner: string;    
    pRule:packageRule;
    itemRemarks: string;
    itemQty: number;
    itemWeight: number;
    itemCost: number;
    itemTax: number;
}

export interface itemGroup 
{
    groupID:string;
    expectedNumberOfPackage:number;
    actualNumberOfPackage:number;
    disabled : boolean;
//    bSelected : boolean;
}
