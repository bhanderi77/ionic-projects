import { myConstants } from '../model/myConstants';
//import { datetime } from '';

export interface packageRule {
    id:string;
    creationt_time:string;
    curTaskCode: string;
    //packageStatus: myConstants.packageStatus;    
    nextTaskCode: string;
    bGroupRead: boolean;    
    bMultipleOutput: boolean;
    bPartialAssign: boolean;
    bReducedOutput:boolean; 
    bIncursCost:boolean;   
}
