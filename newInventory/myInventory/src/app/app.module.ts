import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireModule } from 'angularfire2';

import { InventoryProvider } from '../providers/inventory/inventory';
import { firebaseConfig } from './firebaseCrediential';

/*
const firebaseConfig = {
  apiKey: "AIzaSyCoLciL31Pd1XBSuI6nI2W_OcjT-fqp-o4",
  authDomain: "demofb-f134c.firebaseapp.com",
  databaseURL: "https://demofb-f134c.firebaseio.com",
  projectId: "demofb-f134c",
  storageBucket: "",
  messagingSenderId: "996567777961"
};*/

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),    
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    InventoryProvider    
  ]
})
export class AppModule {}
