import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the NewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new',
  templateUrl: 'new.html',
})
export class NewPage {

  message:string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.message = navParams.get("message");
    console.log("Message : " , this.message);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewPage');
  }

  goToPreviousPage(){
    this.navCtrl.pop(); 
  }

}
