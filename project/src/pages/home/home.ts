import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewPage } from '../new/new';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  userName: string;
  userList: Array<any>;
  searchInput: string = '';
  constructor(public navCtrl: NavController) {
    this.userName = "";
    this.userList = [{ "name": "hardik" }, { "name": "saloni" }, { "name": "monika" }];
  }

  goToNewPage() {
    this.navCtrl.push(NewPage, { "message": "This is my message" });
  }

  addUser() {
    console.log('Entering displayUserNameAndPassword() ');
    this.userList.push({ "name": this.userName });
    this.userName = "";
    console.log('userList ', this.userList);
    console.log('Leaving displayUserNameAndPassword() ');
  }

  getItems(ev: any) {
    this.userList = this.userList.filter((user) => {
      return (user.name.toLowerCase().indexOf(this.searchInput.toLowerCase()) > -1);
    })
  }

}
